<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Tlt
 * @author     Natchapol Kittigul <afronutt@gmail.com>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Tlt', JPATH_COMPONENT);
JLoader::register('TltController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = JControllerLegacy::getInstance('Tlt');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
