<?php
/**
 * @version     CVS: 1.0.0
 * @package     com_tlt
 * @subpackage  mod_tlt
 * @author      Natchapol Kittigul <afronutt@gmail.com>
 * @copyright   2017 Natchapol Kittigul
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
$elements = ModTltHelper::getList($params);
?>

<?php if (!empty($elements)) : ?>
	<table class="table">
		<?php foreach ($elements as $element) : ?>
			<tr>
				<th><?php echo ModTltHelper::renderTranslatableHeader($params, $params->get('field')); ?></th>
				<td><?php echo ModTltHelper::renderElement(
						$params->get('table'), $params->get('field'), $element->{$params->get('field')}
					); ?></td>
			</tr>
		<?php endforeach; ?>
	</table>
<?php endif;
