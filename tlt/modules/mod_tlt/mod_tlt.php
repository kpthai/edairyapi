<?php

/**
 * @version     CVS: 1.0.0
 * @package     com_tlt
 * @subpackage  mod_tlt
 * @author      Natchapol Kittigul <afronutt@gmail.com>
 * @copyright   2017 Natchapol Kittigul
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

// Include the syndicate functions only once
JLoader::register('ModTltHelper', dirname(__FILE__) . '/helper.php');

$doc = JFactory::getDocument();

/* */
$doc->addStyleSheet(JURI::base() . '/media/mod_tlt/css/style.css');

/* */
$doc->addScript(JURI::base() . '/media/mod_tlt/js/script.js');

require JModuleHelper::getLayoutPath('mod_tlt', $params->get('content_type', 'blank'));
