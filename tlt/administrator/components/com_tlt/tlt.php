<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Tlt
 * @author     Natchapol Kittigul <afronutt@gmail.com>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_tlt'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Tlt', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('TltHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'tlt.php');

$controller = JControllerLegacy::getInstance('Tlt');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
