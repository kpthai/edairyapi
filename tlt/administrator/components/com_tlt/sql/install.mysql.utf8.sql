CREATE TABLE IF NOT EXISTS `#__tlt_member` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` VARCHAR(255)  NOT NULL ,
`surname` VARCHAR(255)  NOT NULL ,
`birthday` DATE NOT NULL ,
`email` VARCHAR(255)  NOT NULL ,
`mobile` VARCHAR(255)  NOT NULL ,
`address` TEXT NOT NULL ,
`line_id` VARCHAR(255)  NOT NULL ,
`facebook` VARCHAR(255)  NOT NULL ,
`password` VARCHAR(255)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
`modified_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8mb4_unicode_ci;

