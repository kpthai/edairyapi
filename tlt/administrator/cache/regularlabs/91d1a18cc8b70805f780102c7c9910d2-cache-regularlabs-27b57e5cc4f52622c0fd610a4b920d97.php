<?php die("Access Denied"); ?>#x#s:1405:"<?xml version="1.0" encoding="utf-8"?>
<extensions>
	<extension>
		<name>Advanced Module Manager</name>
		<id>advancedmodulemanager</id>
		<alias>advancedmodulemanager</alias>
		<extname>advancedmodules</extname>
		<version>7.2.1</version>
		<has_pro>1</has_pro>
		<pro>0</pro>
		<downloadurl>http://download.regularlabs.com/?ext=advancedmodulemanager</downloadurl>
		<downloadurl_pro></downloadurl_pro>
		<changelog><![CDATA[
<div style="font-family:monospace;font-size:1.2em;">05-Sep-2017 : v7.2.1<br /> # Fixes issue with menu assignments not working when using aliases<br /> # Fixes issue with menu assignments not working when using aliases<br /> # Fixes issue with some assignments return false positives<br /><br />04-Sep-2017 : v7.2.0<br /> + Adds ability to collect advanced parameters without using cache (usage for other extensions)<br /> + Adds time fields in date from/to calendar selection<br /> + [PRO] Fixes issue with Joomla updater not seeing new Pro versions after upgrading from Free to Pro<br /> ^ Updates Mobile Detect Library to 2.8.25<br /> ^ Updates translations: cs-CZ, es-ES<br /> # [PRO] Fixes issue with fatal error when using RedShop 2.0<br /><br />22-Jul-2017 : v7.1.6<br /> # Fixes issue with Regular Labs plugins no longer being run on category blog pages</div><div style="text-align: right;"><i>...click for more...</i></div>
		]]></changelog>
	</extension>
</extensions>";