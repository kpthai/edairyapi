<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Edairy records.
 *
 * @since  1.6
 */
class EdairyModelCow_statistics extends JModelList
{
/**
	* Constructor.
	*
	* @param   array  $config  An optional associative array of configuration settings.
	*
	* @see        JController
	* @since      1.6
	*/
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.`id`',
				'create_date', 'a.`create_date`',
				'farmer_id', 'a.`farmer_id`',
				'farm_id', 'a.`farm_id`',
				'total1', 'a.`total1`',
				'total2', 'a.`total2`',
				'total3', 'a.`total3`',
				'total4', 'a.`total4`',
				'total5', 'a.`total5`',
				'total6', 'a.`total6`',
				'total7', 'a.`total7`',
				'total8', 'a.`total8`',
				'total9', 'a.`total9`',
				'total10', 'a.`total10`',
				'total11', 'a.`total11`',
				'total12', 'a.`total12`',
				'total13', 'a.`total13`',
				'total14', 'a.`total14`',
				'total15', 'a.`total15`',
				'total_female', 'a.`total_female`',
				'total_male', 'a.`total_male`',
				'total_cow', 'a.`total_cow`',
				'remark', 'a.`remark`',
				'create_by', 'a.`create_by`',
				'ordering', 'a.`ordering`',
				'state', 'a.`state`',
				'created_by', 'a.`created_by`',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
		$this->setState('filter.state', $published);
		// Filtering create_date
		$this->setState('filter.create_date.from', $app->getUserStateFromRequest($this->context.'.filter.create_date.from', 'filter_from_create_date', '', 'string'));
		$this->setState('filter.create_date.to', $app->getUserStateFromRequest($this->context.'.filter.create_date.to', 'filter_to_create_date', '', 'string'));

		// Filtering farmer_id
		$this->setState('filter.farmer_id', $app->getUserStateFromRequest($this->context.'.filter.farmer_id', 'filter_farmer_id', '', 'string'));

		// Filtering farm_id
		$this->setState('filter.farm_id', $app->getUserStateFromRequest($this->context.'.filter.farm_id', 'filter_farm_id', '', 'string'));


		// Load the parameters.
		$params = JComponentHelper::getParams('com_edairy');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.create_date', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select', 'DISTINCT a.*'
			)
		);
		$query->from('`#__ed_cow_statistic` AS a');

		// Join over the users for the checked out user
		$query->select("uc.name AS uEditor");
		$query->join("LEFT", "#__users AS uc ON uc.id=a.checked_out");
		// Join over the foreign key 'farmer_id'
		$query->select('CONCAT(`#__ed_farmer_2754507`.`citizen_id`, \' \', `#__ed_farmer_2754507`.`name`, \' \', `#__ed_farmer_2754507`.`surname`) AS farmers_fk_value_2754507');
		$query->join('LEFT', '#__ed_farmer AS #__ed_farmer_2754507 ON #__ed_farmer_2754507.`id` = a.`farmer_id`');
		// Join over the foreign key 'farm_id'
		$query->select('CONCAT(`#__ed_farm_2754508`.`member_code`, \' \', `#__ed_farm_2754508`.`name`) AS farms_fk_value_2754508');
		$query->join('LEFT', '#__ed_farm AS #__ed_farm_2754508 ON #__ed_farm_2754508.`id` = a.`farm_id`');

		// Join over the user field 'created_by'
		$query->select('`created_by`.name AS `created_by`');
		$query->join('LEFT', '#__users AS `created_by` ON `created_by`.id = a.`created_by`');

		// Filter by published state
		$published = $this->getState('filter.state');

		if (is_numeric($published))
		{
			$query->where('a.state = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.state IN (0, 1))');
		}

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				$query->where('( a.create_date LIKE ' . $search . '  OR CONCAT(`#__ed_farmer_2754507`.`citizen_id`, \' \', `#__ed_farmer_2754507`.`name`, \' \', `#__ed_farmer_2754507`.`surname`) LIKE ' . $search . '  OR CONCAT(`#__ed_farm_2754508`.`member_code`, \' \', `#__ed_farm_2754508`.`name`) LIKE ' . $search . '  OR  a.total_female LIKE ' . $search . '  OR  a.total_male LIKE ' . $search . '  OR  a.total_cow LIKE ' . $search . '  OR  a.remark LIKE ' . $search . ' )');
			}
		}


		//Filtering create_date
		$filter_create_date_from = $this->state->get("filter.create_date.from");

		if ($filter_create_date_from !== null && !empty($filter_create_date_from))
		{
			$query->where("a.`create_date` >= '".$db->escape($filter_create_date_from)."'");
		}
		$filter_create_date_to = $this->state->get("filter.create_date.to");

		if ($filter_create_date_to !== null  && !empty($filter_create_date_to))
		{
			$query->where("a.`create_date` <= '".$db->escape($filter_create_date_to)."'");
		}

		//Filtering farmer_id
		$filter_farmer_id = $this->state->get("filter.farmer_id");
		if ($filter_farmer_id !== null && !empty($filter_farmer_id))
		{
			$query->where("a.`farmer_id` = '".$db->escape($filter_farmer_id)."'");
		}

		//Filtering farm_id
		$filter_farm_id = $this->state->get("filter.farm_id");
		if ($filter_farm_id !== null && !empty($filter_farm_id))
		{
			$query->where("a.`farm_id` = '".$db->escape($filter_farm_id)."'");
		}
		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');

		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();

		foreach ($items as $oneItem) {

			if (isset($oneItem->farmer_id))
			{
				$values = explode(',', $oneItem->farmer_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('CONCAT(`#__ed_farmer_2754507`.`citizen_id`, \' \', `#__ed_farmer_2754507`.`name`, \' \', `#__ed_farmer_2754507`.`surname`) AS `fk_value`')
							->from($db->quoteName('#__ed_farmer', '#__ed_farmer_2754507'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->fk_value;
					}
				}

			$oneItem->farmer_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->farmer_id;

			}

			if (isset($oneItem->farm_id))
			{
				$values = explode(',', $oneItem->farm_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('CONCAT(`#__ed_farm_2754508`.`member_code`, \' \', `#__ed_farm_2754508`.`name`) AS `fk_value`')
							->from($db->quoteName('#__ed_farm', '#__ed_farm_2754508'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->fk_value;
					}
				}

			$oneItem->farm_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->farm_id;

			}
		}
		return $items;
	}
}
