<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Edairy records.
 *
 * @since  1.6
 */
class EdairyModelBreedings extends JModelList
{
/**
	* Constructor.
	*
	* @param   array  $config  An optional associative array of configuration settings.
	*
	* @see        JController
	* @since      1.6
	*/
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.`id`',
				'cow_id', 'a.`cow_id`',
				'round', 'a.`round`',
				'no', 'a.`no`',
				'breeding_date', 'a.`breeding_date`',
				'father_id', 'a.`father_id`',
				'lot_no', 'a.`lot_no`',
				'semen_source', 'a.`semen_source`',
				'inspection_date', 'a.`inspection_date`',
				'inspection_result', 'a.`inspection_result`',
				'vet_id', 'a.`vet_id`',
				'create_by', 'a.`create_by`',
				'update_by', 'a.`update_by`',
				'create_datetime', 'a.`create_datetime`',
				'update_datetime', 'a.`update_datetime`',
				'ordering', 'a.`ordering`',
				'state', 'a.`state`',
				'created_by', 'a.`created_by`',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
		$this->setState('filter.state', $published);
		// Filtering cow_id
		$this->setState('filter.cow_id', $app->getUserStateFromRequest($this->context.'.filter.cow_id', 'filter_cow_id', '', 'string'));

		// Filtering breeding_date
		$this->setState('filter.breeding_date.from', $app->getUserStateFromRequest($this->context.'.filter.breeding_date.from', 'filter_from_breeding_date', '', 'string'));
		$this->setState('filter.breeding_date.to', $app->getUserStateFromRequest($this->context.'.filter.breeding_date.to', 'filter_to_breeding_date', '', 'string'));

		// Filtering father_id
		$this->setState('filter.father_id', $app->getUserStateFromRequest($this->context.'.filter.father_id', 'filter_father_id', '', 'string'));

		// Filtering inspection_date
		$this->setState('filter.inspection_date.from', $app->getUserStateFromRequest($this->context.'.filter.inspection_date.from', 'filter_from_inspection_date', '', 'string'));
		$this->setState('filter.inspection_date.to', $app->getUserStateFromRequest($this->context.'.filter.inspection_date.to', 'filter_to_inspection_date', '', 'string'));

		// Filtering vet_id
		$this->setState('filter.vet_id', $app->getUserStateFromRequest($this->context.'.filter.vet_id', 'filter_vet_id', '', 'string'));

		// Filtering update_datetime
		$this->setState('filter.update_datetime.from', $app->getUserStateFromRequest($this->context.'.filter.update_datetime.from', 'filter_from_update_datetime', '', 'string'));
		$this->setState('filter.update_datetime.to', $app->getUserStateFromRequest($this->context.'.filter.update_datetime.to', 'filter_to_update_datetime', '', 'string'));


		// Load the parameters.
		$params = JComponentHelper::getParams('com_edairy');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.id', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select', 'DISTINCT a.*'
			)
		);
		$query->from('`#__ed_cow_breeding` AS a');

		// Join over the users for the checked out user
		$query->select("uc.name AS uEditor");
		$query->join("LEFT", "#__users AS uc ON uc.id=a.checked_out");
		// Join over the foreign key 'cow_id'
		$query->select('CONCAT(`#__ed_cow_2698905`.`cow_id`, \' \', `#__ed_cow_2698905`.`name`) AS cows_fk_value_2698905');
		$query->join('LEFT', '#__ed_cow AS #__ed_cow_2698905 ON #__ed_cow_2698905.`id` = a.`cow_id`');
		// Join over the foreign key 'father_id'
		$query->select('CONCAT(`#__ed_cow_2698909`.`cow_id`, \' \', `#__ed_cow_2698909`.`name`) AS cows_fk_value_2698909');
		$query->join('LEFT', '#__ed_cow AS #__ed_cow_2698909 ON #__ed_cow_2698909.`id` = a.`father_id`');

		// Join over the user field 'vet_id'
		$query->select('`vet_id`.name AS `vet_id`');
		$query->join('LEFT', '#__users AS `vet_id` ON `vet_id`.id = a.`vet_id`');

		// Join over the user field 'create_by'
		$query->select('`create_by`.name AS `create_by`');
		$query->join('LEFT', '#__users AS `create_by` ON `create_by`.id = a.`create_by`');

		// Join over the user field 'update_by'
		$query->select('`update_by`.name AS `update_by`');
		$query->join('LEFT', '#__users AS `update_by` ON `update_by`.id = a.`update_by`');

		// Join over the user field 'created_by'
		$query->select('`created_by`.name AS `created_by`');
		$query->join('LEFT', '#__users AS `created_by` ON `created_by`.id = a.`created_by`');

		// Filter by published state
		$published = $this->getState('filter.state');

		if (is_numeric($published))
		{
			$query->where('a.state = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.state IN (0, 1))');
		}

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				
			}
		}


		//Filtering cow_id
		$filter_cow_id = $this->state->get("filter.cow_id");
		if ($filter_cow_id !== null && !empty($filter_cow_id))
		{
			$query->where("a.`cow_id` = '".$db->escape($filter_cow_id)."'");
		}

		//Filtering breeding_date
		$filter_breeding_date_from = $this->state->get("filter.breeding_date.from");

		if ($filter_breeding_date_from !== null && !empty($filter_breeding_date_from))
		{
			$query->where("a.`breeding_date` >= '".$db->escape($filter_breeding_date_from)."'");
		}
		$filter_breeding_date_to = $this->state->get("filter.breeding_date.to");

		if ($filter_breeding_date_to !== null  && !empty($filter_breeding_date_to))
		{
			$query->where("a.`breeding_date` <= '".$db->escape($filter_breeding_date_to)."'");
		}

		//Filtering father_id
		$filter_father_id = $this->state->get("filter.father_id");
		if ($filter_father_id !== null && !empty($filter_father_id))
		{
			$query->where("a.`father_id` = '".$db->escape($filter_father_id)."'");
		}

		//Filtering inspection_date
		$filter_inspection_date_from = $this->state->get("filter.inspection_date.from");

		if ($filter_inspection_date_from !== null && !empty($filter_inspection_date_from))
		{
			$query->where("a.`inspection_date` >= '".$db->escape($filter_inspection_date_from)."'");
		}
		$filter_inspection_date_to = $this->state->get("filter.inspection_date.to");

		if ($filter_inspection_date_to !== null  && !empty($filter_inspection_date_to))
		{
			$query->where("a.`inspection_date` <= '".$db->escape($filter_inspection_date_to)."'");
		}

		//Filtering vet_id
		$filter_vet_id = $this->state->get("filter.vet_id");
		if ($filter_vet_id !== null && !empty($filter_vet_id))
		{
			$query->where("a.`vet_id` = '".$db->escape($filter_vet_id)."'");
		}

		//Filtering update_datetime
		$filter_update_datetime_from = $this->state->get("filter.update_datetime.from");

		if ($filter_update_datetime_from !== null && !empty($filter_update_datetime_from))
		{
			$query->where("a.`update_datetime` >= '".$db->escape($filter_update_datetime_from)."'");
		}
		$filter_update_datetime_to = $this->state->get("filter.update_datetime.to");

		if ($filter_update_datetime_to !== null  && !empty($filter_update_datetime_to))
		{
			$query->where("a.`update_datetime` <= '".$db->escape($filter_update_datetime_to)."'");
		}
		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');

		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();

		foreach ($items as $oneItem) {

			if (isset($oneItem->cow_id))
			{
				$values = explode(',', $oneItem->cow_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('CONCAT(`#__ed_cow_2698905`.`cow_id`, \' \', `#__ed_cow_2698905`.`name`) AS `fk_value`')
							->from($db->quoteName('#__ed_cow', '#__ed_cow_2698905'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->fk_value;
					}
				}

			$oneItem->cow_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->cow_id;

			}

			if (isset($oneItem->father_id))
			{
				$values = explode(',', $oneItem->father_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('CONCAT(`#__ed_cow_2698909`.`cow_id`, \' \', `#__ed_cow_2698909`.`name`) AS `fk_value`')
							->from($db->quoteName('#__ed_cow', '#__ed_cow_2698909'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->fk_value;
					}
				}

			$oneItem->father_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->father_id;

			}
		}
		return $items;
	}
}
