<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Edairy model.
 *
 * @since  1.6
 */
class EdairyModelCoop extends JModelAdmin
{
	/**
	 * @var      string    The prefix to use with controller messages.
	 * @since    1.6
	 */
	protected $text_prefix = 'COM_EDAIRY';

	/**
	 * @var   	string  	Alias to manage history control
	 * @since   3.2
	 */
	public $typeAlias = 'com_edairy.coop';

	/**
	 * @var null  Item data
	 * @since  1.6
	 */
	protected $item = null;

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param   string  $type    The table type to instantiate
	 * @param   string  $prefix  A prefix for the table class name. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return    JTable    A database object
	 *
	 * @since    1.6
	 */
	public function getTable($type = 'Coop', $prefix = 'EdairyTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      An optional array of data for the form to interogate.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  JForm  A JForm object on success, false on failure
	 *
	 * @since    1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Initialise variables.
		$app = JFactory::getApplication();

		// Get the form.
		$form = $this->loadForm(
			'com_edairy.coop', 'coop',
			array('control' => 'jform',
				'load_data' => $loadData
				)
			);

		if (empty($form))
		{
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return   mixed  The data for the form.
	 *
	 * @since    1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_edairy.edit.coop.data', array());

		if (empty($data))
		{
			if ($this->item === null)
			{
				$this->item = $this->getItem();
			}

			$data = $this->item;
		}

		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param   integer  $pk  The id of the primary key.
	 *
	 * @return  mixed    Object on success, false on failure.
	 *
	 * @since    1.6
	 */
	public function getItem($pk = null)
	{
		if ($item = parent::getItem($pk))
		{
			// Do any procesing on fields here if needed
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
				->select("*")
				->from('#__ed_coop_gmp_certify')
				->where("coop_id='{$item->id}'")
				->order("certify_date asc, id asc");
			$db->setQuery($query);
			$result = $db->loadObjectList();
			$item->gmp = $result;

			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
				->select("*")
				->from('#__ed_coop_send_milk_company')
				->where("coop_id='{$item->id}'")
				->order("id asc");
			$db->setQuery($query);
			$result = $db->loadObjectList();
			$item->send_milk_company = $result;

			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
				->select("*")
				->from('#__ed_coop_has_mou')
				->where("coop_id='{$item->id}'")
				->order("year asc");
			$db->setQuery($query);
			$result = $db->loadObjectList();
			$item->mou = $result;
		}

		return $item;
	}

	/**
	 * Method to duplicate an Coop
	 *
	 * @param   array  &$pks  An array of primary key IDs.
	 *
	 * @return  boolean  True if successful.
	 *
	 * @throws  Exception
	 */
	public function duplicate(&$pks)
	{
		$user = JFactory::getUser();

		// Access checks.
		if (!$user->authorise('core.create', 'com_edairy'))
		{
			throw new Exception(JText::_('JERROR_CORE_CREATE_NOT_PERMITTED'));
		}

		$dispatcher = JEventDispatcher::getInstance();
		$context    = $this->option . '.' . $this->name;

		// Include the plugins for the save events.
		JPluginHelper::importPlugin($this->events_map['save']);

		$table = $this->getTable();

		foreach ($pks as $pk)
		{
			if ($table->load($pk, true))
			{
				// Reset the id to create a new record.
				$table->id = 0;

				if (!$table->check())
				{
					throw new Exception($table->getError());
				}
				
				if (!empty($table->region_id))
				{
					if (is_array($table->region_id))
					{
						$table->region_id = implode(',', $table->region_id);
					}
				}
				else
				{
					$table->region_id = '';
				}

				if (!empty($table->province_id))
				{
					if (is_array($table->province_id))
					{
						$table->province_id = implode(',', $table->province_id);
					}
				}
				else
				{
					$table->province_id = '';
				}

				if (!empty($table->district_id))
				{
					if (is_array($table->district_id))
					{
						$table->district_id = implode(',', $table->district_id);
					}
				}
				else
				{
					$table->district_id = '';
				}

				if (!empty($table->sub_district_id))
				{
					if (is_array($table->sub_district_id))
					{
						$table->sub_district_id = implode(',', $table->sub_district_id);
					}
				}
				else
				{
					$table->sub_district_id = '';
				}


				// Trigger the before save event.
				$result = $dispatcher->trigger($this->event_before_save, array($context, &$table, true));

				if (in_array(false, $result, true) || !$table->store())
				{
					throw new Exception($table->getError());
				}

				// Trigger the after save event.
				$dispatcher->trigger($this->event_after_save, array($context, &$table, true));
			}
			else
			{
				throw new Exception($table->getError());
			}
		}

		// Clean cache
		$this->cleanCache();

		return true;
	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @param   JTable  $table  Table Object
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function prepareTable($table)
	{
		jimport('joomla.filter.output');

		if (empty($table->id))
		{
			// Set ordering to the last item if not set
			if (@$table->ordering === '')
			{
				$db = JFactory::getDbo();
				$db->setQuery('SELECT MAX(ordering) FROM #__ed_coop');
				$max             = $db->loadResult();
				$table->ordering = $max + 1;
			}
		}
	}

	public function getGMP(){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__ed_coop_gmp_certify')
			->where("id='{$_POST[gmp_id]}'");
		$db->setQuery($query);
		$result = $db->loadObject();
		return $result;
	}

	public function saveGMP(){
		$data = new StdClass();
		$data->coop_id = $_POST["gmp_coop_id"];
		$data->certify_no = $_POST["gmp_certify_no"];
		$data->certify_date = formatSQLDate($_POST["gmp_certify_date"]);
		$data->expire_date = formatSQLDate($_POST["gmp_expire_date"]);
		$data->certify_agency = $_POST["gmp_certify_agency"];

		$user = JFactory::getUser();

		if($_POST["gmp_id"]==""){
			$data->create_by = $user->id;
			$data->create_datetime = date("Y-m-d");
		}
		$data->update_by = $user->id;
		$data->update_datetime = date("Y-m-d");
		$data->ordering=0;
		$data->state=1;
		$data->checked_out = 0;
		$data->checked_out_time = "0000-00-00 00:00:00";
		$data->created_by = $user->id;

		if($_POST["gmp_id"]!=""){
			$data->id = $_POST["gmp_id"];
			$updateDataResult = JFactory::getDbo()->updateObject('#__ed_coop_gmp_certify', $data, 'id');

			if($updateDataResult){
				return true;
			}else{
				return false;
			}
		
		}else{
			$addDataResult = JFactory::getDbo()->insertObject('#__ed_coop_gmp_certify', $data, 'id');

			if($addDataResult){
				return true;
			}else{
				return false;
			}
		}
		
	}

	public function deleteGMP(){
		$db = JFactory::getDbo();
		 
		$query = $db->getQuery(true);
		 
		 
		$query->delete($db->quoteName('#__ed_coop_gmp_certify'));
		$query->where("id='{$_POST[gmp_id]}'");
		 
		$db->setQuery($query);
		 
		$result = $db->execute();
		if($result){
			return true;
		}else{
			return false;
		}
	}


	public function getSendMilkCompany(){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__ed_coop_send_milk_company')
			->where("id='{$_POST[smc_id]}'");
		$db->setQuery($query);
		$result = $db->loadObject();
		return $result;
	}


	public function saveSendMilkCompany(){

		$data = new StdClass();
		$data->coop_id = $_POST["smc_coop_id"];
		$data->name = $_POST["smc_name"];
		$data->address = $_POST["smc_address"];

		$user = JFactory::getUser();

		if($_POST["gmp_id"]==""){
			$data->create_by = $user->id;
			$data->create_datetime = date("Y-m-d");
		}
		$data->update_by = $user->id;
		$data->update_datetime = date("Y-m-d");
		$data->ordering=0;
		$data->state=1;
		$data->checked_out = 0;
		$data->checked_out_time = "0000-00-00 00:00:00";
		$data->created_by = $user->id;

		if($_POST["smc_id"]!=""){
			$data->id = $_POST["smc_id"];
			$updateDataResult = JFactory::getDbo()->updateObject('#__ed_coop_send_milk_company', $data, 'id');

			if($updateDataResult){
				return true;
			}else{
				return false;
			}
		
		}else{
			$addDataResult = JFactory::getDbo()->insertObject('#__ed_coop_send_milk_company', $data, 'id');

			if($addDataResult){
				return true;
			}else{
				return false;
			}
		}
		
	}


	public function deleteSendMilkCompany(){
		$db = JFactory::getDbo();
		 
		$query = $db->getQuery(true);
		 
		 
		$query->delete($db->quoteName('#__ed_coop_send_milk_company'));
		$query->where("id='{$_POST[smc_id]}'");
		 
		$db->setQuery($query);
		 
		$result = $db->execute();
		if($result){
			return true;
		}else{
			return false;
		}
	}

	public function getMOU(){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__ed_coop_has_mou')
			->where("id='{$_POST[mou_id]}'");
		$db->setQuery($query);
		$result = $db->loadObject();
		return $result;
	}

	public function saveMOU(){

		$data = new StdClass();
		$data->coop_id = $_POST["mou_coop_id"];
		$data->year = $_POST["mou_year"];
		$data->daily_amount = $_POST["mou_daily_amount"];

		$user = JFactory::getUser();

		if($_POST["gmp_id"]==""){
			$data->create_by = $user->id;
			$data->create_datetime = date("Y-m-d");
		}
		$data->update_by = $user->id;
		$data->update_datetime = date("Y-m-d");
		$data->ordering=0;
		$data->state=1;
		$data->checked_out = 0;
		$data->checked_out_time = "0000-00-00 00:00:00";
		$data->created_by = $user->id;

		if($_POST["mou_id"]!=""){
			$data->id = $_POST["mou_id"];
			$updateDataResult = JFactory::getDbo()->updateObject('#__ed_coop_has_mou', $data, 'id');

			if($updateDataResult){
				return true;
			}else{
				return false;
			}
		
		}else{
			$addDataResult = JFactory::getDbo()->insertObject('#__ed_coop_has_mou', $data, 'id');

			if($addDataResult){
				return true;
			}else{
				return false;
			}
		}
		
	}


	public function deleteMOU(){
		$db = JFactory::getDbo();
		 
		$query = $db->getQuery(true);
		 
		 
		$query->delete($db->quoteName('#__ed_coop_has_mou'));
		$query->where("id='{$_POST[mou_id]}'");
		 
		$db->setQuery($query);
		 
		$result = $db->execute();
		if($result){
			return true;
		}else{
			return false;
		}
	}



}
