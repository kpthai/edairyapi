<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Edairy records.
 *
 * @since  1.6
 */
class EdairyModelFarms extends JModelList
{
/**
	* Constructor.
	*
	* @param   array  $config  An optional associative array of configuration settings.
	*
	* @see        JController
	* @since      1.6
	*/
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.`id`',
				'member_code', 'a.`member_code`',
				'name', 'a.`name`',
				'coop_id', 'a.`coop_id`',
				'address', 'a.`address`',
				'province_id', 'a.`province_id`',
				'region_id', 'a.`region_id`',
				'district_id', 'a.`district_id`',
				'sub_district_id', 'a.`sub_district_id`',
				'post_code', 'a.`post_code`',
				'latitude', 'a.`latitude`',
				'longitude', 'a.`longitude`',
				'farm_project_id', 'a.`farm_project_id`',
				'status', 'a.`status`',
				'is_join_breeding_system', 'a.`is_join_breeding_system`',
				'farm_condition_id', 'a.`farm_condition_id`',
				'environment', 'a.`environment`',
				'performance_improvement_project_id', 'a.`performance_improvement_project_id`',
				'milking_system_id', 'a.`milking_system_id`',
				'waste_disposal_type', 'a.`waste_disposal_type`',
				'member_start_date', 'a.`member_start_date`',
				'breeding_start_date', 'a.`breeding_start_date`',
				'ordering', 'a.`ordering`',
				'state', 'a.`state`',
				'created_by', 'a.`created_by`',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
		$this->setState('filter.state', $published);
		// Filtering coop_id
		$this->setState('filter.coop_id', $app->getUserStateFromRequest($this->context.'.filter.coop_id', 'filter_coop_id', '', 'string'));

		// Filtering province_id
		$this->setState('filter.province_id', $app->getUserStateFromRequest($this->context.'.filter.province_id', 'filter_province_id', '', 'string'));

		// Filtering region_id
		$this->setState('filter.region_id', $app->getUserStateFromRequest($this->context.'.filter.region_id', 'filter_region_id', '', 'string'));

		// Filtering status
		$this->setState('filter.status', $app->getUserStateFromRequest($this->context.'.filter.status', 'filter_status', '', 'string'));


		// Load the parameters.
		$params = JComponentHelper::getParams('com_edairy');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.member_code', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select', 'DISTINCT a.*'
			)
		);
		$query->from('`#__ed_farm` AS a');

		// Join over the users for the checked out user
		$query->select("uc.name AS uEditor");
		$query->join("LEFT", "#__users AS uc ON uc.id=a.checked_out");
		// Join over the foreign key 'coop_id'
		$query->select('CONCAT(`#__ed_coop_2698708`.`coop_code`, \' \', `#__ed_coop_2698708`.`coop_abbr`, \' \', `#__ed_coop_2698708`.`name`) AS coops_fk_value_2698708');
		$query->join('LEFT', '#__ed_coop AS #__ed_coop_2698708 ON #__ed_coop_2698708.`id` = a.`coop_id`');
		// Join over the foreign key 'province_id'
		$query->select('`#__ed_province_2698712`.`name` AS provinces_fk_value_2698712');
		$query->join('LEFT', '#__ed_province AS #__ed_province_2698712 ON #__ed_province_2698712.`id` = a.`province_id`');
		// Join over the foreign key 'region_id'
		$query->select('`#__ed_region_2698707`.`name` AS regions_fk_value_2698707');
		$query->join('LEFT', '#__ed_region AS #__ed_region_2698707 ON #__ed_region_2698707.`id` = a.`region_id`');
		// Join over the foreign key 'district_id'
		$query->select('`#__ed_district_2698713`.`name` AS districts_fk_value_2698713');
		$query->join('LEFT', '#__ed_district AS #__ed_district_2698713 ON #__ed_district_2698713.`id` = a.`district_id`');
		// Join over the foreign key 'sub_district_id'
		$query->select('`#__ed_sub_district_2698714`.`name` AS sub_districts_fk_value_2698714');
		$query->join('LEFT', '#__ed_sub_district AS #__ed_sub_district_2698714 ON #__ed_sub_district_2698714.`id` = a.`sub_district_id`');
		// Join over the foreign key 'farm_project_id'
		$query->select('`#__ed_cow_project_2698718`.`name` AS cow_projects_fk_value_2698718');
		$query->join('LEFT', '#__ed_cow_project AS #__ed_cow_project_2698718 ON #__ed_cow_project_2698718.`id` = a.`farm_project_id`');
		// Join over the foreign key 'performance_improvement_project_id'
		$query->select('`#__ed_performance_improvement_project_2698723`.`name` AS performance_improvement_projects_fk_value_2698723');
		$query->join('LEFT', '#__ed_performance_improvement_project AS #__ed_performance_improvement_project_2698723 ON #__ed_performance_improvement_project_2698723.`id` = a.`performance_improvement_project_id`');
		// Join over the foreign key 'milking_system_id'
		$query->select('`#__ed_milking_system_2698724`.`name` AS milking_systems_fk_value_2698724');
		$query->join('LEFT', '#__ed_milking_system AS #__ed_milking_system_2698724 ON #__ed_milking_system_2698724.`id` = a.`milking_system_id`');

		// Join over the user field 'created_by'
		$query->select('`created_by`.name AS `created_by`');
		$query->join('LEFT', '#__users AS `created_by` ON `created_by`.id = a.`created_by`');

		// Filter by published state
		$published = $this->getState('filter.state');

		if (is_numeric($published))
		{
			$query->where('a.state = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.state IN (0, 1))');
		}

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				$query->where('( a.member_code LIKE ' . $search . '  OR  a.name LIKE ' . $search . ' )');
			}
		}


		//Filtering coop_id
		$filter_coop_id = $this->state->get("filter.coop_id");
		if ($filter_coop_id !== null && !empty($filter_coop_id))
		{
			$query->where("a.`coop_id` = '".$db->escape($filter_coop_id)."'");
		}

		//Filtering province_id
		$filter_province_id = $this->state->get("filter.province_id");
		if ($filter_province_id !== null && !empty($filter_province_id))
		{
			$query->where("a.`province_id` = '".$db->escape($filter_province_id)."'");
		}

		//Filtering region_id
		$filter_region_id = $this->state->get("filter.region_id");
		if ($filter_region_id !== null && !empty($filter_region_id))
		{
			$query->where("a.`region_id` = '".$db->escape($filter_region_id)."'");
		}

		//Filtering status
		$filter_status = $this->state->get("filter.status");
		if ($filter_status !== null && !empty($filter_status))
		{
			$query->where("a.`status` = '".$db->escape($filter_status)."'");
		}


		//Custom Filter
		if ($_REQUEST["search_region"]!="")
		{
			$query->where("a.`region_id` = '".$db->escape($_REQUEST["search_region"])."'");
		}

		if ($_REQUEST["search_province"]!="")
		{
			$query->where("a.`province_id` = '".$db->escape($_REQUEST["search_province"])."'");
		}

		if ($_REQUEST["search_farm_name"]!="")
		{
			$query->where("a.`name` LIKE '%".$db->escape($_REQUEST["search_farm_name"])."%'");
		}

		if ($_REQUEST["search_member_code"]!="")
		{
			$query->where("a.`member_code` LIKE '%".$db->escape($_REQUEST["search_member_code"])."%'");
		}


		if ($_REQUEST["search_farmer_name"]!="")
		{
			$query->where("a.id in (select farm_id from #__ed_farmer f where f.name like '%{$_REQUEST[search_farmer_name]}%' )");
		}

		if ($_REQUEST["search_farmer_surname"]!="")
		{
			$query->where("a.id in (select farm_id from #__ed_farmer f where f.surname like '%{$_REQUEST[search_farmer_surname]}%' )");
		}

		

		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');

		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();

		foreach ($items as $oneItem) {

			if (isset($oneItem->coop_id))
			{
				$values = explode(',', $oneItem->coop_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('CONCAT(`#__ed_coop_2698708`.`coop_code`, \' \', `#__ed_coop_2698708`.`coop_abbr`, \' \', `#__ed_coop_2698708`.`name`) AS `fk_value`')
							->from($db->quoteName('#__ed_coop', '#__ed_coop_2698708'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->fk_value;
					}
				}

			$oneItem->coop_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->coop_id;

			}

			if (isset($oneItem->province_id))
			{
				$values = explode(',', $oneItem->province_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('`#__ed_province_2698712`.`name`')
							->from($db->quoteName('#__ed_province', '#__ed_province_2698712'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->name;
					}
				}

			$oneItem->province_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->province_id;

			}

			if (isset($oneItem->region_id))
			{
				$values = explode(',', $oneItem->region_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('`#__ed_region_2698707`.`name`')
							->from($db->quoteName('#__ed_region', '#__ed_region_2698707'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->name;
					}
				}

			$oneItem->region_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->region_id;

			}

			if (isset($oneItem->district_id))
			{
				$values = explode(',', $oneItem->district_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('`#__ed_district_2698713`.`name`')
							->from($db->quoteName('#__ed_district', '#__ed_district_2698713'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->name;
					}
				}

			$oneItem->district_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->district_id;

			}

			if (isset($oneItem->sub_district_id))
			{
				$values = explode(',', $oneItem->sub_district_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('`#__ed_sub_district_2698714`.`name`')
							->from($db->quoteName('#__ed_sub_district', '#__ed_sub_district_2698714'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->name;
					}
				}

			$oneItem->sub_district_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->sub_district_id;

			}

			if (isset($oneItem->farm_project_id))
			{
				$values = explode(',', $oneItem->farm_project_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('`#__ed_cow_project_2698718`.`name`')
							->from($db->quoteName('#__ed_cow_project', '#__ed_cow_project_2698718'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->name;
					}
				}

			$oneItem->farm_project_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->farm_project_id;

			}
					$oneItem->status = empty($oneItem->status) ? '' : JText::_('COM_EDAIRY_FARMS_STATUS_OPTION_' . strtoupper($oneItem->status));
					$oneItem->is_join_breeding_system = empty($oneItem->is_join_breeding_system) ? '' : JText::_('COM_EDAIRY_FARMS_IS_JOIN_BREEDING_SYSTEM_OPTION_' . strtoupper($oneItem->is_join_breeding_system));
					$oneItem->farm_condition_id = JText::_('COM_EDAIRY_FARMS_FARM_CONDITION_ID_OPTION_' . strtoupper($oneItem->farm_condition_id));

			if (isset($oneItem->performance_improvement_project_id))
			{
				$values = explode(',', $oneItem->performance_improvement_project_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('`#__ed_performance_improvement_project_2698723`.`name`')
							->from($db->quoteName('#__ed_performance_improvement_project', '#__ed_performance_improvement_project_2698723'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->name;
					}
				}

			$oneItem->performance_improvement_project_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->performance_improvement_project_id;

			}


			if (isset($oneItem->id))
			{
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('*')
							->from("#__ed_farmer")
							->where("farm_id='{$oneItem->id}'")
							->limit(1);
					$db->setQuery($query);
					$results = $db->loadObject();
					$oneItem->farmer_name = $results->citizen_id . " : " . $results->name . " " . $results->surname;

			}

			if (isset($oneItem->milking_system_id))
			{
				$values = explode(',', $oneItem->milking_system_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('`#__ed_milking_system_2698724`.`name`')
							->from($db->quoteName('#__ed_milking_system', '#__ed_milking_system_2698724'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->name;
					}
				}

			$oneItem->milking_system_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->milking_system_id;

			}
					$oneItem->waste_disposal_type = empty($oneItem->waste_disposal_type) ? '' : JText::_('COM_EDAIRY_FARMS_WASTE_DISPOSAL_TYPE_OPTION_' . strtoupper($oneItem->waste_disposal_type));
		}
		return $items;
	}


	public function getMasterData(){

		$masterData = new StdClass();
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__ed_region')
			->where("state=1");
		$db->setQuery($query);
		$masterData->mas_region = $db->loadObjectList();


		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__ed_province')
			->where("state=1");
		$db->setQuery($query);
		$masterData->mas_province = $db->loadObjectList();

		return $masterData;

	}
}
