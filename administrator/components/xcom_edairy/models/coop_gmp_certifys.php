<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Edairy records.
 *
 * @since  1.6
 */
class EdairyModelCoop_gmp_certifys extends JModelList
{
/**
	* Constructor.
	*
	* @param   array  $config  An optional associative array of configuration settings.
	*
	* @see        JController
	* @since      1.6
	*/
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.`id`',
				'coop_id', 'a.`coop_id`',
				'certify_no', 'a.`certify_no`',
				'certify_date', 'a.`certify_date`',
				'expire_date', 'a.`expire_date`',
				'certify_agency', 'a.`certify_agency`',
				'create_by', 'a.`create_by`',
				'update_by', 'a.`update_by`',
				'create_datetime', 'a.`create_datetime`',
				'update_datetime', 'a.`update_datetime`',
				'ordering', 'a.`ordering`',
				'state', 'a.`state`',
				'created_by', 'a.`created_by`',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
		$this->setState('filter.state', $published);
		// Filtering coop_id
		$this->setState('filter.coop_id', $app->getUserStateFromRequest($this->context.'.filter.coop_id', 'filter_coop_id', '', 'string'));

		// Filtering certify_date
		$this->setState('filter.certify_date.from', $app->getUserStateFromRequest($this->context.'.filter.certify_date.from', 'filter_from_certify_date', '', 'string'));
		$this->setState('filter.certify_date.to', $app->getUserStateFromRequest($this->context.'.filter.certify_date.to', 'filter_to_certify_date', '', 'string'));

		// Filtering expire_date
		$this->setState('filter.expire_date.from', $app->getUserStateFromRequest($this->context.'.filter.expire_date.from', 'filter_from_expire_date', '', 'string'));
		$this->setState('filter.expire_date.to', $app->getUserStateFromRequest($this->context.'.filter.expire_date.to', 'filter_to_expire_date', '', 'string'));

		// Filtering update_by
		$this->setState('filter.update_by', $app->getUserStateFromRequest($this->context.'.filter.update_by', 'filter_update_by', '', 'string'));


		// Load the parameters.
		$params = JComponentHelper::getParams('com_edairy');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.coop_id', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select', 'DISTINCT a.*'
			)
		);
		$query->from('`#__ed_coop_gmp_certify` AS a');

		// Join over the users for the checked out user
		$query->select("uc.name AS uEditor");
		$query->join("LEFT", "#__users AS uc ON uc.id=a.checked_out");
		// Join over the foreign key 'coop_id'
		$query->select('CONCAT(`#__ed_coop_2698638`.`coop_code`, \' \', `#__ed_coop_2698638`.`coop_abbr`, \' \', `#__ed_coop_2698638`.`name`) AS coops_fk_value_2698638');
		$query->join('LEFT', '#__ed_coop AS #__ed_coop_2698638 ON #__ed_coop_2698638.`id` = a.`coop_id`');

		// Join over the user field 'create_by'
		$query->select('`create_by`.name AS `create_by`');
		$query->join('LEFT', '#__users AS `create_by` ON `create_by`.id = a.`create_by`');

		// Join over the user field 'update_by'
		$query->select('`update_by`.name AS `update_by`');
		$query->join('LEFT', '#__users AS `update_by` ON `update_by`.id = a.`update_by`');

		// Join over the user field 'created_by'
		$query->select('`created_by`.name AS `created_by`');
		$query->join('LEFT', '#__users AS `created_by` ON `created_by`.id = a.`created_by`');

		// Filter by published state
		$published = $this->getState('filter.state');

		if (is_numeric($published))
		{
			$query->where('a.state = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.state IN (0, 1))');
		}

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				$query->where('(CONCAT(`#__ed_coop_2698638`.`coop_code`, \' \', `#__ed_coop_2698638`.`coop_abbr`, \' \', `#__ed_coop_2698638`.`name`) LIKE ' . $search . '  OR  a.certify_no LIKE ' . $search . '  OR  a.certify_agency LIKE ' . $search . ' )');
			}
		}


		//Filtering coop_id
		$filter_coop_id = $this->state->get("filter.coop_id");
		if ($filter_coop_id !== null && !empty($filter_coop_id))
		{
			$query->where("a.`coop_id` = '".$db->escape($filter_coop_id)."'");
		}

		//Filtering certify_date
		$filter_certify_date_from = $this->state->get("filter.certify_date.from");

		if ($filter_certify_date_from !== null && !empty($filter_certify_date_from))
		{
			$query->where("a.`certify_date` >= '".$db->escape($filter_certify_date_from)."'");
		}
		$filter_certify_date_to = $this->state->get("filter.certify_date.to");

		if ($filter_certify_date_to !== null  && !empty($filter_certify_date_to))
		{
			$query->where("a.`certify_date` <= '".$db->escape($filter_certify_date_to)."'");
		}

		//Filtering expire_date
		$filter_expire_date_from = $this->state->get("filter.expire_date.from");

		if ($filter_expire_date_from !== null && !empty($filter_expire_date_from))
		{
			$query->where("a.`expire_date` >= '".$db->escape($filter_expire_date_from)."'");
		}
		$filter_expire_date_to = $this->state->get("filter.expire_date.to");

		if ($filter_expire_date_to !== null  && !empty($filter_expire_date_to))
		{
			$query->where("a.`expire_date` <= '".$db->escape($filter_expire_date_to)."'");
		}

		//Filtering update_by
		$filter_update_by = $this->state->get("filter.update_by");
		if ($filter_update_by !== null && !empty($filter_update_by))
		{
			$query->where("a.`update_by` = '".$db->escape($filter_update_by)."'");
		}
		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');

		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();

		foreach ($items as $oneItem) {

			if (isset($oneItem->coop_id))
			{
				$values = explode(',', $oneItem->coop_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('CONCAT(`#__ed_coop_2698638`.`coop_code`, \' \', `#__ed_coop_2698638`.`coop_abbr`, \' \', `#__ed_coop_2698638`.`name`) AS `fk_value`')
							->from($db->quoteName('#__ed_coop', '#__ed_coop_2698638'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->fk_value;
					}
				}

			$oneItem->coop_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->coop_id;

			}
		}
		return $items;
	}
}
