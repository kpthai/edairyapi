<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Edairy records.
 *
 * @since  1.6
 */
class EdairyModelCows extends JModelList
{
/**
	* Constructor.
	*
	* @param   array  $config  An optional associative array of configuration settings.
	*
	* @see        JController
	* @since      1.6
	*/
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.`id`',
				'farm_id', 'a.`farm_id`',
				'cow_id', 'a.`cow_id`',
				'name', 'a.`name`',
				'dld_id', 'a.`dld_id`',
				'ear_id', 'a.`ear_id`',
				'birthdate', 'a.`birthdate`',
				'gender', 'a.`gender`',
				'move_farm_date', 'a.`move_farm_date`',
				'move_type', 'a.`move_type`',
				'move_from_farm_id', 'a.`move_from_farm_id`',
				'breed', 'a.`breed`',
				'father_cow_id', 'a.`father_cow_id`',
				'mother_cow_id', 'a.`mother_cow_id`',
				'create_by', 'a.`create_by`',
				'update_by', 'a.`update_by`',
				'create_datetime', 'a.`create_datetime`',
				'update_datetime', 'a.`update_datetime`',
				'ordering', 'a.`ordering`',
				'state', 'a.`state`',
				'created_by', 'a.`created_by`',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
		$this->setState('filter.state', $published);
		// Filtering farm_id
		$this->setState('filter.farm_id', $app->getUserStateFromRequest($this->context.'.filter.farm_id', 'filter_farm_id', '', 'string'));

		// Filtering birthdate
		$this->setState('filter.birthdate.from', $app->getUserStateFromRequest($this->context.'.filter.birthdate.from', 'filter_from_birthdate', '', 'string'));
		$this->setState('filter.birthdate.to', $app->getUserStateFromRequest($this->context.'.filter.birthdate.to', 'filter_to_birthdate', '', 'string'));

		// Filtering gender
		$this->setState('filter.gender', $app->getUserStateFromRequest($this->context.'.filter.gender', 'filter_gender', '', 'string'));

		// Filtering update_by
		$this->setState('filter.update_by', $app->getUserStateFromRequest($this->context.'.filter.update_by', 'filter_update_by', '', 'string'));

		// Filtering update_datetime
		$this->setState('filter.update_datetime.from', $app->getUserStateFromRequest($this->context.'.filter.update_datetime.from', 'filter_from_update_datetime', '', 'string'));
		$this->setState('filter.update_datetime.to', $app->getUserStateFromRequest($this->context.'.filter.update_datetime.to', 'filter_to_update_datetime', '', 'string'));


		// Load the parameters.
		$params = JComponentHelper::getParams('com_edairy');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.farm_id', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select', 'DISTINCT a.*'
			)
		);
		$query->from('`#__ed_cow` AS a');

		// Join over the users for the checked out user
		$query->select("uc.name AS uEditor");
		$query->join("LEFT", "#__users AS uc ON uc.id=a.checked_out");
		// Join over the foreign key 'farm_id'
		$query->select('CONCAT(`#__ed_farm_2698844`.`member_code`, \' \', `#__ed_farm_2698844`.`name`) AS farms_fk_value_2698844');
		$query->join('LEFT', '#__ed_farm AS #__ed_farm_2698844 ON #__ed_farm_2698844.`id` = a.`farm_id`');
		// Join over the foreign key 'move_from_farm_id'
		$query->select('CONCAT(`#__ed_farm_2698853`.`member_code`, \' \', `#__ed_farm_2698853`.`name`) AS farms_fk_value_2698853');
		$query->join('LEFT', '#__ed_farm AS #__ed_farm_2698853 ON #__ed_farm_2698853.`id` = a.`move_from_farm_id`');
		// Join over the foreign key 'father_cow_id'
		$query->select('CONCAT(`#__ed_cow_2698855`.`cow_id`, \' \', `#__ed_cow_2698855`.`name`) AS cows_fk_value_2698855');
		$query->join('LEFT', '#__ed_cow AS #__ed_cow_2698855 ON #__ed_cow_2698855.`id` = a.`father_cow_id`');
		// Join over the foreign key 'mother_cow_id'
		$query->select('CONCAT(`#__ed_cow_2698856`.`cow_id`, \' \', `#__ed_cow_2698856`.`name`) AS cows_fk_value_2698856');
		$query->join('LEFT', '#__ed_cow AS #__ed_cow_2698856 ON #__ed_cow_2698856.`id` = a.`mother_cow_id`');

		// Join over the user field 'create_by'
		$query->select('`create_by`.name AS `create_by`');
		$query->join('LEFT', '#__users AS `create_by` ON `create_by`.id = a.`create_by`');

		// Join over the user field 'update_by'
		$query->select('`update_by`.name AS `update_by`');
		$query->join('LEFT', '#__users AS `update_by` ON `update_by`.id = a.`update_by`');

		// Join over the user field 'created_by'
		$query->select('`created_by`.name AS `created_by`');
		$query->join('LEFT', '#__users AS `created_by` ON `created_by`.id = a.`created_by`');

		// Filter by published state
		$published = $this->getState('filter.state');

		if (is_numeric($published))
		{
			$query->where('a.state = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.state IN (0, 1))');
		}

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				$query->where('(CONCAT(`#__ed_farm_2698844`.`member_code`, \' \', `#__ed_farm_2698844`.`name`) LIKE ' . $search . '  OR  a.cow_id LIKE ' . $search . '  OR  a.name LIKE ' . $search . '  OR  a.dld_id LIKE ' . $search . '  OR  a.ear_id LIKE ' . $search . '  OR  a.breed LIKE ' . $search . ' )');
			}
		}


		//Filtering farm_id
		$filter_farm_id = $this->state->get("filter.farm_id");
		if ($filter_farm_id !== null && !empty($filter_farm_id))
		{
			$query->where("a.`farm_id` = '".$db->escape($filter_farm_id)."'");
		}

		//Filtering birthdate
		$filter_birthdate_from = $this->state->get("filter.birthdate.from");

		if ($filter_birthdate_from !== null && !empty($filter_birthdate_from))
		{
			$query->where("a.`birthdate` >= '".$db->escape($filter_birthdate_from)."'");
		}
		$filter_birthdate_to = $this->state->get("filter.birthdate.to");

		if ($filter_birthdate_to !== null  && !empty($filter_birthdate_to))
		{
			$query->where("a.`birthdate` <= '".$db->escape($filter_birthdate_to)."'");
		}

		//Filtering gender
		$filter_gender = $this->state->get("filter.gender");
		if ($filter_gender !== null && !empty($filter_gender))
		{
			$query->where("a.`gender` = '".$db->escape($filter_gender)."'");
		}

		//Filtering update_by
		$filter_update_by = $this->state->get("filter.update_by");
		if ($filter_update_by !== null && !empty($filter_update_by))
		{
			$query->where("a.`update_by` = '".$db->escape($filter_update_by)."'");
		}

		//Filtering update_datetime
		$filter_update_datetime_from = $this->state->get("filter.update_datetime.from");

		if ($filter_update_datetime_from !== null && !empty($filter_update_datetime_from))
		{
			$query->where("a.`update_datetime` >= '".$db->escape($filter_update_datetime_from)."'");
		}
		$filter_update_datetime_to = $this->state->get("filter.update_datetime.to");

		if ($filter_update_datetime_to !== null  && !empty($filter_update_datetime_to))
		{
			$query->where("a.`update_datetime` <= '".$db->escape($filter_update_datetime_to)."'");
		}



		//Custom Filter
		if ($_REQUEST["search_region"]!="")
		{
			$query->where("a.`region_id` = '".$db->escape($_REQUEST["search_region"])."'");
		}

		if ($_REQUEST["search_province"]!="")
		{
			$query->where("a.`province_id` = '".$db->escape($_REQUEST["search_province"])."'");
		}

		if ($_REQUEST["search_farm_name"]!="")
		{
			$query->where("`#__ed_farm_2698844`.`name` like '%{$_REQUEST[search_farm_name]}%'");
		}

		if ($_REQUEST["search_member_code"]!="")
		{
			$query->where("`#__ed_farm_2698844`.`member_code` like '%{$_REQUEST[search_member_code]}%'");
		}

		if ($_REQUEST["search_cow_id"]!="")
		{
			$query->where("a.`cow_id` = '".$db->escape($_REQUEST["search_cow_id"])."'");
		}

		if ($_REQUEST["search_cow_name"]!="")
		{
			$query->where("a.`name` = '".$db->escape($_REQUEST["search_cow_name"])."'");
		}

		if ($_REQUEST["search_farmer_name"]!="")
		{
			$query->where("`#__ed_farm_2698844`.id in (select farm_id from #__ed_farmer f where f.name like '%{$_REQUEST[search_farmer_name]}%' )");
		}

		if ($_REQUEST["search_farmer_surname"]!="")
		{
			$query->where("`#__ed_farm_2698844`.id in (select farm_id from #__ed_farmer f where f.surname like '%{$_REQUEST[search_farmer_surname]}%' )");
		}


		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');

		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();

		foreach ($items as $oneItem) {

			if (isset($oneItem->farm_id))
			{
				$values = explode(',', $oneItem->farm_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('CONCAT(`#__ed_farm_2698844`.`member_code`, \' \', `#__ed_farm_2698844`.`name`) AS `fk_value`')
							->from($db->quoteName('#__ed_farm', '#__ed_farm_2698844'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->fk_value;
					}
				}

			$oneItem->farm_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->farm_id;

			}
					$oneItem->gender = empty($oneItem->gender) ? '' : JText::_('COM_EDAIRY_COWS_GENDER_OPTION_' . strtoupper($oneItem->gender));
					$oneItem->move_type = empty($oneItem->move_type) ? '' : JText::_('COM_EDAIRY_COWS_MOVE_TYPE_OPTION_' . strtoupper($oneItem->move_type));

			if (isset($oneItem->move_from_farm_id))
			{
				$values = explode(',', $oneItem->move_from_farm_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('CONCAT(`#__ed_farm_2698853`.`member_code`, \' \', `#__ed_farm_2698853`.`name`) AS `fk_value`')
							->from($db->quoteName('#__ed_farm', '#__ed_farm_2698853'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->fk_value;
					}
				}

			$oneItem->move_from_farm_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->move_from_farm_id;

			}

			if (isset($oneItem->father_cow_id))
			{
				$values = explode(',', $oneItem->father_cow_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('CONCAT(`#__ed_cow_2698855`.`cow_id`, \' \', `#__ed_cow_2698855`.`name`) AS `fk_value`')
							->from($db->quoteName('#__ed_cow', '#__ed_cow_2698855'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->fk_value;
					}
				}

			$oneItem->father_cow_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->father_cow_id;

			}

			if (isset($oneItem->mother_cow_id))
			{
				$values = explode(',', $oneItem->mother_cow_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('CONCAT(`#__ed_cow_2698856`.`cow_id`, \' \', `#__ed_cow_2698856`.`name`) AS `fk_value`')
							->from($db->quoteName('#__ed_cow', '#__ed_cow_2698856'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->fk_value;
					}
				}

			$oneItem->mother_cow_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->mother_cow_id;

			}
		}
		return $items;
	}

	public function getMasterData(){

		$masterData = new StdClass();
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__ed_region')
			->where("state=1");
		$db->setQuery($query);
		$masterData->mas_region = $db->loadObjectList();


		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__ed_province')
			->where("state=1");
		$db->setQuery($query);
		$masterData->mas_province = $db->loadObjectList();

		return $masterData;

	}
}
