<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Farm controller class.
 *
 * @since  1.6
 */
class EdairyControllerFarm extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'farms';
		parent::__construct();
	}


	public function getGAP(){
		 $model = $this->getModel('Farm', 'EdairyModel');

		 echo json_encode($model->getGAP());
		 jexit();
	}

	public function saveGAP(){
		 $model = $this->getModel('Farm', 'EdairyModel');

        if($model->saveGAP()){
        	$this->setMessage(JText::_('บันทึกมาตรฐาน GAP ได้สำเร็จ'), "success");
		 	$this->setRedirect(JRoute::_("index.php?option=com_edairy&view=farm&layout=edit&id={$_POST[gap_farm_id]}&default_tab=gap", false));
		}else{
			$this->setMessage(JText::_('ไม่สามารถบันทึกมาตรฐาน GAP ได้'), "error");
			$this->setRedirect(JRoute::_("index.php?option=com_edairy&view=farm&layout=edit&id={$_POST[gap_farm_id]}&default_tab=gap", false));
		}
	}


	public function deleteGAP(){
		 $model = $this->getModel('Farm', 'EdairyModel');

        if($model->deleteGAP()){
        	$data = array(
		        'delete_result' => true
		    );

		    echo json_encode( $data );
		    jexit();
		}else{
			$data = array(
		        'delete_result' => false
		    );

		    echo json_encode( $data );
		    jexit();
		}
	}


	public function getOwner(){
		 $model = $this->getModel('Farm', 'EdairyModel');

		 echo json_encode($model->getOwner());
		 jexit();
	}

	public function saveOwner(){
		 $model = $this->getModel('Farm', 'EdairyModel');

        if($model->saveOwner()){
        	$this->setMessage(JText::_('บันทึกประวัติเจ้าของฟาร์มได้สำเร็จ'), "success");
		 	$this->setRedirect(JRoute::_("index.php?option=com_edairy&view=farm&layout=edit&id={$_POST[owner_farm_id]}&default_tab=farmer", false));
		}else{
			$this->setMessage(JText::_('ไม่สามารถบันทึกมประวัติเจ้าของฟาร์มได้'), "error");
			$this->setRedirect(JRoute::_("index.php?option=com_edairy&view=farm&layout=edit&id={$_POST[owner_farm_id]}&default_tab=farmer", false));
		}
	}


	public function deleteOwner(){
		 $model = $this->getModel('Farm', 'EdairyModel');

        if($model->deleteOwner()){
        	$data = array(
		        'delete_result' => true
		    );

		    echo json_encode( $data );
		    jexit();
		}else{
			$data = array(
		        'delete_result' => false
		    );

		    echo json_encode( $data );
		    jexit();
		}
	}

	public function getTraining(){
		 $model = $this->getModel('Farm', 'EdairyModel');

		 echo json_encode($model->getTraining());
		 jexit();
	}

	public function saveTraining(){
		 $model = $this->getModel('Farm', 'EdairyModel');

        if($model->saveTraining()){
        	$this->setMessage(JText::_('บันทึกประวัติการฝึกอบรมได้สำเร็จ'), "success");
		 	$this->setRedirect(JRoute::_("index.php?option=com_edairy&view=farm&layout=edit&id={$_POST[training_farm_id]}&default_tab=farmer", false));
		}else{
			$this->setMessage(JText::_('ไม่สามารถบันทึกประวัติการฝึกอบรมได้'), "error");
			$this->setRedirect(JRoute::_("index.php?option=com_edairy&view=farm&layout=edit&id={$_POST[training_farm_id]}&default_tab=farmer", false));
		}
	}


	public function deleteTraining(){
		 $model = $this->getModel('Farm', 'EdairyModel');

        if($model->deleteTraining()){
        	$data = array(
		        'delete_result' => true
		    );

		    echo json_encode( $data );
		    jexit();
		}else{
			$data = array(
		        'delete_result' => false
		    );

		    echo json_encode( $data );
		    jexit();
		}
	}


	public function saveFarmer(){
		 $model = $this->getModel('Farm', 'EdairyModel');

        if($model->saveTraining()){
        	$this->setMessage(JText::_('บันทึกเกษตรกรได้สำเร็จ'), "success");
		 	$this->setRedirect(JRoute::_("index.php?option=com_edairy&view=farm&layout=edit&id={$_POST[farmer_farm_id]}&default_tab=farmer", false));
		}else{
			$this->setMessage(JText::_('ไม่สามารถบันทึเกษตรกรได้'), "error");
			$this->setRedirect(JRoute::_("index.php?option=com_edairy&view=farm&layout=edit&id={$_POST[farmer_farm_id]}&default_tab=farmer", false));
		}
	}

	


	
}
