CREATE TABLE IF NOT EXISTS `#__ed_region` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` VARCHAR(100)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_province` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` VARCHAR(100)  NOT NULL ,
`region_id` INT NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_district` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` VARCHAR(100)  NOT NULL ,
`province_id` INT NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_sub_district` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` VARCHAR(100)  NOT NULL ,
`district_id` INT NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_coop` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`coop_code` VARCHAR(100)  NOT NULL ,
`coop_abbr` VARCHAR(100)  NOT NULL ,
`name` VARCHAR(100)  NOT NULL ,
`address` TEXT NOT NULL ,
`region_id` INT NOT NULL ,
`province_id` INT NOT NULL ,
`district_id` INT NOT NULL ,
`sub_district_id` INT NOT NULL ,
`post_code` VARCHAR(5)  NOT NULL ,
`latitude` DECIMAL(12,8)  NOT NULL ,
`longitude` DECIMAL(12,8)  NOT NULL ,
`create_by` INT(11)  NOT NULL ,
`update_by` INT(11)  NOT NULL ,
`create_datetime` DATE NOT NULL ,
`update_datetime` DATE NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_coop_gmp_certify` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`coop_id` INT NOT NULL ,
`certify_no` VARCHAR(100)  NOT NULL ,
`certify_date` DATE NOT NULL ,
`expire_date` DATE NOT NULL ,
`certify_agency` VARCHAR(100)  NOT NULL ,
`create_by` INT(11)  NOT NULL ,
`update_by` INT(11)  NOT NULL ,
`create_datetime` DATE NOT NULL ,
`update_datetime` DATE NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_coop_send_milk_company` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`coop_id` INT NOT NULL ,
`name` VARCHAR(100)  NOT NULL ,
`address` VARCHAR(300)  NOT NULL ,
`create_by` INT(11)  NOT NULL ,
`update_by` INT(11)  NOT NULL ,
`create_datetime` DATE NOT NULL ,
`update_datetime` DATE NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_coop_has_mou` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`coop_id` INT NOT NULL ,
`year` INT(4)  NOT NULL ,
`daily_amount` DECIMAL(10,2)  NOT NULL ,
`create_by` INT(11)  NOT NULL ,
`update_by` INT(11)  NOT NULL ,
`create_datetime` DATE NOT NULL ,
`update_datetime` DATE NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_cow_project` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` VARCHAR(100)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_farm_condition` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` VARCHAR(100)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_performance_improvement_project` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` VARCHAR(100)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_milking_system` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` VARCHAR(100)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_farm` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`member_code` VARCHAR(100)  NOT NULL ,
`name` VARCHAR(100)  NOT NULL ,
`coop_id` INT NOT NULL ,
`address` VARCHAR(300)  NOT NULL ,
`province_id` INT NOT NULL ,
`region_id` INT NOT NULL ,
`district_id` INT NOT NULL ,
`sub_district_id` INT NOT NULL ,
`post_code` INT(5)  NOT NULL ,
`latitude` DECIMAL(12,8)  NOT NULL ,
`longitude` DECIMAL(12,8)  NOT NULL ,
`farm_project_id` INT NOT NULL ,
`status` VARCHAR(255)  NOT NULL ,
`is_join_breeding_system` VARCHAR(255)  NOT NULL ,
`farm_condition_id` VARCHAR(255)  NOT NULL ,
`environment` VARCHAR(300)  NOT NULL ,
`performance_improvement_project_id` INT NOT NULL ,
`milking_system_id` INT NOT NULL ,
`waste_disposal_type` VARCHAR(255)  NOT NULL ,
`member_start_date` DATE NOT NULL ,
`breeding_start_date` DATE NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_farm_gap_certify` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`farm_id` INT NOT NULL ,
`certify_no` VARCHAR(100)  NOT NULL ,
`certify_date` DATE NOT NULL ,
`expire_date` DATE NOT NULL ,
`certify_agency` VARCHAR(100)  NOT NULL ,
`create_by` INT(11)  NOT NULL ,
`update_by` INT(11)  NOT NULL ,
`create_datetime` DATE NOT NULL ,
`update_datetime` DATE NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_farm_grade_certify` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`farm_id` INT NOT NULL ,
`grade` VARCHAR(50)  NOT NULL ,
`certify_date` DATE NOT NULL ,
`book_no` VARCHAR(100)  NOT NULL ,
`create_by` INT(11)  NOT NULL ,
`update_by` INT(11)  NOT NULL ,
`create_datetime` DATE NOT NULL ,
`update_datetime` DATE NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_farm_grade_announcement` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`year` INT(4)  NOT NULL ,
`announce_date` DATE NOT NULL ,
`detail` TEXT NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_farmer` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`citizen_id` VARCHAR(13)  NOT NULL ,
`image_url` TEXT NOT NULL ,
`title_id` VARCHAR(255)  NOT NULL ,
`name` VARCHAR(100)  NOT NULL ,
`surname` VARCHAR(100)  NOT NULL ,
`birthdate` DATE NOT NULL ,
`education_level_id` VARCHAR(255)  NOT NULL ,
`address` VARCHAR(300)  NOT NULL ,
`region_id` INT NOT NULL ,
`province_id` INT NOT NULL ,
`district_id` INT NOT NULL ,
`sub_district_id` INT NOT NULL ,
`post_code` INT(5)  NOT NULL ,
`latitude` DECIMAL(12,8)  NOT NULL ,
`longitude` DECIMAL(12,8)  NOT NULL ,
`mobile` VARCHAR(50)  NOT NULL ,
`fax` VARCHAR(50)  NOT NULL ,
`email` VARCHAR(100)  NOT NULL ,
`line_id` VARCHAR(100)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_farm_has_farmer` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`farm_id` INT(11)  NOT NULL ,
`farmer_id` INT(11)  NOT NULL ,
`own_date` DATE NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_training_program` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` VARCHAR(100)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_farmer_training` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`farmer_id` INT NOT NULL ,
`training_program_id` INT NOT NULL ,
`training_date` DATE NOT NULL ,
`training_place` VARCHAR(100)  NOT NULL ,
`training_day` INT(11)  NOT NULL ,
`create_by` INT(11)  NOT NULL ,
`update_by` INT(11)  NOT NULL ,
`create_datetime` DATE NOT NULL ,
`update_datetime` DATE NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_farm_score` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`farm_id` INT NOT NULL ,
`assessment_date` DATE NOT NULL ,
`score` DECIMAL(10,2)  NOT NULL ,
`score_result` VARCHAR(50)  NOT NULL ,
`assessment_data` TEXT NOT NULL ,
`remark` VARCHAR(100)  NOT NULL ,
`create_by` INT(11)  NOT NULL ,
`update_by` INT(11)  NOT NULL ,
`create_datetime` DATE NOT NULL ,
`update_datetime` DATE NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_cow` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`farm_id` INT NOT NULL ,
`cow_id` VARCHAR(100)  NOT NULL ,
`name` VARCHAR(100)  NOT NULL ,
`dld_id` VARCHAR(100)  NOT NULL ,
`ear_id` VARCHAR(100)  NOT NULL ,
`birthdate` DATE NOT NULL ,
`gender` VARCHAR(255)  NOT NULL ,
`move_farm_date` DATE NOT NULL ,
`move_type` VARCHAR(255)  NOT NULL ,
`move_from_farm_id` INT NOT NULL ,
`breed` VARCHAR(300)  NOT NULL ,
`father_cow_id` INT NOT NULL ,
`mother_cow_id` INT NOT NULL ,
`create_by` INT(11)  NOT NULL ,
`update_by` INT(11)  NOT NULL ,
`create_datetime` DATE NOT NULL ,
`update_datetime` DATE NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_cow_status` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` VARCHAR(100)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_cow_has_cow_status` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`cow_id` INT(11)  NOT NULL ,
`status_code` VARCHAR(100)  NOT NULL ,
`cow_status_id` INT(11)  NOT NULL ,
`status_date` DATE NOT NULL ,
`create_by` INT(11)  NOT NULL ,
`update_by` INT(11)  NOT NULL ,
`create_date` DATE NOT NULL ,
`update_date` DATE NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_cow_move_out` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`cow_id` INT NOT NULL ,
`move_out_date` DATE NOT NULL ,
`move_type` VARCHAR(255)  NOT NULL ,
`from_farm_id` INT NOT NULL ,
`to_farm_id` INT NOT NULL ,
`move_objective` VARCHAR(100)  NOT NULL ,
`move_reason` VARCHAR(100)  NOT NULL ,
`create_by` INT(11)  NOT NULL ,
`update_by` INT(11)  NOT NULL ,
`create_date` DATE NOT NULL ,
`update_date` DATE NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_cow_breeding` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`cow_id` INT NOT NULL ,
`round` INT(11)  NOT NULL ,
`no` INT(11)  NOT NULL ,
`breeding_date` DATE NOT NULL ,
`father_id` INT NOT NULL ,
`lot_no` VARCHAR(100)  NOT NULL ,
`semen_source` VARCHAR(100)  NOT NULL ,
`inspection_date` DATE NOT NULL ,
`inspection_result` VARCHAR(100)  NOT NULL ,
`vet_id` INT(11)  NOT NULL ,
`create_by` INT(11)  NOT NULL ,
`update_by` INT(11)  NOT NULL ,
`create_datetime` DATE NOT NULL ,
`update_datetime` DATE NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_cow_has_child` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`cow_id` INT(11)  NOT NULL ,
`breeding_date` DATE NOT NULL ,
`father_id` INT(11)  NOT NULL ,
`stop_milking_date` DATE NOT NULL ,
`birth_date` DATE NOT NULL ,
`child_status` INT(11)  NOT NULL ,
`child_gender` INT(1)  NOT NULL ,
`child_weight` DECIMAL(10,2)  NOT NULL ,
`child_ear_id` VARCHAR(100)  NOT NULL ,
`vet_id` INT(11)  NOT NULL ,
`create_by` INT(11)  NOT NULL ,
`update_by` INT(11)  NOT NULL ,
`create_date` DATE NOT NULL ,
`update_date` DATE NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;


INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `content_history_options`)
SELECT * FROM ( SELECT 'ภาค','com_edairy.region','{"special":{"dbtable":"#__ed_region","key":"id","type":"Region","prefix":"EdairyTable"}}', '{"formFile":"administrator\/components\/com_edairy\/models\/forms\/region.xml", "hideFields":["checked_out","checked_out_time","params","language"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_edairy.region')
) LIMIT 1;

INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `content_history_options`)
SELECT * FROM ( SELECT 'จังหวัด','com_edairy.province','{"special":{"dbtable":"#__ed_province","key":"id","type":"Province","prefix":"EdairyTable"}}', '{"formFile":"administrator\/components\/com_edairy\/models\/forms\/province.xml", "hideFields":["checked_out","checked_out_time","params","language"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"region_id","targetTable":"#__ed_region","targetColumn":"id","displayColumn":"name"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_edairy.province')
) LIMIT 1;

INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `content_history_options`)
SELECT * FROM ( SELECT 'อำเภอ','com_edairy.district','{"special":{"dbtable":"#__ed_district","key":"id","type":"District","prefix":"EdairyTable"}}', '{"formFile":"administrator\/components\/com_edairy\/models\/forms\/district.xml", "hideFields":["checked_out","checked_out_time","params","language"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"province_id","targetTable":"#__ed_province","targetColumn":"id","displayColumn":"name"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_edairy.district')
) LIMIT 1;

INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `content_history_options`)
SELECT * FROM ( SELECT 'ตำบล','com_edairy.sub_district','{"special":{"dbtable":"#__ed_sub_district","key":"id","type":"Sub_district","prefix":"EdairyTable"}}', '{"formFile":"administrator\/components\/com_edairy\/models\/forms\/sub_district.xml", "hideFields":["checked_out","checked_out_time","params","language"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"district_id","targetTable":"#__ed_district","targetColumn":"id","displayColumn":"name"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_edairy.sub_district')
) LIMIT 1;

INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `content_history_options`)
SELECT * FROM ( SELECT 'สหกรณ์/ศูนย์รับน้ำนมดิบ','com_edairy.coop','{"special":{"dbtable":"#__ed_coop","key":"id","type":"Coop","prefix":"EdairyTable"}}', '{"formFile":"administrator\/components\/com_edairy\/models\/forms\/coop.xml", "hideFields":["checked_out","checked_out_time","params","language" ,"address"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"region_id","targetTable":"#__ed_region","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"province_id","targetTable":"#__ed_province","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"district_id","targetTable":"#__ed_district","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"sub_district_id","targetTable":"#__ed_sub_district","targetColumn":"id","displayColumn":"name"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_edairy.coop')
) LIMIT 1;

INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `content_history_options`)
SELECT * FROM ( SELECT 'การรับรองมาตรฐาน GMP','com_edairy.coop_gmp_certify','{"special":{"dbtable":"#__ed_coop_gmp_certify","key":"id","type":"Coop_gmp_certify","prefix":"EdairyTable"}}', '{"formFile":"administrator\/components\/com_edairy\/models\/forms\/coop_gmp_certify.xml", "hideFields":["checked_out","checked_out_time","params","language"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"coop_id","targetTable":"#__ed_coop","targetColumn":"id","displayColumn":"coop_code"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_edairy.coop_gmp_certify')
) LIMIT 1;

INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `content_history_options`)
SELECT * FROM ( SELECT 'โรงงาน/บริษัทที่ส่งนม','com_edairy.coop_send_milk_company','{"special":{"dbtable":"#__ed_coop_send_milk_company","key":"id","type":"Coop_send_milk_company","prefix":"EdairyTable"}}', '{"formFile":"administrator\/components\/com_edairy\/models\/forms\/coop_send_milk_company.xml", "hideFields":["checked_out","checked_out_time","params","language"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"coop_id","targetTable":"#__ed_coop","targetColumn":"id","displayColumn":"coop_code"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_edairy.coop_send_milk_company')
) LIMIT 1;

INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `content_history_options`)
SELECT * FROM ( SELECT 'ปริมาณน้ำนมที่ส่ง อ.ส.ค. (ตาม MOU)','com_edairy.mou','{"special":{"dbtable":"#__ed_coop_has_mou","key":"id","type":"Mou","prefix":"EdairyTable"}}', '{"formFile":"administrator\/components\/com_edairy\/models\/forms\/mou.xml", "hideFields":["checked_out","checked_out_time","params","language"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"coop_id","targetTable":"#__ed_coop","targetColumn":"id","displayColumn":"coop_code"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_edairy.mou')
) LIMIT 1;

INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `content_history_options`)
SELECT * FROM ( SELECT 'โครงการเลี้ยงโค','com_edairy.cow_project','{"special":{"dbtable":"#__ed_cow_project","key":"id","type":"Cow_project","prefix":"EdairyTable"}}', '{"formFile":"administrator\/components\/com_edairy\/models\/forms\/cow_project.xml", "hideFields":["checked_out","checked_out_time","params","language"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_edairy.cow_project')
) LIMIT 1;

INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `content_history_options`)
SELECT * FROM ( SELECT 'สภาพโรงเรือน','com_edairy.farm_condition','{"special":{"dbtable":"#__ed_farm_condition","key":"id","type":"Farm_condition","prefix":"EdairyTable"}}', '{"formFile":"administrator\/components\/com_edairy\/models\/forms\/farm_condition.xml", "hideFields":["checked_out","checked_out_time","params","language"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_edairy.farm_condition')
) LIMIT 1;

INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `content_history_options`)
SELECT * FROM ( SELECT 'โครงการเพิ่มประสิทธิภาพ','com_edairy.performance_improvement_project','{"special":{"dbtable":"#__ed_performance_improvement_project","key":"id","type":"Performance_improvement_project","prefix":"EdairyTable"}}', '{"formFile":"administrator\/components\/com_edairy\/models\/forms\/performance_improvement_project.xml", "hideFields":["checked_out","checked_out_time","params","language"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_edairy.performance_improvement_project')
) LIMIT 1;

INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `content_history_options`)
SELECT * FROM ( SELECT 'ระบบการรีด','com_edairy.milking_system','{"special":{"dbtable":"#__ed_milking_system","key":"id","type":"Milking_system","prefix":"EdairyTable"}}', '{"formFile":"administrator\/components\/com_edairy\/models\/forms\/milking_system.xml", "hideFields":["checked_out","checked_out_time","params","language"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_edairy.milking_system')
) LIMIT 1;

INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `content_history_options`)
SELECT * FROM ( SELECT 'ฟาร์ม','com_edairy.farm','{"special":{"dbtable":"#__ed_farm","key":"id","type":"Farm","prefix":"EdairyTable"}}', '{"formFile":"administrator\/components\/com_edairy\/models\/forms\/farm.xml", "hideFields":["checked_out","checked_out_time","params","language"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"coop_id","targetTable":"#__ed_coop","targetColumn":"id","displayColumn":"coop_code"},{"sourceColumn":"province_id","targetTable":"#__ed_province","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"region_id","targetTable":"#__ed_region","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"district_id","targetTable":"#__ed_district","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"sub_district_id","targetTable":"#__ed_sub_district","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"farm_project_id","targetTable":"#__ed_cow_project","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"performance_improvement_project_id","targetTable":"#__ed_performance_improvement_project","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"milking_system_id","targetTable":"#__ed_milking_system","targetColumn":"id","displayColumn":"name"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_edairy.farm')
) LIMIT 1;

INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `content_history_options`)
SELECT * FROM ( SELECT 'การรับรองมาตรฐาน GAP','com_edairy.gap_certify','{"special":{"dbtable":"#__ed_farm_gap_certify","key":"id","type":"Gap_certify","prefix":"EdairyTable"}}', '{"formFile":"administrator\/components\/com_edairy\/models\/forms\/gap_certify.xml", "hideFields":["checked_out","checked_out_time","params","language"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"farm_id","targetTable":"#__ed_farm","targetColumn":"id","displayColumn":"member_code"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_edairy.gap_certify')
) LIMIT 1;

INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `content_history_options`)
SELECT * FROM ( SELECT 'การจัดกลุ่มฟาร์ม','com_edairy.farm_grade_certify','{"special":{"dbtable":"#__ed_farm_grade_certify","key":"id","type":"Farm_grade_certify","prefix":"EdairyTable"}}', '{"formFile":"administrator\/components\/com_edairy\/models\/forms\/farm_grade_certify.xml", "hideFields":["checked_out","checked_out_time","params","language"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"farm_id","targetTable":"#__ed_farm","targetColumn":"id","displayColumn":"member_code"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_edairy.farm_grade_certify')
) LIMIT 1;

INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `content_history_options`)
SELECT * FROM ( SELECT 'เกณฑ์การจัดกลุ่มฟาร์ม','com_edairy.farm_grade_announcement','{"special":{"dbtable":"#__ed_farm_grade_announcement","key":"id","type":"Farm_grade_announcement","prefix":"EdairyTable"}}', '{"formFile":"administrator\/components\/com_edairy\/models\/forms\/farm_grade_announcement.xml", "hideFields":["checked_out","checked_out_time","params","language" ,"detail"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_edairy.farm_grade_announcement')
) LIMIT 1;

INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `content_history_options`)
SELECT * FROM ( SELECT 'เกษตรกร','com_edairy.farmer','{"special":{"dbtable":"#__ed_farmer","key":"id","type":"Farmer","prefix":"EdairyTable"}}', '{"formFile":"administrator\/components\/com_edairy\/models\/forms\/farmer.xml", "hideFields":["checked_out","checked_out_time","params","language" ,"image_url"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"region_id","targetTable":"#__ed_region","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"province_id","targetTable":"#__ed_province","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"district_id","targetTable":"#__ed_district","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"sub_district_id","targetTable":"#__ed_sub_district","targetColumn":"id","displayColumn":"name"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_edairy.farmer')
) LIMIT 1;

INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `content_history_options`)
SELECT * FROM ( SELECT 'Farmer','com_edairy.farmer518847','{"special":{"dbtable":"#__ed_farm_has_farmer","key":"id","type":"Farmer518847","prefix":"EdairyTable"}}', '{"formFile":"administrator\/components\/com_edairy\/models\/forms\/farmer518847.xml", "hideFields":["checked_out","checked_out_time","params","language"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_edairy.farmer518847')
) LIMIT 1;

INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `content_history_options`)
SELECT * FROM ( SELECT 'การฝึกอบรม','com_edairy.training_program','{"special":{"dbtable":"#__ed_training_program","key":"id","type":"Training_program","prefix":"EdairyTable"}}', '{"formFile":"administrator\/components\/com_edairy\/models\/forms\/training_program.xml", "hideFields":["checked_out","checked_out_time","params","language"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_edairy.training_program')
) LIMIT 1;

INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `content_history_options`)
SELECT * FROM ( SELECT 'ประวัติการฝึกอบรม','com_edairy.farmer_training','{"special":{"dbtable":"#__ed_farmer_training","key":"id","type":"Farmer_training","prefix":"EdairyTable"}}', '{"formFile":"administrator\/components\/com_edairy\/models\/forms\/farmer_training.xml", "hideFields":["checked_out","checked_out_time","params","language"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"farmer_id","targetTable":"#__ed_farmer","targetColumn":"id","displayColumn":"citizen_id"},{"sourceColumn":"training_program_id","targetTable":"#__ed_training_program","targetColumn":"id","displayColumn":"name"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_edairy.farmer_training')
) LIMIT 1;

INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `content_history_options`)
SELECT * FROM ( SELECT 'คะแนนคอก','com_edairy.farm_score','{"special":{"dbtable":"#__ed_farm_score","key":"id","type":"Farm_score","prefix":"EdairyTable"}}', '{"formFile":"administrator\/components\/com_edairy\/models\/forms\/farm_score.xml", "hideFields":["checked_out","checked_out_time","params","language"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"farm_id","targetTable":"#__ed_farm","targetColumn":"id","displayColumn":"member_code"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_edairy.farm_score')
) LIMIT 1;

INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `content_history_options`)
SELECT * FROM ( SELECT 'โค','com_edairy.cow','{"special":{"dbtable":"#__ed_cow","key":"id","type":"Cow","prefix":"EdairyTable"}}', '{"formFile":"administrator\/components\/com_edairy\/models\/forms\/cow.xml", "hideFields":["checked_out","checked_out_time","params","language"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"farm_id","targetTable":"#__ed_farm","targetColumn":"id","displayColumn":"member_code"},{"sourceColumn":"move_from_farm_id","targetTable":"#__ed_farm","targetColumn":"id","displayColumn":"member_code"},{"sourceColumn":"father_cow_id","targetTable":"#__ed_cow","targetColumn":"id","displayColumn":"cow_id"},{"sourceColumn":"mother_cow_id","targetTable":"#__ed_cow","targetColumn":"id","displayColumn":"cow_id"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_edairy.cow')
) LIMIT 1;

INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `content_history_options`)
SELECT * FROM ( SELECT 'สถานะโค','com_edairy.cow_status','{"special":{"dbtable":"#__ed_cow_status","key":"id","type":"Cow_status","prefix":"EdairyTable"}}', '{"formFile":"administrator\/components\/com_edairy\/models\/forms\/cow_status.xml", "hideFields":["checked_out","checked_out_time","params","language"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_edairy.cow_status')
) LIMIT 1;

INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `content_history_options`)
SELECT * FROM ( SELECT 'Statu','com_edairy.statu725728','{"special":{"dbtable":"#__ed_cow_has_cow_status","key":"id","type":"Statu725728","prefix":"EdairyTable"}}', '{"formFile":"administrator\/components\/com_edairy\/models\/forms\/statu725728.xml", "hideFields":["checked_out","checked_out_time","params","language"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_edairy.statu725728')
) LIMIT 1;

INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `content_history_options`)
SELECT * FROM ( SELECT 'ย้ายออกจากฝูง','com_edairy.move_out','{"special":{"dbtable":"#__ed_cow_move_out","key":"id","type":"Move_out","prefix":"EdairyTable"}}', '{"formFile":"administrator\/components\/com_edairy\/models\/forms\/move_out.xml", "hideFields":["checked_out","checked_out_time","params","language"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"cow_id","targetTable":"#__ed_cow","targetColumn":"id","displayColumn":"cow_id"},{"sourceColumn":"from_farm_id","targetTable":"#__ed_farm","targetColumn":"id","displayColumn":"member_code"},{"sourceColumn":"to_farm_id","targetTable":"#__ed_farm","targetColumn":"id","displayColumn":"member_code"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_edairy.move_out')
) LIMIT 1;

INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `content_history_options`)
SELECT * FROM ( SELECT 'ประวัติการผสมพันธ์','com_edairy.breeding','{"special":{"dbtable":"#__ed_cow_breeding","key":"id","type":"Breeding","prefix":"EdairyTable"}}', '{"formFile":"administrator\/components\/com_edairy\/models\/forms\/breeding.xml", "hideFields":["checked_out","checked_out_time","params","language"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"cow_id","targetTable":"#__ed_cow","targetColumn":"id","displayColumn":"cow_id"},{"sourceColumn":"father_id","targetTable":"#__ed_cow","targetColumn":"id","displayColumn":"cow_id"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_edairy.breeding')
) LIMIT 1;

INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `content_history_options`)
SELECT * FROM ( SELECT 'Child','com_edairy.child','{"special":{"dbtable":"#__ed_cow_has_child","key":"id","type":"Child","prefix":"EdairyTable"}}', '{"formFile":"administrator\/components\/com_edairy\/models\/forms\/child.xml", "hideFields":["checked_out","checked_out_time","params","language"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_edairy.child')
) LIMIT 1;
