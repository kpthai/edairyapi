<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_edairy'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

function formatDate($date){

	if($date=="")
		return $date;
  if($date=="0000-00-00")
    return "-";
  $raw_date = explode("-", $date);
  $raw_date[0]+=543;
  return  $raw_date[2] . "/" . $raw_date[1] . "/" . $raw_date[0];
}

function formatDateCalendar($date){

  if($date=="" || $date=="0000-00-00")
    return "";
  $raw_date = explode("-", $date);
  $raw_date[0]+=543;
  return  $raw_date[2] . "/" . $raw_date[1] . "/" . $raw_date[0];
}

function formatDateFullHistory($date){

    if($date=="")
        return $date;
  $raw_date = explode("-", $date);
  $raw_date[0]+=543;
  return  $raw_date[2] . "/" . $raw_date[1] . "/" . $raw_date[0];
}


function formatDateFull($date){
  $month_map = array("01"=>"มกราคม", "02"=>"กุมภาพันธ์", "03"=>"มีนาคม", "04"=>"เมษายน", "05"=>"พฤษภาคม", "06"=>"มิถุนายน", "07"=>"กรกฎาคม", "08"=>"สิงหาคม", "09"=>"กันยายน", "10"=>"ตุลาคม", "11"=>"พฤศจิกายน", "12"=>"ธันวาคม");
    if($date=="")
        return $date;
  $raw_date = explode("-", $date);
  $raw_date[0]+=543;
  return  intval($raw_date[2]) . " " . $month_map[$raw_date[1]] . " " . $raw_date[0];
}

function formatDateFullOfficial($date){
  $month_map = array("01"=>"มกราคม", "02"=>"กุมภาพันธ์", "03"=>"มีนาคม", "04"=>"เมษายน", "05"=>"พฤษภาคม", "06"=>"มิถุนายน", "07"=>"กรกฎาคม", "08"=>"สิงหาคม", "09"=>"กันยายน", "10"=>"ตุลาคม", "11"=>"พฤศจิกายน", "12"=>"ธันวาคม");
    if($date=="")
        return $date;
  $raw_date = explode("-", $date);
  $raw_date[0]+=543;
  return  intval($raw_date[2]) . " เดือน " . $month_map[$raw_date[1]] . " พ.ศ. " . $raw_date[0];
}


function displayFilter($text){
	$search = array("", " ", "0");
	$replace = array("-", "-", "-");
	return str_replace($search, $replace, $text);
}

function formatSQLDate($date){
  $raw_date = explode("/", $date);
  return $raw_date[2]-543 . "-" . $raw_date[1] . "-" . $raw_date[0];
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Edairy', JPATH_COMPONENT_ADMINISTRATOR);

$controller = JControllerLegacy::getInstance('Edairy');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
