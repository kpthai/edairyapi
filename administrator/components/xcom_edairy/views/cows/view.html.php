<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Edairy.
 *
 * @since  1.6
 */
class EdairyViewCows extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');

		$this->masterData = $this->get('MasterData');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		EdairyHelpersEdairyadmin::addSubmenu('cows');

		$this->addToolbar();

		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function addToolbar()
	{
		$state = $this->get('State');
		$canDo = EdairyHelpersEdairyadmin::getActions();

		JToolBarHelper::title(JText::_('COM_EDAIRY_TITLE_COWS'), 'cows.png');

		// Check if the form exists before showing the add/edit buttons
		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/cow';

		if (file_exists($formPath))
		{
			if ($canDo->get('core.create'))
			{
				JToolBarHelper::addNew('cow.add', 'JTOOLBAR_NEW');

				if (isset($this->items[0]))
				{
					JToolbarHelper::custom('cows.duplicate', 'copy.png', 'copy_f2.png', 'JTOOLBAR_DUPLICATE', true);
				}
			}

			if ($canDo->get('core.edit') && isset($this->items[0]))
			{
				JToolBarHelper::editList('cow.edit', 'JTOOLBAR_EDIT');
			}
		}

		if ($canDo->get('core.edit.state'))
		{
			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::custom('cows.publish', 'publish.png', 'publish_f2.png', 'JTOOLBAR_PUBLISH', true);
				JToolBarHelper::custom('cows.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
			}
			elseif (isset($this->items[0]))
			{
				// If this component does not use state then show a direct delete button as we can not trash
				JToolBarHelper::deleteList('', 'cows.delete', 'JTOOLBAR_DELETE');
			}

			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::archiveList('cows.archive', 'JTOOLBAR_ARCHIVE');
			}

			if (isset($this->items[0]->checked_out))
			{
				JToolBarHelper::custom('cows.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
			}
		}

		// Show trash and delete for components that uses the state field
		if (isset($this->items[0]->state))
		{
			if ($state->get('filter.state') == -2 && $canDo->get('core.delete'))
			{
				JToolBarHelper::deleteList('', 'cows.delete', 'JTOOLBAR_EMPTY_TRASH');
				JToolBarHelper::divider();
			}
			elseif ($canDo->get('core.edit.state'))
			{
				JToolBarHelper::trash('cows.trash', 'JTOOLBAR_TRASH');
				JToolBarHelper::divider();
			}
		}

		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_edairy');
		}

		// Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_edairy&view=cows');

		$this->extra_sidebar = '';                                                
        //Filter for the field farm_id;
        jimport('joomla.form.form');
        $options = array();
        JForm::addFormPath(JPATH_COMPONENT . '/models/forms');
        $form = JForm::getInstance('com_edairy.cow', 'cow');

        $field = $form->getField('farm_id');

        $query = $form->getFieldAttribute('filter_farm_id','query');
        $translate = $form->getFieldAttribute('filter_farm_id','translate');
        $key = $form->getFieldAttribute('filter_farm_id','key_field');
        $value = $form->getFieldAttribute('filter_farm_id','value_field');

        // Get the database object.
        $db = JFactory::getDbo();

        // Set the query and get the result list.
        $db->setQuery($query);
        $items = $db->loadObjectlist();

        // Build the field options.
        if (!empty($items))
        {
            foreach ($items as $item)
            {
                if ($translate == true)
                {
                    $options[] = JHtml::_('select.option', $item->$key, JText::_($item->$value));
                }
                else
                {
                    $options[] = JHtml::_('select.option', $item->$key, $item->$value);
                }
            }
        }

        JHtmlSidebar::addFilter(
            '$ฟาร์ม',
            'filter_farm_id',
            JHtml::_('select.options', $options, "value", "text", $this->state->get('filter.farm_id')),
            true
        );
			//Filter for the field birthdate
		$this->extra_sidebar .= '<div class="other-filters">';
			$this->extra_sidebar .= '<small><label for="filter_from_birthdate">'. JText::sprintf('COM_EDAIRY_FROM_FILTER', 'วันเกิด') .'</label></small>';
			$this->extra_sidebar .= JHtml::_('calendar', $this->state->get('filter.birthdate.from'), 'filter_from_birthdate', 'filter_from_birthdate', '%Y-%m-%d', array('style' => 'width:142px;', 'onchange' => 'this.form.submit();'));
			$this->extra_sidebar .= '<small><label for="filter_to_birthdate">'. JText::sprintf('COM_EDAIRY_TO_FILTER', 'วันเกิด') .'</label></small>';
			$this->extra_sidebar .= JHtml::_('calendar', $this->state->get('filter.birthdate.to'), 'filter_to_birthdate', 'filter_to_birthdate', '%Y-%m-%d', array('style' => 'width:142px;', 'onchange'=> 'this.form.submit();'));
		$this->extra_sidebar .= '</div>';
			$this->extra_sidebar .= '<hr class="hr-condensed">';

		//Filter for the field gender
		$select_label = JText::sprintf('COM_EDAIRY_FILTER_SELECT_LABEL', 'เพศ');
		$options = array();
		$options[0] = new stdClass();
		$options[0]->value = "1";
		$options[0]->text = "เพศผู้";
		$options[1] = new stdClass();
		$options[1]->value = "2";
		$options[1]->text = "เพศเมีย";
		JHtmlSidebar::addFilter(
			$select_label,
			'filter_gender',
			JHtml::_('select.options', $options , "value", "text", $this->state->get('filter.gender'), true)
		);

		//Filter for the field update_by
		$this->extra_sidebar .= '<div class="other-filters">';
		$this->extra_sidebar .= '<small><label for="filter_update_by">ปรับปรุงโดย</label></small>';
		$this->extra_sidebar .= JHtmlList::users('filter_update_by', $this->state->get('filter.update_by'), 1, 'onchange="this.form.submit();"');
		$this->extra_sidebar .= '</div>';
		$this->extra_sidebar .= '<hr class="hr-condensed">';

			//Filter for the field update_datetime
		$this->extra_sidebar .= '<div class="other-filters">';
			$this->extra_sidebar .= '<small><label for="filter_from_update_datetime">'. JText::sprintf('COM_EDAIRY_FROM_FILTER', 'วันที่ปรับปรุง') .'</label></small>';
			$this->extra_sidebar .= JHtml::_('calendar', $this->state->get('filter.update_datetime.from'), 'filter_from_update_datetime', 'filter_from_update_datetime', '%Y-%m-%d', array('style' => 'width:142px;', 'onchange' => 'this.form.submit();'));
			$this->extra_sidebar .= '<small><label for="filter_to_update_datetime">'. JText::sprintf('COM_EDAIRY_TO_FILTER', 'วันที่ปรับปรุง') .'</label></small>';
			$this->extra_sidebar .= JHtml::_('calendar', $this->state->get('filter.update_datetime.to'), 'filter_to_update_datetime', 'filter_to_update_datetime', '%Y-%m-%d', array('style' => 'width:142px;', 'onchange'=> 'this.form.submit();'));
		$this->extra_sidebar .= '</div>';
			$this->extra_sidebar .= '<hr class="hr-condensed">';

		JHtmlSidebar::addFilter(

			JText::_('JOPTION_SELECT_PUBLISHED'),

			'filter_published',

			JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), "value", "text", $this->state->get('filter.state'), true)

		);
	}

	/**
	 * Method to order fields 
	 *
	 * @return void 
	 */
	protected function getSortFields()
	{
		return array(
			'a.`id`' => JText::_('JGRID_HEADING_ID'),
			'a.`farm_id`' => JText::_('COM_EDAIRY_COWS_FARM_ID'),
			'a.`cow_id`' => JText::_('COM_EDAIRY_COWS_COW_ID'),
			'a.`name`' => JText::_('COM_EDAIRY_COWS_NAME'),
			'a.`dld_id`' => JText::_('COM_EDAIRY_COWS_DLD_ID'),
			'a.`ear_id`' => JText::_('COM_EDAIRY_COWS_EAR_ID'),
			'a.`birthdate`' => JText::_('COM_EDAIRY_COWS_BIRTHDATE'),
			'a.`gender`' => JText::_('COM_EDAIRY_COWS_GENDER'),
			'a.`breed`' => JText::_('COM_EDAIRY_COWS_BREED'),
			'a.`update_by`' => JText::_('COM_EDAIRY_COWS_UPDATE_BY'),
			'a.`update_datetime`' => JText::_('COM_EDAIRY_COWS_UPDATE_DATETIME'),
			'a.`state`' => JText::_('JSTATUS'),
		);
	}
}
