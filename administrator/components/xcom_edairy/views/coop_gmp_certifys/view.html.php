<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Edairy.
 *
 * @since  1.6
 */
class EdairyViewCoop_gmp_certifys extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		EdairyHelpersEdairyadmin::addSubmenu('coop_gmp_certifys');

		$this->addToolbar();

		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function addToolbar()
	{
		$state = $this->get('State');
		$canDo = EdairyHelpersEdairyadmin::getActions();

		JToolBarHelper::title(JText::_('COM_EDAIRY_TITLE_COOP_GMP_CERTIFYS'), 'coop_gmp_certifys.png');

		// Check if the form exists before showing the add/edit buttons
		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/coop_gmp_certify';

		if (file_exists($formPath))
		{
			if ($canDo->get('core.create'))
			{
				JToolBarHelper::addNew('coop_gmp_certify.add', 'JTOOLBAR_NEW');

				if (isset($this->items[0]))
				{
					JToolbarHelper::custom('coop_gmp_certifys.duplicate', 'copy.png', 'copy_f2.png', 'JTOOLBAR_DUPLICATE', true);
				}
			}

			if ($canDo->get('core.edit') && isset($this->items[0]))
			{
				JToolBarHelper::editList('coop_gmp_certify.edit', 'JTOOLBAR_EDIT');
			}
		}

		if ($canDo->get('core.edit.state'))
		{
			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::custom('coop_gmp_certifys.publish', 'publish.png', 'publish_f2.png', 'JTOOLBAR_PUBLISH', true);
				JToolBarHelper::custom('coop_gmp_certifys.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
			}
			elseif (isset($this->items[0]))
			{
				// If this component does not use state then show a direct delete button as we can not trash
				JToolBarHelper::deleteList('', 'coop_gmp_certifys.delete', 'JTOOLBAR_DELETE');
			}

			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::archiveList('coop_gmp_certifys.archive', 'JTOOLBAR_ARCHIVE');
			}

			if (isset($this->items[0]->checked_out))
			{
				JToolBarHelper::custom('coop_gmp_certifys.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
			}
		}

		// Show trash and delete for components that uses the state field
		if (isset($this->items[0]->state))
		{
			if ($state->get('filter.state') == -2 && $canDo->get('core.delete'))
			{
				JToolBarHelper::deleteList('', 'coop_gmp_certifys.delete', 'JTOOLBAR_EMPTY_TRASH');
				JToolBarHelper::divider();
			}
			elseif ($canDo->get('core.edit.state'))
			{
				JToolBarHelper::trash('coop_gmp_certifys.trash', 'JTOOLBAR_TRASH');
				JToolBarHelper::divider();
			}
		}

		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_edairy');
		}

		// Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_edairy&view=coop_gmp_certifys');

		$this->extra_sidebar = '';                                                
        //Filter for the field coop_id;
        jimport('joomla.form.form');
        $options = array();
        JForm::addFormPath(JPATH_COMPONENT . '/models/forms');
        $form = JForm::getInstance('com_edairy.coop_gmp_certify', 'coop_gmp_certify');

        $field = $form->getField('coop_id');

        $query = $form->getFieldAttribute('filter_coop_id','query');
        $translate = $form->getFieldAttribute('filter_coop_id','translate');
        $key = $form->getFieldAttribute('filter_coop_id','key_field');
        $value = $form->getFieldAttribute('filter_coop_id','value_field');

        // Get the database object.
        $db = JFactory::getDbo();

        // Set the query and get the result list.
        $db->setQuery($query);
        $items = $db->loadObjectlist();

        // Build the field options.
        if (!empty($items))
        {
            foreach ($items as $item)
            {
                if ($translate == true)
                {
                    $options[] = JHtml::_('select.option', $item->$key, JText::_($item->$value));
                }
                else
                {
                    $options[] = JHtml::_('select.option', $item->$key, $item->$value);
                }
            }
        }

        JHtmlSidebar::addFilter(
            '$สหกรณ์/ศูนย์รับน้ำนมดิบ',
            'filter_coop_id',
            JHtml::_('select.options', $options, "value", "text", $this->state->get('filter.coop_id')),
            true
        );
			//Filter for the field certify_date
		$this->extra_sidebar .= '<div class="other-filters">';
			$this->extra_sidebar .= '<small><label for="filter_from_certify_date">'. JText::sprintf('COM_EDAIRY_FROM_FILTER', 'วันที่ได้รับการรับรอง') .'</label></small>';
			$this->extra_sidebar .= JHtml::_('calendar', $this->state->get('filter.certify_date.from'), 'filter_from_certify_date', 'filter_from_certify_date', '%Y-%m-%d', array('style' => 'width:142px;', 'onchange' => 'this.form.submit();'));
			$this->extra_sidebar .= '<small><label for="filter_to_certify_date">'. JText::sprintf('COM_EDAIRY_TO_FILTER', 'วันที่ได้รับการรับรอง') .'</label></small>';
			$this->extra_sidebar .= JHtml::_('calendar', $this->state->get('filter.certify_date.to'), 'filter_to_certify_date', 'filter_to_certify_date', '%Y-%m-%d', array('style' => 'width:142px;', 'onchange'=> 'this.form.submit();'));
		$this->extra_sidebar .= '</div>';
			$this->extra_sidebar .= '<hr class="hr-condensed">';

			//Filter for the field expire_date
		$this->extra_sidebar .= '<div class="other-filters">';
			$this->extra_sidebar .= '<small><label for="filter_from_expire_date">'. JText::sprintf('COM_EDAIRY_FROM_FILTER', 'วันที่หมดอายุ') .'</label></small>';
			$this->extra_sidebar .= JHtml::_('calendar', $this->state->get('filter.expire_date.from'), 'filter_from_expire_date', 'filter_from_expire_date', '%Y-%m-%d', array('style' => 'width:142px;', 'onchange' => 'this.form.submit();'));
			$this->extra_sidebar .= '<small><label for="filter_to_expire_date">'. JText::sprintf('COM_EDAIRY_TO_FILTER', 'วันที่หมดอายุ') .'</label></small>';
			$this->extra_sidebar .= JHtml::_('calendar', $this->state->get('filter.expire_date.to'), 'filter_to_expire_date', 'filter_to_expire_date', '%Y-%m-%d', array('style' => 'width:142px;', 'onchange'=> 'this.form.submit();'));
		$this->extra_sidebar .= '</div>';
			$this->extra_sidebar .= '<hr class="hr-condensed">';

		//Filter for the field update_by
		$this->extra_sidebar .= '<div class="other-filters">';
		$this->extra_sidebar .= '<small><label for="filter_update_by">ปรับปรุงโดย</label></small>';
		$this->extra_sidebar .= JHtmlList::users('filter_update_by', $this->state->get('filter.update_by'), 1, 'onchange="this.form.submit();"');
		$this->extra_sidebar .= '</div>';
		$this->extra_sidebar .= '<hr class="hr-condensed">';

		JHtmlSidebar::addFilter(

			JText::_('JOPTION_SELECT_PUBLISHED'),

			'filter_published',

			JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), "value", "text", $this->state->get('filter.state'), true)

		);
	}

	/**
	 * Method to order fields 
	 *
	 * @return void 
	 */
	protected function getSortFields()
	{
		return array(
			'a.`id`' => JText::_('JGRID_HEADING_ID'),
			'a.`coop_id`' => JText::_('COM_EDAIRY_COOP_GMP_CERTIFYS_COOP_ID'),
			'a.`certify_no`' => JText::_('COM_EDAIRY_COOP_GMP_CERTIFYS_CERTIFY_NO'),
			'a.`certify_date`' => JText::_('COM_EDAIRY_COOP_GMP_CERTIFYS_CERTIFY_DATE'),
			'a.`expire_date`' => JText::_('COM_EDAIRY_COOP_GMP_CERTIFYS_EXPIRE_DATE'),
			'a.`certify_agency`' => JText::_('COM_EDAIRY_COOP_GMP_CERTIFYS_CERTIFY_AGENCY'),
			'a.`update_by`' => JText::_('COM_EDAIRY_COOP_GMP_CERTIFYS_UPDATE_BY'),
			'a.`update_datetime`' => JText::_('COM_EDAIRY_COOP_GMP_CERTIFYS_UPDATE_DATETIME'),
			'a.`state`' => JText::_('JSTATUS'),
		);
	}
}
