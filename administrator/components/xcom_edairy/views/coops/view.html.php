<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Edairy.
 *
 * @since  1.6
 */
class EdairyViewCoops extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');

        $this->masterData = $this->get('MasterData');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		EdairyHelpersEdairyadmin::addSubmenu('coops');

		$this->addToolbar();

		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function addToolbar()
	{
		$state = $this->get('State');
		$canDo = EdairyHelpersEdairyadmin::getActions();

		JToolBarHelper::title(JText::_('COM_EDAIRY_TITLE_COOPS'), 'coops.png');

		// Check if the form exists before showing the add/edit buttons
		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/coop';

		if (file_exists($formPath))
		{
			if ($canDo->get('core.create'))
			{
				JToolBarHelper::addNew('coop.add', 'JTOOLBAR_NEW');

				if (isset($this->items[0]))
				{
					JToolbarHelper::custom('coops.duplicate', 'copy.png', 'copy_f2.png', 'JTOOLBAR_DUPLICATE', true);
				}
			}

			if ($canDo->get('core.edit') && isset($this->items[0]))
			{
				JToolBarHelper::editList('coop.edit', 'JTOOLBAR_EDIT');
			}
		}

		if ($canDo->get('core.edit.state'))
		{
			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::custom('coops.publish', 'publish.png', 'publish_f2.png', 'JTOOLBAR_PUBLISH', true);
				JToolBarHelper::custom('coops.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
			}
			elseif (isset($this->items[0]))
			{
				// If this component does not use state then show a direct delete button as we can not trash
				JToolBarHelper::deleteList('', 'coops.delete', 'JTOOLBAR_DELETE');
			}

			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::archiveList('coops.archive', 'JTOOLBAR_ARCHIVE');
			}

			if (isset($this->items[0]->checked_out))
			{
				JToolBarHelper::custom('coops.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
			}
		}

		// Show trash and delete for components that uses the state field
		if (isset($this->items[0]->state))
		{
			if ($state->get('filter.state') == -2 && $canDo->get('core.delete'))
			{
				JToolBarHelper::deleteList('', 'coops.delete', 'JTOOLBAR_EMPTY_TRASH');
				JToolBarHelper::divider();
			}
			elseif ($canDo->get('core.edit.state'))
			{
				JToolBarHelper::trash('coops.trash', 'JTOOLBAR_TRASH');
				JToolBarHelper::divider();
			}
		}

		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_edairy');
		}

		// Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_edairy&view=coops');

		$this->extra_sidebar = '';                                                
        //Filter for the field region_id;
        jimport('joomla.form.form');
        $options = array();
        JForm::addFormPath(JPATH_COMPONENT . '/models/forms');
        $form = JForm::getInstance('com_edairy.coop', 'coop');

        $field = $form->getField('region_id');

        $query = $form->getFieldAttribute('filter_region_id','query');
        $translate = $form->getFieldAttribute('filter_region_id','translate');
        $key = $form->getFieldAttribute('filter_region_id','key_field');
        $value = $form->getFieldAttribute('filter_region_id','value_field');

        // Get the database object.
        $db = JFactory::getDbo();

        // Set the query and get the result list.
        $db->setQuery($query);
        $items = $db->loadObjectlist();

        // Build the field options.
        if (!empty($items))
        {
            foreach ($items as $item)
            {
                if ($translate == true)
                {
                    $options[] = JHtml::_('select.option', $item->$key, JText::_($item->$value));
                }
                else
                {
                    $options[] = JHtml::_('select.option', $item->$key, $item->$value);
                }
            }
        }

        JHtmlSidebar::addFilter(
            '$ภาค',
            'filter_region_id',
            JHtml::_('select.options', $options, "value", "text", $this->state->get('filter.region_id')),
            true
        );                                                
        //Filter for the field province_id;
        jimport('joomla.form.form');
        $options = array();
        JForm::addFormPath(JPATH_COMPONENT . '/models/forms');
        $form = JForm::getInstance('com_edairy.coop', 'coop');

        $field = $form->getField('province_id');

        $query = $form->getFieldAttribute('filter_province_id','query');
        $translate = $form->getFieldAttribute('filter_province_id','translate');
        $key = $form->getFieldAttribute('filter_province_id','key_field');
        $value = $form->getFieldAttribute('filter_province_id','value_field');

        // Get the database object.
        $db = JFactory::getDbo();

        // Set the query and get the result list.
        $db->setQuery($query);
        $items = $db->loadObjectlist();

        // Build the field options.
        if (!empty($items))
        {
            foreach ($items as $item)
            {
                if ($translate == true)
                {
                    $options[] = JHtml::_('select.option', $item->$key, JText::_($item->$value));
                }
                else
                {
                    $options[] = JHtml::_('select.option', $item->$key, $item->$value);
                }
            }
        }

        JHtmlSidebar::addFilter(
            '$จังหวัด',
            'filter_province_id',
            JHtml::_('select.options', $options, "value", "text", $this->state->get('filter.province_id')),
            true
        );                                                
        //Filter for the field district_id;
        jimport('joomla.form.form');
        $options = array();
        JForm::addFormPath(JPATH_COMPONENT . '/models/forms');
        $form = JForm::getInstance('com_edairy.coop', 'coop');

        $field = $form->getField('district_id');

        $query = $form->getFieldAttribute('filter_district_id','query');
        $translate = $form->getFieldAttribute('filter_district_id','translate');
        $key = $form->getFieldAttribute('filter_district_id','key_field');
        $value = $form->getFieldAttribute('filter_district_id','value_field');

        // Get the database object.
        $db = JFactory::getDbo();

        // Set the query and get the result list.
        $db->setQuery($query);
        $items = $db->loadObjectlist();

        // Build the field options.
        if (!empty($items))
        {
            foreach ($items as $item)
            {
                if ($translate == true)
                {
                    $options[] = JHtml::_('select.option', $item->$key, JText::_($item->$value));
                }
                else
                {
                    $options[] = JHtml::_('select.option', $item->$key, $item->$value);
                }
            }
        }

        JHtmlSidebar::addFilter(
            '$อำเภอ',
            'filter_district_id',
            JHtml::_('select.options', $options, "value", "text", $this->state->get('filter.district_id')),
            true
        );                                                
        //Filter for the field sub_district_id;
        jimport('joomla.form.form');
        $options = array();
        JForm::addFormPath(JPATH_COMPONENT . '/models/forms');
        $form = JForm::getInstance('com_edairy.coop', 'coop');

        $field = $form->getField('sub_district_id');

        $query = $form->getFieldAttribute('filter_sub_district_id','query');
        $translate = $form->getFieldAttribute('filter_sub_district_id','translate');
        $key = $form->getFieldAttribute('filter_sub_district_id','key_field');
        $value = $form->getFieldAttribute('filter_sub_district_id','value_field');

        // Get the database object.
        $db = JFactory::getDbo();

        // Set the query and get the result list.
        $db->setQuery($query);
        $items = $db->loadObjectlist();

        // Build the field options.
        if (!empty($items))
        {
            foreach ($items as $item)
            {
                if ($translate == true)
                {
                    $options[] = JHtml::_('select.option', $item->$key, JText::_($item->$value));
                }
                else
                {
                    $options[] = JHtml::_('select.option', $item->$key, $item->$value);
                }
            }
        }

        JHtmlSidebar::addFilter(
            '$ตำบล',
            'filter_sub_district_id',
            JHtml::_('select.options', $options, "value", "text", $this->state->get('filter.sub_district_id')),
            true
        );
		JHtmlSidebar::addFilter(

			JText::_('JOPTION_SELECT_PUBLISHED'),

			'filter_published',

			JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), "value", "text", $this->state->get('filter.state'), true)

		);
	}

	/**
	 * Method to order fields 
	 *
	 * @return void 
	 */
	protected function getSortFields()
	{
		return array(
			'a.`id`' => JText::_('JGRID_HEADING_ID'),
			'a.`coop_code`' => JText::_('COM_EDAIRY_COOPS_COOP_CODE'),
			'a.`coop_abbr`' => JText::_('COM_EDAIRY_COOPS_COOP_ABBR'),
			'a.`name`' => JText::_('COM_EDAIRY_COOPS_NAME'),
			'a.`region_id`' => JText::_('COM_EDAIRY_COOPS_REGION_ID'),
			'a.`province_id`' => JText::_('COM_EDAIRY_COOPS_PROVINCE_ID'),
			'a.`district_id`' => JText::_('COM_EDAIRY_COOPS_DISTRICT_ID'),
			'a.`sub_district_id`' => JText::_('COM_EDAIRY_COOPS_SUB_DISTRICT_ID'),
			'a.`state`' => JText::_('JSTATUS'),
		);
	}
}
