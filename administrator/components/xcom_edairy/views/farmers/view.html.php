<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Edairy.
 *
 * @since  1.6
 */
class EdairyViewFarmers extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		EdairyHelpersEdairyadmin::addSubmenu('farmers');

		$this->addToolbar();

		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function addToolbar()
	{
		$state = $this->get('State');
		$canDo = EdairyHelpersEdairyadmin::getActions();

		JToolBarHelper::title(JText::_('COM_EDAIRY_TITLE_FARMERS'), 'farmers.png');

		// Check if the form exists before showing the add/edit buttons
		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/farmer';

		if (file_exists($formPath))
		{
			if ($canDo->get('core.create'))
			{
				JToolBarHelper::addNew('farmer.add', 'JTOOLBAR_NEW');

				if (isset($this->items[0]))
				{
					JToolbarHelper::custom('farmers.duplicate', 'copy.png', 'copy_f2.png', 'JTOOLBAR_DUPLICATE', true);
				}
			}

			if ($canDo->get('core.edit') && isset($this->items[0]))
			{
				JToolBarHelper::editList('farmer.edit', 'JTOOLBAR_EDIT');
			}
		}

		if ($canDo->get('core.edit.state'))
		{
			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::custom('farmers.publish', 'publish.png', 'publish_f2.png', 'JTOOLBAR_PUBLISH', true);
				JToolBarHelper::custom('farmers.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
			}
			elseif (isset($this->items[0]))
			{
				// If this component does not use state then show a direct delete button as we can not trash
				JToolBarHelper::deleteList('', 'farmers.delete', 'JTOOLBAR_DELETE');
			}

			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::archiveList('farmers.archive', 'JTOOLBAR_ARCHIVE');
			}

			if (isset($this->items[0]->checked_out))
			{
				JToolBarHelper::custom('farmers.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
			}
		}

		// Show trash and delete for components that uses the state field
		if (isset($this->items[0]->state))
		{
			if ($state->get('filter.state') == -2 && $canDo->get('core.delete'))
			{
				JToolBarHelper::deleteList('', 'farmers.delete', 'JTOOLBAR_EMPTY_TRASH');
				JToolBarHelper::divider();
			}
			elseif ($canDo->get('core.edit.state'))
			{
				JToolBarHelper::trash('farmers.trash', 'JTOOLBAR_TRASH');
				JToolBarHelper::divider();
			}
		}

		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_edairy');
		}

		// Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_edairy&view=farmers');

		$this->extra_sidebar = '';
			//Filter for the field birthdate
		$this->extra_sidebar .= '<div class="other-filters">';
			$this->extra_sidebar .= '<small><label for="filter_from_birthdate">'. JText::sprintf('COM_EDAIRY_FROM_FILTER', 'วันเกิด') .'</label></small>';
			$this->extra_sidebar .= JHtml::_('calendar', $this->state->get('filter.birthdate.from'), 'filter_from_birthdate', 'filter_from_birthdate', '%Y-%m-%d', array('style' => 'width:142px;', 'onchange' => 'this.form.submit();'));
			$this->extra_sidebar .= '<small><label for="filter_to_birthdate">'. JText::sprintf('COM_EDAIRY_TO_FILTER', 'วันเกิด') .'</label></small>';
			$this->extra_sidebar .= JHtml::_('calendar', $this->state->get('filter.birthdate.to'), 'filter_to_birthdate', 'filter_to_birthdate', '%Y-%m-%d', array('style' => 'width:142px;', 'onchange'=> 'this.form.submit();'));
		$this->extra_sidebar .= '</div>';
			$this->extra_sidebar .= '<hr class="hr-condensed">';

		//Filter for the field education_level_id
		$select_label = JText::sprintf('COM_EDAIRY_FILTER_SELECT_LABEL', 'ระดับการศึกษา');
		$options = array();
		$options[0] = new stdClass();
		$options[0]->value = "1";
		$options[0]->text = "ประถมศึกษา";
		$options[1] = new stdClass();
		$options[1]->value = "2";
		$options[1]->text = "มัธยมศึกษา";
		$options[2] = new stdClass();
		$options[2]->value = "3";
		$options[2]->text = "ปวช.";
		$options[3] = new stdClass();
		$options[3]->value = "4";
		$options[3]->text = "ปวศ.";
		$options[4] = new stdClass();
		$options[4]->value = "5";
		$options[4]->text = "ปริญญาตรี";
		$options[5] = new stdClass();
		$options[5]->value = "6";
		$options[5]->text = "ปริญญาโท";
		$options[6] = new stdClass();
		$options[6]->value = "7";
		$options[6]->text = "ปริญญาเอก";
		JHtmlSidebar::addFilter(
			$select_label,
			'filter_education_level_id',
			JHtml::_('select.options', $options , "value", "text", $this->state->get('filter.education_level_id'), true)
		);
                                                
        //Filter for the field region_id;
        jimport('joomla.form.form');
        $options = array();
        JForm::addFormPath(JPATH_COMPONENT . '/models/forms');
        $form = JForm::getInstance('com_edairy.farmer', 'farmer');

        $field = $form->getField('region_id');

        $query = $form->getFieldAttribute('filter_region_id','query');
        $translate = $form->getFieldAttribute('filter_region_id','translate');
        $key = $form->getFieldAttribute('filter_region_id','key_field');
        $value = $form->getFieldAttribute('filter_region_id','value_field');

        // Get the database object.
        $db = JFactory::getDbo();

        // Set the query and get the result list.
        $db->setQuery($query);
        $items = $db->loadObjectlist();

        // Build the field options.
        if (!empty($items))
        {
            foreach ($items as $item)
            {
                if ($translate == true)
                {
                    $options[] = JHtml::_('select.option', $item->$key, JText::_($item->$value));
                }
                else
                {
                    $options[] = JHtml::_('select.option', $item->$key, $item->$value);
                }
            }
        }

        JHtmlSidebar::addFilter(
            '$ภาค',
            'filter_region_id',
            JHtml::_('select.options', $options, "value", "text", $this->state->get('filter.region_id')),
            true
        );                                                
        //Filter for the field province_id;
        jimport('joomla.form.form');
        $options = array();
        JForm::addFormPath(JPATH_COMPONENT . '/models/forms');
        $form = JForm::getInstance('com_edairy.farmer', 'farmer');

        $field = $form->getField('province_id');

        $query = $form->getFieldAttribute('filter_province_id','query');
        $translate = $form->getFieldAttribute('filter_province_id','translate');
        $key = $form->getFieldAttribute('filter_province_id','key_field');
        $value = $form->getFieldAttribute('filter_province_id','value_field');

        // Get the database object.
        $db = JFactory::getDbo();

        // Set the query and get the result list.
        $db->setQuery($query);
        $items = $db->loadObjectlist();

        // Build the field options.
        if (!empty($items))
        {
            foreach ($items as $item)
            {
                if ($translate == true)
                {
                    $options[] = JHtml::_('select.option', $item->$key, JText::_($item->$value));
                }
                else
                {
                    $options[] = JHtml::_('select.option', $item->$key, $item->$value);
                }
            }
        }

        JHtmlSidebar::addFilter(
            '$จังหวัด',
            'filter_province_id',
            JHtml::_('select.options', $options, "value", "text", $this->state->get('filter.province_id')),
            true
        );                                                
        //Filter for the field district_id;
        jimport('joomla.form.form');
        $options = array();
        JForm::addFormPath(JPATH_COMPONENT . '/models/forms');
        $form = JForm::getInstance('com_edairy.farmer', 'farmer');

        $field = $form->getField('district_id');

        $query = $form->getFieldAttribute('filter_district_id','query');
        $translate = $form->getFieldAttribute('filter_district_id','translate');
        $key = $form->getFieldAttribute('filter_district_id','key_field');
        $value = $form->getFieldAttribute('filter_district_id','value_field');

        // Get the database object.
        $db = JFactory::getDbo();

        // Set the query and get the result list.
        $db->setQuery($query);
        $items = $db->loadObjectlist();

        // Build the field options.
        if (!empty($items))
        {
            foreach ($items as $item)
            {
                if ($translate == true)
                {
                    $options[] = JHtml::_('select.option', $item->$key, JText::_($item->$value));
                }
                else
                {
                    $options[] = JHtml::_('select.option', $item->$key, $item->$value);
                }
            }
        }

        JHtmlSidebar::addFilter(
            '$อำเภอ',
            'filter_district_id',
            JHtml::_('select.options', $options, "value", "text", $this->state->get('filter.district_id')),
            true
        );                                                
        //Filter for the field sub_district_id;
        jimport('joomla.form.form');
        $options = array();
        JForm::addFormPath(JPATH_COMPONENT . '/models/forms');
        $form = JForm::getInstance('com_edairy.farmer', 'farmer');

        $field = $form->getField('sub_district_id');

        $query = $form->getFieldAttribute('filter_sub_district_id','query');
        $translate = $form->getFieldAttribute('filter_sub_district_id','translate');
        $key = $form->getFieldAttribute('filter_sub_district_id','key_field');
        $value = $form->getFieldAttribute('filter_sub_district_id','value_field');

        // Get the database object.
        $db = JFactory::getDbo();

        // Set the query and get the result list.
        $db->setQuery($query);
        $items = $db->loadObjectlist();

        // Build the field options.
        if (!empty($items))
        {
            foreach ($items as $item)
            {
                if ($translate == true)
                {
                    $options[] = JHtml::_('select.option', $item->$key, JText::_($item->$value));
                }
                else
                {
                    $options[] = JHtml::_('select.option', $item->$key, $item->$value);
                }
            }
        }

        JHtmlSidebar::addFilter(
            '$ตำบล',
            'filter_sub_district_id',
            JHtml::_('select.options', $options, "value", "text", $this->state->get('filter.sub_district_id')),
            true
        );
		JHtmlSidebar::addFilter(

			JText::_('JOPTION_SELECT_PUBLISHED'),

			'filter_published',

			JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), "value", "text", $this->state->get('filter.state'), true)

		);
	}

	/**
	 * Method to order fields 
	 *
	 * @return void 
	 */
	protected function getSortFields()
	{
		return array(
			'a.`id`' => JText::_('JGRID_HEADING_ID'),
			'a.`citizen_id`' => JText::_('COM_EDAIRY_FARMERS_CITIZEN_ID'),
			'a.`image_url`' => JText::_('COM_EDAIRY_FARMERS_IMAGE_URL'),
			'a.`title_id`' => JText::_('COM_EDAIRY_FARMERS_TITLE_ID'),
			'a.`name`' => JText::_('COM_EDAIRY_FARMERS_NAME'),
			'a.`surname`' => JText::_('COM_EDAIRY_FARMERS_SURNAME'),
			'a.`birthdate`' => JText::_('COM_EDAIRY_FARMERS_BIRTHDATE'),
			'a.`region_id`' => JText::_('COM_EDAIRY_FARMERS_REGION_ID'),
			'a.`province_id`' => JText::_('COM_EDAIRY_FARMERS_PROVINCE_ID'),
			'a.`district_id`' => JText::_('COM_EDAIRY_FARMERS_DISTRICT_ID'),
			'a.`mobile`' => JText::_('COM_EDAIRY_FARMERS_MOBILE'),
			'a.`state`' => JText::_('JSTATUS'),
		);
	}
}
