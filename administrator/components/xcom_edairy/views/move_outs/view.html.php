<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Edairy.
 *
 * @since  1.6
 */
class EdairyViewMove_outs extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		EdairyHelpersEdairyadmin::addSubmenu('move_outs');

		$this->addToolbar();

		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function addToolbar()
	{
		$state = $this->get('State');
		$canDo = EdairyHelpersEdairyadmin::getActions();

		JToolBarHelper::title(JText::_('COM_EDAIRY_TITLE_MOVE_OUTS'), 'move_outs.png');

		// Check if the form exists before showing the add/edit buttons
		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/move_out';

		if (file_exists($formPath))
		{
			if ($canDo->get('core.create'))
			{
				JToolBarHelper::addNew('move_out.add', 'JTOOLBAR_NEW');

				if (isset($this->items[0]))
				{
					JToolbarHelper::custom('move_outs.duplicate', 'copy.png', 'copy_f2.png', 'JTOOLBAR_DUPLICATE', true);
				}
			}

			if ($canDo->get('core.edit') && isset($this->items[0]))
			{
				JToolBarHelper::editList('move_out.edit', 'JTOOLBAR_EDIT');
			}
		}

		if ($canDo->get('core.edit.state'))
		{
			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::custom('move_outs.publish', 'publish.png', 'publish_f2.png', 'JTOOLBAR_PUBLISH', true);
				JToolBarHelper::custom('move_outs.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
			}
			elseif (isset($this->items[0]))
			{
				// If this component does not use state then show a direct delete button as we can not trash
				JToolBarHelper::deleteList('', 'move_outs.delete', 'JTOOLBAR_DELETE');
			}

			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::archiveList('move_outs.archive', 'JTOOLBAR_ARCHIVE');
			}

			if (isset($this->items[0]->checked_out))
			{
				JToolBarHelper::custom('move_outs.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
			}
		}

		// Show trash and delete for components that uses the state field
		if (isset($this->items[0]->state))
		{
			if ($state->get('filter.state') == -2 && $canDo->get('core.delete'))
			{
				JToolBarHelper::deleteList('', 'move_outs.delete', 'JTOOLBAR_EMPTY_TRASH');
				JToolBarHelper::divider();
			}
			elseif ($canDo->get('core.edit.state'))
			{
				JToolBarHelper::trash('move_outs.trash', 'JTOOLBAR_TRASH');
				JToolBarHelper::divider();
			}
		}

		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_edairy');
		}

		// Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_edairy&view=move_outs');

		$this->extra_sidebar = '';                                                
        //Filter for the field cow_id;
        jimport('joomla.form.form');
        $options = array();
        JForm::addFormPath(JPATH_COMPONENT . '/models/forms');
        $form = JForm::getInstance('com_edairy.move_out', 'move_out');

        $field = $form->getField('cow_id');

        $query = $form->getFieldAttribute('filter_cow_id','query');
        $translate = $form->getFieldAttribute('filter_cow_id','translate');
        $key = $form->getFieldAttribute('filter_cow_id','key_field');
        $value = $form->getFieldAttribute('filter_cow_id','value_field');

        // Get the database object.
        $db = JFactory::getDbo();

        // Set the query and get the result list.
        $db->setQuery($query);
        $items = $db->loadObjectlist();

        // Build the field options.
        if (!empty($items))
        {
            foreach ($items as $item)
            {
                if ($translate == true)
                {
                    $options[] = JHtml::_('select.option', $item->$key, JText::_($item->$value));
                }
                else
                {
                    $options[] = JHtml::_('select.option', $item->$key, $item->$value);
                }
            }
        }

        JHtmlSidebar::addFilter(
            '$โค',
            'filter_cow_id',
            JHtml::_('select.options', $options, "value", "text", $this->state->get('filter.cow_id')),
            true
        );
			//Filter for the field move_out_date
		$this->extra_sidebar .= '<div class="other-filters">';
			$this->extra_sidebar .= '<small><label for="filter_from_move_out_date">'. JText::sprintf('COM_EDAIRY_FROM_FILTER', 'วันที่ย้ายออก') .'</label></small>';
			$this->extra_sidebar .= JHtml::_('calendar', $this->state->get('filter.move_out_date.from'), 'filter_from_move_out_date', 'filter_from_move_out_date', '%Y-%m-%d', array('style' => 'width:142px;', 'onchange' => 'this.form.submit();'));
			$this->extra_sidebar .= '<small><label for="filter_to_move_out_date">'. JText::sprintf('COM_EDAIRY_TO_FILTER', 'วันที่ย้ายออก') .'</label></small>';
			$this->extra_sidebar .= JHtml::_('calendar', $this->state->get('filter.move_out_date.to'), 'filter_to_move_out_date', 'filter_to_move_out_date', '%Y-%m-%d', array('style' => 'width:142px;', 'onchange'=> 'this.form.submit();'));
		$this->extra_sidebar .= '</div>';
			$this->extra_sidebar .= '<hr class="hr-condensed">';

		//Filter for the field move_type
		$select_label = JText::sprintf('COM_EDAIRY_FILTER_SELECT_LABEL', 'วิธีการ');
		$options = array();
		$options[0] = new stdClass();
		$options[0]->value = "1";
		$options[0]->text = "ตาย";
		$options[1] = new stdClass();
		$options[1]->value = "2";
		$options[1]->text = "ขาย";
		$options[2] = new stdClass();
		$options[2]->value = "3";
		$options[2]->text = "คัดทิ้ง";
		JHtmlSidebar::addFilter(
			$select_label,
			'filter_move_type',
			JHtml::_('select.options', $options , "value", "text", $this->state->get('filter.move_type'), true)
		);
                                                
        //Filter for the field from_farm_id;
        jimport('joomla.form.form');
        $options = array();
        JForm::addFormPath(JPATH_COMPONENT . '/models/forms');
        $form = JForm::getInstance('com_edairy.move_out', 'move_out');

        $field = $form->getField('from_farm_id');

        $query = $form->getFieldAttribute('filter_from_farm_id','query');
        $translate = $form->getFieldAttribute('filter_from_farm_id','translate');
        $key = $form->getFieldAttribute('filter_from_farm_id','key_field');
        $value = $form->getFieldAttribute('filter_from_farm_id','value_field');

        // Get the database object.
        $db = JFactory::getDbo();

        // Set the query and get the result list.
        $db->setQuery($query);
        $items = $db->loadObjectlist();

        // Build the field options.
        if (!empty($items))
        {
            foreach ($items as $item)
            {
                if ($translate == true)
                {
                    $options[] = JHtml::_('select.option', $item->$key, JText::_($item->$value));
                }
                else
                {
                    $options[] = JHtml::_('select.option', $item->$key, $item->$value);
                }
            }
        }

        JHtmlSidebar::addFilter(
            '$ต้นทาง',
            'filter_from_farm_id',
            JHtml::_('select.options', $options, "value", "text", $this->state->get('filter.from_farm_id')),
            true
        );                                                
        //Filter for the field to_farm_id;
        jimport('joomla.form.form');
        $options = array();
        JForm::addFormPath(JPATH_COMPONENT . '/models/forms');
        $form = JForm::getInstance('com_edairy.move_out', 'move_out');

        $field = $form->getField('to_farm_id');

        $query = $form->getFieldAttribute('filter_to_farm_id','query');
        $translate = $form->getFieldAttribute('filter_to_farm_id','translate');
        $key = $form->getFieldAttribute('filter_to_farm_id','key_field');
        $value = $form->getFieldAttribute('filter_to_farm_id','value_field');

        // Get the database object.
        $db = JFactory::getDbo();

        // Set the query and get the result list.
        $db->setQuery($query);
        $items = $db->loadObjectlist();

        // Build the field options.
        if (!empty($items))
        {
            foreach ($items as $item)
            {
                if ($translate == true)
                {
                    $options[] = JHtml::_('select.option', $item->$key, JText::_($item->$value));
                }
                else
                {
                    $options[] = JHtml::_('select.option', $item->$key, $item->$value);
                }
            }
        }

        JHtmlSidebar::addFilter(
            '$ปลายทาง',
            'filter_to_farm_id',
            JHtml::_('select.options', $options, "value", "text", $this->state->get('filter.to_farm_id')),
            true
        );
		//Filter for the field update_by
		$this->extra_sidebar .= '<div class="other-filters">';
		$this->extra_sidebar .= '<small><label for="filter_update_by">ปรับปรุงโดย</label></small>';
		$this->extra_sidebar .= JHtmlList::users('filter_update_by', $this->state->get('filter.update_by'), 1, 'onchange="this.form.submit();"');
		$this->extra_sidebar .= '</div>';
		$this->extra_sidebar .= '<hr class="hr-condensed">';

			//Filter for the field update_date
		$this->extra_sidebar .= '<div class="other-filters">';
			$this->extra_sidebar .= '<small><label for="filter_from_update_date">'. JText::sprintf('COM_EDAIRY_FROM_FILTER', 'วันที่ปรับปรุง') .'</label></small>';
			$this->extra_sidebar .= JHtml::_('calendar', $this->state->get('filter.update_date.from'), 'filter_from_update_date', 'filter_from_update_date', '%Y-%m-%d', array('style' => 'width:142px;', 'onchange' => 'this.form.submit();'));
			$this->extra_sidebar .= '<small><label for="filter_to_update_date">'. JText::sprintf('COM_EDAIRY_TO_FILTER', 'วันที่ปรับปรุง') .'</label></small>';
			$this->extra_sidebar .= JHtml::_('calendar', $this->state->get('filter.update_date.to'), 'filter_to_update_date', 'filter_to_update_date', '%Y-%m-%d', array('style' => 'width:142px;', 'onchange'=> 'this.form.submit();'));
		$this->extra_sidebar .= '</div>';
			$this->extra_sidebar .= '<hr class="hr-condensed">';

		JHtmlSidebar::addFilter(

			JText::_('JOPTION_SELECT_PUBLISHED'),

			'filter_published',

			JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), "value", "text", $this->state->get('filter.state'), true)

		);
	}

	/**
	 * Method to order fields 
	 *
	 * @return void 
	 */
	protected function getSortFields()
	{
		return array(
			'a.`id`' => JText::_('JGRID_HEADING_ID'),
			'a.`cow_id`' => JText::_('COM_EDAIRY_MOVE_OUTS_COW_ID'),
			'a.`move_out_date`' => JText::_('COM_EDAIRY_MOVE_OUTS_MOVE_OUT_DATE'),
			'a.`move_type`' => JText::_('COM_EDAIRY_MOVE_OUTS_MOVE_TYPE'),
			'a.`from_farm_id`' => JText::_('COM_EDAIRY_MOVE_OUTS_FROM_FARM_ID'),
			'a.`to_farm_id`' => JText::_('COM_EDAIRY_MOVE_OUTS_TO_FARM_ID'),
			'a.`move_objective`' => JText::_('COM_EDAIRY_MOVE_OUTS_MOVE_OBJECTIVE'),
			'a.`move_reason`' => JText::_('COM_EDAIRY_MOVE_OUTS_MOVE_REASON'),
			'a.`update_by`' => JText::_('COM_EDAIRY_MOVE_OUTS_UPDATE_BY'),
			'a.`update_date`' => JText::_('COM_EDAIRY_MOVE_OUTS_UPDATE_DATE'),
			'a.`state`' => JText::_('JSTATUS'),
		);
	}
}
