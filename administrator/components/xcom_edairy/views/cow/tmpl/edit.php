<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_edairy/css/form.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	js('input:hidden.farm_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('farm_idhidden')){
			js('#jform_farm_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_farm_id").trigger("liszt:updated");
	js('input:hidden.move_from_farm_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('move_from_farm_idhidden')){
			js('#jform_move_from_farm_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_move_from_farm_id").trigger("liszt:updated");
	js('input:hidden.father_cow_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('father_cow_idhidden')){
			js('#jform_father_cow_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_father_cow_id").trigger("liszt:updated");
	js('input:hidden.mother_cow_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('mother_cow_idhidden')){
			js('#jform_mother_cow_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_mother_cow_id").trigger("liszt:updated");
	});

	Joomla.submitbutton = function (task) {
		if (task == 'cow.cancel') {
			Joomla.submitform(task, document.getElementById('cow-form'));
		}
		else {
			
			if (task != 'cow.cancel' && document.formvalidator.isValid(document.id('cow-form'))) {
				
				Joomla.submitform(task, document.getElementById('cow-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_edairy&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="cow-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', 'ข้อมูลทั่วไป'); ?>
		<div class="row-fluid">
			<div class="span5 form-horizontal">
				<fieldset class="adminform">

				<h4>รูปโค</h4>
				<img src="../images/model_cow.png" width="200" style="padding:10px" /><br />

				<input type="file" />

				<h4>ข้อมูลเจ้าของ</h4>
									<?php echo $this->form->renderField('id'); ?>

									<?php echo $this->form->renderField('farm_id'); ?>

				<div class="control-group">
					<div class="control-label">
						<label class="">รหัสศูนย์/สหกรณ์</label>
					</div>
					<div class="controls">
						<input type="text" name="coop_name" readonly placeholder="อัตโนมัติ" value="<?php echo $this->item->farm->coop_name; ?>" />
					</div>
				</div>
				<div class="control-group">
					<div class="control-label">
						<label class="">หมายเลขสมาชิก</label>
					</div>
					<div class="controls">
						<input type="text" name="member_code" placeholder="อัตโนมัติ" readonly value="<?php echo $this->item->farm->member_code; ?>"/>
					</div>
				</div>

				<div class="control-group">
					<div class="control-label">
						<label class="">ชื่อเกษตรกร</label>
					</div>
					<div class="controls">
						<input type="text" name="farmer_name" placeholder="อัตโนมัติ" readonly value="<?php echo $this->item->farm->farmer_name; ?>"/>
					</div>
				</div>

				

			<?php
				foreach((array)$this->item->farm_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="farm_id" name="jform[farm_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>								<?php echo $this->form->renderField('create_by'); ?>
				<?php echo $this->form->renderField('update_by'); ?>
				<?php echo $this->form->renderField('create_datetime'); ?>
				<?php echo $this->form->renderField('update_datetime'); ?>
				<?php echo $this->form->renderField('state'); ?>
				<?php echo $this->form->renderField('created_by'); ?>


					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>

			<div class="span5 form-horizontal">
				<h4>ข้อมูลโค</h4>
				<?php echo $this->form->renderField('cow_id'); ?>
				<?php echo $this->form->renderField('name'); ?>
				<?php echo $this->form->renderField('dld_id'); ?>
				<?php echo $this->form->renderField('ear_id'); ?>
				<?php echo $this->form->renderField('birthdate'); ?>
				<?php echo $this->form->renderField('gender'); ?>
				<?php echo $this->form->renderField('move_farm_date'); ?>
				<?php echo $this->form->renderField('move_type'); ?>
				<?php echo $this->form->renderField('move_from_farm_id'); ?>

			<?php
				foreach((array)$this->item->move_from_farm_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="move_from_farm_id" name="jform[move_from_farm_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('breed'); ?>
				<?php echo $this->form->renderField('father_cow_id'); ?>

			<?php
				foreach((array)$this->item->father_cow_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="father_cow_id" name="jform[father_cow_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('mother_cow_id'); ?>

			<?php
				foreach((array)$this->item->mother_cow_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="mother_cow_id" name="jform[mother_cow_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'breed', JText::_('สายพันธ์', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">
					<div style="float:right">
						<button class="btn btn-info" data-toggle="modal" data-target="#GMPModal">เพิ่มรายการ</button>
					</div>
					<h3>ประวัติเจ้าของฟาร์ม</h3>
					<table class="table table-striped table-bordered table-blue">
						<thead>
							<tr>
								<th style="text-align:center">ที่</th>
								<th style="text-align:center">เลขบัตรประชาชน</th>
								<th style="text-align:center">ชื่อ-สกุล</th>
								<th style="text-align:center">วันที่เริ่มเป็นเจ้าของ</th>
								<th style="text-align:center">แก้ไข</th>
								<th style="text-align:center">ลบ</th>
							</tr>
						</thead>
						<tbody>
							<?php if(count($this->item->owner_history)==0){ ?>
								<tr>
									<td style="text-align:center" colspan="6">ไม่พบข้อมูล</td>
								</tr>
							<?php }else{ ?>
								<?php $i=0;
									foreach($this->item->owner_history as $key=>$data){ ?>
									<tr>
										<td><?php echo ++$i; ?></td>
										<td><?php echo $data->citizen_id; ?></td>
										<td><?php echo $data->name; ?> <?php echo $data->surname; ?></td>
										<td><?php echo formatDate($data->own_date); ?></td>
										<td style="text-align: center"><button type="button" class="btn btn-warning" data-toggle="modal" data-target="#OwnerModal" onClick="javascript:getOwner(<?php echo $data->id; ?>);">แก้ไข</button></td>
										<td style="text-align: center"><button type="button" class="btn btn-danger" onClick="javascript: if(confirm('ยืนยันการลบ?')){jQuery(this).prop('disabled', true);deleteOwner(<?php echo $data->id; ?>);}">ลบ</button></td>
									</tr>
								<?php } ?>
							<?php } ?>
						</tbody>
					</table>
									
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>




		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'cow_status', JText::_('สถานภาพ', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">
					<div style="float:right">
						<button class="btn btn-info" data-toggle="modal" data-target="#GMPModal">เพิ่มรายการ</button>
					</div>
					<h3>ประวัติเจ้าของฟาร์ม</h3>
					<table class="table table-striped table-bordered table-blue">
						<thead>
							<tr>
								<th style="text-align:center">ที่</th>
								<th style="text-align:center">เลขบัตรประชาชน</th>
								<th style="text-align:center">ชื่อ-สกุล</th>
								<th style="text-align:center">วันที่เริ่มเป็นเจ้าของ</th>
								<th style="text-align:center">แก้ไข</th>
								<th style="text-align:center">ลบ</th>
							</tr>
						</thead>
						<tbody>
							<?php if(count($this->item->owner_history)==0){ ?>
								<tr>
									<td style="text-align:center" colspan="6">ไม่พบข้อมูล</td>
								</tr>
							<?php }else{ ?>
								<?php $i=0;
									foreach($this->item->owner_history as $key=>$data){ ?>
									<tr>
										<td><?php echo ++$i; ?></td>
										<td><?php echo $data->citizen_id; ?></td>
										<td><?php echo $data->name; ?> <?php echo $data->surname; ?></td>
										<td><?php echo formatDate($data->own_date); ?></td>
										<td style="text-align: center"><button type="button" class="btn btn-warning" data-toggle="modal" data-target="#OwnerModal" onClick="javascript:getOwner(<?php echo $data->id; ?>);">แก้ไข</button></td>
										<td style="text-align: center"><button type="button" class="btn btn-danger" onClick="javascript: if(confirm('ยืนยันการลบ?')){jQuery(this).prop('disabled', true);deleteOwner(<?php echo $data->id; ?>);}">ลบ</button></td>
									</tr>
								<?php } ?>
							<?php } ?>
						</tbody>
					</table>
									
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>



		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'move_out', JText::_('ย้ายออกจากฝูง', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">
					<div style="float:right">
						<button class="btn btn-info" data-toggle="modal" data-target="#GMPModal">เพิ่มรายการ</button>
					</div>
					<h3>ประวัติเจ้าของฟาร์ม</h3>
					<table class="table table-striped table-bordered table-blue">
						<thead>
							<tr>
								<th style="text-align:center">ที่</th>
								<th style="text-align:center">เลขบัตรประชาชน</th>
								<th style="text-align:center">ชื่อ-สกุล</th>
								<th style="text-align:center">วันที่เริ่มเป็นเจ้าของ</th>
								<th style="text-align:center">แก้ไข</th>
								<th style="text-align:center">ลบ</th>
							</tr>
						</thead>
						<tbody>
							<?php if(count($this->item->owner_history)==0){ ?>
								<tr>
									<td style="text-align:center" colspan="6">ไม่พบข้อมูล</td>
								</tr>
							<?php }else{ ?>
								<?php $i=0;
									foreach($this->item->owner_history as $key=>$data){ ?>
									<tr>
										<td><?php echo ++$i; ?></td>
										<td><?php echo $data->citizen_id; ?></td>
										<td><?php echo $data->name; ?> <?php echo $data->surname; ?></td>
										<td><?php echo formatDate($data->own_date); ?></td>
										<td style="text-align: center"><button type="button" class="btn btn-warning" data-toggle="modal" data-target="#OwnerModal" onClick="javascript:getOwner(<?php echo $data->id; ?>);">แก้ไข</button></td>
										<td style="text-align: center"><button type="button" class="btn btn-danger" onClick="javascript: if(confirm('ยืนยันการลบ?')){jQuery(this).prop('disabled', true);deleteOwner(<?php echo $data->id; ?>);}">ลบ</button></td>
									</tr>
								<?php } ?>
							<?php } ?>
						</tbody>
					</table>
									
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>




		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'breeding_history', JText::_('ประวัติการผสมพันธ์', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">
					<div style="float:right">
						<button class="btn btn-info" data-toggle="modal" data-target="#GMPModal">เพิ่มรายการ</button>
					</div>
					<h3>ประวัติเจ้าของฟาร์ม</h3>
					<table class="table table-striped table-bordered table-blue">
						<thead>
							<tr>
								<th style="text-align:center">ที่</th>
								<th style="text-align:center">เลขบัตรประชาชน</th>
								<th style="text-align:center">ชื่อ-สกุล</th>
								<th style="text-align:center">วันที่เริ่มเป็นเจ้าของ</th>
								<th style="text-align:center">แก้ไข</th>
								<th style="text-align:center">ลบ</th>
							</tr>
						</thead>
						<tbody>
							<?php if(count($this->item->owner_history)==0){ ?>
								<tr>
									<td style="text-align:center" colspan="6">ไม่พบข้อมูล</td>
								</tr>
							<?php }else{ ?>
								<?php $i=0;
									foreach($this->item->owner_history as $key=>$data){ ?>
									<tr>
										<td><?php echo ++$i; ?></td>
										<td><?php echo $data->citizen_id; ?></td>
										<td><?php echo $data->name; ?> <?php echo $data->surname; ?></td>
										<td><?php echo formatDate($data->own_date); ?></td>
										<td style="text-align: center"><button type="button" class="btn btn-warning" data-toggle="modal" data-target="#OwnerModal" onClick="javascript:getOwner(<?php echo $data->id; ?>);">แก้ไข</button></td>
										<td style="text-align: center"><button type="button" class="btn btn-danger" onClick="javascript: if(confirm('ยืนยันการลบ?')){jQuery(this).prop('disabled', true);deleteOwner(<?php echo $data->id; ?>);}">ลบ</button></td>
									</tr>
								<?php } ?>
							<?php } ?>
						</tbody>
					</table>
									
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>



		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'child_history', JText::_('ประวัติการออกลูก', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">
					<div style="float:right">
						<button class="btn btn-info" data-toggle="modal" data-target="#GMPModal">เพิ่มรายการ</button>
					</div>
					<h3>ประวัติเจ้าของฟาร์ม</h3>
					<table class="table table-striped table-bordered table-blue">
						<thead>
							<tr>
								<th style="text-align:center">ที่</th>
								<th style="text-align:center">เลขบัตรประชาชน</th>
								<th style="text-align:center">ชื่อ-สกุล</th>
								<th style="text-align:center">วันที่เริ่มเป็นเจ้าของ</th>
								<th style="text-align:center">แก้ไข</th>
								<th style="text-align:center">ลบ</th>
							</tr>
						</thead>
						<tbody>
							<?php if(count($this->item->owner_history)==0){ ?>
								<tr>
									<td style="text-align:center" colspan="6">ไม่พบข้อมูล</td>
								</tr>
							<?php }else{ ?>
								<?php $i=0;
									foreach($this->item->owner_history as $key=>$data){ ?>
									<tr>
										<td><?php echo ++$i; ?></td>
										<td><?php echo $data->citizen_id; ?></td>
										<td><?php echo $data->name; ?> <?php echo $data->surname; ?></td>
										<td><?php echo formatDate($data->own_date); ?></td>
										<td style="text-align: center"><button type="button" class="btn btn-warning" data-toggle="modal" data-target="#OwnerModal" onClick="javascript:getOwner(<?php echo $data->id; ?>);">แก้ไข</button></td>
										<td style="text-align: center"><button type="button" class="btn btn-danger" onClick="javascript: if(confirm('ยืนยันการลบ?')){jQuery(this).prop('disabled', true);deleteOwner(<?php echo $data->id; ?>);}">ลบ</button></td>
									</tr>
								<?php } ?>
							<?php } ?>
						</tbody>
					</table>
									
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>


		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'move_out', JText::_('ประวัติการรักษา', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">
					<div style="float:right">
						<button class="btn btn-info" data-toggle="modal" data-target="#GMPModal">เพิ่มรายการ</button>
					</div>
					<h3>ประวัติเจ้าของฟาร์ม</h3>
					<table class="table table-striped table-bordered table-blue">
						<thead>
							<tr>
								<th style="text-align:center">ที่</th>
								<th style="text-align:center">เลขบัตรประชาชน</th>
								<th style="text-align:center">ชื่อ-สกุล</th>
								<th style="text-align:center">วันที่เริ่มเป็นเจ้าของ</th>
								<th style="text-align:center">แก้ไข</th>
								<th style="text-align:center">ลบ</th>
							</tr>
						</thead>
						<tbody>
							<?php if(count($this->item->owner_history)==0){ ?>
								<tr>
									<td style="text-align:center" colspan="6">ไม่พบข้อมูล</td>
								</tr>
							<?php }else{ ?>
								<?php $i=0;
									foreach($this->item->owner_history as $key=>$data){ ?>
									<tr>
										<td><?php echo ++$i; ?></td>
										<td><?php echo $data->citizen_id; ?></td>
										<td><?php echo $data->name; ?> <?php echo $data->surname; ?></td>
										<td><?php echo formatDate($data->own_date); ?></td>
										<td style="text-align: center"><button type="button" class="btn btn-warning" data-toggle="modal" data-target="#OwnerModal" onClick="javascript:getOwner(<?php echo $data->id; ?>);">แก้ไข</button></td>
										<td style="text-align: center"><button type="button" class="btn btn-danger" onClick="javascript: if(confirm('ยืนยันการลบ?')){jQuery(this).prop('disabled', true);deleteOwner(<?php echo $data->id; ?>);}">ลบ</button></td>
									</tr>
								<?php } ?>
							<?php } ?>
						</tbody>
					</table>
									
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
