<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_edairy/css/form.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	js('input:hidden.farm_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('farm_idhidden')){
			js('#jform_farm_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_farm_id").trigger("liszt:updated");
	});

	Joomla.submitbutton = function (task) {
		if (task == 'farm_grade_certify.cancel') {
			Joomla.submitform(task, document.getElementById('farm_grade_certify-form'));
		}
		else {
			
			if (task != 'farm_grade_certify.cancel' && document.formvalidator.isValid(document.id('farm_grade_certify-form'))) {
				
				Joomla.submitform(task, document.getElementById('farm_grade_certify-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_edairy&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="farm_grade_certify-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_EDAIRY_TITLE_FARM_GRADE_CERTIFY', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

									<?php echo $this->form->renderField('id'); ?>
				<?php echo $this->form->renderField('farm_id'); ?>

			<?php
				foreach((array)$this->item->farm_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="farm_id" name="jform[farm_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				
				<?php echo $this->form->renderField('certify_date'); ?>
				<?php echo $this->form->renderField('book_no'); ?>

				<h3>การจัดกลุ่ม</h3>
				<table border="1" cellpadding="5" width="600">
					<tr>
						<th>กลุ่ม</th>
						<th>รายละเอียด</th>
					</tr>
					<tr>
						<td>A</td>
						<td>รายละเอียดกลุ่ม A</td>
					</tr>
					<tr>
						<td>B</td>
						<td>รายละเอียดกลุ่ม B</td>
					</tr>
					<tr>
						<td>C</td>
						<td>รายละเอียดกลุ่ม C</td>
					</tr>
					<tr>
						<td>D</td>
						<td>รายละเอียดกลุ่ม D</td>
					</tr>
					<tr>
						<td>E</td>
						<td>รายละเอียดกลุ่ม E</td>
					</tr>
				</table>
				<br />
				<?php echo $this->form->renderField('grade'); ?>
				<?php echo $this->form->renderField('create_by'); ?>
				<?php echo $this->form->renderField('update_by'); ?>
				<?php echo $this->form->renderField('create_datetime'); ?>
				<?php echo $this->form->renderField('update_datetime'); ?>
				<?php echo $this->form->renderField('state'); ?>
				<?php echo $this->form->renderField('created_by'); ?>


					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
