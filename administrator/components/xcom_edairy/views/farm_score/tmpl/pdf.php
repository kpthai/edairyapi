<?php 

// Include the main TCPDF library (search for installation path).
require_once('../media/com_edairy/tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('EDAIRY');
$pdf->SetTitle('EDAIRY');
$pdf->SetSubject('EDAIRY');
$pdf->SetKeywords('EDAIRY');

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
// set font

//$fontname = $pdf->addTTFfont('fonts/Browa.ttf', 'TrueTypeUnicode', '', 32);
$pdf->SetFont('thsarabunpsk', '', 14, '', true);



//PAGE 3 >> PAGE 1
$pdf->AddPage();
$pdf->setPageOrientation ('P', $autopagebreak='', $bottommargin='');
// get the current page break margin
$bMargin = $pdf->getBreakMargin();
// get current auto-page-break mode
$auto_page_break = $pdf->getAutoPageBreak();
// disable auto-page-break
$pdf->SetAutoPageBreak(true, 0);
// set bacground image
/*
$heading_html = <<<EOD
<img src="images/header.png" />

EOD;

$pdf->writeHTMLCell($w=0, $h=0, $x='10', $y='5', $heading_html, $border=0, $ln=1, $fill=0, $reseth=true, $align='L', $autopadding=false);

*/

$score_data = explode("|", $this->item->assessment_data);

$sum1 = $score_data[0]+$score_data[1]+$score_data[2]+$score_data[3]+$score_data[4]+$score_data[5]+$score_data[6]+$score_data[7]+$score_data[8];

$sum2 = $score_data[9]+$score_data[10]+$score_data[11]+$score_data[12]+$score_data[13]+$score_data[14];

$sum3 = $score_data[15]+$score_data[16]+$score_data[17]+$score_data[18]+$score_data[19]+$score_data[20];

$sum4 = $score_data[21]+$score_data[22]+$score_data[23]+$score_data[24];

$sum5 = $score_data[25]+$score_data[26]+$score_data[27]+$score_data[28];

$sum6 = $score_data[29]+$score_data[30]+$score_data[31]+$score_data[32];

$score_total = $sum1 + $sum2 + $sum3 + $sum4 + $sum5 + $sum6;

		if($score_total>=90){
			$farm_score=4;
		}else if($score_total>=80){
			$farm_score=3;
		}else if($score_total>=70){
			$farm_score=2;
		}else{
			$farm_score=1;
		}

$html = <<<EOD
<div style="text-align:center;font-size:20px">
<table width="1000">
	<tr>
		<td width="80">
			<img src="../images/dpo_logo.jpg" width="80"/>
		</td>
		<td>
			<br /><br />			<b>องค์การส่งเสริมกิจการโคนมแห่งประเทศไทย (อ.ส.ค.) กระทรวงเกษตรและสหกรณ์<br />ใบคะแนนคอก</b>
		</td>
	</tr>
</table>
</div>
<br />
					<table cellpadding="5">
						<tr>
							<td width="240"><b>1. คอกรีดและคอกพักโค (20 คะแนน)</b></td>
							<td width="100">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<table border="1" width="20"><tr><td align="center">{$sum1}</td></tr></table></td>
							<td width="250"><b>3. อุปกรณ์ที่เกี่ยวข้องกับน้ำนม (24 คะแนน)</b></td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<table border="1" width="20"><tr><td align="center">{$sum3}</td></tr></table></td>
						</tr>
						<tr>
							<td>1.1 คอกรีด</td>
							<td></td>
							<td>- ภาชนะรีดนมสะอาดไม่มีกลิ่นอับ</td>
							<td>(4) <table border="1" width="20"><tr><td align="center">{$score_data[15]}</td></tr></table></td>
						</tr>
						<tr>
							<td>- คอกรีดแห้งสะอาด</td>
							<td>(3) <table border="1" width="20"><tr><td align="center">{$score_data[0]}</td></tr></table></td>
							<td>- ภาชนะบรรจุนมสะอาดไม่มีกลิ่นอับ</td>
							<td>(4) <table border="1" width="20"><tr><td align="center">{$score_data[16]}</td></tr></table></td>
						</tr>
						<tr>
							<td>- การระบายอากาศดีไม่อับทึบ</td>
							<td>(2) <table border="1" width="20"><tr><td align="center">{$score_data[1]}</td></tr></table></td>
							<td>- ล้างทำความสะอาดอุปกรณ์ถูกต้องตามขั้นตอน</td>
							<td>(4) <table border="1" width="20"><tr><td align="center">{$score_data[17]}</td></tr></table></td>
						</tr>
						<tr>
							<td>- มีห้องเก็บอาหารและอุปกรณ์ที่สะอาดเรียบร้อย</td>
							<td>(2) <table border="1" width="20"><tr><td align="center">{$score_data[2]}</td></tr></table></td>
							<td>- ล้างและเก็บถังใส่นมทันทีหลังรีดนม</td>
							<td>(4) <table border="1" width="20"><tr><td align="center">{$score_data[18]}</td></tr></table></td>
						</tr>
						<tr>
							<td>- การระบายมูลโคถูกสุขลักษณะ</td>
							<td>(2) <table border="1" width="20"><tr><td align="center">{$score_data[3]}</td></tr></table></td>
							<td>- จัดเก็บอุปกรณ์เป็นระเบียบและอยู่ในที่<br />อากาศถ่ายเทสะดวก</td>
							<td>(4) <table border="1" width="20"><tr><td align="center">{$score_data[19]}</td></tr></table></td>
						</tr>
						<tr>
							<td>- ไม่มีสัตว์เลี้ยงอื่นปะปนบริเวณคอกรีด</td>
							<td>(2) <table border="1" width="20"><tr><td align="center">{$score_data[4]}</td></tr></table></td>
							<td>- อุปกรณ์และภาชนะใส่นมต้องทำความสะอาด<br />อย่างไม่มีรอยตะเข็บ รอยต่อหรือบุบ</td>
							<td>(4) <table border="1" width="20"><tr><td align="center">{$score_data[20]}</td></tr></table></td>
						</tr>

						<tr>
							<td>1.2 คอกพัก</td>
							<td></td>
							<td><b>(4) การขนส่งและปฎิบัติต่อน้ำนม (12 คะแนน)</b></td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<table border="1" width="20"><tr><td align="center">{$sum4}</td></tr></table></td>
						</tr>
						<tr>
							<td>- คอกสะอาดกว้างขวางพอ</td>
							<td>(3) <table border="1" width="20"><tr><td align="center">{$score_data[5]}</td></tr></table></td>
							<td>- รีดนมตรงเวลาสม่ำเสมอ</td>
							<td>(3) <table border="1" width="20"><tr><td align="center">{$score_data[21]}</td></tr></table></td>
						</tr>
						<tr>
							<td>- มีน้ำสะอาดและแร่ธาตุตลอดเวลา</td>
							<td>(2) <table border="1" width="20"><tr><td align="center">{$score_data[6]}</td></tr></table></td>
							<td>- ไม่รีดนมก่อนเวลารับนมนานเกินไป</td>
							<td>(3) <table border="1" width="20"><tr><td align="center">{$score_data[22]}</td></tr></table></td>
						</tr>
						<tr>
							<td>- มีร่มเงาเพียงพอ</td>
							<td>(2) <table border="1" width="20"><tr><td align="center">{$score_data[7]}</td></tr></table></td>
							<td>- ไม่ตั้งถังนมตากแดด</td>
							<td>(3) <table border="1" width="20"><tr><td align="center">{$score_data[23]}</td></tr></table></td>
						</tr>
						<tr>
							<td>- แบ่งโคตามประเภท มีคอกลูกโคแยกต่างหาก</td>
							<td>(2) <table border="1" width="20"><tr><td align="center">{$score_data[8]}</td></tr></table></td>
							<td>- การขนส่งนมไปยังศูนย์ใช้ระยะเวลาสั้น</td>
							<td>(3) <table border="1" width="20"><tr><td align="center">{$score_data[24]}</td></tr></table></td>
						</tr>
						<tr>
							<td><b>2. การรีดนม (20 คะแนน)</b></td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<table border="1" width="20"><tr><td align="center">{$sum2}</td></tr></table></td>
							<td><b>5. ตัวโค (12 คะแนน)</b></td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<table border="1" width="20"><tr><td align="center">{$sum5}</td></tr></table></td>
						</tr>
						<tr>
							<td>- ทำความสะอาดตัวโค / แปรงขนก่อนรีด</td>
							<td>(3) <table border="1" width="20"><tr><td align="center">{$score_data[9]}</td></tr></table></td>
							<td>- โคทุกตัวในฝูงต้องผ่านการตรวจโรคประจำปี</td>
							<td>(3) <table border="1" width="20"><tr><td align="center">{$score_data[25]}</td></tr></table></td>
						</tr>
						<tr>
							<td>- ล้างและเช็ดเต้านมให้แห้งก่อนรีด</td>
							<td>(3) <table border="1" width="20"><tr><td align="center">{$score_data[10]}</td></tr></table></td>
							<td>- ฉีดวัคซีนตามโปรแกรมสม่ำเสมอ</td>
							<td>(3) <table border="1" width="20"><tr><td align="center">{$score_data[26]}</td></tr></table></td>
						</tr>
						<tr>
							<td>- ตรวจน้ำนมด้วยถ้วยตรวจนมก่อนรีด</td>
							<td>(3) <table border="1" width="20"><tr><td align="center">{$score_data[11]}</td></tr></table></td>
							<td>- โคมีสุขภาพสมบูรณ์</td>
							<td>(3) <table border="1" width="20"><tr><td align="center">{$score_data[27]}</td></tr></table></td>
						</tr>
						<tr>
							<td>- รีดนมอย่างถูกวิธี</td>
							<td>(5) <table border="1" width="20"><tr><td align="center">{$score_data[12]}</td></tr></table></td>
							<td>- โคทุกตัวมีพันธ์ประวัติ</td>
							<td>(3) <table border="1" width="20"><tr><td align="center">{$score_data[28]}</td></tr></table></td>
						</tr>
						<tr>
							<td>- ใช้น้ำยาจุ่มหัวนมทันทีหลังรีดเสร็จ</td>
							<td>(3) <table border="1" width="20"><tr><td align="center">{$score_data[13]}</td></tr></table></td>
							<td><b>(6) ตัวเกษตรกร (12 คะแนน)</b></td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<table border="1" width="20"><tr><td align="center">{$sum6}</td></tr></table></td>
						</tr>
						<tr>
							<td>- ใช้น้ำยา CMT ตรวจสุขภาพเต้านม</td>
							<td>(3) <table border="1" width="20"><tr><td align="center">{$score_data[14]}</td></tr></table></td>
							<td>- การแต่งกายขณะรีดนม</td>
							<td>(3) <table border="1" width="20"><tr><td align="center">{$score_data[29]}</td></tr></table></td>
						</tr>
						<tr>
							<td colspan="2" rowspan="3">
								
							</td>
							<td>- ให้ความร่วมมือและปฎิบัติตามคำแนะนำ</td>
							<td>(3) <table border="1" width="20"><tr><td align="center">{$score_data[30]}</td></tr></table></td>
						</tr>
						<tr>
							<td>- มีความสนใจกระตือรือร้น ใฝ่รู้</td>
							<td>(3) <table border="1" width="20"><tr><td align="center">{$score_data[31]}</td></tr></table></td>
						</tr>
						<tr>
							<td>- มีการจดบันทึกประจำฟาร์ม</td>
							<td>(3) <table border="1" width="20"><tr><td align="center">{$score_data[32]}</td></tr></table></td>
						</tr>
					</table>
					
				<hr />
					<b>คะแนนเต็ม 100 คะแนน คะแนนที่ได้ {$score_total} คะแนน &nbsp;&nbsp;&nbsp; คะแนนคอก {$farm_score}
				<hr />

				<b>เกณฑ์คะแนนคอก</b>
								<table>
									<tr>
										<td>(4) 90 คะแนนขึ้นไป = ดีเลิศ</td>
										<td>เพิ่มราคา = 0.15 บาท/กก.</td>
									</tr>
									<tr>
										<td>(3) 80 คะแนนขึ้นไป = ดีมาก</td>
										<td>เพิ่มราคา = 0.10 บาท/กก.</td>
									</tr>
									<tr>
										<td>(2) 70 คะแนนขึ้นไป = ดี</td>
										<td>เพิ่มราคา = 0.05 บาท/กก.</td>
									</tr>
									<tr>
										<td>(1) ต่ำกว่า 70 คะแนนขึ้นไป = ไม่ดี</td>
										<td>เพิ่มราคา = 0.00 บาท/กก.</td>
									</tr>
								</table>
EOD;




// print a block of text using Write()
$pdf->writeHTMLCell($w=0, $h=0, $x='10', $y='10', $html, $border=0, $ln=1, $fill=0, $reseth=true, $align='L', $autopadding=false);

 






// set bacground image

// if (isset($_POST['download'])) {
// 	$pdf->Output("detail_id{$_POST[id]}.pdf", 'D');
// }else{
	$pdf->Output("Farm_Score_{$_GET[id]}.pdf", 'I');
// }
die();
