<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Edairy helper.
 *
 * @since  1.6
 */
class EdairyHelpersEdairyadmin
{
	/**
	 * Configure the Linkbar.
	 *
	 * @param   string  $vName  string
	 *
	 * @return void
	 */
	public static function addSubmenu($vName = '')
	{


JHtmlSidebar::addEntry('<b>เมนูหลัก</b> <hr class="sidebar-hr"/>'
		);

JHtmlSidebar::addEntry(
			'<i class="fa fa-users fa-md"></i> ' . JText::_('COM_EDAIRY_TITLE_COOPS'),
			'index.php?option=com_edairy&view=coops',
			$vName == 'coops'
		);

/*
JHtmlSidebar::addEntry(
			'<i class="fa fa-users fa-md"></i> ' . JText::_('COM_EDAIRY_TITLE_COOP_GMP_CERTIFYS'),
			'index.php?option=com_edairy&view=coop_gmp_certifys',
			$vName == 'coop_gmp_certifys'
		);

JHtmlSidebar::addEntry(
			'<i class="fa fa-users fa-md"></i> ' . JText::_('COM_EDAIRY_TITLE_COOP_SEND_MILK_COMPANYS'),
			'index.php?option=com_edairy&view=coop_send_milk_companys',
			$vName == 'coop_send_milk_companys'
		);

JHtmlSidebar::addEntry(
			JText::_('COM_EDAIRY_TITLE_MOUS'),
			'index.php?option=com_edairy&view=mous',
			$vName == 'mous'
		);





JHtmlSidebar::addEntry(
			'<i class="fa fa-home fa-md"></i> ' . JText::_('COM_EDAIRY_TITLE_FARMS'),
			'index.php?option=com_edairy&view=farms',
			$vName == 'farms'
		);


JHtmlSidebar::addEntry(
			JText::_('COM_EDAIRY_TITLE_GAP_CERTIFYS'),
			'index.php?option=com_edairy&view=gap_certifys',
			$vName == 'gap_certifys'
		);


*/


JHtmlSidebar::addEntry(
			'<i class="fa fa-user-circle-o fa-md"></i> ' . "เกษตรกร/ฟาร์ม",
			'index.php?option=com_edairy&view=farms',
			$vName == 'farms'
		);


/*
JHtmlSidebar::addEntry(
			JText::_('COM_EDAIRY_TITLE_FARMER_TRAININGS'),
			'index.php?option=com_edairy&view=farmer_trainings',
			$vName == 'farmer_trainings'
		);


*/
JHtmlSidebar::addEntry(
			'<i class="fa fa-id-card fa-md"></i> ' . JText::_('ทะเบียนโค'),
			'index.php?option=com_edairy&view=cows',
			$vName == 'cows'
		);

JHtmlSidebar::addEntry(
			'<i class="fa fa-check-square-o fa-md"></i> ' . JText::_('คะแนนคอก'),
			'index.php?option=com_edairy&view=farm_scores',
			$vName == 'farm_scores'
		);
JHtmlSidebar::addEntry(
			'<i class="fa fa-check-square fa-md"></i> ' . JText::_('การจัดกลุ่มฟาร์ม'),
			'index.php?option=com_edairy&view=farm_grade_certifys',
			$vName == 'farm_grade_certifys'
		);
/*
JHtmlSidebar::addEntry(
			JText::_('COM_EDAIRY_TITLE_MOVE_OUTS'),
			'index.php?option=com_edairy&view=move_outs',
			$vName == 'move_outs'
		);
*/
		/*
JHtmlSidebar::addEntry(
			'<i class="fa fa-heart fa-md"></i> ' . JText::_('COM_EDAIRY_TITLE_BREEDINGS'),
			'index.php?option=com_edairy&view=breedings',
			$vName == 'breedings'
		);
*/

		JHtmlSidebar::addEntry('<br /><b>ข้อมูลพื้นฐาน</b> <hr class="sidebar-hr"/>'
		);

		JHtmlSidebar::addEntry(
			'<i class="fa fa-map fa-md"></i> ' . JText::_('COM_EDAIRY_TITLE_REGIONS'),
			'index.php?option=com_edairy&view=regions',
			$vName == 'regions'
		);

JHtmlSidebar::addEntry(
			'<i class="fa fa-map-signs fa-md"></i> ' . JText::_('COM_EDAIRY_TITLE_PROVINCES'),
			'index.php?option=com_edairy&view=provinces',
			$vName == 'provinces'
		);

JHtmlSidebar::addEntry(
			'<i class="fa fa-map-pin fa-md"></i> ' .  JText::_('COM_EDAIRY_TITLE_DISTRICTS'),
			'index.php?option=com_edairy&view=districts',
			$vName == 'districts'
		);

JHtmlSidebar::addEntry(
			'<i class="fa fa-location-arrow fa-md"></i> ' . JText::_('COM_EDAIRY_TITLE_SUB_DISTRICTS'),
			'index.php?option=com_edairy&view=sub_districts',
			$vName == 'sub_districts'
		);

JHtmlSidebar::addEntry(
			'<i class="fa fa-share-alt fa-md"></i> ' . JText::_('COM_EDAIRY_TITLE_COW_PROJECTS'),
			'index.php?option=com_edairy&view=cow_projects',
			$vName == 'cow_projects'
		);

JHtmlSidebar::addEntry(
			'<i class="fa fa-industry fa-md"></i> ' . JText::_('COM_EDAIRY_TITLE_FARM_CONDITIONS'),
			'index.php?option=com_edairy&view=farm_conditions',
			$vName == 'farm_conditions'
		);

JHtmlSidebar::addEntry(
			'<i class="fa fa-bar-chart fa-md"></i> ' . JText::_('COM_EDAIRY_TITLE_PERFORMANCE_IMPROVEMENT_PROJECTS'),
			'index.php?option=com_edairy&view=performance_improvement_projects',
			$vName == 'performance_improvement_projects'
		);

JHtmlSidebar::addEntry(
			'<i class="fa fa-thermometer-three-quarters fa-md"></i> ' . JText::_('COM_EDAIRY_TITLE_MILKING_SYSTEMS'),
			'index.php?option=com_edairy&view=milking_systems',
			$vName == 'milking_systems'
		);

JHtmlSidebar::addEntry(
			'<i class="fa fa-institution fa-md"></i> ' . JText::_('COM_EDAIRY_TITLE_FARM_GRADE_ANNOUNCEMENTS'),
			'index.php?option=com_edairy&view=farm_grade_announcements',
			$vName == 'farm_grade_announcements'
		);

JHtmlSidebar::addEntry(
			'<i class="fa fa-info-circle fa-md"></i> ' .JText::_('COM_EDAIRY_TITLE_COW_STATUSES'),
			'index.php?option=com_edairy&view=cow_statuses',
			$vName == 'cow_statuses'
		);

JHtmlSidebar::addEntry(
			'<i class="fa fa-calendar fa-md"></i> ' . JText::_('COM_EDAIRY_TITLE_TRAINING_PROGRAMS'),
			'index.php?option=com_edairy&view=training_programs',
			$vName == 'training_programs'
		);

	}

	/**
	 * Gets the files attached to an item
	 *
	 * @param   int     $pk     The item's id
	 *
	 * @param   string  $table  The table's name
	 *
	 * @param   string  $field  The field's name
	 *
	 * @return  array  The files
	 */
	public static function getFiles($pk, $table, $field)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query
			->select($field)
			->from($table)
			->where('id = ' . (int) $pk);

		$db->setQuery($query);

		return explode(',', $db->loadResult());
	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @return    JObject
	 *
	 * @since    1.6
	 */
	public static function getActions()
	{
		$user   = JFactory::getUser();
		$result = new JObject;

		$assetName = 'com_edairy';

		$actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
		);

		foreach ($actions as $action)
		{
			$result->set($action, $user->authorise($action, $assetName));
		}

		return $result;
	}
}
