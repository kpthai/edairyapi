<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Edairy helper.
 *
 * @since  1.6
 */
class EdairyHelper
{
	/**
	 * Configure the Linkbar.
	 *
	 * @param   string  $vName  string
	 *
	 * @return void
	 */
	public static function addSubmenu($vName = '')
	{
		
		$user = JFactory::getUser();

		$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
				->select("*")
				->from('#__ed_user_role');
			$db->setQuery($query);
			$role_table = $db->loadObjectList();


JHtmlSidebar::addEntry('<b>เมนูหลัก</b> <hr class="sidebar-hr"/>');

$is_show = false;
foreach ($role_table as $key => $value) {
	if($value->view_name=="coops"){
		$role_row = $value->{"role" . $user->role};
		$role_array = explode(",", $role_row);
		if(in_array("R", $role_array)){
			$is_show = true;
			break;
		}
	}
}

if($is_show){
	JHtmlSidebar::addEntry(
				'<i class="fa fa-users fa-md"></i> ' . JText::_('COM_EDAIRY_TITLE_COOPS'),
				'index.php?option=com_edairy&view=coops',
				$vName == 'coops'
			);
}

if($_REQUEST["view"]=="farms" && $_REQUEST["layout"]!="farm_survey"){
	$a = "active";
}
JHtmlSidebar::addEntry("<li class='{$a}'><a href='index.php?option=com_edairy&amp;view=farms'><i class='fa fa-user-circle-o fa-md'></i> เกษตรกร/ฟาร์ม</a></li>");
/*
JHtmlSidebar::addEntry(
			'<i class="fa fa-user-circle-o fa-md"></i> ' . "เกษตรกร/ฟาร์ม",
			'index.php?option=com_edairy&view=farms',
			$vName == 'farms'
		);
*/
$is_show = false;
foreach ($role_table as $key => $value) {
	if($value->view_name=="cows"){
		$role_row = $value->{"role" . $user->role};
		$role_array = explode(",", $role_row);
		if(in_array("R", $role_array)){
			$is_show = true;
			break;
		}
	}
}

if($is_show){
JHtmlSidebar::addEntry(
			'<i class="fa fa-id-card fa-md"></i> ' . JText::_('ทะเบียนโค'),
			'index.php?option=com_edairy&view=cows',
			$vName == 'cows'
		);
}











$is_show = false;
foreach ($role_table as $key => $value) {
	if($value->view_name=="farm_scores"){
		$role_row = $value->{"role" . $user->role};
		$role_array = explode(",", $role_row);
		if(in_array("R", $role_array)){
			$is_show = true;
			break;
		}
	}
}

if($is_show){


JHtmlSidebar::addEntry("<li><a href='javascript:jQuery(\".sidebar-sub-2\").toggleClass(\"in-sub\");'><i class='fa fa-check-square-o fa-md'></i> การประเมินฟาร์ม <i class='fa fa-chevron-circle-down fa-md'></i></a></li>");

$a = "";
if($_REQUEST["view"]=="farm_scores"){
	$a = "active";
}
JHtmlSidebar::addEntry("<li class='sidebar-sub-2 {$a}'><a href='index.php?option=com_edairy&amp;view=farm_scores'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='fa fa-chevron-circle-right fa-md'></i>คะแนนคอก</a></li>");
$a = "";
if($_REQUEST["view"]=="farm_grade_certifys"){
	$a = "active";
}
JHtmlSidebar::addEntry("<li class='sidebar-sub-2 {$a}'><a href='index.php?option=com_edairy&amp;view=farm_grade_certifys'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='fa fa-chevron-circle-right fa-md'></i>การจัดกลุ่มฟาร์ม</a></li>");
$a = "";

$farmAssessmentViews = array("farm_scores", "farm_grade_certifys");
if(in_array($_REQUEST["view"], $farmAssessmentViews)){
	JHtmlSidebar::addEntry("<script type='text/javascript'>jQuery(\".sidebar-sub-2\").toggleClass(\"in-sub\");</script>");
}


}










$is_show = false;
foreach ($role_table as $key => $value) {
	if($value->view_name=="cow_statistics"){
		$role_row = $value->{"role" . $user->role};
		$role_array = explode(",", $role_row);
		if(in_array("R", $role_array)){
			$is_show = true;
			break;
		}
	}
}

if($is_show){


JHtmlSidebar::addEntry("<li><a href='javascript:jQuery(\".sidebar-sub-4\").toggleClass(\"in-sub\");'><i class='fa fa-search fa-md'></i> การสำรวจโค <i class='fa fa-chevron-circle-down fa-md'></i></a></li>");

$a = "";
if($_REQUEST["view"]=="cow_statistics"){
	$a = "active";
}
JHtmlSidebar::addEntry("<li class='sidebar-sub-4 {$a}'><a href='index.php?option=com_edairy&amp;view=cow_statistics'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='fa fa-chevron-circle-right fa-md'></i>สำรวจประชากรโค</a></li>");
$a = "";
if($_REQUEST["view"]=="farms" && $_REQUEST["layout"]=="farm_survey"){
	$a = "active";
}
JHtmlSidebar::addEntry("<li class='sidebar-sub-4 {$a}'><a href='index.php?option=com_edairy&amp;view=farms&layout=farm_survey'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='fa fa-chevron-circle-right fa-md'></i>สำรวจสถานภาพโค</a></li>");
$a = "";

$healthServiceViews = array("cow_statistics");
if(in_array($_REQUEST["view"], $healthServiceViews)){
	JHtmlSidebar::addEntry("<script type='text/javascript'>jQuery(\".sidebar-sub-4\").toggleClass(\"in-sub\");</script>");
}


if($_REQUEST["view"]=="farms" && ($_REQUEST["layout"]=="farm_survey" || $_REQUEST["layout"]=="edit_farm_survey")){
	JHtmlSidebar::addEntry("<script type='text/javascript'>jQuery(\".sidebar-sub-4\").toggleClass(\"in-sub\");</script>");
}

}





JHtmlSidebar::addEntry("<li><a href='javascript:jQuery(\".sidebar-sub-1\").toggleClass(\"in-sub\");'><i class='fa fa-venus-mars fa-md'></i> การผสมเทียม <i class='fa fa-chevron-circle-down fa-md'></i></a></li>");
$a = "";
if($_REQUEST["view"]=="inseminations"){
	$a = "active";
}
JHtmlSidebar::addEntry("<li class='sidebar-sub-1 {$a}'><a href='index.php?option=com_edairy&amp;view=inseminations'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='fa fa-chevron-circle-right fa-md'></i>บริการผสมเทียม</a></li>");


$a = "";
if($_REQUEST["view"]=="pregnant_inspections"){
	$a = "active";
}
JHtmlSidebar::addEntry("<li class='sidebar-sub-1 {$a}'><a href='index.php?option=com_edairy&amp;view=pregnant_inspections'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='fa fa-chevron-circle-right fa-md'></i>บันทึกการตรวจท้อง</a></li>");
$a = "";
if($_REQUEST["view"]=="stop_milkings"){
	$a = "active";
}
JHtmlSidebar::addEntry("<li class='sidebar-sub-1 {$a}'><a href='index.php?option=com_edairy&amp;view=stop_milkings'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='fa fa-chevron-circle-right fa-md'></i>บันทึกการหยุดรีดนม</a></li>");
$a = "";
if($_REQUEST["view"]=="birth_inspections"){
	$a = "active";
}
JHtmlSidebar::addEntry("<li class='sidebar-sub-1 {$a}'><a href='index.php?option=com_edairy&amp;view=birth_inspections'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='fa fa-chevron-circle-right fa-md'></i>บันทึกการคลอด</a></li>");

$inseminationsViews = array("inseminations", "pregnant_inspections", "stop_milkings", "birth_inspections");
if(in_array($_REQUEST["view"], $inseminationsViews)){
	JHtmlSidebar::addEntry("<script type='text/javascript'>jQuery(\".sidebar-sub-1\").toggleClass(\"in-sub\");</script>");
}





JHtmlSidebar::addEntry("<li><a href='javascript:jQuery(\".sidebar-sub-3\").toggleClass(\"in-sub\");'><i class='fa fa-ambulance fa-md'></i> การบริการสัตวแพทย์ <i class='fa fa-chevron-circle-down fa-md'></i></a></li>");

$a = "";
if($_REQUEST["view"]=="healths"){
	$a = "active";
}
JHtmlSidebar::addEntry("<li class='sidebar-sub-3 {$a}'><a href='index.php?option=com_edairy&amp;view=healths'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='fa fa-chevron-circle-right fa-md'></i>บริการสัตวแพทย์</a></li>");
$a = "";
if($_REQUEST["view"]=="heal_results"){
	$a = "active";
}
JHtmlSidebar::addEntry("<li class='sidebar-sub-3 {$a}'><a href='index.php?option=com_edairy&amp;view=heal_results'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='fa fa-chevron-circle-right fa-md'></i>บันทึกผลการรักษา</a></li>");
$a = "";

$healthServiceViews = array("healths", "heal_results");
if(in_array($_REQUEST["view"], $healthServiceViews)){
	JHtmlSidebar::addEntry("<script type='text/javascript'>jQuery(\".sidebar-sub-3\").toggleClass(\"in-sub\");</script>");
}





		JHtmlSidebar::addEntry("<li><a href='javascript:jQuery(\".sidebar-sub-5\").toggleClass(\"in-sub\");'><i class='fa fa-truck fa-md'></i> การรับส่งน้ำนมดิบ <i class='fa fa-chevron-circle-down fa-md'></i></a></li>");

$a = "";
if($_REQUEST["view"]=="receive_milk_quantity_cows"){
	$a = "active";
}
JHtmlSidebar::addEntry("<li class='sidebar-sub-5 {$a}'><a href='index.php?option=com_edairy&amp;view=receive_milk_quantity_cows'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='fa fa-chevron-circle-right fa-md'></i>ปริมาณน้ำนมโคแต่ละตัว</a></li>");
$a = "";
if($_REQUEST["view"]=="receive_milk_quantities" && $_REQUEST["layout"]!="report"){
	$a = "active";
}
JHtmlSidebar::addEntry("<li class='sidebar-sub-5 {$a}'><a href='index.php?option=com_edairy&amp;view=receive_milk_quantities'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='fa fa-chevron-circle-right fa-md'></i>การรับน้ำนมดิบ</a></li>");
$a = "";

if($_REQUEST["view"]=="send_milk_quantities"){
	$a = "active";
}
JHtmlSidebar::addEntry("<li class='sidebar-sub-5 {$a}'><a href='index.php?option=com_edairy&amp;view=send_milk_quantities'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='fa fa-chevron-circle-right fa-md'></i>การส่งน้ำนมดิบ</a></li>");
$a = "";

if($_REQUEST["view"]=="receive_milk_quantities" && $_REQUEST["layout"]=="report"){
	$a = "active";
}
JHtmlSidebar::addEntry("<li class='sidebar-sub-5 {$a}'><a href='index.php?option=com_edairy&amp;view=receive_milk_quantities&layout=report'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='fa fa-chevron-circle-right fa-md'></i>ใบรับน้ำนมดิบ</a></li>");
$a = "";

$milkViews = array("receive_milk_quantity_cows", "receive_milk_quantities", "send_milk_quantities", "receive_milk_reports");
if(in_array($_REQUEST["view"], $milkViews)){
	JHtmlSidebar::addEntry("<script type='text/javascript'>jQuery(\".sidebar-sub-5\").toggleClass(\"in-sub\");</script>");
}





JHtmlSidebar::addEntry("<li><a href='javascript:jQuery(\".sidebar-sub-6\").toggleClass(\"in-sub\");'><i class='fa fa-filter fa-md'></i> คุณภาพน้ำนมดิบ <i class='fa fa-chevron-circle-down fa-md'></i></a></li>");

$a = "";
if($_REQUEST["view"]=="milk_quality_headers"){
	$a = "active";
}
JHtmlSidebar::addEntry("<li class='sidebar-sub-6 {$a}'><a href='index.php?option=com_edairy&amp;view=milk_quality_headers'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='fa fa-chevron-circle-right fa-md'></i>รายเกษตรกร</a></li>");
$a = "";
if($_REQUEST["view"]=="milk_coop_qualities"){
	$a = "active";
}
JHtmlSidebar::addEntry("<li class='sidebar-sub-6 {$a}'><a href='index.php?option=com_edairy&amp;view=milk_coop_qualities'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='fa fa-chevron-circle-right fa-md'></i>รายสหกรณ์</a></li>");
$a = "";
if($_REQUEST["view"]=="milk_quality_transactions"){
	$a = "active";
}
JHtmlSidebar::addEntry("<li class='sidebar-sub-6 {$a}'><a href='index.php?option=com_edairy&amp;view=milk_quality_transactions'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='fa fa-chevron-circle-right fa-md'></i>ใบคุณภาพน้ำนมดิบเกษตกร</a></li>");
$a = "";

$milkQualityViews = array("milk_quality_headers", "milk_coop_qualities", "milk_quality_transactions");
if(in_array($_REQUEST["view"], $milkQualityViews)){
	JHtmlSidebar::addEntry("<script type='text/javascript'>jQuery(\".sidebar-sub-6\").toggleClass(\"in-sub\");</script>");
}



JHtmlSidebar::addEntry("<li><a href='javascript:jQuery(\".sidebar-sub-7\").toggleClass(\"in-sub\");'><i class='fa fa-cart-plus fa-md'></i> การคิดราคาน้ำนม <i class='fa fa-chevron-circle-down fa-md'></i></a></li>");

$a = "";
if($_REQUEST["view"]=="milk_payment_headers"){
	$a = "active";
}
JHtmlSidebar::addEntry("<li class='sidebar-sub-7 {$a}'><a href='index.php?option=com_edairy&amp;view=milk_payment_headers'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='fa fa-chevron-circle-right fa-md'></i>รายเกษตรกร</a></li>");
$a = "";
if($_REQUEST["view"]=="milk_coop_payment_headers"){
	$a = "active";
}
JHtmlSidebar::addEntry("<li class='sidebar-sub-7 {$a}'><a href='index.php?option=com_edairy&amp;view=milk_coop_payment_headers'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='fa fa-chevron-circle-right fa-md'></i>รายสหกรณ์</a></li>");
$a = "";

$milkPaymentViews = array("milk_payment_headers", "milk_coop_payment_headers");
if(in_array($_REQUEST["view"], $milkPaymentViews)){
	JHtmlSidebar::addEntry("<script type='text/javascript'>jQuery(\".sidebar-sub-7\").toggleClass(\"in-sub\");</script>");
}




JHtmlSidebar::addEntry(
			"<i class='fa fa-money fa-md'></i> " . JText::_('รายได้-ค่าใช้จ่าย'),
			'index.php?option=com_edairy&view=farmer_payments',
			$vName == 'farmer_payments'
		);

$reports_active = "";
if($_REQUEST["view"]=="reports" && $_REQUEST["layout"]!="dashboard"){
	$reports_active = " class='active'";
}
JHtmlSidebar::addEntry("<li {$reports_active}><a href='index.php?option=com_edairy&view=reports'><i class='fa fa-pie-chart fa-md'></i> รายงาน </a></li>");

$reports_active = "";
if($_REQUEST["view"]=="reports" && $_REQUEST["layout"]=="dashboard"){
	$reports_active = " class='active'";
}
JHtmlSidebar::addEntry("<li {$reports_active}><a href='index.php?option=com_edairy&view=reports&layout=dashboard'><i class='fa fa-pie-chart fa-md'></i> แดชบอร์ด </a></li>");
/*
JHtmlSidebar::addEntry(
			"<i class='fa fa-pie-chart fa-md'></i> " . JText::_('รายงาน'),
			'index.php?option=com_edairy&view=reports',
			$vName == 'reports'
		);*/

JHtmlSidebar::addEntry(
			"<i class='fa fa-rss fa-md'></i> " . JText::_('การแจ้งเตือน'),
			'index.php?option=com_edairy&view=reports',
			$vName == 'notifications'
		);


if($user->role>=6){

JHtmlSidebar::addEntry(
			'<i class="fa fa-user-circle-o fa-md"></i> ' . "จัดการผู้ใช้งาน",
			'index.php?option=com_users',
			$vName == 'user'
		);
}





	
		JHtmlSidebar::addEntry('<br /><b>ข้อมูลพื้นฐาน</b> <hr class="sidebar-hr"/>'
		);

if($user->role>=6){
		JHtmlSidebar::addEntry(
			'<i class="fa fa-map fa-md"></i> ' . JText::_('COM_EDAIRY_TITLE_REGIONS'),
			'index.php?option=com_edairy&view=regions',
			$vName == 'regions'
		);

JHtmlSidebar::addEntry(
			'<i class="fa fa-map-signs fa-md"></i> ' . JText::_('COM_EDAIRY_TITLE_PROVINCES'),
			'index.php?option=com_edairy&view=provinces',
			$vName == 'provinces'
		);

JHtmlSidebar::addEntry(
			'<i class="fa fa-map-pin fa-md"></i> ' .  JText::_('COM_EDAIRY_TITLE_DISTRICTS'),
			'index.php?option=com_edairy&view=districts',
			$vName == 'districts'
		);

JHtmlSidebar::addEntry(
			'<i class="fa fa-location-arrow fa-md"></i> ' . JText::_('COM_EDAIRY_TITLE_SUB_DISTRICTS'),
			'index.php?option=com_edairy&view=sub_districts',
			$vName == 'sub_districts'
		);
}
JHtmlSidebar::addEntry(
			'<i class="fa fa-share-alt fa-md"></i> ' . JText::_('COM_EDAIRY_TITLE_COW_PROJECTS'),
			'index.php?option=com_edairy&view=cow_projects',
			$vName == 'cow_projects'
		);

JHtmlSidebar::addEntry(
			'<i class="fa fa-industry fa-md"></i> ' . JText::_('COM_EDAIRY_TITLE_FARM_CONDITIONS'),
			'index.php?option=com_edairy&view=farm_conditions',
			$vName == 'farm_conditions'
		);
	


JHtmlSidebar::addEntry(
			'<i class="fa fa-bar-chart fa-md"></i> ' . JText::_('COM_EDAIRY_TITLE_PERFORMANCE_IMPROVEMENT_PROJECTS'),
			'index.php?option=com_edairy&view=performance_improvement_projects',
			$vName == 'performance_improvement_projects'
		);

JHtmlSidebar::addEntry(
			'<i class="fa fa-thermometer-three-quarters fa-md"></i> ' . JText::_('COM_EDAIRY_TITLE_MILKING_SYSTEMS'),
			'index.php?option=com_edairy&view=milking_systems',
			$vName == 'milking_systems'
		);

JHtmlSidebar::addEntry(
			'<i class="fa fa-institution fa-md"></i> ' . JText::_('COM_EDAIRY_TITLE_FARM_GRADE_ANNOUNCEMENTS'),
			'index.php?option=com_edairy&view=farm_grade_announcements',
			$vName == 'farm_grade_announcements'
		);

JHtmlSidebar::addEntry(
			'<i class="fa fa-map fa-md"></i> ' . JText::_('เกณฑ์การคิดราคาน้ำนม'),
			'index.php?option=com_edairy&view=milk_quality_parameters',
			$vName == 'milk_quality_parameters'
		);

JHtmlSidebar::addEntry(
			'<i class="fa fa-gear fa-md"></i> ' .JText::_('COM_EDAIRY_TITLE_COW_STATUSES'),
			'index.php?option=com_edairy&view=cow_statuses',
			$vName == 'cow_statuses'
		);

JHtmlSidebar::addEntry(
			'<i class="fa fa-calendar fa-md"></i> ' . JText::_('COM_EDAIRY_TITLE_TRAINING_PROGRAMS'),
			'index.php?option=com_edairy&view=training_programs',
			$vName == 'training_programs'
		);

JHtmlSidebar::addEntry(
			"<i class='fa fa-stethoscope fa-md'></i> " . JText::_('COM_EDAIRY_TITLE_HEALTH_SERVICES'),
			'index.php?option=com_edairy&view=health_services',
			$vName == 'health_services'
		);

JHtmlSidebar::addEntry(
			"<i class='fa fa-plus-square fa-md'></i> " . JText::_('COM_EDAIRY_TITLE_HEALTH_SUB_SERVICES'),
			'index.php?option=com_edairy&view=health_sub_services',
			$vName == 'health_sub_services'
		);

JHtmlSidebar::addEntry(
			"<i class='fa fa-medkit fa-md'></i> " . JText::_('COM_EDAIRY_TITLE_MEDICINES'),
			'index.php?option=com_edairy&view=medicines',
			$vName == 'medicines'
		);

JHtmlSidebar::addEntry(
			"<i class='fa fa-share-alt fa-md'></i> " . JText::_('COM_EDAIRY_TITLE_BREEDERS'),
			'index.php?option=com_edairy&view=breeders',
			$vName == 'breeders'
		);

JHtmlSidebar::addEntry(
			"<i class='fa fa-paper-plane fa-md'></i> " . JText::_('COM_EDAIRY_TITLE_MOVE_OUT_OBJECTIVES'),
			'index.php?option=com_edairy&view=move_out_objectives',
			$vName == 'move_out_objectives'
		);

JHtmlSidebar::addEntry(
			"<i class='fa fa-paper-plane-o fa-md'></i> " . JText::_('COM_EDAIRY_TITLE_MOVE_OUT_REASONS'),
			'index.php?option=com_edairy&view=move_out_reasons',
			$vName == 'move_out_reasons'
		);

		JHtmlSidebar::addEntry('<br /><br />');
		
	}

	/**
	 * Gets the files attached to an item
	 *
	 * @param   int     $pk     The item's id
	 *
	 * @param   string  $table  The table's name
	 *
	 * @param   string  $field  The field's name
	 *
	 * @return  array  The files
	 */
	public static function getFiles($pk, $table, $field)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query
			->select($field)
			->from($table)
			->where('id = ' . (int) $pk);

		$db->setQuery($query);

		return explode(',', $db->loadResult());
	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @return    JObject
	 *
	 * @since    1.6
	 */
	public static function getActions()
	{
		$user   = JFactory::getUser();
		$result = new JObject;

		$assetName = 'com_edairy';

		$actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
		);

		foreach ($actions as $action)
		{
			$result->set($action, $user->authorise($action, $assetName));
		}

		return $result;
	}
}

