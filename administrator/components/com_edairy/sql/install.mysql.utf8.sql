CREATE TABLE IF NOT EXISTS `#__ed_cow_insemination` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`run_no` VARCHAR(100)  NOT NULL ,
`create_date` DATE NOT NULL ,
`address` VARCHAR(300)  NOT NULL ,
`payment_type` VARCHAR(255)  NOT NULL ,
`user_id1` INT(11)  NOT NULL ,
`user_id2` INT(11)  NOT NULL ,
`grand_total` DECIMAL(10,2)  NOT NULL ,
`farm_id` INT NOT NULL ,
`farmer_id` INT NOT NULL ,
`coop_id` INT NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_cow_insemination_line` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`cow_id` INT NOT NULL ,
`create_date` DATE NOT NULL ,
`men_interval` INT NOT NULL ,
`insemination_time` INT(1)  NOT NULL ,
`lot_no` VARCHAR(200)  NOT NULL ,
`amount` INT NOT NULL ,
`price` DECIMAL(10,2)  NOT NULL ,
`total_price` DECIMAL(10,2)  NOT NULL ,
`cow_insemination_id` INT NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_receive_milk_quantity` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`run_no` VARCHAR(100)  NOT NULL ,
`create_datetime` DATE NOT NULL ,
`round` VARCHAR(255)  NOT NULL ,
`total_amount` DECIMAL(10,2)  NOT NULL ,
`coop_id` INT NOT NULL ,
`farmer_id` INT NOT NULL ,
`receiver_id` INT(11)  NOT NULL ,
`remark` VARCHAR(300)  NOT NULL ,
`denied_amount` INT NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_receive_milk_quantity_line` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`receive_milk_quantity_id` INT NOT NULL ,
`amount` DECIMAL(10,2)  NOT NULL ,
`farmer_id` INT NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_receive_milk_quantity_cow` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`create_date` DATE NOT NULL ,
`cow_id` INT NOT NULL ,
`morning_amount` DECIMAL(10,2)  NOT NULL ,
`evening_amount` DECIMAL(10,2)  NOT NULL ,
`total_amount` DECIMAL(10,2)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_send_milk_quantity` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`run_no` VARCHAR(100)  NOT NULL ,
`create_date` DATE NOT NULL ,
`coop_id` INT NOT NULL ,
`leave_time` TIME NOT NULL ,
`license_plate` VARCHAR(100)  NOT NULL ,
`driver_name` VARCHAR(200)  NOT NULL ,
`litre_amount` DECIMAL(10,2)  NOT NULL ,
`kg_amount` DECIMAL(10,2)  NOT NULL ,
`temperature` DECIMAL(10,2)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_cow_statistic` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`create_date` DATE NOT NULL ,
`farmer_id` INT NOT NULL ,
`farm_id` INT NOT NULL ,
`total1` INT NOT NULL ,
`total2` INT NOT NULL ,
`total3` INT NOT NULL ,
`total4` INT NOT NULL ,
`total5` INT NOT NULL ,
`total6` INT NOT NULL ,
`total7` INT NOT NULL ,
`total8` INT NOT NULL ,
`total9` INT NOT NULL ,
`total10` INT NOT NULL ,
`total11` INT NOT NULL ,
`total12` INT NOT NULL ,
`total13` INT NOT NULL ,
`total14` INT NOT NULL ,
`total15` INT NOT NULL ,
`total_female` INT NOT NULL ,
`total_male` INT NOT NULL ,
`total_cow` INT NOT NULL ,
`remark` VARCHAR(300)  NOT NULL ,
`create_by` INT NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_cow_birth_inspection` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`cow_id` INT NOT NULL ,
`birth_date` DATE NOT NULL ,
`birth_method` VARCHAR(255)  NOT NULL ,
`is_child_alive` VARCHAR(255)  NOT NULL ,
`child_gender` VARCHAR(255)  NOT NULL ,
`child_weight` DECIMAL(10,2)  NOT NULL ,
`child_cow_id` INT NOT NULL ,
`create_by` INT(11)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_cow_pregnant_inspection` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`cow_id` INT NOT NULL ,
`create_date` DATE NOT NULL ,
`inspection_result` VARCHAR(255)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_cow_medicine` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` VARCHAR(200)  NOT NULL ,
`unit` VARCHAR(50)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_cow_health` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`run_no` VARCHAR(100)  NOT NULL ,
`create_date` DATE NOT NULL ,
`address` VARCHAR(300)  NOT NULL ,
`payment_type` VARCHAR(255)  NOT NULL ,
`user_id1` INT(11)  NOT NULL ,
`user_id2` INT(11)  NOT NULL ,
`grand_total` DECIMAL(10,2)  NOT NULL ,
`farm_id` INT NOT NULL ,
`farmer_id` INT NOT NULL ,
`coop_id` INT NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_cow_health_service` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` VARCHAR(200)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_cow_health_sub_service` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` VARCHAR(200)  NOT NULL ,
`cow_health_service_id` INT NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_cow_health_line` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`cow_id` INT NOT NULL ,
`cow_name` VARCHAR(300)  NOT NULL ,
`is_not_specify` INT(1)  NOT NULL ,
`cow_health_service_id` INT NOT NULL ,
`cow_health_sub_service_id` INT NOT NULL ,
`cow_medicine_id` INT NOT NULL ,
`amount` DECIMAL(10,2)  NOT NULL ,
`price` DECIMAL(10,2)  NOT NULL ,
`total_price` DECIMAL(10,2)  NOT NULL ,
`cow_health_id` INT NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_cow_heal_result` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`cow_id` INT NOT NULL ,
`create_date` DATE NOT NULL ,
`heal_result` VARCHAR(255)  NOT NULL ,
`cow_health_line_id` INT NOT NULL ,
`vet_id` INT NOT NULL ,
`create_by` INT NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_cow_stop_milking` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`cow_id` INT NOT NULL ,
`create_date` DATE NOT NULL ,
`create_by` INT(11)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_region` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` VARCHAR(100)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_province` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` VARCHAR(100)  NOT NULL ,
`region_id` INT NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_district` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` VARCHAR(100)  NOT NULL ,
`province_id` INT NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_sub_district` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` VARCHAR(100)  NOT NULL ,
`district_id` INT NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_coop` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`coop_code` VARCHAR(100)  NOT NULL ,
`coop_abbr` VARCHAR(100)  NOT NULL ,
`name` VARCHAR(100)  NOT NULL ,
`address` TEXT NOT NULL ,
`region_id` INT NOT NULL ,
`province_id` INT NOT NULL ,
`district_id` INT NOT NULL ,
`sub_district_id` INT NOT NULL ,
`post_code` VARCHAR(5)  NOT NULL ,
`latitude` DECIMAL(12,8)  NOT NULL ,
`longitude` DECIMAL(12,8)  NOT NULL ,
`create_by` INT(11)  NOT NULL ,
`update_by` INT(11)  NOT NULL ,
`create_datetime` DATE NOT NULL ,
`update_datetime` DATE NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_coop_gmp_certify` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`coop_id` INT NOT NULL ,
`certify_no` VARCHAR(100)  NOT NULL ,
`certify_date` DATE NOT NULL ,
`expire_date` DATE NOT NULL ,
`certify_agency` VARCHAR(100)  NOT NULL ,
`create_by` INT(11)  NOT NULL ,
`update_by` INT(11)  NOT NULL ,
`create_datetime` DATE NOT NULL ,
`update_datetime` DATE NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_coop_send_milk_company` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`coop_id` INT NOT NULL ,
`name` VARCHAR(100)  NOT NULL ,
`address` VARCHAR(300)  NOT NULL ,
`create_by` INT(11)  NOT NULL ,
`update_by` INT(11)  NOT NULL ,
`create_datetime` DATE NOT NULL ,
`update_datetime` DATE NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_coop_has_mou` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`coop_id` INT NOT NULL ,
`year` INT(4)  NOT NULL ,
`daily_amount` DECIMAL(10,2)  NOT NULL ,
`create_by` INT(11)  NOT NULL ,
`update_by` INT(11)  NOT NULL ,
`create_datetime` DATE NOT NULL ,
`update_datetime` DATE NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_cow_project` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` VARCHAR(100)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_farm_condition` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` VARCHAR(100)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_performance_improvement_project` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` VARCHAR(100)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_milking_system` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` VARCHAR(100)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_farm` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`member_code` VARCHAR(100)  NOT NULL ,
`name` VARCHAR(100)  NOT NULL ,
`coop_id` INT NOT NULL ,
`address` VARCHAR(300)  NOT NULL ,
`province_id` INT NOT NULL ,
`region_id` INT NOT NULL ,
`district_id` INT NOT NULL ,
`sub_district_id` INT NOT NULL ,
`post_code` INT(5)  NOT NULL ,
`latitude` DECIMAL(12,8)  NOT NULL ,
`longitude` DECIMAL(12,8)  NOT NULL ,
`farm_project_id` INT NOT NULL ,
`status` VARCHAR(255)  NOT NULL ,
`is_join_breeding_system` VARCHAR(255)  NOT NULL ,
`farm_condition_id` VARCHAR(255)  NOT NULL ,
`environment` VARCHAR(300)  NOT NULL ,
`performance_improvement_project_id` INT NOT NULL ,
`milking_system_id` INT NOT NULL ,
`waste_disposal_type` VARCHAR(255)  NOT NULL ,
`member_start_date` DATE NOT NULL ,
`breeding_start_date` DATE NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_farm_gap_certify` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`farm_id` INT NOT NULL ,
`certify_no` VARCHAR(100)  NOT NULL ,
`certify_date` DATE NOT NULL ,
`expire_date` DATE NOT NULL ,
`certify_agency` VARCHAR(100)  NOT NULL ,
`create_by` INT(11)  NOT NULL ,
`update_by` INT(11)  NOT NULL ,
`create_datetime` DATE NOT NULL ,
`update_datetime` DATE NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_farm_grade_certify` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`farm_id` INT NOT NULL ,
`grade` VARCHAR(50)  NOT NULL ,
`certify_date` DATE NOT NULL ,
`book_no` VARCHAR(100)  NOT NULL ,
`create_by` INT(11)  NOT NULL ,
`update_by` INT(11)  NOT NULL ,
`create_datetime` DATE NOT NULL ,
`update_datetime` DATE NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_farm_grade_announcement` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`year` INT(4)  NOT NULL ,
`announce_date` DATE NOT NULL ,
`detail` TEXT NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_farmer` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`citizen_id` VARCHAR(13)  NOT NULL ,
`image_url` TEXT NOT NULL ,
`title_id` VARCHAR(255)  NOT NULL ,
`name` VARCHAR(100)  NOT NULL ,
`surname` VARCHAR(100)  NOT NULL ,
`birthdate` DATE NOT NULL ,
`education_level_id` VARCHAR(255)  NOT NULL ,
`address` VARCHAR(300)  NOT NULL ,
`region_id` INT NOT NULL ,
`province_id` INT NOT NULL ,
`district_id` INT NOT NULL ,
`sub_district_id` INT NOT NULL ,
`post_code` INT(5)  NOT NULL ,
`latitude` DECIMAL(12,8)  NOT NULL ,
`longitude` DECIMAL(12,8)  NOT NULL ,
`mobile` VARCHAR(50)  NOT NULL ,
`fax` VARCHAR(50)  NOT NULL ,
`email` VARCHAR(100)  NOT NULL ,
`line_id` VARCHAR(100)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_farm_has_farmer` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`farm_id` INT(11)  NOT NULL ,
`farmer_id` INT(11)  NOT NULL ,
`own_date` DATE NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_training_program` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` VARCHAR(100)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_farmer_training` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`farmer_id` INT NOT NULL ,
`training_program_id` INT NOT NULL ,
`training_date` DATE NOT NULL ,
`training_place` VARCHAR(100)  NOT NULL ,
`training_day` INT(11)  NOT NULL ,
`create_by` INT(11)  NOT NULL ,
`update_by` INT(11)  NOT NULL ,
`create_datetime` DATE NOT NULL ,
`update_datetime` DATE NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_farm_score` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`farm_id` INT NOT NULL ,
`assessment_date` DATE NOT NULL ,
`score` DECIMAL(10,2)  NOT NULL ,
`score_result` VARCHAR(50)  NOT NULL ,
`assessment_data` TEXT NOT NULL ,
`remark` VARCHAR(100)  NOT NULL ,
`create_by` INT(11)  NOT NULL ,
`update_by` INT(11)  NOT NULL ,
`create_datetime` DATE NOT NULL ,
`update_datetime` DATE NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_cow` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`farm_id` INT NOT NULL ,
`cow_id` VARCHAR(100)  NOT NULL ,
`name` VARCHAR(100)  NOT NULL ,
`dld_id` VARCHAR(100)  NOT NULL ,
`ear_id` VARCHAR(100)  NOT NULL ,
`birthdate` DATE NOT NULL ,
`gender` VARCHAR(255)  NOT NULL ,
`move_farm_date` DATE NOT NULL ,
`move_type` VARCHAR(255)  NOT NULL ,
`move_from_farm_id` INT NOT NULL ,
`breed` VARCHAR(300)  NOT NULL ,
`father_cow_id` INT NOT NULL ,
`mother_cow_id` INT NOT NULL ,
`create_by` INT(11)  NOT NULL ,
`update_by` INT(11)  NOT NULL ,
`create_datetime` DATE NOT NULL ,
`update_datetime` DATE NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_cow_status` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` VARCHAR(100)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_cow_has_cow_status` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`cow_id` INT(11)  NOT NULL ,
`status_code` VARCHAR(100)  NOT NULL ,
`cow_status_id` INT(11)  NOT NULL ,
`status_date` DATE NOT NULL ,
`create_by` INT(11)  NOT NULL ,
`update_by` INT(11)  NOT NULL ,
`create_date` DATE NOT NULL ,
`update_date` DATE NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_cow_move_out` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`cow_id` INT NOT NULL ,
`move_out_date` DATE NOT NULL ,
`move_type` VARCHAR(255)  NOT NULL ,
`from_farm_id` INT NOT NULL ,
`to_farm_id` INT NOT NULL ,
`move_objective` VARCHAR(100)  NOT NULL ,
`move_reason` VARCHAR(100)  NOT NULL ,
`create_by` INT(11)  NOT NULL ,
`update_by` INT(11)  NOT NULL ,
`create_date` DATE NOT NULL ,
`update_date` DATE NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_cow_breeding` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`cow_id` INT NOT NULL ,
`round` INT(11)  NOT NULL ,
`no` INT(11)  NOT NULL ,
`breeding_date` DATE NOT NULL ,
`father_id` INT NOT NULL ,
`lot_no` VARCHAR(100)  NOT NULL ,
`semen_source` VARCHAR(100)  NOT NULL ,
`inspection_date` DATE NOT NULL ,
`inspection_result` VARCHAR(100)  NOT NULL ,
`vet_id` INT(11)  NOT NULL ,
`create_by` INT(11)  NOT NULL ,
`update_by` INT(11)  NOT NULL ,
`create_datetime` DATE NOT NULL ,
`update_datetime` DATE NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__ed_cow_has_child` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`cow_id` INT(11)  NOT NULL ,
`breeding_date` DATE NOT NULL ,
`father_id` INT(11)  NOT NULL ,
`stop_milking_date` DATE NOT NULL ,
`birth_date` DATE NOT NULL ,
`child_status` INT(11)  NOT NULL ,
`child_gender` INT(1)  NOT NULL ,
`child_weight` DECIMAL(10,2)  NOT NULL ,
`child_ear_id` VARCHAR(100)  NOT NULL ,
`vet_id` INT(11)  NOT NULL ,
`create_by` INT(11)  NOT NULL ,
`update_by` INT(11)  NOT NULL ,
`create_date` DATE NOT NULL ,
`update_date` DATE NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci;

