<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Cow controller class.
 *
 * @since  1.6
 */
class EdairyControllerCow extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'cows';
		parent::__construct();
	}

	public function updateCowStatus(){
		 $model = $this->getModel('Cow', 'EdairyModel');
        if($model->updateCowStatus()){
        	$data = array(
		        'update_result' => true
		    );

		    echo json_encode( $data );
		    jexit();
		}else{
			$data = array(
		        'update_result' => false
		    );

		    echo json_encode( $data );
		    jexit();
		}
	}

	public function searchCow(){
		$model = $this->getModel('Cow', 'EdairyModel');
        $data = $model->searchCow();
        	
		    echo json_encode( $data );
		    jexit();
		
	}

	public function getAllCow(){
		$model = $this->getModel('Cow', 'EdairyModel');
        $data = $model->getAllCow();
        	
		    echo json_encode( $data );
		    jexit();
		
	}

	public function saveBreed(){
		 $model = $this->getModel('Cow', 'EdairyModel');
        if($model->saveBreed()){
        	$data = array(
		        'save_result' => true
		    );

		    echo json_encode( $data );
		    jexit();
		}else{
			$data = array(
		        'save_result' => false
		    );

		    echo json_encode( $data );
		    jexit();
		}
	}

	public function saveMoveOut(){
		 $model = $this->getModel('Cow', 'EdairyModel');

        if($model->saveMoveOut()){
        	$this->setMessage(JText::_('บันทึกการย้ายออกได้สำเร็จ'), "success");
		 	$this->setRedirect(JRoute::_("index.php?option=com_edairy&view=cow&layout=edit&id={$_POST[move_out_cow_id]}&default_tab=move_out", false));
		}else{
			$this->setMessage(JText::_('ไม่สามารถบันทึกการย้ายออกได้'), "error");
			$this->setRedirect(JRoute::_("index.php?option=com_edairy&view=cow&layout=edit&id={$_POST[move_out_cow_id]}&default_tab=move_out", false));
		}
	}


	public function getMoveOut(){
		 $model = $this->getModel('Cow', 'EdairyModel');

		 echo json_encode($model->getMoveOut());
		 jexit();
	}

	public function deleteMoveOut(){
		 $model = $this->getModel('Cow', 'EdairyModel');

        if($model->deleteMoveOut()){
        	$data = array(
		        'delete_result' => true
		    );

		    echo json_encode( $data );
		    jexit();
		}else{
			$data = array(
		        'delete_result' => false
		    );

		    echo json_encode( $data );
		    jexit();
		}
	}

}
