<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Pregnant_inspection controller class.
 *
 * @since  1.6
 */
class EdairyControllerPregnant_inspection extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'pregnant_inspections';
		parent::__construct();
	}

	public function postSaveHook($model, $validData)
	{
	    $item = $model->getItem();
	    $model = $this->getModel('Pregnant_inspection', 'EdairyModel');

        $model->saveStopMilkingAndBirthDate($item);

	}
}
