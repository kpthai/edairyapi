<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Receive_milk_quantity_cow controller class.
 *
 * @since  1.6
 */
class EdairyControllerReceive_milk_quantity_cow extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'receive_milk_quantity_cows';
		parent::__construct();
	}


	public function getCowsInFarm(){
		 $model = $this->getModel('Receive_milk_quantity_cow', 'EdairyModel');

		 echo json_encode($model->getCowsInFarm());
		 jexit();
	}

	public function save(){
		$model = $this->getModel('Receive_milk_quantity_cow', 'EdairyModel');

        if($model->customSave()){
        	$this->setMessage(JText::_('บันทึกปริมาณน้ำนมโคแต่ละตัวได้สำเร็จ'), "success");
		 	$this->setRedirect(JRoute::_("index.php?option=com_edairy&view=receive_milk_quantity_cows", false));
		}else{
			$this->setMessage(JText::_('ไม่สามารถบันทึกปริมาณน้ำนมโคแต่ละตัวได้สำเร็จ'), "error");
			$this->setRedirect(JRoute::_("index.php?option=com_edairy&view=receive_milk_quantity_cows", false));
		}
		//die();
	}
}
