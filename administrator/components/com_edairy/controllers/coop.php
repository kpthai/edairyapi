<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Coop controller class.
 *
 * @since  1.6
 */
class EdairyControllerCoop extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'coops';
		parent::__construct();
	}

	public function getGMP(){
		 $model = $this->getModel('Coop', 'EdairyModel');

		 echo json_encode($model->getGMP());
		 jexit();
	}

	public function saveGMP(){
		 $model = $this->getModel('Coop', 'EdairyModel');

        if($model->saveGMP()){
        	$this->setMessage(JText::_('บันทึกมาตรฐาน GMP ได้สำเร็จ'), "success");
		 	$this->setRedirect(JRoute::_("index.php?option=com_edairy&view=coop&layout=edit&id={$_POST[gmp_coop_id]}&default_tab=gmp", false));
		}else{
			$this->setMessage(JText::_('ไม่สามารถบันทึกมาตรฐาน GMP ได้'), "error");
			$this->setRedirect(JRoute::_("index.php?option=com_edairy&view=coop&layout=edit&id={$_POST[gmp_coop_id]}&default_tab=gmp", false));
		}
	}


	public function deleteGMP(){
		 $model = $this->getModel('Coop', 'EdairyModel');

        if($model->deleteGMP()){
        	$data = array(
		        'delete_result' => true
		    );

		    echo json_encode( $data );
		    jexit();
		}else{
			$data = array(
		        'delete_result' => false
		    );

		    echo json_encode( $data );
		    jexit();
		}
	}

	public function getSendMilkCompany(){
		 $model = $this->getModel('Coop', 'EdairyModel');

		 echo json_encode($model->getSendMilkCompany());
		 jexit();
	}

	public function saveSendMilkCompany(){
		 $model = $this->getModel('Coop', 'EdairyModel');

        if($model->saveSendMilkCompany()){
		 	$this->setRedirect(JRoute::_("index.php?option=com_edairy&view=coop&layout=edit&id={$_POST[smc_coop_id]}&default_tab=send_milk_company", false));
		}else{
			$this->setMessage(JText::_('ไม่สามารถบันทึกโรงงาน/บริษัทที่ส่งนมได้'), "error");
			$this->setRedirect(JRoute::_("index.php?option=com_edairy&view=coop&layout=edit&id={$_POST[smc_coop_id]}&default_tab=send_milk_company", false));
		}
	}

	public function deleteSendMilkCompany(){
		 $model = $this->getModel('Coop', 'EdairyModel');

        if($model->deleteSendMilkCompany()){
        	$data = array(
		        'delete_result' => true
		    );

		    echo json_encode( $data );
		    jexit();
		}else{
			$data = array(
		        'delete_result' => false
		    );

		    echo json_encode( $data );
		    jexit();
		}
	}

	public function getMOU(){
		 $model = $this->getModel('Coop', 'EdairyModel');

		 echo json_encode($model->getMOU());
		 jexit();
	}

	public function saveMOU(){
		 $model = $this->getModel('Coop', 'EdairyModel');

        if($model->saveMOU()){
		 	$this->setRedirect(JRoute::_("index.php?option=com_edairy&view=coop&layout=edit&id={$_POST[mou_coop_id]}&default_tab=mou", false));
		}else{
			$this->setMessage(JText::_('ไม่สามารถบันทึก MOU ได้'), "error");
			$this->setRedirect(JRoute::_("index.php?option=com_edairy&view=coop&layout=edit&id={$_POST[mou_coop_id]}&default_tab=mou", false));
		}
	}

	public function deleteMOU(){
		 $model = $this->getModel('Coop', 'EdairyModel');

        if($model->deleteMOU()){
        	$data = array(
		        'delete_result' => true
		    );

		    echo json_encode( $data );
		    jexit();
		}else{
			$data = array(
		        'delete_result' => false
		    );

		    echo json_encode( $data );
		    jexit();
		}
	}

}
