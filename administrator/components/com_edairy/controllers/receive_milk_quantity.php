<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Receive_milk_quantity controller class.
 *
 * @since  1.6
 */
class EdairyControllerReceive_milk_quantity extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'receive_milk_quantities';
		parent::__construct();
	}

	public function postSaveHook($model, $validData)
	{
	    $item = $model->getItem();
	    $model = $this->getModel('Receive_milk_quantity', 'EdairyModel');

        $model->saveReceiveMilkQuantityLine($item);

	}

	public function importExcel()
	{
		$model = $this->getModel('Receive_milk_quantity', 'EdairyModel');

		$return = $model->importExcel();
        if($return!==false){
        	$this->setMessage(JText::_('อับโหลดไฟล์สำเร็จ'), "success");
		 	$this->setRedirect(JRoute::_("index.php?option=com_edairy&view=receive_milk_quantity&layout=import_excel&filename=" . $return, false));
		}else{
			$this->setMessage(JText::_('ไม่สามารถอับโหลดไฟล์ได้'), "error");
			$this->setRedirect(JRoute::_("index.php?option=com_edairy&view=receive_milk_quantities", false));
		}
	}
}
