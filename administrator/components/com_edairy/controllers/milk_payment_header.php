<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Milk_payment_header controller class.
 *
 * @since  1.6
 */
class EdairyControllerMilk_payment_header extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'milk_payment_headers';
		parent::__construct();
	}

	public function save()
	{
		// $model = $this->getModel('Farm', 'EdairyModel');

        $coop_id = $_POST["jform"]["coop_id"];
        $period = $_POST["jform"]["period"];
        $from_date = $_POST["jform"]["from_date"];
        $to_date = $_POST["jform"]["to_date"];
        

        //$this->setMessage(JText::_('บันทึกมาตรฐาน GAP ได้สำเร็จ'), "success");
		$this->setRedirect(JRoute::_("index.php?option=com_edairy&view=milk_payment_header&layout=step2&coop_id={$coop_id}&period={$period}&from_date={$from_date}&to_date={$to_date}", false));
		
	}
	public function step2()
	{
		// $model = $this->getModel('Farm', 'EdairyModel');

        
        //$this->setMessage(JText::_('บันทึกมาตรฐาน GAP ได้สำเร็จ'), "success");

		$coop_id = $_POST["coop_id"];
        $period = $_POST["period"];
        $from_date = $_POST["from_date"];
        $to_date = $_POST["to_date"];

		$this->setRedirect(JRoute::_("index.php?option=com_edairy&view=milk_payment_header&layout=step3&coop_id={$coop_id}&period={$period}&from_date={$from_date}&to_date={$to_date}", false));
		
	}
	public function step3()
	{
		// $model = $this->getModel('Farm', 'EdairyModel');

        $coop_id = $_POST["coop_id"];
        $period = $_POST["period"];
        $from_date = $_POST["from_date"];
        $to_date = $_POST["to_date"];
        
        //$this->setMessage(JText::_('บันทึกมาตรฐาน GAP ได้สำเร็จ'), "success");
		$this->setRedirect(JRoute::_("index.php?option=com_edairy&view=milk_payment_header&layout=step4&coop_id={$coop_id}&period={$period}&from_date={$from_date}&to_date={$to_date}", false));
		
	}
	public function step4()
	{
		// $model = $this->getModel('Farm', 'EdairyModel');

        $coop_id = $_POST["coop_id"];
        $period = $_POST["period"];
        $from_date = $_POST["from_date"];
        $to_date = $_POST["to_date"];
        //$this->setMessage(JText::_('บันทึกมาตรฐาน GAP ได้สำเร็จ'), "success");
		$this->setRedirect(JRoute::_("index.php?option=com_edairy&view=milk_payment_header&layout=step5&coop_id={$coop_id}&period={$period}&from_date={$from_date}&to_date={$to_date}", false));
		
	}
	public function step5()
	{
        $model = $this->getModel('Milk_payment_header', 'EdairyModel');

        if($model->customSave()){
        	$this->setMessage(JText::_('บันทึกการคิดราคาน้ำนม-รายเกษตรกรได้สำเร็จ'), "success");
    	}else{
    		$this->setMessage(JText::_('ไม่สามารถบันทึกการคิดราคาน้ำนม-รายเกษตรกรได้'), "error");
    	}
		$this->setRedirect(JRoute::_("index.php?option=com_edairy&view=milk_payment_headers", false));
		
	}

	
}
