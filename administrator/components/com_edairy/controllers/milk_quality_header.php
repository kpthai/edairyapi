<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Milk_quality_header controller class.
 *
 * @since  1.6
 */
class EdairyControllerMilk_quality_header extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'milk_quality_headers';
		parent::__construct();
	}

	public function getMilkQualityTransaction(){
		 $model = $this->getModel('Milk_quality_header', 'EdairyModel');

		 echo json_encode($model->getMilkQualityTransaction());
		 jexit();
	}

	public function saveMilkQualityTransaction(){
		 $model = $this->getModel('Milk_quality_header', 'EdairyModel');


        if($model->saveMilkQualityTransaction()){
        	$this->setMessage(JText::_('บันทึกรายการคุณภาพน้ำนมเกษตรกรได้สำเร็จ'), "success");
		 	$this->setRedirect(JRoute::_("index.php?option=com_edairy&view=milk_quality_header&layout=edit&id={$_POST[milk_quality_id]}", false));
		}else{
			$this->setMessage(JText::_('ไม่สามารถบันทึกรายการคุณภาพน้ำนมเกษตรกรได้'), "error");
			$this->setRedirect(JRoute::_("index.php?option=com_edairy&view=milk_quality_header&layout=edit&id={$_POST[milk_quality_id]}", false));
		}
	}


	public function deleteMilkQualityTransaction(){
		 $model = $this->getModel('Milk_quality_header', 'EdairyModel');

        if($model->deleteTraining()){
        	$data = array(
		        'delete_result' => true
		    );

		    echo json_encode( $data );
		    jexit();
		}else{
			$data = array(
		        'delete_result' => false
		    );

		    echo json_encode( $data );
		    jexit();
		}
	}

	public function importExcel()
	{
		$model = $this->getModel('Milk_quality_header', 'EdairyModel');

		$return = $model->importExcel();
        if($return!==false){
        	$this->setMessage(JText::_('อับโหลดไฟล์สำเร็จ'), "success");
		 	$this->setRedirect(JRoute::_("index.php?option=com_edairy&view=milk_quality_header&layout=import_excel&milk_quality_id=" . $_POST['milk_quality_id'] ."&filename=" . $return, false));
		}else{
			$this->setMessage(JText::_('ไม่สามารถอับโหลดไฟล์ได้'), "error");
			$this->setRedirect(JRoute::_("index.php?option=com_edairy&view=milk_quality_headers", false));
		}
	}


	public function getFarmStandardScore(){
		 $model = $this->getModel('Milk_quality_header', 'EdairyModel');

		 echo json_encode($model->getFarmStandardScore());
		 jexit();
	}


}
