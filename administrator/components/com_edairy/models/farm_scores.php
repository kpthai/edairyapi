<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Edairy records.
 *
 * @since  1.6
 */
class EdairyModelFarm_scores extends JModelList
{
/**
	* Constructor.
	*
	* @param   array  $config  An optional associative array of configuration settings.
	*
	* @see        JController
	* @since      1.6
	*/
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.`id`',
				'farm_id', 'a.`farm_id`',
				'assessment_date', 'a.`assessment_date`',
				'score', 'a.`score`',
				'score_result', 'a.`score_result`',
				'assessment_data', 'a.`assessment_data`',
				'remark', 'a.`remark`',
				'create_by', 'a.`create_by`',
				'update_by', 'a.`update_by`',
				'create_datetime', 'a.`create_datetime`',
				'update_datetime', 'a.`update_datetime`',
				'ordering', 'a.`ordering`',
				'state', 'a.`state`',
				'created_by', 'a.`created_by`',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
		$this->setState('filter.state', $published);
		// Filtering farm_id
		$this->setState('filter.farm_id', $app->getUserStateFromRequest($this->context.'.filter.farm_id', 'filter_farm_id', '', 'string'));

		// Filtering assessment_date
		$this->setState('filter.assessment_date.from', $app->getUserStateFromRequest($this->context.'.filter.assessment_date.from', 'filter_from_assessment_date', '', 'string'));
		$this->setState('filter.assessment_date.to', $app->getUserStateFromRequest($this->context.'.filter.assessment_date.to', 'filter_to_assessment_date', '', 'string'));

		// Filtering update_by
		$this->setState('filter.update_by', $app->getUserStateFromRequest($this->context.'.filter.update_by', 'filter_update_by', '', 'string'));

		// Filtering update_datetime
		$this->setState('filter.update_datetime.from', $app->getUserStateFromRequest($this->context.'.filter.update_datetime.from', 'filter_from_update_datetime', '', 'string'));
		$this->setState('filter.update_datetime.to', $app->getUserStateFromRequest($this->context.'.filter.update_datetime.to', 'filter_to_update_datetime', '', 'string'));


		// Load the parameters.
		$params = JComponentHelper::getParams('com_edairy');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.farm_id', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select', 'DISTINCT a.*'
			)
		);
		$query->from('`#__ed_farm_score` AS a');

		// Join over the users for the checked out user
		$query->select("uc.name AS uEditor");
		$query->join("LEFT", "#__users AS uc ON uc.id=a.checked_out");
		// Join over the foreign key 'farm_id'
		$query->select('CONCAT(`#__ed_farm_2698828`.`member_code`, \' \', `#__ed_farm_2698828`.`name`) AS farms_fk_value_2698828');
		$query->join('LEFT', '#__ed_farm AS #__ed_farm_2698828 ON #__ed_farm_2698828.`id` = a.`farm_id`');

		// Join over the user field 'create_by'
		$query->select('`create_by`.name AS `create_by`');
		$query->join('LEFT', '#__users AS `create_by` ON `create_by`.id = a.`create_by`');

		// Join over the user field 'update_by'
		$query->select('`update_by`.name AS `update_by`');
		$query->join('LEFT', '#__users AS `update_by` ON `update_by`.id = a.`update_by`');

		// Join over the user field 'created_by'
		$query->select('`created_by`.name AS `created_by`');
		$query->join('LEFT', '#__users AS `created_by` ON `created_by`.id = a.`created_by`');

		// Filter by published state
		$published = $this->getState('filter.state');

		if (is_numeric($published))
		{
			$query->where('a.state = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.state IN (0, 1))');
		}

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				$query->where('(CONCAT(`#__ed_farm_2698828`.`member_code`, \' \', `#__ed_farm_2698828`.`name`) LIKE ' . $search . '  OR  a.score LIKE ' . $search . '  OR  a.score_result LIKE ' . $search . '  OR  a.remark LIKE ' . $search . ' )');
			}
		}


		//Filtering farm_id
		$filter_farm_id = $this->state->get("filter.farm_id");
		if ($filter_farm_id !== null && !empty($filter_farm_id))
		{
			$query->where("a.`farm_id` = '".$db->escape($filter_farm_id)."'");
		}

		//Filtering assessment_date
		$filter_assessment_date_from = $this->state->get("filter.assessment_date.from");

		if ($filter_assessment_date_from !== null && !empty($filter_assessment_date_from))
		{
			$query->where("a.`assessment_date` >= '".$db->escape($filter_assessment_date_from)."'");
		}
		$filter_assessment_date_to = $this->state->get("filter.assessment_date.to");

		if ($filter_assessment_date_to !== null  && !empty($filter_assessment_date_to))
		{
			$query->where("a.`assessment_date` <= '".$db->escape($filter_assessment_date_to)."'");
		}

		//Filtering update_by
		$filter_update_by = $this->state->get("filter.update_by");
		if ($filter_update_by !== null && !empty($filter_update_by))
		{
			$query->where("a.`update_by` = '".$db->escape($filter_update_by)."'");
		}

		//Filtering update_datetime
		$filter_update_datetime_from = $this->state->get("filter.update_datetime.from");

		if ($filter_update_datetime_from !== null && !empty($filter_update_datetime_from))
		{
			$query->where("a.`update_datetime` >= '".$db->escape($filter_update_datetime_from)."'");
		}
		$filter_update_datetime_to = $this->state->get("filter.update_datetime.to");

		if ($filter_update_datetime_to !== null  && !empty($filter_update_datetime_to))
		{
			$query->where("a.`update_datetime` <= '".$db->escape($filter_update_datetime_to)."'");
		}


		//Custom Filter
		if ($_REQUEST["search_coop"]!="")
		{
			$query->where("a.coop_id in (select id from #__ed_coop c where c.name like '%{$_REQUEST[search_coop]}%' or c.coop_code like '%{$_REQUEST[search_coop]}%' )");
		}
		if ($_REQUEST["search_farm_name"]!="")
		{
			$query->where("a.farm_id in (select id from #__ed_farm f where f.name like '%{$_REQUEST[search_farm_name]}%' )");
		}

		if ($_REQUEST["search_member_code"]!="")
		{

			$query->where("a.farm_id in (select id from #__ed_farm f where f.member_code like '%{$_REQUEST[search_member_code]}%' )");
		}
		if ($_REQUEST["search_date_from"]!="" && $_REQUEST["search_date_to"]!="")
		{
			$date_from = formatSQLDate($_REQUEST["search_date_from"]);
			$date_to = formatSQLDate($_REQUEST["search_date_to"]);
			$query->where("a.assessment_date between '{$date_from}' and '{$date_to}'");
		}

		//User Role Filter
		$user = JFactory::getUser();

		if($user->role==1){ // Farmer
			$query->where("a.farm_id in (select farm_id from #__ed_farmer f where f.id in ({$user->farmer_id}))");
		}
		
		if($user->role==2){ // Vet
			$query->where("a.farm_id in (select id from #__ed_farm f where f.coop_id in ({$user->coop_id}))");
		}


		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');

		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();

		foreach ($items as $oneItem) {

			if (isset($oneItem->farm_id))
			{
				$values = explode(',', $oneItem->farm_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('CONCAT(`#__ed_farm_2698828`.`member_code`, \' \', `#__ed_farm_2698828`.`name`) AS `fk_value`')
							->from($db->quoteName('#__ed_farm', '#__ed_farm_2698828'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->fk_value;
					}
				}

			$oneItem->farm_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->farm_id;

			}
		}
		return $items;
	}
}
