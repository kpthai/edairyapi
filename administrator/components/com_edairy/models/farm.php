<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Edairy model.
 *
 * @since  1.6
 */
class EdairyModelFarm extends JModelAdmin
{
	/**
	 * @var      string    The prefix to use with controller messages.
	 * @since    1.6
	 */
	protected $text_prefix = 'COM_EDAIRY';

	/**
	 * @var   	string  	Alias to manage history control
	 * @since   3.2
	 */
	public $typeAlias = 'com_edairy.farm';

	/**
	 * @var null  Item data
	 * @since  1.6
	 */
	protected $item = null;

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param   string  $type    The table type to instantiate
	 * @param   string  $prefix  A prefix for the table class name. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return    JTable    A database object
	 *
	 * @since    1.6
	 */
	public function getTable($type = 'Farm', $prefix = 'EdairyTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      An optional array of data for the form to interogate.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  JForm  A JForm object on success, false on failure
	 *
	 * @since    1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Initialise variables.
		$app = JFactory::getApplication();

		// Get the form.
		$form = $this->loadForm(
			'com_edairy.farm', 'farm',
			array('control' => 'jform',
				'load_data' => $loadData
			)
		);

		if (empty($form))
		{
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return   mixed  The data for the form.
	 *
	 * @since    1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_edairy.edit.farm.data', array());

		if (empty($data))
		{
			if ($this->item === null)
			{
				$this->item = $this->getItem();
			}

			$data = $this->item;
		}

		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param   integer  $pk  The id of the primary key.
	 *
	 * @return  mixed    Object on success, false on failure.
	 *
	 * @since    1.6
	 */
	public function getItem($pk = null)
	{
		if ($item = parent::getItem($pk))
		{
			// Do any procesing on fields here if needed
			if($_REQUEST["layout"]!="farm_survey"){
				$db = JFactory::getDbo();
				$query = $db->getQuery(true);
				$query
					->select("*")
					->from('#__ed_farm_gap_certify')
					->where("farm_id='{$item->id}'")
					->order("certify_date asc, id asc");
				$db->setQuery($query);
				$result = $db->loadObjectList();
				$item->gap = $result;


				$db = JFactory::getDbo();
				$query = $db->getQuery(true);
				$query
					->select("*")
					->from('#__ed_farmer')
					->where("farm_id='{$item->id}'")
					->limit(1);
				$db->setQuery($query);
				$result = $db->loadObject();
				$item->farmer = $result;

				$db = JFactory::getDbo();
				$query = $db->getQuery(true);
				$query
					->select("t.*, p.name as training_program")
					->from('#__ed_farmer_training t')
					->join("left", "#__ed_training_program p on training_program_id=p.id")
					->where("farmer_id='{$item->farmer->id}'")
					->limit(1);
				$db->setQuery($query);
				$result = $db->loadObjectList();
				$item->training = $result;


				$db = JFactory::getDbo();
				$query = $db->getQuery(true);
				$query
					->select("fhf.*, f.citizen_id, f.name, f.surname")
					->from('#__ed_farm_has_farmer fhf')
					->join("left", "#__ed_farmer f on f.id = farmer_id")
					->where("farmer_id='{$item->farmer->id}'");
				$db->setQuery($query);
				$result = $db->loadObjectList();
				$item->owner = $result;
			}else{


				if($_REQUEST["id"]!=""){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
						->select("*")
						->from('#__ed_farm')
						->where("id=(select farm_id from #__ed_farm_survey_transaction where id='{$_REQUEST[id]}')");
					$db->setQuery($query);
					$item= $db->loadObject();


					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
						->select("st.*, (select id from #__ed_farmer where farm_id = st.farm_id) as farmer_id")
						->from('#__ed_farm_survey_transaction st')
						->where("id='{$_REQUEST[id]}'");
					$db->setQuery($query);
					$result = $db->loadObject();
					$item->survey_transaction = $result;


					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
						->select("*, (select CONCAT(CONCAT(name, ' '), description) from #__ed_cow_status where id=cow_status_id ) as cow_status")
						->from('#__ed_cow')
						->where("farm_id='{$item->survey_transaction->farm_id}'");
					$db->setQuery($query);
					$result = $db->loadObjectList();
					$item->cow = $result;

				}else if($_REQUEST["farm_id"]!=""){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
						->select("*")
						->from('#__ed_farm')
						->where("id='{$_REQUEST[farm_id]}'");
					$db->setQuery($query);

					$item= $db->loadObject();



					/*$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
						->select("st.*, (select id from #__ed_farmer where farm_id = st.farm_id) as farmer_id")
						->from('#__ed_farm_survey_transaction st')
						->where("id='{$_REQUEST[id]}'");
					$db->setQuery($query);
					$result = $db->loadObject();
					$item->survey_transaction = $result;*/

					ini_set('memory_limit', '-1');

					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
						->select("*")
						->from('#__ed_cow')
						->where("farm_id='{$_REQUEST[farm_id]}'");
					$db->setQuery($query);


					$result = $db->loadObjectList();

					
					$item->cow = $result;


				}

				$db = JFactory::getDbo();
				$query = $db->getQuery(true);

				if($item->survey_transaction->create_date!=null && $item->survey_transaction->create_date!=""){
					$create_date = $item->survey_transaction->create_date;
					$query
						->select("c.*, (select CONCAT(CONCAT(name, ' '), description) from #__ed_cow_status where id=(select cow_status_id from #__ed_cow_has_cow_status where cow_id=c.id and status_date = '{$create_date}') ) as cow_status")
						->from('#__ed_cow c')
						->where("farm_id='{$item->survey_transaction->farm_id}'
							and exists (select * from #__ed_cow_has_cow_status where cow_id=c.id and status_date = '{$create_date}')
							");
				}else{
					$query
						->select("c.*, (select CONCAT(CONCAT(name, ' '), description) from #__ed_cow_status where id=cow_status_id ) as cow_status")
						->from('#__ed_cow c')
						->where("farm_id='{$_REQUEST[farm_id]}'
							and not exists (select * from #__ed_cow_has_cow_status where cow_id=c.id and status_date > NOW() - INTERVAL 2 MONTH)
							");
				}
				$db->setQuery($query);
				$result = $db->loadObjectList();
				$item->cow_2month = $result;

				//print_r($item->cow);
			}

		}

		return $item;
	}

	/**
	 * Method to duplicate an Farm
	 *
	 * @param   array  &$pks  An array of primary key IDs.
	 *
	 * @return  boolean  True if successful.
	 *
	 * @throws  Exception
	 */
	public function duplicate(&$pks)
	{
		$user = JFactory::getUser();

		// Access checks.
		if (!$user->authorise('core.create', 'com_edairy'))
		{
			throw new Exception(JText::_('JERROR_CORE_CREATE_NOT_PERMITTED'));
		}

		$dispatcher = JEventDispatcher::getInstance();
		$context    = $this->option . '.' . $this->name;

		// Include the plugins for the save events.
		JPluginHelper::importPlugin($this->events_map['save']);

		$table = $this->getTable();

		foreach ($pks as $pk)
		{
			if ($table->load($pk, true))
			{
				// Reset the id to create a new record.
				$table->id = 0;

				if (!$table->check())
				{
					throw new Exception($table->getError());
				}
				
				if (!empty($table->coop_id))
				{
					if (is_array($table->coop_id))
					{
						$table->coop_id = implode(',', $table->coop_id);
					}
				}
				else
				{
					$table->coop_id = '';
				}

				if (!empty($table->province_id))
				{
					if (is_array($table->province_id))
					{
						$table->province_id = implode(',', $table->province_id);
					}
				}
				else
				{
					$table->province_id = '';
				}

				if (!empty($table->region_id))
				{
					if (is_array($table->region_id))
					{
						$table->region_id = implode(',', $table->region_id);
					}
				}
				else
				{
					$table->region_id = '';
				}

				if (!empty($table->district_id))
				{
					if (is_array($table->district_id))
					{
						$table->district_id = implode(',', $table->district_id);
					}
				}
				else
				{
					$table->district_id = '';
				}

				if (!empty($table->sub_district_id))
				{
					if (is_array($table->sub_district_id))
					{
						$table->sub_district_id = implode(',', $table->sub_district_id);
					}
				}
				else
				{
					$table->sub_district_id = '';
				}

				if (!empty($table->farm_project_id))
				{
					if (is_array($table->farm_project_id))
					{
						$table->farm_project_id = implode(',', $table->farm_project_id);
					}
				}
				else
				{
					$table->farm_project_id = '';
				}

				if (!empty($table->performance_improvement_project_id))
				{
					if (is_array($table->performance_improvement_project_id))
					{
						$table->performance_improvement_project_id = implode(',', $table->performance_improvement_project_id);
					}
				}
				else
				{
					$table->performance_improvement_project_id = '';
				}

				if (!empty($table->milking_system_id))
				{
					if (is_array($table->milking_system_id))
					{
						$table->milking_system_id = implode(',', $table->milking_system_id);
					}
				}
				else
				{
					$table->milking_system_id = '';
				}


				// Trigger the before save event.
				$result = $dispatcher->trigger($this->event_before_save, array($context, &$table, true));

				if (in_array(false, $result, true) || !$table->store())
				{
					throw new Exception($table->getError());
				}

				// Trigger the after save event.
				$dispatcher->trigger($this->event_after_save, array($context, &$table, true));
			}
			else
			{
				throw new Exception($table->getError());
			}
		}

		// Clean cache
		$this->cleanCache();

		return true;
	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @param   JTable  $table  Table Object
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function prepareTable($table)
	{
		jimport('joomla.filter.output');

		if (empty($table->id))
		{
			// Set ordering to the last item if not set
			if (@$table->ordering === '')
			{
				$db = JFactory::getDbo();
				$db->setQuery('SELECT MAX(ordering) FROM #__ed_farm');
				$max             = $db->loadResult();
				$table->ordering = $max + 1;
			}
		}else{
			$this->saveFarmer();
		}
	}




	public function getGAP(){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__ed_farm_gap_certify')
			->where("id='{$_POST[gap_id]}'");
		$db->setQuery($query);
		$result = $db->loadObject();
		return $result;
	}

	public function saveGAP(){
		$data = new StdClass();
		$data->farm_id = $_POST["gap_farm_id"];
		$data->certify_no = $_POST["gap_certify_no"];
		$data->certify_date = formatSQLDate($_POST["gap_certify_date"]);
		$data->expire_date = formatSQLDate($_POST["gap_expire_date"]);
		$data->certify_agency = $_POST["gap_certify_agency"];

		$user = JFactory::getUser();

		if($_POST["gap_id"]==""){
			$data->create_by = $user->id;
			$data->create_datetime = date("Y-m-d");
		}
		$data->update_by = $user->id;
		$data->update_datetime = date("Y-m-d");
		$data->ordering=0;
		$data->state=1;
		$data->checked_out = 0;
		$data->checked_out_time = "0000-00-00 00:00:00";
		$data->created_by = $user->id;

		if($_POST["gap_id"]!=""){
			$data->id = $_POST["gap_id"];
			$updateDataResult = JFactory::getDbo()->updateObject('#__ed_farm_gap_certify', $data, 'id');

			if($updateDataResult){
				return true;
			}else{
				return false;
			}
		
		}else{
			$addDataResult = JFactory::getDbo()->insertObject('#__ed_farm_gap_certify', $data, 'id');

			if($addDataResult){
				return true;
			}else{
				return false;
			}
		}
		
	}

	public function deleteGAP(){
		$db = JFactory::getDbo();
		 
		$query = $db->getQuery(true);
		 
		 
		$query->delete($db->quoteName('#__ed_farm_gap_certify'));
		$query->where("id='{$_POST[gap_id]}'");
		 
		$db->setQuery($query);
		 
		$result = $db->execute();
		if($result){
			return true;
		}else{
			return false;
		}
	}


	public function getTraining(){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__ed_farmer_training')
			->where("id='{$_POST[training_id]}'");
		$db->setQuery($query);
		$result = $db->loadObject();
		return $result;
	}

	public function saveTraining(){
		$data = new StdClass();
		$data->farmer_id = $_POST["training_farmer_id"];
		$data->training_program_id = $_POST["training_training_program_id"];
		$data->training_date = formatSQLDate($_POST["training_training_date"]);
		$data->training_place = $_POST["training_training_place"];
		$data->training_day = $_POST["training_training_day"];

		$user = JFactory::getUser();

		if($_POST["training_id"]==""){
			$data->create_by = $user->id;
			$data->create_datetime = date("Y-m-d");
		}
		$data->update_by = $user->id;
		$data->update_datetime = date("Y-m-d");
		$data->ordering=0;
		$data->state=1;
		$data->checked_out = 0;
		$data->checked_out_time = "0000-00-00 00:00:00";
		$data->created_by = $user->id;

		if($_POST["training_id"]!=""){
			$data->id = $_POST["training_id"];
			$updateDataResult = JFactory::getDbo()->updateObject('#__ed_farmer_training', $data, 'id');

			if($updateDataResult){
				return true;
			}else{
				return false;
			}
		
		}else{
			$addDataResult = JFactory::getDbo()->insertObject('#__ed_farmer_training', $data, 'id');

			if($addDataResult){
				return true;
			}else{
				return false;
			}
		}
		
	}

	public function deleteTraining(){
		$db = JFactory::getDbo();
		 
		$query = $db->getQuery(true);
		 
		 
		$query->delete($db->quoteName('#__ed_farmer_training'));
		$query->where("id='{$_POST[training_id]}'");
		 
		$db->setQuery($query);
		 
		$result = $db->execute();
		if($result){
			return true;
		}else{
			return false;
		}
	}



	public function getOwner(){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__ed_farm_has_farmer')
			->where("id='{$_POST[owner_id]}'");
		$db->setQuery($query);
		$result = $db->loadObject();
		return $result;
	}

	public function saveOwner(){
		$data = new StdClass();
		$data->farm_id = $_POST["owner_farm_id"];
		$data->farmer_id = $_POST["owner_farmer_id"];
		$data->own_date = formatSQLDate($_POST["owner_own_date"]);

		$user = JFactory::getUser();

		
		$data->ordering=0;
		$data->state=1;
		$data->checked_out = 0;
		$data->checked_out_time = "0000-00-00 00:00:00";
		$data->created_by = $user->id;

		if($_POST["owner_id"]!=""){
			$data->id = $_POST["owner_id"];
			$updateDataResult = JFactory::getDbo()->updateObject('#__ed_farm_has_farmer', $data, 'id');

			if($updateDataResult){
				return true;
			}else{
				return false;
			}
		
		}else{
			$addDataResult = JFactory::getDbo()->insertObject('#__ed_farm_has_farmer', $data, 'id');

			if($addDataResult){
				return true;
			}else{
				return false;
			}
		}
		
	}

	public function deleteOwner(){
		$db = JFactory::getDbo();
		 
		$query = $db->getQuery(true);
		 
		 
		$query->delete($db->quoteName('#__ed_farm_has_farmer'));
		$query->where("id='{$_POST[owner_id]}'");
		 
		$db->setQuery($query);
		 
		$result = $db->execute();
		if($result){
			return true;
		}else{
			return false;
		}
	}



	public function getFarmer(){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query
            ->select("*")
            ->from('#__ed_farmer')
            ->where("id='{$_POST[farm_id]}'");
        $db->setQuery($query);
        $result = $db->loadObject();
        return $result;
    }

    public function saveFarmer(){



    	if($_POST["farmer_citizen_id"]==""){
    		return false;
    	}
        $data = new StdClass();
        $data->farm_id = $_POST["jform"]["id"];
        $data->citizen_id = $_POST["farmer_citizen_id"];

        // IMAGE URL
        if($_FILES["upload_file"]["name"]!=""){
	        $filename = time() . "_"  . $_FILES["upload_file"]["name"];
		    $folder = JPATH_SITE . "/" . "upload";

		
		    $src = $_FILES["upload_file"]['tmp_name'];
		    $dest = $folder . "/" . $filename;

		    if (isset($_FILES["upload_file"])) {
		        move_uploaded_file($src, $dest);
		    }

		    $data->image_url = $filename;
		}

        $data->title_id = $_POST["farmer_title_id"];
        $data->name = $_POST["farmer_name"];
        $data->surname = $_POST["farmer_surname"];
        $data->birthdate = formatSQLDate($_POST["farmer_birthdate"]);
        $data->education_level_id = $_POST["farmer_education_level_id"];
        $data->address = $_POST["farmer_address"];
        $data->region_id = $_POST["farmer_region_id"];
        $data->province_id = $_POST["farmer_province_id"];
        $data->district_id = $_POST["farmer_district_id"];
        $data->sub_district_id = $_POST["farmer_sub_district_id"];
        $data->post_code = $_POST["farmer_post_code"];
        $data->latitude = $_POST["farmer_latitude"];
        $data->longitude = $_POST["farmer_longitude"];
        $data->mobile = $_POST["farmer_mobile"];
        $data->fax = $_POST["farmer_fax"];
        $data->email = $_POST["farmer_email"];
        $data->line_id = $_POST["farmer_line_id"];

        $user = JFactory::getUser();

       
        $data->ordering=0;
        $data->state=1;
        $data->checked_out = 0;
        $data->checked_out_time = "0000-00-00 00:00:00";
        $data->created_by = $user->id;

        if($_POST["farmer_id"]!=""){
            $data->id = $_POST["farmer_id"];
            $updateDataResult = JFactory::getDbo()->updateObject('#__ed_farmer', $data, 'id');

            if($updateDataResult){
                return true;
            }else{
                return false;
            }
        
        }else{
            $addDataResult = JFactory::getDbo()->insertObject('#__ed_farmer', $data, 'id');

            if($addDataResult){

            	// Owner
            	$tempData = $data;
            	$data = new StdClass();
				$data->farm_id = $_POST["jform"]["id"];
				$data->farmer_id = $tempData->id;
				$data->own_date = date("Y-m-d");

				$user = JFactory::getUser();

				
				$data->ordering=0;
				$data->state=1;
				$data->checked_out = 0;
				$data->checked_out_time = "0000-00-00 00:00:00";
				$data->created_by = $user->id;

				$addDataResult = JFactory::getDbo()->insertObject('#__ed_farm_has_farmer', $data, 'id');

				if($addDataResult){
					return true;
				}else{
					return false;
				}

                return true;
            }else{
                return false;
            }
        }
        
    }

    

	public function getMasterData(){

		$masterData = new StdClass();

		if($_REQUEST["layout"]!="farm_survey"){

			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
				->select("*")
				->from('#__ed_training_program')
				->where("state=1");
			$db->setQuery($query);
			$masterData->mas_training_program = $db->loadObjectList();


			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
				->select("*")
				->from('#__ed_region')
				->where("state=1");
			$db->setQuery($query);
			$masterData->mas_region = $db->loadObjectList();


			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
				->select("*")
				->from('#__ed_province')
				->where("state=1");
			$db->setQuery($query);
			$masterData->mas_province = $db->loadObjectList();


			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
				->select("*")
				->from('#__ed_district')
				->where("state=1");
			$db->setQuery($query);
			$masterData->mas_district = $db->loadObjectList();


			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
				->select("*")
				->from('#__ed_sub_district')
				->where("state=1");
			$db->setQuery($query);
			$masterData->mas_sub_district = $db->loadObjectList();

		}
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("id, citizen_id, name, surname")
			->from('#__ed_farmer')
			->where("state=1");
		$db->setQuery($query);
		$masterData->mas_farmer = $db->loadObjectList();


		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("id, name, member_code")
			->from('#__ed_farm')
			->where("state=1");
		$db->setQuery($query);
		$masterData->mas_farm = $db->loadObjectList();


		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__ed_cow_status')
			->where("state=1");
		$db->setQuery($query);
		$masterData->mas_cow_status = $db->loadObjectList();


		return $masterData;

	}



	public function getFarmerInFarm(){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__ed_farmer')
			->where("farm_id='{$_POST[farm_id]}'")
			->limit(1);
		$db->setQuery($query);
		$result = $db->loadObject();
		return $result;
	}



	public function confirmCowStatus(){

		$create_date = date("Y-m-d");

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__ed_farm_survey_transaction')
			->where("farm_id='{$_POST[farm_id]}' and create_date='{$create_date}'");
		$db->setQuery($query);
		$result = $db->loadObjectList();

		$user = JFactory::getUser();


		if(count($result)==0){
			$data = new StdClass();
			$data->farm_id = $_POST["farm_id"];
			$data->create_date = date("Y-m-d");
			$data->survey_by = $user->id;
			$addDataResult = JFactory::getDbo()->insertObject('#__ed_farm_survey_transaction', $data, 'id');
		}


		$data = new StdClass();
		$data->cow_id = $_POST["cow_id"];
		$data->cow_status_id = $_POST["cow_status_id"];
		$data->status_date = date("Y-m-d");
		
		

		
		$data->ordering=0;
		$data->state=1;
		$data->checked_out = 0;
		$data->checked_out_time = "0000-00-00 00:00:00";
		$data->created_by = $user->id;

	
			$addDataResult = JFactory::getDbo()->insertObject('#__ed_cow_has_cow_status', $data, 'id');

			if($addDataResult){

				$data = new StdClass();
				$data->id = $_POST["cow_id"];
				$data->cow_status_id = $_POST["cow_status_id"];
				$addDataResult = JFactory::getDbo()->updateObject('#__ed_cow', $data, 'id');
				
				return true;
			}else{
				return false;
			}
		
		
	}


	public function getDistrictInProvince(){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__ed_district')
			->where("province_id='{$_GET[province_id]}'");
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result;
	}


	public function getSubDistrictInDistrict(){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__ed_sub_district')
			->where("district_id='{$_GET[district_id]}'");
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result;
	}
}
