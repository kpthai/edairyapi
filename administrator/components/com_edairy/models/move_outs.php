<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Edairy records.
 *
 * @since  1.6
 */
class EdairyModelMove_outs extends JModelList
{
/**
	* Constructor.
	*
	* @param   array  $config  An optional associative array of configuration settings.
	*
	* @see        JController
	* @since      1.6
	*/
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.`id`',
				'cow_id', 'a.`cow_id`',
				'move_out_date', 'a.`move_out_date`',
				'move_type', 'a.`move_type`',
				'from_farm_id', 'a.`from_farm_id`',
				'to_farm_id', 'a.`to_farm_id`',
				'move_objective', 'a.`move_objective`',
				'move_reason', 'a.`move_reason`',
				'create_by', 'a.`create_by`',
				'update_by', 'a.`update_by`',
				'create_date', 'a.`create_date`',
				'update_date', 'a.`update_date`',
				'ordering', 'a.`ordering`',
				'state', 'a.`state`',
				'created_by', 'a.`created_by`',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
		$this->setState('filter.state', $published);
		// Filtering cow_id
		$this->setState('filter.cow_id', $app->getUserStateFromRequest($this->context.'.filter.cow_id', 'filter_cow_id', '', 'string'));

		// Filtering move_out_date
		$this->setState('filter.move_out_date.from', $app->getUserStateFromRequest($this->context.'.filter.move_out_date.from', 'filter_from_move_out_date', '', 'string'));
		$this->setState('filter.move_out_date.to', $app->getUserStateFromRequest($this->context.'.filter.move_out_date.to', 'filter_to_move_out_date', '', 'string'));

		// Filtering move_type
		$this->setState('filter.move_type', $app->getUserStateFromRequest($this->context.'.filter.move_type', 'filter_move_type', '', 'string'));

		// Filtering from_farm_id
		$this->setState('filter.from_farm_id', $app->getUserStateFromRequest($this->context.'.filter.from_farm_id', 'filter_from_farm_id', '', 'string'));

		// Filtering to_farm_id
		$this->setState('filter.to_farm_id', $app->getUserStateFromRequest($this->context.'.filter.to_farm_id', 'filter_to_farm_id', '', 'string'));

		// Filtering update_by
		$this->setState('filter.update_by', $app->getUserStateFromRequest($this->context.'.filter.update_by', 'filter_update_by', '', 'string'));

		// Filtering update_date
		$this->setState('filter.update_date.from', $app->getUserStateFromRequest($this->context.'.filter.update_date.from', 'filter_from_update_date', '', 'string'));
		$this->setState('filter.update_date.to', $app->getUserStateFromRequest($this->context.'.filter.update_date.to', 'filter_to_update_date', '', 'string'));


		// Load the parameters.
		$params = JComponentHelper::getParams('com_edairy');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.cow_id', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select', 'DISTINCT a.*'
			)
		);
		$query->from('`#__ed_cow_move_out` AS a');

		// Join over the users for the checked out user
		$query->select("uc.name AS uEditor");
		$query->join("LEFT", "#__users AS uc ON uc.id=a.checked_out");
		// Join over the foreign key 'cow_id'
		$query->select('CONCAT(`#__ed_cow_2698888`.`cow_id`, \' \', `#__ed_cow_2698888`.`name`) AS cows_fk_value_2698888');
		$query->join('LEFT', '#__ed_cow AS #__ed_cow_2698888 ON #__ed_cow_2698888.`id` = a.`cow_id`');
		// Join over the foreign key 'from_farm_id'
		$query->select('CONCAT(`#__ed_farm_2698891`.`member_code`, \' \', `#__ed_farm_2698891`.`name`) AS farms_fk_value_2698891');
		$query->join('LEFT', '#__ed_farm AS #__ed_farm_2698891 ON #__ed_farm_2698891.`id` = a.`from_farm_id`');
		// Join over the foreign key 'to_farm_id'
		$query->select('CONCAT(`#__ed_farm_2698892`.`member_code`, \' \', `#__ed_farm_2698892`.`name`) AS farms_fk_value_2698892');
		$query->join('LEFT', '#__ed_farm AS #__ed_farm_2698892 ON #__ed_farm_2698892.`id` = a.`to_farm_id`');

		// Join over the user field 'create_by'
		$query->select('`create_by`.name AS `create_by`');
		$query->join('LEFT', '#__users AS `create_by` ON `create_by`.id = a.`create_by`');

		// Join over the user field 'update_by'
		$query->select('`update_by`.name AS `update_by`');
		$query->join('LEFT', '#__users AS `update_by` ON `update_by`.id = a.`update_by`');

		// Join over the user field 'created_by'
		$query->select('`created_by`.name AS `created_by`');
		$query->join('LEFT', '#__users AS `created_by` ON `created_by`.id = a.`created_by`');

		// Filter by published state
		$published = $this->getState('filter.state');

		if (is_numeric($published))
		{
			$query->where('a.state = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.state IN (0, 1))');
		}

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				$query->where('(CONCAT(`#__ed_cow_2698888`.`cow_id`, \' \', `#__ed_cow_2698888`.`name`) LIKE ' . $search . '  OR  a.move_objective LIKE ' . $search . '  OR  a.move_reason LIKE ' . $search . ' )');
			}
		}


		//Filtering cow_id
		$filter_cow_id = $this->state->get("filter.cow_id");
		if ($filter_cow_id !== null && !empty($filter_cow_id))
		{
			$query->where("a.`cow_id` = '".$db->escape($filter_cow_id)."'");
		}

		//Filtering move_out_date
		$filter_move_out_date_from = $this->state->get("filter.move_out_date.from");

		if ($filter_move_out_date_from !== null && !empty($filter_move_out_date_from))
		{
			$query->where("a.`move_out_date` >= '".$db->escape($filter_move_out_date_from)."'");
		}
		$filter_move_out_date_to = $this->state->get("filter.move_out_date.to");

		if ($filter_move_out_date_to !== null  && !empty($filter_move_out_date_to))
		{
			$query->where("a.`move_out_date` <= '".$db->escape($filter_move_out_date_to)."'");
		}

		//Filtering move_type
		$filter_move_type = $this->state->get("filter.move_type");
		if ($filter_move_type !== null && !empty($filter_move_type))
		{
			$query->where("a.`move_type` = '".$db->escape($filter_move_type)."'");
		}

		//Filtering from_farm_id
		$filter_from_farm_id = $this->state->get("filter.from_farm_id");
		if ($filter_from_farm_id !== null && !empty($filter_from_farm_id))
		{
			$query->where("a.`from_farm_id` = '".$db->escape($filter_from_farm_id)."'");
		}

		//Filtering to_farm_id
		$filter_to_farm_id = $this->state->get("filter.to_farm_id");
		if ($filter_to_farm_id !== null && !empty($filter_to_farm_id))
		{
			$query->where("a.`to_farm_id` = '".$db->escape($filter_to_farm_id)."'");
		}

		//Filtering update_by
		$filter_update_by = $this->state->get("filter.update_by");
		if ($filter_update_by !== null && !empty($filter_update_by))
		{
			$query->where("a.`update_by` = '".$db->escape($filter_update_by)."'");
		}

		//Filtering update_date
		$filter_update_date_from = $this->state->get("filter.update_date.from");

		if ($filter_update_date_from !== null && !empty($filter_update_date_from))
		{
			$query->where("a.`update_date` >= '".$db->escape($filter_update_date_from)."'");
		}
		$filter_update_date_to = $this->state->get("filter.update_date.to");

		if ($filter_update_date_to !== null  && !empty($filter_update_date_to))
		{
			$query->where("a.`update_date` <= '".$db->escape($filter_update_date_to)."'");
		}
		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');

		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();

		foreach ($items as $oneItem) {

			if (isset($oneItem->cow_id))
			{
				$values = explode(',', $oneItem->cow_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('CONCAT(`#__ed_cow_2698888`.`cow_id`, \' \', `#__ed_cow_2698888`.`name`) AS `fk_value`')
							->from($db->quoteName('#__ed_cow', '#__ed_cow_2698888'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->fk_value;
					}
				}

			$oneItem->cow_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->cow_id;

			}
					$oneItem->move_type = JText::_('COM_EDAIRY_MOVE_OUTS_MOVE_TYPE_OPTION_' . strtoupper($oneItem->move_type));

			if (isset($oneItem->from_farm_id))
			{
				$values = explode(',', $oneItem->from_farm_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('CONCAT(`#__ed_farm_2698891`.`member_code`, \' \', `#__ed_farm_2698891`.`name`) AS `fk_value`')
							->from($db->quoteName('#__ed_farm', '#__ed_farm_2698891'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->fk_value;
					}
				}

			$oneItem->from_farm_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->from_farm_id;

			}

			if (isset($oneItem->to_farm_id))
			{
				$values = explode(',', $oneItem->to_farm_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('CONCAT(`#__ed_farm_2698892`.`member_code`, \' \', `#__ed_farm_2698892`.`name`) AS `fk_value`')
							->from($db->quoteName('#__ed_farm', '#__ed_farm_2698892'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->fk_value;
					}
				}

			$oneItem->to_farm_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->to_farm_id;

			}
		}
		return $items;
	}
}
