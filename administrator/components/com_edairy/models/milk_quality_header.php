<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Edairy model.
 *
 * @since  1.6
 */
class EdairyModelMilk_quality_header extends JModelAdmin
{
	/**
	 * @var      string    The prefix to use with controller messages.
	 * @since    1.6
	 */
	protected $text_prefix = 'COM_EDAIRY';

	/**
	 * @var   	string  	Alias to manage history control
	 * @since   3.2
	 */
	public $typeAlias = 'com_edairy.milk_quality_header';

	/**
	 * @var null  Item data
	 * @since  1.6
	 */
	protected $item = null;

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param   string  $type    The table type to instantiate
	 * @param   string  $prefix  A prefix for the table class name. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return    JTable    A database object
	 *
	 * @since    1.6
	 */
	public function getTable($type = 'Milk_quality_header', $prefix = 'EdairyTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      An optional array of data for the form to interogate.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  JForm  A JForm object on success, false on failure
	 *
	 * @since    1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Initialise variables.
		$app = JFactory::getApplication();

		// Get the form.
		$form = $this->loadForm(
			'com_edairy.milk_quality_header', 'milk_quality_header',
			array('control' => 'jform',
				'load_data' => $loadData
			)
		);

		if (empty($form))
		{
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return   mixed  The data for the form.
	 *
	 * @since    1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_edairy.edit.milk_quality_header.data', array());

		if (empty($data))
		{
			if ($this->item === null)
			{
				$this->item = $this->getItem();
			}

			$data = $this->item;
		}

		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param   integer  $pk  The id of the primary key.
	 *
	 * @return  mixed    Object on success, false on failure.
	 *
	 * @since    1.6
	 */
	public function getItem($pk = null)
	{
		if ($item = parent::getItem($pk))
		{
			// Do any procesing on fields here if needed
			$value = $item->id;
			$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('*')
							->from("#__ed_milk_quality_transaction")
							->where("milk_quality_id='{$value}'");
					$db->setQuery($query);
					$results = $db->loadObjectList();

			$value = $item->coop_id;
			$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('*')
							->from("#__ed_coop")
							->where("id='{$value}'");
					$db->setQuery($query);
					$resultCoop = $db->loadObject();
					$item->coop_name = $resultCoop->name;



					foreach($results as $rkey=>$rdata){
						$query = $db->getQuery(true);
						$query
								->select('*')
								->from("#__ed_farmer")
								->where("id='{$rdata->farmer_id}'");
						$db->setQuery($query);
						$result = $db->loadObject();
						$rdata->farmer = $result;

						$query = $db->getQuery(true);
						$query
								->select('*')
								->from("#__ed_farm")
								->where("id='{$rdata->farmer->farm_id}'");
						$db->setQuery($query);
						$result = $db->loadObject();
						$rdata->farm = $result;
					}

				$item->milk_quality_transaction = $results;
		}

		return $item;
	}

	/**
	 * Method to duplicate an Milk_quality_header
	 *
	 * @param   array  &$pks  An array of primary key IDs.
	 *
	 * @return  boolean  True if successful.
	 *
	 * @throws  Exception
	 */
	public function duplicate(&$pks)
	{
		$user = JFactory::getUser();

		// Access checks.
		if (!$user->authorise('core.create', 'com_edairy'))
		{
			throw new Exception(JText::_('JERROR_CORE_CREATE_NOT_PERMITTED'));
		}

		$dispatcher = JEventDispatcher::getInstance();
		$context    = $this->option . '.' . $this->name;

		// Include the plugins for the save events.
		JPluginHelper::importPlugin($this->events_map['save']);

		$table = $this->getTable();

		foreach ($pks as $pk)
		{
			if ($table->load($pk, true))
			{
				// Reset the id to create a new record.
				$table->id = 0;

				if (!$table->check())
				{
					throw new Exception($table->getError());
				}
				
				if (!empty($table->coop_id))
				{
					if (is_array($table->coop_id))
					{
						$table->coop_id = implode(',', $table->coop_id);
					}
				}
				else
				{
					$table->coop_id = '';
				}


				// Trigger the before save event.
				$result = $dispatcher->trigger($this->event_before_save, array($context, &$table, true));

				if (in_array(false, $result, true) || !$table->store())
				{
					throw new Exception($table->getError());
				}

				// Trigger the after save event.
				$dispatcher->trigger($this->event_after_save, array($context, &$table, true));
			}
			else
			{
				throw new Exception($table->getError());
			}
		}

		// Clean cache
		$this->cleanCache();

		return true;
	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @param   JTable  $table  Table Object
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function prepareTable($table)
	{
		jimport('joomla.filter.output');

		$table->from_date = formatSQLDate($table->from_date);
		$table->to_date = formatSQLDate($table->to_date);
		
		if (empty($table->id))
		{

			// RUN NO

					$db = JFactory::getDbo();

					$query = $db->getQuery(true);

					$query
							->select('*')
							->from("#__ed_coop")
							->where("id ='{$table->coop_id}' ");
					$db->setQuery($query);
					$coop= $db->loadObject();

					$query = $db->getQuery(true);
					
					$month_run_no = date("m");
					$year_run_no = substr(date("Y"), 2, 2)+43;
					$query
							->select('*')
							->from("#__ed_milk_coop_quality")
							->where("run_no LIKE '{$coop->coop_abbr}-QC{$year_run_no}{$month_run_no}%' ")
							->order("run_no desc");
					$db->setQuery($query);
					$last_row= $db->loadObject();

					/*if($i!=0){
						echo $query;
						print_r($last_row);
						die();
					}*/
					if($last_row==null){
						$run_no = "{$coop->coop_abbr}-QC{$year_run_no}{$month_run_no}001";
					}else{
						$last_row_run_no = sprintf('%03d', intval(substr($last_row->run_no, -3))+1);

						$run_no = "{$coop->coop_abbr}-QC{$year_run_no}{$month_run_no}" . $last_row_run_no;
					}

					$table->run_no = $run_no;
					
			// Set ordering to the last item if not set
			if (@$table->ordering === '')
			{
				$db = JFactory::getDbo();
				$db->setQuery('SELECT MAX(ordering) FROM #__ed_milk_quality_header');
				$max             = $db->loadResult();
				$table->ordering = $max + 1;
			}
		}
	}


	public function getMasterData(){

		

		$masterData = new StdClass();

		if($this->item->coop_id==""){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
				->select("*")
				->from('#__ed_farmer')
				->where("state=1");
			$db->setQuery($query);
			$masterData->mas_farmer = $db->loadObjectList();
		}else{
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
				->select("f.*")
				->from('#__ed_farmer f')
				->join("left", "#__ed_farm fm on fm.id=f.farm_id")
				->where("f.state=1 and coop_id='{$this->item->coop_id}'");
			$db->setQuery($query);
			$masterData->mas_farmer = $db->loadObjectList();
		}


		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__ed_milk_quality_parameter')
			->where("state=1")
			->order("create_date desc")
			->limit("1");
		$db->setQuery($query);
		$masterData->mas_milk_price = $db->loadObject();

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__ed_milk_quality_parameter_config')
			->where("state=1 and milk_quality_parameter_id='{$masterData->mas_milk_price->id}'")
			->limit("1");
		$db->setQuery($query);
		$masterData->mas_milk_price_config = $db->loadObject();

		return $masterData;

	}


	public function getMilkQualityTransaction(){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__ed_milk_quality_transaction')
			->where("id='{$_POST[milk_quality_transaction_id]}'");
		$db->setQuery($query);
		$result = $db->loadObject();
		return $result;
	}

	public function saveMilkQualityTransaction(){

		$data = new StdClass();
		$data->milk_quality_id= $_POST["milk_quality_id"];
		$data->create_date = date("Y-m-d");
		$data->create_time = date("H:i:s");
		$data->farmer_id = $_POST["farmer_id"];
		$data->mb = $_POST["mb"];
		$data->scc = $_POST["scc"];
		$data->fat = $_POST["fat"];
		$data->snf = $_POST["snf"];
		$data->ts = $_POST["ts"];
		$data->protein = $_POST["protein"];
		$data->lactose = $_POST["lactose"];
		$data->price = $_POST["price"];
		$data->total_price = $_POST["total_price"];

		$user = JFactory::getUser();

		
		$data->ordering=0;
		$data->state=1;
		$data->checked_out = 0;
		$data->checked_out_time = "0000-00-00 00:00:00";
		$data->created_by = $user->id;

		if($_POST["milk_quality_transaction_id"]!=""){
			$data->id = $_POST["milk_quality_transaction_id"];
			$updateDataResult = JFactory::getDbo()->updateObject('#__ed_milk_quality_transaction', $data, 'id');

			if($updateDataResult){
				return true;
			}else{
				return false;
			}
		
		}else{
			$addDataResult = JFactory::getDbo()->insertObject('#__ed_milk_quality_transaction', $data, 'id');

			if($addDataResult){
				return true;
			}else{
				return false;
			}
		}
	}

	public function importExcel(){

	    $filename = time() . "_"  . $_FILES["upload_file"]["name"];
	    $folder = JPATH_SITE . "/" . "upload";

	
	    $src = $_FILES["upload_file"]['tmp_name'];
	    $dest = $folder . "/" . $filename;

	    if (isset($_FILES["upload_file"])) {
	        move_uploaded_file($src, $dest);
	        return $filename;
	    }

	    return false;

	}


	public function getFarmStandardScore(){

		$farmStandardScore = new StdClass();

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__ed_farm_gap_certify')
			->where("farm_id=(select farm_id from #__ed_farmer where id='{$_POST[farmer_id]}') and expire_date >= NOW()")
			->order("expire_date desc")
			->limit(1);
		$db->setQuery($query);
		$result = $db->loadObject();

		$farmStandardScore->gap = $result;

		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__ed_farm_score')
			->where("farm_id=(select farm_id from #__ed_farmer where id='{$_POST[farmer_id]}')")
			->order("assessment_date desc")
			->limit(1);
		$db->setQuery($query);
		$result = $db->loadObject();

		$farmStandardScore->farm_score = $result;


		
		return $farmStandardScore;
	}
}
