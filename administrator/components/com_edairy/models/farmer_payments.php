<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Edairy records.
 *
 * @since  1.6
 */
class EdairyModelFarmer_payments extends JModelList
{
/**
	* Constructor.
	*
	* @param   array  $config  An optional associative array of configuration settings.
	*
	* @see        JController
	* @since      1.6
	*/
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.`id`',
				'create_date', 'a.`create_date`',
				'month', 'a.`month`',
				'year', 'a.`year`',
				'milk_amount1', 'a.`milk_amount1`',
				'milk_price1', 'a.`milk_price1`',
				'milk_amount2', 'a.`milk_amount2`',
				'milk_price2', 'a.`milk_price2`',
				'other_income', 'a.`other_income`',
				'food_amount', 'a.`food_amount`',
				'food_price', 'a.`food_price`',
				'concentrate_food', 'a.`concentrate_food`',
				'milk_powder_amount', 'a.`milk_powder_amount`',
				'milk_powder_price', 'a.`milk_powder_price`',
				'coarse_food_amount', 'a.`coarse_food_amount`',
				'fix_expense', 'a.`fix_expense`',
				'other_expense', 'a.`other_expense`',
				'ordering', 'a.`ordering`',
				'state', 'a.`state`',
				'created_by', 'a.`created_by`',
				'modified_by', 'a.`modified_by`',
				'expense1', 'a.`expense1`',
				'expense2', 'a.`expense2`',
				'expense3', 'a.`expense3`',
				'expense4', 'a.`expense4`',
				'expense5', 'a.`expense5`',
				'expense6', 'a.`expense6`',
				'expense7', 'a.`expense7`',
				'expense8', 'a.`expense8`',
				'expense9', 'a.`expense9`',
				'expense10', 'a.`expense10`',
				'expense11', 'a.`expense11`',
				'expense12', 'a.`expense12`',
				'expense13', 'a.`expense13`',
				'expense14', 'a.`expense14`',
				'expense15', 'a.`expense15`',
				'expense16', 'a.`expense16`',
				'expense17', 'a.`expense17`',
				'expense18', 'a.`expense18`',
				'expense19', 'a.`expense19`',
				'expense20', 'a.`expense20`',
				'expense21', 'a.`expense21`',
				'expense22', 'a.`expense22`',
				'expense23', 'a.`expense23`',
				'expense24', 'a.`expense24`',
				'expense25', 'a.`expense25`',
				'expense26', 'a.`expense26`',
				'expense27', 'a.`expense27`',
				'expense28', 'a.`expense28`',
				'expense29', 'a.`expense29`',
				'expense30', 'a.`expense30`',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
		$this->setState('filter.state', $published);
		// Filtering create_date
		$this->setState('filter.create_date.from', $app->getUserStateFromRequest($this->context.'.filter.create_date.from', 'filter_from_create_date', '', 'string'));
		$this->setState('filter.create_date.to', $app->getUserStateFromRequest($this->context.'.filter.create_date.to', 'filter_to_create_date', '', 'string'));


		// Load the parameters.
		$params = JComponentHelper::getParams('com_edairy');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.create_date', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select', 'DISTINCT a.*'
			)
		);
		$query->from('`#__ed_farmer_payment` AS a');

		// Join over the users for the checked out user
		$query->select("uc.name AS uEditor");
		$query->join("LEFT", "#__users AS uc ON uc.id=a.checked_out");

		// Join over the user field 'created_by'
		$query->select('`created_by`.name AS `created_by`');
		$query->join('LEFT', '#__users AS `created_by` ON `created_by`.id = a.`created_by`');

		// Join over the user field 'modified_by'
		$query->select('`modified_by`.name AS `modified_by`');
		$query->join('LEFT', '#__users AS `modified_by` ON `modified_by`.id = a.`modified_by`');

		// Filter by published state
		$published = $this->getState('filter.state');

		if (is_numeric($published))
		{
			$query->where('a.state = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.state IN (0, 1))');
		}

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				$query->where('( a.create_date LIKE ' . $search . '  OR  a.year LIKE ' . $search . ' )');
			}
		}


		// Filtering create_date
		$filter_create_date_from = $this->state->get("filter.create_date.from");

		if ($filter_create_date_from !== null && !empty($filter_create_date_from))
		{
			$query->where("a.`create_date` >= '".$db->escape($filter_create_date_from)."'");
		}
		$filter_create_date_to = $this->state->get("filter.create_date.to");

		if ($filter_create_date_to !== null  && !empty($filter_create_date_to))
		{
			$query->where("a.`create_date` <= '".$db->escape($filter_create_date_to)."'");
		}



		//Custom Filter
		if ($_REQUEST["search_coop"]!="")
		{

			$query->where("a.farmer_id in (select id from #__ed_farmer where  farm_id in (select id from #__ed_farm f where f.coop_id in (select id from #__ed_coop c where c.name like '%{$_REQUEST[search_coop]}%' or c.coop_code like '%{$_REQUEST[search_coop]}%' )  ))");
		}

		if ($_REQUEST["search_member_code"]!="")
		{
			$query->where("a.farmer_id in (select id from #__ed_farmer where  farm_id in (select id from #__ed_farm f where f.member_code like '%{$_REQUEST[search_member_code]}%' ))");
		}


		

		if ($_REQUEST["search_farmer_name"]!="")
		{
			$query->where("a.farmer_id in (select id from #__ed_farmer f where f.name like '%{$_REQUEST[search_farmer_name]}%')");

		}

		if ($_REQUEST["search_farmer_surname"]!="")
		{
			$query->where("a.farmer_id in (select id from #__ed_farmer f where f.surname like '%{$_REQUEST[search_farmer_surname]}%')");
		}

		if ($_REQUEST["search_month"]!="")
		{
			$query->where("a.month like '{$_REQUEST[search_month]}'");
		}

		if ($_REQUEST["search_year"]!="")
		{
			$query->where("a.year like '{$_REQUEST[search_year]}'");
		}

		

		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');

		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();
		foreach ($items as $oneItem) {

			$farmer_id = $oneItem->farmer_id;
			if (isset($oneItem->farmer_id))
			{
				$values = explode(',', $oneItem->farmer_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('CONCAT(`#__ed_farmer_2754452`.`citizen_id`, \' \', `#__ed_farmer_2754452`.`name`, \' \', `#__ed_farmer_2754452`.`surname`) AS `fk_value`')
							->from($db->quoteName('#__ed_farmer', '#__ed_farmer_2754452'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->fk_value;
					}
				}

			$oneItem->farmer_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->farmer_id;

			}


			$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('*')
							->from("#__ed_farmer")
							->where("id='{$farmer_id}'")
							->limit(1);
					$db->setQuery($query);
					$results = $db->loadObject();

					$farmer_data = $results;



					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('*')
							->from("#__ed_farm")
							->where("id='{$farmer_data->farm_id}'")
							->limit(1);
					$db->setQuery($query);
					$results = $db->loadObject();

					$farm_data = $results;


					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('*')
							->from("#__ed_coop")
							->where("id='{$farm_data->coop_id}'")
							->limit(1);
					$db->setQuery($query);
					$results = $db->loadObject();

					$coop_data = $results;

					$oneItem->coop = $coop_data->coop_code . " " . $coop_data->coop->abbr . " " . $coop_data->name;

					$oneItem->farm = $farm_data->member_code . " " . $farm_data->name;
		}

		return $items;
	}
}
