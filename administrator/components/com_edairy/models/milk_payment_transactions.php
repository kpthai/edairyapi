<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Edairy records.
 *
 * @since  1.6
 */
class EdairyModelMilk_payment_transactions extends JModelList
{
/**
	* Constructor.
	*
	* @param   array  $config  An optional associative array of configuration settings.
	*
	* @see        JController
	* @since      1.6
	*/
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.`id`',
				'create_date', 'a.`create_date`',
				'milk_payment_id', 'a.`milk_payment_id`',
				'farmer_id', 'a.`farmer_id`',
				'milk_amount', 'a.`milk_amount`',
				'milk_price', 'a.`milk_price`',
				'total_income', 'a.`total_income`',
				'health_insemination_service', 'a.`health_insemination_service`',
				'tax', 'a.`tax`',
				'total_outcome', 'a.`total_outcome`',
				'grand_total', 'a.`grand_total`',
				'ordering', 'a.`ordering`',
				'state', 'a.`state`',
				'created_by', 'a.`created_by`',
				'modified_by', 'a.`modified_by`',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
		$this->setState('filter.state', $published);
		// Filtering create_date
		$this->setState('filter.create_date.from', $app->getUserStateFromRequest($this->context.'.filter.create_date.from', 'filter_from_create_date', '', 'string'));
		$this->setState('filter.create_date.to', $app->getUserStateFromRequest($this->context.'.filter.create_date.to', 'filter_to_create_date', '', 'string'));

		// Filtering milk_payment_id
		$this->setState('filter.milk_payment_id', $app->getUserStateFromRequest($this->context.'.filter.milk_payment_id', 'filter_milk_payment_id', '', 'string'));

		// Filtering farmer_id
		$this->setState('filter.farmer_id', $app->getUserStateFromRequest($this->context.'.filter.farmer_id', 'filter_farmer_id', '', 'string'));


		// Load the parameters.
		$params = JComponentHelper::getParams('com_edairy');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.create_date', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select', 'DISTINCT a.*'
			)
		);
		$query->from('`#__ed_milk_payment_transaction` AS a');

		// Join over the users for the checked out user
		$query->select("uc.name AS uEditor");
		$query->join("LEFT", "#__users AS uc ON uc.id=a.checked_out");
		// Join over the foreign key 'milk_payment_id'
		$query->select('CONCAT(`#__ed_milk_payment_header_2781630`.`coop_id`, \' \', `#__ed_milk_payment_header_2781630`.`period`, \' \', `#__ed_milk_payment_header_2781630`.`from_date`, \' \', `#__ed_milk_payment_header_2781630`.`to_date`) AS milk_payment_headers_fk_value_2781630');
		$query->join('LEFT', '#__ed_milk_payment_header AS #__ed_milk_payment_header_2781630 ON #__ed_milk_payment_header_2781630.`id` = a.`milk_payment_id`');
		// Join over the foreign key 'farmer_id'
		$query->select('CONCAT(`#__ed_farmer_2781610`.`citizen_id`, \' \', `#__ed_farmer_2781610`.`name`, \' \', `#__ed_farmer_2781610`.`surname`) AS farmers_fk_value_2781610');
		$query->join('LEFT', '#__ed_farmer AS #__ed_farmer_2781610 ON #__ed_farmer_2781610.`id` = a.`farmer_id`');

		// Join over the user field 'created_by'
		$query->select('`created_by`.name AS `created_by`');
		$query->join('LEFT', '#__users AS `created_by` ON `created_by`.id = a.`created_by`');

		// Join over the user field 'modified_by'
		$query->select('`modified_by`.name AS `modified_by`');
		$query->join('LEFT', '#__users AS `modified_by` ON `modified_by`.id = a.`modified_by`');

		// Filter by published state
		$published = $this->getState('filter.state');

		if (is_numeric($published))
		{
			$query->where('a.state = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.state IN (0, 1))');
		}

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				$query->where('( a.create_date LIKE ' . $search . '  OR CONCAT(`#__ed_milk_payment_header_2781630`.`coop_id`, \' \', `#__ed_milk_payment_header_2781630`.`period`, \' \', `#__ed_milk_payment_header_2781630`.`from_date`, \' \', `#__ed_milk_payment_header_2781630`.`to_date`) LIKE ' . $search . '  OR CONCAT(`#__ed_farmer_2781610`.`citizen_id`, \' \', `#__ed_farmer_2781610`.`name`, \' \', `#__ed_farmer_2781610`.`surname`) LIKE ' . $search . '  OR  a.milk_amount LIKE ' . $search . '  OR  a.milk_price LIKE ' . $search . '  OR  a.total_income LIKE ' . $search . '  OR  a.health_insemination_service LIKE ' . $search . '  OR  a.tax LIKE ' . $search . '  OR  a.total_outcome LIKE ' . $search . '  OR  a.grand_total LIKE ' . $search . ' )');
			}
		}


		// Filtering create_date
		$filter_create_date_from = $this->state->get("filter.create_date.from");

		if ($filter_create_date_from !== null && !empty($filter_create_date_from))
		{
			$query->where("a.`create_date` >= '".$db->escape($filter_create_date_from)."'");
		}
		$filter_create_date_to = $this->state->get("filter.create_date.to");

		if ($filter_create_date_to !== null  && !empty($filter_create_date_to))
		{
			$query->where("a.`create_date` <= '".$db->escape($filter_create_date_to)."'");
		}

		// Filtering milk_payment_id
		$filter_milk_payment_id = $this->state->get("filter.milk_payment_id");

		if ($filter_milk_payment_id !== null && !empty($filter_milk_payment_id))
		{
			$query->where("a.`milk_payment_id` = '".$db->escape($filter_milk_payment_id)."'");
		}

		// Filtering farmer_id
		$filter_farmer_id = $this->state->get("filter.farmer_id");

		if ($filter_farmer_id !== null && !empty($filter_farmer_id))
		{
			$query->where("a.`farmer_id` = '".$db->escape($filter_farmer_id)."'");
		}
		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');

		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();

		foreach ($items as $oneItem)
		{

			if (isset($oneItem->milk_payment_id))
			{
				$values = explode(',', $oneItem->milk_payment_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('CONCAT(`#__ed_milk_payment_header_2781630`.`coop_id`, \' \', `#__ed_milk_payment_header_2781630`.`period`, \' \', `#__ed_milk_payment_header_2781630`.`from_date`, \' \', `#__ed_milk_payment_header_2781630`.`to_date`) AS `fk_value`')
							->from($db->quoteName('#__ed_milk_payment_header', '#__ed_milk_payment_header_2781630'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->fk_value;
					}
				}

			$oneItem->milk_payment_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->milk_payment_id;

			}

			if (isset($oneItem->farmer_id))
			{
				$values = explode(',', $oneItem->farmer_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('CONCAT(`#__ed_farmer_2781610`.`citizen_id`, \' \', `#__ed_farmer_2781610`.`name`, \' \', `#__ed_farmer_2781610`.`surname`) AS `fk_value`')
							->from($db->quoteName('#__ed_farmer', '#__ed_farmer_2781610'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->fk_value;
					}
				}

			$oneItem->farmer_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->farmer_id;

			}
		}

		return $items;
	}
}
