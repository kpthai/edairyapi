<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Edairy model.
 *
 * @since  1.6
 */
class EdairyModelReceive_milk_quantity_cow extends JModelAdmin
{
	/**
	 * @var      string    The prefix to use with controller messages.
	 * @since    1.6
	 */
	protected $text_prefix = 'COM_EDAIRY';

	/**
	 * @var   	string  	Alias to manage history control
	 * @since   3.2
	 */
	public $typeAlias = 'com_edairy.receive_milk_quantity_cow';

	/**
	 * @var null  Item data
	 * @since  1.6
	 */
	protected $item = null;

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param   string  $type    The table type to instantiate
	 * @param   string  $prefix  A prefix for the table class name. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return    JTable    A database object
	 *
	 * @since    1.6
	 */
	public function getTable($type = 'Receive_milk_quantity_cow', $prefix = 'EdairyTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      An optional array of data for the form to interogate.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  JForm  A JForm object on success, false on failure
	 *
	 * @since    1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Initialise variables.
		$app = JFactory::getApplication();

		// Get the form.
		$form = $this->loadForm(
			'com_edairy.receive_milk_quantity_cow', 'receive_milk_quantity_cow',
			array('control' => 'jform',
				'load_data' => $loadData
			)
		);

		if (empty($form))
		{
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return   mixed  The data for the form.
	 *
	 * @since    1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_edairy.edit.receive_milk_quantity_cow.data', array());

		if (empty($data))
		{
			if ($this->item === null)
			{
				$this->item = $this->getItem();
			}

			$data = $this->item;
		}

		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param   integer  $pk  The id of the primary key.
	 *
	 * @return  mixed    Object on success, false on failure.
	 *
	 * @since    1.6
	 */
	public function getItem($pk = null)
	{
		if ($item = parent::getItem($pk))
		{
			// Do any procesing on fields here if needed
		}

		return $item;
	}

	/**
	 * Method to duplicate an Receive_milk_quantity_cow
	 *
	 * @param   array  &$pks  An array of primary key IDs.
	 *
	 * @return  boolean  True if successful.
	 *
	 * @throws  Exception
	 */
	public function duplicate(&$pks)
	{
		$user = JFactory::getUser();

		// Access checks.
		if (!$user->authorise('core.create', 'com_edairy'))
		{
			throw new Exception(JText::_('JERROR_CORE_CREATE_NOT_PERMITTED'));
		}

		$dispatcher = JEventDispatcher::getInstance();
		$context    = $this->option . '.' . $this->name;

		// Include the plugins for the save events.
		JPluginHelper::importPlugin($this->events_map['save']);

		$table = $this->getTable();

		foreach ($pks as $pk)
		{
			if ($table->load($pk, true))
			{
				// Reset the id to create a new record.
				$table->id = 0;

				if (!$table->check())
				{
					throw new Exception($table->getError());
				}
				
				if (!empty($table->cow_id))
				{
					if (is_array($table->cow_id))
					{
						$table->cow_id = implode(',', $table->cow_id);
					}
				}
				else
				{
					$table->cow_id = '';
				}


				// Trigger the before save event.
				$result = $dispatcher->trigger($this->event_before_save, array($context, &$table, true));

				if (in_array(false, $result, true) || !$table->store())
				{
					throw new Exception($table->getError());
				}

				// Trigger the after save event.
				$dispatcher->trigger($this->event_after_save, array($context, &$table, true));
			}
			else
			{
				throw new Exception($table->getError());
			}
		}

		// Clean cache
		$this->cleanCache();

		return true;
	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @param   JTable  $table  Table Object
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function prepareTable($table)
	{
		jimport('joomla.filter.output');

		if (empty($table->id))
		{
			// Set ordering to the last item if not set
			if (@$table->ordering === '')
			{
				$db = JFactory::getDbo();
				$db->setQuery('SELECT MAX(ordering) FROM #__ed_receive_milk_quantity_cow');
				$max             = $db->loadResult();
				$table->ordering = $max + 1;
			}
		}
	}

	public function getMasterData(){

		$masterData = new StdClass();

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__ed_farmer')
			->where("state=1");
		$db->setQuery($query);
		$masterData->mas_farmer = $db->loadObjectList();

		return $masterData;

	}

	public function getCowsInFarm(){

		$_POST["create_date"] = formatSQLDate($_POST["create_date"]);

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__ed_cow')
			->where("farm_id=(select farm_id from #__ed_farmer where id = '{$_POST[farmer_id]}')")
			->order("farm_order asc, name asc");
		$db->setQuery($query);
		$results = $db->loadObjectList();

		foreach($results as $key=>$data){
			$query = $db->getQuery(true);
			$query
				->select("*")
				->from('#__ed_receive_milk_quantity_cow')
				->where("cow_id='{$data->id}' and create_date='{$_POST[create_date]}'");
			$db->setQuery($query);
			$result = $db->loadObject();
			$data->receive_milk_quantity = $result;
		}
		return $results;
	}

	public function customSave(){
		$i=0;
		$create_date = formatSQLDate($_POST["jform"]["create_date"]);
		foreach($_REQUEST['cow_id'] as $ckey=>$cdata){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__ed_receive_milk_quantity_cow'));
			$query->where("create_date='{$create_date}' and cow_id='{$cdata}'");
				 
			$db->setQuery($query);
			$result = $db->execute();

				$data = new StdClass();
				$data->cow_id = $cdata;
				$data->create_date = $create_date;
				$data->morning_amount = $_POST["morning_amount"][$i];
				$data->evening_amount = $_POST["evening_amount"][$i];
				$data->total_amount = $_POST["total_amount"][$i];

				$data->state=1;
				$data->ordering=0;
				$data->checked_out = 0;
				$data->checked_out_time = "0000-00-00 00:00:00";



				$user = JFactory::getUser();

				$data->created_by = $user->id;

			
				$addDataResult = JFactory::getDbo()->insertObject('#__ed_receive_milk_quantity_cow', $data, 'id');


				// FARM ORDER

				$data = new StdClass();
				$data->id = $cdata;

				$data->farm_order = $_POST["farm_order"][$i];
				$updateDataResult = JFactory::getDbo()->updateObject('#__ed_cow', $data, 'id');


			$i++;
		}
		return true;
	}
}
