<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Edairy model.
 *
 * @since  1.6
 */
class EdairyModelReceive_milk_quantity extends JModelAdmin
{
	/**
	 * @var      string    The prefix to use with controller messages.
	 * @since    1.6
	 */
	protected $text_prefix = 'COM_EDAIRY';

	/**
	 * @var   	string  	Alias to manage history control
	 * @since   3.2
	 */
	public $typeAlias = 'com_edairy.receive_milk_quantity';

	/**
	 * @var null  Item data
	 * @since  1.6
	 */
	protected $item = null;

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param   string  $type    The table type to instantiate
	 * @param   string  $prefix  A prefix for the table class name. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return    JTable    A database object
	 *
	 * @since    1.6
	 */
	public function getTable($type = 'Receive_milk_quantity', $prefix = 'EdairyTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      An optional array of data for the form to interogate.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  JForm  A JForm object on success, false on failure
	 *
	 * @since    1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Initialise variables.
		$app = JFactory::getApplication();

		// Get the form.
		$form = $this->loadForm(
			'com_edairy.receive_milk_quantity', 'receive_milk_quantity',
			array('control' => 'jform',
				'load_data' => $loadData
			)
		);

		if (empty($form))
		{
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return   mixed  The data for the form.
	 *
	 * @since    1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_edairy.edit.receive_milk_quantity.data', array());

		if (empty($data))
		{
			if ($this->item === null)
			{
				$this->item = $this->getItem();
			}

			$data = $this->item;
		}

		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param   integer  $pk  The id of the primary key.
	 *
	 * @return  mixed    Object on success, false on failure.
	 *
	 * @since    1.6
	 */
	public function getItem($pk = null)
	{
		if ($item = parent::getItem($pk))
		{
			// Do any procesing on fields here if needed

			$value = $item->coop_id;
			$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('CONCAT(`#__ed_coop_2754453`.`coop_code`, \' \', `#__ed_coop_2754453`.`name`) AS `fk_value`')
							->from($db->quoteName('#__ed_coop', '#__ed_coop_2754453'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$result = $db->loadObject();
					$item->coop = $result->fk_value;



			

			$value = $item->farmer_id;
			$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('CONCAT(`#__ed_farmer_2754452`.`name`, \' \', `#__ed_farmer_2754452`.`surname`) AS `fk_value`, farm_id')
							->from($db->quoteName('#__ed_farmer', '#__ed_farmer_2754452'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$result = $db->loadObject();
					$item->farmer = $result->fk_value;


					$value = $result->farm_id;
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('`#__ed_farm_2754451`.`member_code` AS `fk_value`')
							->from($db->quoteName('#__ed_farm', '#__ed_farm_2754451'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$result = $db->loadObject();
					$item->member_code = $result->fk_value;

			$value = $item->id;
			$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('*')
							->from("#__ed_receive_milk_quantity_line")
							->where("receive_milk_quantity_id='{$value}'");
					$db->setQuery($query);
					$results = $db->loadObjectList();

					foreach($results as $k=>$d){
						$db = JFactory::getDbo();
						$query = $db->getQuery(true);
						$query
								->select('*')
								->from("#__ed_cow")
								->where("id='{$d->cow_id}'");
						$db->setQuery($query);
						$cow = $db->loadObject();
						$d->cow=$cow;
					}
					$item->receive_milk_quantity_line = $results;

		}

		return $item;
	}

	/**
	 * Method to duplicate an Receive_milk_quantity
	 *
	 * @param   array  &$pks  An array of primary key IDs.
	 *
	 * @return  boolean  True if successful.
	 *
	 * @throws  Exception
	 */
	public function duplicate(&$pks)
	{
		$user = JFactory::getUser();

		// Access checks.
		if (!$user->authorise('core.create', 'com_edairy'))
		{
			throw new Exception(JText::_('JERROR_CORE_CREATE_NOT_PERMITTED'));
		}

		$dispatcher = JEventDispatcher::getInstance();
		$context    = $this->option . '.' . $this->name;

		// Include the plugins for the save events.
		JPluginHelper::importPlugin($this->events_map['save']);

		$table = $this->getTable();

		foreach ($pks as $pk)
		{
			if ($table->load($pk, true))
			{
				// Reset the id to create a new record.
				$table->id = 0;

				if (!$table->check())
				{
					throw new Exception($table->getError());
				}
				
				if (!empty($table->coop_id))
				{
					if (is_array($table->coop_id))
					{
						$table->coop_id = implode(',', $table->coop_id);
					}
				}
				else
				{
					$table->coop_id = '';
				}

				if (!empty($table->farmer_id))
				{
					if (is_array($table->farmer_id))
					{
						$table->farmer_id = implode(',', $table->farmer_id);
					}
				}
				else
				{
					$table->farmer_id = '';
				}


				// Trigger the before save event.
				$result = $dispatcher->trigger($this->event_before_save, array($context, &$table, true));

				if (in_array(false, $result, true) || !$table->store())
				{
					throw new Exception($table->getError());
				}

				// Trigger the after save event.
				$dispatcher->trigger($this->event_after_save, array($context, &$table, true));
			}
			else
			{
				throw new Exception($table->getError());
			}
		}

		// Clean cache
		$this->cleanCache();

		return true;
	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @param   JTable  $table  Table Object
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function prepareTable($table)
	{
		jimport('joomla.filter.output');

		$table->create_datetime = formatSQLDate($table->create_datetime);

		if (empty($table->id))
		{
			// Set ordering to the last item if not set
			if (@$table->ordering === '')
			{
				$db = JFactory::getDbo();
				$db->setQuery('SELECT MAX(ordering) FROM #__ed_receive_milk_quantity');
				$max             = $db->loadResult();
				$table->ordering = $max + 1;
			}
		}
	}

	public function saveReceiveMilkQuantityLine($saved_item){

		$db = JFactory::getDbo();
		 
		$query = $db->getQuery(true);
		 
		 
		$query->delete($db->quoteName('#__ed_receive_milk_quantity_line'));
		$query->where("receive_milk_quantity_id='{$saved_item->id}'");
		 
		$db->setQuery($query);
		$result = $db->execute();



		for($i=0;$i<count($_POST["amount"]);$i++){
			if($_POST["amount"][$i]!="" && !in_array($i, $_POST["delete_mark"])){
				$data = new StdClass();
				$data->amount = $_POST["amount"][$i];
				$data->farmer_id = $_POST["farmer_id"][$i];
				
				$data->state=1;
				$data->ordering=0;
				$data->checked_out = 0;
				$data->checked_out_time = "0000-00-00 00:00:00";

				//FK
				$data->receive_milk_quantity_id = $saved_item->id;

				$user = JFactory::getUser();

				$data->created_by = $user->id;

			
				$addDataResult = JFactory::getDbo()->insertObject('#__ed_receive_milk_quantity_line', $data, 'id');
			}
		}

			
	}


	public function importExcel(){

	    $filename = time() . "_"  . $_FILES["upload_file"]["name"];
	    $folder = JPATH_SITE . "/" . "upload";

	
	    $src = $_FILES["upload_file"]['tmp_name'];
	    $dest = $folder . "/" . $filename;

	    if (isset($_FILES["upload_file"])) {
	        move_uploaded_file($src, $dest);
	        return $filename;
	    }

	    return false;

	}
}
