<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Edairy records.
 *
 * @since  1.6
 */
class EdairyModelReceive_milk_quantity_cows extends JModelList
{
/**
	* Constructor.
	*
	* @param   array  $config  An optional associative array of configuration settings.
	*
	* @see        JController
	* @since      1.6
	*/
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.`id`',
				'create_date', 'a.`create_date`',
				'cow_id', 'a.`cow_id`',
				'morning_amount', 'a.`morning_amount`',
				'evening_amount', 'a.`evening_amount`',
				'total_amount', 'a.`total_amount`',
				'ordering', 'a.`ordering`',
				'state', 'a.`state`',
				'created_by', 'a.`created_by`',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
		$this->setState('filter.state', $published);
		// Filtering create_date
		$this->setState('filter.create_date.from', $app->getUserStateFromRequest($this->context.'.filter.create_date.from', 'filter_from_create_date', '', 'string'));
		$this->setState('filter.create_date.to', $app->getUserStateFromRequest($this->context.'.filter.create_date.to', 'filter_to_create_date', '', 'string'));


		// Load the parameters.
		$params = JComponentHelper::getParams('com_edairy');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.create_date', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select', 'DISTINCT a.*'
			)
		);
		$query->from('`#__ed_receive_milk_quantity_cow` AS a');

		// Join over the users for the checked out user
		$query->select("uc.name AS uEditor");
		$query->join("LEFT", "#__users AS uc ON uc.id=a.checked_out");
		// Join over the foreign key 'cow_id'
		$query->select('CONCAT(`#__ed_cow_2754613`.`cow_id`, \' \', `#__ed_cow_2754613`.`name`) AS cows_fk_value_2754613');
		$query->join('LEFT', '#__ed_cow AS #__ed_cow_2754613 ON #__ed_cow_2754613.`id` = a.`cow_id`');

		// Join over the user field 'created_by'
		$query->select('`created_by`.name AS `created_by`');
		$query->join('LEFT', '#__users AS `created_by` ON `created_by`.id = a.`created_by`');

		// Filter by published state
		$published = $this->getState('filter.state');

		if (is_numeric($published))
		{
			$query->where('a.state = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.state IN (0, 1))');
		}

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				$query->where('( a.create_date LIKE ' . $search . '  OR  a.morning_amount LIKE ' . $search . '  OR  a.evening_amount LIKE ' . $search . '  OR  a.total_amount LIKE ' . $search . ' )');
			}
		}


		//Filtering create_date
		$filter_create_date_from = $this->state->get("filter.create_date.from");

		if ($filter_create_date_from !== null && !empty($filter_create_date_from))
		{
			$query->where("a.`create_date` >= '".$db->escape($filter_create_date_from)."'");
		}
		$filter_create_date_to = $this->state->get("filter.create_date.to");

		if ($filter_create_date_to !== null  && !empty($filter_create_date_to))
		{
			$query->where("a.`create_date` <= '".$db->escape($filter_create_date_to)."'");
		}

		if ($_REQUEST["search_coop"]!="")
		{

			$query->where("a.cow_id in (select id from #__ed_cow where farm_id in (select id from #__ed_farm f where f.coop_id in ( select id from #__ed_coop c where c.coop_code like '%{$_REQUEST[search_coop]}%' or c.coop_abbr like '%{$_REQUEST[search_coop]}%' or c.name like '%{$_REQUEST[search_coop]}%')))");


		}

		if ($_REQUEST["search_member_code"]!="")
		{
			$query->where("a.cow_id in (select id from #__ed_cow where farm_id in (select id from #__ed_farm f where f.member_code like '%{$_REQUEST[search_member_code]}%'))");
		}

		if ($_REQUEST["search_farmer_name"]!="")
		{
			$query->where("a.cow_id in (select id from #__ed_cow where farm_id in (select farm_id from #__ed_farmer f where f.name like '%{$_REQUEST[search_farmer_name]}%'))");
		}

		if ($_REQUEST["search_farmer_surname"]!="")
		{
			$query->where("a.cow_id in (select id from #__ed_cow where farm_id in (select farm_id from #__ed_farmer f where f.surname like '%{$_REQUEST[search_farmer_surname]}%'))");
		}

		if ($_REQUEST["search_cow_id"]!="")
		{
			$query->where("a.cow_id in (select id from #__ed_cow c where c.cow_id like '%{$_REQUEST[search_cow_id]}%')");
		}

		if ($_REQUEST["search_cow_name"]!="")
		{
			$query->where("a.cow_id in ( select id from #__ed_cow c where c.name like '%{$_REQUEST[search_cow_name]}%')");
		
		}

		if ($_REQUEST["search_date_from"]!="" && $_REQUEST["search_date_to"]!="")
		{
			$date_from = formatSQLDate($_REQUEST["search_date_from"]);
			$date_to = formatSQLDate($_REQUEST["search_date_to"]);
			$query->where("a.create_date between '{$date_from}' and '{$date_to}'");
		}

		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');

		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();

		foreach ($items as $oneItem) {

			if (isset($oneItem->cow_id))
			{
				$values = explode(',', $oneItem->cow_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('CONCAT(`#__ed_cow_2754613`.`cow_id`, \' \', `#__ed_cow_2754613`.`name`) AS `fk_value`, farm_id')
							->from($db->quoteName('#__ed_cow', '#__ed_cow_2754613'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->fk_value;
					}

					$farm_id = $results->farm_id;

					$query = $db->getQuery(true);
					$query
							->select('*')
							->from('#__ed_farmer')
							->where("farm_id='{$farm_id}'");
					$db->setQuery($query);
					$results_farmer = $db->loadObject();


					$query = $db->getQuery(true);
					$query
							->select('*')
							->from('#__ed_farm')
							->where("id='{$farm_id}'");
					$db->setQuery($query);
					$results_farm = $db->loadObject();
				}

			$oneItem->cow_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->cow_id;

			$oneItem->farmer_id = $results_farmer->id;

			$oneItem->farmer = $results_farmer;

			$oneItem->farm = $results_farm;

			}
		}
		return $items;
	}
}
