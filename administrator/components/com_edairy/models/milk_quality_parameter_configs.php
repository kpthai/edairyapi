<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Edairy records.
 *
 * @since  1.6
 */
class EdairyModelMilk_quality_parameter_configs extends JModelList
{
/**
	* Constructor.
	*
	* @param   array  $config  An optional associative array of configuration settings.
	*
	* @see        JController
	* @since      1.6
	*/
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.`id`',
				'ordering', 'a.`ordering`',
				'state', 'a.`state`',
				'created_by', 'a.`created_by`',
				'modified_by', 'a.`modified_by`',
				'mb_range1_low', 'a.`mb_range1_low`',
				'mb_range1_high', 'a.`mb_range1_high`',
				'mb_range1_change', 'a.`mb_range1_change`',
				'mb_range2_low', 'a.`mb_range2_low`',
				'mb_range2_high', 'a.`mb_range2_high`',
				'mb_range2_change', 'a.`mb_range2_change`',
				'mb_range3_low', 'a.`mb_range3_low`',
				'mb_range3_high', 'a.`mb_range3_high`',
				'mb_range3_change', 'a.`mb_range3_change`',
				'mb_range4_low', 'a.`mb_range4_low`',
				'mb_range4_high', 'a.`mb_range4_high`',
				'mb_range4_change', 'a.`mb_range4_change`',
				'mb_range5_low', 'a.`mb_range5_low`',
				'mb_range5_high', 'a.`mb_range5_high`',
				'mb_range5_change', 'a.`mb_range5_change`',
				'mb_range6_low', 'a.`mb_range6_low`',
				'mb_range6_high', 'a.`mb_range6_high`',
				'mb_range6_change', 'a.`mb_range6_change`',
				'mb_range7_low', 'a.`mb_range7_low`',
				'mb_range7_high', 'a.`mb_range7_high`',
				'mb_range7_change', 'a.`mb_range7_change`',
				'mb_range8_low', 'a.`mb_range8_low`',
				'mb_range8_high', 'a.`mb_range8_high`',
				'mb_range8_change', 'a.`mb_range8_change`',
				'mb_range9_low', 'a.`mb_range9_low`',
				'mb_range9_high', 'a.`mb_range9_high`',
				'mb_range9_change', 'a.`mb_range9_change`',
				'mb_range10_low', 'a.`mb_range10_low`',
				'mb_range10_high', 'a.`mb_range10_high`',
				'mb_range10_change', 'a.`mb_range10_change`',
				'scc_range1_low', 'a.`scc_range1_low`',
				'scc_range1_high', 'a.`scc_range1_high`',
				'scc_range1_change', 'a.`scc_range1_change`',
				'scc_range2_low', 'a.`scc_range2_low`',
				'scc_range2_high', 'a.`scc_range2_high`',
				'scc_range2_change', 'a.`scc_range2_change`',
				'scc_range3_low', 'a.`scc_range3_low`',
				'scc_range3_high', 'a.`scc_range3_high`',
				'scc_range3_change', 'a.`scc_range3_change`',
				'scc_range4_low', 'a.`scc_range4_low`',
				'scc_range4_high', 'a.`scc_range4_high`',
				'scc_range4_change', 'a.`scc_range4_change`',
				'scc_range5_low', 'a.`scc_range5_low`',
				'scc_range5_high', 'a.`scc_range5_high`',
				'scc_range5_change', 'a.`scc_range5_change`',
				'scc_range6_low', 'a.`scc_range6_low`',
				'scc_range6_high', 'a.`scc_range6_high`',
				'scc_range6_change', 'a.`scc_range6_change`',
				'scc_range7_low', 'a.`scc_range7_low`',
				'scc_range7_high', 'a.`scc_range7_high`',
				'scc_range7_change', 'a.`scc_range7_change`',
				'scc_range8_low', 'a.`scc_range8_low`',
				'scc_range8_high', 'a.`scc_range8_high`',
				'scc_range8_change', 'a.`scc_range8_change`',
				'scc_range9_low', 'a.`scc_range9_low`',
				'scc_range9_high', 'a.`scc_range9_high`',
				'scc_range9_change', 'a.`scc_range9_change`',
				'scc_range10_low', 'a.`scc_range10_low`',
				'scc_range10_high', 'a.`scc_range10_high`',
				'scc_range10_change', 'a.`scc_range10_change`',
				'fat_range1_low', 'a.`fat_range1_low`',
				'fat_range1_high', 'a.`fat_range1_high`',
				'fat_range1_change', 'a.`fat_range1_change`',
				'fat_range2_low', 'a.`fat_range2_low`',
				'fat_range2_high', 'a.`fat_range2_high`',
				'fat_range2_change', 'a.`fat_range2_change`',
				'fat_range3_low', 'a.`fat_range3_low`',
				'fat_range3_high', 'a.`fat_range3_high`',
				'fat_range3_change', 'a.`fat_range3_change`',
				'fat_range4_low', 'a.`fat_range4_low`',
				'fat_range4_high', 'a.`fat_range4_high`',
				'fat_range4_change', 'a.`fat_range4_change`',
				'fat_range5_low', 'a.`fat_range5_low`',
				'fat_range5_high', 'a.`fat_range5_high`',
				'fat_range5_change', 'a.`fat_range5_change`',
				'fat_range6_low', 'a.`fat_range6_low`',
				'fat_range6_high', 'a.`fat_range6_high`',
				'fat_range6_change', 'a.`fat_range6_change`',
				'fat_range7_low', 'a.`fat_range7_low`',
				'fat_range7_high', 'a.`fat_range7_high`',
				'fat_range7_change', 'a.`fat_range7_change`',
				'fat_range8_low', 'a.`fat_range8_low`',
				'fat_range8_high', 'a.`fat_range8_high`',
				'fat_range8_change', 'a.`fat_range8_change`',
				'fat_range9_low', 'a.`fat_range9_low`',
				'fat_range9_high', 'a.`fat_range9_high`',
				'fat_range9_change', 'a.`fat_range9_change`',
				'fat_range10_low', 'a.`fat_range10_low`',
				'fat_range10_high', 'a.`fat_range10_high`',
				'fat_range10_change', 'a.`fat_range10_change`',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
		$this->setState('filter.state', $published);

		// Load the parameters.
		$params = JComponentHelper::getParams('com_edairy');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.id', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select', 'DISTINCT a.*'
			)
		);
		$query->from('`#__ed_milk_quality_parameter_config` AS a');

		// Join over the users for the checked out user
		$query->select("uc.name AS uEditor");
		$query->join("LEFT", "#__users AS uc ON uc.id=a.checked_out");

		// Join over the user field 'created_by'
		$query->select('`created_by`.name AS `created_by`');
		$query->join('LEFT', '#__users AS `created_by` ON `created_by`.id = a.`created_by`');

		// Join over the user field 'modified_by'
		$query->select('`modified_by`.name AS `modified_by`');
		$query->join('LEFT', '#__users AS `modified_by` ON `modified_by`.id = a.`modified_by`');

		// Filter by published state
		$published = $this->getState('filter.state');

		if (is_numeric($published))
		{
			$query->where('a.state = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.state IN (0, 1))');
		}

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				
			}
		}

		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');

		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();


		return $items;
	}
}
