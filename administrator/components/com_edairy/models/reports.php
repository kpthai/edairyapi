<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Edairy records.
 *
 * @since  1.6
 */
class EdairyModelReports extends JModelList
{
	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
		$this->setState('filter.state', $published);

		// Load the parameters.
		$params = JComponentHelper::getParams('com_edairy');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.cow_id', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		$db	= $this->getDbo();
		$query	= $db->getQuery(true);

		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();


		return $items;
	}


	public function getMasterData(){

		$masterData = new StdClass();
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__ed_coop')
			->where("state=1");
		$db->setQuery($query);
		$masterData->mas_coop = $db->loadObjectList();


		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__ed_farmer')
			->where("state=1");
		$db->setQuery($query);
		$masterData->mas_farmer = $db->loadObjectList();


		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__ed_region')
			->where("state=1");
		$db->setQuery($query);
		$masterData->mas_region = $db->loadObjectList();



		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__ed_province')
			->where("state=1");
		$db->setQuery($query);
		$masterData->mas_province = $db->loadObjectList();


		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__users')
			->where("role=2")
			->order("name asc");
		$db->setQuery($query);
		$masterData->mas_vet = $db->loadObjectList();

		return $masterData;

	}

	public function getReport1(){
		$reportData = new StdClass();

		if($_REQUEST["year"]!="%" && $_REQUEST["year"]!=""){
			$year=$_REQUEST["year"]-543;

		}else{
			$year = "%";
		}

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*, MONTH(create_date) as month")
			->from('#__ed_farmer_payment')
			->group("month")
			->where("MONTH(create_date) like '{$_REQUEST[month]}' and YEAR(create_date) like '{$year}' and farmer_id like '{$_REQUEST[farmer_id]}' and farmer_id in (select id from #__ed_farmer where farm_id in (select id from #__ed_farm where coop_id like '{$_REQUEST[coop_id]}'))")
			->order("create_date asc");
		$db->setQuery($query);
		$reportData->general = $db->loadObjectList();


		$query = $db->getQuery(true);
		$query
			->select("*, MONTH(create_date) as month")
			->from('#__ed_cow_health')
			->group("month")
			->where("MONTH(create_date) like '{$_REQUEST[month]}' and YEAR(create_date) like '{$year}' and farmer_id like '{$_REQUEST[farmer_id]}' and farmer_id in (select id from #__ed_farmer where farm_id in (select id from #__ed_farm where coop_id like '{$_REQUEST[coop_id]}'))")
			->order("create_date asc");
		$db->setQuery($query);
		$reportData->health = $db->loadObjectList();


		$query = $db->getQuery(true);
		$query
			->select("*, MONTH(create_date) as month")
			->from('#__ed_cow_insemination')
			->group("month")
			->where("MONTH(create_date) like '{$_REQUEST[month]}' and YEAR(create_date) like '{$year}' and farmer_id like '{$_REQUEST[farmer_id]}' and farmer_id in (select id from #__ed_farmer where farm_id in (select id from #__ed_farm where coop_id like '{$_REQUEST[coop_id]}'))")
			->order("create_date asc");
		$db->setQuery($query);
		$reportData->insemination = $db->loadObjectList();


		//Milk Quantity - Coop or Farmer?

		$query = $db->getQuery(true);
		$query
			->select("*, MONTH(create_date) as month")
			->from('#__ed_milk_payment_transaction')
			->group("month")
			->where("MONTH(create_date) like '{$_REQUEST[month]}' and YEAR(create_date) like '{$year}' and farmer_id like '{$_REQUEST[farmer_id]}' and farmer_id in (select id from #__ed_farmer where farm_id in (select id from #__ed_farm where coop_id like '{$_REQUEST[coop_id]}'))")
			->order("create_date asc");
		$db->setQuery($query);
		$reportData->milk = $db->loadObjectList();



		return $reportData;
	}


	public function getReport2(){

		$reportData = new StdClass();

		$date_from = formatSQLDate($_REQUEST["date_from"]);
		$date_to = formatSQLDate($_REQUEST["date_to"]);

		//pregnant inspection

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*, DATE_ADD(`insemination_date`, INTERVAL 280 DAY) as schedule_date1, DATE_ADD(`insemination_date`, INTERVAL 220 DAY) as schedule_date2, DATE_ADD(`insemination_date`, INTERVAL 24 DAY) as schedule_date3, DATE_ADD(`insemination_date`, INTERVAL 30 DAY) as schedule_date4")
			->from('#__ed_cow_pregnant_inspection')
			->where("cow_id in (select id from #__ed_cow where farm_id in (select id from #__ed_farm where coop_id in (select id from #__ed_coop where id like '{$_REQUEST[coop_id]}')) and farm_id in (select farm_id from #__ed_farmer where id like '{$_REQUEST[farmer_id]}')) and state=1 and insemination_date between '{$date_from}' and '{$date_to}'");


		$db->setQuery($query);
		$results = $db->loadObjectList();

		foreach($results as $key=>$data){
			$query = $db->getQuery(true);
			$query
				->select("*")
				->from('#__ed_cow')
				->where("id='{$data->cow_id}'");
			$db->setQuery($query);
			$result = $db->loadObject();

			$data->cow = $result;
		}

		$reportData->pregnant_inspection = $results;
		
		//birth inspection

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*, (select insemination_date from #__ed_cow_pregnant_inspection i where i.cow_id=cow_id order by insemination_date desc limit 1) as last_insemination_date, DATEDIFF(NOW(), birth_date) as date_diff1, DATE_ADD(`birth_date`, INTERVAL 3 MONTH) as schedule_date1, DATE_ADD(`birth_date`, INTERVAL 8 MONTH) as schedule_date1")
			->from('#__ed_cow_birth_inspection')
			->where("cow_id in (select id from #__ed_cow where farm_id in (select id from #__ed_farm where coop_id in (select id from #__ed_coop where id like '{$_REQUEST[coop_id]}')) and farm_id in (select farm_id from #__ed_farmer where id like '{$_REQUEST[farmer_id]}')) and state=1");


		$db->setQuery($query);
		$results = $db->loadObjectList();

		foreach($results as $key=>$data){
			$query = $db->getQuery(true);
			$query
				->select("*")
				->from('#__ed_cow')
				->where("id='{$data->cow_id}'");
			$db->setQuery($query);
			$result = $db->loadObject();

			$data->cow = $result;
		}

		$reportData->birth_inspection = $results;


		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*, (select insemination_date from #__ed_cow_pregnant_inspection i where i.cow_id=cow_id order by insemination_date desc limit 1) as last_insemination_date, DATEDIFF(NOW(), birth_date) as date_diff1,DATE_ADD(`birth_date`, INTERVAL 60 DAY) as schedule_date1")
			->from('#__ed_cow_birth_inspection')
			->where("cow_id in (select id from #__ed_cow where farm_id in (select id from #__ed_farm where coop_id in (select id from #__ed_coop where id like '{$_REQUEST[coop_id]}')) and farm_id in (select farm_id from #__ed_farmer where id like '{$_REQUEST[farmer_id]}')) and state=1 ");

		//and NOT EXISTS (select * from #__ed_cow_pregnant_inspection ii where ii.cow_id=cow_id and insemination_date< NOW())

		$db->setQuery($query);
		$results = $db->loadObjectList();

		foreach($results as $key=>$data){
			$query = $db->getQuery(true);
			$query
				->select("*")
				->from('#__ed_cow')
				->where("id='{$data->cow_id}'");
			$db->setQuery($query);
			$result = $db->loadObject();

			$data->cow = $result;
		}

		$reportData->birth_inspection2 = $results;
		

		//insemination

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*, DATEDIFF(NOW(), (select birth_date from #__ed_cow_birth_inspection i where i.cow_id=cow_id order by birth_date desc limit 1)) as date_diff1, MAX(create_date) as last_create_date, DATEDIFF(NOW(), create_date) as date_diff2")
			->from('#__ed_cow_insemination_line')
			->where("cow_id in (select id from #__ed_cow where farm_id in (select id from #__ed_farm where coop_id in (select id from #__ed_coop where id like '{$_REQUEST[coop_id]}')) and farm_id in (select farm_id from #__ed_farmer where id like '{$_REQUEST[farmer_id]}')) and state=1")
			->group("cow_id");


		$db->setQuery($query);
		$results = $db->loadObjectList();

		foreach($results as $key=>$data){
			$query = $db->getQuery(true);
			$query
				->select("*")
				->from('#__ed_cow')
				->where("id='{$data->cow_id}'");
			$db->setQuery($query);
			$result = $db->loadObject();

			$data->cow = $result;
		}

		$reportData->insemination = $results;

		return $reportData;
	}

	public function getReport5(){

		$reportData = new StdClass();

		$province_id_null_query = "";
		if($_REQUEST["province_id"]=="%"){
			$province_id_null_query = " or province_id IS NULL";
		}

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__ed_farmer')
			->where("farm_id in (select id from #__ed_farm f where f.coop_id in (select id from #__ed_coop c where id like '{$_REQUEST[coop_id]}' and region_id like '{$_REQUEST[region_id]}' and (province_id like '{$_REQUEST[province_id]}' {$province_id_null_query}))) and id like '{$_REQUEST[farmer_id]}'")
			->order("name asc");

		$db->setQuery($query);
		$reportData = $db->loadObjectList();

		foreach($reportData as $key=>$data){
			$query = $db->getQuery(true);
			$query
				->select("*")
				->from('#__ed_cow')
				->where("farm_id='{$data->farm_id}'");
			$db->setQuery($query);
			$result = $db->loadObjectList();

			$data->cow = $result;
		}
		
		



		return $reportData;
	}


	public function getReport6(){

		$reportData = new StdClass();

		$date_from = formatSQLDate($_REQUEST["date_from"]);
		$date_to = formatSQLDate($_REQUEST["date_to"]);

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*, (select SUM(kg_amount) from #__ed_send_milk_quantity where create_date=create_datetime and coop_id='{$_REQUEST[coop_id]}' and state=1) as send_kg_amount")
			->from('#__ed_receive_milk_quantity')
			->where("coop_id='{$_REQUEST[coop_id]}' and state=1 and create_datetime between '{$date_from}' and '{$date_to}'")
			->group("create_datetime, round")
			->order("create_datetime asc, round asc");
		$db->setQuery($query);
		$reportData = $db->loadObjectList();


		return $reportData;
	}

	public function getReport7(){

		$reportData = new StdClass();

		$date_from = formatSQLDate($_REQUEST["date_from"]);
		$date_to = formatSQLDate($_REQUEST["date_to"]);

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*, (select SUM(kg_amount) from #__ed_send_milk_quantity where create_date=create_datetime and coop_id='{$_REQUEST[coop_id]}' and state=1) as send_kg_amount")
			->from('#__ed_receive_milk_quantity')
			->where("coop_id='{$_REQUEST[coop_id]}' and state=1 and create_datetime between '{$date_from}' and '{$date_to}'")
			->group("create_datetime, round")
			->order("create_datetime asc, round asc");
		$db->setQuery($query);
		$reportData = $db->loadObjectList();



		return $reportData;
	}


	public function getDashboard(){
			$reportData = new StdClass();
			
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
				->select("cow_status_id, count(*) as count")
				->from('#__ed_cow')
				->group("cow_status_id")
				->where("state=1");
			$db->setQuery($query);
			$results = $db->loadObjectList();

			$reportData->cow = $results;

			$query = $db->getQuery(true);
			$query
				->select("cow_status_id, count(*) as count")
				->from('#__ed_cow')
				->group("cow_status_id")
				->where("state=1 and gender=2");
			$db->setQuery($query);
			$results = $db->loadObjectList();

			$reportData->cow_female = $results;


			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
				->select("region_id, count(*) as count")
				->from('#__ed_farm')
				->group("region_id")
				->where("state=1");
			$db->setQuery($query);
			$results = $db->loadObjectList();

			$reportData->farm = $results;



			$query = $db->getQuery(true);
			$query
				->select("cow_status_id, count(*) as count")
				->from('#__ed_cow')
				->group("cow_status_id")
				->where("state=1 and farm_id in (select id from #__ed_farm where region_id=1)");
			$db->setQuery($query);
			$results = $db->loadObjectList();
			$reportData->cow1 = $results;


			$query = $db->getQuery(true);
			$query
				->select("cow_status_id, count(*) as count")
				->from('#__ed_cow')
				->group("cow_status_id")
				->where("state=1 and gender=2 and farm_id in (select id from #__ed_farm where region_id=1)");
			$db->setQuery($query);
			$results = $db->loadObjectList();

			$reportData->cow_female1 = $results;





			$query = $db->getQuery(true);
			$query
				->select("cow_status_id, count(*) as count")
				->from('#__ed_cow')
				->group("cow_status_id")
				->where("state=1 and farm_id in (select id from #__ed_farm where region_id=2)");
			$db->setQuery($query);
			$results = $db->loadObjectList();
			$reportData->cow2 = $results;


			$query = $db->getQuery(true);
			$query
				->select("cow_status_id, count(*) as count")
				->from('#__ed_cow')
				->group("cow_status_id")
				->where("state=1 and gender=2 and farm_id in (select id from #__ed_farm where region_id=2)");
			$db->setQuery($query);
			$results = $db->loadObjectList();

			$reportData->cow_female2 = $results;



			$query = $db->getQuery(true);
			$query
				->select("cow_status_id, count(*) as count")
				->from('#__ed_cow')
				->group("cow_status_id")
				->where("state=1 and farm_id in (select id from #__ed_farm where region_id=3)");
			$db->setQuery($query);
			$results = $db->loadObjectList();
			$reportData->cow3 = $results;


			$query = $db->getQuery(true);
			$query
				->select("cow_status_id, count(*) as count")
				->from('#__ed_cow')
				->group("cow_status_id")
				->where("state=1 and gender=2 and farm_id in (select id from #__ed_farm where region_id=3)");
			$db->setQuery($query);
			$results = $db->loadObjectList();

			$reportData->cow_female3 = $results;



			$query = $db->getQuery(true);
			$query
				->select("cow_status_id, count(*) as count")
				->from('#__ed_cow')
				->group("cow_status_id")
				->where("state=1 and farm_id in (select id from #__ed_farm where region_id=4)");
			$db->setQuery($query);
			$results = $db->loadObjectList();
			$reportData->cow4 = $results;


			$query = $db->getQuery(true);
			$query
				->select("cow_status_id, count(*) as count")
				->from('#__ed_cow')
				->group("cow_status_id")
				->where("state=1 and gender=2 and farm_id in (select id from #__ed_farm where region_id=4)");
			$db->setQuery($query);
			$results = $db->loadObjectList();

			$reportData->cow_female4 = $results;


			$query = $db->getQuery(true);
			$query
				->select("cow_status_id, count(*) as count")
				->from('#__ed_cow')
				->group("cow_status_id")
				->where("state=1 and farm_id in (select id from #__ed_farm where region_id=5)");
			$db->setQuery($query);
			$results = $db->loadObjectList();
			$reportData->cow5 = $results;


			$query = $db->getQuery(true);
			$query
				->select("cow_status_id, count(*) as count")
				->from('#__ed_cow')
				->group("cow_status_id")
				->where("state=1 and gender=2 and farm_id in (select id from #__ed_farm where region_id=5)");
			$db->setQuery($query);
			$results = $db->loadObjectList();

			$reportData->cow_female5 = $results;

			if($_REQUEST["month"]==""){
				$_REQUEST["month"]=date("m");
			}

			if($_REQUEST["year"]==""){
				$_REQUEST["year"]=date("Y")+543;
			}

			if($_REQUEST["year"]=="%"){
				$year = "%";
			}else{
				$year = intval($_REQUEST["year"]);
				$year = $$_REQUEST["year"] - 543;
			}

			$query = $db->getQuery(true);
			$query
				->select("c.region_id, c.coop_type, SUM(total_amount)/1000 as sum_ton_amount")
				->from('#__ed_receive_milk_quantity r')
				->join("left", "#__ed_coop c on c.id=coop_id")
				->group("c.region_id, c.coop_type")
				->where("r.state=1 and MONTH(r.create_datetime) like '{$_REQUEST[month]}' and YEAR(r.create_datetime) like '{$year}'");
			$db->setQuery($query);
			$results = $db->loadObjectList();

			$reportData->receive_milk = $results;



		return $reportData;
	}

	public function getFarmerInCoop(){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("f.*")
				->from('#__ed_farmer f')
				->join("left", "#__ed_farm fm on fm.id=f.farm_id")
				->where("f.state=1 and coop_id='{$_REQUEST[coop_id]}'");

		$db->setQuery($query);
		$results = $db->loadObjectList();

		return $results;
	}
}
