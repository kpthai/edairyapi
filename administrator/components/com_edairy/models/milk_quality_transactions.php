<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Edairy records.
 *
 * @since  1.6
 */
class EdairyModelMilk_quality_transactions extends JModelList
{
/**
	* Constructor.
	*
	* @param   array  $config  An optional associative array of configuration settings.
	*
	* @see        JController
	* @since      1.6
	*/
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.`id`',
				'milk_quality_id', 'a.`milk_quality_id`',
				'create_date', 'a.`create_date`',
				'create_time', 'a.`create_time`',
				'coop_id', 'a.`coop_id`',
				'license_plate', 'a.`license_plate`',
				'mb', 'a.`mb`',
				'scc', 'a.`scc`',
				'fat', 'a.`fat`',
				'snf', 'a.`snf`',
				'ts', 'a.`ts`',
				'protein', 'a.`protein`',
				'lactose', 'a.`lactose`',
				'price', 'a.`price`',
				'amount', 'a.`amount`',
				'total_price', 'a.`total_price`',
				'upload_file', 'a.`upload_file`',
				'ordering', 'a.`ordering`',
				'state', 'a.`state`',
				'created_by', 'a.`created_by`',
				'modified_by', 'a.`modified_by`',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
		$this->setState('filter.state', $published);
		// Filtering milk_quality_id
		$this->setState('filter.milk_quality_id', $app->getUserStateFromRequest($this->context.'.filter.milk_quality_id', 'filter_milk_quality_id', '', 'string'));

		// Filtering create_date
		$this->setState('filter.create_date.from', $app->getUserStateFromRequest($this->context.'.filter.create_date.from', 'filter_from_create_date', '', 'string'));
		$this->setState('filter.create_date.to', $app->getUserStateFromRequest($this->context.'.filter.create_date.to', 'filter_to_create_date', '', 'string'));

		// Filtering coop_id
		// $this->setState('filter.coop_id', $app->getUserStateFromRequest($this->context.'.filter.coop_id', 'filter_coop_id', '', 'string'));


		// Load the parameters.
		$params = JComponentHelper::getParams('com_edairy');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.milk_quality_id', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select', 'DISTINCT a.*'
			)
		);
		$query->from('`#__ed_milk_quality_transaction` AS a');

		// Join over the users for the checked out user
		$query->select("uc.name AS uEditor");
		$query->join("LEFT", "#__users AS uc ON uc.id=a.checked_out");
		// Join over the foreign key 'milk_quality_id'
		$query->select('`#__ed_milk_quality_header_2781682`.`run_no` AS milk_quality_headers_fk_value_2781682');
		$query->join('LEFT', '#__ed_milk_quality_header AS #__ed_milk_quality_header_2781682 ON #__ed_milk_quality_header_2781682.`id` = a.`milk_quality_id`');
		// Join over the foreign key 'coop_id'
		// $query->select('CONCAT(`#__ed_coop_2781685`.`coop_code`, \' \', `#__ed_coop_2781685`.`coop_abbr`, \' \', `#__ed_coop_2781685`.`name`) AS coops_fk_value_2781685');
		// $query->join('LEFT', '#__ed_coop AS #__ed_coop_2781685 ON #__ed_coop_2781685.`id` = a.`coop_id`');

		// Join over the user field 'created_by'
		$query->select('`created_by`.name AS `created_by`');
		$query->join('LEFT', '#__users AS `created_by` ON `created_by`.id = a.`created_by`');

		// Join over the user field 'modified_by'
		$query->select('`modified_by`.name AS `modified_by`');
		$query->join('LEFT', '#__users AS `modified_by` ON `modified_by`.id = a.`modified_by`');

		// Filter by published state
		$published = $this->getState('filter.state');

		if (is_numeric($published))
		{
			$query->where('a.state = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.state IN (0, 1))');
		}

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				$query->where('(#__ed_milk_quality_header_2781682.run_no LIKE ' . $search . '  OR  a.create_date LIKE ' . $search . '  OR CONCAT(`#__ed_coop_2781685`.`coop_code`, \' \', `#__ed_coop_2781685`.`coop_abbr`, \' \', `#__ed_coop_2781685`.`name`) LIKE ' . $search . '  OR  a.license_plate LIKE ' . $search . '  OR  a.price LIKE ' . $search . '  OR  a.amount LIKE ' . $search . '  OR  a.total_price LIKE ' . $search . ' )');
			}
		}


		// Filtering milk_quality_id
		$filter_milk_quality_id = $this->state->get("filter.milk_quality_id");

		if ($filter_milk_quality_id !== null && !empty($filter_milk_quality_id))
		{
			$query->where("a.`milk_quality_id` = '".$db->escape($filter_milk_quality_id)."'");
		}

		// Filtering create_date
		$filter_create_date_from = $this->state->get("filter.create_date.from");

		if ($filter_create_date_from !== null && !empty($filter_create_date_from))
		{
			$query->where("a.`create_date` >= '".$db->escape($filter_create_date_from)."'");
		}
		$filter_create_date_to = $this->state->get("filter.create_date.to");

		if ($filter_create_date_to !== null  && !empty($filter_create_date_to))
		{
			$query->where("a.`create_date` <= '".$db->escape($filter_create_date_to)."'");
		}


		//Custom Filter
		if ($_REQUEST["search_run_no"]!="")
		{
			$query->where("#__ed_milk_quality_header_2781682.`run_no` LIKE '%".$db->escape($_REQUEST["search_run_no"])."%'");
		}

		if ($_REQUEST["search_coop"]!="")
		{
			$query->where("#__ed_milk_quality_header_2781682.coop_id in (select id from #__ed_coop c where c.name like '%{$_REQUEST[search_coop]}%' or c.coop_code like '%{$_REQUEST[search_coop]}%' )");
		}


		if ($_REQUEST["search_date_from"]!="" && $_REQUEST["search_date_to"]!="")
		{
			$date_from = formatSQLDate($_REQUEST["search_date_from"]);
			$date_to = formatSQLDate($_REQUEST["search_date_to"]);
			$query->where("#__ed_milk_quality_header_2781682.from_date like '{$date_from}' and #__ed_milk_quality_header_2781682.to_date like '{$date_to}'");
		}

		// Filtering coop_id
		// $filter_coop_id = $this->state->get("filter.coop_id");

		// if ($filter_coop_id !== null && !empty($filter_coop_id))
		// {
		// 	$query->where("a.`coop_id` = '".$db->escape($filter_coop_id)."'");
		// }
		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');

		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();

		foreach ($items as $oneItem)
		{

			if (isset($oneItem->milk_quality_id))
			{
				$values = explode(',', $oneItem->milk_quality_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('`#__ed_milk_quality_header_2781682`.`run_no`')
							->from($db->quoteName('#__ed_milk_quality_header', '#__ed_milk_quality_header_2781682'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->run_no;
					}
				}

				$milk_quality_id = $oneItem->milk_quality_id;
			$oneItem->milk_quality_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->milk_quality_id;

					$query = $db->getQuery(true);
					$query
						->select("*")
						->from('#__ed_milk_quality_header')
						->where("id='{$milk_quality_id}'");
					$db->setQuery($query);
					$result = $db->loadObject();
					$oneItem->milk_quality_header = $result;

					// print_r($item);
					// die();



			}

			if(isset($oneItem->farmer_id))
			{
				$query = $db->getQuery(true);
						$query
								->select('farm_id')
								->from("#__ed_farmer")
								->where("id='{$oneItem->farmer_id}'");
						$db->setQuery($query);
						$result = $db->loadObject();
						$oneItem->farmer = $result;

						$query = $db->getQuery(true);
						$query
								->select('coop_id')
								->from("#__ed_farm")
								->where("id='{$oneItem->farmer->farm_id}'");
						$db->setQuery($query);
						$result = $db->loadObject();
						$oneItem->farm = $result;

						$query = $db->getQuery(true);
						$query
								->select('*')
								->from("#__ed_coop")
								->where("id='{$oneItem->farm->coop_id}'");
						$db->setQuery($query);
						$result = $db->loadObject();
						$oneItem->coop = $result;
			}



			// if (isset($oneItem->coop_id))
			// {
			// 	$values = explode(',', $oneItem->coop_id);

			// 	$textValue = array();
			// 	foreach ($values as $value){
			// 		$db = JFactory::getDbo();
			// 		$query = $db->getQuery(true);
			// 		$query
			// 				->select('CONCAT(`#__ed_coop_2781685`.`coop_code`, \' \', `#__ed_coop_2781685`.`coop_abbr`, \' \', `#__ed_coop_2781685`.`name`) AS `fk_value`')
			// 				->from($db->quoteName('#__ed_coop', '#__ed_coop_2781685'))
			// 				->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
			// 		$db->setQuery($query);
			// 		$results = $db->loadObject();
			// 		if ($results) {
			// 			$textValue[] = $results->fk_value;
			// 		}
			// 	}

			// $oneItem->coop_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->coop_id;

			// }
		}

		return $items;
	}
}
