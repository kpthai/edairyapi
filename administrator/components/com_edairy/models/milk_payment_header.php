<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Edairy model.
 *
 * @since  1.6
 */
class EdairyModelMilk_payment_header extends JModelAdmin
{
	/**
	 * @var      string    The prefix to use with controller messages.
	 * @since    1.6
	 */
	protected $text_prefix = 'COM_EDAIRY';

	/**
	 * @var   	string  	Alias to manage history control
	 * @since   3.2
	 */
	public $typeAlias = 'com_edairy.milk_payment_header';

	/**
	 * @var null  Item data
	 * @since  1.6
	 */
	protected $item = null;

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param   string  $type    The table type to instantiate
	 * @param   string  $prefix  A prefix for the table class name. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return    JTable    A database object
	 *
	 * @since    1.6
	 */
	public function getTable($type = 'Milk_payment_header', $prefix = 'EdairyTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      An optional array of data for the form to interogate.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  JForm  A JForm object on success, false on failure
	 *
	 * @since    1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Initialise variables.
		$app = JFactory::getApplication();

		// Get the form.
		$form = $this->loadForm(
			'com_edairy.milk_payment_header', 'milk_payment_header',
			array('control' => 'jform',
				'load_data' => $loadData
			)
		);

		if (empty($form))
		{
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return   mixed  The data for the form.
	 *
	 * @since    1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_edairy.edit.milk_payment_header.data', array());

		if (empty($data))
		{
			if ($this->item === null)
			{
				$this->item = $this->getItem();
			}

			$data = $this->item;
		}

		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param   integer  $pk  The id of the primary key.
	 *
	 * @return  mixed    Object on success, false on failure.
	 *
	 * @since    1.6
	 */
	public function getItem($pk = null)
	{
		if ($item = parent::getItem($pk))
		{
			// Do any procesing on fields here if needed
		}

		return $item;
	}

	/**
	 * Method to duplicate an Milk_payment_header
	 *
	 * @param   array  &$pks  An array of primary key IDs.
	 *
	 * @return  boolean  True if successful.
	 *
	 * @throws  Exception
	 */
	public function duplicate(&$pks)
	{
		$user = JFactory::getUser();

		// Access checks.
		if (!$user->authorise('core.create', 'com_edairy'))
		{
			throw new Exception(JText::_('JERROR_CORE_CREATE_NOT_PERMITTED'));
		}

		$dispatcher = JEventDispatcher::getInstance();
		$context    = $this->option . '.' . $this->name;

		// Include the plugins for the save events.
		JPluginHelper::importPlugin($this->events_map['save']);

		$table = $this->getTable();

		foreach ($pks as $pk)
		{
			if ($table->load($pk, true))
			{
				// Reset the id to create a new record.
				$table->id = 0;

				if (!$table->check())
				{
					throw new Exception($table->getError());
				}
				
				if (!empty($table->coop_id))
				{
					if (is_array($table->coop_id))
					{
						$table->coop_id = implode(',', $table->coop_id);
					}
				}
				else
				{
					$table->coop_id = '';
				}


				// Trigger the before save event.
				$result = $dispatcher->trigger($this->event_before_save, array($context, &$table, true));

				if (in_array(false, $result, true) || !$table->store())
				{
					throw new Exception($table->getError());
				}

				// Trigger the after save event.
				$dispatcher->trigger($this->event_after_save, array($context, &$table, true));
			}
			else
			{
				throw new Exception($table->getError());
			}
		}

		// Clean cache
		$this->cleanCache();

		return true;
	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @param   JTable  $table  Table Object
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function prepareTable($table)
	{
		jimport('joomla.filter.output');

		$table->from_date = formatSQLDate($table->from_date);
		$table->to_date = formatSQLDate($table->to_date);

		if (empty($table->id))
		{
			// Set ordering to the last item if not set
			if (@$table->ordering === '')
			{
				$db = JFactory::getDbo();
				$db->setQuery('SELECT MAX(ordering) FROM #__ed_milk_payment_header');
				$max             = $db->loadResult();
				$table->ordering = $max + 1;
			}
		}
	}


	
	public function getMasterData(){

		$masterData = new StdClass();
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__ed_farmer')
			->where("state=1");
		$db->setQuery($query);
		$masterData->mas_farmer = $db->loadObjectList();

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__ed_coop')
			->where("state=1");
		$db->setQuery($query);
		$masterData->mas_coop = $db->loadObjectList();



		return $masterData;

	}

	public function getStep2(){

		$from_date = formatSQLDate($_REQUEST['from_date']);
		$to_date = formatSQLDate($_REQUEST['to_date']);

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*, count(*) as count, SUM(total_amount) as total_amount_sum")
			->from('#__ed_receive_milk_quantity')
			->where("coop_id='{$_REQUEST[coop_id]}' and create_datetime between '{$from_date}' and '{$to_date}' and state=1")
			->group("farmer_id");
		$db->setQuery($query);
		$results = $db->loadObjectList();

		foreach($results as $key=>$data){
			//farmer
			$query = $db->getQuery(true);
			$query
				->select("*")
				->from('#__ed_farmer')
				->where("id='{$data->farmer_id}' and state=1");
			$db->setQuery($query);
			$result_farmer = $db->loadObject();
			$data->farmer = $result_farmer;

			//farm
			$query = $db->getQuery(true);
			$query
				->select("*")
				->from('#__ed_farm')
				->where("id='{$result_farmer->farm_id}' and state=1");
			$db->setQuery($query);
			$result_farm = $db->loadObject();
			$data->farm = $result_farm;
		}

		return $results;
	}



	public function getStep3(){

		$from_date = formatSQLDate($_REQUEST['from_date']);
		$to_date = formatSQLDate($_REQUEST['to_date']);

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*, count(*) as count, SUM(total_amount) as total_amount_sum")
			->from('#__ed_receive_milk_quantity')
			->where("coop_id='{$_REQUEST[coop_id]}' and create_datetime between '{$from_date}' and '{$to_date}' and state=1")
			->group("farmer_id");
		$db->setQuery($query);
		$farmers = $db->loadObjectList();



		foreach($farmers as $key=>$data){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
				->select("*, (select run_no from #__ed_milk_quality_header where id=milk_quality_id) as run_no")
				->from('#__ed_milk_quality_transaction')
				->where("farmer_id like '{$data->farmer_id}' and create_date between '{$from_date}' and '{$to_date}' and state=1")
				->order('id desc')
				->limit(1);
			$db->setQuery($query);
			$result_milk_quality_transaction = $db->loadObject();

			$data->milk_quality_transaction = $result_milk_quality_transaction;
		}

		foreach($farmers as $key=>$data){
			//farmer
			$query = $db->getQuery(true);
			$query
				->select("*")
				->from('#__ed_farmer')
				->where("id='{$data->farmer_id}' and state=1");
			$db->setQuery($query);
			$result_farmer = $db->loadObject();
			$data->farmer = $result_farmer;

			//farm
			$query = $db->getQuery(true);
			$query
				->select("*")
				->from('#__ed_farm')
				->where("id='{$result_farmer->farm_id}' and state=1");
			$db->setQuery($query);
			$result_farm = $db->loadObject();
			$data->farm = $result_farm;
		}


		return $farmers;
	}



	public function getStep4(){



		$from_date = formatSQLDate($_REQUEST['from_date']);
		$to_date = formatSQLDate($_REQUEST['to_date']);

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*, count(*) as count, SUM(total_amount) as total_amount_sum")
			->from('#__ed_receive_milk_quantity')
			->where("coop_id='{$_REQUEST[coop_id]}' and create_datetime between '{$from_date}' and '{$to_date}' and state=1")
			->group("farmer_id");
		$db->setQuery($query);
		$farmers = $db->loadObjectList();


		
		foreach($farmers as $key=>$data){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
				->select("*, count(*) as count, '{$data->farmer_id}' as farmer_id, SUM(total_price) as total_price_sum")
				->from('#__ed_cow_insemination_line')
				->where("cow_id in (select id from #__ed_cow where farm_id in (select farm_id from #__ed_farmer where id like '{$data->farmer_id}')) and create_date between '{$from_date}' and '{$to_date}' and state=1")
				->group("farmer_id");
			$db->setQuery($query);


			$result_insemination_line = $db->loadObject();

			$data->insemination_line = $result_insemination_line;


			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
				->select("hl.*, h.create_date, count(*) as count, '{$data->farmer_id}' as farmer_id, SUM(total_price) as total_price_sum")
				->from('#__ed_cow_health_line hl')
				->join('left', "#__ed_cow_health h on h.id=cow_health_id")
				->where("cow_id in (select id from #__ed_cow where farm_id in (select farm_id from #__ed_farmer where id like '{$data->farmer_id}')) and create_date between '{$from_date}' and '{$to_date}' and hl.state=1")
				->group("farmer_id");
			$db->setQuery($query);
			$result_health_line = $db->loadObject();

			$data->health_line = $result_health_line;



		}


		foreach($farmers as $key=>$data){
			//farmer
			$query = $db->getQuery(true);
			$query
				->select("*")
				->from('#__ed_farmer')
				->where("id='{$data->farmer_id}' and state=1");
			$db->setQuery($query);
			$result_farmer = $db->loadObject();
			$data->farmer = $result_farmer;

			//farm
			$query = $db->getQuery(true);
			$query
				->select("*")
				->from('#__ed_farm')
				->where("id='{$result_farmer->farm_id}' and state=1");
			$db->setQuery($query);
			$result_farm = $db->loadObject();
			$data->farm = $result_farm;
		}


		return $farmers;
	}



	public function getStep5(){



		$from_date = formatSQLDate($_REQUEST['from_date']);
		$to_date = formatSQLDate($_REQUEST['to_date']);


		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*, count(*) as count, SUM(total_amount) as total_amount_sum")
			->from('#__ed_receive_milk_quantity')
			->where("coop_id='{$_REQUEST[coop_id]}' and create_datetime between '{$from_date}' and '{$to_date}' and state=1")
			->group("farmer_id");
		$db->setQuery($query);
		$farmers = $db->loadObjectList();

		foreach($farmers as $key=>$data){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
				->select("*, (select run_no from #__ed_milk_quality_header where id=milk_quality_id) as run_no")
				->from('#__ed_milk_quality_transaction')
				->where("farmer_id like '{$data->farmer_id}' and create_date between '{$from_date}' and '{$to_date}' and state=1")
				->order('id desc')
				->limit(1);
			$db->setQuery($query);
			$result_milk_quality_transaction = $db->loadObject();

			$data->milk_quality_transaction = $result_milk_quality_transaction;

		}


		foreach($farmers as $key=>$data){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
				->select("*, count(*) as count, '{$data->farmer_id}' as farmer_id, SUM(total_price) as total_price_sum")
				->from('#__ed_cow_insemination_line')
				->where("cow_id in (select id from #__ed_cow where farm_id in (select farm_id from #__ed_farmer where id like '{$data->farmer_id}')) and create_date between '{$from_date}' and '{$to_date}' and state=1")
				->group("farmer_id");
			$db->setQuery($query);


			$result_insemination_line = $db->loadObject();

			$data->insemination_line = $result_insemination_line;


			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
				->select("hl.*, h.create_date, count(*) as count, '{$data->farmer_id}' as farmer_id, SUM(total_price) as total_price_sum")
				->from('#__ed_cow_health_line hl')
				->join('left', "#__ed_cow_health h on h.id=cow_health_id")
				->where("cow_id in (select id from #__ed_cow where farm_id in (select farm_id from #__ed_farmer where id like '{$data->farmer_id}')) and create_date between '{$from_date}' and '{$to_date}' and hl.state=1")
				->group("farmer_id");
			$db->setQuery($query);
			$result_health_line = $db->loadObject();

			$data->health_line = $result_health_line;



		}


		foreach($farmers as $key=>$data){
			//farmer
			$query = $db->getQuery(true);
			$query
				->select("*")
				->from('#__ed_farmer')
				->where("id='{$data->farmer_id}' and state=1");
			$db->setQuery($query);
			$result_farmer = $db->loadObject();
			$data->farmer = $result_farmer;

			//farm
			$query = $db->getQuery(true);
			$query
				->select("*")
				->from('#__ed_farm')
				->where("id='{$result_farmer->farm_id}' and state=1");
			$db->setQuery($query);
			$result_farm = $db->loadObject();
			$data->farm = $result_farm;
		}


		return $farmers;
	}

	function customSave(){
		$data = new StdClass();
		$data->coop_id = $_POST["coop_id"];
		$data->period = $_POST["period"];
		$data->from_date = formatSQLDate($_POST["from_date"]);
		$data->to_date = formatSQLDate($_POST["to_date"]);
		$data->total_send_milk_amount = $_POST["total_send_milk_amount"];

		$user = JFactory::getUser();

		
		$data->ordering=0;
		$data->state=1;
		$data->checked_out = 0;
		$data->checked_out_time = "0000-00-00 00:00:00";
		$data->created_by = $user->id;

		$addDataResult = JFactory::getDbo()->insertObject('#__ed_milk_payment_header', $data, 'id');

		$milk_payment_header_id = $data->id;

		if($addDataResult){

			//milk_payment_header_line

			$from_date = formatSQLDate($_REQUEST['from_date']);
		$to_date = formatSQLDate($_REQUEST['to_date']);


		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*, count(*) as count, SUM(total_amount) as total_amount_sum")
			->from('#__ed_receive_milk_quantity')
			->where("coop_id='{$_REQUEST[coop_id]}' and create_datetime between '{$from_date}' and '{$to_date}' and state=1")
			->group("farmer_id");
		$db->setQuery($query);
		$farmers = $db->loadObjectList();

		foreach($farmers as $key=>$data){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
				->select("*, (select run_no from #__ed_milk_quality_header where id=milk_quality_id) as run_no")
				->from('#__ed_milk_quality_transaction')
				->where("farmer_id like '{$data->farmer_id}' and create_date between '{$from_date}' and '{$to_date}' and state=1")
				->order('id desc')
				->limit(1);
			$db->setQuery($query);
			$result_milk_quality_transaction = $db->loadObject();

			$data->milk_quality_transaction = $result_milk_quality_transaction;

		}


		foreach($farmers as $key=>$data){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
				->select("*, count(*) as count, '{$data->farmer_id}' as farmer_id, SUM(total_price) as total_price_sum")
				->from('#__ed_cow_insemination_line')
				->where("cow_id in (select id from #__ed_cow where farm_id in (select farm_id from #__ed_farmer where id like '{$data->farmer_id}')) and create_date between '{$from_date}' and '{$to_date}' and state=1")
				->group("farmer_id");
			$db->setQuery($query);


			$result_insemination_line = $db->loadObject();

			$data->insemination_line = $result_insemination_line;


			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
				->select("hl.*, h.create_date, count(*) as count, '{$data->farmer_id}' as farmer_id, SUM(total_price) as total_price_sum")
				->from('#__ed_cow_health_line hl')
				->join('left', "#__ed_cow_health h on h.id=cow_health_id")
				->where("cow_id in (select id from #__ed_cow where farm_id in (select farm_id from #__ed_farmer where id like '{$data->farmer_id}')) and create_date between '{$from_date}' and '{$to_date}' and hl.state=1")
				->group("farmer_id");
			$db->setQuery($query);
			$result_health_line = $db->loadObject();

			$data->health_line = $result_health_line;



		}


		foreach($farmers as $key=>$data){
			//farmer
			$query = $db->getQuery(true);
			$query
				->select("*")
				->from('#__ed_farmer')
				->where("id='{$data->farmer_id}' and state=1");
			$db->setQuery($query);
			$result_farmer = $db->loadObject();
			$data->farmer = $result_farmer;

			//farm
			$query = $db->getQuery(true);
			$query
				->select("*")
				->from('#__ed_farm')
				->where("id='{$result_farmer->farm_id}' and state=1");
			$db->setQuery($query);
			$result_farm = $db->loadObject();
			$data->farm = $result_farm;
		}

		foreach($farmers as $key=>$data){ 

			//Calculation Logic
			$milk_price = $data->milk_quality_transaction->total_price;
			if($data->milk_quality_transaction->total_price==""){
				$data->milk_quality_transaction->total_price = "<font color='red'>ไม่พบข้อมูล</font>";
			}
			if($data->insemination_line->count==""){
				$data->insemination_line->count=0;
			}

			if($data->health_line->count==""){
				$data->health_line->count = 0;
			}

			if($data->insemination_line->total_price_sum == ""){
				$data->insemination_line->total_price_sum = 0;
			}

			if($data->health_line->total_price_sum == ""){
				$data->health_line->total_price_sum = 0;
			}
			$line_grand_total = number_format($data->insemination_line->total_price_sum+$data->health_line->total_price_sum,2);

			$income = number_format($data->total_amount_sum*$milk_price, 2);

			$data_line = new StdClass();
			$data_line->create_date = date("Y-m-d");
			$data_line->milk_payment_id = $milk_payment_header_id;
			$data_line->farmer_id = $data->farmer_id;
			$data_line->milk_amount = $data->total_amount_sum;
			$data_line->milk_price = $data->milk_quality_transaction->total_price;
			$data_line->total_income = $income;
			$data_line->health_insemination_service = $line_grand_total;
			$data_line->tax = $line_grand_total*0.03;
			$data_line->total_outcome = $line_grand_total*1.03;
			$data_line->grand_total = $income-$line_grand_total*1.03;

			$user = JFactory::getUser();

			
			$data_line->ordering=0;
			$data_line->state=1;
			$data_line->checked_out = 0;
			$data_line->checked_out_time = "0000-00-00 00:00:00";
			$data_line->created_by = $user->id;

			$addDataResult = JFactory::getDbo()->insertObject('#__ed_milk_payment_transaction', $data_line, 'id');

		}

			return true;
		}else{
			return false;
		}
		
	}
}
