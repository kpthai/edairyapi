<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Edairy records.
 *
 * @since  1.6
 */
class EdairyModelReceive_milk_quantities extends JModelList
{
/**
	* Constructor.
	*
	* @param   array  $config  An optional associative array of configuration settings.
	*
	* @see        JController
	* @since      1.6
	*/
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.`id`',
				'run_no', 'a.`run_no`',
				'create_datetime', 'a.`create_datetime`',
				'round', 'a.`round`',
				'total_amount', 'a.`total_amount`',
				'coop_id', 'a.`coop_id`',
				'farmer_id', 'a.`farmer_id`',
				'receiver_id', 'a.`receiver_id`',
				'remark', 'a.`remark`',
				'denied_amount', 'a.`denied_amount`',
				'ordering', 'a.`ordering`',
				'state', 'a.`state`',
				'created_by', 'a.`created_by`',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
		$this->setState('filter.state', $published);
		// Filtering create_datetime
		$this->setState('filter.create_datetime.from', $app->getUserStateFromRequest($this->context.'.filter.create_datetime.from', 'filter_from_create_datetime', '', 'string'));
		$this->setState('filter.create_datetime.to', $app->getUserStateFromRequest($this->context.'.filter.create_datetime.to', 'filter_to_create_datetime', '', 'string'));

		// Filtering round
		$this->setState('filter.round', $app->getUserStateFromRequest($this->context.'.filter.round', 'filter_round', '', 'string'));

		// Filtering coop_id
		$this->setState('filter.coop_id', $app->getUserStateFromRequest($this->context.'.filter.coop_id', 'filter_coop_id', '', 'string'));

		// Filtering farmer_id
		$this->setState('filter.farmer_id', $app->getUserStateFromRequest($this->context.'.filter.farmer_id', 'filter_farmer_id', '', 'string'));


		// Load the parameters.
		$params = JComponentHelper::getParams('com_edairy');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.run_no', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select', 'DISTINCT a.*'
			)
		);
		$query->from('`#__ed_receive_milk_quantity` AS a');

		// Join over the users for the checked out user
		$query->select("uc.name AS uEditor");
		$query->join("LEFT", "#__users AS uc ON uc.id=a.checked_out");
		// Join over the foreign key 'coop_id'
		$query->select('CONCAT(`#__ed_coop_2754484`.`coop_code`, \' \', `#__ed_coop_2754484`.`name`) AS coops_fk_value_2754484');
		$query->join('LEFT', '#__ed_coop AS #__ed_coop_2754484 ON #__ed_coop_2754484.`id` = a.`coop_id`');
		// Join over the foreign key 'farmer_id'
		$query->select('CONCAT(`#__ed_farmer_2754480`.`citizen_id`, \' \', `#__ed_farmer_2754480`.`name`, \' \', `#__ed_farmer_2754480`.`surname`) AS farmers_fk_value_2754480');
		$query->join('LEFT', '#__ed_farmer AS #__ed_farmer_2754480 ON #__ed_farmer_2754480.`id` = a.`farmer_id`');

		// Join over the user field 'receiver_id'
		$query->select('`receiver_id`.name AS `receiver_id`');
		$query->join('LEFT', '#__users AS `receiver_id` ON `receiver_id`.id = a.`receiver_id`');

		// Join over the user field 'created_by'
		$query->select('`created_by`.name AS `created_by`');
		$query->join('LEFT', '#__users AS `created_by` ON `created_by`.id = a.`created_by`');

		// Filter by published state
		$published = $this->getState('filter.state');

		if (is_numeric($published))
		{
			$query->where('a.state = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.state IN (0, 1))');
		}

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				$query->where('( a.run_no LIKE ' . $search . '  OR  a.create_datetime LIKE ' . $search . '  OR  a.round LIKE ' . $search . '  OR  a.total_amount LIKE ' . $search . '  OR CONCAT(`#__ed_coop_2754484`.`coop_code`, \' \', `#__ed_coop_2754484`.`name`) LIKE ' . $search . '  OR CONCAT(`#__ed_farmer_2754480`.`citizen_id`, \' \', `#__ed_farmer_2754480`.`name`, \' \', `#__ed_farmer_2754480`.`surname`) LIKE ' . $search . '  OR  a.remark LIKE ' . $search . ' )');
			}
		}


		//Filtering create_datetime
		$filter_create_datetime_from = $this->state->get("filter.create_datetime.from");

		if ($filter_create_datetime_from !== null && !empty($filter_create_datetime_from))
		{
			$query->where("a.`create_datetime` >= '".$db->escape($filter_create_datetime_from)."'");
		}
		$filter_create_datetime_to = $this->state->get("filter.create_datetime.to");

		if ($filter_create_datetime_to !== null  && !empty($filter_create_datetime_to))
		{
			$query->where("a.`create_datetime` <= '".$db->escape($filter_create_datetime_to)."'");
		}

		//Filtering round
		$filter_round = $this->state->get("filter.round");
		if ($filter_round !== null && !empty($filter_round))
		{
			$query->where("a.`round` = '".$db->escape($filter_round)."'");
		}

		//Filtering coop_id
		$filter_coop_id = $this->state->get("filter.coop_id");
		if ($filter_coop_id !== null && !empty($filter_coop_id))
		{
			$query->where("a.`coop_id` = '".$db->escape($filter_coop_id)."'");
		}

		//Filtering farmer_id
		$filter_farmer_id = $this->state->get("filter.farmer_id");
		if ($filter_farmer_id !== null && !empty($filter_farmer_id))
		{
			$query->where("a.`farmer_id` = '".$db->escape($filter_farmer_id)."'");
		}

		if ($_REQUEST["search_member_code"]!="")
		{
			
			$query->where("a.farmer_id in (select id from #__ed_farmer fm where farm_id in (select id from #__ed_farm f where f.member_code like '%{$_REQUEST[search_member_code]}%' ))");
		}

		if ($_REQUEST["search_farmer"]!="")
		{
			$query->where("a.farmer_id in (select id from #__ed_farmer f where f.name like '%{$_REQUEST[search_farmer]}%' or f.surname like '%{$_REQUEST[search_farmer]}%')");
		}

		if ($_REQUEST["search_date_from"]!="" && $_REQUEST["search_date_to"]!="")
		{
			$date_from = formatSQLDate($_REQUEST["search_date_from"]);
			$date_to = formatSQLDate($_REQUEST["search_date_to"]);
			$query->where("a.create_datetime between '{$date_from}' and '{$date_to}'");
		}

		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');

		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();

		foreach ($items as $oneItem) {
					$oneItem->round = ($oneItem->round == '') ? '' : JText::_('COM_EDAIRY_RECEIVE_MILK_QUANTITIES_ROUND_OPTION_' . strtoupper($oneItem->round));

			if (isset($oneItem->coop_id))
			{
				$values = explode(',', $oneItem->coop_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('CONCAT(`#__ed_coop_2754484`.`coop_code`, \' \', `#__ed_coop_2754484`.`name`) AS `fk_value`')
							->from($db->quoteName('#__ed_coop', '#__ed_coop_2754484'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->fk_value;
					}
				}

			$oneItem->coop_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->coop_id;

			}

			if (isset($oneItem->farmer_id))
			{
				$values = explode(',', $oneItem->farmer_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('CONCAT(`#__ed_farmer_2754480`.`citizen_id`, \' \', `#__ed_farmer_2754480`.`name`, \' \', `#__ed_farmer_2754480`.`surname`) AS `fk_value`')
							->from($db->quoteName('#__ed_farmer', '#__ed_farmer_2754480'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->fk_value;
					}
				}

			$oneItem->farmer_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->farmer_id;

			}
		}
		return $items;
	}
}
