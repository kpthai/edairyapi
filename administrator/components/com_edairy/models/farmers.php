<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Edairy records.
 *
 * @since  1.6
 */
class EdairyModelFarmers extends JModelList
{
/**
	* Constructor.
	*
	* @param   array  $config  An optional associative array of configuration settings.
	*
	* @see        JController
	* @since      1.6
	*/
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.`id`',
				'citizen_id', 'a.`citizen_id`',
				'image_url', 'a.`image_url`',
				'title_id', 'a.`title_id`',
				'name', 'a.`name`',
				'surname', 'a.`surname`',
				'birthdate', 'a.`birthdate`',
				'education_level_id', 'a.`education_level_id`',
				'address', 'a.`address`',
				'region_id', 'a.`region_id`',
				'province_id', 'a.`province_id`',
				'district_id', 'a.`district_id`',
				'sub_district_id', 'a.`sub_district_id`',
				'post_code', 'a.`post_code`',
				'latitude', 'a.`latitude`',
				'longitude', 'a.`longitude`',
				'mobile', 'a.`mobile`',
				'fax', 'a.`fax`',
				'email', 'a.`email`',
				'line_id', 'a.`line_id`',
				'ordering', 'a.`ordering`',
				'state', 'a.`state`',
				'created_by', 'a.`created_by`',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
		$this->setState('filter.state', $published);
		// Filtering birthdate
		$this->setState('filter.birthdate.from', $app->getUserStateFromRequest($this->context.'.filter.birthdate.from', 'filter_from_birthdate', '', 'string'));
		$this->setState('filter.birthdate.to', $app->getUserStateFromRequest($this->context.'.filter.birthdate.to', 'filter_to_birthdate', '', 'string'));

		// Filtering education_level_id
		$this->setState('filter.education_level_id', $app->getUserStateFromRequest($this->context.'.filter.education_level_id', 'filter_education_level_id', '', 'string'));

		// Filtering region_id
		$this->setState('filter.region_id', $app->getUserStateFromRequest($this->context.'.filter.region_id', 'filter_region_id', '', 'string'));

		// Filtering province_id
		$this->setState('filter.province_id', $app->getUserStateFromRequest($this->context.'.filter.province_id', 'filter_province_id', '', 'string'));

		// Filtering district_id
		$this->setState('filter.district_id', $app->getUserStateFromRequest($this->context.'.filter.district_id', 'filter_district_id', '', 'string'));

		// Filtering sub_district_id
		$this->setState('filter.sub_district_id', $app->getUserStateFromRequest($this->context.'.filter.sub_district_id', 'filter_sub_district_id', '', 'string'));


		// Load the parameters.
		$params = JComponentHelper::getParams('com_edairy');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.citizen_id', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select', 'DISTINCT a.*'
			)
		);
		$query->from('`#__ed_farmer` AS a');

		// Join over the users for the checked out user
		$query->select("uc.name AS uEditor");
		$query->join("LEFT", "#__users AS uc ON uc.id=a.checked_out");
		// Join over the foreign key 'region_id'
		$query->select('`#__ed_region_2698780`.`name` AS regions_fk_value_2698780');
		$query->join('LEFT', '#__ed_region AS #__ed_region_2698780 ON #__ed_region_2698780.`id` = a.`region_id`');
		// Join over the foreign key 'province_id'
		$query->select('`#__ed_province_2698781`.`name` AS provinces_fk_value_2698781');
		$query->join('LEFT', '#__ed_province AS #__ed_province_2698781 ON #__ed_province_2698781.`id` = a.`province_id`');
		// Join over the foreign key 'district_id'
		$query->select('`#__ed_district_2698782`.`name` AS districts_fk_value_2698782');
		$query->join('LEFT', '#__ed_district AS #__ed_district_2698782 ON #__ed_district_2698782.`id` = a.`district_id`');
		// Join over the foreign key 'sub_district_id'
		$query->select('`#__ed_sub_district_2698783`.`name` AS sub_districts_fk_value_2698783');
		$query->join('LEFT', '#__ed_sub_district AS #__ed_sub_district_2698783 ON #__ed_sub_district_2698783.`id` = a.`sub_district_id`');

		// Join over the user field 'created_by'
		$query->select('`created_by`.name AS `created_by`');
		$query->join('LEFT', '#__users AS `created_by` ON `created_by`.id = a.`created_by`');

		// Filter by published state
		$published = $this->getState('filter.state');

		if (is_numeric($published))
		{
			$query->where('a.state = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.state IN (0, 1))');
		}

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				$query->where('( a.citizen_id LIKE ' . $search . '  OR  a.name LIKE ' . $search . '  OR  a.surname LIKE ' . $search . '  OR  a.post_code LIKE ' . $search . '  OR  a.mobile LIKE ' . $search . '  OR  a.fax LIKE ' . $search . '  OR  a.email LIKE ' . $search . '  OR  a.line_id LIKE ' . $search . ' )');
			}
		}


		//Filtering birthdate
		$filter_birthdate_from = $this->state->get("filter.birthdate.from");

		if ($filter_birthdate_from !== null && !empty($filter_birthdate_from))
		{
			$query->where("a.`birthdate` >= '".$db->escape($filter_birthdate_from)."'");
		}
		$filter_birthdate_to = $this->state->get("filter.birthdate.to");

		if ($filter_birthdate_to !== null  && !empty($filter_birthdate_to))
		{
			$query->where("a.`birthdate` <= '".$db->escape($filter_birthdate_to)."'");
		}

		//Filtering education_level_id
		$filter_education_level_id = $this->state->get("filter.education_level_id");
		if ($filter_education_level_id !== null && !empty($filter_education_level_id))
		{
			$query->where("a.`education_level_id` = '".$db->escape($filter_education_level_id)."'");
		}

		//Filtering region_id
		$filter_region_id = $this->state->get("filter.region_id");
		if ($filter_region_id !== null && !empty($filter_region_id))
		{
			$query->where("a.`region_id` = '".$db->escape($filter_region_id)."'");
		}

		//Filtering province_id
		$filter_province_id = $this->state->get("filter.province_id");
		if ($filter_province_id !== null && !empty($filter_province_id))
		{
			$query->where("a.`province_id` = '".$db->escape($filter_province_id)."'");
		}

		//Filtering district_id
		$filter_district_id = $this->state->get("filter.district_id");
		if ($filter_district_id !== null && !empty($filter_district_id))
		{
			$query->where("a.`district_id` = '".$db->escape($filter_district_id)."'");
		}

		//Filtering sub_district_id
		$filter_sub_district_id = $this->state->get("filter.sub_district_id");
		if ($filter_sub_district_id !== null && !empty($filter_sub_district_id))
		{
			$query->where("a.`sub_district_id` = '".$db->escape($filter_sub_district_id)."'");
		}
		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');

		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();

		foreach ($items as $oneItem) {
					$oneItem->title_id = JText::_('COM_EDAIRY_FARMERS_TITLE_ID_OPTION_' . strtoupper($oneItem->title_id));
					$oneItem->education_level_id = JText::_('COM_EDAIRY_FARMERS_EDUCATION_LEVEL_ID_OPTION_' . strtoupper($oneItem->education_level_id));

			if (isset($oneItem->region_id))
			{
				$values = explode(',', $oneItem->region_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('`#__ed_region_2698780`.`name`')
							->from($db->quoteName('#__ed_region', '#__ed_region_2698780'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->name;
					}
				}

			$oneItem->region_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->region_id;

			}

			if (isset($oneItem->province_id))
			{
				$values = explode(',', $oneItem->province_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('`#__ed_province_2698781`.`name`')
							->from($db->quoteName('#__ed_province', '#__ed_province_2698781'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->name;
					}
				}

			$oneItem->province_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->province_id;

			}

			if (isset($oneItem->district_id))
			{
				$values = explode(',', $oneItem->district_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('`#__ed_district_2698782`.`name`')
							->from($db->quoteName('#__ed_district', '#__ed_district_2698782'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->name;
					}
				}

			$oneItem->district_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->district_id;

			}

			if (isset($oneItem->sub_district_id))
			{
				$values = explode(',', $oneItem->sub_district_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('`#__ed_sub_district_2698783`.`name`')
							->from($db->quoteName('#__ed_sub_district', '#__ed_sub_district_2698783'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->name;
					}
				}

			$oneItem->sub_district_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->sub_district_id;

			}
		}
		return $items;
	}
}
