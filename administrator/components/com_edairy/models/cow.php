<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Edairy model.
 *
 * @since  1.6
 */
class EdairyModelCow extends JModelAdmin
{
	/**
	 * @var      string    The prefix to use with controller messages.
	 * @since    1.6
	 */
	protected $text_prefix = 'COM_EDAIRY';

	/**
	 * @var   	string  	Alias to manage history control
	 * @since   3.2
	 */
	public $typeAlias = 'com_edairy.cow';

	/**
	 * @var null  Item data
	 * @since  1.6
	 */
	protected $item = null;

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param   string  $type    The table type to instantiate
	 * @param   string  $prefix  A prefix for the table class name. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return    JTable    A database object
	 *
	 * @since    1.6
	 */
	public function getTable($type = 'Cow', $prefix = 'EdairyTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      An optional array of data for the form to interogate.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  JForm  A JForm object on success, false on failure
	 *
	 * @since    1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Initialise variables.
		$app = JFactory::getApplication();

		// Get the form.
		$form = $this->loadForm(
			'com_edairy.cow', 'cow',
			array('control' => 'jform',
				'load_data' => $loadData
			)
		);

		if (empty($form))
		{
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return   mixed  The data for the form.
	 *
	 * @since    1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_edairy.edit.cow.data', array());

		if (empty($data))
		{
			if ($this->item === null)
			{
				$this->item = $this->getItem();
			}

			$data = $this->item;
		}

		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param   integer  $pk  The id of the primary key.
	 *
	 * @return  mixed    Object on success, false on failure.
	 *
	 * @since    1.6
	 */
	public function getItem($pk = null)
	{
		if ($item = parent::getItem($pk))
		{
			// Do any procesing on fields here if needed

			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
				->select("f.member_code, c.name as coop_name, (select CONCAT(CONCAT(name, ' '), surname) FROM #__ed_farmer where farm_id=f.id) as farmer_name")
				->from('#__ed_farm f')
				->join('left', "#__ed_coop c on c.id=f.coop_id")
				->where("f.id='{$item->farm_id}'")
				->limit(1);
			$db->setQuery($query);
			$result = $db->loadObject();
			$item->farm = $result;


			//Move_out

			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
				->select("m.*, (select CONCAT(CONCAT(member_code,' '), name) from #__ed_farm where id=from_farm_id) as from_farm, (select CONCAT(CONCAT(member_code,' '), name) from #__ed_farm where id=to_farm_id) as to_farm")
				->from('#__ed_cow_move_out m')
				->where("m.cow_id='{$item->id}' and m.state=1")
				->order('move_out_date desc');
			$db->setQuery($query);
			$result = $db->loadObjectList();
			$item->move_out_history = $result;

			//Insemination

			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
				->select("*")
				->from('#__ed_cow_insemination_line')
				->where("cow_id='{$item->id}' and state=1");
			$db->setQuery($query);
			$result = $db->loadObjectList();
			$item->insemination_history = $result;

			//HasChild

			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
				->select("*")
				->from('#__ed_cow_has_child')
				->where("cow_id='{$item->id}' and state=1");
			$db->setQuery($query);
			$result = $db->loadObjectList();
			$item->cow_has_child_history = $result;


			//Health

			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
				->select("hl.*, (select create_date from #__ed_cow_health h where h.id=cow_health_id) as create_date, hs.name as health_service, hss.name as health_sub_service, m.name as medicine")
				->from('#__ed_cow_health_line hl')
				->join("left", "#__ed_cow_health_service hs on hs.id=hl.cow_health_service_id")
				->join("left", "#__ed_cow_health_sub_service hss on hs.id=hl.cow_health_sub_service_id")
				->join("left", "#__ed_cow_medicine m on m.id=hl.cow_medicine_id")
				->where("cow_id='{$item->id}' and hl.state=1")
				->order("create_date desc");
			$db->setQuery($query);
			$result = $db->loadObjectList();
			$item->health_history = $result;




			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
				->select("chcs.*, c.name, c.description")
				->from('#__ed_cow_has_cow_status chcs')
				->join("left", "#__ed_cow_status c on c.id = cow_status_id")
				->where("cow_id='{$item->id}' and chcs.state=1")
				->order("chcs.id desc");
			$db->setQuery($query);
			$result = $db->loadObjectList();
			$item->cow_status_history = $result;

			$oneItem = $item;
			if (isset($oneItem->cow_id))
			{
				$value = $oneItem->cow_id;
				$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select("*")
							->from('#__ed_cow_breed')
							->where($db->quoteName('cow_id') . ' = '. $db->quote($db->escape($value)))
							->order("percent desc");
					$db->setQuery($query);
					$results = $db->loadObjectList();
					$oneItem->breeds = $results;
			}
			$item = $oneItem;



		}

		return $item;
	}

	/**
	 * Method to duplicate an Cow
	 *
	 * @param   array  &$pks  An array of primary key IDs.
	 *
	 * @return  boolean  True if successful.
	 *
	 * @throws  Exception
	 */
	public function duplicate(&$pks)
	{
		$user = JFactory::getUser();

		// Access checks.
		if (!$user->authorise('core.create', 'com_edairy'))
		{
			throw new Exception(JText::_('JERROR_CORE_CREATE_NOT_PERMITTED'));
		}

		$dispatcher = JEventDispatcher::getInstance();
		$context    = $this->option . '.' . $this->name;

		// Include the plugins for the save events.
		JPluginHelper::importPlugin($this->events_map['save']);

		$table = $this->getTable();

		foreach ($pks as $pk)
		{
			if ($table->load($pk, true))
			{
				// Reset the id to create a new record.
				$table->id = 0;

				if (!$table->check())
				{
					throw new Exception($table->getError());
				}
				
				if (!empty($table->farm_id))
				{
					if (is_array($table->farm_id))
					{
						$table->farm_id = implode(',', $table->farm_id);
					}
				}
				else
				{
					$table->farm_id = '';
				}

				if (!empty($table->move_from_farm_id))
				{
					if (is_array($table->move_from_farm_id))
					{
						$table->move_from_farm_id = implode(',', $table->move_from_farm_id);
					}
				}
				else
				{
					$table->move_from_farm_id = '';
				}

				if (!empty($table->father_cow_id))
				{
					if (is_array($table->father_cow_id))
					{
						$table->father_cow_id = implode(',', $table->father_cow_id);
					}
				}
				else
				{
					$table->father_cow_id = '';
				}

				if (!empty($table->mother_cow_id))
				{
					if (is_array($table->mother_cow_id))
					{
						$table->mother_cow_id = implode(',', $table->mother_cow_id);
					}
				}
				else
				{
					$table->mother_cow_id = '';
				}


				// Trigger the before save event.
				$result = $dispatcher->trigger($this->event_before_save, array($context, &$table, true));

				if (in_array(false, $result, true) || !$table->store())
				{
					throw new Exception($table->getError());
				}

				// Trigger the after save event.
				$dispatcher->trigger($this->event_after_save, array($context, &$table, true));
			}
			else
			{
				throw new Exception($table->getError());
			}
		}

		// Clean cache
		$this->cleanCache();

		return true;
	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @param   JTable  $table  Table Object
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function prepareTable($table)
	{
		jimport('joomla.filter.output');

		$table->birthdate = formatSQLDate($_POST["birthdate"]);

		$table->move_farm_date = formatSQLDate($_POST["move_farm_date"]);

		if($table->create_datetime == ""){
			
			$table->create_datetime = date("Y-m-d");
		}
		$table->update_datetime = date("Y-m-d");
		
		$user = JFactory::getUser();
		if($table->create_by==""|| $table->create_by==0){
			
			$table->create_by = $user->id;
		}
		$table->update_by = $user->id;



		if (empty($table->id))
		{
			// Set ordering to the last item if not set
			if (@$table->ordering === '')
			{
				$db = JFactory::getDbo();
				$db->setQuery('SELECT MAX(ordering) FROM #__ed_cow');
				$max             = $db->loadResult();
				$table->ordering = $max + 1;
			}
		}
	}


	public function getMasterData(){

		$masterData = new StdClass();
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__ed_cow_status')
			->where("state=1");
		$db->setQuery($query);
		$masterData->mas_cow_status = $db->loadObjectList();


		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("id, member_code, name")
			->from('#__ed_farm')
			->where("state=1");
		$db->setQuery($query);
		$masterData->mas_farm = $db->loadObjectList();


		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__ed_move_out_objective')
			->where("state=1");
		$db->setQuery($query);
		$masterData->mas_move_out_objective = $db->loadObjectList();


		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__ed_move_out_reason')
			->where("state=1");
		$db->setQuery($query);
		$masterData->mas_move_out_reason = $db->loadObjectList();

		return $masterData;

	}



	public function updateCowStatus(){
		$data = new StdClass();
		$data->cow_id = $_POST["cow_id"];
		$data->cow_status_id = $_POST["cow_status_id"];
		$data->status_date = date("Y-m-d");

		$user = JFactory::getUser();

		$data->update_by = $user->id;
		$data->update_date = date("Y-m-d");
		$data->ordering=0;
		$data->state=1;
		$data->checked_out = 0;
		$data->checked_out_time = "0000-00-00 00:00:00";
		$data->created_by = $user->id;

			$addDataResult = JFactory::getDbo()->insertObject('#__ed_cow_has_cow_status', $data, 'id');

			if($addDataResult){
				$data = new StdClass();
				$data->id = $_POST["cow_id"];
				$data->cow_status_id = $_POST["cow_status_id"];
				$addDataResult = JFactory::getDbo()->updateObject('#__ed_cow', $data, 'id');

				return true;
			}else{
				return false;
			}



		
		
	}


	public function searchCow(){

		$masterData = new StdClass();
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("c.cow_id, c.name, c.father_cow_id, c.mother_cow_id, (select CONCAT(cow_id, CONCAT(' ', name)) from #__ed_cow cc where cc.cow_id=c.father_cow_id) as granddad_name, (select CONCAT(cow_id, CONCAT(' ', name)) from #__ed_cow ccc where ccc.cow_id=c.mother_cow_id) as grandmom_name")
			->from('#__ed_cow c')
			->where("c.cow_id='{$_GET[cow_id]}' and state=1")
			->limit(1);
		$db->setQuery($query);

		$oneItem = $db->loadObject();
			if (isset($oneItem->cow_id))
			{
				$value = $oneItem->cow_id;
				$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select("*")
							->from('#__ed_cow_breed')
							->where($db->quoteName('cow_id') . ' = '. $db->quote($db->escape($value)))
							->order("percent desc");
					$db->setQuery($query);
					$results = $db->loadObjectList();
					$oneItem->breeds = $results;
			}
		return $oneItem;


	}

	public function getSearchCow(){


		$masterData = new StdClass();
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("c.cow_id, c.name, c.father_cow_id, c.mother_cow_id, (select CONCAT(cow_id, CONCAT(' ', name)) from #__ed_cow cc where cc.cow_id=c.father_cow_id) as granddad_name, (select CONCAT(cow_id, CONCAT(' ', name)) from #__ed_cow ccc where ccc.cow_id=c.mother_cow_id) as grandmom_name")
			->from('#__ed_cow c')
			->where("c.cow_id='{$_GET[cow_id]}' and state=1")
			->limit(1);
		$db->setQuery($query);

		$oneItem = $db->loadObject();
			if (isset($oneItem->cow_id))
			{
				$value = $oneItem->cow_id;
				$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select("*")
							->from('#__ed_cow_breed')
							->where($db->quoteName('cow_id') . ' = '. $db->quote($db->escape($value)))
							->order("percent desc");
					$db->setQuery($query);
					$results = $db->loadObjectList();
					$oneItem->breeds = $results;
			}
		return $oneItem;


	}


	public function getAllCow(){

		$masterData = new StdClass();
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("CONCAT(CONCAT(c.cow_id, ' '), name) as cow_name")
			->from('#__ed_cow c')
			->where("state=1");
		$db->setQuery($query);

		$results = $db->loadObjectList();
		$resultsArray = Array();
		foreach($results as $key=>$data){
			array_push($resultsArray, $data->cow_name);
		}
			
		return $resultsArray;
		


	}


	public function saveBreed(){

		$calculated_breed = str_replace(" ", "", $_REQUEST["calculated_breed"]);

		$pos = strrpos($calculated_breed, ",");
		if ($pos === false) { 
			$calculated_breed_array[0] = $calculated_breed;
		}else{
			$calculated_breed_array =explode(",", $calculated_breed);
		}

		foreach($calculated_breed_array as $ckey=>$cdata){
			$pos = strrpos($cdata, "%");
			if ($pos === false) { 
				return false;
			}
		}

		$total=0;
		foreach($calculated_breed_array as $ckey=>$cdata){
			$cdata_array = explode("%", $cdata);
			$total = $total+intval($cdata_array[0]);
		}
		if($total>100){
			return false;
		}


		$db = JFactory::getDbo();
		 
		$query = $db->getQuery(true);
		 
		 
		$query->delete($db->quoteName('#__ed_cow_breed'));
		$query->where("cow_id='{$_REQUEST[cow_id]}'");
		 
		$db->setQuery($query);
		$result = $db->execute();

		foreach($calculated_breed_array as $ckey=>$cdata){
			$cdata_array = explode("%", $cdata);


			$data = new StdClass();
			$data->cow_id = $_REQUEST["cow_id"];
			$data->breed = $cdata_array[1];
			$data->percent = $cdata_array[0];

			$addDataResult = JFactory::getDbo()->insertObject('#__ed_cow_breed', $data, 'id');
		}


		return true;

		
		
	}

	public function getMoveOut(){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("*")
			->from('#__ed_cow_move_out')
			->where("id='{$_POST[id]}'");
		$db->setQuery($query);
		$result = $db->loadObject();
		return $result;
	}


	public function saveMoveOut(){
		$data = new StdClass();
		$data->cow_id = $_POST["move_out_cow_id"];
		$data->move_out_date = formatSQLDate($_POST["move_out_move_out_date"]);
		$data->move_type = $_POST["move_out_move_type"];
		$data->from_farm_id = $_POST["move_out_from_farm_id"];

		$data->to_farm_id = null;
		$data->move_objective = null;
		$data->move_reason = null;

		if($_POST["move_out_move_type"]=="1"){
			$data->to_farm_id = $_POST["move_out_to_farm_id1"];
		}else if($_POST["move_out_move_type"]=="3"){
			$data->to_farm_id = $_POST["move_out_to_farm_id2"];
			$data->move_objective = $_POST["move_out_move_objective"];
			$data->move_reason = $_POST["move_out_move_reason"];
		}

		

		

		$user = JFactory::getUser();

		
		$data->ordering=0;
		$data->state=1;
		$data->checked_out = 0;
		$data->checked_out_time = "0000-00-00 00:00:00";
		$data->created_by = $user->id;

		if($_POST["move_out_id"]!=""){
			$data->id = $_POST["move_out_id"];
			$updateDataResult = JFactory::getDbo()->updateObject('#__ed_cow_move_out', $data, 'id');

			if($updateDataResult){
				return true;
			}else{
				return false;
			}
		
		}else{
			$addDataResult = JFactory::getDbo()->insertObject('#__ed_cow_move_out', $data, 'id');

			if($addDataResult){
				return true;
			}else{
				return false;
			}
		}
	}

	public function deleteMoveOut(){
		$db = JFactory::getDbo();
		 
		$query = $db->getQuery(true);
		 
		 
		$query->delete($db->quoteName('#__ed_cow_move_out'));
		$query->where("id='{$_POST[id]}'");
		 
		$db->setQuery($query);
		 
		$result = $db->execute();
		if($result){
			return true;
		}else{
			return false;
		}
	}

}
