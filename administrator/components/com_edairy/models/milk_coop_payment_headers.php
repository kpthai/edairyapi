<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Edairy records.
 *
 * @since  1.6
 */
class EdairyModelMilk_coop_payment_headers extends JModelList
{
/**
	* Constructor.
	*
	* @param   array  $config  An optional associative array of configuration settings.
	*
	* @see        JController
	* @since      1.6
	*/
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.`id`',
				'coop_id', 'a.`coop_id`',
				'period', 'a.`period`',
				'from_date', 'a.`from_date`',
				'to_date', 'a.`to_date`',
				'total_car', 'a.`total_car`',
				'total_milk_amount', 'a.`total_milk_amount`',
				'grand_total', 'a.`grand_total`',
				'ordering', 'a.`ordering`',
				'state', 'a.`state`',
				'created_by', 'a.`created_by`',
				'modified_by', 'a.`modified_by`',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
		$this->setState('filter.state', $published);
		// Filtering coop_id
		$this->setState('filter.coop_id', $app->getUserStateFromRequest($this->context.'.filter.coop_id', 'filter_coop_id', '', 'string'));


		// Load the parameters.
		$params = JComponentHelper::getParams('com_edairy');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.coop_id', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select', 'DISTINCT a.*'
			)
		);
		$query->from('`#__ed_milk_coop_payment_header` AS a');

		// Join over the users for the checked out user
		$query->select("uc.name AS uEditor");
		$query->join("LEFT", "#__users AS uc ON uc.id=a.checked_out");
		// Join over the foreign key 'coop_id'
		$query->select('CONCAT(`#__ed_coop_2781637`.`coop_code`, \' \', `#__ed_coop_2781637`.`coop_abbr`, \' \', `#__ed_coop_2781637`.`name`) AS coops_fk_value_2781637');
		$query->join('LEFT', '#__ed_coop AS #__ed_coop_2781637 ON #__ed_coop_2781637.`id` = a.`coop_id`');

		// Join over the user field 'created_by'
		$query->select('`created_by`.name AS `created_by`');
		$query->join('LEFT', '#__users AS `created_by` ON `created_by`.id = a.`created_by`');

		// Join over the user field 'modified_by'
		$query->select('`modified_by`.name AS `modified_by`');
		$query->join('LEFT', '#__users AS `modified_by` ON `modified_by`.id = a.`modified_by`');

		// Filter by published state
		$published = $this->getState('filter.state');

		if (is_numeric($published))
		{
			$query->where('a.state = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.state IN (0, 1))');
		}

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				$query->where('(CONCAT(`#__ed_coop_2781637`.`coop_code`, \' \', `#__ed_coop_2781637`.`coop_abbr`, \' \', `#__ed_coop_2781637`.`name`) LIKE ' . $search . '  OR  a.period LIKE ' . $search . '  OR  a.from_date LIKE ' . $search . '  OR  a.to_date LIKE ' . $search . '  OR  a.total_car LIKE ' . $search . '  OR  a.total_milk_amount LIKE ' . $search . '  OR  a.grand_total LIKE ' . $search . ' )');
			}
		}

		// Custom Search
		if ($_REQUEST["search_date_from"]!="" && $_REQUEST["search_date_to"]!="")
		{
			$date_from = formatSQLDate($_REQUEST["search_date_from"]);
			$date_to = formatSQLDate($_REQUEST["search_date_to"]);
			$query->where("a.from_date >= '{$date_from}' and a.to_date <= '{$date_to}' ");
		}

		if($_REQUEST["search_coop"]!=""){
			$search = $db->Quote('%' . $db->escape($_REQUEST["search_coop"], true) . '%');
				$query->where('CONCAT(`#__ed_coop_2781637`.`coop_code`, \' \', `#__ed_coop_2781637`.`coop_abbr`, \' \', `#__ed_coop_2781637`.`name`) LIKE ' . $search);
		}

		// Filtering coop_id
		$filter_coop_id = $this->state->get("filter.coop_id");

		if ($filter_coop_id !== null && !empty($filter_coop_id))
		{
			$query->where("a.`coop_id` = '".$db->escape($filter_coop_id)."'");
		}
		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');

		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();

		foreach ($items as $oneItem)
		{

			if (isset($oneItem->coop_id))
			{
				$values = explode(',', $oneItem->coop_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('CONCAT(`#__ed_coop_2781637`.`coop_code`, \' \', `#__ed_coop_2781637`.`coop_abbr`, \' \', `#__ed_coop_2781637`.`name`) AS `fk_value`')
							->from($db->quoteName('#__ed_coop', '#__ed_coop_2781637'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->fk_value;
					}
				}
			$oneItem->original_coop_id = $oneItem->coop_id;
			$oneItem->coop_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->coop_id;

			}
		}

		return $items;
	}
}
