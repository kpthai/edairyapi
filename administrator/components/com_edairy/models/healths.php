<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Edairy records.
 *
 * @since  1.6
 */
class EdairyModelHealths extends JModelList
{
/**
	* Constructor.
	*
	* @param   array  $config  An optional associative array of configuration settings.
	*
	* @see        JController
	* @since      1.6
	*/
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.`id`',
				'run_no', 'a.`run_no`',
				'create_date', 'a.`create_date`',
				'address', 'a.`address`',
				'payment_type', 'a.`payment_type`',
				'user_id1', 'a.`user_id1`',
				'user_id2', 'a.`user_id2`',
				'grand_total', 'a.`grand_total`',
				'farm_id', 'a.`farm_id`',
				'farmer_id', 'a.`farmer_id`',
				'coop_id', 'a.`coop_id`',
				'ordering', 'a.`ordering`',
				'state', 'a.`state`',
				'created_by', 'a.`created_by`',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
		$this->setState('filter.state', $published);
		// Filtering create_date
		$this->setState('filter.create_date.from', $app->getUserStateFromRequest($this->context.'.filter.create_date.from', 'filter_from_create_date', '', 'string'));
		$this->setState('filter.create_date.to', $app->getUserStateFromRequest($this->context.'.filter.create_date.to', 'filter_to_create_date', '', 'string'));

		// Filtering payment_type
		$this->setState('filter.payment_type', $app->getUserStateFromRequest($this->context.'.filter.payment_type', 'filter_payment_type', '', 'string'));

		// Filtering user_id1
		$this->setState('filter.user_id1', $app->getUserStateFromRequest($this->context.'.filter.user_id1', 'filter_user_id1', '', 'string'));

		// Filtering farm_id
		$this->setState('filter.farm_id', $app->getUserStateFromRequest($this->context.'.filter.farm_id', 'filter_farm_id', '', 'string'));

		// Filtering farmer_id
		$this->setState('filter.farmer_id', $app->getUserStateFromRequest($this->context.'.filter.farmer_id', 'filter_farmer_id', '', 'string'));

		// Filtering coop_id
		$this->setState('filter.coop_id', $app->getUserStateFromRequest($this->context.'.filter.coop_id', 'filter_coop_id', '', 'string'));


		// Load the parameters.
		$params = JComponentHelper::getParams('com_edairy');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.run_no', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select', 'DISTINCT a.*'
			)
		);
		$query->from('`#__ed_cow_health` AS a');

		// Join over the users for the checked out user
		$query->select("uc.name AS uEditor");
		$query->join("LEFT", "#__users AS uc ON uc.id=a.checked_out");

		// Join over the user field 'user_id1'
		$query->select('`user_id1`.name AS `user_id1`');
		$query->join('LEFT', '#__users AS `user_id1` ON `user_id1`.id = a.`user_id1`');

		// Join over the user field 'user_id2'
		$query->select('`user_id2`.name AS `user_id2`');
		$query->join('LEFT', '#__users AS `user_id2` ON `user_id2`.id = a.`user_id2`');
		// Join over the foreign key 'farm_id'
		$query->select('CONCAT(`#__ed_farm_2754467`.`member_code`, \' \', `#__ed_farm_2754467`.`name`) AS farms_fk_value_2754467');
		$query->join('LEFT', '#__ed_farm AS #__ed_farm_2754467 ON #__ed_farm_2754467.`id` = a.`farm_id`');
		// Join over the foreign key 'farmer_id'
		$query->select('CONCAT(`#__ed_farmer_2754468`.`citizen_id`, \' \', `#__ed_farmer_2754468`.`name`, \' \', `#__ed_farmer_2754468`.`surname`) AS farmers_fk_value_2754468');
		$query->join('LEFT', '#__ed_farmer AS #__ed_farmer_2754468 ON #__ed_farmer_2754468.`id` = a.`farmer_id`');
		// Join over the foreign key 'coop_id'
		$query->select('CONCAT(`#__ed_coop_2754469`.`coop_code`, \' \', `#__ed_coop_2754469`.`name`) AS coops_fk_value_2754469');
		$query->join('LEFT', '#__ed_coop AS #__ed_coop_2754469 ON #__ed_coop_2754469.`id` = a.`coop_id`');

		// Join over the user field 'created_by'
		$query->select('`created_by`.name AS `created_by`');
		$query->join('LEFT', '#__users AS `created_by` ON `created_by`.id = a.`created_by`');

		// Filter by published state
		$published = $this->getState('filter.state');

		if (is_numeric($published))
		{
			$query->where('a.state = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.state IN (0, 1))');
		}

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				$query->where('( a.run_no LIKE ' . $search . '  OR  a.grand_total LIKE ' . $search . ' )');
			}
		}


		//Filtering create_date
		$filter_create_date_from = $this->state->get("filter.create_date.from");

		if ($filter_create_date_from !== null && !empty($filter_create_date_from))
		{
			$query->where("a.`create_date` >= '".$db->escape($filter_create_date_from)."'");
		}
		$filter_create_date_to = $this->state->get("filter.create_date.to");

		if ($filter_create_date_to !== null  && !empty($filter_create_date_to))
		{
			$query->where("a.`create_date` <= '".$db->escape($filter_create_date_to)."'");
		}

		//Filtering payment_type
		$filter_payment_type = $this->state->get("filter.payment_type");
		if ($filter_payment_type !== null && !empty($filter_payment_type))
		{
			$query->where("a.`payment_type` = '".$db->escape($filter_payment_type)."'");
		}

		//Filtering user_id1
		$filter_user_id1 = $this->state->get("filter.user_id1");
		if ($filter_user_id1 !== null && !empty($filter_user_id1))
		{
			$query->where("a.`user_id1` = '".$db->escape($filter_user_id1)."'");
		}

		//Filtering farm_id
		$filter_farm_id = $this->state->get("filter.farm_id");
		if ($filter_farm_id !== null && !empty($filter_farm_id))
		{
			$query->where("a.`farm_id` = '".$db->escape($filter_farm_id)."'");
		}

		//Filtering farmer_id
		$filter_farmer_id = $this->state->get("filter.farmer_id");
		if ($filter_farmer_id !== null && !empty($filter_farmer_id))
		{
			$query->where("a.`farmer_id` = '".$db->escape($filter_farmer_id)."'");
		}

		//Filtering coop_id
		$filter_coop_id = $this->state->get("filter.coop_id");
		if ($filter_coop_id !== null && !empty($filter_coop_id))
		{
			$query->where("a.`coop_id` = '".$db->escape($filter_coop_id)."'");
		}


		//Custom Filter
		if ($_REQUEST["search_run_no"]!="")
		{
			$query->where("a.`run_no` LIKE '%".$db->escape($_REQUEST["search_run_no"])."%'");
		}

		if ($_REQUEST["search_coop"]!="")
		{
			$query->where("a.coop_id in (select id from #__ed_coop c where c.name like '%{$_REQUEST[search_coop]}%' or c.coop_code like '%{$_REQUEST[search_coop]}%' )");
		}

		if ($_REQUEST["search_farm_name"]!="")
		{
			$query->where("a.farm_id in (select id from #__ed_farm f where f.name like '%{$_REQUEST[search_farm_name]}%' )");
		}

		if ($_REQUEST["search_member_code"]!="")
		{

			$query->where("a.farm_id in (select id from #__ed_farm f where f.member_code like '%{$_REQUEST[search_member_code]}%' )");
		}

		if ($_REQUEST["search_cow_id"]!="")
		{
			$query->where("a.id in (select cow_insemination_id from #__ed_cow_insemination_line l where l.cow_id in ( select id from #__ed_cow c where c.cow_id like '%{$_REQUEST[search_cow_id]}%'))");
		}

		if ($_REQUEST["search_cow_name"]!="")
		{
			$query->where("a.id in (select cow_insemination_id from #__ed_cow_insemination_line l where l.cow_id in ( select id from #__ed_cow c where c.name like '%{$_REQUEST[search_cow_name]}%'))");
		
		}

		if ($_REQUEST["search_farmer_name"]!="")
		{
			$query->where("a.farmer_id in (select id from #__ed_farmer f where f.name like '%{$_REQUEST[search_farmer_name]}%' )");
		}

		if ($_REQUEST["search_farmer_surname"]!="")
		{
			$query->where("a.farmer_id in (select id from #__ed_farmer f where f.surname like '%{$_REQUEST[search_farmer_surname]}%' )");
		}

		if ($_REQUEST["search_date_from"]!="" && $_REQUEST["search_date_to"]!="")
		{
			$date_from = formatSQLDate($_REQUEST["search_date_from"]);
			$date_to = formatSQLDate($_REQUEST["search_date_to"]);
			$query->where("a.create_date between '{$date_from}' and '{$date_to}'");
		}


		//User Role Filter
		$user = JFactory::getUser();

		if($user->role==1){ // Farmer
			$query->where("a.farmer_id in ({$user->farmer_id})");
		}
		
		if($user->role==2){ // Vet
			$query->where("a.farm_id in (select id from #__ed_farm f where f.coop_id in ({$user->coop_id}))");
		}


		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');

		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();

		foreach ($items as $oneItem) {
					$oneItem->payment_type = ($oneItem->payment_type == '') ? '' : JText::_('COM_EDAIRY_HEALTHS_PAYMENT_TYPE_OPTION_' . strtoupper($oneItem->payment_type));

			if (isset($oneItem->farm_id))
			{
				$values = explode(',', $oneItem->farm_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('CONCAT(`#__ed_farm_2754467`.`member_code`, \' \', `#__ed_farm_2754467`.`name`) AS `fk_value`')
							->from($db->quoteName('#__ed_farm', '#__ed_farm_2754467'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->fk_value;
					}
				}

			$oneItem->farm_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->farm_id;

			}

			if (isset($oneItem->farmer_id))
			{
				$values = explode(',', $oneItem->farmer_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('CONCAT(`#__ed_farmer_2754468`.`citizen_id`, \' \', `#__ed_farmer_2754468`.`name`, \' \', `#__ed_farmer_2754468`.`surname`) AS `fk_value`')
							->from($db->quoteName('#__ed_farmer', '#__ed_farmer_2754468'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->fk_value;
					}
				}

			$oneItem->farmer_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->farmer_id;

			}

			if (isset($oneItem->coop_id))
			{
				$values = explode(',', $oneItem->coop_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('CONCAT(`#__ed_coop_2754469`.`coop_code`, \' \', `#__ed_coop_2754469`.`name`) AS `fk_value`')
							->from($db->quoteName('#__ed_coop', '#__ed_coop_2754469'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->fk_value;
					}
				}

			$oneItem->coop_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->coop_id;

			}
		}
		return $items;
	}
}
