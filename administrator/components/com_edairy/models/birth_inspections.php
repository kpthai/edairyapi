<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Edairy records.
 *
 * @since  1.6
 */
class EdairyModelBirth_inspections extends JModelList
{
/**
	* Constructor.
	*
	* @param   array  $config  An optional associative array of configuration settings.
	*
	* @see        JController
	* @since      1.6
	*/
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.`id`',
				'cow_id', 'a.`cow_id`',
				'birth_date', 'a.`birth_date`',
				'birth_method', 'a.`birth_method`',
				'is_child_alive', 'a.`is_child_alive`',
				'child_gender', 'a.`child_gender`',
				'child_weight', 'a.`child_weight`',
				'child_cow_id', 'a.`child_cow_id`',
				'create_by', 'a.`create_by`',
				'ordering', 'a.`ordering`',
				'state', 'a.`state`',
				'created_by', 'a.`created_by`',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
		$this->setState('filter.state', $published);
		// Filtering cow_id
		$this->setState('filter.cow_id', $app->getUserStateFromRequest($this->context.'.filter.cow_id', 'filter_cow_id', '', 'string'));

		// Filtering birth_date
		$this->setState('filter.birth_date.from', $app->getUserStateFromRequest($this->context.'.filter.birth_date.from', 'filter_from_birth_date', '', 'string'));
		$this->setState('filter.birth_date.to', $app->getUserStateFromRequest($this->context.'.filter.birth_date.to', 'filter_to_birth_date', '', 'string'));

		// Filtering birth_method
		$this->setState('filter.birth_method', $app->getUserStateFromRequest($this->context.'.filter.birth_method', 'filter_birth_method', '', 'string'));

		// Filtering is_child_alive
		$this->setState('filter.is_child_alive', $app->getUserStateFromRequest($this->context.'.filter.is_child_alive', 'filter_is_child_alive', '', 'string'));

		// Filtering child_gender
		$this->setState('filter.child_gender', $app->getUserStateFromRequest($this->context.'.filter.child_gender', 'filter_child_gender', '', 'string'));

		// Filtering create_by
		$this->setState('filter.create_by', $app->getUserStateFromRequest($this->context.'.filter.create_by', 'filter_create_by', '', 'string'));


		// Load the parameters.
		$params = JComponentHelper::getParams('com_edairy');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.cow_id', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select', 'DISTINCT a.*'
			)
		);
		$query->from('`#__ed_cow_birth_inspection` AS a');

		// Join over the users for the checked out user
		$query->select("uc.name AS uEditor");
		$query->join("LEFT", "#__users AS uc ON uc.id=a.checked_out");
		// Join over the foreign key 'cow_id'
		$query->select('CONCAT(`#__ed_cow_2754535`.`cow_id`, \' \', `#__ed_cow_2754535`.`name`) AS cows_fk_value_2754535');
		$query->join('LEFT', '#__ed_cow AS #__ed_cow_2754535 ON #__ed_cow_2754535.`id` = a.`cow_id`');
		// Join over the foreign key 'child_cow_id'
		$query->select('CONCAT(`#__ed_cow_2754542`.`cow_id`, \' \', `#__ed_cow_2754542`.`name`) AS cows_fk_value_2754542');
		$query->join('LEFT', '#__ed_cow AS #__ed_cow_2754542 ON #__ed_cow_2754542.`id` = a.`child_cow_id`');

		// Join over the user field 'create_by'
		$query->select('`create_by`.name AS `create_by`');
		$query->join('LEFT', '#__users AS `create_by` ON `create_by`.id = a.`create_by`');

		// Join over the user field 'created_by'
		$query->select('`created_by`.name AS `created_by`');
		$query->join('LEFT', '#__users AS `created_by` ON `created_by`.id = a.`created_by`');

		// Filter by published state
		$published = $this->getState('filter.state');

		if (is_numeric($published))
		{
			$query->where('a.state = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.state IN (0, 1))');
		}

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				$query->where('(CONCAT(`#__ed_cow_2754535`.`cow_id`, \' \', `#__ed_cow_2754535`.`name`) LIKE ' . $search . '  OR  a.birth_date LIKE ' . $search . '  OR  a.child_weight LIKE ' . $search . '  OR  a.create_by LIKE ' . $search . ' )');
			}
		}


		//Filtering cow_id
		$filter_cow_id = $this->state->get("filter.cow_id");
		if ($filter_cow_id !== null && !empty($filter_cow_id))
		{
			$query->where("a.`cow_id` = '".$db->escape($filter_cow_id)."'");
		}

		//Filtering birth_date
		$filter_birth_date_from = $this->state->get("filter.birth_date.from");

		if ($filter_birth_date_from !== null && !empty($filter_birth_date_from))
		{
			$query->where("a.`birth_date` >= '".$db->escape($filter_birth_date_from)."'");
		}
		$filter_birth_date_to = $this->state->get("filter.birth_date.to");

		if ($filter_birth_date_to !== null  && !empty($filter_birth_date_to))
		{
			$query->where("a.`birth_date` <= '".$db->escape($filter_birth_date_to)."'");
		}

		//Filtering birth_method
		$filter_birth_method = $this->state->get("filter.birth_method");
		if ($filter_birth_method !== null && !empty($filter_birth_method))
		{
			$query->where("a.`birth_method` = '".$db->escape($filter_birth_method)."'");
		}

		//Filtering is_child_alive
		$filter_is_child_alive = $this->state->get("filter.is_child_alive");
		if ($filter_is_child_alive !== null && !empty($filter_is_child_alive))
		{
			$query->where("a.`is_child_alive` = '".$db->escape($filter_is_child_alive)."'");
		}

		//Filtering child_gender
		$filter_child_gender = $this->state->get("filter.child_gender");
		if ($filter_child_gender !== null && !empty($filter_child_gender))
		{
			$query->where("a.`child_gender` = '".$db->escape($filter_child_gender)."'");
		}

		//Filtering create_by
		$filter_create_by = $this->state->get("filter.create_by");
		if ($filter_create_by !== null && !empty($filter_create_by))
		{
			$query->where("a.`create_by` = '".$db->escape($filter_create_by)."'");
		}


		//Custom Filter
		if ($_REQUEST["search_coop"]!="")
		{

			$query->where("a.cow_pregnant_inspection_id in (select id from #__ed_cow_pregnant_inspection where cow_id in (select id from #__ed_cow where  farm_id in (select id from #__ed_farm f where f.coop_id in (select id from #__ed_coop c where c.name like '%{$_REQUEST[search_coop]}%' or c.coop_code like '%{$_REQUEST[search_coop]}%' )  )))");
		}

		if ($_REQUEST["search_member_code"]!="")
		{
			$query->where("a.cow_pregnant_inspection_id in (select id from #__ed_cow_pregnant_inspection where cow_id in (select id from #__ed_cow where  farm_id in (select id from #__ed_farm f where f.member_code like '%{$_REQUEST[search_member_code]}%' )))");
		}


		if ($_REQUEST["search_cow_id"]!="")
		{
			$query->where("a.cow_pregnant_inspection_id in (select id from #__ed_cow_pregnant_inspection where cow_id in (select id from #__ed_cow c where c.cow_id like '%{$_REQUEST[search_cow_id]}%'))");

		}

		if ($_REQUEST["search_cow_name"]!="")
		{
			$query->where("a.cow_pregnant_inspection_id in (select id from #__ed_cow_pregnant_inspection where cow_id in (select id from #__ed_cow c where c.name like '%{$_REQUEST[search_cow_name]}%'))");

			
		}

		if ($_REQUEST["search_farmer_name"]!="")
		{
			$query->where("a.cow_pregnant_inspection_id in (select id from #__ed_cow_pregnant_inspection where cow_id in (select id from #__ed_cow where  farm_id in (select farm_id from #__ed_farmer f where f.name like '%{$_REQUEST[search_farmer_name]}%')))");

		}

		if ($_REQUEST["search_farmer_surname"]!="")
		{
			$query->where("a.cow_pregnant_inspection_id in (select id from #__ed_cow_pregnant_inspection where cow_id in (select id from #__ed_cow where  farm_id in (select farm_id from #__ed_farmer f where f.surname like '%{$_REQUEST[search_farmer_surname]}%')))");
		}

		if ($_REQUEST["search_record_status"]!="")
		{
			if($_REQUEST["search_record_status"]==1){
				$query->where("a.actual_date = '0000-00-00'");
			}else{
				$query->where("a.actual_date <> '0000-00-00'");
			}
		}



		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');

		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();

		foreach ($items as $oneItem) {

			if (isset($oneItem->cow_id))
			{
				$values = explode(',', $oneItem->cow_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('CONCAT(`#__ed_cow_2754535`.`cow_id`, \' \', `#__ed_cow_2754535`.`name`) AS `fk_value`')
							->from($db->quoteName('#__ed_cow', '#__ed_cow_2754535'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->fk_value;
					}
				}

			$oneItem->cow_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->cow_id;

			}
					$oneItem->birth_method = ($oneItem->birth_method == '') ? '' : JText::_('COM_EDAIRY_BIRTH_INSPECTIONS_BIRTH_METHOD_OPTION_' . strtoupper($oneItem->birth_method));
					$oneItem->is_child_alive = ($oneItem->is_child_alive == '') ? '' : JText::_('COM_EDAIRY_BIRTH_INSPECTIONS_IS_CHILD_ALIVE_OPTION_' . strtoupper($oneItem->is_child_alive));
					$oneItem->child_gender = ($oneItem->child_gender == '') ? '' : JText::_('COM_EDAIRY_BIRTH_INSPECTIONS_CHILD_GENDER_OPTION_' . strtoupper($oneItem->child_gender));

			if (isset($oneItem->child_cow_id))
			{
				$values = explode(',', $oneItem->child_cow_id);

				$textValue = array();
				foreach ($values as $value){
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query
							->select('CONCAT(`#__ed_cow_2754542`.`cow_id`, \' \', `#__ed_cow_2754542`.`name`) AS `fk_value`')
							->from($db->quoteName('#__ed_cow', '#__ed_cow_2754542'))
							->where($db->quoteName('id') . ' = '. $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();
					if ($results) {
						$textValue[] = $results->fk_value;
					}
				}

			$oneItem->child_cow_id = !empty($textValue) ? implode(', ', $textValue) : $oneItem->child_cow_id;

			}
		}
		return $items;
	}
}
