<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Edairy model.
 *
 * @since  1.6
 */
class EdairyModelMilk_quality_parameter extends JModelAdmin
{
	/**
	 * @var      string    The prefix to use with controller messages.
	 * @since    1.6
	 */
	protected $text_prefix = 'COM_EDAIRY';

	/**
	 * @var   	string  	Alias to manage history control
	 * @since   3.2
	 */
	public $typeAlias = 'com_edairy.milk_quality_parameter';

	/**
	 * @var null  Item data
	 * @since  1.6
	 */
	protected $item = null;

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param   string  $type    The table type to instantiate
	 * @param   string  $prefix  A prefix for the table class name. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return    JTable    A database object
	 *
	 * @since    1.6
	 */
	public function getTable($type = 'Milk_quality_parameter', $prefix = 'EdairyTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      An optional array of data for the form to interogate.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  JForm  A JForm object on success, false on failure
	 *
	 * @since    1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Initialise variables.
		$app = JFactory::getApplication();

		// Get the form.
		$form = $this->loadForm(
			'com_edairy.milk_quality_parameter', 'milk_quality_parameter',
			array('control' => 'jform',
				'load_data' => $loadData
			)
		);

		if (empty($form))
		{
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return   mixed  The data for the form.
	 *
	 * @since    1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_edairy.edit.milk_quality_parameter.data', array());

		if (empty($data))
		{
			if ($this->item === null)
			{
				$this->item = $this->getItem();
			}

			$data = $this->item;
		}

		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param   integer  $pk  The id of the primary key.
	 *
	 * @return  mixed    Object on success, false on failure.
	 *
	 * @since    1.6
	 */
	public function getItem($pk = null)
	{
		if ($item = parent::getItem($pk))
		{
			// Do any procesing on fields here if needed
			$db = JFactory::getDbo();
				$query = $db->getQuery(true);
				$query
					->select("*")
					->from('#__ed_milk_quality_parameter_config')
					->where("milk_quality_parameter_id='{$item->id}'")
					->order("id asc");
				$db->setQuery($query);
				$result = $db->loadObject();
				$item->parameter_config = $result;

		}

		return $item;
	}

	/**
	 * Method to duplicate an Milk_quality_parameter
	 *
	 * @param   array  &$pks  An array of primary key IDs.
	 *
	 * @return  boolean  True if successful.
	 *
	 * @throws  Exception
	 */
	public function duplicate(&$pks)
	{
		$user = JFactory::getUser();

		// Access checks.
		if (!$user->authorise('core.create', 'com_edairy'))
		{
			throw new Exception(JText::_('JERROR_CORE_CREATE_NOT_PERMITTED'));
		}

		$dispatcher = JEventDispatcher::getInstance();
		$context    = $this->option . '.' . $this->name;

		// Include the plugins for the save events.
		JPluginHelper::importPlugin($this->events_map['save']);

		$table = $this->getTable();

		foreach ($pks as $pk)
		{
			if ($table->load($pk, true))
			{
				// Reset the id to create a new record.
				$table->id = 0;

				if (!$table->check())
				{
					throw new Exception($table->getError());
				}
				

				// Trigger the before save event.
				$result = $dispatcher->trigger($this->event_before_save, array($context, &$table, true));

				if (in_array(false, $result, true) || !$table->store())
				{
					throw new Exception($table->getError());
				}

				// Trigger the after save event.
				$dispatcher->trigger($this->event_after_save, array($context, &$table, true));
			}
			else
			{
				throw new Exception($table->getError());
			}
		}

		// Clean cache
		$this->cleanCache();

		return true;
	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @param   JTable  $table  Table Object
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function prepareTable($table)
	{
		jimport('joomla.filter.output');

		$table->create_date = formatSQLDate($table->create_date);

		if (empty($table->id))
		{
			// Set ordering to the last item if not set
			if (@$table->ordering === '')
			{
				$db = JFactory::getDbo();
				$db->setQuery('SELECT MAX(ordering) FROM #__ed_milk_quality_parameter');
				$max             = $db->loadResult();
				$table->ordering = $max + 1;
			}
		}
	}



	public function saveQualityParameterConfig($saved_item){

		
		$db = JFactory::getDbo();
			$query = $db->getQuery(true);

		 
		 
		$query->delete($db->quoteName('#__ed_milk_quality_parameter_config'));
		$query->where("milk_quality_parameter_id='{$saved_item->id}'");
		 
		$db->setQuery($query);
		$result = $db->execute();

				
			//Adapter
				
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);

				$data = new StdClass();
				$data->milk_quality_parameter_id = $saved_item->id;
				
				$data->mb_range1_low = $_REQUEST["mb_range1_low"];
				$data->mb_range1_high = $_REQUEST["mb_range1_high"];
				$data->mb_range1_change = $_REQUEST["mb_range1_change"];
				$data->mb_range2_low = $_REQUEST["mb_range2_low"];
				$data->mb_range2_high = $_REQUEST["mb_range2_high"];
				$data->mb_range2_change = $_REQUEST["mb_range2_change"];
				$data->mb_range3_low = $_REQUEST["mb_range3_low"];
				$data->mb_range3_high = $_REQUEST["mb_range3_high"];
				$data->mb_range3_change = $_REQUEST["mb_range3_change"];
				$data->mb_range4_low = $_REQUEST["mb_range4_low"];
				$data->mb_range4_high = $_REQUEST["mb_range4_high"];
				$data->mb_range4_change = $_REQUEST["mb_range4_change"];
				$data->mb_range5_low = $_REQUEST["mb_range5_low"];
				$data->mb_range5_high = $_REQUEST["mb_range5_high"];
				$data->mb_range5_change = $_REQUEST["mb_range5_change"];
				$data->mb_range6_low = $_REQUEST["mb_range6_low"];
				$data->mb_range6_high = $_REQUEST["mb_range6_high"];
				$data->mb_range6_change = $_REQUEST["mb_range6_change"];
				$data->mb_range7_low = $_REQUEST["mb_range7_low"];
				$data->mb_range7_high = $_REQUEST["mb_range7_high"];
				$data->mb_range7_change = $_REQUEST["mb_range7_change"];
				$data->mb_range8_low = $_REQUEST["mb_range8_low"];
				$data->mb_range8_high = $_REQUEST["mb_range8_high"];
				$data->mb_range8_change = $_REQUEST["mb_range8_change"];
				$data->mb_range9_low = $_REQUEST["mb_range9_low"];
				$data->mb_range9_high = $_REQUEST["mb_range9_high"];
				$data->mb_range9_change = $_REQUEST["mb_range9_change"];
				$data->mb_range10_low = $_REQUEST["mb_range10_low"];
				$data->mb_range10_high = $_REQUEST["mb_range10_high"];
				$data->mb_range10_change = $_REQUEST["mb_range10_change"];

				$data->scc_range1_low = $_REQUEST["scc_range1_low"];
				$data->scc_range1_high = $_REQUEST["scc_range1_high"];
				$data->scc_range1_change = $_REQUEST["scc_range1_change"];
				$data->scc_range2_low = $_REQUEST["scc_range2_low"];
				$data->scc_range2_high = $_REQUEST["scc_range2_high"];
				$data->scc_range2_change = $_REQUEST["scc_range2_change"];
				$data->scc_range3_low = $_REQUEST["scc_range3_low"];
				$data->scc_range3_high = $_REQUEST["scc_range3_high"];
				$data->scc_range3_change = $_REQUEST["scc_range3_change"];
				$data->scc_range4_low = $_REQUEST["scc_range4_low"];
				$data->scc_range4_high = $_REQUEST["scc_range4_high"];
				$data->scc_range4_change = $_REQUEST["scc_range4_change"];
				$data->scc_range5_low = $_REQUEST["scc_range5_low"];
				$data->scc_range5_high = $_REQUEST["scc_range5_high"];
				$data->scc_range5_change = $_REQUEST["scc_range5_change"];
				$data->scc_range6_low = $_REQUEST["scc_range6_low"];
				$data->scc_range6_high = $_REQUEST["scc_range6_high"];
				$data->scc_range6_change = $_REQUEST["scc_range6_change"];
				$data->scc_range7_low = $_REQUEST["scc_range7_low"];
				$data->scc_range7_high = $_REQUEST["scc_range7_high"];
				$data->scc_range7_change = $_REQUEST["scc_range7_change"];
				$data->scc_range8_low = $_REQUEST["scc_range8_low"];
				$data->scc_range8_high = $_REQUEST["scc_range8_high"];
				$data->scc_range8_change = $_REQUEST["scc_range8_change"];
				$data->scc_range9_low = $_REQUEST["scc_range9_low"];
				$data->scc_range9_high = $_REQUEST["scc_range9_high"];
				$data->scc_range9_change = $_REQUEST["scc_range9_change"];
				$data->scc_range10_low = $_REQUEST["scc_range10_low"];
				$data->scc_range10_high = $_REQUEST["scc_range10_high"];
				$data->scc_range10_change = $_REQUEST["scc_range10_change"];

				$data->fat_range1_low = $_REQUEST["fat_range1_low"];
				$data->fat_range1_high = $_REQUEST["fat_range1_high"];
				$data->fat_range1_change = $_REQUEST["fat_range1_change"];
				$data->fat_range2_low = $_REQUEST["fat_range2_low"];
				$data->fat_range2_high = $_REQUEST["fat_range2_high"];
				$data->fat_range2_change = $_REQUEST["fat_range2_change"];
				$data->fat_range3_low = $_REQUEST["fat_range3_low"];
				$data->fat_range3_high = $_REQUEST["fat_range3_high"];
				$data->fat_range3_change = $_REQUEST["fat_range3_change"];
				$data->fat_range4_low = $_REQUEST["fat_range4_low"];
				$data->fat_range4_high = $_REQUEST["fat_range4_high"];
				$data->fat_range4_change = $_REQUEST["fat_range4_change"];
				$data->fat_range5_low = $_REQUEST["fat_range5_low"];
				$data->fat_range5_high = $_REQUEST["fat_range5_high"];
				$data->fat_range5_change = $_REQUEST["fat_range5_change"];
				$data->fat_range6_low = $_REQUEST["fat_range6_low"];
				$data->fat_range6_high = $_REQUEST["fat_range6_high"];
				$data->fat_range6_change = $_REQUEST["fat_range6_change"];
				$data->fat_range7_low = $_REQUEST["fat_range7_low"];
				$data->fat_range7_high = $_REQUEST["fat_range7_high"];
				$data->fat_range7_change = $_REQUEST["fat_range7_change"];
				$data->fat_range8_low = $_REQUEST["fat_range8_low"];
				$data->fat_range8_high = $_REQUEST["fat_range8_high"];
				$data->fat_range8_change = $_REQUEST["fat_range8_change"];
				$data->fat_range9_low = $_REQUEST["fat_range9_low"];
				$data->fat_range9_high = $_REQUEST["fat_range9_high"];
				$data->fat_range9_change = $_REQUEST["fat_range9_change"];
				$data->fat_range10_low = $_REQUEST["fat_range10_low"];
				$data->fat_range10_high = $_REQUEST["fat_range10_high"];
				$data->fat_range10_change = $_REQUEST["fat_range10_change"];

				$data->snf_range1_low = $_REQUEST["snf_range1_low"];
				$data->snf_range1_high = $_REQUEST["snf_range1_high"];
				$data->snf_range1_change = $_REQUEST["snf_range1_change"];
				$data->snf_range2_low = $_REQUEST["snf_range2_low"];
				$data->snf_range2_high = $_REQUEST["snf_range2_high"];
				$data->snf_range2_change = $_REQUEST["snf_range2_change"];
				$data->snf_range3_low = $_REQUEST["snf_range3_low"];
				$data->snf_range3_high = $_REQUEST["snf_range3_high"];
				$data->snf_range3_change = $_REQUEST["snf_range3_change"];
				$data->snf_range4_low = $_REQUEST["snf_range4_low"];
				$data->snf_range4_high = $_REQUEST["snf_range4_high"];
				$data->snf_range4_change = $_REQUEST["snf_range4_change"];
				$data->snf_range5_low = $_REQUEST["snf_range5_low"];
				$data->snf_range5_high = $_REQUEST["snf_range5_high"];
				$data->snf_range5_change = $_REQUEST["snf_range5_change"];
				$data->snf_range6_low = $_REQUEST["snf_range6_low"];
				$data->snf_range6_high = $_REQUEST["snf_range6_high"];
				$data->snf_range6_change = $_REQUEST["snf_range6_change"];
				$data->snf_range7_low = $_REQUEST["snf_range7_low"];
				$data->snf_range7_high = $_REQUEST["snf_range7_high"];
				$data->snf_range7_change = $_REQUEST["snf_range7_change"];
				$data->snf_range8_low = $_REQUEST["snf_range8_low"];
				$data->snf_range8_high = $_REQUEST["snf_range8_high"];
				$data->snf_range8_change = $_REQUEST["snf_range8_change"];
				$data->snf_range9_low = $_REQUEST["snf_range9_low"];
				$data->snf_range9_high = $_REQUEST["snf_range9_high"];
				$data->snf_range9_change = $_REQUEST["snf_range9_change"];
				$data->snf_range10_low = $_REQUEST["snf_range10_low"];
				$data->snf_range10_high = $_REQUEST["snf_range10_high"];
				$data->snf_range10_change = $_REQUEST["snf_range10_change"];

				$data->ts_range1_low = $_REQUEST["ts_range1_low"];
				$data->ts_range1_high = $_REQUEST["ts_range1_high"];
				$data->ts_range1_change = $_REQUEST["ts_range1_change"];
				$data->ts_range2_low = $_REQUEST["ts_range2_low"];
				$data->ts_range2_high = $_REQUEST["ts_range2_high"];
				$data->ts_range2_change = $_REQUEST["ts_range2_change"];
				$data->ts_range3_low = $_REQUEST["ts_range3_low"];
				$data->ts_range3_high = $_REQUEST["ts_range3_high"];
				$data->ts_range3_change = $_REQUEST["ts_range3_change"];
				$data->ts_range4_low = $_REQUEST["ts_range4_low"];
				$data->ts_range4_high = $_REQUEST["ts_range4_high"];
				$data->ts_range4_change = $_REQUEST["ts_range4_change"];
				$data->ts_range5_low = $_REQUEST["ts_range5_low"];
				$data->ts_range5_high = $_REQUEST["ts_range5_high"];
				$data->ts_range5_change = $_REQUEST["ts_range5_change"];
				$data->ts_range6_low = $_REQUEST["ts_range6_low"];
				$data->ts_range6_high = $_REQUEST["ts_range6_high"];
				$data->ts_range6_change = $_REQUEST["ts_range6_change"];
				$data->ts_range7_low = $_REQUEST["ts_range7_low"];
				$data->ts_range7_high = $_REQUEST["ts_range7_high"];
				$data->ts_range7_change = $_REQUEST["ts_range7_change"];
				$data->ts_range8_low = $_REQUEST["ts_range8_low"];
				$data->ts_range8_high = $_REQUEST["ts_range8_high"];
				$data->ts_range8_change = $_REQUEST["ts_range8_change"];
				$data->ts_range9_low = $_REQUEST["ts_range9_low"];
				$data->ts_range9_high = $_REQUEST["ts_range9_high"];
				$data->ts_range9_change = $_REQUEST["ts_range9_change"];
				$data->ts_range10_low = $_REQUEST["ts_range10_low"];
				$data->ts_range10_high = $_REQUEST["ts_range10_high"];
				$data->ts_range10_change = $_REQUEST["ts_range10_change"];

				$data->protein_range1_low = $_REQUEST["protein_range1_low"];
				$data->protein_range1_high = $_REQUEST["protein_range1_high"];
				$data->protein_range1_change = $_REQUEST["protein_range1_change"];
				$data->protein_range2_low = $_REQUEST["protein_range2_low"];
				$data->protein_range2_high = $_REQUEST["protein_range2_high"];
				$data->protein_range2_change = $_REQUEST["protein_range2_change"];
				$data->protein_range3_low = $_REQUEST["protein_range3_low"];
				$data->protein_range3_high = $_REQUEST["protein_range3_high"];
				$data->protein_range3_change = $_REQUEST["protein_range3_change"];
				$data->protein_range4_low = $_REQUEST["protein_range4_low"];
				$data->protein_range4_high = $_REQUEST["protein_range4_high"];
				$data->protein_range4_change = $_REQUEST["protein_range4_change"];
				$data->protein_range5_low = $_REQUEST["protein_range5_low"];
				$data->protein_range5_high = $_REQUEST["protein_range5_high"];
				$data->protein_range5_change = $_REQUEST["protein_range5_change"];
				$data->protein_range6_low = $_REQUEST["protein_range6_low"];
				$data->protein_range6_high = $_REQUEST["protein_range6_high"];
				$data->protein_range6_change = $_REQUEST["protein_range6_change"];
				$data->protein_range7_low = $_REQUEST["protein_range7_low"];
				$data->protein_range7_high = $_REQUEST["protein_range7_high"];
				$data->protein_range7_change = $_REQUEST["protein_range7_change"];
				$data->protein_range8_low = $_REQUEST["protein_range8_low"];
				$data->protein_range8_high = $_REQUEST["protein_range8_high"];
				$data->protein_range8_change = $_REQUEST["protein_range8_change"];
				$data->protein_range9_low = $_REQUEST["protein_range9_low"];
				$data->protein_range9_high = $_REQUEST["protein_range9_high"];
				$data->protein_range9_change = $_REQUEST["protein_range9_change"];
				$data->protein_range10_low = $_REQUEST["protein_range10_low"];
				$data->protein_range10_high = $_REQUEST["protein_range10_high"];
				$data->protein_range10_change = $_REQUEST["protein_range10_change"];

				$data->lactose_range1_low = $_REQUEST["lactose_range1_low"];
				$data->lactose_range1_high = $_REQUEST["lactose_range1_high"];
				$data->lactose_range1_change = $_REQUEST["lactose_range1_change"];
				$data->lactose_range2_low = $_REQUEST["lactose_range2_low"];
				$data->lactose_range2_high = $_REQUEST["lactose_range2_high"];
				$data->lactose_range2_change = $_REQUEST["lactose_range2_change"];
				$data->lactose_range3_low = $_REQUEST["lactose_range3_low"];
				$data->lactose_range3_high = $_REQUEST["lactose_range3_high"];
				$data->lactose_range3_change = $_REQUEST["lactose_range3_change"];
				$data->lactose_range4_low = $_REQUEST["lactose_range4_low"];
				$data->lactose_range4_high = $_REQUEST["lactose_range4_high"];
				$data->lactose_range4_change = $_REQUEST["lactose_range4_change"];
				$data->lactose_range5_low = $_REQUEST["lactose_range5_low"];
				$data->lactose_range5_high = $_REQUEST["lactose_range5_high"];
				$data->lactose_range5_change = $_REQUEST["lactose_range5_change"];
				$data->lactose_range6_low = $_REQUEST["lactose_range6_low"];
				$data->lactose_range6_high = $_REQUEST["lactose_range6_high"];
				$data->lactose_range6_change = $_REQUEST["lactose_range6_change"];
				$data->lactose_range7_low = $_REQUEST["lactose_range7_low"];
				$data->lactose_range7_high = $_REQUEST["lactose_range7_high"];
				$data->lactose_range7_change = $_REQUEST["lactose_range7_change"];
				$data->lactose_range8_low = $_REQUEST["lactose_range8_low"];
				$data->lactose_range8_high = $_REQUEST["lactose_range8_high"];
				$data->lactose_range8_change = $_REQUEST["lactose_range8_change"];
				$data->lactose_range9_low = $_REQUEST["lactose_range9_low"];
				$data->lactose_range9_high = $_REQUEST["lactose_range9_high"];
				$data->lactose_range9_change = $_REQUEST["lactose_range9_change"];
				$data->lactose_range10_low = $_REQUEST["lactose_range10_low"];
				$data->lactose_range10_high = $_REQUEST["lactose_range10_high"];
				$data->lactose_range10_change = $_REQUEST["lactose_range10_change"];

				$data->state=1;
				$data->ordering=0;
				$data->checked_out = 0;
				$data->checked_out_time = "0000-00-00 00:00:00";

				//FK
				//$data->cow_insemination_id = $saved_item->id;



				$user = JFactory::getUser();

				$data->created_by = $user->id;

			
				$addDataResult = JFactory::getDbo()->insertObject('#__ed_milk_quality_parameter_config', $data, 'id');


				



			
	}
}
