<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Class EdairyController
 *
 * @since  1.6
 */
class EdairyController extends JControllerLegacy
{
	/**
	 * Method to display a view.
	 *
	 * @param   boolean  $cachable   If true, the view output will be cached
	 * @param   mixed    $urlparams  An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return   JController This object to support chaining.
	 *
	 * @since    1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{

			$user = JFactory::getUser();

		$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
				->select("*")
				->from('#__ed_user_role');
			$db->setQuery($query);
			$role_table = $db->loadObjectList();

			$is_show = false;
			$is_at_least_one = false;

			foreach ($role_table as $key => $value) {
				if($value->view_name==$_REQUEST["view"]){
					$is_at_least_one = true;
					$role_row = $value->{"role" . $user->role};
					$role_array = explode(",", $role_row);
					if(in_array("R", $role_array)){
						$is_show = true;
						break;
					}
				}
			}
			if(!$is_show){
				if($is_at_least_one){
					$view="farms";
				}else{
					$view = JFactory::getApplication()->input->getCmd('view', 'coops');
				}
			}else{
				$view = JFactory::getApplication()->input->getCmd('view', 'coops');
			}
		JFactory::getApplication()->input->set('view', $view);

		parent::display($cachable, $urlparams);

		return $this;
	}
}
