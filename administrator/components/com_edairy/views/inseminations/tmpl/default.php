<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'administrator/components/com_edairy/assets/css/edairy.css');
$document->addStyleSheet(JUri::root() . 'media/com_edairy/css/list.css');

$user      = JFactory::getUser();
$userId    = $user->get('id');
$listOrder = $this->state->get('list.ordering');
$listDirn  = $this->state->get('list.direction');
$canOrder  = $user->authorise('core.edit.state', 'com_edairy');
$saveOrder = $listOrder == 'a.`ordering`';

if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?option=com_edairy&task=inseminations.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'inseminationList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}

$sortFields = $this->getSortFields();
?>

<style type="text/css">
	.js-stools{
		display: none;
	}
</style>

<script src="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript" charset="UTF-8"></script>

<link rel="stylesheet" href="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.css">
<script type="text/javascript">
	jQuery(document).ready(function () {
		jQuery('#clear-search-button').on('click', function () {
			jQuery('select[name=search_region]').val('');
			jQuery('select[name=search_province]').val('');
			jQuery('select[name=search_region]').val('');
			jQuery('select').trigger('liszt:updated');
			jQuery('.well input').val('');
			jQuery('#adminForm').submit();
		});
	});

	js = jQuery.noConflict();
	js(document).ready(function () {
		js('.datetimepicker').datetimepicker({
            lang:'th',
            timepicker:false,
            format:'d/m/Y'
        });
     });
</script>
<form action="<?php echo JRoute::_('index.php?option=com_edairy&view=inseminations'); ?>" method="post"
	  name="adminForm" id="adminForm">
	<?php if (!empty($this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
		<?php else : ?>
		<div id="j-main-container">
			<?php endif; ?>

			<h4>ค้นหาบริการผสมเทียม</h4>
			<div id="filter-bar" class="btn-toolbar well" style="min-height:100px">

				<div class="filter-search btn-group pull-left">
					
					<label style="display:inline;">
						&nbsp;เลขที่ใบบริการผสมเทียม 
					</label>
					<input type="text" name="search_run_no" value="<?php echo $_REQUEST["search_run_no"]; ?>" />	
					 



					<label style="display:inline;">
						&nbsp;รหัสศูนย์/ชื่อสหกรณ์ 
					</label>
					 <input type="text" name="search_coop" value="<?php echo $_REQUEST["search_coop"]; ?>" />	
					 

					

					<label style="display:inline;">
						&nbsp;หมายเลขสมาชิก/เบอร์ถัง
					</label>
					<input type="text" name="search_member_code" value="<?php echo $_REQUEST["search_member_code"]; ?>" />	
					 



					

				</div>

					<div style="clear:both">
					</div>

					
					<label style="display:inline;">
						&nbsp;ชื่อ
					</label>


					 <input type="text" name="search_farmer_name" value="<?php echo $_REQUEST["search_farmer_name"]; ?>"/>


					 <label style="display:inline;">
						&nbsp;นามสกุล
					</label>


					 <input type="text" name="search_farmer_surname" value="<?php echo $_REQUEST["search_farmer_surname"]; ?>"/>
				
					 
					 <label style="display:inline;">
						&nbsp;ระหว่างวันที่
					</label>


					 <input type="text" name="search_date_from" class="datetimepicker" value="<?php echo $_REQUEST['search_date_from']; ?>"/>

					 <label style="display:inline;">
						&nbsp;ถึงวันที่
					</label>
					 
					 <input type="text" name="search_date_to" class="datetimepicker" value="<?php echo $_REQUEST['search_date_to']; ?>"/>

					<div style="clear:both">
					</div>



					<div class="btn-group pull-left">
					<button class="btn hasTooltip" type="submit"
							title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>">
						<i class="icon-search"></i></button>
					<button class="btn hasTooltip" id="clear-search-button" type="button"
							title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>">
						<i class="icon-remove"></i></button>
				</div>
				<div class="btn-group pull-right hidden-phone">
					<label for="limit"
						   class="element-invisible">
						<?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?>
					</label>
					<?php// echo $this->pagination->getLimitBox(); ?>
				</div>
				</div>

            <?php  echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this)); ?>

			<div class="clearfix"></div>

			<b>พบทั้งสิ้น <?php echo $this->pagination->total; ?> รายการ</b>
			<table class="table table-striped" id="inseminationList">
				<thead>
				<tr>
					<?php if (isset($this->items[0]->ordering)): ?>
						<th width="1%" class="nowrap center hidden-phone" style="display: none">
                            <?php echo JHtml::_('searchtools.sort', '', 'a.ordering', $listDirn, $listOrder, null, 'asc', 'JGRID_HEADING_ORDERING', 'icon-menu-2'); ?>
                        </th>
					<?php endif; ?>
					<th width="1%" class="hidden-phone" style="display: none">
						<input type="checkbox" name="checkall-toggle" value=""
							   title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)"/>
					</th>
					<?php if (isset($this->items[0]->state)): ?>
						<th width="1%" class="nowrap center">
								<?php echo JHtml::_('searchtools.sort', 'JSTATUS', 'a.`state`', $listDirn, $listOrder); ?>
</th>
					<?php endif; ?>

									<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'id', 'a.`id`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_INSEMINATIONS_RUN_NO', 'a.`run_no`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_INSEMINATIONS_CREATE_DATE', 'a.`create_date`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_INSEMINATIONS_PAYMENT_TYPE', 'a.`payment_type`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_INSEMINATIONS_GRAND_TOTAL', 'a.`grand_total`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_INSEMINATIONS_FARM_ID', 'a.`farm_id`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_INSEMINATIONS_FARMER_ID', 'a.`farmer_id`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_INSEMINATIONS_COOP_ID', 'a.`coop_id`', $listDirn, $listOrder); ?>
				</th>
				<th>
					ใบแจ้งหนี้/ ใบเสร็จ
				</th>
				<th width="3%">
					แก้ไข
				</th>
				<th width="3%">
					ลบ
				</th>	
					
				</tr>
				</thead>
				<tfoot>
				<tr>
					<td colspan="<?php echo isset($this->items[0]) ? count(get_object_vars($this->items[0])) : 10; ?>">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
				</tfoot>
				<tbody>
				<?php $itemIndex=0; foreach ($this->items as $i => $item) :
					$ordering   = ($listOrder == 'a.ordering');
					$canCreate  = $user->authorise('core.create', 'com_edairy');
					$canEdit    = $user->authorise('core.edit', 'com_edairy');
					$canCheckin = $user->authorise('core.manage', 'com_edairy');
					$canChange  = $user->authorise('core.edit.state', 'com_edairy');
					?>
					<tr class="row<?php echo $i % 2; ?>">

						<?php if (isset($this->items[0]->ordering)) : ?>
							<td class="order nowrap center hidden-phone" style="display: none">
								<?php if ($canChange) :
									$disableClassName = '';
									$disabledLabel    = '';

									if (!$saveOrder) :
										$disabledLabel    = JText::_('JORDERINGDISABLED');
										$disableClassName = 'inactive tip-top';
									endif; ?>
									<span class="sortable-handler hasTooltip <?php echo $disableClassName ?>"
										  title="<?php echo $disabledLabel ?>">
							<i class="icon-menu"></i>
						</span>
									<input type="text" style="display:none" name="order[]" size="5"
										   value="<?php echo $item->ordering; ?>" class="width-20 text-area-order "/>
								<?php else : ?>
									<span class="sortable-handler inactive">
							<i class="icon-menu"></i>
						</span>
								<?php endif; ?>
							</td>
						<?php endif; ?>
						<td class="hidden-phone" style="display: none">
							<?php echo JHtml::_('grid.id', $i, $item->id); ?>
						</td>
						<?php if (isset($this->items[0]->state)): ?>
							<td class="center">
								<?php echo JHtml::_('jgrid.published', $item->state, $i, 'inseminations.', $canChange, 'cb'); ?>
</td>
						<?php endif; ?>

										<td>

					<?php echo $item->id; ?>
				</td>				<td>
				<?php if (isset($item->checked_out) && $item->checked_out && ($canEdit || $canChange)) : ?>
					<?php echo JHtml::_('jgrid.checkedout', $i, $item->uEditor, $item->checked_out_time, 'inseminations.', $canCheckin); ?>
				<?php endif; ?>
				<?php if (false)://($canEdit) : ?>
					<a href="<?php echo JRoute::_('index.php?option=com_edairy&task=insemination.edit&id='.(int) $item->id); ?>">
					<?php echo $this->escape($item->run_no); ?></a>
				<?php else : ?>
					<?php echo $this->escape($item->run_no); ?>
				<?php endif; ?>

				</td>				<td>

					<?php echo formatDate($item->create_date); ?>
				</td>				<td>

					<?php echo $item->payment_type; ?>
				</td>				<td>

					<?php echo $item->grand_total; ?>
				</td>				<td>

					<?php echo $item->farm_id; ?>
				</td>				<td>

					<?php echo $item->farmer_id; ?>
				</td>				<td>

					<?php echo $item->coop_id; ?>
				</td>
				<td>
					<?php if($item->payment_type=="เงินสด"){ ?>
						<a href="index.php?option=com_edairy&view=insemination&layout=pdf_receipt&id=<?php echo $item->id; ?>" class="btn" target="_blank"><div class="icon-print"></div> พิมพ์</a>
					<?php }else{ ?>
						<a href="index.php?option=com_edairy&view=insemination&layout=pdf_invoice&id=<?php echo $item->id; ?>" class="btn" target="_blank"><div class="icon-print"></div> พิมพ์</a>
					<?php } ?>
					
				</td>
				<td>
					<?php 
						$can_edit = true;
						$user = JFactory::getUser();
						if($user->role<7){
							if($item->created_by!=$user->id){
								$can_edit = false;
							}
						}
					?>
					<?php if($can_edit){ ?>
						<a href="<?php echo JRoute::_('index.php?option=com_edairy&task=insemination.edit&id='.(int) $item->id); ?>" class="btn btn-warning"> แก้ไข</a>
					<?php }else{ ?>
						<a href="#" class="btn btn-warning" disabled> แก้ไข</a>
					<?php } ?>
				</td>
				<td>
					<?php if($can_edit){ ?>
						<a href="#" class="btn btn-danger" onClick="javascript: if(confirm('ยืนยันการลบ ?')){jQuery('#cb<?php echo $itemIndex; ?>').attr('checked', 'checked'); Joomla.submitbutton('inseminations.trash');}"> ลบ</a>
					<?php }else{ ?>
						<a href="#" class="btn btn-danger" disabled> ลบ</a>
					<?php } ?>
				</td>

					</tr>
				<?php $itemIndex++; endforeach; ?>
				</tbody>
			</table>

			<input type="hidden" name="task" value=""/>
			<input type="hidden" name="boxchecked" value="0"/>
            <input type="hidden" name="list[fullorder]" value="<?php echo $listOrder; ?> <?php echo $listDirn; ?>"/>
			<?php echo JHtml::_('form.token'); ?>
		</div>
</form>
<script>
    window.toggleField = function (id, task, field) {

        var f = document.adminForm, i = 0, cbx, cb = f[ id ];

        if (!cb) return false;

        while (true) {
            cbx = f[ 'cb' + i ];

            if (!cbx) break;

            cbx.checked = false;
            i++;
        }

        var inputField   = document.createElement('input');

        inputField.type  = 'hidden';
        inputField.name  = 'field';
        inputField.value = field;
        f.appendChild(inputField);

        cb.checked = true;
        f.boxchecked.value = 1;
        window.submitform(task);

        return false;
    };
</script>