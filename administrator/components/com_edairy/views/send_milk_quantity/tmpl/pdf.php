<?php 

// Include the main TCPDF library (search for installation path).
require_once('../media/com_edairy/tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('EDAIRY');
$pdf->SetTitle('EDAIRY');
$pdf->SetSubject('EDAIRY');
$pdf->SetKeywords('EDAIRY');

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
// set font

//$fontname = $pdf->addTTFfont('fonts/Browa.ttf', 'TrueTypeUnicode', '', 32);
$pdf->SetFont('thsarabunpsk', '', 14, '', true);



//PAGE 3 >> PAGE 1
$pdf->AddPage();
$pdf->setPageOrientation ('P', $autopagebreak='', $bottommargin='');
// get the current page break margin
$bMargin = $pdf->getBreakMargin();
// get current auto-page-break mode
$auto_page_break = $pdf->getAutoPageBreak();
// disable auto-page-break
$pdf->SetAutoPageBreak(true, 0);


function num2thai($number){
	$t1 = array("ศูนย์", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");
	$t2 = array("เอ็ด", "ยี่", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน");
	$zerobahtshow = 0; // ในกรณีที่มีแต่จำนวนสตางค์ เช่น 0.25 หรือ .75 จะให้แสดงคำว่า ศูนย์บาท หรือไม่ 0 = ไม่แสดง, 1 = แสดง
	(string) $number;
	$number = explode(".", $number);
	if(!empty($number[1])){
		if(strlen($number[1]) == 1){
			$number[1] .= "0";
		}else if(strlen($number[1]) > 2){
			if($number[1]{2} < 5){
				$number[1] = substr($number[1], 0, 2);
			}else{
				$number[1] = $number[1]{0}.($number[1]{1}+1);
			}
		}
	}
	
	for($i=0; $i<count($number); $i++){
		$countnum[$i] = strlen($number[$i]);
		if($countnum[$i] <= 7){
			$var[$i][] = $number[$i];
		}else{
			$loopround = ceil($countnum[$i]/6);
			for($j=1; $j<=$loopround; $j++){
				if($j == 1){
					$slen = 0;
					$elen = $countnum[$i]-(($loopround-1)*6);
				}else{
					$slen = $countnum[$i]-((($loopround+1)-$j)*6);
					$elen = 6;
				}
				$var[$i][] = substr($number[$i], $slen, $elen);
			}
		}	

		$nstring[$i] = "";
		for($k=0; $k<count($var[$i]); $k++){
			if($k > 0) $nstring[$i] .= $t2[7];
			$val = $var[$i][$k];
			$tnstring = "";
			$countval = strlen($val);
			for($l=7; $l>=2; $l--){
				if($countval >= $l){
					$v = substr($val, -$l, 1);
					if($v > 0){
						if($l == 2 && $v == 1){
							$tnstring .= $t2[($l)];
						}elseif($l == 2 && $v == 2){
							$tnstring .= $t2[1].$t2[($l)];
						}else{
							$tnstring .= $t1[$v].$t2[($l)];
						}
					}
				}
			}
			if($countval >= 1){
				$v = substr($val, -1, 1);
				if($v > 0){
					if($v == 1 && $countval > 1 && substr($val, -2, 1) > 0){
						$tnstring .= $t2[0];
					}else{
						$tnstring .= $t1[$v];
					}

				}
			}

			$nstring[$i] .= $tnstring;
		}

	}
	$rstring = "";
	if(!empty($nstring[0]) || $zerobahtshow == 1 || empty($nstring[1])){
		if($nstring[0] == "") $nstring[0] = $t1[0];
		$rstring .= $nstring[0]."บาท";
	}
	if(count($number) == 1 || empty($nstring[1])){
		$rstring .= "ถ้วน";
	}else{
		$rstring .= $nstring[1]."สตางค์";
	}
	return $rstring;
}
// set bacground image
/*
$heading_html = <<<EOD
<img src="images/header.png" />

EOD;

$pdf->writeHTMLCell($w=0, $h=0, $x='10', $y='5', $heading_html, $border=0, $ln=1, $fill=0, $reseth=true, $align='L', $autopadding=false);

*/

$create_date = formatDate($this->item->create_date);


$this->item->create_time = substr($this->item->leave_time,0,5);

$html = <<<EOD
เลขที่ : {$this->item->run_no} <br />
วันที่ : {$create_date} <br />
EOD;


$money_word = num2thai($this->item->grand_total);

// print a block of text using Write()
$pdf->writeHTMLCell($w=0, $h=0, $x='10', $y='10', $html, $border=0, $ln=1, $fill=0, $reseth=true, $align='R', $autopadding=false);



if($this->item->receiver_id!=""){
	$receive_user = JFactory::getUser($this->item->receiver_id);
	$receive_user = $receive_user->name;
}else{
	$receive_user = "-";
}
if($this->item->remark==""){
	$this->item->remark = "-";
}

$html = <<<EOD
<div style="text-align:center;font-size:20px">
			<img src="../images/dpo_logo.jpg" width="70"/><br />
			<b>
				องค์การส่งเสริมกิจการโคนมแห่งประเทศไทย (อ.ส.ค.) กระทรวงเกษตรและสหกรณ์<br />
				ใบส่งน้ำนมดิบ
			</b>

</div>
<br />
<table>
	<tr>
		<td width="250">
			สหกรณ์/ศูนย์รับน้ำนม: {$this->item->coop}
		</td>
		<td>
			เวลาออกจากศูนย์: {$this->item->create_time} น.
		</td>
		<td>
			เวลาเข้า อ.ส.ค.: ...........................
		</td>
	</tr>
	<tr>
		<td width="250">
			ทะเบียนรถ: {$this->item->license_plate}
		</td>
		<td>
			พนักงานขับรถ: {$this->item->driver_name}
		</td>
		<td>
		</td>
	</tr>
</table>
<br /><br />
<table width="800" border="1" cellpadding="5">
	<tr>
		<th align="center" width="350">รายการ</th>
		<th align="center" width="150">หน่วย (ลิตร)</th>
		<th align="center" width="150">หน่วย (กก.)</th>
	</tr>
	<tr>
		<td>&nbsp;จากศูนย์รับน้ำนม</td>
		<td align="center">{$this->item->litre_amount}</td>
		<td align="center">{$this->item->kg_amount}</td>
	</tr>
	<tr>
		<td>&nbsp;ถึงศูนย์รวบรวมน้ำนมดิบ</td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>&nbsp;นมเสื่อมคุณภาพ</td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>&nbsp;ผลต่าง</td>
		<td></td>
		<td></td>
	</tr>
</table>

<br />
<br />
<b>การตรวจสอบคุณภาพ</b>
<br />
<table width="800" border="1" cellpadding="5">
	<tr>
		<th rowspan="2" style="text-align:center" width="190">รายการ</th>
		<th rowspan="2" style="text-align:center" width="90">ผลตรวจ ณ ศูนย์ฯ</th>
		<th colspan="4" style="text-align:center" width="280">ผลตรวจ ณ โรงงาน</th>
		<th rowspan="2" style="text-align:center" width="90">หมายเหตุ</th>
	</tr>
	<tr>
		<th style="text-align:center">ครั้งที่ 1</th>
		<th style="text-align:center">ครั้งที่ 2</th>
		<th style="text-align:center">ครั้งที่ 3</th>
		<th style="text-align:center">เฉลี่ย</th>
	</tr>
	<tr>
		<td>&nbsp;อุณหภูมิ (C)</td>
		<td align="center">{$this->item->temperature}</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>&nbsp;ความถ่วงจำเพาะ</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>&nbsp;%ไขมัน</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>&nbsp;การตรวจสอบแอลกอฮอล์</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>&nbsp;พี.เอช.</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>&nbsp;%ของแข็งไม่รวมไขมัน</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>&nbsp;%รีซาซูรีน</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
</table>

<br /><br /><br />
<table>
	<tr>
		<td width="425">
			ลงชื่อ................................................ผู้ส่งของ<br />
		</td>
		<td width="500">
			ลงชื่อ................................................ผู้รับ<br />
		</td>
	</tr>
	<tr>
		<td width="425">
			ลงชื่อ................................................ผู้ตรวจสอบ
		</td>
		<td width="500">
			ลงชื่อ................................................พนักงานขับรถ
		</td>
	</tr>
</table>

EOD;




// print a block of text using Write()
$pdf->writeHTMLCell($w=0, $h=0, $x='10', $y='10', $html, $border=0, $ln=1, $fill=0, $reseth=true, $align='L', $autopadding=false);

 






// set bacground image

// if (isset($_POST['download'])) {
// 	$pdf->Output("detail_id{$_POST[id]}.pdf", 'D');
// }else{
	$pdf->Output("Invoice_{$_GET[id]}.pdf", 'I');
// }
die();
