<?php 

// Include the main TCPDF library (search for installation path).
require_once('../media/com_edairy/tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('EDAIRY');
$pdf->SetTitle('EDAIRY');
$pdf->SetSubject('EDAIRY');
$pdf->SetKeywords('EDAIRY');

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
// set font

//$fontname = $pdf->addTTFfont('fonts/Browa.ttf', 'TrueTypeUnicode', '', 32);
$pdf->SetFont('thsarabunpsk', '', 14, '', true);



//PAGE 3 >> PAGE 1
$pdf->AddPage();
$pdf->setPageOrientation ('P', $autopagebreak='', $bottommargin='');
// get the current page break margin
$bMargin = $pdf->getBreakMargin();
// get current auto-page-break mode
$auto_page_break = $pdf->getAutoPageBreak();
// disable auto-page-break
$pdf->SetAutoPageBreak(true, 40);


function num2thai($number){
	$t1 = array("ศูนย์", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");
	$t2 = array("เอ็ด", "ยี่", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน");
	$zerobahtshow = 0; // ในกรณีที่มีแต่จำนวนสตางค์ เช่น 0.25 หรือ .75 จะให้แสดงคำว่า ศูนย์บาท หรือไม่ 0 = ไม่แสดง, 1 = แสดง
	(string) $number;
	$number = explode(".", $number);
	if(!empty($number[1])){
		if(strlen($number[1]) == 1){
			$number[1] .= "0";
		}else if(strlen($number[1]) > 2){
			if($number[1]{2} < 5){
				$number[1] = substr($number[1], 0, 2);
			}else{
				$number[1] = $number[1]{0}.($number[1]{1}+1);
			}
		}
	}
	
	for($i=0; $i<count($number); $i++){
		$countnum[$i] = strlen($number[$i]);
		if($countnum[$i] <= 7){
			$var[$i][] = $number[$i];
		}else{
			$loopround = ceil($countnum[$i]/6);
			for($j=1; $j<=$loopround; $j++){
				if($j == 1){
					$slen = 0;
					$elen = $countnum[$i]-(($loopround-1)*6);
				}else{
					$slen = $countnum[$i]-((($loopround+1)-$j)*6);
					$elen = 6;
				}
				$var[$i][] = substr($number[$i], $slen, $elen);
			}
		}	

		$nstring[$i] = "";
		for($k=0; $k<count($var[$i]); $k++){
			if($k > 0) $nstring[$i] .= $t2[7];
			$val = $var[$i][$k];
			$tnstring = "";
			$countval = strlen($val);
			for($l=7; $l>=2; $l--){
				if($countval >= $l){
					$v = substr($val, -$l, 1);
					if($v > 0){
						if($l == 2 && $v == 1){
							$tnstring .= $t2[($l)];
						}elseif($l == 2 && $v == 2){
							$tnstring .= $t2[1].$t2[($l)];
						}else{
							$tnstring .= $t1[$v].$t2[($l)];
						}
					}
				}
			}
			if($countval >= 1){
				$v = substr($val, -1, 1);
				if($v > 0){
					if($v == 1 && $countval > 1 && substr($val, -2, 1) > 0){
						$tnstring .= $t2[0];
					}else{
						$tnstring .= $t1[$v];
					}

				}
			}

			$nstring[$i] .= $tnstring;
		}

	}
	$rstring = "";
	if(!empty($nstring[0]) || $zerobahtshow == 1 || empty($nstring[1])){
		if($nstring[0] == "") $nstring[0] = $t1[0];
		$rstring .= $nstring[0]."บาท";
	}
	if(count($number) == 1 || empty($nstring[1])){
		$rstring .= "ถ้วน";
	}else{
		$rstring .= $nstring[1]."สตางค์";
	}
	return $rstring;
}
// set bacground image
/*
$heading_html = <<<EOD
<img src="images/header.png" />

EOD;

$pdf->writeHTMLCell($w=0, $h=0, $x='10', $y='5', $heading_html, $border=0, $ln=1, $fill=0, $reseth=true, $align='L', $autopadding=false);

*/

$create_date = formatDate($this->item->create_date);


// $html = <<<EOD
// เลขที่ : {$this->item->run_no} <br />
// วันที่ : {$create_date}

// EOD;


// $money_word = num2thai($this->item->grand_total);

// // print a block of text using Write()
// $pdf->writeHTMLCell($w=0, $h=0, $x='10', $y='10', $html, $border=0, $ln=1, $fill=0, $reseth=true, $align='R', $autopadding=false);



// Line Processing
$insemination_time_map = array(1=>"เช้า", 2=>"เย็น");
$line_html = "";

for($i=0;$i<count($this->item->insemination_line);$i++){
	$index=$i+1;
$line_html .= <<<EOD
<tr nobr="true">
		<td>{$index}</td>
		<td>{$this->item->insemination_line[$i]->cow->cow_id}</td>
		<td>{$this->item->insemination_line[$i]->cow->name}</td>
		<td></td>
		<td style="text-align:center">{$insemination_time_map[$this->item->insemination_line[$i]->insemination_time]}</td>
		<td>{$this->item->insemination_line[$i]->lot_no}</td>
		<td style="text-align:right">{$this->item->insemination_line[$i]->price}</td>
		<td style="text-align:center">{$this->item->insemination_line[$i]->amount}</td>
		<td style="text-align:right">{$this->item->insemination_line[$i]->total_price}</td>
	</tr>
EOD;
}

 

$html = <<<EOD
<div style="text-align:center;font-size:20px">
			<img src="../images/dpo_logo.jpg" width="70"/><br />
			<b>
				องค์การส่งเสริมกิจการโคนมแห่งประเทศไทย (อ.ส.ค.) กระทรวงเกษตรและสหกรณ์<br />
				รายได้-ค่าใช้จ่ายในการเลี้ยงโคนมประจำเดือน {$this->item->month} ปี {$this->item->year}
			</b>

</div>
<br />
<table>
	<tr nobr="true">
		<td>
			สหกรณ์/ศูนย์รับน้ำนม: {$this->item->coop}
		</td>
		<td>
			หมายเลขสมาชิก: {$this->item->member_code}
		</td>
		<td>
			ชื่อเกษตรกร: {$this->item->farmer}
		</td>
	</tr>
</table>
<br />


<table border="1" width="670" cellpadding="5"  nobr="true">
					<tr nobr="true">
						<th style="text-align:center" width="80"><b>ลำดับ</b></th>
						<th style="text-align:center" width="460"><b>รายได้</b></th>
						<th style="text-align:center" width="120"><b>เป็นเงิน (บาท)</b></th>
					</tr>
					<tr nobr="true">
						<td valign="top" style="text-align:center">1.</td>
						<td>
							ค่าน้ำนมดิบ <br />
							1.1 ปริมาณน้ำนม งวด 1 
							{$this->item->milk_amount1}
							กก.ๆ ละ
							{$this->item->milk_price1}
							บาท
							<br />
							1.2 ปริมาณน้ำนม งวด 2
							{$this->item->milk_amount2}
							กก.ๆ ละ
							{$this->item->milk_price2}
						</td>
						<td style="text-align:center">
							<br /><br />
							{$this->item->expense1}<br />
							{$this->item->expense2}
						</td>
					</tr>
					<tr nobr="true">
						<td valign="top" style="text-align:center">2.</td>
						<td>
							ค่าขายลูกโค/โคนม
						</td>
						<td style="text-align:center">
							{$this->item->expense3}
						</td>
					</tr>
					<tr nobr="true">
						<td valign="top" style="text-align:center">3.</td>
						<td>
							รายรับอื่นๆ (ระบุ) {$this->item->other_income}
						</td>
						<td style="text-align:center">
							{$this->item->expense4}
						</td>
					</tr>
					<tr nobr="true">
						<td valign="top" style="text-align:center"></td>
						<td style="font-weight:bold;text-align:right">
							รวมรายรับ
						</td>
						<td style="text-align:center">
							{$this->item->expense5}
						</td>
					</tr>
					<tr nobr="true">
						<th style="text-align:center"><b>ลำดับ</b></th>
						<th style="text-align:center"><b>ค่าใช้จ่าย</b></th>
						<th style="text-align:center"><b>เป็นเงิน (บาท)</b></th>
					</tr>
					<tr nobr="true">
						<td valign="top" style="text-align:center">1.</td>
						<td>
							อาหารข้นซื้อสำเร็จ จำนวน {$this->item->food_amount} กก.ๆ ละ {$this->item->food_price} บาท
						</td>
						<td style="text-align:center">
							{$this->item->expense6}
						</td>
					</tr>
					<tr nobr="true">
						<td valign="top" style="text-align:center">2.</td>
						<td>
							อาหารข้นโดยซื้อวัตถุดิบ {$this->item->concentrate_food}
						</td>
						<td style="text-align:center">
							{$this->item->expense7}
						</td>
					</tr>
					<tr nobr="true">
						<td valign="top" style="text-align:center">3.</td>
						<td>
							นมผงเลี้ยงลูกโค จำนวน {$this->item->milk_powder_amount} กก.ๆ ละ {$this->item->milk_powder_price} บาท
						</td>
						<td style="text-align:center">
							{$this->item->expense8}
						</td>
					</tr>
					<tr nobr="true">
						<td valign="top" style="text-align:center">4.</td>
						<td>
							อาหารหยาบ (ฟาง, หญ้า ฯลฯ) {$this->item->coarse_food_amount} กก. 
						</td>
						<td style="text-align:center">
							{$this->item->expense9}
						</td>
					</tr>

					<tr nobr="true">
						<td valign="top" style="text-align:center">5.</td>
						<td>
							แร่ธาตุ
						</td>
						<td style="text-align:center">
							{$this->item->expense10}
						</td>
					</tr>
					<tr nobr="true">
						<td valign="top" style="text-align:center">6.</td>
						<td>
							ค่าผสมเทียม
						</td>
						<td style="text-align:center">
							{$this->item->expense11}
						</td>
					</tr>
					<tr nobr="true">
						<td valign="top" style="text-align:center">7.</td>
						<td>
							ค่าสัตวแพทย์ ค่ายา และวัคซีน
						</td>
						<td style="text-align:center">
							{$this->item->expense12}
						</td>
					</tr>
					<tr nobr="true">
						<td valign="top" style="text-align:center">8.</td>
						<td>
							ค่าจ้างรถส่งนม
						</td>
						<td style="text-align:center">
							{$this->item->expense13}
						</td>
					</tr>
					<tr nobr="true">
						<td valign="top" style="text-align:center">9.</td>
						<td>
							ค่าน้ำมันเชื้อเพลิงที่ใช้ในกิจการโคนม
						</td>
						<td style="text-align:center">
							{$this->item->expense14}
						</td>
					</tr>
					<tr nobr="true">
						<td valign="top" style="text-align:center">10.</td>
						<td>
							ค่าปุ๋ยที่ใช้ในแปลงหญ้า
						</td>
						<td style="text-align:center">
							{$this->item->expense15}
						</td>
					</tr>
					<tr nobr="true">
						<td valign="top" style="text-align:center">11.</td>
						<td>
							ค่าไฟฟ้า
						</td>
						<td style="text-align:center">
							{$this->item->expense16}
						</td>
					</tr>
					<tr nobr="true">
						<td valign="top" style="text-align:center">12.</td>
						<td>
							ค่าน้ำประปา
						</td>
						<td style="text-align:center">
							{$this->item->expense17}
						</td>
					</tr>
					<tr nobr="true">
						<td valign="top" style="text-align:center">13.</td>
						<td>
							ค่าแรงงานจ้าง
						</td>
						<td style="text-align:center">
							{$this->item->expense18}
						</td>
					</tr>
					<tr nobr="true">
						<td valign="top" style="text-align:center">14.</td>
						<td>
							ค่าแรงงานในครอบครัว
						</td>
						<td style="text-align:center">
							{$this->item->expense19}
						</td>
					</tr>
					<tr nobr="true">
						<td valign="top" style="text-align:center">15.</td>
						<td>
							ค่าดอกเบี้ยเงินกู้
						</td>
						<td style="text-align:center">
							{$this->item->expense20}
						</td>
					</tr>
					<tr nobr="true">
						<td valign="top" style="text-align:center">16.</td>
						<td>
							ภาษีหัก ณ ที่จ่าย
						</td>
						<td style="text-align:center">
							{$this->item->expense21}
						</td>
					</tr>
					<tr nobr="true">
						<td valign="top" style="text-align:center">17.</td>
						<td>
							ค่าบริการตรวจคุณภาพนมดิบ
						</td>
						<td style="text-align:center">
							{$this->item->expense22}
						</td>
					</tr>
					<tr nobr="true">
						<td valign="top" style="text-align:center">18.</td>
						<td>
							ซื้อพันธ์โคนม
						</td>
						<td style="text-align:center">
							{$this->item->expense23}
						</td>
					</tr>

					<tr nobr="true">
						<td valign="top" style="text-align:center">19.</td>
						<td>
							ค่าซ่อมแซม (ระบุ) {$this->item->fix_expense}
						</td>
						<td style="text-align:center">
							{$this->item->expense24}
						</td>
					</tr>

					<tr nobr="true">
						<td valign="top" style="text-align:center">20.</td>
						<td>
							ค่าใช้จ่ายอื่นๆ (ระบุ) {$this->item->other_expense}
						</td>
						<td style="text-align:center">
							{$this->item->expense25}
						</td>
					</tr>
					<tr nobr="true">
						<td valign="top" style="text-align:center"></td>
						<td style="font-weight:bold;text-align:right">
							รวมรายจ่าย
						</td>
						<td style="text-align:center">
							{$this->item->expense26}
						</td>
					</tr>
					<tr nobr="true">
						<th style="text-align:center"></th>
						<th style="text-align:center"><b>กำไร</b></th>
						<th style="text-align:center"><b>เป็นเงิน (บาท)</b></th>
					</tr>
					<tr nobr="true">
						<td valign="top" style="text-align:center"></td>
						<td style="font-weight:bold;text-align:right">
							รายรับ - รายจ่าย
						</td>
						<td style="text-align:center">
							{$this->item->expense27}
						</td>
					</tr>
				</table>
EOD;




// print a block of text using Write()
$pdf->writeHTMLCell($w=0, $h=0, $x='10', $y='10', $html, $border=0, $ln=1, $fill=0, $reseth=true, $align='L', $autopadding=false);

 






// set bacground image

// if (isset($_POST['download'])) {
// 	$pdf->Output("detail_id{$_POST[id]}.pdf", 'D');
// }else{
	$pdf->Output("Invoice_{$_GET[id]}.pdf", 'I');
// }
die();
