<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_edairy/css/form.css');

if($this->item->create_date==""){
	$this->item->create_date = date("Y-m-d");
}
?>

<script src="../media/com_edairy/typeahead/bootstrap3-typeahead.js" type="text/javascript" charset="UTF-8"></script>

<script src="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript" charset="UTF-8"></script>

<link rel="stylesheet" href="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.css">



<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		js('.datetimepicker').datetimepicker({
            lang:'th',
            timepicker:false,
            format:'d/m/Y'
        });
	});

	Joomla.submitbutton = function (task) {
		if (task == 'farmer_payment.cancel') {
			Joomla.submitform(task, document.getElementById('farmer_payment-form'));
		}
		else {
			
			if (task != 'farmer_payment.cancel' && document.formvalidator.isValid(document.id('farmer_payment-form'))) {
				
				Joomla.submitform(task, document.getElementById('farmer_payment-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_edairy&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="farmer_payment-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_EDAIRY_TITLE_FARMER_PAYMENT', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">
									<div style="display:none">
										<?php echo $this->form->renderField('id'); ?>
									</div>
									<?php echo $this->form->renderField('farmer_id'); ?>
									<div class="control-group">
													<div class="control-label">
														<label>
															วันที่
														</label>
													</div>
													<div class="controls">
														<input type="text" name="jform[create_date]" class="datetimepicker"  value="<?php echo formatDate($this->item->create_date); ?>"/>
													</div>
												</div>
												
				<?php echo $this->form->renderField('month'); ?>
				<?php echo $this->form->renderField('year'); ?>



				<hr />

				<table border="1" width="800" cellpadding="5">
					<tr>
						<th style="text-align:center">ลำดับ</th>
						<th style="text-align:center">รายได้</th>
						<th style="text-align:center">เป็นเงิน (บาท)</th>
					</tr>
					<tr>
						<td valign="top" style="text-align:center">1.</td>
						<td>
							ค่าน้ำนมดิบ <br />
							1.1 ปริมาณน้ำนม งวด 1 
							<input type="text" style="width:100px" name="jform[milk_amount1]" value="<?php echo $this->item->milk_amount1; ?>" />
							กก.ๆ ละ
							<input type="text" style="width:100px" name="jform[milk_price1]" value="<?php echo $this->item->milk_price1; ?>"/>
							บาท
							<br />
							1.2 ปริมาณน้ำนม งวด 2
							<input type="text" style="width:100px" name="jform[milk_amount2]" value="<?php echo $this->item->milk_amount2; ?>" />
							กก.ๆ ละ
							<input type="text" style="width:100px" name="jform[milk_price2]" value="<?php echo $this->item->milk_price2; ?>"/>
							บาท
						</td>
						<td style="text-align:center">
							<br />
							<input type="text" name="jform[expense1]" value="<?php echo $this->item->expense1; ?>" /><br /><input type="text" name="jform[expense2]" value="<?php echo $this->item->expense2; ?>" />
						</td>
					</tr>
					<tr>
						<td valign="top" style="text-align:center">2.</td>
						<td>
							ค่าขายลูกโค/โคนม
						</td>
						<td style="text-align:center">
							<input type="text" name="jform[expense3]" value="<?php echo $this->item->expense3; ?>"/>
						</td>
					</tr>
					<tr>
						<td valign="top" style="text-align:center">3.</td>
						<td>
							รายรับอื่นๆ (ระบุ) <input type="text" name="jform[other_income]" value="<?php echo $this->item->other_income; ?>" />
						</td>
						<td style="text-align:center">
							<input type="text" name="jform[expense4]" value="<?php echo $this->item->expense4; ?>"/>
						</td>
					</tr>
					<tr>
						<td valign="top" style="text-align:center"></td>
						<td style="font-weight:bold;text-align:right">
							รวมรายรับ
						</td>
						<td style="text-align:center">
							<input type="text" name="jform[expense5]" value="<?php echo $this->item->expense5; ?>"/>
						</td>
					</tr>
					<tr>
						<th style="text-align:center">ลำดับ</th>
						<th style="text-align:center">ค่าใช้จ่าย</th>
						<th style="text-align:center">เป็นเงิน (บาท)</th>
					</tr>
					<tr>
						<td valign="top" style="text-align:center">1.</td>
						<td>
							อาหารข้นซื้อสำเร็จ จำนวน <input type="text" style="width:100px" name="jform[food_amount]" value="<?php echo $this->item->food_amount; ?>" /> กก.ๆ ละ <input type="text" style="width:100px" name="jform[food_price]" value="<?php echo $this->item->food_price; ?>"/> บาท
						</td>
						<td style="text-align:center">
							<input type="text" name="jform[expense6]" value="<?php echo $this->item->expense6; ?>" />
						</td>
					</tr>
					<tr>
						<td valign="top" style="text-align:center">2.</td>
						<td>
							อาหารข้นโดยซื้อวัตถุดิบ <input type="text" name="jform[concentrate_food]" value="<?php echo $this->item->concentrate_food; ?>" /> 
						</td>
						<td style="text-align:center">
							<input type="text" name="jform[expense7]" value="<?php echo $this->item->expense7; ?>" />
						</td>
					</tr>
					<tr>
						<td valign="top" style="text-align:center">3.</td>
						<td>
							นมผงเลี้ยงลูกโค จำนวน <input type="text" style="width:100px" name="jform[milk_powder_amount]" value="<?php echo $this->item->milk_powder_amount; ?>"/> กก.ๆ ละ <input type="text" style="width:100px" name="jform[milk_powder_price]" value="<?php echo $this->item->milk_powder_price; ?>"/> บาท
						</td>
						<td style="text-align:center">
							<input type="text" name="jform[expense8]" value="<?php echo $this->item->expense8; ?>" />
						</td>
					</tr>
					<tr>
						<td valign="top" style="text-align:center">4.</td>
						<td>
							อาหารหยาบ (ฟาง, หญ้า ฯลฯ) <input type="text" style="width:100px" name="jform[coarse_food_amount]" value="<?php echo $this->item->coarse_food_amount; ?>"/> กก. 
						</td>
						<td style="text-align:center">
							<input type="text" name="jform[expense9]" value="<?php echo $this->item->expense9; ?>" />
						</td>
					</tr>

					<tr>
						<td valign="top" style="text-align:center">5.</td>
						<td>
							แร่ธาตุ
						</td>
						<td style="text-align:center">
							<input type="text" name="jform[expense10]" value="<?php echo $this->item->expense10; ?>" />
						</td>
					</tr>
					<tr>
						<td valign="top" style="text-align:center">6.</td>
						<td>
							ค่าผสมเทียม
						</td>
						<td style="text-align:center">
							<input type="text" name="jform[expense11]" value="<?php echo $this->item->expense11; ?>" />
						</td>
					</tr>
					<tr>
						<td valign="top" style="text-align:center">7.</td>
						<td>
							ค่าสัตวแพทย์ ค่ายา และวัคซีน
						</td>
						<td style="text-align:center">
							<input type="text" name="jform[expense12]" value="<?php echo $this->item->expense12; ?>" />
						</td>
					</tr>
					<tr>
						<td valign="top" style="text-align:center">8.</td>
						<td>
							ค่าจ้างรถส่งนม
						</td>
						<td style="text-align:center">
							<input type="text" name="jform[expense13]" value="<?php echo $this->item->expense13; ?>" />
						</td>
					</tr>
					<tr>
						<td valign="top" style="text-align:center">9.</td>
						<td>
							ค่าน้ำมันเชื้อเพลิงที่ใช้ในกิจการโคนม
						</td>
						<td style="text-align:center">
							<input type="text" name="jform[expense14]" value="<?php echo $this->item->expense14; ?>" />
						</td>
					</tr>
					<tr>
						<td valign="top" style="text-align:center">10.</td>
						<td>
							ค่าปุ๋ยที่ใช้ในแปลงหญ้า
						</td>
						<td style="text-align:center">
							<input type="text" name="jform[expense15]" value="<?php echo $this->item->expense15; ?>" />
						</td>
					</tr>
					<tr>
						<td valign="top" style="text-align:center">11.</td>
						<td>
							ค่าไฟฟ้า
						</td>
						<td style="text-align:center">
							<input type="text" name="jform[expense16]" value="<?php echo $this->item->expense16; ?>" />
						</td>
					</tr>
					<tr>
						<td valign="top" style="text-align:center">12.</td>
						<td>
							ค่าน้ำประปา
						</td>
						<td style="text-align:center">
							<input type="text" name="jform[expense17]" value="<?php echo $this->item->expense17; ?>" />
						</td>
					</tr>
					<tr>
						<td valign="top" style="text-align:center">13.</td>
						<td>
							ค่าแรงงานจ้าง
						</td>
						<td style="text-align:center">
							<input type="text" name="jform[expense18]" value="<?php echo $this->item->expense18; ?>" />
						</td>
					</tr>
					<tr>
						<td valign="top" style="text-align:center">14.</td>
						<td>
							ค่าแรงงานในครอบครัว
						</td>
						<td style="text-align:center">
							<input type="text" name="jform[expense19]" value="<?php echo $this->item->expense19; ?>" />
						</td>
					</tr>
					<tr>
						<td valign="top" style="text-align:center">15.</td>
						<td>
							ค่าดอกเบี้ยเงินกู้
						</td>
						<td style="text-align:center">
							<input type="text" name="jform[expense20]" value="<?php echo $this->item->expense20; ?>" />
						</td>
					</tr>
					<tr>
						<td valign="top" style="text-align:center">16.</td>
						<td>
							ภาษีหัก ณ ที่จ่าย
						</td>
						<td style="text-align:center">
							<input type="text" name="jform[expense21]" value="<?php echo $this->item->expense21; ?>" />
						</td>
					</tr>
					<tr>
						<td valign="top" style="text-align:center">17.</td>
						<td>
							ค่าบริการตรวจคุณภาพนมดิบ
						</td>
						<td style="text-align:center">
							<input type="text" name="jform[expense22]" value="<?php echo $this->item->expense22; ?>" />
						</td>
					</tr>
					<tr>
						<td valign="top" style="text-align:center">18.</td>
						<td>
							ซื้อพันธ์โคนม
						</td>
						<td style="text-align:center">
							<input type="text" name="jform[expense23]" value="<?php echo $this->item->expense23; ?>" />
						</td>
					</tr>

					<tr>
						<td valign="top" style="text-align:center">19.</td>
						<td>
							ค่าซ่อมแซม (ระบุ) <input type="text" name="jform[fix_expense]" value="<?php echo $this->item->fix_expense; ?>" /> 
						</td>
						<td style="text-align:center">
							<input type="text" name="jform[expense24]" value="<?php echo $this->item->expense24; ?>" />
						</td>
					</tr>

					<tr>
						<td valign="top" style="text-align:center">20.</td>
						<td>
							ค่าใช้จ่ายอื่นๆ (ระบุ) <input type="text" name="jform[other_expense]" value="<?php echo $this->item->other_expense; ?>" /> 
						</td>
						<td style="text-align:center">
							<input type="text" name="jform[expense25]" value="<?php echo $this->item->expense25; ?>" />
						</td>
					</tr>
					<tr>
						<td valign="top" style="text-align:center"></td>
						<td style="font-weight:bold;text-align:right">
							รวมรายจ่าย
						</td>
						<td style="text-align:center">
							<input type="text" name="jform[expense26]" value="<?php echo $this->item->expense26; ?>" />
						</td>
					</tr>
					<tr>
						<th style="text-align:center"></th>
						<th style="text-align:center"><b>กำไร</b></th>
						<th style="text-align:center"><b>เป็นเงิน (บาท)</b></th>
					</tr>
					<tr>
						<td valign="top" style="text-align:center"></td>
						<td style="font-weight:bold;text-align:right">
							รายรับ - รายจ่าย
						</td>
						<td style="text-align:center">
							<input type="text" name="jform[expense27]" value="<?php echo $this->item->expense27; ?>" />
						</td>
					</tr>
				</table>
				<hr />
				
				<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
				
				<div style="display:none">
					<?php echo $this->form->renderField('state'); ?>
				</div>

				<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
				<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />
				<?php echo $this->form->renderField('created_by'); ?>

				<?php echo $this->form->renderField('modified_by'); ?>				
				<?php /*
				<?php echo $this->form->renderField('milk_amount1'); ?>
				<?php echo $this->form->renderField('milk_price1'); ?>
				<?php echo $this->form->renderField('milk_amount2'); ?>
				<?php echo $this->form->renderField('milk_price2'); ?>
				<?php echo $this->form->renderField('other_income'); ?>
				<?php echo $this->form->renderField('food_amount'); ?>
				<?php echo $this->form->renderField('food_price'); ?>
				<?php echo $this->form->renderField('concentrate_food'); ?>
				<?php echo $this->form->renderField('milk_powder_amount'); ?>
				<?php echo $this->form->renderField('milk_powder_price'); ?>
				<?php echo $this->form->renderField('coarse_food_amount'); ?>
				<?php echo $this->form->renderField('fix_expense'); ?>
				<?php echo $this->form->renderField('other_expense'); ?>

				<?php echo $this->form->renderField('expense1'); ?>
				<?php echo $this->form->renderField('expense2'); ?>
				<?php echo $this->form->renderField('expense3'); ?>
				<?php echo $this->form->renderField('expense4'); ?>
				<?php echo $this->form->renderField('expense5'); ?>
				<?php echo $this->form->renderField('expense6'); ?>
				<?php echo $this->form->renderField('expense7'); ?>
				<?php echo $this->form->renderField('expense8'); ?>
				<?php echo $this->form->renderField('expense9'); ?>
				<?php echo $this->form->renderField('expense10'); ?>
				<?php echo $this->form->renderField('expense11'); ?>
				<?php echo $this->form->renderField('expense12'); ?>
				<?php echo $this->form->renderField('expense13'); ?>
				<?php echo $this->form->renderField('expense14'); ?>
				<?php echo $this->form->renderField('expense15'); ?>
				<?php echo $this->form->renderField('expense16'); ?>
				<?php echo $this->form->renderField('expense17'); ?>
				<?php echo $this->form->renderField('expense18'); ?>
				<?php echo $this->form->renderField('expense19'); ?>
				<?php echo $this->form->renderField('expense20'); ?>
				<?php echo $this->form->renderField('expense21'); ?>
				<?php echo $this->form->renderField('expense22'); ?>
				<?php echo $this->form->renderField('expense23'); ?>
				<?php echo $this->form->renderField('expense24'); ?>
				<?php echo $this->form->renderField('expense25'); ?>
				<?php echo $this->form->renderField('expense26'); ?>
				<?php echo $this->form->renderField('expense27'); ?>
				<?php echo $this->form->renderField('expense28'); ?>
				<?php echo $this->form->renderField('expense29'); ?>
				<?php echo $this->form->renderField('expense30'); ?>*/ ?>


					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
