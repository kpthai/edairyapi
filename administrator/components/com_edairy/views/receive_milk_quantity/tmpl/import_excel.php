<?php
    require('../media/com_edairy/spreadsheet-reader-master/php-excel-reader/excel_reader2.php');

    require('../media/com_edairy/spreadsheet-reader-master/SpreadsheetReader.php');

    $reader = new SpreadsheetReader("../upload/{$_REQUEST[filename]}");
    // insert every row just after reading it
?>
<h2>นำเข้าข้อมูลเสร็จสมบูรณ์</h2>
<hr />
	<table width="1000" border="1" cellpadding="3">
		<tr>
			<th>รหัสศูนย์</th>
			<th>วันที่</th>
			<th>รอบ</th>
			<th>เบอร์ถัง</th>
			<th>ปริมาณน้ำนมดิบ (กก.)</th>
		</tr>
	
		<?php
			$i=0;
		    foreach ($reader as $row)
		    {
		    	if($i!=0){
		    		$row[3] = sprintf('%04d', $row[3]);

		    		$db = JFactory::getDbo();
					$query = $db->getQuery(true);

					$query
							->select('*')
							->from("#__ed_farm")
							->where("member_code ='{$row[0]}{$row[3]}' ");
					$db->setQuery($query);
					$farm = $db->loadObject();

					$query = $db->getQuery(true);

					$query
							->select('*')
							->from("#__ed_farmer")
							->where("farm_id ='{$farm->id}' ");
					$db->setQuery($query);
					$farmer = $db->loadObject();

					$query = $db->getQuery(true);

					$query
							->select('*')
							->from("#__ed_coop")
							->where("id ='{$farm->coop_id}' ");
					$db->setQuery($query);
					$coop= $db->loadObject();

					if($coop==null){
						continue;
					}

					// RUN NO

					$query = $db->getQuery(true);
					
					$month_run_no = date("m");
					$year_run_no = substr(date("Y"), 2, 2)+43;
					$query
							->select('*')
							->from("#__ed_receive_milk_quantity")
							->where("run_no LIKE '{$coop->coop_abbr}-R{$year_run_no}{$month_run_no}%' ")
							->order("run_no desc");
					$db->setQuery($query);
					$last_row= $db->loadObject();

					/*if($i!=0){
						echo $query;
						print_r($last_row);
						die();
					}*/
					if($last_row==null){
						$run_no = "{$coop->coop_abbr}-R{$year_run_no}{$month_run_no}00001";
					}else{
						$last_row_run_no = sprintf('%05d', intval(substr($last_row->run_no, -5))+1);

						$run_no = "{$coop->coop_abbr}-R{$year_run_no}{$month_run_no}" . $last_row_run_no;
					}

					$data = new StdClass();
					$data->run_no = $run_no;
					$data->create_datetime = date("Y-m-d");

					if($row[2]=="เช้า"){
						$round=1;
					}else{
						$round=2;
					}
					$data->round = $round;
					$data->total_amount = $row[4];
					$data->coop_id = $coop->id;
					$data->farmer_id = $farmer->id;
					$user = JFactory::getUser();

					$data->ordering=0;
					$data->state=1;
					$data->checked_out = 0;
					$data->checked_out_time = "0000-00-00 00:00:00";
					$data->created_by = $user->id;

					
					$addDataResult = JFactory::getDbo()->insertObject('#__ed_receive_milk_quantity', $data, 'id');

					$saved_item_id = $data->id;
					// Line

					$data = new StdClass();
					$data->amount = $row[4];
					$data->farmer_id = $farmer->id;
					
					$data->state=1;
					$data->ordering=0;
					$data->checked_out = 0;
					$data->checked_out_time = "0000-00-00 00:00:00";

					//FK
					$data->receive_milk_quantity_id = $saved_item_id;

					$user = JFactory::getUser();

					$data->created_by = $user->id;

				
					$addDataResult = JFactory::getDbo()->insertObject('#__ed_receive_milk_quantity_line', $data, 'id');


					//print_r($farmer);

		?>
			<tr>
				<td align="center"><?php echo $row[0]; ?></td>
				<td align="center"><?php echo $row[1]; ?></td>
				<td align="center"><?php echo $row[2]; ?></td>
				<td align="center"><?php echo $row[3]; ?></td>
				<td align="right"><?php echo $row[4]; ?></td>
			</tr>
		<?php
				}
		       $i++;
		    }
		?>
</table>
<hr />
<input class="btn btn-primary" type="button" onclick="javascript:window.location='index.php?option=com_edairy&view=receive_milk_quantities'" value="ตกลง" style="width:150px" />