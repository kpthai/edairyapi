<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_edairy/css/form.css');


?>

<script src="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript" charset="UTF-8"></script>

<link rel="stylesheet" href="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.css">

<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		js('.datetimepicker').datetimepicker({
            lang:'th',
            timepicker:false,
            format:'d/m/Y'
        });
	js('input:hidden.coop_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('coop_idhidden')){
			js('#jform_coop_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_coop_id").trigger("liszt:updated");
	js('input:hidden.farmer_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('farmer_idhidden')){
			js('#jform_farmer_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_farmer_id").trigger("liszt:updated");
	});

	Joomla.submitbutton = function (task) {
		if (task == 'receive_milk_quantity.cancel') {
			Joomla.submitform(task, document.getElementById('receive_milk_quantity-form'));
		}
		else {
			
			if (task != 'receive_milk_quantity.cancel' && document.formvalidator.isValid(document.id('receive_milk_quantity-form'))) {
				
				Joomla.submitform(task, document.getElementById('receive_milk_quantity-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_edairy&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="receive_milk_quantity-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_EDAIRY_TITLE_RECEIVE_MILK_QUANTITY', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

								<div style="display:none">
									<?php echo $this->form->renderField('id'); ?>
								</div>
				<?php //echo $this->form->renderField('run_no'); ?>
				<?php //echo $this->form->renderField('create_datetime'); ?>



				<table>
										<tr>
											<td>
				<?php echo $this->form->renderField('run_no'); ?>
											</td>
											<td>
												&nbsp;&nbsp;
											</td>
											<td>
				<?php // echo $this->form->renderField('create_date'); ?>
												<div class="control-group">
													<div class="control-label">
														<label>
															วันที่
														</label>
													</div>
													<div class="controls">
														<input type="text" name="jform[create_datetime]" class="datetimepicker"  value="<?php echo formatDate($this->item->create_datetime); ?>"/>
													</div>
												</div>
											</td>
										</tr>
									</table>

								<table>
										<tr>
											<td>
													<?php echo $this->form->renderField('coop_id'); ?>

			<?php
				foreach((array)$this->item->coop_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="coop_id" name="jform[coop_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>
											</td>
											<td>
												&nbsp;&nbsp;
											</td>
											<td>
					<?php echo $this->form->renderField('farmer_id'); ?>

				<?php
					foreach((array)$this->item->farmer_id as $value): 
						if(!is_array($value)):
							echo '<input type="hidden" class="farmer_id" name="jform[farmer_idhidden]['.$value.']" value="'.$value.'" />';
						endif;
					endforeach;
				?>
											</td>
										</tr>

										<tr>
											<td><?php echo $this->form->renderField('round'); ?></td>
											<td>
												&nbsp;&nbsp;
											</td>
											<td>
											<div class="control-group">
			<div class="control-label"><label id="jform_create_time-lbl" for="jform_create_time" class="hasTooltip" title="" data-original-title="<strong>เวลา</strong>">
	เวลา</label>
</div>
		<div class="controls"><input type="text" name="jform[create_time]" id="jform_create_time" value="<?php echo substr($this->item->create_time, 0, 5); ?>" placeholder="เวลา" aria-invalid="false"> น.</div>
</div>
</td>
										</tr>
									</table>

				


				<hr />

				<div style="width:400px;text-align:right">
						<input class="btn btn-primary" type="button" value="เพิ่มรายการ" onClick="javascript:addNewRow();" />
					</div>
					<div style="margin-top:-25px"><b>รายการถังน้ำนมที่รับซื้อ</b></div>
					<br />
					<table border="1" style="width:400px" cellpadding="5" class="clonableTable">
						<tr>
							<th style="text-align:center">ที่</th>
							<th style="text-align:center">ปริมาณ (กก.)</th>
							<th style="text-align:center">ลบ</th>
						</tr>
						<?php if(count($this->item->receive_milk_quantity_line)!=0){ ?>
							<?php for($i=0;$i<count($this->item->receive_milk_quantity_line);$i++){ ?>
								<tr>
									<td><div class="row-index"><?php echo $i+1; ?></div></td>
									<td style="text-align:center"><input type="text" name="amount[]" style="width:100px" value="<?php echo $this->item->receive_milk_quantity_line[$i]->amount; ?>" onChange="javascript:sum();" /></td>
									
									<td style="text-align:center"><input type="checkbox" name="delete_mark[]" value="<?php echo $i; ?>" onChange="javascript:sum();" /></td>
								</tr>
							<?php } ?>
						<?php }else{ ?>
							<?php for($i=0;$i<5;$i++){ ?>
								<tr>
									<td><div class="row-index"><?php echo $i+1; ?></div></td>
									<td style="text-align:center"><input type="text" name="amount[]" onChange="javascript:sum();" style="width:100px" /></td>
									
									<td style="text-align:center"><input type="checkbox" name="delete_mark[]" value="<?php echo $i; ?>" onChange="javascript:sum();" /></td>
								</tr>
							<?php } ?>
						<?php } ?>
					</table><br />
					<div style="clear:both"></div>
				<?php echo $this->form->renderField('total_amount'); ?>
				<hr />

												<?php echo $this->form->renderField('receiver_id'); ?>
				<?php echo $this->form->renderField('remark'); ?>
				<?php echo $this->form->renderField('denied_amount'); ?>

				<div style="display:none">
					<?php echo $this->form->renderField('state'); ?>
				</div>
				<?php echo $this->form->renderField('created_by'); ?>


					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>

<script type="text/javascript">

<?php if(count($this->item->receive_milk_quantity_line)!=0){ ?>
		var rowCount=<?php echo count($this->item->receive_milk_quantity_line); ?>;
	<?php }else{ ?>
		var rowCount=5;
	<?php } ?>
	function addNewRow(){
		var tr    = jQuery('.clonableTable tr:last');
		var clone = tr.clone();
		clone.removeClass("chzn-done");
		clone.find(':text').val('');
		clone.find('select').val('');
		clone.find('.chzn-container').remove();
		clone.find('select').css({display: "inline-block"});
		clone.find('select').css({width: "100%"});
		//clone.find('.insemination_time').css({width: "70px"});
		//clone.find('.insemination_time').val(1);
		clone.find('.row-index').html(++rowCount);
		clone.find('select').chosen();

		tr.after(clone);
	}
	function sum(){
	  var total=0;
	  var lineIndex = 0;
	  jQuery("input[name='amount[]']").each(function(){
	          //console.log(parseFloat($(this).val()));
	          if(!isNaN(parseFloat(jQuery(this).val())) && !jQuery("input[name='delete_mark[]']").eq(lineIndex).is(':checked')){
	            total+=parseFloat(jQuery(this).val());
	          }
	          lineIndex++;
	        });
	  jQuery("input[name='jform[total_amount]']").val(total.toFixed(2));
	}


</script>