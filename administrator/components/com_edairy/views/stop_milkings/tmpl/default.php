<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'administrator/components/com_edairy/assets/css/edairy.css');
$document->addStyleSheet(JUri::root() . 'media/com_edairy/css/list.css');

$user      = JFactory::getUser();
$userId    = $user->get('id');
$listOrder = $this->state->get('list.ordering');
$listDirn  = $this->state->get('list.direction');
$canOrder  = $user->authorise('core.edit.state', 'com_edairy');
$saveOrder = $listOrder == 'a.`ordering`';

if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?option=com_edairy&task=stop_milkings.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'stop_milkingList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}

$sortFields = $this->getSortFields();
?>
<style type="text/css">
	.js-stools{
		display: none;
	}
</style>

<script src="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript" charset="UTF-8"></script>

<link rel="stylesheet" href="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.css">
<script type="text/javascript">
	jQuery(document).ready(function () {
		jQuery('#clear-search-button').on('click', function () {
			jQuery('select[name=search_record_status]').val('');
			jQuery('select').trigger('liszt:updated');
			jQuery('.well input').val('');
			jQuery('#adminForm').submit();
		});
	});

</script>
<form action="<?php echo JRoute::_('index.php?option=com_edairy&view=stop_milkings'); ?>" method="post"
	  name="adminForm" id="adminForm">
	<?php if (!empty($this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
		<?php else : ?>
		<div id="j-main-container">
			<?php endif; ?>

			<h4>ค้นหาบันทึกการหยุดรีดนม</h4>
			<div id="filter-bar" class="btn-toolbar well" style="min-height:140px">

				<div class="filter-search btn-group pull-left">
					

					<label style="display:inline;">
						&nbsp;รหัสศูนย์/ชื่อสหกรณ์ 
					</label>
					 <input type="text" name="search_coop" value="<?php echo $_REQUEST["search_coop"]; ?>" />	

					

					<label style="display:inline;">
						&nbsp;หมายเลขสมาชิก/เบอร์ถัง
					</label>
					<input type="text" name="search_member_code" value="<?php echo $_REQUEST["search_member_code"]; ?>" />	
					 

					

					</div>

					<div style="clear:both"></div>

						<label style="display:inline;">
						&nbsp;ชื่อ
					</label>


					 <input type="text" name="search_farmer_name" value="<?php echo $_REQUEST["search_farmer_name"]; ?>"/>


					 <label style="display:inline;">
						&nbsp;นามสกุล
					</label>


					 <input type="text" name="search_farmer_surname" value="<?php echo $_REQUEST["search_farmer_surname"]; ?>"/>

					 <label style="display:inline;">
						&nbsp;หมายเลขโค
					</label>


					 <input type="text" name="search_cow_id" value="<?php echo $_REQUEST['search_cow_id']; ?>"/>


					 <label style="display:inline;">
						&nbsp;ชื่อโค
					</label>


					 <input type="text" name="search_cow_name" value="<?php echo $_REQUEST['search_cow_name']; ?>"/>

					 
					 	<div style="clear:both">
					</div>


					 <label style="display:inline;">
						&nbsp;แสดงรายการที่
					</label>


					 <select name="search_record_status">
					 	<option value="">ทั้งหมด</option>
					 	<option value="1"<?php if($_REQUEST["search_record_status"]==1){ echo " selected";}?>>ยังไม่บันทึก</option>
					 	<option value="2"<?php if($_REQUEST["search_record_status"]==2){ echo " selected";}?>>บันทึกแล้ว</option>
					 	
					 </select>

					<div style="clear:both">
					</div>



					<div class="btn-group pull-left">
					<button class="btn hasTooltip" type="submit"
							title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>">
						<i class="icon-search"></i></button>
					<button class="btn hasTooltip" id="clear-search-button" type="button"
							title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>">
						<i class="icon-remove"></i></button>
				</div>
				<div class="btn-group pull-right hidden-phone">
					<label for="limit"
						   class="element-invisible">
						<?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?>
					</label>
					<?php// echo $this->pagination->getLimitBox(); ?>
				</div>
				</div>
            <?php //echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this)); ?>

			<div class="clearfix"></div>
			<b>พบทั้งสิ้น <?php echo $this->pagination->total; ?> รายการ</b>
			
			<table class="table table-striped" id="stop_milkingList">
				<thead>
				<tr>
					<?php if (isset($this->items[0]->ordering)): ?>
						<th width="1%" class="nowrap center hidden-phone">
                            <?php echo JHtml::_('searchtools.sort', '', 'a.ordering', $listDirn, $listOrder, null, 'asc', 'JGRID_HEADING_ORDERING', 'icon-menu-2'); ?>
                        </th>
					<?php endif; ?>
					<th width="1%" class="hidden-phone">
						<input type="checkbox" name="checkall-toggle" value=""
							   title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)"/>
					</th>
					<?php if (isset($this->items[0]->state)): ?>
						<th width="1%" class="nowrap center">
								<?php echo JHtml::_('searchtools.sort', 'JSTATUS', 'a.`state`', $listDirn, $listOrder); ?>
</th>
					<?php endif; ?>

									<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'id', 'a.`id`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_STOP_MILKINGS_COW_ID', 'a.`cow_id`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'กำหนดวันหยุดรีด', 'a.`create_date`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'กำหนดวันคลอด', 'a.`birth_date`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'วันหยุดรีดนมจริง', 'a.`actual_date`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_STOP_MILKINGS_CREATE_BY', 'a.`create_by`', $listDirn, $listOrder); ?>
				</th>
				<th width="10%">
					บันทึกการหยุดรีดนม
				</th>
					
				</tr>
				</thead>
				<tfoot>
				<tr>
					<td colspan="<?php echo isset($this->items[0]) ? count(get_object_vars($this->items[0])) : 10; ?>">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
				</tfoot>
				<tbody>
				<?php foreach ($this->items as $i => $item) :
					$ordering   = ($listOrder == 'a.ordering');
					$canCreate  = $user->authorise('core.create', 'com_edairy');
					$canEdit    = $user->authorise('core.edit', 'com_edairy');
					$canCheckin = $user->authorise('core.manage', 'com_edairy');
					$canChange  = $user->authorise('core.edit.state', 'com_edairy');
					?>
					<tr class="row<?php echo $i % 2; ?>">

						<?php if (isset($this->items[0]->ordering)) : ?>
							<td class="order nowrap center hidden-phone">
								<?php if ($canChange) :
									$disableClassName = '';
									$disabledLabel    = '';

									if (!$saveOrder) :
										$disabledLabel    = JText::_('JORDERINGDISABLED');
										$disableClassName = 'inactive tip-top';
									endif; ?>
									<span class="sortable-handler hasTooltip <?php echo $disableClassName ?>"
										  title="<?php echo $disabledLabel ?>">
							<i class="icon-menu"></i>
						</span>
									<input type="text" style="display:none" name="order[]" size="5"
										   value="<?php echo $item->ordering; ?>" class="width-20 text-area-order "/>
								<?php else : ?>
									<span class="sortable-handler inactive">
							<i class="icon-menu"></i>
						</span>
								<?php endif; ?>
							</td>
						<?php endif; ?>
						<td class="hidden-phone">
							<?php echo JHtml::_('grid.id', $i, $item->id); ?>
						</td>
						<?php if (isset($this->items[0]->state)): ?>
							<td class="center">
								<?php echo JHtml::_('jgrid.published', $item->state, $i, 'stop_milkings.', $canChange, 'cb'); ?>
</td>
						<?php endif; ?>

										<td>

					<?php echo $item->id; ?>
				</td>				<td>

					<?php echo $item->cow_id; ?>
				</td>				<td>

					<?php echo formatDate($item->create_date); ?>
				</td>	
				<td>

					<?php echo formatDate($item->birth_date); ?>
				</td>
				<td>

					<?php echo formatDate($item->actual_date); ?>
				</td>
							<td>

					<?php echo $item->create_by; ?>
				</td>
				<td>
					<a href="<?php echo JRoute::_('index.php?option=com_edairy&task=stop_milking.edit&id='.(int) $item->id); ?>" class="btn btn-warning"> บันทีกผล</a>
				</td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>

			<input type="hidden" name="task" value=""/>
			<input type="hidden" name="boxchecked" value="0"/>
            <input type="hidden" name="list[fullorder]" value="<?php echo $listOrder; ?> <?php echo $listDirn; ?>"/>
			<?php echo JHtml::_('form.token'); ?>
		</div>
</form>
<script>
    window.toggleField = function (id, task, field) {

        var f = document.adminForm, i = 0, cbx, cb = f[ id ];

        if (!cb) return false;

        while (true) {
            cbx = f[ 'cb' + i ];

            if (!cbx) break;

            cbx.checked = false;
            i++;
        }

        var inputField   = document.createElement('input');

        inputField.type  = 'hidden';
        inputField.name  = 'field';
        inputField.value = field;
        f.appendChild(inputField);

        cb.checked = true;
        f.boxchecked.value = 1;
        window.submitform(task);

        return false;
    };
</script>