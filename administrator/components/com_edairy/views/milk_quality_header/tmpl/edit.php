<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_edairy/css/form.css');
?>
<script src="../media/com_edairy/typeahead/bootstrap3-typeahead.js" type="text/javascript" charset="UTF-8"></script>

<script src="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript" charset="UTF-8"></script>

<link rel="stylesheet" href="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.css">


<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
		js('.datetimepicker').datetimepicker({
            lang:'th',
            timepicker:false,
            format:'d/m/Y'
        });

	js('input:hidden.coop_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('coop_idhidden')){
			js('#jform_coop_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_coop_id").trigger("liszt:updated");
	});

	Joomla.submitbutton = function (task) {
		if (task == 'milk_quality_header.cancel') {
			Joomla.submitform(task, document.getElementById('milk_quality_header-form'));
		}
		else {
			
			if (task != 'milk_quality_header.cancel' && document.formvalidator.isValid(document.id('milk_quality_header-form'))) {

				if(jQuery("#jform_coop_id").val()==""){
					alert("กรุณาเลือก สหกรณ์/ศูนย์รับน้ำนม");
					return false;
				}
				
				Joomla.submitform(task, document.getElementById('milk_quality_header-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>


<form
	action="<?php echo JRoute::_('index.php?option=com_edairy&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="milk_quality_header-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_EDAIRY_TITLE_MILK_QUALITY_HEADER', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">


					
									<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
				<?php echo $this->form->renderField('run_no'); ?>
				

				<table>
						<tr>
							<td width="450">
								<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
				<?php echo $this->form->renderField('coop_id'); ?>
							</td>
							<td>
								<?php
				foreach((array)$this->item->coop_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="coop_id" name="jform[coop_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('period'); ?>

							</td>
						</tr>
					</table>

				<table>
						<tr>
							<td width="450">
								<div class="control-group">
									<div class="control-label">
										<label>
											ระหว่างวันที่
										</label>
									</div>
									<div class="controls">
										<input type="text" name="jform[from_date]" class="datetimepicker"  value="<?php echo formatDate($this->item->from_date); ?>"/>
									</div>
								</div>
							</td>
							<td>
								<div class="control-group">
									<div class="control-label">
										<label>
											ถึงวันที่
										</label>
									</div>
									<div class="controls">
										<input type="text" name="jform[to_date]" class="datetimepicker"  value="<?php echo formatDate($this->item->to_date); ?>"/>
									</div>
								</div>
							</td>
						</tr>
				</table>
									

				<div style="display:none">
					<?php echo $this->form->renderField('total_send_milk_amount'); ?>
				</div>

				<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
				
				<div style="float:right">
					<?php if($this->item->id==""){ ?>
						<button class="btn btn-info" type="button" onClick="javascript:alert('ระบบจะทำการบันทึกข้อมูล จากนั้นให้ท่านกดเพิ่มรายการอีกครั้ง');Joomla.submitbutton('milk_quality_header.apply');">เพิ่มรายการ</button>
					<?php }else{ ?>
						<button type="button" class="btn btn-info" data-toggle="modal" data-target="#QualityModal"  onClick="javascript:clearQualityForm();">เพิ่มรายการ</button>
					<?php } ?>
				</div>
				<br /><br />
				<div style="clear:both"></div>
				<table border="1" cellpadding="5" width="100%">
					<tr>
						<th>ลำดับที่</th>
						<th>หมายเลขสมาชิก</th>
						<th>ชื่อ-สกุล เกษตรกร</th>
						<th>สถานะ</th>
						<th>ราคา/กก. (บาท)</th>
						<th>บักทึกผลคุณภาพ</th>
					</tr>
					<?php $i=1;foreach($this->item->milk_quality_transaction as $key=>$data){ ?>
						<tr>
							<td style="text-align: center"><?php echo $i++; ?></td>
							<td><?php echo $data->farm->member_code; ?></td>
							<td><?php echo $data->farmer->citizen_id . " " . $data->farmer->name . " " . $data->farmer->surname; ?></td>
							<td style="text-align:center">บันทึกแล้ว</td>
							<td style="text-align:right"><?php echo $data->total_price; ?></td>
							<td style="text-align:center"><input type="button" class="btn btn-warning" value="บันทึกผลคุณภาพ" data-toggle="modal" data-target="#QualityModal" onClick="javascript:getMilkQuailityTransaction(<?php echo $data->id; ?>);"/></td>
						</tr>
					<?php } ?>

				</table>
				<br /><br />

				<div style="display: none">
					<?php echo $this->form->renderField('state'); ?>
				</div>
				
				<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
				<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />
				<?php echo $this->form->renderField('created_by'); ?>

				<?php echo $this->form->renderField('modified_by'); ?>

					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>


<!-- Modal -->
<div id="QualityModal" class="modal fade" role="dialog" style="display:none">
<form id="QualityForm" action="" method="post">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">คุณภาพน้ำนมเกษตรกร</h4>
      </div>
      <div class="modal-body">
        <table>
        	<tr>
        		<td width="120">เกษตรกร</td>
        		<td>
        			<select name="farmer_id" onChange="javascript:getFarmStandardScore(this.value);">
					 	<option value="">-- เลือกเกษตรกร --</option>
					 	<?php 
							foreach($this->masterData->mas_farmer as $bkey=>$bdata){ 
						?>
								<option value="<?php echo $bdata->id;?>"><?php echo $bdata->citizen_id; ?> : <?php echo $bdata->name; ?> <?php echo $bdata->surname; ?></option>
						<?php } ?>
        			</select>
        		</td>
        	</tr>
        </table>
        <hr />
        <b>ผลการตรวจวิเคราะห์</b>
        <table cellpadding="3">
        	<tr>
        		<td width="160">MB (Hrs.)</td>
        		<td><input type="text" id="mb" name="mb" onChange="javascript:calculateMb();" /></td>
        		<td width="160">ราคา เพิ่ม/ลด (บาท)</td>
        		<td><input type="text" id="mb_change" class="change" value="0.00" readonly /></td>
        	</tr>
        	<tr>
        		<td>SCC (Cell/ml.)</td>
        		<td><input type="text" id="scc" name="scc" onChange="javascript:calculateScc();" /></td>
        		<td>ราคา เพิ่ม/ลด (บาท)</td>
        		<td><input type="text" id="scc_change" class="change" value="0.00" readonly /></td>
        	</tr>
        	<tr>
        		<td>FAT (%)</td>
        		<td><input type="text" id="fat" name="fat" /></td>
        		<td>ราคา เพิ่ม/ลด (บาท)</td>
        		<td><input type="text" id="fat_change" class="change" value="0.00" readonly /></td>
        	</tr>
        	<tr>
        		<td>SNF (%)</td>
        		<td><input type="text" id="snf" name="snf" /></td>
        		<td>ราคา เพิ่ม/ลด (บาท)</td>
        		<td><input type="text" id="snf_change" class="change" value="0.00" readonly /></td>
        	</tr>
        	<tr>
        		<td>TS (%)</td>
        		<td><input type="text" id="ts" name="ts" /></td>
        		<td>ราคา เพิ่ม/ลด (บาท)</td>
        		<td><input type="text" id="ts_change" class="change" value="0.00" readonly /></td>
        	</tr>
        	<tr>
        		<td>Protein (%)</td>
        		<td><input type="text" id="protein" name="protein" /></td>
        		<td>ราคา เพิ่ม/ลด (บาท)</td>
        		<td><input type="text" id="protein_change" class="change" value="0.00" readonly /></td>
        	</tr>
        	<tr>
        		<td>Lactose (%)</td>
        		<td><input type="text" id="lactose" name="lactose" /></td>
        		<td>ราคา เพิ่ม/ลด (บาท)</td>
        		<td><input type="text" id="lactose_change" class="change" value="0.00" readonly /></td>
        	</tr>
        </table>

        <b>ค่ามาตรฐานฟาร์ม</b>
        <table cellpadding="3">
        	<tr>
        		<td width="160">คะแนนคอก</td>
        		<td><input type="text" id="farm_score" name="farm_score" readonly /></td>
        		<td width="160">ราคา เพิ่ม/ลด (บาท)</td>
        		<td><input type="text" id="farm_score_change" name="farm_score_change" class="change" readonly /></td>
        	</tr>
        	<tr>
        		<td>มาตรฐาน GAP</td>
        		<td><input type="text" id="gap" name="gap" readonly /></td>
        		<td>ราคา เพิ่ม/ลด (บาท)</td>
        		<td><input type="text" id="gap_change" name="gap_change" class="change" readonly /></td>
        	</tr>
        	<tr>
        		<td>ราคามาตรฐาน (บาท/กก.)</td>
        		<td><input type="text" name="price"  readonly /></td>
        		<td></td>
        		<td></td>
        	</tr>
        	<tr>
        		<td>สรุปราคา (บาท/กก.)</td>
        		<td><input name="total_price" type="text" readonly /></td>
        		<td></td>
        		<td></td>
        	</tr>
        </table>

        <input type="hidden" name="milk_quality_id" value="<?php echo $_REQUEST['id']; ?>" />
        <input type="hidden" name="milk_quality_transaction_id" value="" />
        <input type="hidden" name="option" value="<?php echo $_REQUEST['option'];?>" />
        <input type="hidden" name="task" value="milk_quality_header.saveMilkQualityTransaction" />
      </div>
      <div class="modal-footer" style="text-align:left">
      	<button type="button" class="btn btn-info" style="width:100px" onClick="javascript:saveMilkQualityTransaction();">บันทึก</button>&nbsp;&nbsp;
        <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
      </div>
    </div>

  </div>
  </form>
</div>

<script type="text/javascript">
	function getFarmStandardScore(farmer_id){
		jQuery.ajax({
            url: "index.php?option=com_edairy&task=milk_quality_header.getFarmStandardScore",
            type: "POST",
            data: {
                'farmer_id': farmer_id
            },
            success: function (data) {
            	var data = jQuery.parseJSON(data);
            	console.log(data);

            	if(data!=null){

	            	if(data.gap!=null){
	            		jQuery("input[name=gap]").val("มี ("+data.gap.certify_no+")");
	            		jQuery("input[name=gap_change]").val((<?php echo $this->masterData->mas_milk_price->gap_up_price; ?>).toFixed(2));
	            	}else{
	            		jQuery("input[name=gap]").val("ไม่มี");
	            		jQuery("input[name=gap_change]").val((<?php echo $this->masterData->mas_milk_price->no_gap_up_price; ?>).toFixed(2));
	            	}

	            	if(data.farm_score!=null){
	            		var score = data.farm_score.score;
	            		if(score>=90){
							jQuery("input[name=farm_score]").val(4);
							jQuery("input[name=farm_score_change]").val((<?php echo $this->masterData->mas_milk_price->score4_up_price; ?>).toFixed(2));
						}else if(score>=80){
							jQuery("input[name=farm_score]").val(3);
							jQuery("input[name=farm_score_change]").val((<?php echo $this->masterData->mas_milk_price->score3_up_price; ?>).toFixed(2));
						}else if(score>=70){
							jQuery("input[name=farm_score]").val(2);
							jQuery("input[name=farm_score_change]").val((<?php echo $this->masterData->mas_milk_price->score2_up_price; ?>).toFixed(2));
						}else{
							jQuery("input[name=farm_score]").val(1);
							jQuery("input[name=farm_score_change]").val((<?php echo $this->masterData->mas_milk_price->score1_up_price; ?>).toFixed(2));
						}
	            	}else{
	            		jQuery("input[name=farm_score]").val(0);
	            		jQuery("input[name=farm_score_change]").val("0.00");
	            	}

	            	// calculateGAP();
	            	// calculateFarmScore();
	            	 calculateTotalPrice();

	            }else{
	            	jQuery("input[name=gap]").val("ไม่มี");
	            }

            }

        });
	}
	function calculateMb(){
		var param_value = jQuery("#mb").val();
		console.log(param_value);
		for(var i=0;i<10;i++){
			if(lowArray[i]!=0 && highArray[i]!=0 && changeArray[i]!=0){
				if(parseFloat(param_value)>=lowArray[i] && parseFloat(param_value)<=highArray[i]){
					jQuery("#mb_change").val(changeArray[i].toFixed(2));
					calculateTotalPrice();
					break;
				}
			}
		}
	}

	function calculateScc(){
		var param_value = jQuery("#scc").val();
		console.log(param_value);
		for(var i=10;i<20;i++){
			if(lowArray[i]!=0 && highArray[i]!=0 && changeArray[i]!=0){
				if(parseFloat(param_value)>=lowArray[i] && parseFloat(param_value)<=highArray[i]){
					jQuery("#scc_change").val(changeArray[i].toFixed(2));
					calculateTotalPrice();
					break;
				}
			}
		}
	}

	function calculateFat(){
		var param_value = jQuery("#fat").val();
		console.log(param_value);
		for(var i=20;i<30;i++){
			if(lowArray[i]!=0 && highArray[i]!=0 && changeArray[i]!=0){
				if(parseFloat(param_value)>=lowArray[i] && parseFloat(param_value)<=highArray[i]){
					jQuery("#fat_change").val(changeArray[i].toFixed(2));
					calculateTotalPrice();
					break;
				}
			}
		}
	}

	function calculateSnf(){
		var param_value = jQuery("#snf").val();
		console.log(param_value);
		for(var i=30;i<40;i++){
			if(lowArray[i]!=0 && highArray[i]!=0 && changeArray[i]!=0){
				if(parseFloat(param_value)>=lowArray[i] && parseFloat(param_value)<=highArray[i]){
					jQuery("#snf_change").val(changeArray[i].toFixed(2));
					calculateTotalPrice();
					break;
				}
			}
		}
	}

	function calculateTs(){
		var param_value = jQuery("#ts").val();
		console.log(param_value);
		for(var i=40;i<50;i++){
			if(lowArray[i]!=0 && highArray[i]!=0 && changeArray[i]!=0){
				if(parseFloat(param_value)>=lowArray[i] && parseFloat(param_value)<=highArray[i]){
					jQuery("#ts_change").val(changeArray[i].toFixed(2));
					calculateTotalPrice();
					break;
				}
			}
		}
	}

	function calculateProtein(){
		var param_value = jQuery("#protein").val();
		console.log(param_value);
		for(var i=50;i<60;i++){
			if(lowArray[i]!=0 && highArray[i]!=0 && changeArray[i]!=0){
				if(parseFloat(param_value)>=lowArray[i] && parseFloat(param_value)<=highArray[i]){
					jQuery("#protein_change").val(changeArray[i].toFixed(2));
					calculateTotalPrice();
					break;
				}
			}
		}
	}


	function calculateLactose(){
		var param_value = jQuery("#lactose").val();
		console.log(param_value);
		for(var i=60;i<70;i++){
			if(lowArray[i]!=0 && highArray[i]!=0 && changeArray[i]!=0){
				if(parseFloat(param_value)>=lowArray[i] && parseFloat(param_value)<=highArray[i]){
					jQuery("#lactose_change").val(changeArray[i].toFixed(2));
					calculateTotalPrice();
					break;
				}
			}
		}
	}


	function calculateTotalPrice(){
		var standard_price = <?php echo $this->masterData->mas_milk_price->milk_standard_price; ?>;
		jQuery("#QualityForm input[name=total_price]").val((parseFloat(standard_price)+parseFloat(jQuery("#mb_change").val())+parseFloat(jQuery("#scc_change").val())+parseFloat(jQuery("#fat_change").val())+parseFloat(jQuery("#snf_change").val())+parseFloat(jQuery("#ts_change").val())+parseFloat(jQuery("#protein_change").val())+parseFloat(jQuery("#lactose_change").val())+parseFloat(jQuery("#gap_change").val())+parseFloat(jQuery("#farm_score_change").val())).toFixed(2));
	}

	function clearQualityForm(){
		jQuery("select[name=farmer_id]").val("");
		jQuery("select[name=farmer_id]").trigger('liszt:updated');
		jQuery("#QualityForm input[name=milk_quality_transaction_id]").val("");
		jQuery("#QualityForm input[type!=hidden]").val("");
		jQuery("#QualityForm .change").val("0.00");
		jQuery("#QualityForm input[name=price]").val("<?php echo $this->masterData->mas_milk_price->milk_standard_price; ?>");
		jQuery("#QualityForm input[name=total_price]").val("<?php echo $this->masterData->mas_milk_price->milk_standard_price; ?>");
	}

	function saveMilkQualityTransaction(){
		if(jQuery("input[name=farmer_id]").val()==""){
			alert("กรุณาใส่ข้อมูลให้ครบถ้วน");
			return false;
		}else{
			document.getElementById('QualityForm').submit();
		}
		
	}

	function getMilkQuailityTransaction(id){
		jQuery.ajax({
            url: "index.php?option=com_edairy&task=milk_quality_header.getMilkQualityTransaction",
            type: "POST",
            data: {
                'milk_quality_transaction_id': id
            },
            success: function (data) {
            	var data = jQuery.parseJSON(data);
            	console.log(data);

            	if(data!=null){

	            	jQuery("select[name=farmer_id]").val(data.farmer_id);

	            	jQuery("select[name=farmer_id]").trigger('liszt:updated');

	            	jQuery("input[name=mb]").val(data.mb);

	            	jQuery("input[name=scc]").val(data.scc);
	            	
	            	jQuery("input[name=fat]").val(data.fat);

	            	jQuery("input[name=snf]").val(data.snf);

	            	jQuery("input[name=ts]").val(data.ts);

	            	jQuery("input[name=protein]").val(data.protein);

	            	jQuery("input[name=lactose]").val(data.lactose);

	            	jQuery("input[name=milk_quality_transaction_id]").val(data.id);

	            	jQuery("input[name=price]").val(data.price);

	            	jQuery("input[name=total_price]").val(data.total_price);

	            	calculateMb();
	            	calculateScc();
	            	calculateFat();
	            	calculateSnf();
	            	calculateTs();
	            	calculateProtein();
	            	calculateLactose();

	            	getFarmStandardScore(data.farmer_id);

	            	calculateTotalPrice();

	            }

            }

        });
	}
	<?php if($_REQUEST["id"]!=""){ ?>

	jQuery("#toolbar").append('<div class="btn-wrapper" id="toolbar-upload"><button class="btn btn-small" onclick="javascript:window.location=\'index.php?option=com_edairy&view=milk_quality_header&layout=download_excel&id=<?php echo $_REQUEST['id']; ?>\'"><span class="icon-download"></span>ดาวน์โหลดไฟล์ Excel</button></div>');


	jQuery("#toolbar").append('<div class="btn-wrapper" id="toolbar-upload"><button class="btn btn-small" data-toggle="modal" data-target="#UploadFileModal"><span class="icon-upload"></span>อับโหลดไฟล์ Excel</button></div>');

	<?php } ?>

	var lowArray = new Array();
	var highArray = new Array();
	var changeArray = new Array();

	function milkParameterRange(low, high, valueChange){
		this.low = low;
		this.high = high;
		this.valueChange = valueChange;
	}

	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->mb_range1_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->mb_range2_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->mb_range3_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->mb_range4_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->mb_range5_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->mb_range6_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->mb_range7_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->mb_range8_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->mb_range9_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->mb_range10_low; ?>);

	highArray.push(<?php echo $this->masterData->mas_milk_price_config->mb_range1_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->mb_range2_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->mb_range3_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->mb_range4_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->mb_range5_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->mb_range6_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->mb_range7_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->mb_range8_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->mb_range9_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->mb_range10_high; ?>);

	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->mb_range1_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->mb_range2_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->mb_range3_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->mb_range4_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->mb_range5_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->mb_range6_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->mb_range7_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->mb_range8_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->mb_range9_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->mb_range10_change; ?>);

	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->scc_range1_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->scc_range2_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->scc_range3_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->scc_range4_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->scc_range5_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->scc_range6_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->scc_range7_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->scc_range8_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->scc_range9_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->scc_range10_low; ?>);

	highArray.push(<?php echo $this->masterData->mas_milk_price_config->scc_range1_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->scc_range2_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->scc_range3_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->scc_range4_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->scc_range5_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->scc_range6_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->scc_range7_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->scc_range8_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->scc_range9_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->scc_range10_high; ?>);

	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->scc_range1_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->scc_range2_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->scc_range3_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->scc_range4_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->scc_range5_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->scc_range6_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->scc_range7_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->scc_range8_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->scc_range9_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->scc_range10_change; ?>);

	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->fat_range1_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->fat_range2_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->fat_range3_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->fat_range4_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->fat_range5_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->fat_range6_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->fat_range7_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->fat_range8_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->fat_range9_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->fat_range10_low; ?>);

	highArray.push(<?php echo $this->masterData->mas_milk_price_config->fat_range1_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->fat_range2_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->fat_range3_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->fat_range4_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->fat_range5_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->fat_range6_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->fat_range7_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->fat_range8_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->fat_range9_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->fat_range10_high; ?>);

	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->fat_range1_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->fat_range2_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->fat_range3_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->fat_range4_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->fat_range5_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->fat_range6_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->fat_range7_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->fat_range8_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->fat_range9_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->fat_range10_change; ?>);

	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->snf_range1_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->snf_range2_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->snf_range3_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->snf_range4_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->snf_range5_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->snf_range6_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->snf_range7_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->snf_range8_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->snf_range9_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->snf_range10_low; ?>);

	highArray.push(<?php echo $this->masterData->mas_milk_price_config->snf_range1_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->snf_range2_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->snf_range3_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->snf_range4_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->snf_range5_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->snf_range6_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->snf_range7_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->snf_range8_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->snf_range9_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->snf_range10_high; ?>);

	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->snf_range1_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->snf_range2_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->snf_range3_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->snf_range4_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->snf_range5_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->snf_range6_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->snf_range7_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->snf_range8_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->snf_range9_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->snf_range10_change; ?>);

	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->ts_range1_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->ts_range2_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->ts_range3_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->ts_range4_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->ts_range5_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->ts_range6_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->ts_range7_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->ts_range8_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->ts_range9_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->ts_range10_low; ?>);

	highArray.push(<?php echo $this->masterData->mas_milk_price_config->ts_range1_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->ts_range2_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->ts_range3_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->ts_range4_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->ts_range5_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->ts_range6_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->ts_range7_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->ts_range8_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->ts_range9_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->ts_range10_high; ?>);

	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->ts_range1_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->ts_range2_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->ts_range3_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->ts_range4_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->ts_range5_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->ts_range6_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->ts_range7_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->ts_range8_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->ts_range9_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->ts_range10_change; ?>);


	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->protein_range1_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->protein_range2_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->protein_range3_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->protein_range4_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->protein_range5_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->protein_range6_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->protein_range7_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->protein_range8_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->protein_range9_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->protein_range10_low; ?>);

	highArray.push(<?php echo $this->masterData->mas_milk_price_config->protein_range1_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->protein_range2_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->protein_range3_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->protein_range4_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->protein_range5_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->protein_range6_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->protein_range7_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->protein_range8_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->protein_range9_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->protein_range10_high; ?>);

	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->protein_range1_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->protein_range2_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->protein_range3_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->protein_range4_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->protein_range5_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->protein_range6_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->protein_range7_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->protein_range8_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->protein_range9_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->protein_range10_change; ?>);

	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->lactose_range1_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->lactose_range2_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->lactose_range3_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->lactose_range4_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->lactose_range5_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->lactose_range6_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->lactose_range7_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->lactose_range8_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->lactose_range9_low; ?>);
	lowArray.push(<?php echo $this->masterData->mas_milk_price_config->lactose_range10_low; ?>);

	highArray.push(<?php echo $this->masterData->mas_milk_price_config->lactose_range1_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->lactose_range2_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->lactose_range3_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->lactose_range4_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->lactose_range5_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->lactose_range6_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->lactose_range7_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->lactose_range8_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->lactose_range9_high; ?>);
	highArray.push(<?php echo $this->masterData->mas_milk_price_config->lactose_range10_high; ?>);

	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->lactose_range1_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->lactose_range2_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->lactose_range3_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->lactose_range4_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->lactose_range5_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->lactose_range6_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->lactose_range7_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->lactose_range8_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->lactose_range9_change; ?>);
	changeArray.push(<?php echo $this->masterData->mas_milk_price_config->lactose_range10_change; ?>);

	console.log(lowArray);
	console.log(highArray);
	console.log(changeArray);

	jQuery("#jform_run_no").attr('readonly', 'readonly');
</script>



<!-- Modal -->
<div id="UploadFileModal" class="modal fade" role="dialog" style="display:none">
<form id="UploadFileForm" action="" method="post" enctype="multipart/form-data">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">อัพโหลดไฟล์คุณภาพน้ำนมดิบรายเกษตรกร</h4>
      </div>
      <div class="modal-body">
        <input type="file" name="upload_file" />
        (ไฟล์ Microsoft Excel ขนาดไม่เกิน 5 MB)

        <input type="hidden" name="milk_quality_id" value="<?php echo $_REQUEST['id'];?>" />
        <input type="hidden" name="option" value="<?php echo $_REQUEST['option'];?>" />
        <input type="hidden" name="task" value="milk_quality_header.importExcel" />
      </div>
      <div class="modal-footer" style="text-align:left">
      	<button type="submit" class="btn btn-info" style="width:100px">ตกลง</button>&nbsp;&nbsp;
        <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
      </div>
    </div>

  </div>
  </form>
</div>
