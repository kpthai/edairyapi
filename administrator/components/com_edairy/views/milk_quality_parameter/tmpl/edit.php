<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_edairy/css/form.css');
?>

<script src="../media/com_edairy/typeahead/bootstrap3-typeahead.js" type="text/javascript" charset="UTF-8"></script>

<script src="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript" charset="UTF-8"></script>

<link rel="stylesheet" href="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.css">

<style type="text/css">
	.panel {
    margin-bottom: 20px;
    background-color: #fff;
    border: 1px solid transparent;
    border-radius: 4px;
    -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
    box-shadow: 0 1px 1px rgba(0,0,0,.05);
}
.panel-default {
    border-color: #ddd;
}
.panel-group .panel {
    margin-bottom: 0;
    border-radius: 4px;
}
.panel-default>.panel-heading {
    color: #333;
    background-color: #f5f5f5;
    border-color: #ddd;
}
.panel-group .panel-heading {
    border-bottom: 0;
}
.panel-heading {
    padding: 5px 10px;
    border-bottom: 1px solid transparent;
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
}
.panel-body {
    padding: 15px;
}
.panel-group .panel-heading+.panel-collapse>.list-group, .panel-group .panel-heading+.panel-collapse>.panel-body {
    border-top: 1px solid #ddd;
}
.panel-default>.panel-heading+.panel-collapse>.panel-body {
    border-top-color: #ddd;
}
</style>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		js('.datetimepicker').datetimepicker({
            lang:'th',
            timepicker:false,
            format:'d/m/Y'
        });
	});

	Joomla.submitbutton = function (task) {
		if (task == 'milk_quality_parameter.cancel') {
			Joomla.submitform(task, document.getElementById('milk_quality_parameter-form'));
		}
		else {
			
			if (task != 'milk_quality_parameter.cancel' && document.formvalidator.isValid(document.id('milk_quality_parameter-form'))) {
				
				Joomla.submitform(task, document.getElementById('milk_quality_parameter-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_edairy&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="milk_quality_parameter-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_EDAIRY_TITLE_MILK_QUALITY_PARAMETER', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

									<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
				<table>
										<tr>
											<td>
				<?php echo $this->form->renderField('run_no'); ?>
											</td>
											<td>
												&nbsp;&nbsp;
											</td>
											<td>
				<?php // echo $this->form->renderField('create_date'); ?>
												<div class="control-group">
													<div class="control-label">
														<label>
															วันที่
														</label>
													</div>
													<div class="controls">
														<input type="text" name="jform[create_date]" class="datetimepicker"  value="<?php echo formatDate($this->item->create_date); ?>"/>
													</div>
												</div>
											</td>
										</tr>
									</table>


									<table>
										<tr>
											<td>
				<?php echo $this->form->renderField('tax'); ?>
											</td>
											<td>
												&nbsp;&nbsp;
											</td>
											<td>
				<?php echo $this->form->renderField('milk_standard_price'); ?>
											</td>
										</tr>
									</table>
				
				
				
				



				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          คะแนนคอก (คลิกหัวข้อเพื่อเปิดหรือปิด)
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        		<?php echo $this->form->renderField('score1_up_price'); ?>
				<?php echo $this->form->renderField('score2_up_price'); ?>
				<?php echo $this->form->renderField('score3_up_price'); ?>
				<?php echo $this->form->renderField('score4_up_price'); ?>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingTwo">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          มาตรฐาน GAP
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body">
        	<?php echo $this->form->renderField('gap_up_price'); ?>
				<?php echo $this->form->renderField('no_gap_up_price'); ?>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          MB (hrs.)
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
        <table cellpadding="5" border="1">
        	<tr>
        		<th>ช่วง</th>
        		<th>ค่าเริ่มจาก</th>
        		<th>ถึง</th>
        		<th>ราคาเพิ่ม/ลด (บาท/กก.)</th>
        	</tr>
        	<tr>
        		<th>1</th>
        		<th>
        			<input type="text" name="mb_range1_low" style="width:150px" value="<?php echo $this->item->parameter_config->mb_range1_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="mb_range1_high" style="width:150px" value="<?php echo $this->item->parameter_config->mb_range1_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="mb_range1_change" style="width:100px" value="<?php echo $this->item->parameter_config->mb_range1_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>2</th>
        		<th>
        			<input type="text" name="mb_range2_low" style="width:150px" value="<?php echo $this->item->parameter_config->mb_range2_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="mb_range2_high" style="width:150px" value="<?php echo $this->item->parameter_config->mb_range2_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="mb_range2_change" style="width:100px" value="<?php echo $this->item->parameter_config->mb_range2_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>3</th>
        		<th>
        			<input type="text" name="mb_range3_low" style="width:150px" value="<?php echo $this->item->parameter_config->mb_range3_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="mb_range3_high" style="width:150px" value="<?php echo $this->item->parameter_config->mb_range3_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="mb_range3_change" style="width:100px" value="<?php echo $this->item->parameter_config->mb_range3_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>4</th>
        		<th>
        			<input type="text" name="mb_range4_low" style="width:150px" value="<?php echo $this->item->parameter_config->mb_range4_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="mb_range4_high" style="width:150px" value="<?php echo $this->item->parameter_config->mb_range4_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="mb_range4_change" style="width:100px" value="<?php echo $this->item->parameter_config->mb_range4_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>5</th>
        		<th>
        			<input type="text" name="mb_range5_low" style="width:150px" value="<?php echo $this->item->parameter_config->mb_range5_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="mb_range5_high" style="width:150px" value="<?php echo $this->item->parameter_config->mb_range5_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="mb_range5_change" style="width:100px" value="<?php echo $this->item->parameter_config->mb_range5_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>6</th>
        		<th>
        			<input type="text" name="mb_range6_low" style="width:150px" value="<?php echo $this->item->parameter_config->mb_range6_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="mb_range6_high" style="width:150px" value="<?php echo $this->item->parameter_config->mb_range6_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="mb_range6_change" style="width:100px" value="<?php echo $this->item->parameter_config->mb_range6_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>7</th>
        		<th>
        			<input type="text" name="mb_range7_low" style="width:150px" value="<?php echo $this->item->parameter_config->mb_range7_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="mb_range7_high" style="width:150px" value="<?php echo $this->item->parameter_config->mb_range7_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="mb_range7_change" style="width:100px" value="<?php echo $this->item->parameter_config->mb_range7_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>8</th>
        		<th>
        			<input type="text" name="mb_range8_low" style="width:150px" value="<?php echo $this->item->parameter_config->mb_range8_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="mb_range8_high" style="width:150px" value="<?php echo $this->item->parameter_config->mb_range8_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="mb_range8_change" style="width:100px" value="<?php echo $this->item->parameter_config->mb_range8_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>9</th>
        		<th>
        			<input type="text" name="mb_range9_low" style="width:150px" value="<?php echo $this->item->parameter_config->mb_range9_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="mb_range9_high" style="width:150px" value="<?php echo $this->item->parameter_config->mb_range9_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="mb_range9_change" style="width:100px" value="<?php echo $this->item->parameter_config->mb_range9_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>10</th>
        		<th>
        			<input type="text" name="mb_range10_low" style="width:150px" value="<?php echo $this->item->parameter_config->mb_range10_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="mb_range10_high" style="width:150px" value="<?php echo $this->item->parameter_config->mb_range10_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="mb_range10_change" style="width:100px" value="<?php echo $this->item->parameter_config->mb_range10_change; ?>"/>
        		</th>
        	</tr>
        </table>

      
      </div>
    </div>
  </div>



  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingFour">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
          SCC (Cell/ml.)
        </a>
      </h4>
    </div>
    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
      <div class="panel-body">
        <table cellpadding="5" border="1">
        	<tr>
        		<th>ช่วง</th>
        		<th>ค่าเริ่มจาก</th>
        		<th>ถึง</th>
        		<th>ราคาเพิ่ม/ลด (บาท/กก.)</th>
        	</tr>
        	<tr>
        		<th>1</th>
        		<th>
        			<input type="text" name="scc_range1_low" style="width:150px" value="<?php echo $this->item->parameter_config->scc_range1_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="scc_range1_high" style="width:150px" value="<?php echo $this->item->parameter_config->scc_range1_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="scc_range1_change" style="width:100px" value="<?php echo $this->item->parameter_config->scc_range1_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>2</th>
        		<th>
        			<input type="text" name="scc_range2_low" style="width:150px" value="<?php echo $this->item->parameter_config->scc_range2_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="scc_range2_high" style="width:150px" value="<?php echo $this->item->parameter_config->scc_range2_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="scc_range2_change" style="width:100px" value="<?php echo $this->item->parameter_config->scc_range2_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>3</th>
        		<th>
        			<input type="text" name="scc_range3_low" style="width:150px" value="<?php echo $this->item->parameter_config->scc_range3_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="scc_range3_high" style="width:150px" value="<?php echo $this->item->parameter_config->scc_range3_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="scc_range3_change" style="width:100px" value="<?php echo $this->item->parameter_config->scc_range3_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>4</th>
        		<th>
        			<input type="text" name="scc_range4_low" style="width:150px" value="<?php echo $this->item->parameter_config->scc_range4_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="scc_range4_high" style="width:150px" value="<?php echo $this->item->parameter_config->scc_range4_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="scc_range4_change" style="width:100px" value="<?php echo $this->item->parameter_config->scc_range4_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>5</th>
        		<th>
        			<input type="text" name="scc_range5_low" style="width:150px" value="<?php echo $this->item->parameter_config->scc_range5_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="scc_range5_high" style="width:150px" value="<?php echo $this->item->parameter_config->scc_range5_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="scc_range5_change" style="width:100px" value="<?php echo $this->item->parameter_config->scc_range5_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>6</th>
        		<th>
        			<input type="text" name="scc_range6_low" style="width:150px" value="<?php echo $this->item->parameter_config->scc_range6_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="scc_range6_high" style="width:150px" value="<?php echo $this->item->parameter_config->scc_range6_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="scc_range6_change" style="width:100px" value="<?php echo $this->item->parameter_config->scc_range6_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>7</th>
        		<th>
        			<input type="text" name="scc_range7_low" style="width:150px" value="<?php echo $this->item->parameter_config->scc_range7_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="scc_range7_high" style="width:150px" value="<?php echo $this->item->parameter_config->scc_range7_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="scc_range7_change" style="width:100px" value="<?php echo $this->item->parameter_config->scc_range7_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>8</th>
        		<th>
        			<input type="text" name="scc_range8_low" style="width:150px" value="<?php echo $this->item->parameter_config->scc_range8_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="scc_range8_high" style="width:150px" value="<?php echo $this->item->parameter_config->scc_range8_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="scc_range8_change" style="width:100px" value="<?php echo $this->item->parameter_config->scc_range8_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>9</th>
        		<th>
        			<input type="text" name="scc_range9_low" style="width:150px" value="<?php echo $this->item->parameter_config->scc_range9_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="scc_range9_high" style="width:150px" value="<?php echo $this->item->parameter_config->scc_range9_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="scc_range9_change" style="width:100px" value="<?php echo $this->item->parameter_config->scc_range9_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>10</th>
        		<th>
        			<input type="text" name="scc_range10_low" style="width:150px" value="<?php echo $this->item->parameter_config->scc_range10_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="scc_range10_high" style="width:150px" value="<?php echo $this->item->parameter_config->scc_range10_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="scc_range10_change" style="width:100px" value="<?php echo $this->item->parameter_config->scc_range10_change; ?>"/>
        		</th>
        	</tr>
        </table>

      
      </div>
    </div>
  </div>





  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingFive">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
          FAT (%)
        </a>
      </h4>
    </div>
    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
      <div class="panel-body">
        <table cellpadding="5" border="1">
        	<tr>
        		<th>ช่วง</th>
        		<th>ค่าเริ่มจาก</th>
        		<th>ถึง</th>
        		<th>ราคาเพิ่ม/ลด (บาท/กก.)</th>
        	</tr>
        	<tr>
        		<th>1</th>
        		<th>
        			<input type="text" name="fat_range1_low" style="width:150px" value="<?php echo $this->item->parameter_config->fat_range1_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="fat_range1_high" style="width:150px" value="<?php echo $this->item->parameter_config->fat_range1_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="fat_range1_change" style="width:100px" value="<?php echo $this->item->parameter_config->fat_range1_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>2</th>
        		<th>
        			<input type="text" name="fat_range2_low" style="width:150px" value="<?php echo $this->item->parameter_config->fat_range2_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="fat_range2_high" style="width:150px" value="<?php echo $this->item->parameter_config->fat_range2_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="fat_range2_change" style="width:100px" value="<?php echo $this->item->parameter_config->fat_range2_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>3</th>
        		<th>
        			<input type="text" name="fat_range3_low" style="width:150px" value="<?php echo $this->item->parameter_config->fat_range3_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="fat_range3_high" style="width:150px" value="<?php echo $this->item->parameter_config->fat_range3_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="fat_range3_change" style="width:100px" value="<?php echo $this->item->parameter_config->fat_range3_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>4</th>
        		<th>
        			<input type="text" name="fat_range4_low" style="width:150px" value="<?php echo $this->item->parameter_config->fat_range4_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="fat_range4_high" style="width:150px" value="<?php echo $this->item->parameter_config->fat_range4_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="fat_range4_change" style="width:100px" value="<?php echo $this->item->parameter_config->fat_range4_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>5</th>
        		<th>
        			<input type="text" name="fat_range5_low" style="width:150px" value="<?php echo $this->item->parameter_config->fat_range5_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="fat_range5_high" style="width:150px" value="<?php echo $this->item->parameter_config->fat_range5_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="fat_range5_change" style="width:100px" value="<?php echo $this->item->parameter_config->fat_range5_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>6</th>
        		<th>
        			<input type="text" name="fat_range6_low" style="width:150px" value="<?php echo $this->item->parameter_config->fat_range6_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="fat_range6_high" style="width:150px" value="<?php echo $this->item->parameter_config->fat_range6_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="fat_range6_change" style="width:100px" value="<?php echo $this->item->parameter_config->fat_range6_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>7</th>
        		<th>
        			<input type="text" name="fat_range7_low" style="width:150px" value="<?php echo $this->item->parameter_config->fat_range7_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="fat_range7_high" style="width:150px" value="<?php echo $this->item->parameter_config->fat_range7_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="fat_range7_change" style="width:100px" value="<?php echo $this->item->parameter_config->fat_range7_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>8</th>
        		<th>
        			<input type="text" name="fat_range8_low" style="width:150px" value="<?php echo $this->item->parameter_config->fat_range8_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="fat_range8_high" style="width:150px" value="<?php echo $this->item->parameter_config->fat_range8_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="fat_range8_change" style="width:100px" value="<?php echo $this->item->parameter_config->fat_range8_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>9</th>
        		<th>
        			<input type="text" name="fat_range9_low" style="width:150px" value="<?php echo $this->item->parameter_config->fat_range9_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="fat_range9_high" style="width:150px" value="<?php echo $this->item->parameter_config->fat_range9_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="fat_range9_change" style="width:100px" value="<?php echo $this->item->parameter_config->fat_range9_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>10</th>
        		<th>
        			<input type="text" name="fat_range10_low" style="width:150px" value="<?php echo $this->item->parameter_config->fat_range10_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="fat_range10_high" style="width:150px" value="<?php echo $this->item->parameter_config->fat_range10_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="fat_range10_change" style="width:100px" value="<?php echo $this->item->parameter_config->fat_range10_change; ?>"/>
        		</th>
        	</tr>
        </table>

      
      </div>
    </div>
  </div>




  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingSix">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
          SNF (%)
        </a>
      </h4>
    </div>
    <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
      <div class="panel-body">
        <table cellpadding="5" border="1">
        	<tr>
        		<th>ช่วง</th>
        		<th>ค่าเริ่มจาก</th>
        		<th>ถึง</th>
        		<th>ราคาเพิ่ม/ลด (บาท/กก.)</th>
        	</tr>
        	<tr>
        		<th>1</th>
        		<th>
        			<input type="text" name="snf_range1_low" style="width:150px" value="<?php echo $this->item->parameter_config->snf_range1_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="snf_range1_high" style="width:150px" value="<?php echo $this->item->parameter_config->snf_range1_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="snf_range1_change" style="width:100px" value="<?php echo $this->item->parameter_config->snf_range1_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>2</th>
        		<th>
        			<input type="text" name="snf_range2_low" style="width:150px" value="<?php echo $this->item->parameter_config->snf_range2_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="snf_range2_high" style="width:150px" value="<?php echo $this->item->parameter_config->snf_range2_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="snf_range2_change" style="width:100px" value="<?php echo $this->item->parameter_config->snf_range2_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>3</th>
        		<th>
        			<input type="text" name="snf_range3_low" style="width:150px" value="<?php echo $this->item->parameter_config->snf_range3_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="snf_range3_high" style="width:150px" value="<?php echo $this->item->parameter_config->snf_range3_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="snf_range3_change" style="width:100px" value="<?php echo $this->item->parameter_config->snf_range3_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>4</th>
        		<th>
        			<input type="text" name="snf_range4_low" style="width:150px" value="<?php echo $this->item->parameter_config->snf_range4_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="snf_range4_high" style="width:150px" value="<?php echo $this->item->parameter_config->snf_range4_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="snf_range4_change" style="width:100px" value="<?php echo $this->item->parameter_config->snf_range4_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>5</th>
        		<th>
        			<input type="text" name="snf_range5_low" style="width:150px" value="<?php echo $this->item->parameter_config->snf_range5_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="snf_range5_high" style="width:150px" value="<?php echo $this->item->parameter_config->snf_range5_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="snf_range5_change" style="width:100px" value="<?php echo $this->item->parameter_config->snf_range5_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>6</th>
        		<th>
        			<input type="text" name="snf_range6_low" style="width:150px" value="<?php echo $this->item->parameter_config->snf_range6_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="snf_range6_high" style="width:150px" value="<?php echo $this->item->parameter_config->snf_range6_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="snf_range6_change" style="width:100px" value="<?php echo $this->item->parameter_config->snf_range6_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>7</th>
        		<th>
        			<input type="text" name="snf_range7_low" style="width:150px" value="<?php echo $this->item->parameter_config->snf_range7_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="snf_range7_high" style="width:150px" value="<?php echo $this->item->parameter_config->snf_range7_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="snf_range7_change" style="width:100px" value="<?php echo $this->item->parameter_config->snf_range7_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>8</th>
        		<th>
        			<input type="text" name="snf_range8_low" style="width:150px" value="<?php echo $this->item->parameter_config->snf_range8_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="snf_range8_high" style="width:150px" value="<?php echo $this->item->parameter_config->snf_range8_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="snf_range8_change" style="width:100px" value="<?php echo $this->item->parameter_config->snf_range8_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>9</th>
        		<th>
        			<input type="text" name="snf_range9_low" style="width:150px" value="<?php echo $this->item->parameter_config->snf_range9_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="snf_range9_high" style="width:150px" value="<?php echo $this->item->parameter_config->snf_range9_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="snf_range9_change" style="width:100px" value="<?php echo $this->item->parameter_config->snf_range9_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>10</th>
        		<th>
        			<input type="text" name="snf_range10_low" style="width:150px" value="<?php echo $this->item->parameter_config->snf_range10_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="snf_range10_high" style="width:150px" value="<?php echo $this->item->parameter_config->snf_range10_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="snf_range10_change" style="width:100px" value="<?php echo $this->item->parameter_config->snf_range10_change; ?>"/>
        		</th>
        	</tr>
        </table>

      
      </div>
    </div>
  </div>



  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingSeven">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
          TS (%)
        </a>
      </h4>
    </div>
    <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
      <div class="panel-body">
        <table cellpadding="5" border="1">
        	<tr>
        		<th>ช่วง</th>
        		<th>ค่าเริ่มจาก</th>
        		<th>ถึง</th>
        		<th>ราคาเพิ่ม/ลด (บาท/กก.)</th>
        	</tr>
        	<tr>
        		<th>1</th>
        		<th>
        			<input type="text" name="ts_range1_low" style="width:150px" value="<?php echo $this->item->parameter_config->ts_range1_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="ts_range1_high" style="width:150px" value="<?php echo $this->item->parameter_config->ts_range1_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="ts_range1_change" style="width:100px" value="<?php echo $this->item->parameter_config->ts_range1_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>2</th>
        		<th>
        			<input type="text" name="ts_range2_low" style="width:150px" value="<?php echo $this->item->parameter_config->ts_range2_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="ts_range2_high" style="width:150px" value="<?php echo $this->item->parameter_config->ts_range2_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="ts_range2_change" style="width:100px" value="<?php echo $this->item->parameter_config->ts_range2_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>3</th>
        		<th>
        			<input type="text" name="ts_range3_low" style="width:150px" value="<?php echo $this->item->parameter_config->ts_range3_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="ts_range3_high" style="width:150px" value="<?php echo $this->item->parameter_config->ts_range3_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="ts_range3_change" style="width:100px" value="<?php echo $this->item->parameter_config->ts_range3_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>4</th>
        		<th>
        			<input type="text" name="ts_range4_low" style="width:150px" value="<?php echo $this->item->parameter_config->ts_range4_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="ts_range4_high" style="width:150px" value="<?php echo $this->item->parameter_config->ts_range4_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="ts_range4_change" style="width:100px" value="<?php echo $this->item->parameter_config->ts_range4_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>5</th>
        		<th>
        			<input type="text" name="ts_range5_low" style="width:150px" value="<?php echo $this->item->parameter_config->ts_range5_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="ts_range5_high" style="width:150px" value="<?php echo $this->item->parameter_config->ts_range5_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="ts_range5_change" style="width:100px" value="<?php echo $this->item->parameter_config->ts_range5_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>6</th>
        		<th>
        			<input type="text" name="ts_range6_low" style="width:150px" value="<?php echo $this->item->parameter_config->ts_range6_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="ts_range6_high" style="width:150px" value="<?php echo $this->item->parameter_config->ts_range6_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="ts_range6_change" style="width:100px" value="<?php echo $this->item->parameter_config->ts_range6_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>7</th>
        		<th>
        			<input type="text" name="ts_range7_low" style="width:150px" value="<?php echo $this->item->parameter_config->ts_range7_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="ts_range7_high" style="width:150px" value="<?php echo $this->item->parameter_config->ts_range7_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="ts_range7_change" style="width:100px" value="<?php echo $this->item->parameter_config->ts_range7_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>8</th>
        		<th>
        			<input type="text" name="ts_range8_low" style="width:150px" value="<?php echo $this->item->parameter_config->ts_range8_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="ts_range8_high" style="width:150px" value="<?php echo $this->item->parameter_config->ts_range8_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="ts_range8_change" style="width:100px" value="<?php echo $this->item->parameter_config->ts_range8_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>9</th>
        		<th>
        			<input type="text" name="ts_range9_low" style="width:150px" value="<?php echo $this->item->parameter_config->ts_range9_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="ts_range9_high" style="width:150px" value="<?php echo $this->item->parameter_config->ts_range9_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="ts_range9_change" style="width:100px" value="<?php echo $this->item->parameter_config->ts_range9_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>10</th>
        		<th>
        			<input type="text" name="ts_range10_low" style="width:150px" value="<?php echo $this->item->parameter_config->ts_range10_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="ts_range10_high" style="width:150px" value="<?php echo $this->item->parameter_config->ts_range10_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="ts_range10_change" style="width:100px" value="<?php echo $this->item->parameter_config->ts_range10_change; ?>"/>
        		</th>
        	</tr>
        </table>

      
      </div>
    </div>
  </div>


  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingEight">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
          Protein (%)
        </a>
      </h4>
    </div>
    <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
      <div class="panel-body">
        <table cellpadding="5" border="1">
        	<tr>
        		<th>ช่วง</th>
        		<th>ค่าเริ่มจาก</th>
        		<th>ถึง</th>
        		<th>ราคาเพิ่ม/ลด (บาท/กก.)</th>
        	</tr>
        	<tr>
        		<th>1</th>
        		<th>
        			<input type="text" name="protein_range1_low" style="width:150px" value="<?php echo $this->item->parameter_config->protein_range1_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="protein_range1_high" style="width:150px" value="<?php echo $this->item->parameter_config->protein_range1_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="protein_range1_change" style="width:100px" value="<?php echo $this->item->parameter_config->protein_range1_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>2</th>
        		<th>
        			<input type="text" name="protein_range2_low" style="width:150px" value="<?php echo $this->item->parameter_config->protein_range2_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="protein_range2_high" style="width:150px" value="<?php echo $this->item->parameter_config->protein_range2_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="protein_range2_change" style="width:100px" value="<?php echo $this->item->parameter_config->protein_range2_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>3</th>
        		<th>
        			<input type="text" name="protein_range3_low" style="width:150px" value="<?php echo $this->item->parameter_config->protein_range3_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="protein_range3_high" style="width:150px" value="<?php echo $this->item->parameter_config->protein_range3_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="protein_range3_change" style="width:100px" value="<?php echo $this->item->parameter_config->protein_range3_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>4</th>
        		<th>
        			<input type="text" name="protein_range4_low" style="width:150px" value="<?php echo $this->item->parameter_config->protein_range4_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="protein_range4_high" style="width:150px" value="<?php echo $this->item->parameter_config->protein_range4_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="protein_range4_change" style="width:100px" value="<?php echo $this->item->parameter_config->protein_range4_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>5</th>
        		<th>
        			<input type="text" name="protein_range5_low" style="width:150px" value="<?php echo $this->item->parameter_config->protein_range5_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="protein_range5_high" style="width:150px" value="<?php echo $this->item->parameter_config->protein_range5_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="protein_range5_change" style="width:100px" value="<?php echo $this->item->parameter_config->protein_range5_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>6</th>
        		<th>
        			<input type="text" name="protein_range6_low" style="width:150px" value="<?php echo $this->item->parameter_config->protein_range6_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="protein_range6_high" style="width:150px" value="<?php echo $this->item->parameter_config->protein_range6_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="protein_range6_change" style="width:100px" value="<?php echo $this->item->parameter_config->protein_range6_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>7</th>
        		<th>
        			<input type="text" name="protein_range7_low" style="width:150px" value="<?php echo $this->item->parameter_config->protein_range7_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="protein_range7_high" style="width:150px" value="<?php echo $this->item->parameter_config->protein_range7_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="protein_range7_change" style="width:100px" value="<?php echo $this->item->parameter_config->protein_range7_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>8</th>
        		<th>
        			<input type="text" name="protein_range8_low" style="width:150px" value="<?php echo $this->item->parameter_config->protein_range8_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="protein_range8_high" style="width:150px" value="<?php echo $this->item->parameter_config->protein_range8_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="protein_range8_change" style="width:100px" value="<?php echo $this->item->parameter_config->protein_range8_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>9</th>
        		<th>
        			<input type="text" name="protein_range9_low" style="width:150px" value="<?php echo $this->item->parameter_config->protein_range9_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="protein_range9_high" style="width:150px" value="<?php echo $this->item->parameter_config->protein_range9_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="protein_range9_change" style="width:100px" value="<?php echo $this->item->parameter_config->protein_range9_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>10</th>
        		<th>
        			<input type="text" name="protein_range10_low" style="width:150px" value="<?php echo $this->item->parameter_config->protein_range10_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="protein_range10_high" style="width:150px" value="<?php echo $this->item->parameter_config->protein_range10_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="protein_range10_change" style="width:100px" value="<?php echo $this->item->parameter_config->protein_range10_change; ?>"/>
        		</th>
        	</tr>
        </table>

      
      </div>
    </div>
  </div>


  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingNine">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
          Lactose (%)
        </a>
      </h4>
    </div>
    <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
      <div class="panel-body">
        <table cellpadding="5" border="1">
        	<tr>
        		<th>ช่วง</th>
        		<th>ค่าเริ่มจาก</th>
        		<th>ถึง</th>
        		<th>ราคาเพิ่ม/ลด (บาท/กก.)</th>
        	</tr>
        	<tr>
        		<th>1</th>
        		<th>
        			<input type="text" name="lactose_range1_low" style="width:150px" value="<?php echo $this->item->parameter_config->lactose_range1_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="lactose_range1_high" style="width:150px" value="<?php echo $this->item->parameter_config->lactose_range1_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="lactose_range1_change" style="width:100px" value="<?php echo $this->item->parameter_config->lactose_range1_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>2</th>
        		<th>
        			<input type="text" name="lactose_range2_low" style="width:150px" value="<?php echo $this->item->parameter_config->lactose_range2_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="lactose_range2_high" style="width:150px" value="<?php echo $this->item->parameter_config->lactose_range2_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="lactose_range2_change" style="width:100px" value="<?php echo $this->item->parameter_config->lactose_range2_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>3</th>
        		<th>
        			<input type="text" name="lactose_range3_low" style="width:150px" value="<?php echo $this->item->parameter_config->lactose_range3_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="lactose_range3_high" style="width:150px" value="<?php echo $this->item->parameter_config->lactose_range3_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="lactose_range3_change" style="width:100px" value="<?php echo $this->item->parameter_config->lactose_range3_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>4</th>
        		<th>
        			<input type="text" name="lactose_range4_low" style="width:150px" value="<?php echo $this->item->parameter_config->lactose_range4_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="lactose_range4_high" style="width:150px" value="<?php echo $this->item->parameter_config->lactose_range4_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="lactose_range4_change" style="width:100px" value="<?php echo $this->item->parameter_config->lactose_range4_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>5</th>
        		<th>
        			<input type="text" name="lactose_range5_low" style="width:150px" value="<?php echo $this->item->parameter_config->lactose_range5_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="lactose_range5_high" style="width:150px" value="<?php echo $this->item->parameter_config->lactose_range5_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="lactose_range5_change" style="width:100px" value="<?php echo $this->item->parameter_config->lactose_range5_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>6</th>
        		<th>
        			<input type="text" name="lactose_range6_low" style="width:150px" value="<?php echo $this->item->parameter_config->lactose_range6_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="lactose_range6_high" style="width:150px" value="<?php echo $this->item->parameter_config->lactose_range6_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="lactose_range6_change" style="width:100px" value="<?php echo $this->item->parameter_config->lactose_range6_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>7</th>
        		<th>
        			<input type="text" name="lactose_range7_low" style="width:150px" value="<?php echo $this->item->parameter_config->lactose_range7_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="lactose_range7_high" style="width:150px" value="<?php echo $this->item->parameter_config->lactose_range7_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="lactose_range7_change" style="width:100px" value="<?php echo $this->item->parameter_config->lactose_range7_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>8</th>
        		<th>
        			<input type="text" name="lactose_range8_low" style="width:150px" value="<?php echo $this->item->parameter_config->lactose_range8_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="lactose_range8_high" style="width:150px" value="<?php echo $this->item->parameter_config->lactose_range8_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="lactose_range8_change" style="width:100px" value="<?php echo $this->item->parameter_config->lactose_range8_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>9</th>
        		<th>
        			<input type="text" name="lactose_range9_low" style="width:150px" value="<?php echo $this->item->parameter_config->lactose_range9_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="lactose_range9_high" style="width:150px" value="<?php echo $this->item->parameter_config->lactose_range9_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="lactose_range9_change" style="width:100px" value="<?php echo $this->item->parameter_config->lactose_range9_change; ?>"/>
        		</th>
        	</tr>
        	<tr>
        		<th>10</th>
        		<th>
        			<input type="text" name="lactose_range10_low" style="width:150px" value="<?php echo $this->item->parameter_config->lactose_range10_low; ?>" />
        		</th>
        		<th>
        			<input type="text" name="lactose_range10_high" style="width:150px" value="<?php echo $this->item->parameter_config->lactose_range10_high; ?>" />
        		</th>
        		<th>
        			<input type="text" name="lactose_range10_change" style="width:100px" value="<?php echo $this->item->parameter_config->lactose_range10_change; ?>"/>
        		</th>
        	</tr>
        </table>

      
      </div>
    </div>
  </div>


</div>
<div style="clear:both"></div>
<br /><br />


				<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />

				<div style="display:none">
					<?php echo $this->form->renderField('state'); ?>
				</div>

				<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
				<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />
				<?php echo $this->form->renderField('created_by'); ?>

				<?php echo $this->form->renderField('modified_by'); ?>

					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
