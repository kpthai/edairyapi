<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_edairy/css/form.css');

if($this->item->create_date==""){
	$this->item->create_date = date("Y-m-d");
}

?>
<script src="../media/com_edairy/typeahead/bootstrap3-typeahead.js" type="text/javascript" charset="UTF-8"></script>

<script src="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript" charset="UTF-8"></script>

<link rel="stylesheet" href="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.css">


<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		js('.datetimepicker').datetimepicker({
            lang:'th',
            timepicker:false,
            format:'d/m/Y'
        });
		
	js('input:hidden.coop_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('coop_idhidden')){
			js('#jform_coop_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_coop_id").trigger("liszt:updated");
	});

	Joomla.submitbutton = function (task) {
		if (task == 'milk_coop_quality.cancel') {
			Joomla.submitform(task, document.getElementById('milk_coop_quality-form'));
		}
		else {
			
			if (task != 'milk_coop_quality.cancel' && document.formvalidator.isValid(document.id('milk_coop_quality-form'))) {
				
				Joomla.submitform(task, document.getElementById('milk_coop_quality-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_edairy&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="milk_coop_quality-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_EDAIRY_TITLE_MILK_COOP_QUALITY', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

									<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
				<?php echo $this->form->renderField('run_no'); ?>

				<table>
						<tr>
							<td width="450">
								<div class="control-group">
									<div class="control-label">
										<label>
											วันที่ตรวจคุณภาพ
										</label>
									</div>
									<div class="controls">
										<input type="text" name="jform[create_date]" class="datetimepicker"  value="<?php echo formatDate($this->item->create_date); ?>"/>
									</div>
								</div>
							</td>
							<td>
								<?php echo $this->form->renderField('create_time'); ?>
							</td>
						</tr>
						<tr>
							<td width="450">
								<?php echo $this->form->renderField('coop_id'); ?>

									<?php
										foreach((array)$this->item->coop_id as $value): 
											if(!is_array($value)):
												echo '<input type="hidden" class="coop_id" name="jform[coop_idhidden]['.$value.']" value="'.$value.'" />';
											endif;
										endforeach;
									?>
							</td>
							<td>
								<?php echo $this->form->renderField('license_plate'); ?>
							</td>
						</tr>
				</table>


				<b>ผลการตรวจวิเคราะห์</b>
				<hr />
				
								
				<?php echo $this->form->renderField('mb'); ?>
				<?php echo $this->form->renderField('scc'); ?>
				<?php echo $this->form->renderField('fat'); ?>
				<?php echo $this->form->renderField('snf'); ?>
				<?php echo $this->form->renderField('ts'); ?>
				<?php echo $this->form->renderField('protein'); ?>
				<?php echo $this->form->renderField('lactose'); ?>
				<?php echo $this->form->renderField('price'); ?>
				<?php echo $this->form->renderField('amount'); ?>
				<?php echo $this->form->renderField('total_price'); ?>


				<?php echo $this->form->renderField('upload_file'); ?>

				<?php if (!empty($this->item->upload_file)) : ?>
					<?php foreach ((array)$this->item->upload_file as $fileSingle) : ?>
						<?php if (!is_array($fileSingle)) : ?>
							<a href="<?php echo JRoute::_(JUri::root() . 'upload' . DIRECTORY_SEPARATOR . $fileSingle, false);?>"><input class="btn btn-success" type="button" value="เรียกดูไฟล์ <?php echo $fileSingle; ?>" /></a> &nbsp; 
						<?php endif; ?>
					<?php endforeach; ?>
				<?php endif; ?>
				<input type="hidden" name="jform[upload_file_hidden]" id="jform_upload_file_hidden" value="<?php echo implode(',', (array)$this->item->upload_file); ?>" />				<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
				<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
				<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
				<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />

				<?php echo $this->form->renderField('created_by'); ?>
				<?php echo $this->form->renderField('modified_by'); ?>

					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>

<?php
if($this->item->create_time=="" || $this->item->create_time=="00:00:00"){
?>
<script type="text/javascript">
	jQuery("#jform_create_time").val('<?php echo date("H:i:s"); ?>');
</script>
<?php

}
?>

<script type="text/javascript">
	function calculateSumPrice(){
		if(jQuery("#jform_price").val()!="" && jQuery("#jform_amount").val()!=""){
			jQuery("#jform_total_price").val(jQuery("#jform_price").val()*jQuery("#jform_amount").val());
		}
		
	}

	jQuery("#jform_price").change(function() {
	  calculateSumPrice();
	});
	jQuery("#jform_amount").change(function() {
	  calculateSumPrice();
	});

	jQuery("#jform_total_price").attr('readonly', 'readonly');
	jQuery("#jform_run_no").attr('readonly', 'readonly');


</script>