<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_edairy/css/form.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	js('input:hidden.cow_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('cow_idhidden')){
			js('#jform_cow_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_cow_id").trigger("liszt:updated");
	js('input:hidden.from_farm_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('from_farm_idhidden')){
			js('#jform_from_farm_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_from_farm_id").trigger("liszt:updated");
	js('input:hidden.to_farm_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('to_farm_idhidden')){
			js('#jform_to_farm_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_to_farm_id").trigger("liszt:updated");
	});

	Joomla.submitbutton = function (task) {
		if (task == 'move_out.cancel') {
			Joomla.submitform(task, document.getElementById('move_out-form'));
		}
		else {
			
			if (task != 'move_out.cancel' && document.formvalidator.isValid(document.id('move_out-form'))) {
				
				Joomla.submitform(task, document.getElementById('move_out-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_edairy&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="move_out-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_EDAIRY_TITLE_MOVE_OUT', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

									<?php echo $this->form->renderField('id'); ?>
				<?php echo $this->form->renderField('cow_id'); ?>

			<?php
				foreach((array)$this->item->cow_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="cow_id" name="jform[cow_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('move_out_date'); ?>
				<?php echo $this->form->renderField('move_type'); ?>
				<?php echo $this->form->renderField('from_farm_id'); ?>

			<?php
				foreach((array)$this->item->from_farm_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="from_farm_id" name="jform[from_farm_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('to_farm_id'); ?>

			<?php
				foreach((array)$this->item->to_farm_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="to_farm_id" name="jform[to_farm_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('move_objective'); ?>
				<?php echo $this->form->renderField('move_reason'); ?>
				<?php echo $this->form->renderField('create_by'); ?>
				<?php echo $this->form->renderField('update_by'); ?>
				<?php echo $this->form->renderField('create_date'); ?>
				<?php echo $this->form->renderField('update_date'); ?>
				<?php echo $this->form->renderField('state'); ?>
				<?php echo $this->form->renderField('created_by'); ?>


					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
