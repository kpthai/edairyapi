<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_edairy/css/form.css');

if($this->item->create_date == ""){
	$this->item->create_date = date("Y-m-d");
}

if($_REQUEST["create_date"]!=""){
	$this->item->create_date = $_REQUEST["create_date"];
}
?>

<script src="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript" charset="UTF-8"></script>

<link rel="stylesheet" href="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.css">


<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		js('.datetimepicker').datetimepicker({
            lang:'th',
            timepicker:false,
            format:'d/m/Y'
        });
	js('input:hidden.cow_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('cow_idhidden')){
			js('#jform_cow_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_cow_id").trigger("liszt:updated");
	});

	Joomla.submitbutton = function (task) {
		if (task == 'receive_milk_quantity_cow.cancel') {
			Joomla.submitform(task, document.getElementById('receive_milk_quantity_cow-form'));
		}
		else {
			
			if (task != 'receive_milk_quantity_cow.cancel' && document.formvalidator.isValid(document.id('receive_milk_quantity_cow-form'))) {
				
				Joomla.submitform(task, document.getElementById('receive_milk_quantity_cow-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_edairy&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="receive_milk_quantity_cow-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_EDAIRY_TITLE_RECEIVE_MILK_QUANTITY_COW', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">
								<div style="display:none">
									<?php echo $this->form->renderField('id'); ?>
								</div>
				
										<div class="control-group">
													<div class="control-label">
														<label>
															วันที่
														</label>
													</div>
													<div class="controls">
														<input type="text" name="jform[create_date]" class="datetimepicker"  value="<?php echo formatDate($this->item->create_date); ?>" onChange="javascript:getCowsInFarm();"/>
													</div>
												</div>

										<div class="control-group">
													<div class="control-label">
														<label>
															เกษตรกร
														</label>
													</div>
													<div class="controls">
														<select id="farmer_id" name="farmer_id" onChange="javascript:getCowsInFarm();">
															<option value="">-- เลือกเกษตรกร --</option>
															<?php foreach($this->masterData->mas_farmer as $key=>$data){ 
																	$selected = "";
																	if($data->id == $_REQUEST["farmer_id"]){
																		$selected = " selected";
																	}
																?>
																<option value="<?php echo $data->id; ?>"<?php echo $selected; ?>><?php echo $data->citizen_id; ?> <?php echo $data->name; ?> <?php echo $data->surname; ?></option>
															<?php } ?>
														</select>
													</div>
												</div>

			<?php /*
				<?php echo $this->form->renderField('cow_id'); ?>

			<?php
				foreach((array)$this->item->cow_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="cow_id" name="jform[cow_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				

			<?php echo $this->form->renderField('morning_amount'); ?>
				<?php echo $this->form->renderField('evening_amount'); ?>
				<?php echo $this->form->renderField('total_amount'); ?>
			*/ ?>
			<table border="1" cellpadding="5" width="800" id="cow_list">
				<tr><th>ที่</th><th>ชื่อ</th><th>หมายเลข</th><th>ปริมาณเช้า (กก.)</th><th>ปริมาณเย็น</th><th>ปริมาณรวม (กก.)</th><th>ลำดับการรีด</th></tr>

				<tr>
					<td colspan="7" align="center">กรุณาเลือกเกษตรกร</td>
				</tr>
			</table>
			<br /><br />
			<div style="display:none">
				<?php echo $this->form->renderField('state'); ?>
			</div>
				<?php echo $this->form->renderField('created_by'); ?>


					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>

<script type="text/javascript">
	function getCowsInFarm(){
		var is_pass = true;
		if(jQuery("#farmer_id").val()==""){
			//alert("กรุณาเลือกเกษตรกร");
			is_pass = false;
		}
		if(jQuery('input[name="jform[create_date]"]').val()==""){
			//alert("กรุณาใส่วันที่");
			is_pass = false;
		}
		if(is_pass){
		jQuery.ajax({
            url: "index.php?option=com_edairy&task=receive_milk_quantity_cow.getCowsInFarm",
            type: "POST",
            data: {
                'farmer_id': jQuery("#farmer_id").val(),
                'create_date': jQuery('input[name="jform[create_date]"]').val()
            },
            success: function (data) {
            	var data = jQuery.parseJSON(data);
            	console.log(data);

            	if(data!=null){
            		jQuery("#cow_list").html('<tr><th>ที่</th><th>ชื่อ</th><th>หมายเลข</th><th>ปริมาณเช้า (กก.)</th><th>ปริมาณเย็น</th><th>ปริมาณรวม (กก.)</th><th>ลำดับการรีด</th></tr>');
            		
            		for(var i=0;i<data.length;i++){
            			var morning_amount = "";
            			var evening_amount = "";
            			var total_amount = "";
            			if(data[i].receive_milk_quantity!=null){
            				morning_amount = data[i].receive_milk_quantity.morning_amount;
            				evening_amount = data[i].receive_milk_quantity.evening_amount;
            				total_amount = data[i].receive_milk_quantity.total_amount;
            			}
            			if(data[i].farm_order==9999){
            				data[i].farm_order = "";
            			}

            			jQuery("#cow_list").append("<tr><td>"+(i+1)+"</td><td><input type='hidden' name='cow_id[]' value='"+data[i].id+"' />"+data[i].name+"</td><td>"+data[i].cow_id+"</td><td><input type='text' style='width:100px' name='morning_amount[]' value='"+morning_amount+"'/></td><td><input type='text' style='width:100px' name='evening_amount[]'  value='"+evening_amount+"'/></td><td><input type='text' style='width:100px' name='total_amount[]'  value='"+total_amount+"'/></td><td><input type='text' style='width:100px' name='farm_order[]' value='"+data[i].farm_order+"'/></td></tr>");
            		}
	            	/*var own_date = data.own_date.split("-");

	            	jQuery("input[name=owner_own_date]").val(own_date[2] + "/" + own_date[1] + "/" + (parseInt(own_date[0])+543));

	            	jQuery("select[name=owner_farmer_id]").val(data.farmer_id);

	            	jQuery("select[name=owner_farmer_id]").trigger('liszt:updated');

	            	jQuery("input[name=owner_farm_id]").val(data.farm_id);

	            	jQuery("input[name=owner_id]").val(data.id);
	            	*/
	            }

            }

        });
	}
	}

	<?php if($_REQUEST["create_date"]!="" && $_REQUEST["farmer_id"]!=""){ ?>
		getCowsInFarm();
	<?php } ?>
</script>
