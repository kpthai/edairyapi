<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'administrator/components/com_edairy/assets/css/edairy.css');
$document->addStyleSheet(JUri::root() . 'media/com_edairy/css/list.css');

$user      = JFactory::getUser();
$userId    = $user->get('id');
$listOrder = $this->state->get('list.ordering');
$listDirn  = $this->state->get('list.direction');
$canOrder  = $user->authorise('core.edit.state', 'com_edairy');
$saveOrder = $listOrder == 'a.`ordering`';

if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?option=com_edairy&task=receive_milk_quantities.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'receive_milk_quantityList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}

$sortFields = $this->getSortFields();
?>
<script src="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript" charset="UTF-8"></script>

<link rel="stylesheet" href="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.css">

<script type="text/javascript">
	jQuery(document).ready(function () {
		jQuery('#clear-search-button').on('click', function () {
			jQuery('select[name=search_region]').val('');
			jQuery('select[name=search_province]').val('');
			jQuery('select[name=search_region]').val('');
			jQuery('select').trigger('liszt:updated');
			jQuery('.well input').val('');
			jQuery('#adminForm').submit();
		});
	});

	js = jQuery.noConflict();
	js(document).ready(function () {
		js('.datetimepicker').datetimepicker({
            lang:'th',
            timepicker:false,
            format:'d/m/Y'
        });
     });
</script>

<form action="<?php echo JRoute::_('index.php?option=com_edairy&view=receive_milk_quantities&layout=report'); ?>" method="post"
	  name="adminForm" id="adminForm">
	<?php if (!empty($this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
		<?php else : ?>
		<div id="j-main-container">
			<?php endif; ?>

			<h4>ใบรับน้ำนมดิบ</h4>
			<div id="filter-bar" class="btn-toolbar well" style="min-height:80px">

				<div class="filter-search btn-group pull-left">
					
					<div style="display:none">
						<label for="filter_search" style="display:inline;">
							&nbsp;รหัสศูนย์/ชื่อสหกรณ์ 
						</label>
						 <input type="text" name="filter_search" id="filter_search"
							   
							   value="<?php echo $this->escape($this->state->get('filter.search')); ?>"
							   title="<?php echo JText::_('JSEARCH_FILTER'); ?>"/>
					</div>
					

					
					<label style="display:inline;">
						&nbsp;หมายเลขสมาชิก/เบอร์ถัง
					</label>
					<input type="text" name="search_member_code" value="<?php echo $_REQUEST["search_member_code"]; ?>" />	
					 



 					<label style="display:inline;">
						&nbsp;ชื่อ-นามสกุลเกษตรกร
					</label>


					 <input type="text" name="search_farmer" value="<?php echo $_REQUEST['search_farmer']; ?>"/>

					 
					 <label style="display:inline;">
						&nbsp;ระหว่างวันที่
					</label>


					 <input type="text" name="search_date_from" class="datetimepicker" value="<?php echo $_REQUEST['search_date_from']; ?>"/>

					 <label style="display:inline;">
						&nbsp;ถึงวันที่
					</label>
					 
					 <input type="text" name="search_date_to" class="datetimepicker" value="<?php echo $_REQUEST['search_date_to']; ?>"/>

					<div style="clear:both">
					</div>



					<div class="btn-group pull-left">
					<button class="btn hasTooltip" type="submit"
							title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>">
						<i class="icon-search"></i></button>
					<button class="btn hasTooltip" id="clear-search-button" type="button"
							title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>">
						<i class="icon-remove"></i></button>
				</div>
				<div class="btn-group pull-right hidden-phone">
					<label for="limit"
						   class="element-invisible">
						<?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?>
					</label>
					<?php// echo $this->pagination->getLimitBox(); ?>
				</div>
				</div>
			</div>

            <?php // echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this)); ?>

			<div class="clearfix"></div>

			<b>พบทั้งสิ้น <?php echo $this->pagination->total; ?> รายการ</b>

            <?php //echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this)); ?>

			<div class="clearfix"></div>
			<table class="table table-striped" id="receive_milk_quantityList">
				<thead>
				<tr>
					<?php if (isset($this->items[0]->ordering)): ?>
						<th width="1%" class="nowrap center hidden-phone">
                            <?php echo JHtml::_('searchtools.sort', '', 'a.ordering', $listDirn, $listOrder, null, 'asc', 'JGRID_HEADING_ORDERING', 'icon-menu-2'); ?>
                        </th>
					<?php endif; ?>
					<th width="1%" class="hidden-phone">
						<input type="checkbox" name="checkall-toggle" value=""
							   title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)"/>
					</th>
					<?php if (isset($this->items[0]->state)): ?>
						<th width="1%" class="nowrap center">
								<?php echo JHtml::_('searchtools.sort', 'JSTATUS', 'a.`state`', $listDirn, $listOrder); ?>
</th>
					<?php endif; ?>

									<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_RECEIVE_MILK_QUANTITIES_ID', 'a.`id`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_RECEIVE_MILK_QUANTITIES_RUN_NO', 'a.`run_no`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_RECEIVE_MILK_QUANTITIES_CREATE_DATETIME', 'a.`create_datetime`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_RECEIVE_MILK_QUANTITIES_ROUND', 'a.`round`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_RECEIVE_MILK_QUANTITIES_TOTAL_AMOUNT', 'a.`total_amount`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_RECEIVE_MILK_QUANTITIES_COOP_ID', 'a.`coop_id`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_RECEIVE_MILK_QUANTITIES_FARMER_ID', 'a.`farmer_id`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_RECEIVE_MILK_QUANTITIES_REMARK', 'a.`remark`', $listDirn, $listOrder); ?>
				</th>
				<th>
					ใบรับน้ำนมดิบ
				</th>
					
				</tr>
				</thead>
				<tfoot>
				<tr>
					<td colspan="<?php echo isset($this->items[0]) ? count(get_object_vars($this->items[0])) : 10; ?>">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
				</tfoot>
				<tbody>
				<?php $itemIndex=0; foreach ($this->items as $i => $item) :
					$ordering   = ($listOrder == 'a.ordering');
					$canCreate  = $user->authorise('core.create', 'com_edairy');
					$canEdit    = $user->authorise('core.edit', 'com_edairy');
					$canCheckin = $user->authorise('core.manage', 'com_edairy');
					$canChange  = $user->authorise('core.edit.state', 'com_edairy');
					?>
					<tr class="row<?php echo $i % 2; ?>">

						<?php if (isset($this->items[0]->ordering)) : ?>
							<td class="order nowrap center hidden-phone">
								<?php if ($canChange) :
									$disableClassName = '';
									$disabledLabel    = '';

									if (!$saveOrder) :
										$disabledLabel    = JText::_('JORDERINGDISABLED');
										$disableClassName = 'inactive tip-top';
									endif; ?>
									<span class="sortable-handler hasTooltip <?php echo $disableClassName ?>"
										  title="<?php echo $disabledLabel ?>">
							<i class="icon-menu"></i>
						</span>
									<input type="text" style="display:none" name="order[]" size="5"
										   value="<?php echo $item->ordering; ?>" class="width-20 text-area-order "/>
								<?php else : ?>
									<span class="sortable-handler inactive">
							<i class="icon-menu"></i>
						</span>
								<?php endif; ?>
							</td>
						<?php endif; ?>
						<td class="hidden-phone">
							<?php echo JHtml::_('grid.id', $i, $item->id); ?>
						</td>
						<?php if (isset($this->items[0]->state)): ?>
							<td class="center">
								<?php echo JHtml::_('jgrid.published', $item->state, $i, 'receive_milk_quantities.', $canChange, 'cb'); ?>
</td>
						<?php endif; ?>

										<td>

					<?php echo $item->id; ?>
				</td>				<td>
				<?php if (isset($item->checked_out) && $item->checked_out && ($canEdit || $canChange)) : ?>
					<?php echo JHtml::_('jgrid.checkedout', $i, $item->uEditor, $item->checked_out_time, 'receive_milk_quantities.', $canCheckin); ?>
				<?php endif; ?>
				<?php if ($canEdit) : ?>
					<a href="<?php echo JRoute::_('index.php?option=com_edairy&task=receive_milk_quantity.edit&id='.(int) $item->id); ?>">
					<?php echo $this->escape($item->run_no); ?></a>
				<?php else : ?>
					<?php echo $this->escape($item->run_no); ?>
				<?php endif; ?>

				</td>				<td>

					<?php echo formatDate($item->create_datetime); ?>
				</td>				<td>

					<?php echo $item->round; ?>
				</td>				<td>

					<?php echo $item->total_amount; ?>
				</td>				<td>

					<?php echo $item->coop_id; ?>
				</td>				<td>

					<?php echo $item->farmer_id; ?>
				</td>				<td>

					<?php echo $item->remark; ?>
				</td>
				<td>
						<a href="index.php?option=com_edairy&view=receive_milk_quantity&layout=pdf&id=<?php echo $item->id; ?>" class="btn" target="_blank"><div class="icon-print"></div> พิมพ์</a>
					
					
				</td>

					</tr>
				<?php $itemIndex++; endforeach; ?>
				</tbody>
			</table>
			<input type="hidden" name="layout" value="report"/>
			<input type="hidden" name="task" value=""/>
			<input type="hidden" name="boxchecked" value="0"/>
            <input type="hidden" name="list[fullorder]" value="<?php echo $listOrder; ?> <?php echo $listDirn; ?>"/>
			<?php echo JHtml::_('form.token'); ?>
		</div>
</form>
<script>
    window.toggleField = function (id, task, field) {

        var f = document.adminForm, i = 0, cbx, cb = f[ id ];

        if (!cb) return false;

        while (true) {
            cbx = f[ 'cb' + i ];

            if (!cbx) break;

            cbx.checked = false;
            i++;
        }

        var inputField   = document.createElement('input');

        inputField.type  = 'hidden';
        inputField.name  = 'field';
        inputField.value = field;
        f.appendChild(inputField);

        cb.checked = true;
        f.boxchecked.value = 1;
        window.submitform(task);

        return false;
    };
</script>