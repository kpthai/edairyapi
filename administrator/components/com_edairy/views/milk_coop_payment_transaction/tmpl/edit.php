<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_edairy/css/form.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	js('input:hidden.milk_coop_payment_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('milk_coop_payment_idhidden')){
			js('#jform_milk_coop_payment_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_milk_coop_payment_id").trigger("liszt:updated");
	js('input:hidden.farmer_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('farmer_idhidden')){
			js('#jform_farmer_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_farmer_id").trigger("liszt:updated");
	});

	Joomla.submitbutton = function (task) {
		if (task == 'milk_coop_payment_transaction.cancel') {
			Joomla.submitform(task, document.getElementById('milk_coop_payment_transaction-form'));
		}
		else {
			
			if (task != 'milk_coop_payment_transaction.cancel' && document.formvalidator.isValid(document.id('milk_coop_payment_transaction-form'))) {
				
				Joomla.submitform(task, document.getElementById('milk_coop_payment_transaction-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_edairy&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="milk_coop_payment_transaction-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_EDAIRY_TITLE_MILK_COOP_PAYMENT_TRANSACTION', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

									<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
				<?php echo $this->form->renderField('create_date'); ?>
				<?php echo $this->form->renderField('license_plate'); ?>
				<?php echo $this->form->renderField('milk_coop_payment_id'); ?>

			<?php
				foreach((array)$this->item->milk_coop_payment_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="milk_coop_payment_id" name="jform[milk_coop_payment_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('farmer_id'); ?>

			<?php
				foreach((array)$this->item->farmer_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="farmer_id" name="jform[farmer_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('milk_amount'); ?>
				<?php echo $this->form->renderField('milk_price'); ?>
				<?php echo $this->form->renderField('total_income'); ?>
				<?php echo $this->form->renderField('insemination_amount'); ?>
				<?php echo $this->form->renderField('total_insemination'); ?>
				<?php echo $this->form->renderField('health_service_amount'); ?>
				<?php echo $this->form->renderField('total_health_service'); ?>
				<?php echo $this->form->renderField('grand_total'); ?>
				<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
				<?php echo $this->form->renderField('state'); ?>
				<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
				<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />
				<?php echo $this->form->renderField('created_by'); ?>

				<?php echo $this->form->renderField('modified_by'); ?>

					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
