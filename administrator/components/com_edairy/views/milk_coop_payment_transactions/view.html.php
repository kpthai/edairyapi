<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Edairy.
 *
 * @since  1.6
 */
class EdairyViewMilk_coop_payment_transactions extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');
        $this->filterForm = $this->get('FilterForm');
        $this->activeFilters = $this->get('ActiveFilters');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		EdairyHelper::addSubmenu('milk_coop_payment_transactions');

		$this->addToolbar();

		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function addToolbar()
	{
		$state = $this->get('State');
		$canDo = EdairyHelper::getActions();

		JToolBarHelper::title(JText::_('COM_EDAIRY_TITLE_MILK_COOP_PAYMENT_TRANSACTIONS'), 'milk_coop_payment_transactions.png');

		// Check if the form exists before showing the add/edit buttons
		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/milk_coop_payment_transaction';

		if (file_exists($formPath))
		{
			if ($canDo->get('core.create'))
			{
				JToolBarHelper::addNew('milk_coop_payment_transaction.add', 'JTOOLBAR_NEW');

				if (isset($this->items[0]))
				{
					JToolbarHelper::custom('milk_coop_payment_transactions.duplicate', 'copy.png', 'copy_f2.png', 'JTOOLBAR_DUPLICATE', true);
				}
			}

			if ($canDo->get('core.edit') && isset($this->items[0]))
			{
				JToolBarHelper::editList('milk_coop_payment_transaction.edit', 'JTOOLBAR_EDIT');
			}
		}

		if ($canDo->get('core.edit.state'))
		{
			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::custom('milk_coop_payment_transactions.publish', 'publish.png', 'publish_f2.png', 'JTOOLBAR_PUBLISH', true);
				JToolBarHelper::custom('milk_coop_payment_transactions.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
			}
			elseif (isset($this->items[0]))
			{
				// If this component does not use state then show a direct delete button as we can not trash
				JToolBarHelper::deleteList('', 'milk_coop_payment_transactions.delete', 'JTOOLBAR_DELETE');
			}

			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::archiveList('milk_coop_payment_transactions.archive', 'JTOOLBAR_ARCHIVE');
			}

			if (isset($this->items[0]->checked_out))
			{
				JToolBarHelper::custom('milk_coop_payment_transactions.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
			}
		}

		// Show trash and delete for components that uses the state field
		if (isset($this->items[0]->state))
		{
			if ($state->get('filter.state') == -2 && $canDo->get('core.delete'))
			{
				JToolBarHelper::deleteList('', 'milk_coop_payment_transactions.delete', 'JTOOLBAR_EMPTY_TRASH');
				JToolBarHelper::divider();
			}
			elseif ($canDo->get('core.edit.state'))
			{
				JToolBarHelper::trash('milk_coop_payment_transactions.trash', 'JTOOLBAR_TRASH');
				JToolBarHelper::divider();
			}
		}

		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_edairy');
		}

		// Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_edairy&view=milk_coop_payment_transactions');
	}

	/**
	 * Method to order fields 
	 *
	 * @return void 
	 */
	protected function getSortFields()
	{
		return array(
			'a.`id`' => JText::_('JGRID_HEADING_ID'),
			'a.`create_date`' => JText::_('COM_EDAIRY_MILK_COOP_PAYMENT_TRANSACTIONS_CREATE_DATE'),
			'a.`license_plate`' => JText::_('COM_EDAIRY_MILK_COOP_PAYMENT_TRANSACTIONS_LICENSE_PLATE'),
			'a.`milk_coop_payment_id`' => JText::_('COM_EDAIRY_MILK_COOP_PAYMENT_TRANSACTIONS_MILK_COOP_PAYMENT_ID'),
			'a.`farmer_id`' => JText::_('COM_EDAIRY_MILK_COOP_PAYMENT_TRANSACTIONS_FARMER_ID'),
			'a.`milk_amount`' => JText::_('COM_EDAIRY_MILK_COOP_PAYMENT_TRANSACTIONS_MILK_AMOUNT'),
			'a.`milk_price`' => JText::_('COM_EDAIRY_MILK_COOP_PAYMENT_TRANSACTIONS_MILK_PRICE'),
			'a.`total_income`' => JText::_('COM_EDAIRY_MILK_COOP_PAYMENT_TRANSACTIONS_TOTAL_INCOME'),
			'a.`insemination_amount`' => JText::_('COM_EDAIRY_MILK_COOP_PAYMENT_TRANSACTIONS_INSEMINATION_AMOUNT'),
			'a.`total_insemination`' => JText::_('COM_EDAIRY_MILK_COOP_PAYMENT_TRANSACTIONS_TOTAL_INSEMINATION'),
			'a.`health_service_amount`' => JText::_('COM_EDAIRY_MILK_COOP_PAYMENT_TRANSACTIONS_HEALTH_SERVICE_AMOUNT'),
			'a.`total_health_service`' => JText::_('COM_EDAIRY_MILK_COOP_PAYMENT_TRANSACTIONS_TOTAL_HEALTH_SERVICE'),
			'a.`grand_total`' => JText::_('COM_EDAIRY_MILK_COOP_PAYMENT_TRANSACTIONS_GRAND_TOTAL'),
			'a.`ordering`' => JText::_('JGRID_HEADING_ORDERING'),
			'a.`state`' => JText::_('JSTATUS'),
		);
	}

    /**
     * Check if state is set
     *
     * @param   mixed  $state  State
     *
     * @return bool
     */
    public function getState($state)
    {
        return isset($this->state->{$state}) ? $this->state->{$state} : false;
    }
}
