<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_edairy/css/form.css');
?>
<script src="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript" charset="UTF-8"></script>

<link rel="stylesheet" href="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.css">
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
		js('.datetimepicker').datetimepicker({
            lang:'th',
            timepicker:false,
            format:'d/m/Y'
        });

	js('input:hidden.farm_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('farm_idhidden')){
			js('#jform_farm_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_farm_id").trigger("liszt:updated");
	});

	Joomla.submitbutton = function (task) {
		if (task == 'farm_score.cancel') {
			Joomla.submitform(task, document.getElementById('farm_score-form'));
		}
		else {
			
			if (task != 'farm_score.cancel' && document.formvalidator.isValid(document.id('farm_score-form'))) {
				
				Joomla.submitform(task, document.getElementById('farm_score-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>
<style type="text/css">
	.score_box{
		width: 50px;
	}

</style>
<form
	action="<?php echo JRoute::_('index.php?option=com_edairy&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="farm_score-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_EDAIRY_TITLE_FARM_SCORE', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

							<div style="display:none">
									<?php echo $this->form->renderField('id'); ?>
								</div>
				<?php echo $this->form->renderField('farm_id'); ?>

			<?php
				foreach((array)$this->item->farm_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="farm_id" name="jform[farm_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				

			<div class="control-group">
					<div class="control-label">
						<label id="jform_assessment_date-lbl" for="jform_assessment_date" class="">
							วันที่ประเมิน</label>
					</div>
					<div class="controls"><div class="input-append"><input type="text" title="" name="jform[assessment_date]" id="jform_assessment_date" value="<?php echo formatDate($this->item->assessment_date); ?>" maxlength="45" class="inputbox hasTooltip datetimepicker" placeholder="วันที่ประเมิน"  aria-invalid="false"></div></div>
		</div>
				
				<hr />
					<b>ใบคะแนนคอก</b>
					<table cellpadding="5" width="1000">
						<tr>
							<td><b>1. คอกรีดและคอกพักโค (20 คะแนน)</b></td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="number" class="score_box" id="sum1" readonly /></td>
							<td><b>3. อุปกรณ์ที่เกี่ยวข้องกับน้ำนม (24 คะแนน)</b></td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="number" class="score_box" id="sum3" readonly /></td>
						</tr>
						<tr>
							<td>1.1 คอกรีด</td>
							<td></td>
							<td>- ภาชนะรีดนมสะอาดไม่มีกลิ่นอับ</td>
							<td>(4) <input type="number" class="score_box" id="score16" onChange="javascript:calculateFarmScore();" /></td>
						</tr>
						<tr>
							<td>- คอกรีดแห้งสะอาด</td>
							<td>(3) <input type="number" class="score_box"  id="score1" onChange="javascript:calculateFarmScore();" /></td>
							<td>- ภาชนะบรรจุนมสะอาดไม่มีกลิ่นอับ</td>
							<td>(4) <input type="number" class="score_box" id="score17" onChange="javascript:calculateFarmScore();" /></td>
						</tr>
						<tr>
							<td>- การระบายอากาศดีไม่อับทึบ</td>
							<td>(2) <input type="number" class="score_box" id="score2" onChange="javascript:calculateFarmScore();" /></td>
							<td>- ล้างทำความสะอาดอุปกรณ์ถูกต้องตามขั้นตอน</td>
							<td>(4) <input type="number" class="score_box" id="score18" onChange="javascript:calculateFarmScore();" /></td>
						</tr>
						<tr>
							<td>- มีห้องเก็บอาหารและอุปกรณ์ที่สะอาดเรียบร้อย</td>
							<td>(2) <input type="number" class="score_box" id="score3" onChange="javascript:calculateFarmScore();" /></td>
							<td>- ล้างและเก็บถังใส่นมทันทีหลังรีดนม</td>
							<td>(4) <input type="number" class="score_box" id="score19" onChange="javascript:calculateFarmScore();" /></td>
						</tr>
						<tr>
							<td>- การระบายมูลโคถูกสุขลักษณะ</td>
							<td>(2) <input type="number" class="score_box" id="score4" onChange="javascript:calculateFarmScore();" /></td>
							<td>- จัดเก็บอุปกรณ์เป็นระเบียบและอยู่ในที่อากาศถ่ายเทสะดวก</td>
							<td>(4) <input type="number" class="score_box" id="score20" onChange="javascript:calculateFarmScore();" /></td>
						</tr>
						<tr>
							<td>- ไม่มีสัตว์เลี้ยงอื่นปะปนบริเวณคอกรีด</td>
							<td>(2) <input type="number" class="score_box" id="score5" onChange="javascript:calculateFarmScore();" /></td>
							<td>- อุปกรณ์และภาชนะใส่นมต้องทำความสะอาดอย่างไม่มีรอยตะเข็บ รอยต่อหรือบุบ</td>
							<td>(4) <input type="number" class="score_box" id="score21" onChange="javascript:calculateFarmScore();" /></td>
						</tr>

						<tr>
							<td>1.2 คอกพัก</td>
							<td></td>
							<td><b>(4) การขนส่งและปฎิบัติต่อน้ำนม (12 คะแนน)</b></td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="number" class="score_box" id="sum4" readonly /></td>
						</tr>
						<tr>
							<td>- คอกสะอาดกว้างขวางพอ</td>
							<td>(3) <input type="number" class="score_box" id="score6" onChange="javascript:calculateFarmScore();" /></td>
							<td>- รีดนมตรงเวลาสม่ำเสมอ</td>
							<td>(3) <input type="number" class="score_box" id="score22" onChange="javascript:calculateFarmScore();" /></td>
						</tr>
						<tr>
							<td>- มีน้ำสะอาดและแร่ธาตุตลอดเวลา</td>
							<td>(2) <input type="number" class="score_box" id="score7" onChange="javascript:calculateFarmScore();" /></td>
							<td>- ไม่รีดนมก่อนเวลารับนมนานเกินไป</td>
							<td>(3) <input type="number" class="score_box" id="score23" /></td>
						</tr>
						<tr>
							<td>- มีร่มเงาเพียงพอ</td>
							<td>(2) <input type="number" class="score_box" id="score8" onChange="javascript:calculateFarmScore();" /></td>
							<td>- ไม่ตั้งถังนมตากแดด</td>
							<td>(3) <input type="number" class="score_box" id="score24" onChange="javascript:calculateFarmScore();" /></td>
						</tr>
						<tr>
							<td>- แบ่งโคตามประเภท มีคอกลูกโคแยกต่างหาก</td>
							<td>(2) <input type="number" class="score_box" id="score9" onChange="javascript:calculateFarmScore();" /></td>
							<td>- การขนส่งนมไปยังศูนย์ใช้ระยะเวลาสั้น</td>
							<td>(3) <input type="number" class="score_box" id="score25" onChange="javascript:calculateFarmScore();" /></td>
						</tr>
						<tr>
							<td><b>2. การรีดนม (20 คะแนน)</b></td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="number" class="score_box" id="sum2" readonly /></td>
							<td><b>5. ตัวโค (12 คะแนน)</b></td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="number" class="score_box" id="sum5" readonly /></td>
						</tr>
						<tr>
							<td>- ทำความสะอาดตัวโค / แปรงขนก่อนรีด</td>
							<td>(3) <input type="number" class="score_box" id="score10" onChange="javascript:calculateFarmScore();" /></td>
							<td>- โคทุกตัวในฝูงต้องผ่านการตรวจโรคประจำปี</td>
							<td>(3) <input type="number" class="score_box" id="score26" onChange="javascript:calculateFarmScore();" /></td>
						</tr>
						<tr>
							<td>- ล้างและเช็ดเต้านมให้แห้งก่อนรีด</td>
							<td>(3) <input type="number" class="score_box" id="score11" onChange="javascript:calculateFarmScore();" /></td>
							<td>- ฉีดวัคซีนตามโปรแกรมสม่ำเสมอ</td>
							<td>(3) <input type="number" class="score_box" id="score27" onChange="javascript:calculateFarmScore();" /></td>
						</tr>
						<tr>
							<td>- ตรวจน้ำนมด้วยถ้วยตรวจนมก่อนรีด</td>
							<td>(3) <input type="number" class="score_box" id="score12" onChange="javascript:calculateFarmScore();" /></td>
							<td>- โคมีสุขภาพสมบูรณ์</td>
							<td>(3) <input type="number" class="score_box" id="score28" onChange="javascript:calculateFarmScore();" /></td>
						</tr>
						<tr>
							<td>- รีดนมอย่างถูกวิธี</td>
							<td>(5) <input type="number" class="score_box" id="score13" onChange="javascript:calculateFarmScore();" /></td>
							<td>- โคทุกตัวมีพันธ์ประวัติ</td>
							<td>(3) <input type="number" class="score_box" id="score29" onChange="javascript:calculateFarmScore();" /></td>
						</tr>
						<tr>
							<td>- ใช้น้ำยาจุ่มหัวนมทันทีหลังรีดเสร็จ</td>
							<td>(3) <input type="number" class="score_box" id="score14" onChange="javascript:calculateFarmScore();" /></td>
							<td><b>(6) ตัวเกษตรกร (12 คะแนน)</b></td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="number" class="score_box" id="sum6" readonly /></td>
						</tr>
						<tr>
							<td>- ใช้น้ำยา CMT ตรวจสุขภาพเต้านม</td>
							<td>(3) <input type="number" class="score_box" id="score15" onChange="javascript:calculateFarmScore();" /></td>
							<td>- การแต่งกายขณะรีดนม</td>
							<td>(3) <input type="number" class="score_box" id="score30" onChange="javascript:calculateFarmScore();" /></td>
						</tr>
						<tr>
							<td colspan="2" rowspan="3">
								<b>เกณฑ์คะแนนคอก</b>
								<table>
									<tr>
										<td>(4) 90 คะแนนขึ้นไป = ดีเลิศ</td>
										<td>เพิ่มราคา = 0.15 บาท/กก.</td>
									</tr>
									<tr>
										<td>(3) 80 คะแนนขึ้นไป = ดีมาก</td>
										<td>เพิ่มราคา = 0.10 บาท/กก.</td>
									</tr>
									<tr>
										<td>(2) 70 คะแนนขึ้นไป = ดี</td>
										<td>เพิ่มราคา = 0.05 บาท/กก.</td>
									</tr>
									<tr>
										<td>(1) ต่ำกว่า 70 คะแนนขึ้นไป = ไม่ดี</td>
										<td>เพิ่มราคา = 0.00 บาท/กก.</td>
									</tr>
								</table>
							</td>
							<td>- ให้ความร่วมมือและปฎิบัติตามคำแนะนำ</td>
							<td>(3) <input type="number" class="score_box" id="score31" onChange="javascript:calculateFarmScore();" /></td>
						</tr>
						<tr>
							<td>- มีความสนใจกระตือรือร้น ใฝ่รู้</td>
							<td>(3) <input type="number" class="score_box" id="score32" onChange="javascript:calculateFarmScore();" /></td>
						</tr>
						<tr>
							<td>- มีการจดบันทึกประจำฟาร์ม</td>
							<td>(3) <input type="number" class="score_box" id="score33" onChange="javascript:calculateFarmScore();" /></td>
						</tr>
					</table>
					
				<hr />
					<b>คะแนนเต็ม 100 คะแนน คะแนนที่ได้ <input type="number" class="score_box" id="sum_total" readonly /> คะแนน &nbsp;&nbsp;&nbsp; คะแนนคอก <input type="number" class="score_box" id="farm_score" readonly /></b>
				<hr />

				<?php echo $this->form->renderField('score'); ?>
				<?php echo $this->form->renderField('score_result'); ?>

				<div style="display: none">
					<?php echo $this->form->renderField('assessment_data'); ?>
				</div>
				<?php echo $this->form->renderField('remark'); ?>
				<?php echo $this->form->renderField('create_by'); ?>
				<?php echo $this->form->renderField('update_by'); ?>
				<div class="control-group">
					<div class="control-label">
						<label class="">วันที่สร้าง</label>
					</div>
					<div class="controls">
						<?php echo formatDate($this->item->create_datetime); ?>
					</div>
				</div>

				<div class="control-group">
					<div class="control-label">
						<label class="">วันที่ปรับปรุง</label>
					</div>
					<div class="controls">
						<?php echo formatDate($this->item->update_datetime); ?>
					</div>
				</div>
				<div style="display:none">
					<?php echo $this->form->renderField('state'); ?>
					<?php echo $this->form->renderField('created_by'); ?>
				</div>
				


					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>

<script type="text/javascript">
	function decodeScoreData(){
		var score_data = "<?php echo $this->item->assessment_data?>";
		var score_array = score_data.split("|");
		for(var i=0;i<score_array.length;i++){
			jQuery("#score"+(i+1)).val(score_array[i]);
		}
		console.log(score_array);
	}

	function calculateFarmScore(){
		var score = 0;
		var sum1 = 0;
		var sum2 = 0;
		var sum3 = 0;
		var sum4 = 0;
		var sum5 = 0;
		var sum6 = 0;

		if(jQuery("#score1").val()!=""){
			score += parseInt(jQuery("#score1").val());
			sum1 += parseInt(jQuery("#score1").val());
		}
		if(jQuery("#score2").val()!=""){
			score += parseInt(jQuery("#score2").val());
			sum1 += parseInt(jQuery("#score2").val());
		}
		if(jQuery("#score3").val()!=""){
			score += parseInt(jQuery("#score3").val());
			sum1 += parseInt(jQuery("#score3").val());
		}
		if(jQuery("#score4").val()!=""){
			score += parseInt(jQuery("#score4").val());
			sum1 += parseInt(jQuery("#score4").val());
		}
		if(jQuery("#score5").val()!=""){
			score += parseInt(jQuery("#score5").val());
			sum1 += parseInt(jQuery("#score5").val());
		}
		if(jQuery("#score6").val()!=""){
			score += parseInt(jQuery("#score6").val());
			sum1 += parseInt(jQuery("#score6").val());
		}
		if(jQuery("#score7").val()!=""){
			score += parseInt(jQuery("#score7").val());
			sum1 += parseInt(jQuery("#score7").val());
		}
		if(jQuery("#score8").val()!=""){
			score += parseInt(jQuery("#score8").val());
			sum1 += parseInt(jQuery("#score8").val());
		}
		if(jQuery("#score9").val()!=""){
			score += parseInt(jQuery("#score9").val());
			sum1 += parseInt(jQuery("#score9").val());
		}
		if(jQuery("#score10").val()!=""){
			score += parseInt(jQuery("#score10").val());
			sum2 += parseInt(jQuery("#score10").val());
		}
		if(jQuery("#score11").val()!=""){
			score += parseInt(jQuery("#score11").val());
			sum2 += parseInt(jQuery("#score11").val());
		}
		if(jQuery("#score12").val()!=""){
			score += parseInt(jQuery("#score12").val());
			sum2 += parseInt(jQuery("#score12").val());
		}
		if(jQuery("#score13").val()!=""){
			score += parseInt(jQuery("#score13").val());
			sum2 += parseInt(jQuery("#score13").val());
		}
		if(jQuery("#score14").val()!=""){
			score += parseInt(jQuery("#score14").val());
			sum2 += parseInt(jQuery("#score14").val());
		}
		if(jQuery("#score15").val()!=""){
			score += parseInt(jQuery("#score15").val());
			sum2 += parseInt(jQuery("#score15").val());
		}
		if(jQuery("#score16").val()!=""){
			score += parseInt(jQuery("#score16").val());
			sum3 += parseInt(jQuery("#score16").val());
		}
		if(jQuery("#score17").val()!=""){
			score += parseInt(jQuery("#score17").val());
			sum3 += parseInt(jQuery("#score17").val());
		}
		if(jQuery("#score18").val()!=""){
			score += parseInt(jQuery("#score18").val());
			sum3 += parseInt(jQuery("#score18").val());
		}
		if(jQuery("#score19").val()!=""){
			score += parseInt(jQuery("#score19").val());
			sum3 += parseInt(jQuery("#score19").val());
		}
		if(jQuery("#score20").val()!=""){
			score += parseInt(jQuery("#score20").val());
			sum3 += parseInt(jQuery("#score20").val());
		}
		if(jQuery("#score21").val()!=""){
			score += parseInt(jQuery("#score21").val());
			sum3 += parseInt(jQuery("#score21").val());
		}
		if(jQuery("#score22").val()!=""){
			score += parseInt(jQuery("#score22").val());
			sum4 += parseInt(jQuery("#score22").val());
		}
		if(jQuery("#score23").val()!=""){
			score += parseInt(jQuery("#score23").val());
			sum4 += parseInt(jQuery("#score23").val());
		}
		if(jQuery("#score24").val()!=""){
			score += parseInt(jQuery("#score24").val());
			sum4 += parseInt(jQuery("#score24").val());
		}
		if(jQuery("#score25").val()!=""){
			score += parseInt(jQuery("#score25").val());
			sum4 += parseInt(jQuery("#score25").val());
		}
		if(jQuery("#score26").val()!=""){
			score += parseInt(jQuery("#score26").val());
			sum5 += parseInt(jQuery("#score26").val());
		}

		if(jQuery("#score27").val()!=""){
			score += parseInt(jQuery("#score27").val());
			sum5 += parseInt(jQuery("#score27").val());
		}
		if(jQuery("#score28").val()!=""){
			score += parseInt(jQuery("#score28").val());
			sum5 += parseInt(jQuery("#score28").val());
		}
		if(jQuery("#score29").val()!=""){
			score += parseInt(jQuery("#score29").val());
			sum5 += parseInt(jQuery("#score29").val());
		}
		if(jQuery("#score30").val()!=""){
			score += parseInt(jQuery("#score30").val());
			sum6 += parseInt(jQuery("#score30").val());
		}
		if(jQuery("#score31").val()!=""){
			score += parseInt(jQuery("#score31").val());
			sum6 += parseInt(jQuery("#score31").val());
		}
		if(jQuery("#score32").val()!=""){
			score += parseInt(jQuery("#score32").val());
			sum6 += parseInt(jQuery("#score32").val());
		}
		if(jQuery("#score33").val()!=""){
			score += parseInt(jQuery("#score33").val());
			sum6 += parseInt(jQuery("#score33").val());
		}

		
		var score_data = jQuery("#score1").val()+"|"+jQuery("#score2").val()+"|"+jQuery("#score3").val()+"|"+jQuery("#score4").val()+"|"+jQuery("#score5").val()+"|"+jQuery("#score6").val()+"|"+jQuery("#score7").val()+"|"+jQuery("#score8").val()+"|"+jQuery("#score9").val()+"|"+jQuery("#score10").val()+"|"+jQuery("#score11").val()+"|"+jQuery("#score12").val()+"|"+jQuery("#score13").val()+"|"+jQuery("#score14").val()+"|"+jQuery("#score15").val()+"|"+jQuery("#score16").val()+"|"+jQuery("#score17").val()+"|"+jQuery("#score18").val()+"|"+jQuery("#score19").val()+"|"+jQuery("#score20").val()+"|"+jQuery("#score21").val()+"|"+jQuery("#score22").val()+"|"+jQuery("#score23").val()+"|"+jQuery("#score24").val()+"|"+jQuery("#score25").val()+"|"+jQuery("#score26").val()+"|"+jQuery("#score27").val()+"|"+jQuery("#score28").val()+"|"+jQuery("#score29").val()+"|"+jQuery("#score30").val()+"|"+jQuery("#score31").val()+"|"+jQuery("#score32").val()+"|"+jQuery("#score33").val();

		console.log(score_data);
		jQuery("#jform_assessment_data").val(score_data);

		if(score>=90){
			jQuery("#farm_score").val(4);
			jQuery("#jform_score_result").val("ดีเลิศ");
		}else if(score>=80){
			jQuery("#farm_score").val(3);
			jQuery("#jform_score_result").val("ดีมาก");
		}else if(score>=70){
			jQuery("#farm_score").val(2);
			jQuery("#jform_score_result").val("ดี");
		}else{
			jQuery("#farm_score").val(1);
			jQuery("#jform_score_result").val("ไม่ดี");
		}

		jQuery("#sum1").val(sum1);
		jQuery("#sum2").val(sum2);
		jQuery("#sum3").val(sum3);
		jQuery("#sum4").val(sum4);
		jQuery("#sum5").val(sum5);
		jQuery("#sum6").val(sum6);

		jQuery("#sum_total").val(score);

		jQuery("#jform_score").val(score);
	}


	decodeScoreData();
	calculateFarmScore();
</script>