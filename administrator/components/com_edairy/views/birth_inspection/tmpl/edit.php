<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_edairy/css/form.css');

if($this->item->birth_date==""){
	$birth_date = date("Y-m-d");
}else{
	$birth_date = $this->item->birth_date;
}

?>

<script src="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript" charset="UTF-8"></script>

<link rel="stylesheet" href="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.css">

<style type="text/css">
	fieldset.radio label {
    width: 200px !important;
}
</style>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {

		js('.datetimepicker').datetimepicker({
            lang:'th',
            timepicker:false,
            format:'d/m/Y'
        });
		
	js('input:hidden.cow_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('cow_idhidden')){
			js('#jform_cow_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_cow_id").trigger("liszt:updated");
	js('input:hidden.child_cow_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('child_cow_idhidden')){
			js('#jform_child_cow_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_child_cow_id").trigger("liszt:updated");
	});

	Joomla.submitbutton = function (task) {
		if (task == 'birth_inspection.cancel') {
			Joomla.submitform(task, document.getElementById('birth_inspection-form'));
		}
		else {
			
			if (task != 'birth_inspection.cancel' && document.formvalidator.isValid(document.id('birth_inspection-form'))) {
				
				Joomla.submitform(task, document.getElementById('birth_inspection-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_edairy&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="birth_inspection-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_EDAIRY_TITLE_BIRTH_INSPECTION', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

									<?php echo $this->form->renderField('id'); ?>
				<?php echo $this->form->renderField('cow_id'); ?>

			<?php
				foreach((array)$this->item->cow_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="cow_id" name="jform[cow_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php //echo $this->form->renderField('birth_date'); ?>

			<div class="control-group">
								<div class="control-label">
									<label>
										วันที่คลอดจริง
									</label>
								</div>
								<div class="controls">
									<input type="text" name="jform[birth_date]" class="datetimepicker"  value="<?php echo formatDate($birth_date); ?>"/>
								</div>
							</div>

				<?php echo $this->form->renderField('birth_method'); ?>
				<?php echo $this->form->renderField('is_child_alive'); ?>
				<?php echo $this->form->renderField('child_gender'); ?>
				<?php echo $this->form->renderField('child_weight'); ?>
				<?php echo $this->form->renderField('child_cow_id'); ?>

			<?php
				foreach((array)$this->item->child_cow_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="child_cow_id" name="jform[child_cow_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('create_by'); ?>
				<div style="display:none">
					<?php echo $this->form->renderField('state'); ?>
				</div>
				<?php echo $this->form->renderField('created_by'); ?>


					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
