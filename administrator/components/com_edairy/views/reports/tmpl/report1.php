<?php 
if($_REQUEST["export"]!="excel"){
?>
<style type="text/css">
	.btn-primary {
		width:150px;
	color: #fff;
	text-shadow: 0 -1px 0 rgba(0,0,0,0.25);
	background-color: #1d6cb0;
	background-image: -moz-linear-gradient(top,#2384d3,#15497c);
	background-image: -webkit-gradient(linear,0 0,0 100%,from(#2384d3),to(#15497c));
	background-image: -webkit-linear-gradient(top,#2384d3,#15497c);
	background-image: -o-linear-gradient(top,#2384d3,#15497c);
	background-image: linear-gradient(to bottom,#2384d3,#15497c);
	background-repeat: repeat-x;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff2384d3', endColorstr='#ff15497c', GradientType=0);
	border-color: #15497c #15497c #0a223b;
	*background-color: #15497c;
	filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
}
.btn-primary:hover,
.btn-primary:focus,
.btn-primary:active,
.btn-primary.active,
.btn-primary.disabled,
.btn-primary[disabled] {
	color: #fff;
	background-color: #15497c;
	*background-color: #113c66;
}
.btn-primary:active,
.btn-primary.active {
	background-color: #0e2f50 \9;
}
button.btn,
input[type="submit"].btn {
	*padding-top: 3px;
	*padding-bottom: 3px;
}
</style>
<script src="/media/jui/js/jquery.min.js"></script>
	<script src="/media/jui/js/chosen.jquery.min.js"></script>
	<link href="/media/jui/css/chosen.css" rel="stylesheet" />

	<script type="text/javascript">
		jQuery(document).ready(function (){
			jQuery('select').chosen();
		});
	</script>

	
<div style="float:right">
	<a href="index.php?option=com_edairy&view=reports&layout=report1&export=excel&month=<?php echo $_REQUEST['month']; ?>&year=<?php echo $_REQUEST['year']; ?>&coop_id=<?php echo $_REQUEST['coop_id']; ?>&farmer_id=<?php echo $_REQUEST['farmer_id']; ?>"><img src="../images/excel.png" width="50"/></a>
</div>
<h3>รายงานรายได้-ค่าใช้จ่ายในการเลี้ยงโค</h3>

<form action="" method="get">
	ประจำเดือน
	<select name="month" style="width:100px">
		<option value="%">ทั้งหมด</option>
		<option value="1"<?php if($_REQUEST['month']==1){ ?> selected<?php } ?>>มกราคม</option>
		<option value="2"<?php if($_REQUEST['month']==2){ ?> selected<?php } ?>>กุมภาพันธ์</option>
		<option value="3"<?php if($_REQUEST['month']==3){ ?> selected<?php } ?>>มีนาคม</option>
		<option value="4"<?php if($_REQUEST['month']==4){ ?> selected<?php } ?>>เมษายน</option>
		<option value="5"<?php if($_REQUEST['month']==5){ ?> selected<?php } ?>>พฤษภาคม</option>
		<option value="6"<?php if($_REQUEST['month']==6){ ?> selected<?php } ?>>มิถุนายน</option>
		<option value="7"<?php if($_REQUEST['month']==7){ ?> selected<?php } ?>>กรกฎาคม</option>
		<option value="8"<?php if($_REQUEST['month']==8){ ?> selected<?php } ?>>สิงหาคม</option>
		<option value="9"<?php if($_REQUEST['month']==9){ ?> selected<?php } ?>>กันยายน</option>
		<option value="10"<?php if($_REQUEST['month']==10){ ?> selected<?php } ?>>ตุลาคม</option>
		<option value="11"<?php if($_REQUEST['month']==11){ ?> selected<?php } ?>>พฤศจิกายน</option>
		<option value="12"<?php if($_REQUEST['month']==12){ ?> selected<?php } ?>>ธันวาคม</option>
	</select>
	&nbsp;&nbsp;
	ปี
	<select name="year" style="width:100px">
		<option value="%">ทั้งหมด</option>
		<?php for($i=date("Y")+543;$i>=2550;$i--){ ?>
			<option value="<?php echo $i; ?>"<?php if($_REQUEST['year']==$i){?> selected<?php } ?>><?php echo $i; ?></option>
		<?php } ?>
	</select>

	&nbsp;&nbsp;

	ศูนย์รับน้ำนมดิบ
	<select name="coop_id" style="width:200px" onChange="javascript: getFarmerInCoop(this.value);">
		<option value="%">-- ทั้งหมด --</option>
		<?php foreach($this->masterData->mas_coop as $key=>$data){ 
				$selected = "";
				if($_REQUEST["coop_id"]==$data->id){
					$selected = " selected";
				}
		?>
			<option value="<?php echo $data->id; ?>"<?php echo $selected; ?>><?php echo $data->coop_code; ?> <?php echo $data->coop_abbr; ?> <?php echo $data->name; ?></option>
		<?php } ?>
	</select>

	&nbsp;&nbsp;

	ชื่อ-รหัส เกษตรกร
	<select name="farmer_id" id="farmer_id">
		<option value="%">-- ทั้งหมด --</option>
		<?php foreach($this->masterData->mas_farmer as $key=>$data){ 
				$selected = "";
				if($_REQUEST["farmer_id"]==$data->id){
					$selected = " selected";
				}
		?>
			<option value="<?php echo $data->id; ?>"<?php echo $selected; ?>><?php echo $data->citizen_id; ?> <?php echo $data->name; ?> <?php echo $data->surname; ?></option>
		<?php } ?>
	</select>

	&nbsp;&nbsp;
	<input type="hidden" name="option" value="com_edairy" />
	<input type="hidden" name="view" value="reports" />
	<input type="hidden" name="layout" value="report1" />
	<input type="submit" class="btn btn-primary" value="ดูรายงาน" />



</form>

<hr />
<?php
}else{
$file="report1.xls";
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=$file");
}

$income1 = array();
$income1_cum = 0;

foreach($this->report->general as $key=>$data){
	$income1[$data->month] = number_format($data->expense1 + $data->expense2, 2);
	$income1_cum += $data->expense1 + $data->expense2;
}


$outcome = array();
$outcome_cum = 0;

$outcome6 = array();
$outcome6_cum = 0;

foreach($this->report->general as $key=>$data){
	$outcome6[$data->month] = number_format($data->expense6, 2);
	$outcome6_cum += $data->expense6;
	$outcome[$data->month] += $data->expense6;
}

$outcome7 = array();
$outcome7_cum = 0;

foreach($this->report->general as $key=>$data){
	$outcome7[$data->month] = number_format($data->expense7, 2);
	$outcome7_cum += $data->expense7;
	$outcome[$data->month] += $data->expense7;
}

$outcome10 = array();
$outcome10_cum = 0;

foreach($this->report->general as $key=>$data){
	$outcome10[$data->month] = number_format($data->expense10, 2);
	$outcome10_cum += $data->expense10;
	$outcome[$data->month] += $data->expense10;
}


$outcome9 = array();
$outcome9_cum = 0;

foreach($this->report->general as $key=>$data){
	$outcome9[$data->month] = number_format($data->expense9, 2);
	$outcome9_cum += $data->expense9;
	$outcome[$data->month] += $data->expense9;
}

$outcome8 = array();
$outcome8_cum = 0;

foreach($this->report->general as $key=>$data){
	$outcome8[$data->month] = number_format($data->expense8, 2);
	$outcome8_cum += $data->expense8;
	$outcome[$data->month] += $data->expense8;
}

$outcome_health = array();
$outcome_health_cum = 0;

foreach($this->report->health as $key=>$data){
	$outcome_health[$data->month] = number_format($data->grand_total, 2);
	$outcome_health_cum += $data->grand_total;
	$outcome[$data->month] += $data->grand_total;
}


$outcome_insemination = array();
$outcome_insemination_cum = 0;

foreach($this->report->insemination as $key=>$data){
	$outcome_insemination[$data->month] = number_format($data->grand_total, 2);
	$outcome_insemination_cum += $data->grand_total;
	$outcome[$data->month] += $data->grand_total;
}

foreach($outcome as $key=>$data){
	$outcome_cum+=$data;
}

$profit = array();
$profit_cum = 0;

for($i=1;$i<=12;$i++){
	$profit[$i] = $income1[$i]-$outcome[$i];
	$profit_cum += $profit[$i];
}


$milk_amount = array();
$milk_amount_cum = 0;

foreach($this->report->milk as $key=>$data){
	$milk_amount[$data->month] = number_format($data->milk_amount, 2);
	$milk_amount_cum += $data->milk_amount;
}


$milk_price = array();
$milk_price_cum = 0;

foreach($this->report->milk as $key=>$data){
	$milk_price[$data->month] = number_format($data->milk_price, 2);
	$milk_price_cum += $data->milk_price;
}


?>
<table width="100%" border="1" cellpadding="3" cellspacing="0">
	<tr>
		<th>รายการ / เดือน</th>
		<th>ม.ค.</th>
		<th>ก.พ.</th>
		<th>มี.ค.</th>
		<th>เม.ย.</th>
		<th>พ.ค.</th>
		<th>มิ.ย.</th>
		<th>ก.ค.</th>
		<th>ส.ค.</th>
		<th>ก.ย.</th>
		<th>ต.ค.</th>
		<th>พ.ย.</th>
		<th>ธ.ค.</th>
		<th>ยอดสะสม</th>
		<th>เฉลี่ยต่อเดือน</th>
	</tr>
	<tr>
		<td>
			<b>1. รายได้ (บาท)</b>
			<br />
			1.1 ขายน้ำนมดิบ 
		</td>
		<td align="right"><?php echo $income1[1]; ?></td>
		<td align="right"><?php echo $income1[2]; ?></td>
		<td align="right"><?php echo $income1[3]; ?></td>
		<td align="right"><?php echo $income1[4]; ?></td>
		<td align="right"><?php echo $income1[5]; ?></td>
		<td align="right"><?php echo $income1[6]; ?></td>
		<td align="right"><?php echo $income1[7]; ?></td>
		<td align="right"><?php echo $income1[8]; ?></td>
		<td align="right"><?php echo $income1[9]; ?></td>
		<td align="right"><?php echo $income1[10]; ?></td>
		<td align="right"><?php echo $income1[11]; ?></td>
		<td align="right"><?php echo $income1[12]; ?></td>
		<td align="right"><?php echo number_format($income1_cum, 2); ?></td>
		<td align="right"><?php echo number_format($income1_cum/12, 2); ?></td>
	</tr>
	<tr>
		<td>
			1.2 ขายพันธ์โคนม
		</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>
			1.3 ขายมูลโค
		</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>
			1.4 อื่นๆ......
		</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr style="background:#DDDDDD">
		<td>
			รวมรายได้
		</td>
		<td align="right"><?php echo $income1[1]; ?></td>
		<td align="right"><?php echo $income1[2]; ?></td>
		<td align="right"><?php echo $income1[3]; ?></td>
		<td align="right"><?php echo $income1[4]; ?></td>
		<td align="right"><?php echo $income1[5]; ?></td>
		<td align="right"><?php echo $income1[6]; ?></td>
		<td align="right"><?php echo $income1[7]; ?></td>
		<td align="right"><?php echo $income1[8]; ?></td>
		<td align="right"><?php echo $income1[9]; ?></td>
		<td align="right"><?php echo $income1[10]; ?></td>
		<td align="right"><?php echo $income1[11]; ?></td>
		<td align="right"><?php echo $income1[12]; ?></td>
		<td align="right"><?php if($income1_cum!=""){ echo number_format($income1_cum, 2); } ?></td>
		<td align="right"><?php if($income1_cum!=""){ echo number_format($income1_cum/12, 2); } ?></td>
	</tr>
	<tr>
		<td>
			<b>2. ค่าใช้จ่าย (บาท)</b>
			<br />
			2.1 ค่าอาหารข้น (วัตถุดิบ)
		</td>
		<td align="right"><?php echo $outcome7[1]; ?></td>
		<td align="right"><?php echo $outcome7[2]; ?></td>
		<td align="right"><?php echo $outcome7[3]; ?></td>
		<td align="right"><?php echo $outcome7[4]; ?></td>
		<td align="right"><?php echo $outcome7[5]; ?></td>
		<td align="right"><?php echo $outcome7[6]; ?></td>
		<td align="right"><?php echo $outcome7[7]; ?></td>
		<td align="right"><?php echo $outcome7[8]; ?></td>
		<td align="right"><?php echo $outcome7[9]; ?></td>
		<td align="right"><?php echo $outcome7[10]; ?></td>
		<td align="right"><?php echo $outcome7[11]; ?></td>
		<td align="right"><?php echo $outcome7[12]; ?></td>
		<td align="right"><?php if($outcome7_cum!=""){ echo number_format($outcome7_cum, 2); } ?></td>
		<td align="right"><?php if($outcome7_cum!=""){ echo number_format($outcome7_cum/12, 2); } ?></td>
	</tr>
	<tr>
		<td>
			2.2 ค่าอาหารข้น (สำเร็จรูป)
		</td>
		<td align="right"><?php echo $outcome6[1]; ?></td>
		<td align="right"><?php echo $outcome6[2]; ?></td>
		<td align="right"><?php echo $outcome6[3]; ?></td>
		<td align="right"><?php echo $outcome6[4]; ?></td>
		<td align="right"><?php echo $outcome6[5]; ?></td>
		<td align="right"><?php echo $outcome6[6]; ?></td>
		<td align="right"><?php echo $outcome6[7]; ?></td>
		<td align="right"><?php echo $outcome6[8]; ?></td>
		<td align="right"><?php echo $outcome6[9]; ?></td>
		<td align="right"><?php echo $outcome6[10]; ?></td>
		<td align="right"><?php echo $outcome6[11]; ?></td>
		<td align="right"><?php echo $outcome6[12]; ?></td>
		<td align="right"><?php if($outcome6_cum!=""){ echo number_format($outcome6_cum, 2); } ?></td>
		<td align="right"><?php if($outcome6_cum!=""){ echo number_format($outcome6_cum/12, 2); } ?></td>
	</tr>
	<tr>
		<td>
			2.3 ค่าแร่ธาตุ
		</td>
		<td align="right"><?php echo $outcome10[1]; ?></td>
		<td align="right"><?php echo $outcome10[2]; ?></td>
		<td align="right"><?php echo $outcome10[3]; ?></td>
		<td align="right"><?php echo $outcome10[4]; ?></td>
		<td align="right"><?php echo $outcome10[5]; ?></td>
		<td align="right"><?php echo $outcome10[6]; ?></td>
		<td align="right"><?php echo $outcome10[7]; ?></td>
		<td align="right"><?php echo $outcome10[8]; ?></td>
		<td align="right"><?php echo $outcome10[9]; ?></td>
		<td align="right"><?php echo $outcome10[10]; ?></td>
		<td align="right"><?php echo $outcome10[11]; ?></td>
		<td align="right"><?php echo $outcome10[12]; ?></td>
		<td align="right"><?php if($outcome10_cum!=""){ echo number_format($outcome10_cum, 2); } ?></td>
		<td align="right"><?php if($outcome10_cum!=""){ echo number_format($outcome10_cum/12, 2); } ?></td>
	
	</tr>
	<tr>
		<td>
			2.4 ค่าอาหารหยาบ
		</td>
		<td align="right"><?php echo $outcome9[1]; ?></td>
		<td align="right"><?php echo $outcome9[2]; ?></td>
		<td align="right"><?php echo $outcome9[3]; ?></td>
		<td align="right"><?php echo $outcome9[4]; ?></td>
		<td align="right"><?php echo $outcome9[5]; ?></td>
		<td align="right"><?php echo $outcome9[6]; ?></td>
		<td align="right"><?php echo $outcome9[7]; ?></td>
		<td align="right"><?php echo $outcome9[8]; ?></td>
		<td align="right"><?php echo $outcome9[9]; ?></td>
		<td align="right"><?php echo $outcome9[10]; ?></td>
		<td align="right"><?php echo $outcome9[11]; ?></td>
		<td align="right"><?php echo $outcome9[12]; ?></td>
		<td align="right"><?php if($outcome9_cum!=""){ echo number_format($outcome9_cum, 2); } ?></td>
		<td align="right"><?php if($outcome9_cum!=""){ echo number_format($outcome9_cum/12, 2); } ?></td>
	</tr>
	<tr>
		<td>
			2.5 ค่านมผงลูกโค
		</td>
		<td align="right"><?php echo $outcome8[1]; ?></td>
		<td align="right"><?php echo $outcome8[2]; ?></td>
		<td align="right"><?php echo $outcome8[3]; ?></td>
		<td align="right"><?php echo $outcome8[4]; ?></td>
		<td align="right"><?php echo $outcome8[5]; ?></td>
		<td align="right"><?php echo $outcome8[6]; ?></td>
		<td align="right"><?php echo $outcome8[7]; ?></td>
		<td align="right"><?php echo $outcome8[8]; ?></td>
		<td align="right"><?php echo $outcome8[9]; ?></td>
		<td align="right"><?php echo $outcome8[10]; ?></td>
		<td align="right"><?php echo $outcome8[11]; ?></td>
		<td align="right"><?php echo $outcome8[12]; ?></td>
		<td align="right"><?php if($outcome8_cum!=""){ echo number_format($outcome8_cum, 2); } ?></td>
		<td align="right"><?php if($outcome8_cum!=""){ echo number_format($outcome8_cum/12, 2); } ?></td>
	</tr>
	<tr>
		<td>
			2.6 ค่าสัตว์แพทย์
		</td>
		<td align="right"><?php echo $outcome_health[1]; ?></td>
		<td align="right"><?php echo $outcome_health[2]; ?></td>
		<td align="right"><?php echo $outcome_health[3]; ?></td>
		<td align="right"><?php echo $outcome_health[4]; ?></td>
		<td align="right"><?php echo $outcome_health[5]; ?></td>
		<td align="right"><?php echo $outcome_health[6]; ?></td>
		<td align="right"><?php echo $outcome_health[7]; ?></td>
		<td align="right"><?php echo $outcome_health[8]; ?></td>
		<td align="right"><?php echo $outcome_health[9]; ?></td>
		<td align="right"><?php echo $outcome_health[10]; ?></td>
		<td align="right"><?php echo $outcome_health[11]; ?></td>
		<td align="right"><?php echo $outcome_health[12]; ?></td>
		<td align="right"><?php if($outcome_health_cum!=""){ echo number_format($outcome_health_cum, 2); } ?></td>
		<td align="right"><?php if($outcome_health_cum!=""){ echo number_format($outcome_health_cum/12, 2); } ?></td>
	</tr>
	<tr>
		<td>
			2.7 ค่าผสมเทียม
		</td>
		<td align="right"><?php echo $outcome_insemination[1]; ?></td>
		<td align="right"><?php echo $outcome_insemination[2]; ?></td>
		<td align="right"><?php echo $outcome_insemination[3]; ?></td>
		<td align="right"><?php echo $outcome_insemination[4]; ?></td>
		<td align="right"><?php echo $outcome_insemination[5]; ?></td>
		<td align="right"><?php echo $outcome_insemination[6]; ?></td>
		<td align="right"><?php echo $outcome_insemination[7]; ?></td>
		<td align="right"><?php echo $outcome_insemination[8]; ?></td>
		<td align="right"><?php echo $outcome_insemination[9]; ?></td>
		<td align="right"><?php echo $outcome_insemination[10]; ?></td>
		<td align="right"><?php echo $outcome_insemination[11]; ?></td>
		<td align="right"><?php echo $outcome_insemination[12]; ?></td>
		<td align="right"><?php if($outcome_insemination_cum!=""){ echo number_format($outcome_insemination_cum, 2); } ?></td>
		<td align="right"><?php if($outcome_insemination_cum!=""){ echo number_format($outcome_insemination_cum/12, 2); } ?></td>
	</tr>
	<tr>
		<td>
			2.8 ค่าบริการตรวจนม
		</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>
			2.9 ค่าขนส่งน้ำนมดิบ
		</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>
			2.10 ค่าน้ำมันเชื้องเพลิง
		</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>
			2.11 ค่าปุ๋ย
		</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>
			2.12 ค่าไฟฟ้า
		</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>
			2.13 ค่าน้ำประปา
		</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>
			2.14 ค่าดอกเบี้ยเงินกู้
		</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>
			2.15 ภาษีหัก ณ ที่จ่าย 1%
		</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>
			2.16 ค่าแรงงานจ้าง
		</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>
			2.17 ค่าแรงงานในครอบครัว
		</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>
			2.18 ค่าซ่อมแซมวัสดุ
		</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>
			2.19 ซื้อพันธ์โคนม
		</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>
			2.20 อื่นๆ
		</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr style="background:#DDDDDD">
		<td>
			รวมค่าใช้จ่าย
		</td>
		<td align="right"><?php if($outcome[1]!=""){ echo number_format($outcome[1], 2); } ?></td>
		<td align="right"><?php if($outcome[2]!=""){ echo number_format($outcome[2], 2); } ?></td>
		<td align="right"><?php if($outcome[3]!=""){ echo number_format($outcome[3], 2); } ?></td>
		<td align="right"><?php if($outcome[4]!=""){ echo number_format($outcome[4], 2); } ?></td>
		<td align="right"><?php if($outcome[5]!=""){ echo number_format($outcome[5], 2); } ?></td>
		<td align="right"><?php if($outcome[6]!=""){ echo number_format($outcome[6], 2); } ?></td>
		<td align="right"><?php if($outcome[7]!=""){ echo number_format($outcome[7], 2); } ?></td>
		<td align="right"><?php if($outcome[8]!=""){ echo number_format($outcome[8], 2); } ?></td>
		<td align="right"><?php if($outcome[9]!=""){ echo number_format($outcome[9], 2); } ?></td>
		<td align="right"><?php if($outcome[10]!=""){ echo number_format($outcome[10], 2); } ?></td>
		<td align="right"><?php if($outcome[11]!=""){ echo number_format($outcome[11], 2); } ?></td>
		<td align="right"><?php if($outcome[12]!=""){ echo number_format($outcome[12], 2); } ?></td>
		<td align="right"><?php if($outcome_cum!=""){ echo number_format($outcome_cum, 2); } ?></td>
		<td align="right"><?php if($outcome_cum!=""){ echo number_format($outcome_cum/12, 2); } ?></td>
	</tr>
	<tr style="background:#DDDDDD">
		<td>
			กำไร
		</td>
		<td align="right"><?php if($profit[1]!=""){ echo number_format($profit[1], 2); } ?></td>
		<td align="right"><?php if($profit[2]!=""){ echo number_format($profit[2], 2); } ?></td>
		<td align="right"><?php if($profit[3]!=""){ echo number_format($profit[3], 2); } ?></td>
		<td align="right"><?php if($profit[4]!=""){ echo number_format($profit[4], 2); } ?></td>
		<td align="right"><?php if($profit[5]!=""){ echo number_format($profit[5], 2); } ?></td>
		<td align="right"><?php if($profit[6]!=""){ echo number_format($profit[6], 2); } ?></td>
		<td align="right"><?php if($profit[7]!=""){ echo number_format($profit[7], 2); } ?></td>
		<td align="right"><?php if($profit[8]!=""){ echo number_format($profit[8], 2); } ?></td>
		<td align="right"><?php if($profit[9]!=""){ echo number_format($profit[9], 2); } ?></td>
		<td align="right"><?php if($profit[10]!=""){ echo number_format($profit[10], 2); } ?></td>
		<td align="right"><?php if($profit[11]!=""){ echo number_format($profit[11], 2); } ?></td>
		<td align="right"><?php if($profit[12]!=""){ echo number_format($profit[12], 2); } ?></td>
		<td align="right"><?php if($profit_cum!=""){ echo number_format($profit_cum, 2); } ?></td>
		<td align="right"><?php if($profit_cum!=""){ echo number_format($profit_cum/12, 2); } ?></td>
	</tr>
	<tr>
		<td>
			ปริมาณน้ำนมดิบ (กก.)
		</td>
		<td align="right"><?php if($milk_amount[1]!=""){ echo number_format($milk_amount[1], 2); } ?></td>
		<td align="right"><?php if($milk_amount[2]!=""){ echo number_format($milk_amount[2], 2); } ?></td>
		<td align="right"><?php if($milk_amount[3]!=""){ echo number_format($milk_amount[3], 2); } ?></td>
		<td align="right"><?php if($milk_amount[4]!=""){ echo number_format($milk_amount[4], 2); } ?></td>
		<td align="right"><?php if($milk_amount[5]!=""){ echo number_format($milk_amount[5], 2); } ?></td>
		<td align="right"><?php if($milk_amount[6]!=""){ echo number_format($milk_amount[6], 2); } ?></td>
		<td align="right"><?php if($milk_amount[7]!=""){ echo number_format($milk_amount[7], 2); } ?></td>
		<td align="right"><?php if($milk_amount[8]!=""){ echo number_format($milk_amount[8], 2); } ?></td>
		<td align="right"><?php if($milk_amount[9]!=""){ echo number_format($milk_amount[9], 2); } ?></td>
		<td align="right"><?php if($milk_amount[10]!=""){ echo number_format($milk_amount[10], 2); } ?></td>
		<td align="right"><?php if($milk_amount[11]!=""){ echo number_format($milk_amount[11], 2); } ?></td>
		<td align="right"><?php if($milk_amount[12]!=""){ echo number_format($milk_amount[12], 2); } ?></td>
		<td align="right"><?php if($milk_amount_cum!=""){ echo number_format($milk_amount_cum, 2); } ?></td>
		<td align="right"><?php if($milk_amount_cum!=""){ echo number_format($milk_amount_cum/12, 2); } ?></td>
	</tr>
	<tr>
		<td>
			ราคาน้ำนมดิบ (บาท/กก.)
		</td>
		<td align="right"><?php if($milk_price[1]!=""){ echo number_format($milk_price[1], 2); } ?></td>
		<td align="right"><?php if($milk_price[2]!=""){ echo number_format($milk_price[2], 2); } ?></td>
		<td align="right"><?php if($milk_price[3]!=""){ echo number_format($milk_price[3], 2); } ?></td>
		<td align="right"><?php if($milk_price[4]!=""){ echo number_format($milk_price[4], 2); } ?></td>
		<td align="right"><?php if($milk_price[5]!=""){ echo number_format($milk_price[5], 2); } ?></td>
		<td align="right"><?php if($milk_price[6]!=""){ echo number_format($milk_price[6], 2); } ?></td>
		<td align="right"><?php if($milk_price[7]!=""){ echo number_format($milk_price[7], 2); } ?></td>
		<td align="right"><?php if($milk_price[8]!=""){ echo number_format($milk_price[8], 2); } ?></td>
		<td align="right"><?php if($milk_price[9]!=""){ echo number_format($milk_price[9], 2); } ?></td>
		<td align="right"><?php if($milk_price[10]!=""){ echo number_format($milk_price[10], 2); } ?></td>
		<td align="right"><?php if($milk_price[11]!=""){ echo number_format($milk_price[11], 2); } ?></td>
		<td align="right"><?php if($milk_price[12]!=""){ echo number_format($milk_price[12], 2); } ?></td>
		<td align="right"><?php echo number_format($milk_price_cum, 2);  ?></td>
		<td align="right"><?php echo number_format($milk_price_cum/12, 2);  ?></td>
	</tr>
	<tr>
		<td>
			*ต้นทุนผันแปร (บาท/นม 1 กก.)
		</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
</table>
<br />
*ต้นทุนผันแปร (บาท/นม 1 กก.) หมายถึง ต้นทุน(บาท)ต่อการผลิตน้ำนมดิบ 1 กิโลกรัม ทั้งนี้ไม่ได้คิดค่าเสื่อมโรงเรือนและตัวโค

<script type="text/javascript">
	function getFarmerInCoop(coop_id){
		jQuery.get("index.php?option=com_edairy&task=reports.getFarmerInCoop&coop_id="+coop_id, function(data){
			data = jQuery.parseJSON(data);
			console.log(data);
			jQuery("#farmer_id").empty();
			jQuery('#farmer_id').append("<option value=''>-- ทั้งหมด --</option>");
			for(var i=0;i<data.length;i++){
				jQuery('#farmer_id').append("<option value='"+data[i].id+"'>"+data[i].citizen_id+" "+data[i].name+" "+data[i].surname+"</option>");
			}
			jQuery('#farmer_id').trigger("liszt:updated");
		});
	}
</script>
<?php 

die();
?>