<?php 
if($_REQUEST["export"]!="excel"){
?>
<style type="text/css">
	.btn-primary {
		width:150px;
	color: #fff;
	text-shadow: 0 -1px 0 rgba(0,0,0,0.25);
	background-color: #1d6cb0;
	background-image: -moz-linear-gradient(top,#2384d3,#15497c);
	background-image: -webkit-gradient(linear,0 0,0 100%,from(#2384d3),to(#15497c));
	background-image: -webkit-linear-gradient(top,#2384d3,#15497c);
	background-image: -o-linear-gradient(top,#2384d3,#15497c);
	background-image: linear-gradient(to bottom,#2384d3,#15497c);
	background-repeat: repeat-x;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff2384d3', endColorstr='#ff15497c', GradientType=0);
	border-color: #15497c #15497c #0a223b;
	*background-color: #15497c;
	filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
}
.btn-primary:hover,
.btn-primary:focus,
.btn-primary:active,
.btn-primary.active,
.btn-primary.disabled,
.btn-primary[disabled] {
	color: #fff;
	background-color: #15497c;
	*background-color: #113c66;
}
.btn-primary:active,
.btn-primary.active {
	background-color: #0e2f50 \9;
}
button.btn,
input[type="submit"].btn {
	*padding-top: 3px;
	*padding-bottom: 3px;
}
</style>
<script src="/media/jui/js/jquery.min.js"></script>
	<script src="/media/jui/js/chosen.jquery.min.js"></script>
	<link href="/media/jui/css/chosen.css" rel="stylesheet" />

	<script type="text/javascript">
		jQuery(document).ready(function (){
			jQuery('select').chosen();
		});
	</script>
<div style="float:right">
	<a href="index.php?option=com_edairy&view=reports&layout=report5&export=excel&region_id=<?php echo $_REQUEST['region_id']; ?>&province_id=<?php echo $_REQUEST['province_id']; ?>&coop_id=<?php echo $_REQUEST['coop_id']; ?>&farmer_id=<?php echo $_REQUEST['farmer_id']; ?>"><img src="../images/excel.png" width="50"/></a>
</div>
<h3>รายงานสำรวจประชากรโค</h3>

<form action="" method="get">
	ภาค
	<select name="region_id">
		<option value="%">-- ทั้งหมด --</option>
		<?php foreach($this->masterData->mas_region as $key=>$data){ 
				$selected = "";
				if($_REQUEST["region_id"]==$data->id){
					$selected = " selected";
				}
		?>
			<option value="<?php echo $data->id; ?>"<?php echo $selected; ?>><?php echo $data->name; ?></option>
		<?php } ?>
	</select>

	&nbsp;&nbsp;

	จังหวัด
	<select name="province_id" style="width:150px">
		<option value="%">-- ทั้งหมด --</option>
		<?php foreach($this->masterData->mas_province as $key=>$data){ 
				$selected = "";
				if($_REQUEST["province_id"]==$data->id){
					$selected = " selected";
				}
		?>
			<option value="<?php echo $data->id; ?>"<?php echo $selected; ?>><?php echo $data->name; ?></option>
		<?php } ?>
	</select>


	&nbsp;&nbsp;
	ศูนย์รับน้ำนมดิบ

	<select name="coop_id" style="width:200px" onChange="javascript: getFarmerInCoop(this.value);">
		<option value="%">-- ทั้งหมด --</option>
		<?php foreach($this->masterData->mas_coop as $key=>$data){ 
				$selected = "";
				if($_REQUEST["coop_id"]==$data->id){
					$selected = " selected";
				}
		?>
			<option value="<?php echo $data->id; ?>"<?php echo $selected; ?>><?php echo $data->coop_code; ?> <?php echo $data->coop_abbr; ?> <?php echo $data->name; ?></option>
		<?php } ?>
	</select>

	
	&nbsp;&nbsp;
	ชื่อ-รหัสเกษตรกร

	<select id="farmer_id" name="farmer_id">
		<option value="%">-- ทั้งหมด --</option>
		<?php foreach($this->masterData->mas_farmer as $key=>$data){ 
				$selected = "";
				if($_REQUEST["farmer_id"]==$data->id){
					$selected = " selected";
				}
		?>
			<option value="<?php echo $data->id; ?>"<?php echo $selected; ?>><?php echo $data->citizen_id; ?> <?php echo $data->name; ?> <?php echo $data->surname; ?></option>
		<?php } ?>
	</select>

	

	
	&nbsp;&nbsp;
	<input type="hidden" name="option" value="com_edairy" />
	<input type="hidden" name="view" value="reports" />
	<input type="hidden" name="layout" value="report5" />
	<input type="submit" class="btn btn-primary" value="ดูรายงาน" />

</form>
<hr />
<?php
}else{
	$file="report5.xls";
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=$file");
}

?>
<table width="100%" border="1" cellpadding="3" cellspacing="0">
	<tr>
		<th rowspan="2">รหัส-ชื่อเกษตรกร</th>
		<th colspan="2">โครีดนม</th>
		<th colspan="2">จำนวนโคหยุดรีด</th>
		<th rowspan="2">แม่โครวม (ตัว)</th>
		<th colspan="3">โคเพศเมียไม่ท้อง</th>
		<th colspan="2">โคสาว</th>
		<th rowspan="2">โคเพศผู้</th>
		<th rowspan="2">รวมทั้งสิ้น (ตัว)</th>
	</tr>
	<tr>
		<th>ท้อง</th>
		<th>ไม่ท้อง</th>
		<th>ท้อง</th>
		<th>ไม่ท้อง</th>
		<th>0-4 เดือน</th>
		<th>4 เดือน-1 ปี</th>
		<th>1-2 ปี</th>
		<th>ท้อง</th>
		<th>ไม้ท้อง อายุ 2 ปีขึ้นไป</th>
	</tr>
	<?php 
		$total_cow_sum = 0;
		$total_male_cow_sum = 0;
		$total_female_cow_sum = 0;
		$total_PL_cow_sum = 0;
		$total_L_cow_sum = 0;
		$total_PD_cow_sum = 0;
		$total_D_cow_sum = 0;

		$total_H_cow_sum = 0;
		$total_PH_cow_sum = 0;

		$H_range1_sum = 0;
		$H_range2_sum = 0;
		$H_range3_sum = 0;

		$PL_array = array(6,10,14,18,22,26,30);
		$L_array = array(4,8,12,16,20,24,28);
		$PD_array = array(7,11,15,19,23,27);
		$D_array = array(5,9,13,17,25,29);

		foreach($this->report as $key=>$data){ 

			$total_cow_sum+=count($data->cow);

			$total_female_cow = 0;
			$total_male_cow = 0;
			$total_PL_cow = 0;
			$total_L_cow = 0;
			$total_PD_cow = 0;
			$total_D_cow = 0;

			$total_H_cow = 0;
			$total_PH_cow = 0;

			$H_range1 = 0;
			$H_range2 = 0;
			$H_range3 = 0;


			foreach($data->cow as $ckey=>$cdata){
				if($cdata->gender == 2){
					$total_female_cow++;
					$total_female_cow_sum++;
				}else{
					$total_male_cow++;
					$total_male_cow_sum++;
				}

				if(in_array($cdata->cow_status_id, $PL_array)){
					$total_PL_cow++;
					$total_PL_cow_sum++;
				}

				if(in_array($cdata->cow_status_id, $L_array)){
					$total_L_cow++;
					$total_L_cow_sum++;
				}

				if(in_array($cdata->cow_status_id, $PD_array)){
					$total_PD_cow++;
					$total_PD_cow_sum++;
				}

				if(in_array($cdata->cow_status_id, $D_array)){
					$total_D_cow++;
					$total_D_cow_sum++;
				}

				if($cdata->cow_status_id==3 && $cdata->gender == 2){
					$total_PH_cow++;
					$total_PH_cow_sum++;
				}

				

				$from = new DateTime($cdata->birthdate);
				$to   = new DateTime('today');
				//echo $from->diff($to)->y . "<br />";

				

				

				if($cdata->cow_status_id==2 && $cdata->gender == 2){
					if($cdata->birthdate!="0000-00-00"){
						if($from->diff($to)->y>=2){
							$total_H_cow++;
							$total_H_cow_sum++;
						}

						if($from->diff($to)->m>=0 && $from->diff($to)->m<=4){
							$H_range1++;
							$H_range1_sum++;
						}
						if($from->diff($to)->m>4 && $from->diff($to)->m<=12){
							$H_range2++;
							$H_range2_sum++;
						}
						if($from->diff($to)->m>12 && $from->diff($to)->m<=24){
							$H_range3++;
							$H_range3_sum++;
						}
					}

				}
			}
	?>
		<tr>
			<td><?php echo $data->citizen_id; ?>  <?php echo $data->name; ?> <?php echo $data->surname; ?></td>
			<td align="right"><?php echo $total_PL_cow; ?></td>
			<td align="right"><?php echo $total_L_cow; ?></td>
			<td align="right"><?php echo $total_PD_cow; ?></td>
			<td align="right"><?php echo $total_D_cow; ?></td>
			<td align="right"><?php echo $total_female_cow; ?></td>
			<td align="right"><?php echo $H_range1; ?></td>
			<td align="right"><?php echo $H_range2; ?></td>
			<td align="right"><?php echo $H_range3; ?></td>
			<td align="right"><?php echo $total_PH_cow; ?></td>
			<td align="right"><?php echo $total_H_cow; ?></td>
			<td align="right"><?php echo $total_male_cow; ?></td>
			<td align="right"><?php echo count($data->cow); ?></td>
		</tr>
	<?php } ?>
	<tr>
			<td align="center"><b>รวม</b></td>
			<td align="right"><b><?php echo $total_PL_cow_sum; ?></b></td>
			<td align="right"><b><?php echo $total_L_cow_sum; ?></b></td>
			<td align="right"><b><?php echo $total_PD_cow_sum; ?></b></td>
			<td align="right"><b><?php echo $total_D_cow_sum; ?></b></td>
			<td align="right"><b><?php echo $total_female_cow_sum; ?></b></td>
			<td align="right"><b><?php echo $H_range1_sum; ?></b></td>
			<td align="right"><b><?php echo $H_range2_sum; ?></b></td>
			<td align="right"><b><?php echo $H_range3_sum; ?></b></td>
			<td align="right"><b><?php echo $total_PH_cow_sum; ?></b></td>
			<td align="right"><b><?php echo $total_H_cow_sum; ?></b></td>
			<td align="right"><b><?php echo $total_male_cow_sum; ?></b></td>
			<td align="right"><b><?php echo $total_cow_sum; ?></b></td>
		</tr>
</table>

<script type="text/javascript">
	function getFarmerInCoop(coop_id){
		jQuery.get("index.php?option=com_edairy&task=reports.getFarmerInCoop&coop_id="+coop_id, function(data){
			data = jQuery.parseJSON(data);
			console.log(data);
			jQuery("#farmer_id").empty();
			jQuery('#farmer_id').append("<option value=''>-- ทั้งหมด --</option>");
			for(var i=0;i<data.length;i++){
				jQuery('#farmer_id').append("<option value='"+data[i].id+"'>"+data[i].citizen_id+" "+data[i].name+" "+data[i].surname+"</option>");
			}
			jQuery('#farmer_id').trigger("liszt:updated");
		});
	}
</script>
<?php
die();
?>