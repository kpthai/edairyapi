<?php 
if($_REQUEST["export"]!="excel"){
?>
<style type="text/css">
	.btn-primary {
		width:150px;
	color: #fff;
	text-shadow: 0 -1px 0 rgba(0,0,0,0.25);
	background-color: #1d6cb0;
	background-image: -moz-linear-gradient(top,#2384d3,#15497c);
	background-image: -webkit-gradient(linear,0 0,0 100%,from(#2384d3),to(#15497c));
	background-image: -webkit-linear-gradient(top,#2384d3,#15497c);
	background-image: -o-linear-gradient(top,#2384d3,#15497c);
	background-image: linear-gradient(to bottom,#2384d3,#15497c);
	background-repeat: repeat-x;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff2384d3', endColorstr='#ff15497c', GradientType=0);
	border-color: #15497c #15497c #0a223b;
	*background-color: #15497c;
	filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
}
.btn-primary:hover,
.btn-primary:focus,
.btn-primary:active,
.btn-primary.active,
.btn-primary.disabled,
.btn-primary[disabled] {
	color: #fff;
	background-color: #15497c;
	*background-color: #113c66;
}
.btn-primary:active,
.btn-primary.active {
	background-color: #0e2f50 \9;
}
button.btn,
input[type="submit"].btn {
	*padding-top: 3px;
	*padding-bottom: 3px;
}
</style>

<script src="/media/jui/js/jquery.min.js"></script>
	<script src="/media/jui/js/chosen.jquery.min.js"></script>
	<link href="/media/jui/css/chosen.css" rel="stylesheet" />
	<script src="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript" charset="UTF-8"></script>

<link rel="stylesheet" href="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.css">
	<script type="text/javascript">
		jQuery(document).ready(function (){
			jQuery('select').chosen();

			jQuery('.datetimepicker').datetimepicker({
            lang:'th',
            timepicker:false,
            format:'d/m/Y'
        });
			
		});

	</script>
<div style="float:right">
	<a href="index.php?option=com_edairy&view=reports&layout=report11&export=excel"><img src="../images/excel.png" width="50"/></a>
</div>
<h3>รายงานแสดงผลการผลิต</h3>

<form action="" method="get">
	ประจำเดือน
	<select name="month" style="width:100px">
		<option value="1">มกราคม</option>
		<option value="2">กุมภาพันธ์</option>
		<option value="3">มีนาคม</option>
		<option value="4">เมษายน</option>
		<option value="5">พฤษภาคม</option>
		<option value="6">มิถุนายน</option>
		<option value="7">กรกฎาคม</option>
		<option value="8">สิงหาคม</option>
		<option value="9">กันยายน</option>
		<option value="10">ตุลาคม</option>
		<option value="11">พฤศจิกายน</option>
		<option value="12">ธันวาคม</option>
	</select>
	&nbsp;&nbsp;
	ปี
	<select name="year" style="width:100px">
		<?php for($i=date("Y")+543;$i>=2550;$i--){ ?>
			<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
		<?php } ?>
	</select>

	&nbsp;&nbsp;

	ศูนย์รับน้ำนมดิบ
	<select name="coop_id" style="width:200px">
		<option value="">-- เลือกศูนย์รับน้ำนมดิบ --</option>
		<?php foreach($this->masterData->mas_coop as $key=>$data){ ?>
			<option value="<?php echo $data->id; ?>"><?php echo $data->coop_code; ?> <?php echo $data->coop_abbr; ?> <?php echo $data->name; ?></option>
		<?php } ?>
	</select>

	&nbsp;&nbsp;
	<input type="submit" class="btn btn-primary" value="ดูรายงาน" />

</form>
<hr />
<?php
}else{
	$file="report11.xls";
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=$file");
}
?>
<table width="100%" border="1" cellpadding="3" cellspacing="0">
	<tr>
		<th>ลำดับ</th>
		<th>หมายเลขสมาชิก</th>
		<th>ชื่อเกษตรกร</th>
		<th>ปริมาณน้ำนมได้มาตรฐาน (กก.)</th>
		<th>ต่ำกว่ามาตรฐาน (ถัง)</th>
	</tr>
	<tr>
		<td colspan="3" align="center">รวม</td>
		<td></td>
		<td></td>
	</tr>
	
</table>
<?php 

die();
?>