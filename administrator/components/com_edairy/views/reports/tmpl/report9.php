<?php 
if($_REQUEST["export"]!="excel"){
?>
<style type="text/css">
	.btn-primary {
		width:150px;
	color: #fff;
	text-shadow: 0 -1px 0 rgba(0,0,0,0.25);
	background-color: #1d6cb0;
	background-image: -moz-linear-gradient(top,#2384d3,#15497c);
	background-image: -webkit-gradient(linear,0 0,0 100%,from(#2384d3),to(#15497c));
	background-image: -webkit-linear-gradient(top,#2384d3,#15497c);
	background-image: -o-linear-gradient(top,#2384d3,#15497c);
	background-image: linear-gradient(to bottom,#2384d3,#15497c);
	background-repeat: repeat-x;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff2384d3', endColorstr='#ff15497c', GradientType=0);
	border-color: #15497c #15497c #0a223b;
	*background-color: #15497c;
	filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
}
.btn-primary:hover,
.btn-primary:focus,
.btn-primary:active,
.btn-primary.active,
.btn-primary.disabled,
.btn-primary[disabled] {
	color: #fff;
	background-color: #15497c;
	*background-color: #113c66;
}
.btn-primary:active,
.btn-primary.active {
	background-color: #0e2f50 \9;
}
button.btn,
input[type="submit"].btn {
	*padding-top: 3px;
	*padding-bottom: 3px;
}
</style>

<script src="/media/jui/js/jquery.min.js"></script>
	<script src="/media/jui/js/chosen.jquery.min.js"></script>
	<link href="/media/jui/css/chosen.css" rel="stylesheet" />
	<script src="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript" charset="UTF-8"></script>

<link rel="stylesheet" href="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.css">
	<script type="text/javascript">
		jQuery(document).ready(function (){
			jQuery('select').chosen();

			jQuery('.datetimepicker').datetimepicker({
            lang:'th',
            timepicker:false,
            format:'d/m/Y'
        });
			
		});

	</script>

<div style="float:right">
	<a href="index.php?option=com_edairy&view=reports&layout=report9&export=excel"><img src="../images/excel.png" width="50"/></a>
</div>
<h3>รายงานประสิทธิภาพการผลิตของฟาร์มสมาชิก</h3>

<form action="" method="get">
	ประจำเดือน
	<select name="month" style="width:100px">
		<option value="1">มกราคม</option>
		<option value="2">กุมภาพันธ์</option>
		<option value="3">มีนาคม</option>
		<option value="4">เมษายน</option>
		<option value="5">พฤษภาคม</option>
		<option value="6">มิถุนายน</option>
		<option value="7">กรกฎาคม</option>
		<option value="8">สิงหาคม</option>
		<option value="9">กันยายน</option>
		<option value="10">ตุลาคม</option>
		<option value="11">พฤศจิกายน</option>
		<option value="12">ธันวาคม</option>
	</select>
	&nbsp;&nbsp;
	ปี
	<select name="year" style="width:100px">
		<?php for($i=date("Y")+543;$i>=2550;$i--){ ?>
			<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
		<?php } ?>
	</select>

	&nbsp;&nbsp;

	ศูนย์รับน้ำนมดิบ
	<select name="coop_id" style="width:200px" onChange="javascript: getFarmerInCoop(this.value);">
		<option value="">-- เลือกศูนย์รับน้ำนมดิบ --</option>
		<?php foreach($this->masterData->mas_coop as $key=>$data){ ?>
			<option value="<?php echo $data->id; ?>"><?php echo $data->coop_code; ?> <?php echo $data->coop_abbr; ?> <?php echo $data->name; ?></option>
		<?php } ?>
	</select>
	&nbsp;&nbsp;

	ชื่อ-รหัส เกษตรกร
	<select id="farmer_id" name="farmer_id">
		<option value="">-- ทั้งหมด --</option>
		<?php foreach($this->masterData->mas_farmer as $key=>$data){ ?>
			<option value="<?php echo $data->id; ?>"><?php echo $data->citizen_id; ?> <?php echo $data->name; ?> <?php echo $data->surname; ?></option>
		<?php } ?>
	</select>


	&nbsp;&nbsp;
	<input type="submit" class="btn btn-primary" value="ดูรายงาน" />

</form>
<hr />
<?php
}else{
	$file="report9.xls";
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=$file");
}
?>
<table width="100%" border="1" cellpadding="3" cellspacing="0">
	<tr>
		<th colspan="3"><b>ประชากรโค</b></th>
		<th><b>มาตรฐาน</b></th>
		<th><b>เฉลี่ยฟาร์ม</b></th>
		<th><b>เฉลี่ยศูนย์</b></th>
		<th colspan="3"><b>การผสมพันธ์</b></th>
		<th><b>มาตรฐาน</b></th>
		<th><b>เฉลี่ยฟาร์ม</b></th>
		<th><b>เฉลี่ยศูนย์</b></th>
	</tr>
	<tr>
		<td>1. แม่โค</td>
		<td align="right">151</td>
		<td>ตัว</td>
		<td></td>
		<td></td>
		<td></td>
		<td colspan="3">1. วันท้องว่างเฉลี่ย</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>1.1 ท้องให้นม</td>
		<td align="right">9</td>
		<td>ตัว</td>
		<td></td>
		<td></td>
		<td></td>
		<td>1.1 แม่โค</td>
		<td align="right">40</td>
		<td>ตัว ผสมติดแล้วมีวันท้องว่างเฉลี่ย (วัน)</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>1.2 ไม่ท้องให้นม</td>
		<td align="right">65</td>
		<td>ตัว</td>
		<td></td>
		<td></td>
		<td></td>
		<td>1.2 แม่โค</td>
		<td align="right">111</td>
		<td>ตัว ผสมยังไม่ติดมีวันท้องว่างเฉลี่ย (วัน)</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>1.3 ท้องหยุดรีดนม</td>
		<td align="right">65</td>
		<td>ตัว</td>
		<td></td>
		<td></td>
		<td></td>
		<td>2. แม่โค</td>
		<td align="right">111</td>
		<td>ตัว คลอดครั้งล่าสุดมีระยะห่างการให้ลูก (วัน)</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>1.4 ไม่ท้องหยุดรีดนม</td>
		<td align="right">65</td>
		<td>ตัว</td>
		<td></td>
		<td></td>
		<td></td>
		<td>3. แม่โค</td>
		<td align="right">111</td>
		<td>ตัว จำนวนครั้งที่ผสมติดเฉลี่ย (ครั้ง)</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>2. โคสาวคลอดครั้งแรก</td>
		<td align="right">65</td>
		<td>ตัว อายุเฉลี่ยเมื่อคลอด (ปี/เดือน)</td>
		<td></td>
		<td></td>
		<td></td>
		<td>4. โคสาว</td>
		<td align="right">111</td>
		<td>ตัว อัตราการผสมติดครั้งแรกในโคสาว (%)</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>3. โคสาวท้อง</td>
		<td align="right">65</td>
		<td>ตัว อายุเฉลี่ยเมื่อผสมติด (ปี/เดือน)</td>
		<td></td>
		<td></td>
		<td></td>
		<td>5. โคสาว</td>
		<td align="right">111</td>
		<td>ตัว อัตราการผสมติดในโคสาว (%)</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>4. โคสาวผสมยังไม่ท้อง</td>
		<td align="right">65</td>
		<td>ตัว อายุเฉลี่ย (ปี/เดือน)</td>
		<td></td>
		<td></td>
		<td></td>
		<td>6. แม่โค</td>
		<td align="right">111</td>
		<td>ตัว อัตราการผสมติดครั้งแรกในแม่โค (%)</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>5. โคสาวยังไม่ได้ผสม</td>
		<td align="right">74</td>
		<td>ตัว</td>
		<td></td>
		<td></td>
		<td></td>
		<td>7. แม่โค</td>
		<td align="right">111</td>
		<td>ตัว อัตราการผสมติดในแม่โค (%)</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>6. โคเพศเมียเกิดใหม่</td>
		<td align="right">74</td>
		<td>ตัว</td>
		<td></td>
		<td></td>
		<td></td>
		<td>8. ลูกโคตายแรกเกิด</td>
		<td align="right">111</td>
		<td>ตัว</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>7. โคเพศผู้เกิดใหม่</td>
		<td align="right">74</td>
		<td>ตัว</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<th colspan="3"><b>การให้นม</b></th>
		<th><b>มาตรฐาน</b></th>
		<th><b>เฉลี่ยฟาร์ม</b></th>
		<th><b>เฉลี่ยศูนย์</b></th>
		<th colspan="3"><b>การคัดทิ้ง</b></th>
		<th><b>มาตรฐาน</b></th>
		<th><b>เฉลี่ยฟาร์ม</b></th>
		<th><b>เฉลี่ยศูนย์</b></th>
	</tr>
	<tr>
		<td>1. แม่โค</td>
		<td align="right">65</td>
		<td>ตัว รีดนมอยู่ มีเฉลี่ยวันรีดนม (วัน)</td>
		<td></td>
		<td></td>
		<td></td>
		<td>1. แม่โคคัดทิ้งในช่วง 1 ปีที่ผ่านมา มี</td>
		<td align="right">111</td>
		<td>ตัว</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>2. แม่โค</td>
		<td align="right">65</td>
		<td>ตัว หยุดรีดนม มีช่วงการให้นมเฉลี่ยได้ (วัน)</td>
		<td></td>
		<td></td>
		<td></td>
		<td>คิดเป็นอัตราการคัดทิ้ง (เปอร์เซ็นต์/ปี)</td>
		<td align="right"></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>3. แม่โค</td>
		<td align="right">65</td>
		<td>ตัว คลอดครั้งล่าสุดมีระยะหยุดรีดนมเฉลี่ยได้ (วัน)</td>
		<td></td>
		<td></td>
		<td></td>
		<td>2. ตั้งใจคัดทิ้ง</td>
		<td align="right">111</td>
		<td>ตัว คิดเป็นเปอร์เซนต์</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>4. แม่โค</td>
		<td align="right">65</td>
		<td>ตัว กำลังให้นมได้รับจดบันทึกน้ำหนักนม (ตัว)</td>
		<td></td>
		<td></td>
		<td></td>
		<td>3. ไม่ตั้งใจคัดทิ้ง</td>
		<td align="right">111</td>
		<td>ตัว คิดเป็นเปอร์เซนต์</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>5. ปริมาณน้ำนมเฉลี่ย/ตัว/วัน(กิโลกรัม)</td>
		<td align="right"></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td>4. อายุเฉลี่ยโคคัดทิ้ง (ปี/เดือน)</td>
		<td align="right"></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
</table>
<br /><br />
<table width="100%" border="1" cellpadding="3" cellspacing="0">
	<tr>
		<th rowspan="2">
			สัดส่วนฝูงโค
		</th>
		<th colspan="2">
			ฟาร์ม
		</th>
		<th colspan="2">
			ศูนย์/สหกรณ์
		</th>
		<th rowspan="5">
			<b>ข้อมูลด้านคุณภาพน้ำนม วันที่ตรวจ</b><br />
			<table>
				<tr>
					<td colspan="2">เกรดนม(MB)</td>
					<td>SCC</td>
				</tr>
				<tr>
					<td>F %</td>
					<td>P %</td>
					<td>L %</td>
				</tr>
				<tr>
					<td>SNF %</td>
					<td>TS %</td>
				</tr>
			</table>
		</th>
		<th rowspan="5">
			<b>ข้อมูลด้านต้นทุนเดือน</b><br />
			<b>ราคาเฉลี่ยน้ำนมดิบ บาท/กก.</b>
			<b>ต้นทุนการผลิต บาท/กก.</b>
		</th>
	</tr>
	<tr>
		<th>ตัว</th>
		<th>%</th>
		<th>ตัว</th>
		<th>%</th>
	</tr>
	<tr>
		<td>โคทั้งหมด</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>แม่โค</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>โคทดแทน</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
</table>

<script type="text/javascript">
	function getFarmerInCoop(coop_id){
		jQuery.get("index.php?option=com_edairy&task=reports.getFarmerInCoop&coop_id="+coop_id, function(data){
			data = jQuery.parseJSON(data);
			console.log(data);
			jQuery("#farmer_id").empty();
			jQuery('#farmer_id').append("<option value=''>-- ทั้งหมด --</option>");
			for(var i=0;i<data.length;i++){
				jQuery('#farmer_id').append("<option value='"+data[i].id+"'>"+data[i].citizen_id+" "+data[i].name+" "+data[i].surname+"</option>");
			}
			jQuery('#farmer_id').trigger("liszt:updated");
		});
	}
</script>
<?php
die();
?>