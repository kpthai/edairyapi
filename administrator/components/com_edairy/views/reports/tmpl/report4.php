<?php 
if($_REQUEST["export"]!="excel"){
?>
<style type="text/css">
	.btn-primary {
		width:150px;
	color: #fff;
	text-shadow: 0 -1px 0 rgba(0,0,0,0.25);
	background-color: #1d6cb0;
	background-image: -moz-linear-gradient(top,#2384d3,#15497c);
	background-image: -webkit-gradient(linear,0 0,0 100%,from(#2384d3),to(#15497c));
	background-image: -webkit-linear-gradient(top,#2384d3,#15497c);
	background-image: -o-linear-gradient(top,#2384d3,#15497c);
	background-image: linear-gradient(to bottom,#2384d3,#15497c);
	background-repeat: repeat-x;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff2384d3', endColorstr='#ff15497c', GradientType=0);
	border-color: #15497c #15497c #0a223b;
	*background-color: #15497c;
	filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
}
.btn-primary:hover,
.btn-primary:focus,
.btn-primary:active,
.btn-primary.active,
.btn-primary.disabled,
.btn-primary[disabled] {
	color: #fff;
	background-color: #15497c;
	*background-color: #113c66;
}
.btn-primary:active,
.btn-primary.active {
	background-color: #0e2f50 \9;
}
button.btn,
input[type="submit"].btn {
	*padding-top: 3px;
	*padding-bottom: 3px;
}
</style>

<script src="/media/jui/js/jquery.min.js"></script>
	<script src="/media/jui/js/chosen.jquery.min.js"></script>
	<link href="/media/jui/css/chosen.css" rel="stylesheet" />
	<script src="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript" charset="UTF-8"></script>

<link rel="stylesheet" href="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.css">
	<script type="text/javascript">
		jQuery(document).ready(function (){
			jQuery('select').chosen();

			jQuery('.datetimepicker').datetimepicker({
            lang:'th',
            timepicker:false,
            format:'d/m/Y'
        });
			
		});

	</script>
<div style="float:right">
	<a href="index.php?option=com_edairy&view=reports&layout=report4&export=excel"><img src="../images/excel.png" width="50"/></a>
</div>
<h3>รายงานการปฎิบัติงานผสมเทียมและตรวจท้อง</h3>

<form action="" method="get">
	ศูนย์รับน้ำนมดิบ
	<select name="coop_id" style="width:200px">
		<option value="">-- เลือกศูนย์รับน้ำนมดิบ --</option>
		<?php foreach($this->masterData->mas_coop as $key=>$data){ ?>
			<option value="<?php echo $data->id; ?>"><?php echo $data->coop_code; ?> <?php echo $data->coop_abbr; ?> <?php echo $data->name; ?></option>
		<?php } ?>
	</select>

	&nbsp;&nbsp;

	นักส่งเสริมผู้ให้บริการ
	<select name="vet_id" style="width:250px">
		<option value="">-- เลือกนักส่งเสริม --</option>
		<?php foreach($this->masterData->mas_vet as $key=>$data){ ?>
			<option value="<?php echo $data->id; ?>"><?php echo $data->name; ?></option>
		<?php } ?>
	</select>


		&nbsp;&nbsp;
	วันที่
	<input type="text" name="date_from" class="datetimepicker" />

	ถึง

	<input type="text" name="date_to" class="datetimepicker" />

	

	
	

	
	&nbsp;&nbsp;
	<input type="submit" class="btn btn-primary" value="ดูรายงาน" />

</form>
<hr />
<?php
}else{
	$file="report4.xls";
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=$file");
}
?>
<table width="100%" border="1" cellpadding="3" cellspacing="0">
	<tr>
		<th rowspan="2" align="center">รายการ</th>
		<th colspan="2">การแจ้งในบันทึก </th>
		<th colspan="2">ปฎิบัติงานได้</th>
		<th colspan="2">ค่าดำเนินการ</th>
		<th rowspan="2" colspan="2">รวม (บาท)</th>
	</tr>
	<tr>
		<th>สมาชิก (ราย)</th>
		<th>โคนม (ราย)</th>
		<th>สมาชิก (ราย)</th>
		<th>โคนม (ราย)</th>
		<th>ค่าเวชภัณฑ์</th>
		<th>ค่าบริการ</th>
	</tr>
	<tr>
		<td>1. ตรวจรักษา</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td colspan="2"></td>
	</tr>
	<tr>
		<td align="center">รวม</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td colspan="2"></td>
	</tr>
	<tr>
		<td colspan="2" align="center">อาการป่วยและโรคทั่วไป</td>
		<td align="center">ตัว</td>
		<td colspan="2" align="center">ระบบสืบพันธ์ + DIP</td>
		<td align="center">ตัว</td>
		<td colspan="2" align="center">การให้บริการอื่นๆ</td>
		<td align="center">ตัว</td>
	</tr>
	<tr>
		<td colspan="2">1.แผลทั่วไป</td>
		<td></td>
		<td colspan="2">1.ไม่เป็นสัด</td>
		<td></td>
		<td colspan="2">1.ล้างมดลูกคลอดใหม่</td>
		<td></td>
	</tr>
	<tr>
		<td colspan="2">2.แผลกีบ</td>
		<td></td>
		<td colspan="2">-รอบการเป็นสัด</td>
		<td></td>
		<td colspan="2">2.ฉีดยาบำรุง</td>
		<td></td>
	</tr>
	<tr>
		<td colspan="2">3.หูด</td>
		<td></td>
		<td colspan="2">-รังไข่ไม่ทำงาน</td>
		<td></td>
		<td colspan="2">3.กรอกแม่เหล็ก</td>
		<td></td>
	</tr>
	<tr>
		<td colspan="2">4.กลาก</td>
		<td></td>
		<td colspan="2">-รังไข่ไม่สมบูรณ์</td>
		<td></td>
		<td colspan="2">4.แต่งกีบ</td>
		<td></td>
	</tr>
	<tr>
		<td colspan="2">5.ฝี</td>
		<td></td>
		<td colspan="2">-ถุงน้ำที่รังไข่</td>
		<td></td>
		<td colspan="2">5.ตัดหัวนมเกิน</td>
		<td></td>
	</tr>
	<tr>
		<td colspan="2">6.ข้ออักเสบ</td>
		<td></td>
		<td colspan="2">-คอร์ปัสลูเทียมค้าง</td>
		<td></td>
		<td colspan="2">5.ตัดเขา/จี้เขา</td>
		<td></td>
	</tr>
	<tr>
		<td colspan="2">7.ติดเชื้อระบบทางเดินหายใจ</td>
		<td></td>
		<td colspan="2">-ท้อง</td>
		<td></td>
		<td colspan="2">7.เก็บตัวอย่างส่งห้องปฎิบัติการ</td>
		<td></td>
	</tr>
	<tr>
		<td colspan="2">8.ไข้ขาแข็ง</td>
		<td></td>
		<td colspan="2">2.ผสมไม่ติด</td>
		<td></td>
		<td colspan="2">8.ชันสูตรซาก</td>
		<td></td>
	</tr>
	<tr>
		<td colspan="2">9.อนาพลาสโมซีส</td>
		<td></td>
		<td colspan="2">-รังไข่ไม่ทำงาน</td>
		<td></td>
		<td colspan="2">9.ซ้ำยา</td>
		<td></td>
	</tr>
	<tr>
		<td colspan="2">10.บาบีซีโอซีส</td>
		<td></td>
		<td colspan="2">-รังไข่ไม่สมบูรณ์</td>
		<td></td>
		<td colspan="2">10.จำหน่ายเวชภัณฑ์</td>
		<td></td>
	</tr>
	<tr>
		<td colspan="2">11.คีโตซีส</td>
		<td></td>
		<td colspan="2">-ถุงน้ำที่รังไข่</td>
		<td></td>
		<td colspan="2">11.ถ่ายพยาธิตามโปรแกรม</td>
		<td></td>
	</tr>
	<tr>
		<td colspan="2">12.ใช้น้ำนม</td>
		<td></td>
		<td colspan="2">-คอร์ปัส ลูเทียมค้าง</td>
		<td></td>
		<td colspan="2">12.อื่นๆ...................</td>
		<td></td>
	</tr>
	<tr>
		<td colspan="2">13.ภาวะโคลุกไม่ขึ้น</td>
		<td></td>
		<td colspan="2">-ไข่ไม่ตก</td>
		<td></td>
		<td colspan="2">การควบคุมและป้องกัน</td>
		<td align="center">ตัว</td>
	</tr>
	<tr>
		<td colspan="2">14.ตาอักเสบ</td>
		<td></td>
		<td colspan="2">-อื่นๆ</td>
		<td></td>
		<td colspan="2">1.ตรวจวัณโรค</td>
		<td></td>
	</tr>
	<tr>
		<td colspan="2">15.อาการทางประสาท</td>
		<td></td>
		<td colspan="2">3.มดลูกอักเสบเปฌรหรอง</td>
		<td></td>
		<td colspan="2">2.เจาะเลือดตรวจโรคแท้งติดต่อ</td>
		<td></td>
	</tr>
	<tr>
		<td colspan="2">16.ภาวะได้รับสารพิษ</td>
		<td></td>
		<td colspan="2">4.รกค้าง</td>
		<td></td>
		<td colspan="2">3.ป้องกันโรคปากและเท้าเปื่อย</td>
		<td></td>
	</tr>
	<tr>
		<td colspan="2">17.อื่นๆ</td>
		<td></td>
		<td colspan="2">5.ช่องคลอด/มดลูกทะลัก</td>
		<td></td>
		<td colspan="2">4.แพ้วัคซีน</td>
		<td></td>
	</tr>
	<tr>
		<td colspan="2">โรคระบบทางเดินอาหาร</td>
		<td align="center">ตัว</td>
		<td colspan="2">6.แท้งลูก</td>
		<td></td>
		<td colspan="2">5.อื่นๆ</td>
		<td></td>
	</tr>
	<tr>
		<td colspan="2">1.ท้องเสีย</td>
		<td></td>
		<td colspan="2">7.คลอดยาก</td>
		<td></td>
		<td colspan="2">จำนวนและสาเหตุโคตาย</td>
		<td align="center">ตัว</td>
	</tr>
	<tr>
		<td colspan="2">2.ท้องอืด</td>
		<td></td>
		<td colspan="2">8.อื่นๆ</td>
		<td></td>
		<td colspan="3" rowspan="10" align="center">
			
			ลงชื่อ................................................ผู้รายงาน <br /><br />
			........................................................ <br /><br />
			หัวหน้าหน่วย..................................
		</td>
	</tr>
	<tr>
		<td colspan="2">3.อาหารอัดแน่น</td>
		<td></td>
		<td colspan="2">โรคของลูกโคหย่านม</td>
		<td align="center">ตัว</td>
	</tr>
	<tr>
		<td colspan="2">4.วัตถุทิ่มตำกระเพาะ</td>
		<td></td>
		<td colspan="2">1.ติดเชื้อระบบทางเดินหายใจ</td>
		<td></td>
	</tr>
	<tr>
		<td colspan="2">5.กระเพาะที่ 4 เคลื่อน</td>
		<td></td>
		<td colspan="2">2.ท้องเสีย</td>
		<td></td>
	</tr>
	<tr>
		<td colspan="2">6.พยาธิ</td>
		<td></td>
		<td colspan="2">2.สะดืออักเสบ</td>
		<td></td>
	</tr>
	<tr>
		<td colspan="2">โรคของเต้านม</td>
		<td align="center">ตัว</td>
		<td colspan="2">4.ข้ออักเสบ</td>
		<td></td>
	</tr>
	<tr>
		<td colspan="2">1.เต้านมอักเสบ</td>
		<td></td>
		<td colspan="2">5.ท้องอืด</td>
		<td></td>
	</tr>
	<tr>
		<td colspan="2">2.แผลเต้านม</td>
		<td></td>
		<td colspan="2">6.ความผิดปกติทางพันธ์ุกรรม</td>
		<td></td>
	</tr>
	<tr>
		<td colspan="2">3.ฝีเต้านม</td>
		<td></td>
		<td colspan="2">7.อื่นๆ</td>
		<td></td>
	</tr>
	<tr>
		<td colspan="2">4.อาการแพ้ที่เต้านม/หัวนม</td>
		<td></td>
		<td colspan="2">ปัญหาข้อเสนอแนะประจำเดือน</td>
		<td></td>
	</tr>
</table>
<br />

<?php
die();
?>