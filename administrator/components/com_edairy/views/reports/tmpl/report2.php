<?php 
if($_REQUEST["export"]!="excel"){
?>
<style type="text/css">
	.btn-primary {
		width:150px;
	color: #fff;
	text-shadow: 0 -1px 0 rgba(0,0,0,0.25);
	background-color: #1d6cb0;
	background-image: -moz-linear-gradient(top,#2384d3,#15497c);
	background-image: -webkit-gradient(linear,0 0,0 100%,from(#2384d3),to(#15497c));
	background-image: -webkit-linear-gradient(top,#2384d3,#15497c);
	background-image: -o-linear-gradient(top,#2384d3,#15497c);
	background-image: linear-gradient(to bottom,#2384d3,#15497c);
	background-repeat: repeat-x;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff2384d3', endColorstr='#ff15497c', GradientType=0);
	border-color: #15497c #15497c #0a223b;
	*background-color: #15497c;
	filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
}
.btn-primary:hover,
.btn-primary:focus,
.btn-primary:active,
.btn-primary.active,
.btn-primary.disabled,
.btn-primary[disabled] {
	color: #fff;
	background-color: #15497c;
	*background-color: #113c66;
}
.btn-primary:active,
.btn-primary.active {
	background-color: #0e2f50 \9;
}
button.btn,
input[type="submit"].btn {
	*padding-top: 3px;
	*padding-bottom: 3px;
}
</style>

<script src="/media/jui/js/jquery.min.js"></script>
	<script src="/media/jui/js/chosen.jquery.min.js"></script>
	<link href="/media/jui/css/chosen.css" rel="stylesheet" />
	<script src="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript" charset="UTF-8"></script>

<link rel="stylesheet" href="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.css">
	<script type="text/javascript">
		jQuery(document).ready(function (){
			jQuery('select').chosen();

			jQuery('.datetimepicker').datetimepicker({
            lang:'th',
            timepicker:false,
            format:'d/m/Y'
        });
			
		});

	</script>

<div style="float:right">
	<a href="index.php?option=com_edairy&view=reports&layout=report2&export=excel&date_from=<?php echo $_REQUEST['date_from']; ?>&date_to=<?php echo $_REQUEST['date_to']; ?>&coop_id=<?php echo $_REQUEST['coop_id']; ?>&farmer_id=<?php echo $_REQUEST['farmer_id']; ?>"><img src="../images/excel.png" width="50"/></a>
</div>
<h3>รายงานช่วยการจัดการฟาร์ม</h3>

<form action="" method="get">
	ศูนย์รับน้ำนมดิบ
	<select name="coop_id" style="width:200px" onChange="javascript: getFarmerInCoop(this.value);">
		<option value="%">-- ทั้งหมด --</option>
		<?php foreach($this->masterData->mas_coop as $key=>$data){ 
				$selected = "";
				if($_REQUEST["coop_id"]==$data->id){
					$selected = " selected";
				}
		?>
			<option value="<?php echo $data->id; ?>"<?php echo $selected; ?>><?php echo $data->coop_code; ?> <?php echo $data->coop_abbr; ?> <?php echo $data->name; ?></option>
		<?php } ?>
	</select>
	&nbsp;&nbsp;

	ชื่อ-รหัส เกษตรกร
	<select name="farmer_id" id="farmer_id">
		<option value="%">-- ทั้งหมด --</option>
		<?php foreach($this->masterData->mas_farmer as $key=>$data){ 
				$selected = "";
				if($_REQUEST["farmer_id"]==$data->id){
					$selected = " selected";
				}
		?>
			<option value="<?php echo $data->id; ?>"<?php echo $selected; ?>><?php echo $data->citizen_id; ?> <?php echo $data->name; ?> <?php echo $data->surname; ?></option>
		<?php } ?>
	</select>

	&nbsp;&nbsp;
	วันที่
	<input type="text" name="date_from" class="datetimepicker" value="<?php echo $_REQUEST['date_from']; ?>" />

	ถึง

	<input type="text" name="date_to" class="datetimepicker" value="<?php echo $_REQUEST['date_to']; ?>" />

	

	

	&nbsp;&nbsp;
	<input type="hidden" name="option" value="com_edairy" />
	<input type="hidden" name="view" value="reports" />
	<input type="hidden" name="layout" value="report2" />
	<input type="submit" class="btn btn-primary" value="ดูรายงาน" />

</form>
<hr />
<?php
}else{
	$file="report2.xls";
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=$file");
}
?>
<b>1. กำหนดคลอด</b>
<table width="100%" border="1" cellspacing="0" cellpadding="2">
	<tr>
			<th>หมายเลขโค</th>
			<th>ชื่อโค</th>
			<th>ท้องที่</th>
			<th>กำหนดคลอด</th>
	</tr>
	<?php foreach($this->report->pregnant_inspection as $key=>$data){ ?>
		<tr>
			<td align="center"><?php echo $data->cow->cow_id; ?></td>
			<td align="center"><?php echo $data->cow->name; ?></td>
			<td align="center">1</td>
			<td align="center"><?php echo formatDate($data->schedule_date1); ?></td>
		</tr>
	<?php } ?>
</table>
<br />
<b>2. โคถึงกำหนดหยุดรีด</b>
<table width="100%" border="1" cellspacing="0" cellpadding="2">
	<tr>
			<th>หมายเลขโค</th>
			<th>ชื่อโค</th>
			<th>หยุดรีด</th>
			<th>กำหนดคลอด</th>
	</tr>
	<?php foreach($this->report->pregnant_inspection as $key=>$data){ ?>
		<tr>
			<td align="center"><?php echo $data->cow->cow_id; ?></td>
			<td align="center"><?php echo $data->cow->name; ?></td>
			<td align="center"><?php echo formatDate($data->schedule_date2); ?></td>
			<td align="center"><?php echo formatDate($data->schedule_date1); ?></td>
		</tr>
	<?php } ?>
</table>

<br />
<b>3. ครบกำหนดกลับสัด</b>
<table width="100%" border="1" cellspacing="0" cellpadding="2">
	<tr>
			<th>หมายเลขโค</th>
			<th>ชื่อโค</th>
			<th>ผสมพันธ์ครั้งสุดท้าย</th>
			<th>กลับสัดระหว่างวันที่</th>
	</tr>
	<?php foreach($this->report->pregnant_inspection as $key=>$data){ ?>
		<tr>
			<td align="center"><?php echo $data->cow->cow_id; ?></td>
			<td align="center"><?php echo $data->cow->name; ?></td>
			<td align="center"><?php echo formatDate($data->insemination_date); ?></td>
			<td align="center"><?php echo formatDate($data->schedule_date3); ?> - <?php echo formatDate($data->schedule_date4); ?></td>
		</tr>
	<?php } ?>
</table>

<br />
<b>4. โคพบว่ายังไม่ตั้งท้องนานกว่า 150 วัน</b>
<table width="100%" border="1" cellspacing="0" cellpadding="2">
	<tr>
			<th>หมายเลขโค</th>
			<th>ชื่อโค</th>
			<th>ระบบสืบพันธ์</th>
			<th>การให้นม</th>
			<th>วันท้องว่าง</th>
			<th>ผสมเมื่อ</th>
			<th>ครั้งที่ผสม</th>
	</tr>
	<?php foreach($this->report->birth_inspection as $key=>$data){ ?>
		<tr>
			<td align="center"><?php echo $data->cow->cow_id; ?></td>
			<td align="center"><?php echo $data->cow->name; ?></td>
			<td align="center"></td>
			<td align="center"></td>
			<td align="center"><?php echo $data->date_diff1; ?></td>
			<td align="center"><?php echo formatDate($data->last_insemination_date); ?></td>
			<td align="center">1</td>
		</tr>
	<?php } ?>
</table>

<br />
<b>5. โคคลอด 60 วันแล้วยังไม่ได้ผสม</b>
<table width="100%" border="1" cellspacing="0" cellpadding="2">
	<tr>
			<th>หมายเลขโค</th>
			<th>ชื่อโค</th>
			<th>คลอดเมื่อ</th>
			<th>ครบ 60 วันหลังคลอด</th>
	</tr>
	<?php foreach($this->report->birth_inspection2 as $key=>$data){ ?>
		<tr>
			<td align="center"><?php echo $data->cow->cow_id; ?></td>
			<td align="center"><?php echo $data->cow->name; ?></td>
			<td align="center"><?php echo formatDate($data->birth_date); ?></td>
			<td align="center"><?php echo formatDate($data->schedule_date1); ?></td>
		</tr>
	<?php } ?>
</table>

<br />
<b>6. โคครบกำหนดต้องตรวจท้อง</b>
<table width="100%" border="1" cellspacing="0" cellpadding="2">
	<tr>
			<th>หมายเลขโค</th>
			<th>ชื่อโค</th>
			<th>ครั้งที่ผสมพันธ์</th>
			<th>วันท้องว่าง</th>
			<th>ผสมพันธ์เมื่อ</th>
			<th>วันหลังผสม</th>
	</tr>
	<?php foreach($this->report->insemination as $key=>$data){ ?>
		<tr>
			<td align="center"><?php echo $data->cow->cow_id; ?></td>
			<td align="center"><?php echo $data->cow->name; ?></td>
			<td align="center">1</td>
			<td align="center"><?php echo $data->date_diff1; ?></td>
			<td align="center"><?php echo formatDate($data->create_date); ?></td>
			<td align="center"><?php echo $data->date_diff2; ?></td>
		</tr>
	<?php } ?>
</table>

<br />
<b>7. กำหนดตรวจโรคและฉีดวัคซีน</b>
<table width="100%" border="1" cellspacing="0" cellpadding="2">
	<tr>
			<th>หมายเลขโค</th>
			<th>ชื่อโค</th>
			<th>สถานภาพ</th>
			<th>ครั้งสุดท้ายเมื่อ</th>
			<th>ครบกำหนด</th>
	</tr>
	
</table>


<br />
<b>8. โคอายุ 18 เดือน ไม่ได้รับการผสม</b>
<table width="100%" border="1" cellspacing="0" cellpadding="2">
	<tr>
			<th>หมายเลขโค</th>
			<th>ชื่อโค</th>
			<th>อายุครบ 18 เดือนเมื่อ</th>
	</tr>
</table>

<br />
<b>9. โคอายุ 24 เดือน ไม่ท้อง</b>
<table width="100%" border="1" cellspacing="0" cellpadding="2">
	<tr>
			<th>หมายเลขโค</th>
			<th>ชื่อโค</th>
			<th>ครั้งที่ผสมพันธ์</th>
			<th>ผสมพันธ์ครั้งสุดท้ายเมื่อ</th>
			<th>อายุครบ 24 เดือนเมื่อ</th>
	</tr>
</table>

<br /><br />
หมายเหตุ ฟาร์มนี้ไม่ได้สำรวจสถานภาพโคนานเกิน 2 เดือน

<script type="text/javascript">
	function getFarmerInCoop(coop_id){
		jQuery.get("index.php?option=com_edairy&task=reports.getFarmerInCoop&coop_id="+coop_id, function(data){
			data = jQuery.parseJSON(data);
			console.log(data);
			jQuery("#farmer_id").empty();
			jQuery('#farmer_id').append("<option value=''>-- ทั้งหมด --</option>");
			for(var i=0;i<data.length;i++){
				jQuery('#farmer_id').append("<option value='"+data[i].id+"'>"+data[i].citizen_id+" "+data[i].name+" "+data[i].surname+"</option>");
			}
			jQuery('#farmer_id').trigger("liszt:updated");
		});
	}
</script>
<?php
die();
?>