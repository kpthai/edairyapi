<?php 
if($_REQUEST["export"]!="excel"){
?>
<style type="text/css">
	.btn-primary {
		width:150px;
	color: #fff;
	text-shadow: 0 -1px 0 rgba(0,0,0,0.25);
	background-color: #1d6cb0;
	background-image: -moz-linear-gradient(top,#2384d3,#15497c);
	background-image: -webkit-gradient(linear,0 0,0 100%,from(#2384d3),to(#15497c));
	background-image: -webkit-linear-gradient(top,#2384d3,#15497c);
	background-image: -o-linear-gradient(top,#2384d3,#15497c);
	background-image: linear-gradient(to bottom,#2384d3,#15497c);
	background-repeat: repeat-x;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff2384d3', endColorstr='#ff15497c', GradientType=0);
	border-color: #15497c #15497c #0a223b;
	*background-color: #15497c;
	filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
}
.btn-primary:hover,
.btn-primary:focus,
.btn-primary:active,
.btn-primary.active,
.btn-primary.disabled,
.btn-primary[disabled] {
	color: #fff;
	background-color: #15497c;
	*background-color: #113c66;
}
.btn-primary:active,
.btn-primary.active {
	background-color: #0e2f50 \9;
}
button.btn,
input[type="submit"].btn {
	*padding-top: 3px;
	*padding-bottom: 3px;
}
</style>

<script src="/media/jui/js/jquery.min.js"></script>
	<script src="/media/jui/js/chosen.jquery.min.js"></script>
	<link href="/media/jui/css/chosen.css" rel="stylesheet" />
	<script src="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript" charset="UTF-8"></script>

<link rel="stylesheet" href="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.css">
	<script type="text/javascript">
		jQuery(document).ready(function (){
			jQuery('select').chosen();

			jQuery('.datetimepicker').datetimepicker({
            lang:'th',
            timepicker:false,
            format:'d/m/Y'
        });
			
		});

	</script>
<div style="float:right">
	<a href="index.php?option=com_edairy&view=reports&layout=report3&export=excel"><img src="../images/excel.png" width="50"/></a>
</div>
<h3>รายงานการปฎิบัติงานผสมเทียมและตรวจท้อง</h3>

<form action="" method="get">
	ศูนย์รับน้ำนมดิบ
	<select name="coop_id" style="width:200px">
		<option value="">-- เลือกศูนย์รับน้ำนมดิบ --</option>
		<?php foreach($this->masterData->mas_coop as $key=>$data){ ?>
			<option value="<?php echo $data->id; ?>"><?php echo $data->coop_code; ?> <?php echo $data->coop_abbr; ?> <?php echo $data->name; ?></option>
		<?php } ?>
	</select>

	&nbsp;&nbsp;

	นักส่งเสริมผู้ให้บริการ
	<select name="vet_id" style="width:250px">
		<option value="">-- เลือกนักส่งเสริม --</option>
		<?php foreach($this->masterData->mas_vet as $key=>$data){ ?>
			<option value="<?php echo $data->id; ?>"><?php echo $data->name; ?></option>
		<?php } ?>
	</select>


	&nbsp;&nbsp;
	วันที่
	<input type="text" name="date_from" class="datetimepicker" />

	ถึง

	<input type="text" name="date_to" class="datetimepicker" />

	

	

	
	&nbsp;&nbsp;
	<input type="submit" class="btn btn-primary" value="ดูรายงาน" />

</form>
<hr />
<?php
}else{
	$file="report3.xls";
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=$file");
}
?>
<table width="100%" border="1" cellpadding="3" cellspacing="0">
	<tr>
		<th rowspan="2">วันที่</th>
		<th rowspan="2">ฟาร์ม</th>
		<th rowspan="2">ชื่อสมาชิก</th>
		<th rowspan="2">เบอร์ถัง</th>
		<th rowspan="2">เบอร์โค</th>
		<th colspan="2">ประเภทโค</th>
		<th rowspan="2">ครั้งที่</th>
		<th rowspan="2">ชื่อพ่อพันธ์</th>
		<th rowspan="2">เลขที่ใบสำคัญ</th>
		<th rowspan="2">ค่าน้ำเชื้อ</th>
		<th rowspan="2">ค่าบริการ</th>
		<th colspan="2">การตรวจท้อง</th>
	</tr>
	<tr>
		<th>แม่โค</th>
		<th>โคสาว</th>
		<th>ท้อง</th>
		<th>ไม่ท้อง</th>
	</tr>
	
</table>
<br />

<table cellpadding="2" cellspacing="0" border="1" width="400">
	<tr>
		<td width="300">อัตราการผสมติดครั้งแรก (%)</td>
		<td width="100"></td>
	</tr>
	<tr>
		<td>อัตราการผสมติดเป็นเพศเมีย (%)</td>
		<td></td>
	</tr>
</table>

<br />

<table>
	<tr>
		<td width="500">
			ลงชื่อ................................................ผู้ปฎิบัติงาน
		</td>
		<td width="500">
			ลงชื่อ................................................หัวหน้าหน่วย
		</td>
	</tr>
</table>
<?php
die();
?>