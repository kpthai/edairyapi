<?php 
if($_REQUEST["export"]!="excel"){
?>
<style type="text/css">
	.btn-primary {
		width:150px;
	color: #fff;
	text-shadow: 0 -1px 0 rgba(0,0,0,0.25);
	background-color: #1d6cb0;
	background-image: -moz-linear-gradient(top,#2384d3,#15497c);
	background-image: -webkit-gradient(linear,0 0,0 100%,from(#2384d3),to(#15497c));
	background-image: -webkit-linear-gradient(top,#2384d3,#15497c);
	background-image: -o-linear-gradient(top,#2384d3,#15497c);
	background-image: linear-gradient(to bottom,#2384d3,#15497c);
	background-repeat: repeat-x;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff2384d3', endColorstr='#ff15497c', GradientType=0);
	border-color: #15497c #15497c #0a223b;
	*background-color: #15497c;
	filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
}
.btn-primary:hover,
.btn-primary:focus,
.btn-primary:active,
.btn-primary.active,
.btn-primary.disabled,
.btn-primary[disabled] {
	color: #fff;
	background-color: #15497c;
	*background-color: #113c66;
}
.btn-primary:active,
.btn-primary.active {
	background-color: #0e2f50 \9;
}
button.btn,
input[type="submit"].btn {
	*padding-top: 3px;
	*padding-bottom: 3px;
}
</style>

<script src="/media/jui/js/jquery.min.js"></script>
	<script src="/media/jui/js/chosen.jquery.min.js"></script>
	<link href="/media/jui/css/chosen.css" rel="stylesheet" />
	<script src="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript" charset="UTF-8"></script>

<link rel="stylesheet" href="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.css">
	<script type="text/javascript">
		jQuery(document).ready(function (){
			jQuery('select').chosen();

			jQuery('.datetimepicker').datetimepicker({
            lang:'th',
            timepicker:false,
            format:'d/m/Y'
        });
			
		});

	</script>
<div style="float:right">
	<a href="index.php?option=com_edairy&view=reports&layout=report7&export=excel&coop_id=<?php echo $_REQUEST['coop_id']; ?>&date_from=<?php echo $_REQUEST['date_from']; ?>&date_to=<?php echo $_REQUEST['date_to']; ?>"><img src="../images/excel.png" width="50"/></a>
</div>
<h3>รายงานปริมาณน้ำนมดิบของสหกรณ์</h3>

<form action="" method="get">
	สหกรณ์
	<select name="coop_id" style="width:200px">
		<option value="">-- เลือกสหกรณ์ --</option>
		<?php foreach($this->masterData->mas_coop as $key=>$data){ 
				$selected = "";
				if($_REQUEST["coop_id"]==$data->id){
					$selected = " selected";
				}
				
			?>
			<option value="<?php echo $data->id; ?>"<?php echo $selected; ?>><?php echo $data->coop_code; ?> <?php echo $data->coop_abbr; ?> <?php echo $data->name; ?></option>
		<?php } ?>
	</select>

			&nbsp;&nbsp;
	วันที่
	<input type="text" name="date_from" class="datetimepicker" value="<?php echo $_REQUEST['date_from']; ?>" />

	ถึง

	<input type="text" name="date_to" class="datetimepicker" value="<?php echo $_REQUEST['date_to']; ?>" />

	

	
	&nbsp;&nbsp;
	<input type="hidden" name="option" value="com_edairy" />
	<input type="hidden" name="view" value="reports" />
	<input type="hidden" name="layout" value="report7" />
	<input type="submit" class="btn btn-primary" value="ดูรายงาน" />

</form>
<hr />
<?php
}else{
	$file="report7.xls";
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=$file");
}
$show_report = array();
foreach($this->report as $key=>$data){
	if($show_report[$data->create_datetime]==null){
		$show_report[$data->create_datetime] = $data;
	}else{
		$show_report[$data->create_datetime]->round2 = $data;
	}
}

?>
<table width="100%" border="1" cellpadding="3" cellspacing="0">
	<tr>
		<th rowspan="2">วัน/เดือน/ปี</th>
		<th colspan="3">ปริมาณที่รับ (กก.)</th>
		<th rowspan="2">ปริมาณที่ส่งโรงงาน อ.ส.ค. (กก.)</th>
		<th rowspan="2">คงเหลือ (กก.)</th>
		<th rowspan="2">MOU (ตัน/วัน)</th>
		<th rowspan="2">ผลต่าง (ตัน/วัน)</th>
	</tr>
	<tr>
		<th>เช้า</th>
		<th>เย็น</th>
		<th>รวม</th>
	</tr>
	<?php foreach($show_report as $key=>$data){ 
			$evening_total_amount = $data->round2->total_amount;
			if($evening_total_amount==""){
				$evening_total_amount = "0.00";
			}
			$day_total_amount = floatval($data->total_amount)+floatval($evening_total_amount);
		?>
		<tr>
			<td><?php echo formatDate($key); ?></td>
			<td align="right"><?php echo $data->total_amount; ?></td>
			<td align="right"><?php echo $evening_total_amount; ?></td>
			<td align="right"><?php echo number_format($day_total_amount,2); ?></td>
			<td align="right"><?php echo number_format($data->send_kg_amount,2); ?></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
	<?php } ?>
	
</table>
<?php
die();
?>