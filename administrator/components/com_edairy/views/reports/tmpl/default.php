<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'administrator/components/com_edairy/assets/css/edairy.css');
$document->addStyleSheet(JUri::root() . 'media/com_edairy/css/list.css');

$user      = JFactory::getUser();
$userId    = $user->get('id');
$listOrder = $this->state->get('list.ordering');
$listDirn  = $this->state->get('list.direction');
$canOrder  = $user->authorise('core.edit.state', 'com_edairy');
$saveOrder = $listOrder == 'a.`ordering`';

if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?option=com_edairy&task=milk_coop_qualities.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'milk_coop_qualityList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}

$sortFields = $this->getSortFields();
?>

<style type="text/css">
	ul.report li{
		font-size:16px;
		line-height: 22px;
	}
</style>
<script src="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript" charset="UTF-8"></script>

<link rel="stylesheet" href="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.css">
<script type="text/javascript">
	jQuery(document).ready(function () {
		jQuery('#clear-search-button').on('click', function () {
			jQuery('.well input').val('');
			jQuery('#adminForm').submit();
		});
	});

	js = jQuery.noConflict();
	js(document).ready(function () {
		js('.datetimepicker').datetimepicker({
            lang:'th',
            timepicker:false,
            format:'d/m/Y'
        });
     });
</script>
<form action="<?php echo JRoute::_('index.php?option=com_edairy&view=milk_coop_qualities'); ?>" method="post"
	  name="adminForm" id="adminForm">
	<?php if (!empty($this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
		<?php else : ?>
		<div id="j-main-container">
			<?php endif; ?>

			<h2>รายงาน</h2>
			
			<ul class="report">
				<li><a target="_blank" href="index.php?option=com_edairy&view=reports&layout=report1">1. รายงานรายได้-ค่าใช้จ่ายในการเลี้ยงโคนม</a></li>
				<li><a target="_blank" href="index.php?option=com_edairy&view=reports&layout=report2">2. รายงานช่วยการจัดการฟาร์ม</a></li>
				<li><a target="_blank" href="index.php?option=com_edairy&view=reports&layout=report3">3. รายงานการปฎิบัติงานผสมเทียมและตรวจท้อง</a></li>
				<li><a target="_blank" href="index.php?option=com_edairy&view=reports&layout=report4">4. รายงานการให้บริการสัตวแพทย์</a></li>
				<li><a target="_blank" href="index.php?option=com_edairy&view=reports&layout=report5">5. รายงานการสำรวจประชากรโค</a></li>
				<li><a target="_blank" href="index.php?option=com_edairy&view=reports&layout=report6">6. รายงานปริมาณน้ำนมของศูนย์รับน้ำนมดิบ</a></li>
				<li><a target="_blank" href="index.php?option=com_edairy&view=reports&layout=report7">7. รายงานปริมาณน้ำนมดิบของสหกรณ์</a></li>
				<li><a target="_blank" href="index.php?option=com_edairy&view=cows">8. บัตรประจำตัวโคนม</a></li>
				<li><a target="_blank" href="index.php?option=com_edairy&view=reports&layout=report9">9. รายงานประสิทธิภาพการผลิตของฟาร์มสมาชิก</a></li>
				<li><a target="_blank" href="index.php?option=com_edairy&view=reports&layout=report10">10. รายงานการประมวลผลการประเมินฟาร์มประสิทธิภาพสูง (ผ่านดัชนีตัวชี้วัด)</a></li>
				<li><a target="_blank" href="index.php?option=com_edairy&view=reports&layout=report11">11. รายงานแสดงผลการผลิต</a></li>
			</ul>
		</div>
</form>
