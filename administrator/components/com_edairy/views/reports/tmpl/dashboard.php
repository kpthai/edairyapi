<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'administrator/components/com_edairy/assets/css/edairy.css');
$document->addStyleSheet(JUri::root() . 'media/com_edairy/css/list.css');

$user      = JFactory::getUser();
$userId    = $user->get('id');
$listOrder = $this->state->get('list.ordering');
$listDirn  = $this->state->get('list.direction');
$canOrder  = $user->authorise('core.edit.state', 'com_edairy');
$saveOrder = $listOrder == 'a.`ordering`';

if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?option=com_edairy&task=milk_coop_qualities.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'milk_coop_qualityList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}

$sortFields = $this->getSortFields();
?>

<style type="text/css">
	ul.report li{
		font-size:16px;
		line-height: 22px;
	}
</style>
<script src="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript" charset="UTF-8"></script>

<link rel="stylesheet" href="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.css">

<script src="../media/com_edairy/js/Chart.bundle.js" type="text/javascript" charset="UTF-8"></script>
<script src="../media/com_edairy/js/utils.js" type="text/javascript" charset="UTF-8"></script>


<script type="text/javascript">
	jQuery(document).ready(function () {
		jQuery('#clear-search-button').on('click', function () {
			jQuery('.well input').val('');
			jQuery('#adminForm').submit();
		});
	});

	js = jQuery.noConflict();
	js(document).ready(function () {
		js('.datetimepicker').datetimepicker({
            lang:'th',
            timepicker:false,
            format:'d/m/Y'
        });
     });

</script>
	<?php if (!empty($this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
		<?php else : ?>
		<div id="j-main-container">
			<?php endif; ?>

			<h2>แดชบอร์ด</h2>
			<hr />

            <?php 
                // chart1
                $cow_milking = 0;
                $cow_stop_milking = 0;
                $cow_replace = 0;

                $PL_array = array(6,10,14,18,22,26,30);
                $L_array = array(4,8,12,16,20,24,28);
                $PD_array = array(7,11,15,19,23,27);
                $D_array = array(5,9,13,17,25,29);

                foreach($this->report->cow as $key=>$data){

                    if(in_array($data->cow_status_id, $PL_array) || in_array($data->cow_status_id, $L_array)){
                        $cow_milking+=$data->count;
                    }else if(in_array($data->cow_status_id, $PD_array) || in_array($data->cow_status_id, $D_array)){
                        $cow_stop_milking+=$data->count;
                    }
                }

                foreach($this->report->cow_female as $key=>$data){
                    if($data->cow_status_id==2){
                        $cow_replace+=$data->count;
                    }
                }

                $sum = $cow_milking+$cow_stop_milking+$cow_replace;

                // chart2
                $farm_in_region = array(0,0,0,0,0,0);
                $farm_sum = 0;
                foreach($this->report->farm as $key=>$data){
                    $farm_in_region[$data->region_id] = $data->count;
                    $farm_sum += $data->count;
                }




                // chart3
                $cow_milking1 = 0;
                $cow_stop_milking1 = 0;
                $cow_replace1 = 0;


                foreach($this->report->cow1 as $key=>$data){

                    if(in_array($data->cow_status_id, $PL_array) || in_array($data->cow_status_id, $L_array)){
                        $cow_milking1+=$data->count;
                    }else if(in_array($data->cow_status_id, $PD_array) || in_array($data->cow_status_id, $D_array)){
                        $cow_stop_milking1+=$data->count;
                    }
                }

                foreach($this->report->cow_female1 as $key=>$data){
                    if($data->cow_status_id==2){
                        $cow_replace1+=$data->count;
                    }
                }

                $sum1 = $cow_milking1+$cow_stop_milking1+$cow_replace1;



                // chart4
                $cow_milking2 = 0;
                $cow_stop_milking2 = 0;
                $cow_replace2 = 0;


                foreach($this->report->cow2 as $key=>$data){

                    if(in_array($data->cow_status_id, $PL_array) || in_array($data->cow_status_id, $L_array)){
                        $cow_milking2+=$data->count;
                    }else if(in_array($data->cow_status_id, $PD_array) || in_array($data->cow_status_id, $D_array)){
                        $cow_stop_milking2+=$data->count;
                    }
                }

                foreach($this->report->cow_female2 as $key=>$data){
                    if($data->cow_status_id==2){
                        $cow_replace2+=$data->count;
                    }
                }

                $sum2 = $cow_milking2+$cow_stop_milking2+$cow_replace2;


                // chart5
                $cow_milking3 = 0;
                $cow_stop_milking3 = 0;
                $cow_replace3 = 0;


                foreach($this->report->cow3 as $key=>$data){

                    if(in_array($data->cow_status_id, $PL_array) || in_array($data->cow_status_id, $L_array)){
                        $cow_milking3+=$data->count;
                    }else if(in_array($data->cow_status_id, $PD_array) || in_array($data->cow_status_id, $D_array)){
                        $cow_stop_milking3+=$data->count;
                    }
                }

                foreach($this->report->cow_female3 as $key=>$data){
                    if($data->cow_status_id==3){
                        $cow_replace3+=$data->count;
                    }
                }

                $sum3 = $cow_milking3+$cow_stop_milking3+$cow_replace3;



                // chart6
                $cow_milking4 = 0;
                $cow_stop_milking4 = 0;
                $cow_replace4 = 0;


                foreach($this->report->cow4 as $key=>$data){

                    if(in_array($data->cow_status_id, $PL_array) || in_array($data->cow_status_id, $L_array)){
                        $cow_milking4+=$data->count;
                    }else if(in_array($data->cow_status_id, $PD_array) || in_array($data->cow_status_id, $D_array)){
                        $cow_stop_milking4+=$data->count;
                    }
                }

                foreach($this->report->cow_female4 as $key=>$data){
                    if($data->cow_status_id==4){
                        $cow_replace4+=$data->count;
                    }
                }

                $sum4 = $cow_milking4+$cow_stop_milking4+$cow_replace4;



                // chart7
                $cow_milking5 = 0;
                $cow_stop_milking5 = 0;
                $cow_replace5 = 0;


                foreach($this->report->cow5 as $key=>$data){

                    if(in_array($data->cow_status_id, $PL_array) || in_array($data->cow_status_id, $L_array)){
                        $cow_milking5+=$data->count;
                    }else if(in_array($data->cow_status_id, $PD_array) || in_array($data->cow_status_id, $D_array)){
                        $cow_stop_milking5+=$data->count;
                    }
                }

                foreach($this->report->cow_female5 as $key=>$data){
                    if($data->cow_status_id==5){
                        $cow_replace5+=$data->count;
                    }
                }

                $sum5 = $cow_milking5+$cow_stop_milking5+$cow_replace5;





            ?>

			<div style="width:48%;float:left">
                <h4>สัดส่วนฝูงโค</h4>
		        <canvas id="chart-area"></canvas>
                <div style="clear:both"></div>
                <br />
                <div style="margin:auto;">
                    <table cellpadding="3">
                        <tr>
                            <td width="120">โครีดนม</td>
                            <td width="100"><?php echo number_format($cow_milking); ?> ตัว</td>
                            <td><?php echo number_format($cow_milking/$sum*100); ?> %</td>
                        </tr>
                        <tr>
                            <td>โคหยุดรีด</td>
                            <td><?php echo number_format($cow_stop_milking); ?> ตัว</td>
                            <td><?php echo number_format($cow_stop_milking/$sum*100); ?> %</td>
                        </tr>
                        <tr>
                            <td>โคทดแทน</td>
                            <td><?php echo number_format($cow_replace); ?> ตัว</td>
                            <td><?php echo number_format($cow_replace/$sum*100); ?> %</td>
                        </tr>
                        <tr>
                            <td><b>รวม</b></td>
                            <td><b><?php echo number_format($sum); ?> ตัว</b></td>
                            <td>100%</td>
                        </tr>
                    </table>
                </div>
		    </div>
		    <div style="width:48%;float:left">
                <h4>จำนวนฟาร์ม</h4>
		        <canvas id="chart-area2"></canvas>
                <div style="clear:both"></div>
                <br />
                <div style="margin:auto;">
                    <table cellpadding="3">
                        <tr>
                            <td width="150">ภาคกลาง</td>
                            <td width="100"><?php echo number_format($farm_in_region[1]); ?></td>
                            <td><?php echo number_format($farm_in_region[1]/$farm_sum*100); ?> %</td>
                        </tr>
                        <tr>
                            <td>ภาคตะวันออกเฉียงเหนือ</td>
                            <td><?php echo number_format($farm_in_region[2]); ?></td>
                            <td><?php echo number_format($farm_in_region[2]/$farm_sum*100); ?> %</td>
                        </tr>
                        <tr>
                            <td>ภาคเหนือตอนบน</td>
                            <td><?php echo number_format($farm_in_region[3]); ?></td>
                            <td><?php echo number_format($farm_in_region[3]/$farm_sum*100); ?> %</td>
                        </tr>
                        <tr>
                            <td>ภาคเหนือตอนล่าง</td>
                            <td><?php echo number_format($farm_in_region[4]); ?></td>
                            <td><?php echo number_format($farm_in_region[4]/$farm_sum*100); ?> %</td>
                        </tr>
                         <tr>
                            <td>ภาคใต้</td>
                            <td><?php echo number_format($farm_in_region[5]); ?></td>
                            <td><?php echo number_format($farm_in_region[5]/$farm_sum*100); ?> %</td>
                        </tr>
                        
                        <tr>
                            <td><b>รวม</b></td>
                            <td><b><?php echo number_format($farm_sum); ?></b></td>
                            <td>100%</td>
                        </tr>
                    </table>
                </div>
		    </div>
		    <div style="clear:both"></div>
		    <hr />
			<div style="width:30%;float:left">
                <h4>ภาคกลาง</h4>
		        <canvas id="chart-area4"></canvas>
                <div style="clear:both"></div>
                <br />
                <div style="margin:auto;">
                    <table cellpadding="3">
                        <tr>
                            <td width="120">โครีดนม</td>
                            <td width="100"><?php echo number_format($cow_milking1); ?> ตัว</td>
                            <td><?php 
                                if($sum1==0){
                                    echo 0;
                                }else{
                                    echo number_format($cow_milking1/$sum1*100); 
                                }
                                ?> 
                            %</td>
                        </tr>
                        <tr>
                            <td>โคหยุดรีด</td>
                            <td><?php echo number_format($cow_stop_milking1); ?> ตัว</td>
                            <td><?php 
                                if($sum1==0){
                                    echo 0;
                                }else{
                                    echo number_format($cow_stop_milking1/$sum1*100); 
                                }
                                ?> 
                            %</td>
                        </tr>
                        <tr>
                            <td>โคทดแทน</td>
                            <td><?php echo number_format($cow_replace1); ?> ตัว</td>
                            <td><?php 
                                if($sum1==0){
                                    echo 0;
                                }else{
                                    echo number_format($cow_replace1/$sum1*100); 
                                }
                                ?> 
                            %</td>
                        </tr>
                        <tr>
                            <td><b>รวม</b></td>
                            <td><b><?php echo number_format($sum1); ?> ตัว</b></td>
                            <td>100%</td>
                        </tr>
                    </table>
                </div>
		    </div>
		    <div style="width:30%;float:left">
		       <h4>ภาคเหนือตอนบน</h4>
                <canvas id="chart-area5"></canvas>
                <div style="clear:both"></div>
                <br />
                <div style="margin:auto;">
                    <table cellpadding="3">
                        <tr>
                            <td width="120">โครีดนม</td>
                            <td width="100"><?php echo number_format($cow_milking2); ?> ตัว</td>
                            <td><?php 
                                if($sum2==0){
                                    echo 0;
                                }else{
                                    echo number_format($cow_milking2/$sum2*100); 
                                }
                                ?> 
                            %</td>
                        </tr>
                        <tr>
                            <td>โคหยุดรีด</td>
                            <td><?php echo number_format($cow_stop_milking2); ?> ตัว</td>
                            <td><?php 
                                if($sum2==0){
                                    echo 0;
                                }else{
                                    echo number_format($cow_stop_milking2/$sum2*100); 
                                }
                                ?> 
                            %</td>
                        </tr>
                        <tr>
                            <td>โคทดแทน</td>
                            <td><?php echo number_format($cow_replace2); ?> ตัว</td>
                            <td><?php 
                                if($sum2==0){
                                    echo 0;
                                }else{
                                    echo number_format($cow_replace2/$sum2*100); 
                                }
                                ?> 
                            %</td>
                        </tr>
                        <tr>
                            <td><b>รวม</b></td>
                            <td><b><?php echo number_format($sum2); ?> ตัว</b></td>
                            <td>100%</td>
                        </tr>
                    </table>
                </div>
		    </div>
		    <div style="width:30%;float:left">
               <h4>ภาคเหนือตอนล่าง</h4>
                <canvas id="chart-area6"></canvas>
                <div style="clear:both"></div>
                <br />
                <div style="margin:auto;">
                    <table cellpadding="3">
                        <tr>
                            <td width="130">โครีดนม</td>
                            <td width="100"><?php echo number_format($cow_milking3); ?> ตัว</td>
                            <td><?php 
                                if($sum3==0){
                                    echo 0;
                                }else{
                                    echo number_format($cow_milking3/$sum3*100); 
                                }
                                ?> 
                            %</td>
                        </tr>
                        <tr>
                            <td>โคหยุดรีด</td>
                            <td><?php echo number_format($cow_stop_milking3); ?> ตัว</td>
                            <td><?php 
                                if($sum3==0){
                                    echo 0;
                                }else{
                                    echo number_format($cow_stop_milking3/$sum3*100); 
                                }
                                ?> 
                            %</td>
                        </tr>
                        <tr>
                            <td>โคทดแทน</td>
                            <td><?php echo number_format($cow_replace3); ?> ตัว</td>
                            <td><?php 
                                if($sum3==0){
                                    echo 0;
                                }else{
                                    echo number_format($cow_replace3/$sum3*100); 
                                }
                                ?> 
                            %</td>
                        </tr>
                        <tr>
                            <td><b>รวม</b></td>
                            <td><b><?php echo number_format($sum3); ?> ตัว</b></td>
                            <td>100%</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="clear:both"></div>
            <hr />
		    <div style="width:30%;float:left">
               <h4>ภาคตะวันออกเฉียงเหนือ</h4>
                <canvas id="chart-area7"></canvas>
                <div style="clear:both"></div>
                <br />
                <div style="margin:auto;">
                    <table cellpadding="4">
                        <tr>
                            <td width="130">โครีดนม</td>
                            <td width="100"><?php echo number_format($cow_milking4); ?> ตัว</td>
                            <td><?php 
                                if($sum4==0){
                                    echo 0;
                                }else{
                                    echo number_format($cow_milking4/$sum4*100); 
                                }
                                ?> 
                            %</td>
                        </tr>
                        <tr>
                            <td>โคหยุดรีด</td>
                            <td><?php echo number_format($cow_stop_milking4); ?> ตัว</td>
                            <td><?php 
                                if($sum4==0){
                                    echo 0;
                                }else{
                                    echo number_format($cow_stop_milking4/$sum4*100); 
                                }
                                ?> 
                            %</td>
                        </tr>
                        <tr>
                            <td>โคทดแทน</td>
                            <td><?php echo number_format($cow_replace4); ?> ตัว</td>
                            <td><?php 
                                if($sum4==0){
                                    echo 0;
                                }else{
                                    echo number_format($cow_replace4/$sum4*100); 
                                }
                                ?> 
                            %</td>
                        </tr>
                        <tr>
                            <td><b>รวม</b></td>
                            <td><b><?php echo number_format($sum4); ?> ตัว</b></td>
                            <td>100%</td>
                        </tr>
                    </table>
                </div>
            </div>
		    <div style="width:30%;float:left">
               <h4>ภาคใต้</h4>
                <canvas id="chart-area8"></canvas>
                <div style="clear:both"></div>
                <br />
                <div style="margin:auto;">
                    <table cellpadding="3">
                        <tr>
                            <td width="150">โครีดนม</td>
                            <td width="100"><?php echo number_format($cow_milking5); ?> ตัว</td>
                            <td><?php 
                                if($sum5==0){
                                    echo 0;
                                }else{
                                    echo number_format($cow_milking5/$sum5*100); 
                                }
                                ?> 
                            %</td>
                        </tr>
                        <tr>
                            <td>โคหยุดรีด</td>
                            <td><?php echo number_format($cow_stop_milking5); ?> ตัว</td>
                            <td><?php 
                                if($sum5==0){
                                    echo 0;
                                }else{
                                    echo number_format($cow_stop_milking5/$sum5*100); 
                                }
                                ?> 
                            %</td>
                        </tr>
                        <tr>
                            <td>โคทดแทน</td>
                            <td><?php echo number_format($cow_replace5); ?> ตัว</td>
                            <td><?php 
                                if($sum5==0){
                                    echo 0;
                                }else{
                                    echo number_format($cow_replace5/$sum5*100); 
                                }
                                ?> 
                            %</td>
                        </tr>
                        <tr>
                            <td><b>รวม</b></td>
                            <td><b><?php echo number_format($sum5); ?> ตัว</b></td>
                            <td>100%</td>
                        </tr>
                    </table>
                </div>
            </div>
		    <div style="clear:both"></div>
		    <hr />
			<div style="width:100%;float:left">
                <div class="well">

                    <form action="" method="get">
                        <h4>ปริมาณน้ำนมดิบ(ตัน) </h4>
                    ประจำเดือน
    <select name="month" style="width:100px">
        <option value="%">ทั้งหมด</option>
        <option value="1"<?php if($_REQUEST['month']==1){ ?> selected<?php } ?>>มกราคม</option>
        <option value="2"<?php if($_REQUEST['month']==2){ ?> selected<?php } ?>>กุมภาพันธ์</option>
        <option value="3"<?php if($_REQUEST['month']==3){ ?> selected<?php } ?>>มีนาคม</option>
        <option value="4"<?php if($_REQUEST['month']==4){ ?> selected<?php } ?>>เมษายน</option>
        <option value="5"<?php if($_REQUEST['month']==5){ ?> selected<?php } ?>>พฤษภาคม</option>
        <option value="6"<?php if($_REQUEST['month']==6){ ?> selected<?php } ?>>มิถุนายน</option>
        <option value="7"<?php if($_REQUEST['month']==7){ ?> selected<?php } ?>>กรกฎาคม</option>
        <option value="8"<?php if($_REQUEST['month']==8){ ?> selected<?php } ?>>สิงหาคม</option>
        <option value="9"<?php if($_REQUEST['month']==9){ ?> selected<?php } ?>>กันยายน</option>
        <option value="10"<?php if($_REQUEST['month']==10){ ?> selected<?php } ?>>ตุลาคม</option>
        <option value="11"<?php if($_REQUEST['month']==11){ ?> selected<?php } ?>>พฤศจิกายน</option>
        <option value="12"<?php if($_REQUEST['month']==12){ ?> selected<?php } ?>>ธันวาคม</option>
    </select>
    &nbsp;&nbsp;
    ปี
    <select name="year" style="width:100px">
        <option value="%">ทั้งหมด</option>
        <?php for($i=date("Y")+543;$i>=2550;$i--){ ?>
            <option value="<?php echo $i; ?>"<?php if($_REQUEST['year']==$i){?> selected<?php } ?>><?php echo $i; ?></option>
        <?php } ?>
    </select>
    <?php 
                $receive_milk_in_region_coop = array(0,0,0,0,0,0);
                $receive_milk_in_region = array(0,0,0,0,0,0);
                
                $receive_milk_sum1 = 0;
                $receive_milk_sum2 = 0;
                foreach($this->report->receive_milk as $key=>$data){
                    if($data->coop_type==1){
                        $receive_milk_in_region_coop[$data->region_id] = $data->sum_ton_amount;
                        $receive_milk_sum1 += $data->sum_ton_amount;
                    }else{
                        $receive_milk_in_region[$data->region_id] = $data->sum_ton_amount;
                        $receive_milk_sum2 += $data->sum_ton_amount;
                    }
                }

    ?>

    <input type="hidden" name="option" value="com_edairy" />
    <input type="hidden" name="view" value="reports" />
    <input type="hidden" name="layout" value="dashboard" />
    <input type="submit" class="btn btn-primary" value="ดูรายงาน" />
</form>


                </div>
		        <canvas id="chart-area3"></canvas>

                <div style="clear:both"></div>
                <br />
                <div style="margin:auto;">

                    <table cellpadding="3">
                        <tr>
                            <td width="150" style="font-weight:bold">ภาค</td>
                            <td width="100" style="font-weight:bold">ศูนย์รับน้ำนมดิบ</td>
                            <td width="100" style="font-weight:bold">สหกรณ์</td>
                        </tr>
                        <tr>
                            <td width="150">ภาคกลาง</td>
                            <td width="100"><?php echo number_format($receive_milk_in_region[1],3); ?></td>
                             <td width="100"><?php echo number_format($receive_milk_in_region_coop[1],3); ?></td>
                        </tr>
                        <tr>
                            <td>ภาคตะวันออกเฉียงเหนือ</td>
                            <td><?php echo number_format($receive_milk_in_region[2],3); ?></td>
                            <td><?php echo number_format($receive_milk_in_region_coop[2],3); ?></td>
                        </tr>
                        <tr>
                            <td>ภาคเหนือตอนบน</td>
                            <td><?php echo number_format($receive_milk_in_region[3],3); ?></td>
                            <td><?php echo number_format($receive_milk_in_region_coop[3],3); ?></td>
                        </tr>
                        <tr>
                            <td>ภาคเหนือตอนล่าง</td>
                            <td><?php echo number_format($receive_milk_in_region[4],3); ?></td>
                            <td><?php echo number_format($receive_milk_in_region_coop[4],3); ?></td>
                        </tr>
                         <tr>
                            <td>ภาคใต้</td>
                            <td><?php echo number_format($receive_milk_in_region[5],3); ?></td>
                            <td><?php echo number_format($receive_milk_in_region_coop[5],3); ?></td>
                        </tr>
                        
                        <tr>
                            <td><b>รวม</b></td>
                            <td><b><?php echo number_format($receive_milk_sum1,3); ?></b></td>
                            <td><b><?php echo number_format($receive_milk_sum2,3); ?></b></td>
                        </tr>
                    </table>
                </div>
		    </div>
		</div>

<script>
    var randomScalingFactor = function() {
        return Math.round(Math.random() * 100);
    };

    var config = {
        type: 'pie',
        data: {
            datasets: [{
                data: [
                    <?php echo $cow_replace; ?>,
                    <?php echo $cow_stop_milking; ?>,
                    <?php echo $cow_milking; ?>,
                ],
                backgroundColor: [
                    window.chartColors.red,
                    window.chartColors.orange,
                    window.chartColors.blue,
                ],
                label: 'Dataset 1'
            }],
            labels: [
                "โคทดแทน",
                "โคหยุดรีด",
                "โครีดนม"
            ]
        },
        options: {
            responsive: true
        }
    };







    var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var color = Chart.helpers.color;
        var barChartData = {
            labels: ["ภาคกลาง", "ภาคเหนือตอนบน", "ภาคเหนือตอนล่าง", "ภาคตะวันออกเฉียงเหนือ", "ภาคใต้"],
            datasets: [{
                label: 'ศูนย์รับน้ำนมดิบ',
                backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
                borderColor: window.chartColors.red,
                borderWidth: 1,
                data: [
                    <?php echo $receive_milk_in_region[1]; ?>,
                    <?php echo $receive_milk_in_region[2]; ?>,
                    <?php echo $receive_milk_in_region[3]; ?>,
                    <?php echo $receive_milk_in_region[4]; ?>,
                    <?php echo $receive_milk_in_region[5]; ?>
                ]
            }, {
                label: 'สหกรณ์',
                backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
                borderColor: window.chartColors.blue,
                borderWidth: 1,
                data: [
                    <?php echo $receive_milk_in_region_coop[1]; ?>,
                    <?php echo $receive_milk_in_region_coop[2]; ?>,
                    <?php echo $receive_milk_in_region_coop[3]; ?>,
                    <?php echo $receive_milk_in_region_coop[4]; ?>,
                    <?php echo $receive_milk_in_region_coop[5]; ?>
                ]
            }]

        };



        var barChartDataFarm = {
            labels: ["ภาคกลาง", "ภาคเหนือตอนบน", "ภาคเหนือตอนล่าง", "ภาคตะวันออกเฉียงเหนือ", "ภาคใต้"],
            datasets: [{
                label: 'จำนวนฟาร์ม',
                backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
                borderColor: window.chartColors.red,
                borderWidth: 1,
                data: [
                    <?php echo $farm_in_region[1]; ?>,
                    <?php echo $farm_in_region[2]; ?>,
                    <?php echo $farm_in_region[3]; ?>,
                    <?php echo $farm_in_region[4]; ?>,
                    <?php echo $farm_in_region[5]; ?>
                ]
            }]

        };


          window.onload = function() {
        var ctx = document.getElementById("chart-area").getContext("2d");
        window.myPie = new Chart(ctx, config);

        var ctx2 = document.getElementById("chart-area2").getContext("2d");
            window.myBar = new Chart(ctx2, {
                type: 'bar',
                data: barChartDataFarm,
                options: {
                    responsive: true,
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'กราฟแสดงจำนวนฟาร์ม'
                    }
                }
            });




            var ctx3 = document.getElementById("chart-area3").getContext("2d");
            window.myBar = new Chart(ctx3, {
                type: 'bar',
                data: barChartData,
                options: {
                    responsive: true,
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'กราฟแสดงปริมาณน้ำนมดิบ(ตัน)'
                    }
                }
            });


            var config2 = {
        type: 'pie',
        data: {
            datasets: [{
                data: [
                    <?php echo $cow_replace1; ?>,
                    <?php echo $cow_stop_milking1; ?>,
                    <?php echo $cow_milking1; ?>,
                ],
                backgroundColor: [
                    window.chartColors.red,
                    window.chartColors.orange,
                    window.chartColors.blue,
                ],
                label: 'Dataset 1'
            }],
            labels: [
                "โคทดแทน",
                "โคหยุดรีด",
                "โครีดนม"
            ]
        },
        options: {
            responsive: true
        }
    };


            var ctx4 = document.getElementById("chart-area4").getContext("2d");
        window.myPie = new Chart(ctx4, config2);



        var config3 = {
        type: 'pie',
        data: {
            datasets: [{
                data: [
                    <?php echo $cow_replace2; ?>,
                    <?php echo $cow_stop_milking2; ?>,
                    <?php echo $cow_milking2; ?>,
                ],
                backgroundColor: [
                    window.chartColors.red,
                    window.chartColors.orange,
                    window.chartColors.blue,
                ],
                label: 'Dataset 1'
            }],
            labels: [
                "โคทดแทน",
                "โคหยุดรีด",
                "โครีดนม"
            ]
        },
        options: {
            responsive: true
        }
    };

        	var ctx5 = document.getElementById("chart-area5").getContext("2d");
        window.myPie = new Chart(ctx5, config3);


        var config4 = {
        type: 'pie',
        data: {
            datasets: [{
                data: [
                    <?php echo $cow_replace3; ?>,
                    <?php echo $cow_stop_milking3; ?>,
                    <?php echo $cow_milking3; ?>,
                ],
                backgroundColor: [
                    window.chartColors.red,
                    window.chartColors.orange,
                    window.chartColors.blue,
                ],
                label: 'Dataset 1'
            }],
            labels: [
                "โคทดแทน",
                "โคหยุดรีด",
                "โครีดนม"
            ]
        },
        options: {
            responsive: true
        }
    };

        	var ctx6 = document.getElementById("chart-area6").getContext("2d");
        window.myPie = new Chart(ctx6, config4);


        var config5 = {
        type: 'pie',
        data: {
            datasets: [{
                data: [
                    <?php echo $cow_replace4; ?>,
                    <?php echo $cow_stop_milking4; ?>,
                    <?php echo $cow_milking4; ?>,
                ],
                backgroundColor: [
                    window.chartColors.red,
                    window.chartColors.orange,
                    window.chartColors.blue,
                ],
                label: 'Dataset 1'
            }],
            labels: [
                "โคทดแทน",
                "โคหยุดรีด",
                "โครีดนม"
            ]
        },
        options: {
            responsive: true
        }
    };

        	var ctx7 = document.getElementById("chart-area7").getContext("2d");
        window.myPie = new Chart(ctx7, config5);


        var config6 = {
        type: 'pie',
        data: {
            datasets: [{
                data: [
                    <?php echo $cow_replace5; ?>,
                    <?php echo $cow_stop_milking5; ?>,
                    <?php echo $cow_milking5; ?>,
                ],
                backgroundColor: [
                    window.chartColors.red,
                    window.chartColors.orange,
                    window.chartColors.blue,
                ],
                label: 'Dataset 1'
            }],
            labels: [
                "โคทดแทน",
                "โคหยุดรีด",
                "โครีดนม"
            ]
        },
        options: {
            responsive: true
        }
    };

        	var ctx8 = document.getElementById("chart-area8").getContext("2d");
        window.myPie = new Chart(ctx8, config6);

    };
    
    </script>
