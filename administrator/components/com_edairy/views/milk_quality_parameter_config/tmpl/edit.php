<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_edairy/css/form.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	});

	Joomla.submitbutton = function (task) {
		if (task == 'milk_quality_parameter_config.cancel') {
			Joomla.submitform(task, document.getElementById('milk_quality_parameter_config-form'));
		}
		else {
			
			if (task != 'milk_quality_parameter_config.cancel' && document.formvalidator.isValid(document.id('milk_quality_parameter_config-form'))) {
				
				Joomla.submitform(task, document.getElementById('milk_quality_parameter_config-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_edairy&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="milk_quality_parameter_config-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_EDAIRY_TITLE_MILK_QUALITY_PARAMETER_CONFIG', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

									<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
				<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
				<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
				<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
				<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />

				<?php echo $this->form->renderField('created_by'); ?>
				<?php echo $this->form->renderField('modified_by'); ?>				<?php echo $this->form->renderField('mb_range1_low'); ?>
				<?php echo $this->form->renderField('mb_range1_high'); ?>
				<?php echo $this->form->renderField('mb_range1_change'); ?>
				<?php echo $this->form->renderField('mb_range2_low'); ?>
				<?php echo $this->form->renderField('mb_range2_high'); ?>
				<?php echo $this->form->renderField('mb_range2_change'); ?>
				<?php echo $this->form->renderField('mb_range3_low'); ?>
				<?php echo $this->form->renderField('mb_range3_high'); ?>
				<?php echo $this->form->renderField('mb_range3_change'); ?>
				<?php echo $this->form->renderField('mb_range4_low'); ?>
				<?php echo $this->form->renderField('mb_range4_high'); ?>
				<?php echo $this->form->renderField('mb_range4_change'); ?>
				<?php echo $this->form->renderField('mb_range5_low'); ?>
				<?php echo $this->form->renderField('mb_range5_high'); ?>
				<?php echo $this->form->renderField('mb_range5_change'); ?>
				<?php echo $this->form->renderField('mb_range6_low'); ?>
				<?php echo $this->form->renderField('mb_range6_high'); ?>
				<?php echo $this->form->renderField('mb_range6_change'); ?>
				<?php echo $this->form->renderField('mb_range7_low'); ?>
				<?php echo $this->form->renderField('mb_range7_high'); ?>
				<?php echo $this->form->renderField('mb_range7_change'); ?>
				<?php echo $this->form->renderField('mb_range8_low'); ?>
				<?php echo $this->form->renderField('mb_range8_high'); ?>
				<?php echo $this->form->renderField('mb_range8_change'); ?>
				<?php echo $this->form->renderField('mb_range9_low'); ?>
				<?php echo $this->form->renderField('mb_range9_high'); ?>
				<?php echo $this->form->renderField('mb_range9_change'); ?>
				<?php echo $this->form->renderField('mb_range10_low'); ?>
				<?php echo $this->form->renderField('mb_range10_high'); ?>
				<?php echo $this->form->renderField('mb_range10_change'); ?>
				<?php echo $this->form->renderField('scc_range1_low'); ?>
				<?php echo $this->form->renderField('scc_range1_high'); ?>
				<?php echo $this->form->renderField('scc_range1_change'); ?>
				<?php echo $this->form->renderField('scc_range2_low'); ?>
				<?php echo $this->form->renderField('scc_range2_high'); ?>
				<?php echo $this->form->renderField('scc_range2_change'); ?>
				<?php echo $this->form->renderField('scc_range3_low'); ?>
				<?php echo $this->form->renderField('scc_range3_high'); ?>
				<?php echo $this->form->renderField('scc_range3_change'); ?>
				<?php echo $this->form->renderField('scc_range4_low'); ?>
				<?php echo $this->form->renderField('scc_range4_high'); ?>
				<?php echo $this->form->renderField('scc_range4_change'); ?>
				<?php echo $this->form->renderField('scc_range5_low'); ?>
				<?php echo $this->form->renderField('scc_range5_high'); ?>
				<?php echo $this->form->renderField('scc_range5_change'); ?>
				<?php echo $this->form->renderField('scc_range6_low'); ?>
				<?php echo $this->form->renderField('scc_range6_high'); ?>
				<?php echo $this->form->renderField('scc_range6_change'); ?>
				<?php echo $this->form->renderField('scc_range7_low'); ?>
				<?php echo $this->form->renderField('scc_range7_high'); ?>
				<?php echo $this->form->renderField('scc_range7_change'); ?>
				<?php echo $this->form->renderField('scc_range8_low'); ?>
				<?php echo $this->form->renderField('scc_range8_high'); ?>
				<?php echo $this->form->renderField('scc_range8_change'); ?>
				<?php echo $this->form->renderField('scc_range9_low'); ?>
				<?php echo $this->form->renderField('scc_range9_high'); ?>
				<?php echo $this->form->renderField('scc_range9_change'); ?>
				<?php echo $this->form->renderField('scc_range10_low'); ?>
				<?php echo $this->form->renderField('scc_range10_high'); ?>
				<?php echo $this->form->renderField('scc_range10_change'); ?>
				<?php echo $this->form->renderField('fat_range1_low'); ?>
				<?php echo $this->form->renderField('fat_range1_high'); ?>
				<?php echo $this->form->renderField('fat_range1_change'); ?>
				<?php echo $this->form->renderField('fat_range2_low'); ?>
				<?php echo $this->form->renderField('fat_range2_high'); ?>
				<?php echo $this->form->renderField('fat_range2_change'); ?>
				<?php echo $this->form->renderField('fat_range3_low'); ?>
				<?php echo $this->form->renderField('fat_range3_high'); ?>
				<?php echo $this->form->renderField('fat_range3_change'); ?>
				<?php echo $this->form->renderField('fat_range4_low'); ?>
				<?php echo $this->form->renderField('fat_range4_high'); ?>
				<?php echo $this->form->renderField('fat_range4_change'); ?>
				<?php echo $this->form->renderField('fat_range5_low'); ?>
				<?php echo $this->form->renderField('fat_range5_high'); ?>
				<?php echo $this->form->renderField('fat_range5_change'); ?>
				<?php echo $this->form->renderField('fat_range6_low'); ?>
				<?php echo $this->form->renderField('fat_range6_high'); ?>
				<?php echo $this->form->renderField('fat_range6_change'); ?>
				<?php echo $this->form->renderField('fat_range7_low'); ?>
				<?php echo $this->form->renderField('fat_range7_high'); ?>
				<?php echo $this->form->renderField('fat_range7_change'); ?>
				<?php echo $this->form->renderField('fat_range8_low'); ?>
				<?php echo $this->form->renderField('fat_range8_high'); ?>
				<?php echo $this->form->renderField('fat_range8_change'); ?>
				<?php echo $this->form->renderField('fat_range9_low'); ?>
				<?php echo $this->form->renderField('fat_range9_high'); ?>
				<?php echo $this->form->renderField('fat_range9_change'); ?>
				<?php echo $this->form->renderField('fat_range10_low'); ?>
				<?php echo $this->form->renderField('fat_range10_high'); ?>
				<?php echo $this->form->renderField('fat_range10_change'); ?>


					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
