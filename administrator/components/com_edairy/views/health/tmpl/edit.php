<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_edairy/css/form.css');

if($this->item->create_date==""){
	$this->item->create_date = date("Y-m-d");
}
?>

<script src="../media/com_edairy/typeahead/bootstrap3-typeahead.js" type="text/javascript" charset="UTF-8"></script>

<script src="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript" charset="UTF-8"></script>

<link rel="stylesheet" href="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.css">


<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		js('.datetimepicker').datetimepicker({
            lang:'th',
            timepicker:false,
            format:'d/m/Y'
        });
	js('input:hidden.farm_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('farm_idhidden')){
			js('#jform_farm_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_farm_id").trigger("liszt:updated");
	js('input:hidden.farmer_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('farmer_idhidden')){
			js('#jform_farmer_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_farmer_id").trigger("liszt:updated");
	js('input:hidden.coop_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('coop_idhidden')){
			js('#jform_coop_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_coop_id").trigger("liszt:updated");
	});

	Joomla.submitbutton = function (task) {
		if (task == 'health.cancel') {
			Joomla.submitform(task, document.getElementById('health-form'));
		}
		else {
			
			if (task != 'health.cancel' && document.formvalidator.isValid(document.id('health-form'))) {
				
				Joomla.submitform(task, document.getElementById('health-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_edairy&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="health-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_EDAIRY_TITLE_HEALTH', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

									<?php echo $this->form->renderField('id'); ?>
				<table>
										<tr>
											<td>
				<?php echo $this->form->renderField('run_no'); ?>
											</td>
											<td>
												&nbsp;&nbsp;
											</td>
											<td>
				<?php // echo $this->form->renderField('create_date'); ?>
												<div class="control-group">
													<div class="control-label">
														<label>
															วันที่
														</label>
													</div>
													<div class="controls">
														<input type="text" name="jform[create_date]" class="datetimepicker"  value="<?php echo formatDate($this->item->create_date); ?>"/>
													</div>
												</div>
											</td>
										</tr>
									</table>
									<table>
										<tr>
											<td>
												<?php echo $this->form->renderField('address'); ?>
											</td>
											<td>
												&nbsp;&nbsp;
											</td>
											<td>
												<?php echo $this->form->renderField('payment_type'); ?>
											</td>
										</tr>
									</table>

									<hr />
					<div style="width:1380px;text-align:right">
						<input class="btn btn-primary" type="button" value="เพิ่มรายการ" onClick="javascript:addNewRow();" />
					</div>
					<br />
					<table border="1" style="max-width:1380px" cellpadding="5" class="clonableTable">
						<tr>
							<th style="text-align:center">ที่</th>
							<th style="text-align:center">หมายเลขโค - ชื่อโค</th>
							<th style="text-align:center">สถานภาพ</th>
							<th style="text-align:center">ชื่อโคไม่ขึ้นทะเบียน</th>
							<th style="text-align:center">บริการ - รายละเอียดบริการ</th>
							<th style="text-align:center">งานเวชภัณฑ์/วัสดุ</th>
							<th style="text-align:center">หน่วยละ</th>
							<th style="text-align:center">จำนวน</th>
							<th style="text-align:center">จำนวนเงิน (บาท)</th>
							<th style="text-align:center">ลบ</th>
						</tr>
						<?php if(count($this->item->health_line)!=0){ ?>
							<?php for($i=0;$i<count($this->item->health_line);$i++){ ?>
								<tr>
									<td><div class="row-index"><?php echo $i+1; ?></div></td>
									<td>
										<input class="cow-typeahead" name="cow_id[]" value="<?php if($this->item->health_line[$i]->cow->cow_id!=""){echo $this->item->health_line[$i]->cow->cow_id . " " . $this->item->health_line[$i]->cow->name;}?>" placeholder="Loading..." />
										
									</td>
									<td>
										<select name="cow_status[]" style="width:180px">
											<option value="">-- เลือกสถานภาพ --</option>
											<?php 
							foreach($this->masterData->mas_cow_status as $bkey=>$bdata){ 
								$selected = "";
								if($bdata->id == $this->item->cow_status_id){
									$selected = " selected";
								}
						?>
								<option value="<?php echo $bdata->id;?>"<?php echo $selected; ?>><?php echo $bdata->name; ?> <?php echo $bdata->description; ?></option>
						<?php } ?>
										</select>
									</td>
									<td><input type="text" name="cow_name[]" style="width:150px" value="<?php echo $this->item->health_line[$i]->cow_name;?>"/></td>
									<td>
										<select name="cow_health_service_id[]">
											<option value="">-- เลือกบริการ --</option>
											 	<?php 
													foreach($this->masterData->mas_cow_health_service as $bkey=>$bdata){
														$selected = "";
														if($this->item->health_line[$i]->cow_health_service_id == $bdata->id){
															$selected = " selected";
														} 
												?>
														<option value="<?php echo $bdata->id;?>"<?php echo $selected; ?>><?php echo $bdata->name; ?></option>
												<?php } ?>
										</select>
										<select name="cow_health_sub_service_id[]">
											<option value="">-- เลือกรายละเอียดบริการ --</option>
											 	<?php 
													foreach($this->masterData->mas_cow_health_sub_service as $bkey=>$bdata){
														$selected = "";
														if($this->item->health_line[$i]->cow_health_sub_service_id == $bdata->id){
															$selected = " selected";
														} 
												?>
														<option value="<?php echo $bdata->id;?>"<?php echo $selected; ?>><?php echo $bdata->name; ?></option>
												<?php } ?>
										</select>
									</td>
									<td>
										<select name="cow_medicine_id[]">
											<option value="">-- เลือกงานเวชภัณฑ์ --</option>
											 	<?php 
													foreach($this->masterData->mas_cow_medicine as $bkey=>$bdata){
														$selected = "";
														if($this->item->health_line[$i]->cow_medicine_id == $bdata->id){
															$selected = " selected";
														} 
												?>
														<option value="<?php echo $bdata->id;?>"<?php echo $selected; ?>><?php echo $bdata->name; ?></option>
												<?php } ?>
										</select>
									</td>
									<td><input type="text" name="price[]" style="width:80px" onChange="javascipt:lineSum();" value="<?php echo $this->item->health_line[$i]->price; ?>" /></td>
									<td><input type="text" name="amount[]" onChange="javascript:lineSum();" style="width:40px" value="<?php echo $this->item->health_line[$i]->amount; ?>" /></td>
									<td><input type="text" name="total_price[]" style="width:80px" value="<?php echo $this->item->health_line[$i]->total_price; ?>" readonly/></td>
									<td style="text-align:center"><input type="checkbox" name="delete_mark[]" value="<?php echo $i; ?>" onChange="javascript:sum();" /></td>
								</tr>
							<?php } ?>
						<?php }else{ ?>
							<?php for($i=0;$i<5;$i++){ ?>
								<tr>
									<td><div class="row-index"><?php echo $i+1; ?></div></td>
									<td>
										<input class="cow-typeahead" name="cow_id[]" value="" placeholder="Loading..." />
									</td>
									<td>
										<select name="cow_status[]" style="width:180px">
											<option value="">-- เลือกสถานภาพ --</option>
											<?php 
							foreach($this->masterData->mas_cow_status as $bkey=>$bdata){ 
								$selected = "";
								if($bdata->id == $this->item->cow_status_id){
									$selected = " selected";
								}
						?>
								<option value="<?php echo $bdata->id;?>"<?php echo $selected; ?>><?php echo $bdata->name; ?> <?php echo $bdata->description; ?></option>
						<?php } ?>
										</select>
									</td>
									<td><input type="text" name="cow_name[]" style="width:150px" /></td>
									<td>
										<select name="cow_health_service_id[]">
											<option value="">-- เลือกบริการ --</option>
											 	<?php 
													foreach($this->masterData->mas_cow_health_service as $bkey=>$bdata){
														$selected = "";
												?>
														<option value="<?php echo $bdata->id;?>"<?php echo $selected; ?>><?php echo $bdata->name; ?></option>
												<?php } ?>
										</select>
										<select name="cow_health_sub_service_id[]">
											<option value="">-- เลือกรายละเอียดบริการ --</option>
											 	<?php 
													foreach($this->masterData->mas_cow_health_sub_service as $bkey=>$bdata){
														$selected = "";
												?>
														<option value="<?php echo $bdata->id;?>"<?php echo $selected; ?>><?php echo $bdata->name; ?></option>
												<?php } ?>
										</select>
									</td>
									<td>
										<select name="cow_medicine_id[]">
											<option value="">-- เลือกงานเวชภัณฑ์ --</option>
											 	<?php 
													foreach($this->masterData->mas_cow_medicine as $bkey=>$bdata){
														$selected = "";
												?>
														<option value="<?php echo $bdata->id;?>"<?php echo $selected; ?>><?php echo $bdata->name; ?></option>
												<?php } ?>
										</select>
									</td>
									<td><input type="text" name="price[]" style="width:80px" onChange="javascipt:lineSum();" /></td>
									<td><input type="text" name="amount[]" onChange="javascript:lineSum();" style="width:40px" /></td>
									<td><input type="text" name="total_price[]" style="width:80px" readonly/></td>
									<td style="text-align:center"><input type="checkbox" name="delete_mark[]" value="<?php echo $i; ?>" onChange="javascript:sum();" /></td>
								</tr>
							<?php } ?>
						<?php } ?>
					</table>
					<div style="clear:both"></div>
					<br />

					<?php echo $this->form->renderField('grand_total'); ?>

					<hr />
					<br />

				<?php echo $this->form->renderField('user_id1'); ?>
				<?php echo $this->form->renderField('user_id2'); ?>
				<?php echo $this->form->renderField('farm_id'); ?>

			<?php
				foreach((array)$this->item->farm_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="farm_id" name="jform[farm_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('farmer_id'); ?>

			<?php
				foreach((array)$this->item->farmer_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="farmer_id" name="jform[farmer_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('coop_id'); ?>

			<?php
				foreach((array)$this->item->coop_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="coop_id" name="jform[coop_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				
			<div style="display:none">
				<?php echo $this->form->renderField('state'); ?>
			</div>
				<?php echo $this->form->renderField('created_by'); ?>


					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>


<script type="text/javascript">

	var temp_data;

	<?php if(count($this->item->health_line)!=0){ ?>
		var rowCount=<?php echo count($this->item->health_line); ?>;
	<?php }else{ ?>
		var rowCount=5;
	<?php } ?>
	function addNewRow(){
		var tr    = jQuery('.clonableTable tr:last');
		var clone = tr.clone();
		clone.removeClass("chzn-done");
		clone.find(':text').val('');
		clone.find('select').val('');
		clone.find('.chzn-container').remove();
		clone.find('select').css({display: "inline-block"});
		clone.find('select').css({width: "100%"});
		clone.find('.health_time').css({width: "70px"});
		clone.find('.health_time').val(1);
		clone.find('.row-index').html(++rowCount);
		clone.find('select').chosen();
		clone.find(".cow-typeahead").typeahead({ source:temp_data });

		tr.after(clone);
	}

	function lineSum(){
	  var lineIndex = 0;
	  jQuery("input[name='total_price[]']").each(function(){

	  	if(!isNaN(parseFloat(jQuery("input[name='amount[]']").eq(lineIndex).val())*parseFloat(jQuery("input[name='price[]']").eq(lineIndex).val()))){
		    jQuery(this).val(parseFloat(jQuery("input[name='amount[]']").eq(lineIndex).val())*parseFloat(jQuery("input[name='price[]']").eq(lineIndex).val()));
		}
	    lineIndex++;      
	  });

	 sum();
	}

	function sum(){
	  var total=0;
	  var lineIndex = 0;
	  jQuery("input[name='total_price[]']").each(function(){
	          //console.log(parseFloat($(this).val()));
	          if(!isNaN(parseFloat(jQuery(this).val())) && !jQuery("input[name='delete_mark[]']").eq(lineIndex).is(':checked')){
	            total+=parseFloat(jQuery(this).val());
	          }
	          lineIndex++;
	        });
	  jQuery("input[name='jform[grand_total]']").val(total.toFixed(2));
	}
	

	jQuery(".cow-typeahead").prop("disabled", true);

	
	jQuery.get("index.php?option=com_edairy&task=cow.getAllCow", function(data){
		
		//console.log(data);
	  jQuery(".cow-typeahead").typeahead({ source:data });

	  temp_data = data;

	  jQuery(".cow-typeahead").prop("disabled", false);
	  jQuery(".cow-typeahead").prop("placeholder", "Auto Complete");

	},'json');
</script>