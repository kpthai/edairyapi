<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_edairy/css/form.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	js('input:hidden.region_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('region_idhidden')){
			js('#jform_region_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_region_id").trigger("liszt:updated");
	js('input:hidden.province_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('province_idhidden')){
			js('#jform_province_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_province_id").trigger("liszt:updated");
	js('input:hidden.district_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('district_idhidden')){
			js('#jform_district_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_district_id").trigger("liszt:updated");
	js('input:hidden.sub_district_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('sub_district_idhidden')){
			js('#jform_sub_district_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_sub_district_id").trigger("liszt:updated");
	});

	Joomla.submitbutton = function (task) {
		if (task == 'farmer.cancel') {
			Joomla.submitform(task, document.getElementById('farmer-form'));
		}
		else {
			
			if (task != 'farmer.cancel' && document.formvalidator.isValid(document.id('farmer-form'))) {
				
				Joomla.submitform(task, document.getElementById('farmer-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_edairy&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="farmer-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_EDAIRY_TITLE_FARMER', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

									<?php echo $this->form->renderField('id'); ?>
				<?php echo $this->form->renderField('citizen_id'); ?>
				<?php echo $this->form->renderField('image_url'); ?>

				<?php if (!empty($this->item->image_url)) : ?>
					<?php foreach ((array)$this->item->image_url as $fileSingle) : ?>
						<?php if (!is_array($fileSingle)) : ?>
							<a href="<?php echo JRoute::_(JUri::root() . 'upload/' . DIRECTORY_SEPARATOR . $fileSingle, false);?>"><?php echo $fileSingle; ?></a> | 
						<?php endif; ?>
					<?php endforeach; ?>
				<?php endif; ?>
				<input type="hidden" name="jform[image_url_hidden]" id="jform_image_url_hidden" value="<?php echo implode(',', (array)$this->item->image_url); ?>" />				<?php echo $this->form->renderField('title_id'); ?>
				<?php echo $this->form->renderField('name'); ?>
				<?php echo $this->form->renderField('surname'); ?>
				<?php echo $this->form->renderField('birthdate'); ?>
				<?php echo $this->form->renderField('education_level_id'); ?>
				<?php echo $this->form->renderField('address'); ?>
				<?php echo $this->form->renderField('region_id'); ?>

			<?php
				foreach((array)$this->item->region_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="region_id" name="jform[region_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('province_id'); ?>

			<?php
				foreach((array)$this->item->province_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="province_id" name="jform[province_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('district_id'); ?>

			<?php
				foreach((array)$this->item->district_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="district_id" name="jform[district_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('sub_district_id'); ?>

			<?php
				foreach((array)$this->item->sub_district_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="sub_district_id" name="jform[sub_district_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('post_code'); ?>
				<?php echo $this->form->renderField('latitude'); ?>
				<?php echo $this->form->renderField('longitude'); ?>
				<?php echo $this->form->renderField('mobile'); ?>
				<?php echo $this->form->renderField('fax'); ?>
				<?php echo $this->form->renderField('email'); ?>
				<?php echo $this->form->renderField('line_id'); ?>
				<?php echo $this->form->renderField('state'); ?>
				<?php echo $this->form->renderField('created_by'); ?>


					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
