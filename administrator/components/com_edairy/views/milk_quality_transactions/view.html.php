<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Edairy.
 *
 * @since  1.6
 */
class EdairyViewMilk_quality_transactions extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');
        $this->filterForm = $this->get('FilterForm');
        $this->activeFilters = $this->get('ActiveFilters');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		EdairyHelper::addSubmenu('milk_quality_transactions');

		$this->addToolbar();

		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function addToolbar()
	{
		$state = $this->get('State');
		$canDo = EdairyHelper::getActions();

		JToolBarHelper::title(JText::_('COM_EDAIRY_TITLE_MILK_QUALITY_TRANSACTIONS'), 'milk_quality_transactions.png');

		// Check if the form exists before showing the add/edit buttons
		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/milk_quality_transaction';

		if (file_exists($formPath))
		{
			if ($canDo->get('core.create'))
			{
				JToolBarHelper::addNew('milk_quality_transaction.add', 'JTOOLBAR_NEW');

				if (isset($this->items[0]))
				{
					// JToolbarHelper::custom('milk_quality_transactions.duplicate', 'copy.png', 'copy_f2.png', 'JTOOLBAR_DUPLICATE', true);
				}
			}

			if ($canDo->get('core.edit') && isset($this->items[0]))
			{
				// JToolBarHelper::editList('milk_quality_transaction.edit', 'JTOOLBAR_EDIT');
			}
		}

		if ($canDo->get('core.edit.state'))
		{
			if (isset($this->items[0]->state))
			{
				// JToolBarHelper::divider();
				// JToolBarHelper::custom('milk_quality_transactions.publish', 'publish.png', 'publish_f2.png', 'JTOOLBAR_PUBLISH', true);
				// JToolBarHelper::custom('milk_quality_transactions.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
			}
			elseif (isset($this->items[0]))
			{
				// If this component does not use state then show a direct delete button as we can not trash
				// JToolBarHelper::deleteList('', 'milk_quality_transactions.delete', 'JTOOLBAR_DELETE');
			}

			if (isset($this->items[0]->state))
			{
				// JToolBarHelper::divider();
				// JToolBarHelper::archiveList('milk_quality_transactions.archive', 'JTOOLBAR_ARCHIVE');
			}

			if (isset($this->items[0]->checked_out))
			{
				// JToolBarHelper::custom('milk_quality_transactions.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
			}
		}

		// Show trash and delete for components that uses the state field
		if (isset($this->items[0]->state))
		{
			if ($state->get('filter.state') == -2 && $canDo->get('core.delete'))
			{
				// JToolBarHelper::deleteList('', 'milk_quality_transactions.delete', 'JTOOLBAR_EMPTY_TRASH');
				// JToolBarHelper::divider();
			}
			elseif ($canDo->get('core.edit.state'))
			{
				// JToolBarHelper::trash('milk_quality_transactions.trash', 'JTOOLBAR_TRASH');
				// JToolBarHelper::divider();
			}
		}

		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_edairy');
		}

		// Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_edairy&view=milk_quality_transactions');
	}

	/**
	 * Method to order fields 
	 *
	 * @return void 
	 */
	protected function getSortFields()
	{
		return array(
			'a.`id`' => JText::_('JGRID_HEADING_ID'),
			'a.`milk_quality_id`' => JText::_('COM_EDAIRY_MILK_QUALITY_TRANSACTIONS_MILK_QUALITY_ID'),
			'a.`create_date`' => JText::_('COM_EDAIRY_MILK_QUALITY_TRANSACTIONS_CREATE_DATE'),
			'a.`create_time`' => JText::_('COM_EDAIRY_MILK_QUALITY_TRANSACTIONS_CREATE_TIME'),
			'a.`coop_id`' => JText::_('COM_EDAIRY_MILK_QUALITY_TRANSACTIONS_COOP_ID'),
			'a.`license_plate`' => JText::_('COM_EDAIRY_MILK_QUALITY_TRANSACTIONS_LICENSE_PLATE'),
			'a.`price`' => JText::_('COM_EDAIRY_MILK_QUALITY_TRANSACTIONS_PRICE'),
			'a.`amount`' => JText::_('COM_EDAIRY_MILK_QUALITY_TRANSACTIONS_AMOUNT'),
			'a.`total_price`' => JText::_('COM_EDAIRY_MILK_QUALITY_TRANSACTIONS_TOTAL_PRICE'),
			'a.`ordering`' => JText::_('JGRID_HEADING_ORDERING'),
			'a.`state`' => JText::_('JSTATUS'),
		);
	}

    /**
     * Check if state is set
     *
     * @param   mixed  $state  State
     *
     * @return bool
     */
    public function getState($state)
    {
        return isset($this->state->{$state}) ? $this->state->{$state} : false;
    }
}
