<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Edairy.
 *
 * @since  1.6
 */
class EdairyViewFarms extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');
        $this->filterForm = $this->get('FilterForm');
        $this->activeFilters = $this->get('ActiveFilters');

        $this->masterData = $this->get('MasterData');


		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		EdairyHelper::addSubmenu('farms');

		$this->addToolbar();

		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function addToolbar()
	{
		$state = $this->get('State');
		$canDo = EdairyHelper::getActions();

		if($_REQUEST["layout"]=="farm_survey"){
			JToolBarHelper::title(JText::_('สำรวจสถานภาพโค'), 'farms.png');
		}else{
			JToolBarHelper::title(JText::_('COM_EDAIRY_TITLE_FARMS'), 'farms.png');
		}

		// Check if the form exists before showing the add/edit buttons
		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/farm';

		if (file_exists($formPath))
		{
			if ($canDo->get('core.create'))
			{
				JToolBarHelper::addNew('farm.add', 'JTOOLBAR_NEW');

				if (isset($this->items[0]))
				{
					//JToolbarHelper::custom('farms.duplicate', 'copy.png', 'copy_f2.png', 'JTOOLBAR_DUPLICATE', true);
				}
			}

			if ($canDo->get('core.edit') && isset($this->items[0]))
			{
				//JToolBarHelper::editList('farm.edit', 'JTOOLBAR_EDIT');
			}
		}

		if ($canDo->get('core.edit.state'))
		{
			if (isset($this->items[0]->state))
			{
				/*JToolBarHelper::divider();
				JToolBarHelper::custom('farms.publish', 'publish.png', 'publish_f2.png', 'JTOOLBAR_PUBLISH', true);
				JToolBarHelper::custom('farms.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);*/
			}
			elseif (isset($this->items[0]))
			{
				// If this component does not use state then show a direct delete button as we can not trash
				//JToolBarHelper::deleteList('', 'farms.delete', 'JTOOLBAR_DELETE');
			}

			if (isset($this->items[0]->state))
			{
				/*JToolBarHelper::divider();
				JToolBarHelper::archiveList('farms.archive', 'JTOOLBAR_ARCHIVE');*/
			}

			if (isset($this->items[0]->checked_out))
			{
				//JToolBarHelper::custom('farms.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
			}
		}

		// Show trash and delete for components that uses the state field
		if (isset($this->items[0]->state))
		{
			if ($state->get('filter.state') == -2 && $canDo->get('core.delete'))
			{
				/*JToolBarHelper::deleteList('', 'farms.delete', 'JTOOLBAR_EMPTY_TRASH');
				JToolBarHelper::divider();*/
			}
			elseif ($canDo->get('core.edit.state'))
			{
				/*JToolBarHelper::trash('farms.trash', 'JTOOLBAR_TRASH');
				JToolBarHelper::divider();*/
			}
		}

		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_edairy');
		}

		// Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_edairy&view=farms');
	}

	/**
	 * Method to order fields 
	 *
	 * @return void 
	 */
	protected function getSortFields()
	{
		return array(
			'a.`id`' => JText::_('JGRID_HEADING_ID'),
			'a.`member_code`' => JText::_('COM_EDAIRY_FARMS_MEMBER_CODE'),
			'a.`name`' => JText::_('COM_EDAIRY_FARMS_NAME'),
			'a.`coop_id`' => JText::_('COM_EDAIRY_FARMS_COOP_ID'),
			'a.`province_id`' => JText::_('COM_EDAIRY_FARMS_PROVINCE_ID'),
			'a.`region_id`' => JText::_('COM_EDAIRY_FARMS_REGION_ID'),
			'a.`status`' => JText::_('COM_EDAIRY_FARMS_STATUS'),
			'a.`state`' => JText::_('JSTATUS'),
		);
	}

    /**
     * Check if state is set
     *
     * @param   mixed  $state  State
     *
     * @return bool
     */
    public function getState($state)
    {
        return isset($this->state->{$state}) ? $this->state->{$state} : false;
    }
}
