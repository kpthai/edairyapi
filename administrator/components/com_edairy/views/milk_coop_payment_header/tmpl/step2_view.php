<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_edairy/css/form.css');
?>

<script src="../media/com_edairy/typeahead/bootstrap3-typeahead.js" type="text/javascript" charset="UTF-8"></script>

<script src="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript" charset="UTF-8"></script>

<link rel="stylesheet" href="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.css">


<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {

		js('.datetimepicker').datetimepicker({
            lang:'th',
            timepicker:false,
            format:'d/m/Y'
        });
		
	js('input:hidden.coop_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('coop_idhidden')){
			js('#jform_coop_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_coop_id").trigger("liszt:updated");
	});

	Joomla.submitbutton = function (task) {
		if (task == 'milk_payment_header.cancel') {
			Joomla.submitform(task, document.getElementById('milk_payment_header-form'));
		}
		else {
			
			if (task != 'milk_payment_header.cancel' && document.formvalidator.isValid(document.id('milk_payment_header-form'))) {
				
				Joomla.submitform(task, document.getElementById('milk_payment_header-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

</style>
<form
	action="<?php echo JRoute::_('index.php?option=com_edairy&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="milk_payment_header-form" class="form-validate">




	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_EDAIRY_TITLE_MILK_PAYMENT_HEADER', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

					<b>ขั้นตอนการทำงาน</b>
					<table width="100%" border="1" cellpadding="5">
						<tr>
							<td width="20%" style="background:green;color:white">1. ใส่ข้อมูล</td>
							<td width="20%" style="background:yellow">2. สรุปค่าน้ำนมดิบ</td>
							<td width="20%">3. สรุปค่าใช้จ่าย</td>
							<td width="40%"></td>
						</tr>
					</table>
					<hr />
					
					สหกรณ์ <?php 

						foreach($this->masterData->mas_coop as $key=>$data){
							if($data->id == $_REQUEST["coop_id"]){
								echo $data->coop_abbr . " " . $data->name;
								break; 
							}
						}
					?> ประจำงวดที่ <?php echo $_REQUEST["period"]; ?> ระหว่างันที่ <?php echo $_REQUEST["from_date"]; ?> ถึง <?php echo $_REQUEST["to_date"]; ?> <br /><br />

					<table width="100%" border="1" cellpadding="5">
						<tr>
							<th>ที่</th>
							<th>วันที่ตรวจคุณภาพ</th>
							<th>ทะเบียนรถ</th>
							<th>ปริมาณ (กก.)</th>
							<th>ราคาที่ได้ (บาท/กก.)</th>
							<th>เป็นเงิน (บาท)</th>
							<th>เรียกดูผลคุณภาพน้ำนม</th>
						</tr>
						<?php 
							$i=1;
							$sum_amount = 0;
							$sum_total_price = 0;

							foreach($this->stepData as $key=>$data){ 
								$sum_amount += $data->amount;
								$sum_total_price += $data->total_price;
							?>
							<tr>
								<td><?php echo $i++; ?></td>
								<td><?php echo formatDate($data->create_date); ?></td>
								<td><?php echo $data->license_plate; ?></td>
								<td align="right"><?php echo $data->amount; ?></td>
								<td align="right"><?php echo $data->price; ?></td>
								<td align="right"><?php echo $data->total_price; ?></td>
								<td style="text-align: center">
									<input type="button" onClick="javascript:window.open('index.php?option=com_edairy&view=milk_coop_quality&layout=edit&id=<?php echo $data->id; ?>');" name="" class="btn btn-primary" value="เรียกดู" style="width:100px" />
								</td>
							</tr>
						<?php
							}
						?>
					</table>

					<br /><br />

					ปริมาณน้ำนมดิบรวม <?php echo number_format($sum_amount,2); ?> กก.<br />
					รวมจำนวนเงินทั้งสิ้น <?php echo number_format($sum_total_price,2); ?> บาท
						</tr>
					</table>

					<br /><br />
			
				<?php /*
				<?php echo $this->form->renderField('from_date'); ?>
				<?php echo $this->form->renderField('to_date'); ?>
				*/ ?>


				


				

				<div style="display:none">

					<?php echo $this->form->renderField('total_send_milk_amount'); ?>
					<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />

					<div style="float:right">
							<button class="btn btn-info" data-toggle="modal" data-target="#QualityModal"  onClick="javascript:clearQualityForm();">เพิ่มรายการ</button>
					</div>
					<br /><br />
					<div style="clear:both"></div>

					<div style="display: none">
						<?php echo $this->form->renderField('state'); ?>
					</div>
					<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
					<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />

				</div>
				<?php echo $this->form->renderField('created_by'); ?>

				<?php echo $this->form->renderField('modified_by'); ?>

					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>

					<hr />

					<input type="hidden" name="coop_id" value="<?php echo $_REQUEST[coop_id]; ?>" />
					<input type="hidden" name="period" value="<?php echo $_REQUEST[period]; ?>" />
					<input type="hidden" name="from_date" value="<?php echo $_REQUEST[from_date]; ?>" />
					<input type="hidden" name="to_date" value="<?php echo $_REQUEST[to_date]; ?>" />

					<input type="button"  onClick="javascript:window.location='index.php?option=com_edairy&view=milk_coop_payment_header&layout=step3_view&id=<?php echo $_REQUEST[id]; ?>&coop_id=<?php echo $_REQUEST[coop_id]; ?>&period=<?php echo $_REQUEST[coop_id]; ?>&from_date=<?php echo $_REQUEST[from_date]; ?>&to_date=<?php echo $_REQUEST[to_date]; ?>';" class="btn btn-primary" value="ดูสรุปค่าใช้จ่าย >" style="width:150px" /> &nbsp;&nbsp;
					<input type="button" class="btn btn-danger" value="ยกเลิก" style="width:100px" onClick="javascript:window.location='index.php?option=com_edairy&view=milk_coop_payment_headers';" /> 
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value="milk_coop_payment_header.step2"/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>

