<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_edairy/css/form.css');


//AIzaSyD9kSzIrHDG2nYTH3S-ni9oBTEGN4JM7wk
?>
<style type="text/css">
	/* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #description {
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
      }

      #infowindow-content .title {
        font-weight: bold;
      }

      #infowindow-content {
        display: none;
      }

      #map #infowindow-content {
        display: inline;
      }

      .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
      }

      #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
      }

      .pac-controls {
        display: inline-block;
        padding: 5px 11px;
      }

      .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      #title {
        color: #fff;
        background-color: #4d90fe;
        font-size: 25px;
        font-weight: 500;
        padding: 6px 12px;
      }
      #target {
        width: 345px;
      }
      fieldset.radio label{
      	width:150px!important;
      }
</style>

<script src="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript" charset="UTF-8"></script>

<link rel="stylesheet" href="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.css">

<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {


		js('#pac-input').bind('keypress', function(e)
{
   if(e.keyCode == 13)
   {
      return false;
   }
});


		js('.datetimepicker').datetimepicker({
            lang:'th',
            timepicker:false,
            format:'d/m/Y'
        });
	js('input:hidden.region_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('region_idhidden')){
			js('#jform_region_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_region_id").trigger("liszt:updated");
	js('input:hidden.province_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('province_idhidden')){
			js('#jform_province_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_province_id").trigger("liszt:updated");
	js('input:hidden.district_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('district_idhidden')){
			js('#jform_district_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_district_id").trigger("liszt:updated");
	js('input:hidden.sub_district_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('sub_district_idhidden')){
			js('#jform_sub_district_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_sub_district_id").trigger("liszt:updated");
	});
	Joomla.submitbutton = function (task) {
			if (task == 'coop.cancel') {
				Joomla.submitform(task, document.getElementById('coop-form'));
			}
			else {
				
				if (task != 'coop.cancel' && document.formvalidator.isValid(document.id('coop-form'))) {
					
					Joomla.submitform(task, document.getElementById('coop-form'));
				}
				else {
					alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
				}
			}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_edairy&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="coop-form" class="form-validate">

	<div class="form-horizontal">
		<?php 
			if($_REQUEST["default_tab"]==""){
				echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general'));
			}else{
				echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => $_REQUEST["default_tab"]));
			} 
		?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('ข้อมูลทั่วไป', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">
								<div style="display:none">
									<?php echo $this->form->renderField('id'); ?>
								</div>
				<?php echo $this->form->renderField('coop_code'); ?>
				<?php echo $this->form->renderField('coop_abbr'); ?>
				<?php echo $this->form->renderField('coop_type'); ?>
				<?php echo $this->form->renderField('name'); ?>
				<?php echo $this->form->renderField('address'); ?>
				<?php echo $this->form->renderField('region_id'); ?>

			<?php
				foreach((array)$this->item->region_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="region_id" name="jform[region_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('province_id'); ?>

			<?php
				foreach((array)$this->item->province_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="province_id" name="jform[province_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('district_id'); ?>

			<?php
				foreach((array)$this->item->district_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="district_id" name="jform[district_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('sub_district_id'); ?>

			<?php
				foreach((array)$this->item->sub_district_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="sub_district_id" name="jform[sub_district_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('post_code'); ?>

			<hr />
			<h4>โปรดเลือกพิกัดตำแหน่งที่ตั้ง</h4>
				<input id="pac-input" class="controls" type="text" placeholder="พิมพ์ชื่อสถานที่ ที่นี่" style="margin-top:10px">
				<div id="map" style="height:300px;width:100%"></div>
			<hr />
				<?php echo $this->form->renderField('latitude'); ?>
				<?php echo $this->form->renderField('longitude'); ?>
				<?php /*
				<?php echo $this->form->renderField('create_by'); ?>
				<?php echo $this->form->renderField('update_by'); ?>
				<?php echo $this->form->renderField('create_datetime'); ?>
				<?php echo $this->form->renderField('update_datetime'); ?>
				*/ ?>

				<div style="display:none">
					<?php echo $this->form->renderField('state'); ?>
				</div>
				<?php echo $this->form->renderField('created_by'); ?>


					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>



		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'gmp', JText::_('มาตรฐาน GMP', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">
					<div style="float:right">
						<button class="btn btn-info" data-toggle="modal" data-target="#GMPModal"  onClick="javascript:clearGMPForm();">เพิ่มรายการ</button>
					</div>
					<h3>ประวัติการรับรองมาตรฐาน GMP</h3>
					<table class="table table-striped table-bordered table-blue">
						<thead>
							<tr>
								<th style="text-align:center">ที่</th>
								<th style="text-align:center">หมายเลขการรับรอง</th>
								<th style="text-align:center">วันที่ได้รับการรับรอง</th>
								<th style="text-align:center">วันที่หมดอายุ</th>
								<th style="text-align:center">หน่วยงานรับรอง</th>
								<th style="text-align:center">ปรับปรุงข้อมูลล่าสุดโดย</th>
								<th style="text-align:center">วันที่ปรับปรุง</th>
								<th style="text-align:center">แก้ไข</th>
								<th style="text-align:center">ลบ</th>
							</tr>
						</thead>
						<tbody>
							<?php if(count($this->item->gmp)==0){ ?>
								<tr>
									<td style="text-align:center" colspan="9">ไม่พบข้อมูล</td>
								</tr>
							<?php }else{ ?>
								<?php $i=0;
									foreach($this->item->gmp as $key=>$data){ ?>
									<tr>
										<td><?php echo ++$i; ?></td>
										<td><?php echo $data->certify_no; ?></td>
										<td><?php echo formatDate($data->certify_date); ?></td>
										<td><?php echo formatDate($data->expire_date); ?></td>
										<td><?php echo $data->certify_agency; ?></td>
										<td><?php 
												$user = JFactory::getUser($data->update_by);
												echo $user->name; 
											?>
											
										</td>
										<td><?php echo formatDate($data->update_datetime); ?></td>
										<td style="text-align: center"><button type="button" class="btn btn-warning" data-toggle="modal" data-target="#GMPModal" onClick="javascript:getGMP(<?php echo $data->id; ?>);">แก้ไข</button></td>
										<td style="text-align: center"><button type="button" class="btn btn-danger" onClick="javascript: if(confirm('ยืนยันการลบ?')){jQuery(this).prop('disabled', true);deleteGMP(<?php echo $data->id; ?>);}">ลบ</button></td>
									</tr>
								<?php } ?>
							<?php } ?>
						</tbody>
					</table>
									
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>


		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'send_milk_company', JText::_('โรงงาน/บริษัทที่ส่งนม', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">
					<div style="float:right">
						<button class="btn btn-info" data-toggle="modal" data-target="#SMCModal" onClick="javascript:clearSMCForm();">เพิ่มรายการ</button>
					</div>
					<h3>โรงงาน/บริษัทที่ส่งนม</h3>
					<table class="table table-striped table-bordered table-blue">
						<thead>
							<tr>
								<th style="text-align:center">ที่</th>
								<th style="text-align:center">โรงงาน/บริษัทที่ส่งนม</th>
								<th style="text-align:center">ที่อยู่</th>
								<th style="text-align:center">ปรับปรุงข้อมูลล่าสุดโดย</th>
								<th style="text-align:center">วันที่ปรับปรุง</th>
								<th style="text-align:center">แก้ไข</th>
								<th style="text-align:center">ลบ</th>
							</tr>
						</thead>
						<tbody>
							<?php if(count($this->item->send_milk_company)==0){ ?>
								<tr>
									<td style="text-align:center" colspan="7">ไม่พบข้อมูล</td>
								</tr>
							<?php }else{ ?>
								<?php $i=0;
									foreach($this->item->send_milk_company as $key=>$data){ ?>
									<tr>
										<td><?php echo ++$i; ?></td>
										<td><?php echo $data->name; ?></td>
										<td><?php echo $data->address; ?></td>
										<td><?php 
												$user = JFactory::getUser($data->update_by);
												echo $user->name; 
											?>
											
										</td>
										<td><?php echo formatDate($data->update_datetime); ?></td>
										<td style="text-align: center"><button type="button" class="btn btn-warning" data-toggle="modal" data-target="#SMCModal" onClick="javascript:getSendMilkCompany(<?php echo $data->id; ?>);">แก้ไข</button></td>
										<td style="text-align: center"><button type="button" class="btn btn-danger" onClick="javascript: if(confirm('ยืนยันการลบ?')){jQuery(this).prop('disabled', true);deleteSendMilkCompany(<?php echo $data->id; ?>);}">ลบ</button></td>
									</tr>
								<?php } ?>
							<?php } ?>
						</tbody>
					</table>
									
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>




		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'mou', JText::_('MOU', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">
					<div style="float:right">
						<button class="btn btn-info" data-toggle="modal" data-target="#MOUModal" onClick="javascript:clearMOUForm();">เพิ่มรายการ</button>
					</div>
					<h3>ปริมาณนมที่ส่ง อ.ส.ค. (ตาม MOU)</h3>
					<table class="table table-striped table-bordered table-blue">
						<thead>
							<tr>
								<th style='text-align: center'>ที่</th>
								<th style='text-align: center'>ปี</th>
								<th style='text-align: center'>ปริมาณ (ตัน/วัน)</th>
								<th style='text-align: center'>ปรับปรุงข้อมูลล่าสุดโดย</th>
								<th style='text-align: center'>วันที่ปรับปรุง</th>
								<th style='text-align: center'>แก้ไข</th>
								<th style='text-align: center'>ลบ</th>
							</tr>
						</thead>
						<tbody>
							<?php if(count($this->item->mou)==0){ ?>
								<tr>
									<td style="text-align:center" colspan="8">ไม่พบข้อมูล</td>
								</tr>
							<?php }else{ ?>
								<?php $i=0;
									foreach($this->item->mou as $key=>$data){ ?>
									<tr>
										<td><?php echo ++$i; ?></td>
										<td><?php echo $data->year; ?></td>
										<td><?php echo $data->daily_amount; ?></td>
										<td><?php 
												$user = JFactory::getUser($data->update_by);
												echo $user->name; 
											?>
											
										</td>
										<td><?php echo formatDate($data->update_datetime); ?></td>
										<td style="text-align: center"><button type="button" class="btn btn-warning" data-toggle="modal" data-target="#MOUModal" onClick="javascript:getMOU(<?php echo $data->id; ?>);">แก้ไข</button></td>
										<td style="text-align: center"><button type="button" class="btn btn-danger" onClick="javascript: if(confirm('ยืนยันการลบ?')){jQuery(this).prop('disabled', true);deleteMOU(<?php echo $data->id; ?>);}">ลบ</button></td>
									</tr>
								<?php } ?>
							<?php } ?>
						</tbody>
					</table>
										
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>


<!-- Modal -->
<div id="GMPModal" class="modal fade" role="dialog" style="display:none">
<form id="GMPForm" action="" method="post">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">การรับรองมาตรฐาน GMP</h4>
      </div>
      <div class="modal-body">
        <table>
        	<tr>
        		<td width="120">หมายเลขการรับรอง*</td>
        		<td><input type="text" name="gmp_certify_no" /></td>
        	</tr>
        	<tr>
        		<td>วันที่ได้รับการรับรอง*</td>
        		<td><input type="text" name="gmp_certify_date" class="datetimepicker" placeholder="DD/MM/YYYY (พ.ศ.)" /></td>
        	</tr>
        	<tr>
        		<td>วันที่หมดอายุ*</td>
        		<td><input type="text" name="gmp_expire_date" class="datetimepicker" placeholder="DD/MM/YYYY (พ.ศ.)"/></td>
        	</tr>
        	<tr>
        		<td>หน่วยงานรับรอง*</td>
        		<td><input type="text" name="gmp_certify_agency" /></td>
        	</tr>
        </table>
        <input type="hidden" name="gmp_id" value="" />
        <input type="hidden" name="gmp_coop_id" value="<?php echo $this->item->id;?>" />
        <input type="hidden" name="option" value="<?php echo $_REQUEST['option'];?>" />
        <input type="hidden" name="task" value="coop.saveGMP" />
      </div>
      <div class="modal-footer" style="text-align:left">
      	<button type="button" class="btn btn-info" style="width:100px" onClick="javascript:addGMP();">บันทึก</button>&nbsp;&nbsp;
        <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
      </div>
    </div>

  </div>
  </form>
</div>





<!-- Modal -->
<div id="SMCModal" class="modal fade" role="dialog" style="display:none">
<form id="SMCForm" action="" method="post">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">โรงงาน/บริษัทที่ส่งนม</h4>
      </div>
      <div class="modal-body">
        <table>
        	<tr>
        		<td width="120">โรงงาน/บริษัทที่ส่งนม*</td>
        		<td><input type="text" name="smc_name" /></td>
        	</tr>
        	<tr>
        		<td>ที่อยู่</td>
        		<td><textarea name="smc_address"></textarea></td>
        	</tr>
        </table>
        <input type="hidden" name="smc_id" value="" />
        <input type="hidden" name="smc_coop_id" value="<?php echo $this->item->id;?>" />
        <input type="hidden" name="option" value="<?php echo $_REQUEST['option'];?>" />
        <input type="hidden" name="task" value="coop.saveSendMilkCompany" />
      </div>
      <div class="modal-footer" style="text-align:left">
      	<button type="button" class="btn btn-info" style="width:100px" onClick="javascript:addSendMilkCompany();">บันทึก</button>&nbsp;&nbsp;
        <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
      </div>
    </div>

  </div>
  </form>
</div>




<!-- Modal -->
<div id="MOUModal" class="modal fade" role="dialog" style="display:none">
<form id="MOUForm" action="" method="post">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">ปริมาณนมที่ส่ง อ.ส.ค. (ตาม MOU)</h4>
      </div>
      <div class="modal-body">
        <table>
        	<tr>
        		<td>ปี*</td>
        		<td><input type="text" name="mou_year" maxlength="4" placeholder="YYYY (พ.ศ.)" /></td>
        	</tr>
        	<tr>
        		<td>ปริมาณ (ตัน/วัน)*</td>
        		<td><input type="text" name="mou_daily_amount" /></td>
        	</tr>
        </table>
        <input type="hidden" name="mou_id" value="" />
        <input type="hidden" name="mou_coop_id" value="<?php echo $this->item->id;?>" />
        <input type="hidden" name="option" value="<?php echo $_REQUEST['option'];?>" />
        <input type="hidden" name="task" value="coop.saveMOU" />
      </div>
      <div class="modal-footer" style="text-align:left">
      	<button type="button" class="btn btn-info" style="width:100px" onClick="javascript:addMOU();">บันทึก</button>&nbsp;&nbsp;
        <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
      </div>
    </div>

  </div>
  </form>
</div>

<script type="text/javascript">
	function getGMP(id){
		jQuery.ajax({
            url: "index.php?option=com_edairy&task=coop.getGMP",
            type: "POST",
            data: {
                'gmp_id': id
            },
            success: function (data) {
            	var data = jQuery.parseJSON(data);
            	console.log(data);

            	if(data!=null){
	            	jQuery("input[name=gmp_certify_no]").val(data.certify_no);
	            	var certify_date = data.certify_date.split("-");

	            	jQuery("input[name=gmp_certify_date]").val(certify_date[2] + "/" + certify_date[1] + "/" + (parseInt(certify_date[0])+543));

	            	var expire_date = data.expire_date.split("-");
	            	jQuery("input[name=gmp_expire_date]").val(expire_date[2] + "/" + expire_date[1] + "/" + (parseInt(expire_date[0])+543));

	            	jQuery("input[name=gmp_certify_agency]").val(data.certify_agency);

	            	jQuery("input[name=gmp_id]").val(data.id);
	            }

            }

        });
	}

	function addGMP(){
		if(jQuery("input[name=gmp_certify_no]").val()=="" || jQuery("input[name=gmp_certify_date]").val()=="" || jQuery("input[name=gmp_expire_date]").val()=="" || jQuery("input[name=gmp_certify_agency]").val()==""){
			alert("กรุณาใส่ข้อมูลให้ครบถ้วน");
			return false;
		}else{
			document.getElementById('GMPForm').submit();
		}
		
	}
	function deleteGMP(id){
		jQuery.ajax({
            url: "index.php?option=com_edairy&task=coop.deleteGMP",
            type: "POST",
            data: {
                'gmp_id': id
            },
            success: function (data) {
            	var data = jQuery.parseJSON(data);
                if (data.delete_result==true) {
                	alert("ลบข้อมูลสำเร็จ");
                } else {
                    alert("ไม่สามารถลบข้อมูลได้");

                }
                window.location = "index.php?option=com_edairy&view=coop&layout=edit&id=<?php echo $_REQUEST['id'];?>&default_tab=gmp";
            }

        });
	}



	function getSendMilkCompany(id){
		jQuery.ajax({
            url: "index.php?option=com_edairy&task=coop.getSendMilkCompany",
            type: "POST",
            data: {
                'smc_id': id
            },
            success: function (data) {
            	var data = jQuery.parseJSON(data);
            	console.log(data);

            	if(data!=null){
	            	jQuery("input[name=smc_name]").val(data.name);

	            	jQuery("textarea[name=smc_address]").val(data.address);

	            	jQuery("input[name=smc_id]").val(data.id);
	            }

            }

        });
	}

	function addSendMilkCompany(){
		if(jQuery("input[name=smc_name]").val()==""){
			alert("กรุณาใส่ข้อมูลให้ครบถ้วน");
			return false;
		}else{
			document.getElementById('SMCForm').submit();
		}
		
	}
	function deleteSendMilkCompany(id){
		jQuery.ajax({
            url: "index.php?option=com_edairy&task=coop.deleteSendMilkCompany",
            type: "POST",
            data: {
                'smc_id': id
            },
            success: function (data) {
            	var data = jQuery.parseJSON(data);
                if (data.delete_result==true) {
                	alert("ลบข้อมูลสำเร็จ");
                } else {
                    alert("ไม่สามารถลบข้อมูลได้");

                }
                window.location = "index.php?option=com_edairy&view=coop&layout=edit&id=<?php echo $_REQUEST['id'];?>&default_tab=send_milk_company";
            }

        });
	}



	function getMOU(id){
		jQuery.ajax({
            url: "index.php?option=com_edairy&task=coop.getMOU",
            type: "POST",
            data: {
                'mou_id': id
            },
            success: function (data) {
            	var data = jQuery.parseJSON(data);
            	console.log(data);

            	if(data!=null){
	            	jQuery("input[name=mou_year]").val(data.year);
	            	jQuery("input[name=mou_daily_amount]").val(data.daily_amount);

	            	jQuery("input[name=mou_id]").val(data.id);
	            }

            }

        });
	}

	function addMOU(){
		if(jQuery("input[name=mou_year]").val()=="" || jQuery("input[name=mou_daily_amount]").val()==""){
			alert("กรุณาใส่ข้อมูลให้ครบถ้วน");
			return false;
		}else{
			document.getElementById('MOUForm').submit();
		}
		
	}
	function deleteMOU(id){
		jQuery.ajax({
            url: "index.php?option=com_edairy&task=coop.deleteMOU",
            type: "POST",
            data: {
                'mou_id': id
            },
            success: function (data) {
            	var data = jQuery.parseJSON(data);
                if (data.delete_result==true) {
                	alert("ลบข้อมูลสำเร็จ");
                } else {
                    alert("ไม่สามารถลบข้อมูลได้");

                }
                window.location = "index.php?option=com_edairy&view=coop&layout=edit&id=<?php echo $_REQUEST['id'];?>&default_tab=mou";
            }

        });
	}


	function clearGMPForm(){
		jQuery("#GMPForm input[type!=hidden]").val("");
	}

	function clearMOUForm(){
		jQuery("#MOUForm input[type!=hidden]").val("");
	}

	function clearSMCForm(){
		jQuery("#SMCForm input[type!=hidden]").val("");
		jQuery("#SMCForm textarea").val("");
	}
</script>


    <script>
      // This example adds a search box to a map, using the Google Place Autocomplete
      // feature. People can enter geographical searches. The search box will return a
      // pick list containing a mix of places and predicted search terms.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      <?php
      	$lat = 13.765001;
      	$lng = 100.53816;

      	if($this->item->latitude!=0){
      		$lat = $this->item->latitude;
      	}
      	if($this->item->longitude!=0){
      		$lng = $this->item->longitude;
      	}
      ?>
      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: <?php echo $lat; ?>, lng: <?php echo $lng; ?>},
          zoom: 13,
          mapTypeId: 'roadmap'
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];

              var myLatlng = {lat: <?php echo $lat; ?>, lng: <?php echo $lng; ?>};

        var marker = new google.maps.Marker({
          position: myLatlng,
          map: map,
          draggable: true,
   		  animation: google.maps.Animation.DROP
        });

        google.maps.event.addListener(marker, 'dragend', function (event) {
		    document.getElementById("jform_latitude").value = this.getPosition().lat();
		    document.getElementById("jform_longitude").value = this.getPosition().lng();
		});


        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }



          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

    
          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            var marker = new google.maps.Marker({
              map: map,
              title: place.name,
              position: place.geometry.location,
              draggable: true,
   		  		animation: google.maps.Animation.DROP
            });
            markers.push(marker);


            google.maps.event.addListener(marker, 'dragend', function (event) {
			    document.getElementById("jform_latitude").value = this.getPosition().lat();
			    document.getElementById("jform_longitude").value = this.getPosition().lng();
			});

            console.log(place.geometry.location);
            js("#jform_latitude").val(place.geometry.location.lat);
            js("#jform_longitude").val(place.geometry.location.lng);

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
      }

    </script>


    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD9kSzIrHDG2nYTH3S-ni9oBTEGN4JM7wk&libraries=places&callback=initMap">
    </script>