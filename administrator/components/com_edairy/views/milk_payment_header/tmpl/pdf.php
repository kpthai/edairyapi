<?php 

// Include the main TCPDF library (search for installation path).
require_once('../media/com_edairy/tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('EDAIRY');
$pdf->SetTitle('EDAIRY');
$pdf->SetSubject('EDAIRY');
$pdf->SetKeywords('EDAIRY');

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
// set font

//$fontname = $pdf->addTTFfont('fonts/Browa.ttf', 'TrueTypeUnicode', '', 32);
$pdf->SetFont('thsarabunpsk', '', 14, '', true);



//PAGE 3 >> PAGE 1
$pdf->AddPage();
$pdf->setPageOrientation ('P', $autopagebreak='', $bottommargin='');
// get the current page break margin
$bMargin = $pdf->getBreakMargin();
// get current auto-page-break mode
$auto_page_break = $pdf->getAutoPageBreak();
// disable auto-page-break
$pdf->SetAutoPageBreak(true, 40);


function num2thai($number){
	$t1 = array("ศูนย์", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");
	$t2 = array("เอ็ด", "ยี่", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน");
	$zerobahtshow = 0; // ในกรณีที่มีแต่จำนวนสตางค์ เช่น 0.25 หรือ .75 จะให้แสดงคำว่า ศูนย์บาท หรือไม่ 0 = ไม่แสดง, 1 = แสดง
	(string) $number;
	$number = explode(".", $number);
	if(!empty($number[1])){
		if(strlen($number[1]) == 1){
			$number[1] .= "0";
		}else if(strlen($number[1]) > 2){
			if($number[1]{2} < 5){
				$number[1] = substr($number[1], 0, 2);
			}else{
				$number[1] = $number[1]{0}.($number[1]{1}+1);
			}
		}
	}
	
	for($i=0; $i<count($number); $i++){
		$countnum[$i] = strlen($number[$i]);
		if($countnum[$i] <= 7){
			$var[$i][] = $number[$i];
		}else{
			$loopround = ceil($countnum[$i]/6);
			for($j=1; $j<=$loopround; $j++){
				if($j == 1){
					$slen = 0;
					$elen = $countnum[$i]-(($loopround-1)*6);
				}else{
					$slen = $countnum[$i]-((($loopround+1)-$j)*6);
					$elen = 6;
				}
				$var[$i][] = substr($number[$i], $slen, $elen);
			}
		}	

		$nstring[$i] = "";
		for($k=0; $k<count($var[$i]); $k++){
			if($k > 0) $nstring[$i] .= $t2[7];
			$val = $var[$i][$k];
			$tnstring = "";
			$countval = strlen($val);
			for($l=7; $l>=2; $l--){
				if($countval >= $l){
					$v = substr($val, -$l, 1);
					if($v > 0){
						if($l == 2 && $v == 1){
							$tnstring .= $t2[($l)];
						}elseif($l == 2 && $v == 2){
							$tnstring .= $t2[1].$t2[($l)];
						}else{
							$tnstring .= $t1[$v].$t2[($l)];
						}
					}
				}
			}
			if($countval >= 1){
				$v = substr($val, -1, 1);
				if($v > 0){
					if($v == 1 && $countval > 1 && substr($val, -2, 1) > 0){
						$tnstring .= $t2[0];
					}else{
						$tnstring .= $t1[$v];
					}

				}
			}

			$nstring[$i] .= $tnstring;
		}

	}
	$rstring = "";
	if(!empty($nstring[0]) || $zerobahtshow == 1 || empty($nstring[1])){
		if($nstring[0] == "") $nstring[0] = $t1[0];
		$rstring .= $nstring[0]."บาท";
	}
	if(count($number) == 1 || empty($nstring[1])){
		$rstring .= "ถ้วน";
	}else{
		$rstring .= $nstring[1]."สตางค์";
	}
	return $rstring;
}
// set bacground image
/*
$heading_html = <<<EOD
<img src="images/header.png" />

EOD;

$pdf->writeHTMLCell($w=0, $h=0, $x='10', $y='5', $heading_html, $border=0, $ln=1, $fill=0, $reseth=true, $align='L', $autopadding=false);

*/

$create_date = formatDate(date("Y-m-d"));


$html = <<<EOD
เลขที่ : {$this->item->run_no} <br />
วันที่ : {$create_date}

EOD;


//$money_word = num2thai($this->item->grand_total);

// print a block of text using Write()
$pdf->writeHTMLCell($w=0, $h=0, $x='10', $y='10', $html, $border=0, $ln=1, $fill=0, $reseth=true, $align='R', $autopadding=false);



// Line Processing
$insemination_time_map = array(1=>"เช้า", 2=>"เย็น");
$line_html = "";

for($i=0;$i<count($this->item->insemination_line);$i++){
	$index=$i+1;
$line_html .= <<<EOD
<tr nobr="true">
		<td>{$index}</td>
		<td>{$this->item->insemination_line[$i]->cow->cow_id}</td>
		<td>{$this->item->insemination_line[$i]->cow->name}</td>
		<td></td>
		<td style="text-align:center">{$insemination_time_map[$this->item->insemination_line[$i]->insemination_time]}</td>
		<td>{$this->item->insemination_line[$i]->lot_no}</td>
		<td style="text-align:right">{$this->item->insemination_line[$i]->price}</td>
		<td style="text-align:center">{$this->item->insemination_line[$i]->amount}</td>
		<td style="text-align:right">{$this->item->insemination_line[$i]->total_price}</td>
	</tr>
EOD;
}

$coop_id = $_REQUEST["coop_id"];
				        $period = $_REQUEST["period"];
				        $from_date = $_REQUEST["from_date"];
				        $to_date = $_REQUEST["to_date"];

						foreach($this->masterData->mas_coop as $key=>$data){
							if($data->id == $_REQUEST["coop_id"]){
								$coop_display_name = $data->coop_abbr . " " . $data->name;
								break; 
							}
						}

$outcome_display = number_format($_REQUEST['outcome'], 2);
$tax = number_format($_REQUEST['outcome']*0.03,2);
$outcome_with_tax = number_format($_REQUEST['outcome']*1.03,2);
$grand_total = number_format($_REQUEST['income']-$_REQUEST['outcome']*1.03,2);
$_REQUEST['income'] = number_format($_REQUEST['income'], 2);
$html = <<<EOD
<div style="text-align:center;font-size:20px">
			<img src="../images/dpo_logo.jpg" width="70"/><br />
			<b>
				องค์การส่งเสริมกิจการโคนมแห่งประเทศไทย (อ.ส.ค.) กระทรวงเกษตรและสหกรณ์
			<br />
				สำนักงาน อ.ส.ค. ภาคกลาง
			<br />
				ใบสรุปรายได้-ค่าใช้จ่าย เกษตรกรสมาชิกผู้เลี้ยงโคนม
			</b>

</div>
<br />
ศูนย์รับน้ำนมดิบ {$coop_display_name} ประจำงวดที่ {$_REQUEST[period]} ระหว่างวันที่ {$_REQUEST[from_date]} ถึง {$_REQUEST[to_date]}
<br />
หมายเลขสมาชิก 101001 &nbsp;&nbsp; ชื่อ-สกุล เกษตรกร มานพ รุ่งโรจน์

<br />

<table border="1" width="670" cellpadding="2"  nobr="true">
					<tr nobr="true">
						<th style="text-align:center" width="100"><b>งวดที่</b></th>
						<th style="text-align:center" width="240"><b>ปริมาณน้ำนมดิบ (กก.)</b></th>
						<th style="text-align:center" width="160"><b>ราคา/กก. (บาท)</b></th>
						<th style="text-align:center" width="160"><b>รายได้ (บาท)</b></th>
					</tr>
					<tr>
						<td align="center">{$_REQUEST[period]}</td>
						<td align="right">{$_REQUEST[milk_amount]}</td>
						<td align="right">{$_REQUEST[milk_price]}</td>
						<td align="right">{$_REQUEST[income]}</td>
					</tr>
					<tr nobr="true">
						<td colspan="3" align="right">รวมรายได้ (บาท)</td>
						<td align="right">{$_REQUEST[income]}</td>
					</tr>
					
				</table>
<br /><br />

<table border="1" width="670" cellpadding="2"  nobr="true">
					<tr nobr="true">
						<th style="text-align:center" width="100"><b>ลำดับ</b></th>
						<th style="text-align:center" width="400"><b>รายการ</b></th>
						<th style="text-align:center" width="160"><b>รายจ่าย (บาท)</b></th>
					</tr>
					<tr>
						<td align="center">1</td>
						<td>ค่าบริการสัตวแพทย์และผสมเทียม</td>
						<td align="right">{$outcome_display}</td>
					</tr>
					<tr>
						<td align="center">2</td>
						<td>ภาษี</td>
						<td align="right">{$tax}</td>
					</tr>
					<tr nobr="true">
						<td colspan="2" align="right">รวมรายจ่าย (บาท)</td>
						<td align="right">{$outcome_with_tax}</td>
					</tr>
					<tr nobr="true">
						<td colspan="2" align="right">รวมรายได้สุทธิ (บาท)</td>
						<td align="right">{$grand_total}</td>
					</tr>
					
				</table>
<br /><br />
<b>ผลคุณภาพน้ำนม</b>
<br />
<table cellpadding="2" border="0">
	<tr>
		<td width="80">MB</td>
		<td align="right" width="70">1</td>
		<td width="50">Hrs.</td>
		<td>ราคาเพิ่ม/ลด</td>
		<td>0.50 บาท</td>
	</tr>
	<tr>
		<td>SCC</td>
		<td align="right">229000</td>
		<td>Cell/ml</td>
		<td>ราคาเพิ่ม/ลด</td>
		<td>0.30 บาท</td>
	</tr>
	<tr>
		<td>FAT</td>
		<td align="right">3.93</td>
		<td>%</td>
		<td>ราคาเพิ่ม/ลด</td>
		<td>0.30 บาท</td>
	</tr>
	<tr>
		<td>SNF</td>
		<td align="right">08.71</td>
		<td>%</td>
		<td>ราคาเพิ่ม/ลด</td>
		<td>0.60 บาท</td>
	</tr>
	<tr>
		<td>คะแนนคอก</td>
		<td align="right">0</td>
		<td></td>
		<td>ราคาเพิ่ม/ลด</td>
		<td>0.00 บาท</td>
	</tr>
	<tr>
		<td>มาตรฐาน GAP</td>
		<td align="right">มี</td>
		<td></td>
		<td>ราคาเพิ่ม/ลด</td>
		<td>0.20 บาท</td>
	</tr>
</table>
EOD;




// print a block of text using Write()
$pdf->writeHTMLCell($w=0, $h=0, $x='10', $y='10', $html, $border=0, $ln=1, $fill=0, $reseth=true, $align='L', $autopadding=false);

 






// set bacground image

// if (isset($_POST['download'])) {
// 	$pdf->Output("detail_id{$_POST[id]}.pdf", 'D');
// }else{
	$pdf->Output("Invoice_{$_GET[id]}.pdf", 'I');
// }
die();
