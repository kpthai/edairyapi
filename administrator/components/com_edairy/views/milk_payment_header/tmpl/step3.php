<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_edairy/css/form.css');
?>

<script src="../media/com_edairy/typeahead/bootstrap3-typeahead.js" type="text/javascript" charset="UTF-8"></script>

<script src="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript" charset="UTF-8"></script>

<link rel="stylesheet" href="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.css">


<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {

		js('.datetimepicker').datetimepicker({
            lang:'th',
            timepicker:false,
            format:'d/m/Y'
        });
		
	js('input:hidden.coop_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('coop_idhidden')){
			js('#jform_coop_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_coop_id").trigger("liszt:updated");
	});

	Joomla.submitbutton = function (task) {
		if (task == 'milk_payment_header.cancel') {
			Joomla.submitform(task, document.getElementById('milk_payment_header-form'));
		}
		else {
			
			if (task != 'milk_payment_header.cancel' && document.formvalidator.isValid(document.id('milk_payment_header-form'))) {
				
				Joomla.submitform(task, document.getElementById('milk_payment_header-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

</style>
<form
	action="<?php echo JRoute::_('index.php?option=com_edairy&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="milk_payment_header-form" class="form-validate">




	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_EDAIRY_TITLE_MILK_PAYMENT_HEADER', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

					<b>ขั้นตอนการทำงาน</b>
					<table width="100%" border="1" cellpadding="5">
						<tr>
							<td width="20%" style="background:green;color:white">1. ระบุรายละเอียดงวด</td>
							<td width="20%" style="background:green;color:white">2. รวมปริมาณน้ำนมดิบ</td>
							<td width="20%" style="background:yellow">3. คิดราคาน้ำนม</td>
							<td width="20%">4. รวมค่าใช้จ่าย</td>
							<td width="20%">5. สรุปรายได้-ค่าใช้จ่าย</td>
						</tr>
					</table>
					<hr />
					
					ศูนย์รับน้ำนมดิบ <?php 

						foreach($this->masterData->mas_coop as $key=>$data){
							if($data->id == $_REQUEST["coop_id"]){
								echo $data->coop_abbr . " " . $data->name;
								break; 
							}
						}
					?> ประจำงวดที่ <?php echo $_REQUEST["period"]; ?> ระหว่างันที่ <?php echo $_REQUEST["from_date"]; ?> ถึง <?php echo $_REQUEST["to_date"]; ?> <br /><br />

					<table width="100%" border="1" cellpadding="5">
						<tr>
							<th>หมายเลขสมาชิก</th>
							<th>ชื่อ-สกุล เกษตรกร</th>
							<th>สรุปราคา (บาท/กก.)</th>
							<th>อ้างอิงเลขที่ใบนำส่งตัวอย่างน้ำนมดิบ</th>
							<th>เรียกดูผลคุณภาพน้ำนม</th>
						</tr>
						<?php foreach($this->stepData as $key=>$data){ 
								if($data->milk_quality_transaction->total_price==""){
									$data->milk_quality_transaction->total_price = "<font color='red'>ไม่พบข้อมูล</font>";
								}
								if($data->milk_quality_transaction->run_no==""){
									$data->milk_quality_transaction->run_no = "<font color='red'>ไม่พบข้อมูล</font>";
								}
							?>
							<tr>
								<td><?php echo $data->farm->member_code; ?></td>
								<td><?php echo $data->farmer->name; ?> <?php echo $data->farmer->surname; ?></td>
								<td align="right"><?php echo $data->milk_quality_transaction->total_price; ?></td>
								<td><?php echo $data->milk_quality_transaction->run_no; ?></td>
								<td style="text-align: center">
									<?php if($data->milk_quality_transaction->run_no=="<font color='red'>ไม่พบข้อมูล</font>"){ ?>
										<font color='red'>ไม่พบข้อมูล</font>
									<?php }else{ ?>
										<input type="button" name="" class="btn btn-primary" value="เรียกดู" style="width:100px"  data-toggle="modal" data-target="#QualityModal" onClick="javascript:getMilkQuailityTransaction(<?php echo $data->milk_quality_transaction->id; ?>);" />
									<?php } ?>
								</td>
							</tr>
						<?php } ?>

						<?php if(count($this->stepData)==0){ ?>
							<tr>
									<td colspan="5" align="center">ไม่พบรายการ</td>
							</tr>
						<?php } ?>
					</table>

					<br /><br />
			
				<?php /*
				<?php echo $this->form->renderField('from_date'); ?>
				<?php echo $this->form->renderField('to_date'); ?>
				*/ ?>


				


				

				<div style="display:none">

					<?php echo $this->form->renderField('total_send_milk_amount'); ?>
					<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />

					<div style="float:right">
							<button class="btn btn-info" data-toggle="modal" data-target="#QualityModal"  onClick="javascript:clearQualityForm();">เพิ่มรายการ</button>
					</div>
					<br /><br />
					<div style="clear:both"></div>

					<div style="display: none">
						<?php echo $this->form->renderField('state'); ?>
					</div>
					<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
					<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />

				</div>
				<?php echo $this->form->renderField('created_by'); ?>

				<?php echo $this->form->renderField('modified_by'); ?>

					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>

					<hr />

					<input type="button" onClick="javascript:history.go(-1);" class="btn btn-default" value="< ย้อนกลับ" style="width:150px" /> &nbsp;&nbsp;

					<?php if(count($this->stepData)!=0){ ?>
						<input type="submit" class="btn btn-primary" value="ถัดไป >" style="width:150px" /> 	&nbsp;&nbsp;
					<?php } ?>

					<input type="hidden" name="coop_id" value="<?php echo $_REQUEST[coop_id]; ?>" />
					<input type="hidden" name="period" value="<?php echo $_REQUEST[period]; ?>" />
					<input type="hidden" name="from_date" value="<?php echo $_REQUEST[from_date]; ?>" />
					<input type="hidden" name="to_date" value="<?php echo $_REQUEST[to_date]; ?>" />



					<input type="button" class="btn btn-danger" value="ยกเลิก" style="width:100px" onClick="javascript:window.location='index.php?option=com_edairy&view=milk_payment_headers';" /> 
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value="milk_payment_header.step3"/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>


<!-- Modal -->
<div id="QualityModal" class="modal fade" role="dialog" style="display:none">
<form id="QualityForm" action="" method="post">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">คุณภาพน้ำนมเกษตรกร</h4>
      </div>
      <div class="modal-body">
        <table>
        	<tr>
        		<td width="120">เกษตรกร</td>
        		<td>
        			<select name="farmer_id" disabled="disabled">
					 	<option value="">-- เลือกเกษตรกร --</option>
					 	<?php 
							foreach($this->masterData->mas_farmer as $bkey=>$bdata){ 
						?>
								<option value="<?php echo $bdata->id;?>"><?php echo $bdata->citizen_id; ?> : <?php echo $bdata->name; ?> <?php echo $bdata->surname; ?></option>
						<?php } ?>
        			</select>
        		</td>
        	</tr>
        </table>
        <hr />
        <b>ผลการตรวจวิเคราะห์</b>
        <table cellpadding="3">
        	<tr>
        		<td width="160">MB (Hrs.)</td>
        		<td><input type="text" name="mb" /></td>
        		<td width="160">ราคา เพิ่ม/ลด (บาท)</td>
        		<td><input type="text" disabled /></td>
        	</tr>
        	<tr>
        		<td>SCC (Cell/ml.)</td>
        		<td><input type="text" name="scc" /></td>
        		<td>ราคา เพิ่ม/ลด (บาท)</td>
        		<td><input type="text" disabled /></td>
        	</tr>
        	<tr>
        		<td>FAT (%)</td>
        		<td><input type="text" name="fat" /></td>
        		<td>ราคา เพิ่ม/ลด (บาท)</td>
        		<td><input type="text" disabled /></td>
        	</tr>
        	<tr>
        		<td>SNF (%)</td>
        		<td><input type="text" name="snf" /></td>
        		<td>ราคา เพิ่ม/ลด (บาท)</td>
        		<td><input type="text" disabled /></td>
        	</tr>
        	<tr>
        		<td>TS (%)</td>
        		<td><input type="text" name="ts" /></td>
        		<td>ราคา เพิ่ม/ลด (บาท)</td>
        		<td><input type="text" disabled /></td>
        	</tr>
        	<tr>
        		<td>Protein (%)</td>
        		<td><input type="text" name="protein" /></td>
        		<td>ราคา เพิ่ม/ลด (บาท)</td>
        		<td><input type="text" disabled /></td>
        	</tr>
        	<tr>
        		<td>Lactose (%)</td>
        		<td><input type="text" name="lactose" /></td>
        		<td>ราคา เพิ่ม/ลด (บาท)</td>
        		<td><input type="text" disabled /></td>
        	</tr>
        </table>

        <b>ค่ามาตรฐานฟาร์ม</b>
        <table cellpadding="3">
        	<tr>
        		<td width="160">คะแนนคอก</td>
        		<td><input type="text" disabled /></td>
        		<td width="160">ราคา เพิ่ม/ลด (บาท)</td>
        		<td><input type="text" disabled /></td>
        	</tr>
        	<tr>
        		<td>มาตรฐาน GAP</td>
        		<td><input type="text" disabled /></td>
        		<td>ราคา เพิ่ม/ลด (บาท)</td>
        		<td><input type="text" disabled /></td>
        	</tr>
        	<tr>
        		<td>ราคามาตรฐาน (บาท/กก.)</td>
        		<td><input type="text" name="price" disabled /></td>
        		<td></td>
        		<td></td>
        	</tr>
        	<tr>
        		<td>สรุปราคา (บาท/กก.)</td>
        		<td><input name="total_price" type="text" disabled/></td>
        		<td></td>
        		<td></td>
        	</tr>
        </table>

        <input type="hidden" name="milk_quality_id" value="<?php echo $_REQUEST['id']; ?>" />
        <input type="hidden" name="milk_quality_transaction_id" value="" />
        <input type="hidden" name="option" value="<?php echo $_REQUEST['option'];?>" />
        <input type="hidden" name="task" value="milk_quality_header.saveMilkQualityTransaction" />
      </div>
      <div class="modal-footer" style="text-align:left">
        <button type="button" class="btn btn-info" data-dismiss="modal">ตกลง</button>
      </div>
    </div>

  </div>
  </form>
</div>


<script type="text/javascript">
	function clearQualityForm(){
		jQuery("#QualityForm input[type!=hidden]").val("");
	}

	function getMilkQuailityTransaction(id){
		jQuery.ajax({
            url: "index.php?option=com_edairy&task=milk_quality_header.getMilkQualityTransaction",
            type: "POST",
            data: {
                'milk_quality_transaction_id': id
            },
            success: function (data) {
            	var data = jQuery.parseJSON(data);
            	console.log(data);

            	if(data!=null){

	            	jQuery("select[name=farmer_id]").val(data.farmer_id);

	            	jQuery("select[name=farmer_id]").trigger('liszt:updated');

	            	jQuery("input[name=mb]").val(data.mb);

	            	jQuery("input[name=scc]").val(data.scc);
	            	
	            	jQuery("input[name=fat]").val(data.fat);

	            	jQuery("input[name=snf]").val(data.snf);

	            	jQuery("input[name=ts]").val(data.ts);

	            	jQuery("input[name=protein]").val(data.protein);

	            	jQuery("input[name=lactose]").val(data.lactose);

	            	jQuery("input[name=milk_quality_transaction_id]").val(data.id);

	            	jQuery("input[name=price]").val(data.price);

	            	jQuery("input[name=total_price]").val(data.total_price);
	            }

            }

        });
	}
</script>
