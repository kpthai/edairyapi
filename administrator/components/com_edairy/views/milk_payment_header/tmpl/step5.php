<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_edairy/css/form.css');
?>

<script src="../media/com_edairy/typeahead/bootstrap3-typeahead.js" type="text/javascript" charset="UTF-8"></script>

<script src="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript" charset="UTF-8"></script>

<link rel="stylesheet" href="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.css">


<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {

		js('.datetimepicker').datetimepicker({
            lang:'th',
            timepicker:false,
            format:'d/m/Y'
        });
		
	js('input:hidden.coop_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('coop_idhidden')){
			js('#jform_coop_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_coop_id").trigger("liszt:updated");
	});

	Joomla.submitbutton = function (task) {
		if (task == 'milk_payment_header.cancel') {
			Joomla.submitform(task, document.getElementById('milk_payment_header-form'));
		}
		else {
			
			if (task != 'milk_payment_header.cancel' && document.formvalidator.isValid(document.id('milk_payment_header-form'))) {
				
				Joomla.submitform(task, document.getElementById('milk_payment_header-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

</style>
<form
	action="<?php echo JRoute::_('index.php?option=com_edairy&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="milk_payment_header-form" class="form-validate">




	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_EDAIRY_TITLE_MILK_PAYMENT_HEADER', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

					<b>ขั้นตอนการทำงาน</b>
					<table width="100%" border="1" cellpadding="5">
						<tr>
							<td width="20%" style="background:green;color:white">1. ระบุรายละเอียดงวด</td>
							<td width="20%" style="background:green;color:white">2. รวมปริมาณน้ำนมดิบ</td>
							<td width="20%" style="background:green;color:white">3. คิดราคาน้ำนม</td>
							<td width="20%" style="background:green;color:white">4. รวมค่าใช้จ่าย</td>
							<td width="20%" style="background:yellow">5. สรุปรายได้-ค่าใช้จ่าย</td>
						</tr>
					</table>
					<hr />
					
					ศูนย์รับน้ำนมดิบ <?php 

						$coop_id = $_REQUEST["coop_id"];
				        $period = $_REQUEST["period"];
				        $from_date = $_REQUEST["from_date"];
				        $to_date = $_REQUEST["to_date"];

						foreach($this->masterData->mas_coop as $key=>$data){
							if($data->id == $_REQUEST["coop_id"]){
								echo $data->coop_abbr . " " . $data->name;
								break; 
							}
						}
					?> ประจำงวดที่ <?php echo $_REQUEST["period"]; ?> ระหว่างันที่ <?php echo $_REQUEST["from_date"]; ?> ถึง <?php echo $_REQUEST["to_date"]; ?> <br /><br />

					<table width="100%" border="1" cellpadding="5">
						<tr>
							<th rowspan="2">หมายเลขสมาชิก</th>
							<th rowspan="2">ชื่อ-สกุล เกษตรกร</th>
							<th colspan="3">รายได้</th>
							<th colspan="3">ค่าใช้จ่าย</th>
							<th rowspan="2">รายได้สุทธิ (บาท)</th>
							<th rowspan="2">พิมพ์ใบสรุปฯ</th>
						</tr>
						<tr>
							<th>ปริมาณน้ำนมดิบ (กก.)</th>
							<th>ราคา/กก. (บาท)</th>
							<th>รวมรายได้ (บาท)</th>
							<th>ค่าบริการสัตวแพทย์และผสมเทียม (บาท)</th>
							<th>ภาษี (บาท)</th>
							<th>รวมรายจ่าย (บาท)</th>
						</tr>
						<?php foreach($this->stepData as $key=>$data){ 
								$milk_price = $data->milk_quality_transaction->total_price;
								if($data->milk_quality_transaction->total_price==""){
									$data->milk_quality_transaction->total_price = "<font color='red'>ไม่พบข้อมูล</font>";
								}
								if($data->insemination_line->count==""){
									$data->insemination_line->count=0;
								}

								if($data->health_line->count==""){
									$data->health_line->count = 0;
								}

								if($data->insemination_line->total_price_sum == ""){
									$data->insemination_line->total_price_sum = 0;
								}

								if($data->health_line->total_price_sum == ""){
									$data->health_line->total_price_sum = 0;
								}
								$line_grand_total_original = $data->insemination_line->total_price_sum+$data->health_line->total_price_sum;
								$line_grand_total = number_format($data->insemination_line->total_price_sum+$data->health_line->total_price_sum,2);

								$income_original = $data->total_amount_sum*$milk_price;
								$income = number_format($data->total_amount_sum*$milk_price, 2);
							?>
							<tr>
								<td><?php echo $data->farm->member_code; ?></td>
								<td><?php echo $data->farmer->name; ?> <?php echo $data->farmer->surname; ?></td>
								<td align="right"><?php echo $data->total_amount_sum; ?></td>
								<td align="right"><?php echo $data->milk_quality_transaction->total_price; ?></td>
								<td align="right"><?php echo $income; ?></td>
								<td align="right"><?php echo $line_grand_total; ?></td>
								<td align="right"><?php echo number_format($line_grand_total*0.03, 2); ?></td>
								<td align="right"><?php echo number_format($line_grand_total*1.03, 2); ?></td>
								<td align="right"><?php echo number_format($data->total_amount_sum*$milk_price-$line_grand_total*1.03, 2); ?></td>
								<td style="text-align: center">

									<input type="button" name="" class="btn btn-primary" value="พิมพ์" style="width:100px" onClick="<?php echo "javascript:window.open('index.php?option=com_edairy&view=milk_payment_header&layout=pdf&coop_id={$coop_id}&period={$period}&from_date={$from_date}&to_date={$to_date}&milk_amount={$data->total_amount_sum}&milk_price={$milk_price}&income={$income_original}&outcome={$line_grand_total_original}&milk_quality_transaction_id={$data->milk_quality_transaction->id}'"?>);" />
								</td>
							</tr>
							<?php } ?>
					</table>

					<br /><br />
			
				<?php /*
				<?php echo $this->form->renderField('from_date'); ?>
				<?php echo $this->form->renderField('to_date'); ?>
				*/ ?>


				


				

				<div style="display:none">

					<?php echo $this->form->renderField('total_send_milk_amount'); ?>
					<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />

					<div style="float:right">
							<button class="btn btn-info" data-toggle="modal" data-target="#QualityModal"  onClick="javascript:clearQualityForm();">เพิ่มรายการ</button>
					</div>
					<br /><br />
					<div style="clear:both"></div>

					<div style="display: none">
						<?php echo $this->form->renderField('state'); ?>
					</div>
					<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
					<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />

				</div>
				<?php echo $this->form->renderField('created_by'); ?>

				<?php echo $this->form->renderField('modified_by'); ?>

					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>

					<hr />
					<input type="hidden" name="coop_id" value="<?php echo $_REQUEST[coop_id]; ?>" />
					<input type="hidden" name="period" value="<?php echo $_REQUEST[period]; ?>" />
					<input type="hidden" name="from_date" value="<?php echo $_REQUEST[from_date]; ?>" />
					<input type="hidden" name="to_date" value="<?php echo $_REQUEST[to_date]; ?>" />
					<input type="hidden" name="total_send_milk_amount" value="<?php echo count($this->stepData); ?>" />

					<input type="button" onClick="javascript:history.go(-1);" class="btn btn-default" value="< ย้อนกลับ" style="width:150px" /> &nbsp;&nbsp;

					<input type="submit" class="btn btn-primary" value="บันทึกและปิด" style="width:150px" /> &nbsp;&nbsp;

					<input type="button" class="btn btn-primary" value="พิมพ์ใบสรุปฯ ทั้งหมด" style="width:180px" /> &nbsp;&nbsp;
					<input type="button" class="btn btn-danger" value="ยกเลิก" style="width:100px" onClick="javascript:window.location='index.php?option=com_edairy&view=milk_payment_headers';" /> 
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value="milk_payment_header.step5"/>
		
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>

