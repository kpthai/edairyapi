<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_edairy/css/form.css');
?>

<script src="../media/com_edairy/typeahead/bootstrap3-typeahead.js" type="text/javascript" charset="UTF-8"></script>

<script src="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript" charset="UTF-8"></script>

<link rel="stylesheet" href="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.css">


<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {

		js('.datetimepicker').datetimepicker({
            lang:'th',
            timepicker:false,
            format:'d/m/Y'
        });
		
	js('input:hidden.coop_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('coop_idhidden')){
			js('#jform_coop_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_coop_id").trigger("liszt:updated");
	});

	Joomla.submitbutton = function (task) {
		if (task == 'milk_payment_header.cancel') {
			Joomla.submitform(task, document.getElementById('milk_payment_header-form'));
		}
		else {
			
			if (task != 'milk_payment_header.cancel' && document.formvalidator.isValid(document.id('milk_payment_header-form'))) {
				
				Joomla.submitform(task, document.getElementById('milk_payment_header-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

</style>
<form
	action="<?php echo JRoute::_('index.php?option=com_edairy&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="milk_payment_header-form" class="form-validate">




	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_EDAIRY_TITLE_MILK_PAYMENT_HEADER', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

					<b>ขั้นตอนการทำงาน</b>
					<table width="100%" border="1" cellpadding="5">
						<tr>
							<td width="20%" style="background:yellow">1. ระบุรายละเอียดงวด</td>
							<td width="20%">2. รวมปริมาณน้ำนมดิบ</td>
							<td width="20%">3. คิดราคาน้ำนม</td>
							<td width="20%">4. รวมค่าใช้จ่าย</td>
							<td width="20%">5. สรุปรายได้-ค่าใช้จ่าย</td>
						</tr>
					</table>
					<hr />
					<table>
						<tr>
							<td width="450">
								<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
				<?php echo $this->form->renderField('coop_id'); ?>
							</td>
							<td>
								<?php
				foreach((array)$this->item->coop_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="coop_id" name="jform[coop_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('period'); ?>

							</td>
						</tr>
					</table>

				<table>
						<tr>
							<td width="450">
								<div class="control-group">
									<div class="control-label">
										<label>
											ระหว่างวันที่
										</label>
									</div>
									<div class="controls">
										<input type="text" name="jform[from_date]" class="datetimepicker"  value="<?php echo formatDate($this->item->from_date); ?>"/>
									</div>
								</div>
							</td>
							<td>
								<div class="control-group">
									<div class="control-label">
										<label>
											ถึงวันที่
										</label>
									</div>
									<div class="controls">
										<input type="text" name="jform[to_date]" class="datetimepicker"  value="<?php echo formatDate($this->item->to_date); ?>"/>
									</div>
								</div>
							</td>
						</tr>
				</table>
									

			
				<?php /*
				<?php echo $this->form->renderField('from_date'); ?>
				<?php echo $this->form->renderField('to_date'); ?>
				*/ ?>


				


				

				<div style="display:none">

					<?php echo $this->form->renderField('total_send_milk_amount'); ?>
					<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />

					<div style="float:right">
							<button class="btn btn-info" data-toggle="modal" data-target="#QualityModal"  onClick="javascript:clearQualityForm();">เพิ่มรายการ</button>
					</div>
					<br /><br />
					<div style="clear:both"></div>

					<div style="display: none">
						<?php echo $this->form->renderField('state'); ?>
					</div>
					<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
					<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />

				</div>
				<?php echo $this->form->renderField('created_by'); ?>

				<?php echo $this->form->renderField('modified_by'); ?>

					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>

					<hr />

					<input type="submit" class="btn btn-primary" value="ถัดไป >" style="width:150px" /> &nbsp;&nbsp;
					<input type="button" class="btn btn-danger" value="ยกเลิก" style="width:100px" onclick="javascript:history.go(-1);" /> 
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value="milk_payment_header.save"/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>

