<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'administrator/components/com_edairy/assets/css/edairy.css');
$document->addStyleSheet(JUri::root() . 'media/com_edairy/css/list.css');

$user      = JFactory::getUser();
$userId    = $user->get('id');
$listOrder = $this->state->get('list.ordering');
$listDirn  = $this->state->get('list.direction');
$canOrder  = $user->authorise('core.edit.state', 'com_edairy');
$saveOrder = $listOrder == 'a.`ordering`';

if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?option=com_edairy&task=milk_quality_headers.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'milk_quality_headerList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}

$sortFields = $this->getSortFields();
?>
<script src="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript" charset="UTF-8"></script>

<link rel="stylesheet" href="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.css">
<script type="text/javascript">
	jQuery(document).ready(function () {
		jQuery('#clear-search-button').on('click', function () {
			jQuery('.well input').val('');
			jQuery('#adminForm').submit();
		});
	});

	js = jQuery.noConflict();
	js(document).ready(function () {
		js('.datetimepicker').datetimepicker({
            lang:'th',
            timepicker:false,
            format:'d/m/Y'
        });
     });
</script>
<form action="<?php echo JRoute::_('index.php?option=com_edairy&view=milk_quality_headers'); ?>" method="post"
	  name="adminForm" id="adminForm">
	<?php if (!empty($this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
		<?php else : ?>
		<div id="j-main-container">
			<?php endif; ?>

			<h4>ค้นหาคุณภาพน้ำนมดิบรายเกษตรกร</h4>
			<div id="filter-bar" class="btn-toolbar well" style="min-height:100px">

				<div class="filter-search btn-group pull-left">
					

					<label style="display:inline;">
						&nbsp;เลขที่ใบนำส่งตัวอย่างน้ำนมดิบ
					</label>
					<input type="text" name="search_run_no" value="<?php echo $_REQUEST["search_run_no"]; ?>" />	
					 

					<label for="filter_search" style="display:inline;">
						&nbsp;รหัสศูนย์/ชื่อสหกรณ์ 
					</label>
					 <input type="text" name="search_coop" value="<?php echo $_REQUEST["search_coop"]; ?>" />	

					

					<div style="clear:both"></div>
					 
					 <label style="display:inline;">
						&nbsp;ระหว่างวันที่
					</label>


					 <input type="text" name="search_date_from" class="datetimepicker" value="<?php echo $_REQUEST['search_date_from']; ?>"/>

					 <label style="display:inline;">
						&nbsp;ถึงวันที่
					</label>
					 
					 <input type="text" name="search_date_to" class="datetimepicker" value="<?php echo $_REQUEST['search_date_to']; ?>"/>

					<div style="clear:both">
					</div>



					<div class="btn-group pull-left">
					<button class="btn hasTooltip" type="submit"
							title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>">
						<i class="icon-search"></i></button>
					<button class="btn hasTooltip" id="clear-search-button" type="button"
							title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>">
						<i class="icon-remove"></i></button>
				</div>
				<div class="btn-group pull-right hidden-phone">
					<label for="limit"
						   class="element-invisible">
						<?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?>
					</label>
					<?php// echo $this->pagination->getLimitBox(); ?>
				</div>
				</div>
			</div>

			<div class="clearfix"></div>

			<b>พบทั้งสิ้น <?php echo $this->pagination->total; ?> รายการ</b>
			
            <?php //echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this)); ?>

			<div class="clearfix"></div>
			<table class="table table-striped" id="milk_quality_headerList">
				<thead>
				<tr>
					<?php if (isset($this->items[0]->ordering)): ?>
						<th width="1%" class="nowrap center hidden-phone">
                            <?php echo JHtml::_('searchtools.sort', '', 'a.`ordering`', $listDirn, $listOrder, null, 'asc', 'JGRID_HEADING_ORDERING', 'icon-menu-2'); ?>
                        </th>
					<?php endif; ?>
					<th width="1%" class="hidden-phone">
						<input type="checkbox" name="checkall-toggle" value=""
							   title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)"/>
					</th>
					<?php if (isset($this->items[0]->state)): ?>
						<th width="1%" class="nowrap center">
								<?php echo JHtml::_('searchtools.sort', 'JSTATUS', 'a.`state`', $listDirn, $listOrder); ?>
</th>
					<?php endif; ?>

									<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_MILK_QUALITY_HEADERS_ID', 'a.`id`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_MILK_QUALITY_HEADERS_RUN_NO', 'a.`run_no`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_MILK_QUALITY_HEADERS_COOP_ID', 'a.`coop_id`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_MILK_QUALITY_HEADERS_PERIOD', 'a.`period`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_MILK_QUALITY_HEADERS_FROM_DATE', 'a.`from_date`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_MILK_QUALITY_HEADERS_TO_DATE', 'a.`to_date`', $listDirn, $listOrder); ?>
				</th>
				<?php /*
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_MILK_QUALITY_HEADERS_TOTAL_SEND_MILK_AMOUNT', 'a.`total_send_milk_amount`', $listDirn, $listOrder); ?>
				</th>
				*/ ?>
				<th>
					พิมพ์
				</th>
				<th width="3%">
					แก้ไข
				</th>
				<th width="3%">
					ลบ
				</th>
					
				</tr>
				</thead>
				<tfoot>
				<tr>
					<td colspan="<?php echo isset($this->items[0]) ? count(get_object_vars($this->items[0])) : 10; ?>">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
				</tfoot>
				<tbody>
				<?php $itemIndex=0; foreach ($this->items as $i => $item) :
					$ordering   = ($listOrder == 'a.ordering');
					$canCreate  = $user->authorise('core.create', 'com_edairy');
					$canEdit    = $user->authorise('core.edit', 'com_edairy');
					$canCheckin = $user->authorise('core.manage', 'com_edairy');
					$canChange  = $user->authorise('core.edit.state', 'com_edairy');
					?>
					<tr class="row<?php echo $i % 2; ?>">

						<?php if (isset($this->items[0]->ordering)) : ?>
							<td class="order nowrap center hidden-phone">
								<?php if ($canChange) :
									$disableClassName = '';
									$disabledLabel    = '';

									if (!$saveOrder) :
										$disabledLabel    = JText::_('JORDERINGDISABLED');
										$disableClassName = 'inactive tip-top';
									endif; ?>
									<span class="sortable-handler hasTooltip <?php echo $disableClassName ?>"
										  title="<?php echo $disabledLabel ?>">
							<i class="icon-menu"></i>
						</span>
									<input type="text" style="display:none" name="order[]" size="5"
										   value="<?php echo $item->ordering; ?>" class="width-20 text-area-order "/>
								<?php else : ?>
									<span class="sortable-handler inactive">
							<i class="icon-menu"></i>
						</span>
								<?php endif; ?>
							</td>
						<?php endif; ?>
						<td class="hidden-phone">
							<?php echo JHtml::_('grid.id', $i, $item->id); ?>
						</td>
						<?php if (isset($this->items[0]->state)): ?>
							<td class="center">
								<?php echo JHtml::_('jgrid.published', $item->state, $i, 'milk_quality_headers.', $canChange, 'cb'); ?>
</td>
						<?php endif; ?>

										<td>

					<?php echo $item->id; ?>
				</td>				<td>
				<?php if (isset($item->checked_out) && $item->checked_out && ($canEdit || $canChange)) : ?>
					<?php echo JHtml::_('jgrid.checkedout', $i, $item->uEditor, $item->checked_out_time, 'milk_quality_headers.', $canCheckin); ?>
				<?php endif; ?>
				<?php if ($canEdit) : ?>
					
					<?php echo $this->escape($item->run_no); ?>
				<?php else : ?>
					<?php echo $this->escape($item->run_no); ?>
				<?php endif; ?>

				</td>				<td>

					<?php echo $item->coop_id; ?>
				</td>				<td>

					<?php echo $item->period; ?>
				</td>				<td>

					<?php echo formatDate($item->from_date); ?>
				</td>				<td>

					<?php echo formatDate($item->to_date); ?>
				</td>				
				<?php /*
				<td>

					<?php echo $item->total_send_milk_amount; ?>
				</td>*/ ?>
				<td>
						<a href="index.php?option=com_edairy&view=milk_quality_header&layout=pdf&id=<?php echo $item->id; ?>" class="btn" target="_blank"><div class="icon-print"></div> พิมพ์</a>
					
				</td>
				<td>
					<?php 
						$can_edit = true;
						$user = JFactory::getUser();
						if($user->role<7){
							if($item->created_by!=$user->id){
								$can_edit = false;
							}
						}
					?>
					<?php if($can_edit){ ?>
						<a href="<?php echo JRoute::_('index.php?option=com_edairy&task=milk_quality_header.edit&id='.(int) $item->id); ?>" class="btn btn-warning"> แก้ไข</a>
					<?php }else{ ?>
						<a href="#" class="btn btn-warning" disabled> แก้ไข</a>
					<?php } ?>
				</td>
				<td>
					<?php if($can_edit){ ?>
						<a href="#" class="btn btn-danger" onClick="javascript: if(confirm('ยืนยันการลบ ?')){jQuery('#cb<?php echo $itemIndex; ?>').attr('checked', 'checked'); Joomla.submitbutton('milk_quality_headers.trash');}"> ลบ</a>
					<?php }else{ ?>
						<a href="#" class="btn btn-danger" disabled> ลบ</a>
					<?php } ?>
				</td>

					</tr>
				<?php $itemIndex++; endforeach; ?>
				</tbody>
			</table>

			<input type="hidden" name="task" value=""/>
			<input type="hidden" name="boxchecked" value="0"/>
            <input type="hidden" name="list[fullorder]" value="<?php echo $listOrder; ?> <?php echo $listDirn; ?>"/>
			<?php echo JHtml::_('form.token'); ?>
		</div>
</form>
<script>
    window.toggleField = function (id, task, field) {

        var f = document.adminForm, i = 0, cbx, cb = f[ id ];

        if (!cb) return false;

        while (true) {
            cbx = f[ 'cb' + i ];

            if (!cbx) break;

            cbx.checked = false;
            i++;
        }

        var inputField   = document.createElement('input');

        inputField.type  = 'hidden';
        inputField.name  = 'field';
        inputField.value = field;
        f.appendChild(inputField);

        cb.checked = true;
        f.boxchecked.value = 1;
        window.submitform(task);

        return false;
    };
</script>