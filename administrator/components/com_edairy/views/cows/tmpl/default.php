<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'administrator/components/com_edairy/assets/css/edairy.css');
$document->addStyleSheet(JUri::root() . 'media/com_edairy/css/list.css');

$user      = JFactory::getUser();
$userId    = $user->get('id');
$listOrder = $this->state->get('list.ordering');
$listDirn  = $this->state->get('list.direction');
$canOrder  = $user->authorise('core.edit.state', 'com_edairy');
$saveOrder = $listOrder == 'a.`ordering`';

if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?option=com_edairy&task=cows.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'cowList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}

$sortFields = $this->getSortFields();
?>
<script type="text/javascript">
	Joomla.orderTable = function () {
		table = document.getElementById("sortTable");
		direction = document.getElementById("directionTable");
		order = table.options[table.selectedIndex].value;
		if (order != '<?php echo $listOrder; ?>') {
			dirn = 'asc';
		} else {
			dirn = direction.options[direction.selectedIndex].value;
		}
		Joomla.tableOrdering(order, dirn, '');
	};

	jQuery(document).ready(function () {
		jQuery('#clear-search-button').on('click', function () {
			jQuery('#filter_search').val('');
			jQuery('select[name=search_region]').val('');
			jQuery('select[name=search_province]').val('');
			jQuery('select[name=search_region]').val('');
			jQuery('select').trigger('liszt:updated');
			jQuery('.well input').val('');
			jQuery('#adminForm').submit();
		});
	});

	window.toggleField = function (id, task, field) {

		var f = document.adminForm,
			i = 0, cbx,
			cb = f[ id ];

		if (!cb) return false;

		while (true) {
			cbx = f[ 'cb' + i ];

			if (!cbx) break;

			cbx.checked = false;
			i++;
		}

		var inputField   = document.createElement('input');
		inputField.type  = 'hidden';
		inputField.name  = 'field';
		inputField.value = field;
		f.appendChild(inputField);

		cb.checked = true;
		f.boxchecked.value = 1;
		window.submitform(task);

		return false;
	};

</script>

<?php

// Joomla Component Creator code to allow adding non select list filters
if (!empty($this->extra_sidebar))
{
	$this->sidebar .= $this->extra_sidebar;
}

?>
<style type="text/css">
	.js-stools{
		display: none;
	}
</style>
<form action="<?php echo JRoute::_('index.php?option=com_edairy&view=cows'); ?>" method="post"
	  name="adminForm" id="adminForm">
	<?php if (!empty($this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
		<?php else : ?>
		<div id="j-main-container">
			<?php endif; ?>

			<h4>ค้นหาทะเบียนโค</h4>
			<div id="filter-bar" class="btn-toolbar well" style="min-height:170px">

				<div class="filter-search btn-group pull-left">
					<label style="display:inline;">
						ภาค
					</label>
					 <select name="search_region" style="width:130px">
					 	<option value="">-- เลือกภาค --</option>
					 	<?php 
							foreach($this->masterData->mas_region as $bkey=>$bdata){ 
								$selected = "";
								if($bdata->id == $_REQUEST["search_region"]){
									$selected = " selected";
								}
						?>
								<option value="<?php echo $bdata->id;?>"<?php echo $selected; ?>><?php echo $bdata->name; ?></option>
						<?php } ?>

					 </select>

					<label style="display:inline;">
						&nbsp;จังหวัด
					</label>
					 <select name="search_province" style="width:130px">
					 	<option value="">-- เลือกจังหวัด --</option>
					 	<?php 
							foreach($this->masterData->mas_province as $bkey=>$bdata){ 
								$selected = "";
								if($bdata->id == $_REQUEST["search_province"]){
									$selected = " selected";
								}
						?>
								<option value="<?php echo $bdata->id;?>"<?php echo $selected; ?>><?php echo $bdata->name; ?></option>
						<?php } ?>
					 </select>

					<label style="display:inline;">
						&nbsp;รหัสศูนย์/ชื่อสหกรณ์ 
					</label>
					 <input type="text" name="search_coop" id="search_coop"
						   value="<?php echo $_REQUEST['search_coop']; ?>"
						   title="<?php echo JText::_('JSEARCH_FILTER'); ?>"/>

					<div style="clear:both">
					</div>
					<label style="display:inline;">
						ชื่อฟาร์ม
					</label>
					<input type="text" name="search_farm_name" value="<?php echo $_REQUEST['search_farm_name']; ?>"/>	
					 

					<label style="display:inline;">
						&nbsp;หมายเลขสมาชิก/เบอร์ถัง
					</label>
					<input type="text" name="search_member_code" value="<?php echo $_REQUEST['search_member_code']; ?>"/>	
					 

					<label style="display:inline;">
						&nbsp;ชื่อ
					</label>


					 <input type="text" name="search_farmer_name" value="<?php echo $_REQUEST['search_farmer_name']; ?>"/>


					 <label style="display:inline;">
						&nbsp;นามสกุล
					</label>


					 <input type="text" name="search_farmer_surname" value="<?php echo $_REQUEST['search_farmer_surname']; ?>"/>

					 <div style="clear:both"></div>

					 <label style="display:inline;">
						&nbsp;หมายเลขโค
					</label>


					 <input type="text" name="search_cow_id" value="<?php echo $_REQUEST['search_cow_id']; ?>"/>


					 <label style="display:inline;">
						&nbsp;ชื่อโค
					</label>


					 <input type="text" name="search_cow_name" value="<?php echo $_REQUEST['search_cow_name']; ?>"/>

					 <div style="clear:both">
					</div>

					<label style="display:inline;">
						&nbsp;แสดงโคสถานะ
					</label>


					 <input type="checkbox" style="margin-top:-10px"/> 
					 <label style="display:inline;">มีชีวิต&nbsp;&nbsp;</label>
					  <input type="checkbox" style="margin-top:-10px"/> 
					  <label style="display:inline;">ตาย&nbsp;&nbsp;</label>
					  <input type="checkbox" style="margin-top:-10px"/> 
					  <label style="display:inline;">ขาย&nbsp;&nbsp;</label> 
					  <input type="checkbox" style="margin-top:-10px"/> 
					  <label style="display:inline;">ถูกคัดทิ้ง</label>

					 <div style="clear:both">
					</div>


					

				</div>
					<div style="clear:both">
					</div>
				<div class="btn-group pull-left">
					<button class="btn hasTooltip" type="submit"
							title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>">
						<i class="icon-search"></i></button>
					<button class="btn hasTooltip" id="clear-search-button" type="button"
							title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>">
						<i class="icon-remove"></i></button>
				</div>

				<?php /*
				<div class="btn-group pull-right hidden-phone">
					<label for="limit"
						   class="element-invisible">
						<?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?>
					</label>
					<?php echo $this->pagination->getLimitBox(); ?>
				</div>
				<div class="btn-group pull-right hidden-phone">
					<label for="directionTable"
						   class="element-invisible">
						<?php echo JText::_('JFIELD_ORDERING_DESC'); ?>
					</label>
					<select name="directionTable" id="directionTable" class="input-medium"
							onchange="Joomla.orderTable()">
						<option value=""><?php echo JText::_('JFIELD_ORDERING_DESC'); ?></option>
						<option value="asc" <?php echo $listDirn == 'asc' ? 'selected="selected"' : ''; ?>>
							<?php echo JText::_('JGLOBAL_ORDER_ASCENDING'); ?>
						</option>
						<option value="desc" <?php echo $listDirn == 'desc' ? 'selected="selected"' : ''; ?>>
							<?php echo JText::_('JGLOBAL_ORDER_DESCENDING'); ?>
						</option>
					</select>
				</div>
				<div class="btn-group pull-right">
					<label for="sortTable" class="element-invisible"><?php echo JText::_('JGLOBAL_SORT_BY'); ?></label>
					<select name="sortTable" id="sortTable" class="input-medium" onchange="Joomla.orderTable()">
						<option value=""><?php echo JText::_('JGLOBAL_SORT_BY'); ?></option>
						<?php echo JHtml::_('select.options', $sortFields, 'value', 'text', $listOrder); ?>
					</select>
				</div>
				*/ ?>

				<?php echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this)); ?>
			</div>

			<b>พบทั้งสิ้น <?php echo $this->pagination->total; ?> รายการ</b>
			<div class="clearfix"></div>
			<table class="table table-striped" id="cowList">
				<thead>
				<tr>
					<?php if (isset($this->items[0]->ordering)): ?>
						<th width="1%" class="nowrap center hidden-phone" style="display:none">
							<?php echo JHtml::_('grid.sort', '<i class="icon-menu-2"></i>', 'a.`ordering`', $listDirn, $listOrder, null, 'asc', 'JGRID_HEADING_ORDERING'); ?>
						</th>
					<?php endif; ?>
					<th width="1%" class="hidden-phone" style="display:none">
						<input type="checkbox" name="checkall-toggle" value=""
							   title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)"/>
					</th>
					<?php if (isset($this->items[0]->state)): ?>
						<th width="1%" class="nowrap center">
								<?php echo JHtml::_('searchtools.sort', 'JSTATUS', 'a.`state`', $listDirn, $listOrder); ?>
</th>
					<?php endif; ?>

									<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_COWS_ID', 'a.`id`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_COWS_FARM_ID', 'a.`farm_id`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_COWS_COW_ID', 'a.`cow_id`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_COWS_NAME', 'a.`name`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_COWS_DLD_ID', 'a.`dld_id`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_COWS_EAR_ID', 'a.`ear_id`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_COWS_BIRTHDATE', 'a.`birthdate`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_COWS_GENDER', 'a.`gender`', $listDirn, $listOrder); ?>
				</th>
				<?php /*
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_COWS_BREED', 'a.`breed`', $listDirn, $listOrder); ?>
				</th>*/ ?>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'สถานะ', 'a.`cow_status_id`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_EDAIRY_COWS_UPDATE_DATETIME', 'a.`update_datetime`', $listDirn, $listOrder); ?>
				</th>
				<th width="10%">
					บัตรประจำตัว
				</th>
				<th width="3%">
					แก้ไข
				</th>
				<th width="3%">
					ลบ
				</th>

					
				</tr>
				</thead>
				<tfoot>
				<tr>
					<td colspan="<?php echo isset($this->items[0]) ? count(get_object_vars($this->items[0])) : 10; ?>">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
				</tfoot>
				<tbody>
				<?php $itemIndex=0; foreach ($this->items as $i => $item) :
					$ordering   = ($listOrder == 'a.ordering');
					$canCreate  = $user->authorise('core.create', 'com_edairy');
					$canEdit    = $user->authorise('core.edit', 'com_edairy');
					$canCheckin = $user->authorise('core.manage', 'com_edairy');
					$canChange  = $user->authorise('core.edit.state', 'com_edairy');
					?>
					<tr class="row<?php echo $i % 2; ?>">

						<?php if (isset($this->items[0]->ordering)) : ?>
							<td class="order nowrap center hidden-phone" style="display:none">
								<?php if ($canChange) :
									$disableClassName = '';
									$disabledLabel    = '';

									if (!$saveOrder) :
										$disabledLabel    = JText::_('JORDERINGDISABLED');
										$disableClassName = 'inactive tip-top';
									endif; ?>
									<span class="sortable-handler hasTooltip <?php echo $disableClassName ?>"
										  title="<?php echo $disabledLabel ?>">
							<i class="icon-menu"></i>
						</span>
									<input type="text" style="display:none" name="order[]" size="5"
										   value="<?php echo $item->ordering; ?>" class="width-20 text-area-order "/>
								<?php else : ?>
									<span class="sortable-handler inactive">
							<i class="icon-menu"></i>
						</span>
								<?php endif; ?>
							</td>
						<?php endif; ?>
						<td class="hidden-phone" style="display:none">
							<?php echo JHtml::_('grid.id', $i, $item->id); ?>
						</td>
						<?php if (isset($this->items[0]->state)): ?>
							<td class="center">
								<?php echo JHtml::_('jgrid.published', $item->state, $i, 'cows.', $canChange, 'cb'); ?>
</td>
						<?php endif; ?>

										<td>

					<?php echo $item->id; ?>
				</td>				<td>

					<?php echo $item->farm_id; ?>
				</td>				<td>
				<?php if (isset($item->checked_out) && $item->checked_out && ($canEdit || $canChange)) : ?>
					<?php echo JHtml::_('jgrid.checkedout', $i, $item->uEditor, $item->checked_out_time, 'cows.', $canCheckin); ?>
				<?php endif; ?>
				<?php if ($canEdit) : ?>
					<a href="<?php echo JRoute::_('index.php?option=com_edairy&task=cow.edit&id='.(int) $item->id); ?>">
					<?php echo $this->escape($item->cow_id); ?></a>
				<?php else : ?>
					<?php echo $this->escape($item->cow_id); ?>
				<?php endif; ?>

				</td>				<td>

					<?php echo $item->name; ?>
				</td>				<td>

					<?php echo $item->dld_id; ?>
				</td>				<td>

					<?php echo $item->ear_id; ?>
				</td>				<td>

					<?php echo formatDate($item->birthdate); ?>
				</td>				<td>

					<?php echo $item->gender; ?>
				</td>
				<?php /*				
				<td>
				<div style="max-width:130px;word-break: break-all;">
					<?php 
						$breed_string = "";
						if(count($item->breeds)>0){
							foreach($item->breeds as $bkey=>$bdata){
								$breed_string .= $bdata->percent . "%" . $bdata->breed . ", ";
							}
							$breed_string = substr($breed_string, 0, strlen($breed_string)-2);

							/*if(strlen($breed_string)>50){
								$breed_string = substr($breed_string, 0, 50) . "...";
							}
						}
						echo $breed_string;
					?>
					</div>
				</td>	*/ ?>

							<td>

					<?php if($item->cow_status_id=="0"){
							$item->cow_status_id = "-";
						} 
						echo $item->cow_status_id; ?>
				</td>				<td>

					<?php echo formatDate($item->update_datetime); ?>
				</td>
				<td>
					<a href="index.php?option=com_edairy&view=cow&layout=pdf&id=<?php echo $item->id; ?>" class="btn" target="_blank"><div class="icon-print"></div> พิมพ์</a>
				</td>
				<td>
					<a href="<?php echo JRoute::_('index.php?option=com_edairy&task=cow.edit&id='.(int) $item->id); ?>" class="btn btn-warning"> แก้ไข</a>
				</td>
				<td>
					<a href="#" class="btn btn-danger" onClick="javascript: if(confirm('ยืนยันการลบ ?')){jQuery('#cb<?php echo $itemIndex; ?>').attr('checked', 'checked'); Joomla.submitbutton('cows.trash');}"> ลบ</a>
				</td>
					</tr>
				<?php $itemIndex++; endforeach; ?>
				</tbody>
			</table>

			<input type="hidden" name="task" value=""/>
			<input type="hidden" name="boxchecked" value="0"/>
			<input type="hidden" name="list[fullorder]" value="<?php echo $listOrder; ?> <?php echo $listDirn; ?>"/>
			<?php echo JHtml::_('form.token'); ?>
		</div>
</form>        
