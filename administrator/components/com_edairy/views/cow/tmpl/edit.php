<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_edairy/css/form.css');
?>

<script src="../media/com_edairy/typeahead/bootstrap3-typeahead.js" type="text/javascript" charset="UTF-8"></script>

<script src="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript" charset="UTF-8"></script>

<link rel="stylesheet" href="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.css">
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
			js('.datetimepicker').datetimepicker({
            lang:'th',
            timepicker:false,
            format:'d/m/Y'
        });
	js('input:hidden.farm_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('farm_idhidden')){
			js('#jform_farm_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_farm_id").trigger("liszt:updated");
	js('input:hidden.move_from_farm_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('move_from_farm_idhidden')){
			js('#jform_move_from_farm_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_move_from_farm_id").trigger("liszt:updated");
	js('input:hidden.father_cow_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('father_cow_idhidden')){
			js('#jform_father_cow_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_father_cow_id").trigger("liszt:updated");
	js('input:hidden.mother_cow_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('mother_cow_idhidden')){
			js('#jform_mother_cow_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_mother_cow_id").trigger("liszt:updated");
	});

	Joomla.submitbutton = function (task) {
		if (task == 'cow.cancel') {
			Joomla.submitform(task, document.getElementById('cow-form'));
		}
		else {
			
			if (task != 'cow.cancel' && document.formvalidator.isValid(document.id('cow-form'))) {
				
				Joomla.submitform(task, document.getElementById('cow-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_edairy&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="cow-form" class="form-validate">

	<div class="form-horizontal">
		

		<?php 
			if($_REQUEST["default_tab"]==""){
				echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general'));
			}else{
				echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => $_REQUEST["default_tab"]));
			} 
		?>
		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', 'ข้อมูลทั่วไป'); ?>
		<div class="row-fluid">
			<div class="span5 form-horizontal">
				<fieldset class="adminform">

				<h4>รูปโค</h4>

				<?php if (!empty($this->item->image_url)) : ?>
					<?php foreach ((array)$this->item->image_url as $fileSingle) : ?>
						<?php if (!is_array($fileSingle)) : ?>
							<img src="../upload/<?php echo $fileSingle; ?>" width="200" style="padding:10px" /><br />
						<?php endif; ?>
					<?php endforeach; ?>
				<?php endif; ?>

				

				<?php echo $this->form->renderField('image_url'); ?>

				

				<h4>ข้อมูลเจ้าของ</h4>
					<div style="display:none">
									<?php echo $this->form->renderField('id'); ?>
					</div>

									<?php echo $this->form->renderField('farm_id'); ?>

				<div class="control-group">
					<div class="control-label">
						<label class="">รหัสศูนย์/สหกรณ์</label>
					</div>
					<div class="controls">
						<input type="text" name="coop_name" readonly placeholder="อัตโนมัติ" value="<?php echo $this->item->farm->coop_name; ?>" />
					</div>
				</div>
				<div class="control-group">
					<div class="control-label">
						<label class="">หมายเลขสมาชิก</label>
					</div>
					<div class="controls">
						<input type="text" name="member_code" placeholder="อัตโนมัติ" readonly value="<?php echo $this->item->farm->member_code; ?>"/>
					</div>
				</div>

				<div class="control-group">
					<div class="control-label">
						<label class="">ชื่อเกษตรกร</label>
					</div>
					<div class="controls">
						<input type="text" name="farmer_name" placeholder="อัตโนมัติ" readonly value="<?php echo $this->item->farm->farmer_name; ?>"/>
					</div>
				</div>

				

			<?php
				foreach((array)$this->item->farm_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="farm_id" name="jform[farm_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>								<?php echo $this->form->renderField('create_by'); ?>
				<?php echo $this->form->renderField('update_by'); ?>

				<div class="control-group">
					<div class="control-label">
						<label class="">วันที่สร้าง</label>
					</div>
					<div class="controls">
						<?php echo formatDate($this->item->create_datetime); ?>
					</div>
				</div>

				<div class="control-group">
					<div class="control-label">
						<label class="">วันที่ปรับปรุง</label>
					</div>
					<div class="controls">
						<?php echo formatDate($this->item->update_datetime); ?>
					</div>
				</div>


				
				<div style="display: none">
					<?php echo $this->form->renderField('create_datetime'); ?>
					<?php echo $this->form->renderField('update_datetime'); ?>
					<?php echo $this->form->renderField('state'); ?>

					<?php echo $this->form->renderField('created_by'); ?>
				</div>



					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>

			<div class="span5 form-horizontal">
				<h4>ข้อมูลโค</h4>
				<?php echo $this->form->renderField('cow_id'); ?>
				<?php echo $this->form->renderField('name'); ?>
				<?php echo $this->form->renderField('dld_id'); ?>
				<?php echo $this->form->renderField('ear_id'); ?>
				
				<div class="control-group">
								<div class="control-label">
									<label>
										วันเกิด
									</label>
								</div>
								<div class="controls">
									<input type="text" name="birthdate" class="datetimepicker"  value="<?php echo formatDate($this->item->birthdate); ?>"/>
								</div>
							</div>

				<?php echo $this->form->renderField('gender'); ?>
				

				<div class="control-group">
								<div class="control-label">
									<label>
										วันย้ายเข้าฟาร์ม
									</label>
								</div>
								<div class="controls">
									<input type="text" name="move_farm_date" class="datetimepicker"  value="<?php echo formatDate($this->item->move_farm_date); ?>"/>
								</div>
							</div>


				<?php echo $this->form->renderField('move_type'); ?>
				<?php echo $this->form->renderField('move_from_farm_id'); ?>

			<?php
				foreach((array)$this->item->move_from_farm_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="move_from_farm_id" name="jform[move_from_farm_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				

			<div style="display:none">

			<?php echo $this->form->renderField('breed'); ?>



				<?php echo $this->form->renderField('father_cow_id'); ?>

			<?php
				foreach((array)$this->item->father_cow_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="father_cow_id" name="jform[father_cow_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('mother_cow_id'); ?>

			<?php
				foreach((array)$this->item->mother_cow_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="mother_cow_id" name="jform[mother_cow_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>
		</div>


			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'breed', JText::_('สายพันธ์', true)); ?>
		<?php
			$breed_string = "";
						if(count($this->item->breeds)>0){
							foreach($this->item->breeds as $bkey=>$bdata){
								$breed_string .= $bdata->percent . "%" . $bdata->breed . ", ";
							}
							$breed_string = substr($breed_string, 0, strlen($breed_string)-2);

							/*if(strlen($breed_string)>50){
								$breed_string = substr($breed_string, 0, 50) . "...";
							}*/
						}
		?>
		<div class="row-fluid">
			<div class="span12 form-horizontal" style="text-align:center">
				<fieldset class="adminform">
					
					<table width="100%">
						<tr valign="top">
							<td width="50%" style="text-align:left">
								<h3>ข้อมูลพ่อพันธ์</h3>
								<table cellpadding="5">
									<tr>
										<td width="120">ค้นหาพ่อพันธ์</td>
										<td>
											<input type="text" id="dad_cow_id" value="" placeholder="Loading..." />
											<a class="btn btn-primary button-select" title="Select User" onClick="javascript:searchDad();"><span class="icon-search"></span> ค้นหา</a>
										</td>
									</tr>
									<tr>
										<td>พ่อ</td>
										<td>
											<input type="text" id="breed_dad" value="" readonly />
										</td>
									</tr>
									<tr>
										<td>สายพันธ์</td>
										<td>
											<input type="text" id="breed_dad_value" value="" style="width:500px" readonly />
										</td>
									</tr>
									<tr>
										<td>ปู่</td>
										<td>
											<input type="text" id="breed_dad_granddad" value="" readonly />
										</td>
									</tr>
									<tr>
										<td>ย่า</td>
										<td>
											<input type="text" id="breed_dad_grandmom" value="" readonly />
										</td>
									</tr>
								</table>

							</td>
							<td width="50%" style="text-align:left">
								<h3>ข้อมูลแม่พันธ์</h3>
								<table cellpadding="5">
									<tr>
										<td width="120">ค้นหาแม่พันธ์</td>
										<td>
											<input type="text" id="mom_cow_id" value=""  placeholder="Loading..." />
											<a class="btn btn-primary button-select" title="Select User" onClick="javascript:searchMom();"><span class="icon-search"></span> ค้นหา</a>
										</td>
									</tr>
									<tr>
										<td>แม่</td>
										<td>
											<input type="text" id="breed_mom" value="" readonly />
										</td>
									</tr>
									<tr>
										<td>สายพันธ์</td>
										<td>
											<input type="text" id="breed_mom_value" value="" style="width:500px" readonly />
										</td>
									</tr>
									<tr>
										<td>ตา</td>
										<td>
											<input type="text" id="breed_mom_granddad" value="" readonly />
										</td>
									</tr>
									<tr>
										<td>ยาย</td>
										<td>
											<input type="text" id="breed_mom_grandmom" value="" readonly />
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<br /><br />
					สายพันธ์ 
					<input type="text" name="calculated_breed" style="width:600px" value="<?php echo $breed_string; ?>"/>

					<br /><br />
					<input type="button" class="btn btn-success" value="คำนวณสายพันธ์" onclick="javascript:calculateBreed();" />
					<br /><br />
					<input type="button" id="save-breed-btn" class="btn btn-primary" value="บันทึกข้อมูลสายพันธ์" onclick="javascript:saveBreed();"  />
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>




		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'cow_status', JText::_('สถานภาพ', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">
					
					<h3>สภานภาพ</h3>
					สถานภาพปัจจุบัน: 
					<select id="cow_status_cow_status_id" name="cow_status_cow_status_id" style="width:300px">
						<option value="">-- โปรดเลือกสถานภาพ --</option>
						<?php 
							foreach($this->masterData->mas_cow_status as $bkey=>$bdata){ 
								$selected = "";
								if($bdata->id == $this->item->cow_status_id){
									$selected = " selected";
								}
						?>
								<option value="<?php echo $bdata->id;?>"<?php echo $selected; ?>><?php echo $bdata->name; ?> <?php echo $bdata->description; ?></option>
						<?php } ?>
					</select>
					<input id="update-btn" type="button" class="btn btn-primary" value="อับเดทสถานะ" onClick="javascript:updateCowStatus();" />
					<br />
					<hr />
					<b>ประวัติสถานภาพโค</b><br /><br />
					<table class="table table-striped table-bordered table-blue">
						<thead>
							<tr>
								<th style="text-align:center">รหัส</th>
								<th style="text-align:center">สถานภาพโค</th>
								<th style="text-align:center">วันที่</th>
								<th style="text-align:center">ปรับปรุงข้อมูลล่าสุดโดย</th>
							</tr>
						</thead>
						<tbody>
							<?php if(count($this->item->cow_status_history)==0){ ?>
								<tr>
									<td style="text-align:center" colspan="6">ไม่พบข้อมูล</td>
								</tr>
							<?php }else{ ?>
								<?php $i=0;
									foreach($this->item->cow_status_history as $key=>$data){ ?>
									<tr>
										<td><?php echo $data->name; ?></td>
										<td><?php echo $data->description; ?></td>
										<td><?php echo formatDate($data->status_date); ?></td>
										<td><?php
										 if($data->update_by!="0" && $data->update_by!=""){
										 	$user = JFactory::getUser($data->update_by);
										 	echo $user->name; 
										 }
										 
										 ?></td>
									</tr>
								<?php } ?>
							<?php } ?>
						</tbody>
					</table>
									
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>



		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'move_out', JText::_('ย้ายออกจากฝูง', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">
					สถานะปัจจุบัน: 
					<b>ยังอยู่ในฟาร์ม</b> &nbsp;&nbsp;&nbsp;
					<button class="btn btn-info" data-toggle="modal" data-target="#MoveOutModal">บันทึกการย้ายออก</button>
					<br />
					<hr />
					<b>ประวัติการเปลี่ยนเจ้าของ/คัดออก</b><br /><br />
					<table class="table table-striped table-bordered table-blue">
						<thead>
							<tr>
								<th style="text-align:center">วันที่</th>
								<th style="text-align:center">วิธีการ</th>
								<th style="text-align:center">ต้นทาง</th>
								<th style="text-align:center">ปลายทาง</th>
								<th style="text-align:center">เหตุผล</th>
								<th style="text-align:center">ปรับปรุงข้อมูลล่าสุดโดย</th>
								<th style="text-align:center">วันที่ปรับปรุง</th>
								<th style="text-align:center">แก้ไข</th>
								<th style="text-align:center">ลบ</th>
							</tr>
						</thead>
						<tbody>
							<?php if(count($this->item->move_out_history)==0){ ?>
								<tr>
									<td style="text-align:center" colspan="9">ไม่พบข้อมูล</td>
								</tr>
							<?php }else{ ?>
								<?php $i=0;
								$move_map = array(1=>"ตาย", 2=>"ขาย", 3=>"คัดออก");
									foreach($this->item->move_out_history as $key=>$data){ ?>
									<tr>
										<td><?php echo formatDate($data->move_out_date); ?></td>
										<td><?php echo $move_map[$data->move_type]; ?></td>
										<td><?php echo $data->from_farm; ?></td>
										<td><?php echo $data->to_farm; ?></td>
										<td><?php echo $data->move_reason; ?></td>
										<td><?php $user = JFactory::getUser($data->created_by); 
											echo $user->name;
										?></td>
										<td><?php echo formatDate($data->move_out_date); ?></td>
										<td style="text-align: center"><button type="button" class="btn btn-warning" data-toggle="modal" data-target="#MoveOutModal" onClick="javascript:getMoveOut(<?php echo $data->id; ?>);">แก้ไข</button></td>
										<td style="text-align: center"><button type="button" class="btn btn-danger" onClick="javascript: if(confirm('ยืนยันการลบ?')){jQuery(this).prop('disabled', true);deleteMoveOut(<?php echo $data->id; ?>);}">ลบ</button></td>
									</tr>
								<?php } ?>
							<?php } ?>
						</tbody>
					</table>
									
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>




		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'breeding_history', JText::_('ประวัติการผสมพันธ์', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">
					<h3>ประวัติการผสมพันธ์</h3>
					<table class="table table-striped table-bordered table-blue">
						<thead>
							<tr>
								<th style="text-align:center">รอบ</th>
								<th style="text-align:center">ครั้งที่</th>
								<th style="text-align:center">วันผสม</th>
								<th style="text-align:center">รหัสพ่อพันธ์</th>
								<th style="text-align:center">ชื่อพ่อพันธ์</th>
								<th style="text-align:center">Lot No.</th>
								<th style="text-align:center">แหล่งน้ำเชื้อ</th>
								<th style="text-align:center">กำหนดตรวจท้อง</th>

								<th style="text-align:center">วันตรวจท้อง</th>
								<th style="text-align:center">ผลตรวจ</th>
								<th style="text-align:center">สัตวแพทย์</th>
								<th style="text-align:center">วันที่ปรับปรุงข้อมูล</th>
							</tr>
						</thead>
						<tbody>
							<?php if(count($this->item->insemination_history)==0){ ?>
								<tr>
									<td style="text-align:center" colspan="12">ไม่พบข้อมูล</td>
								</tr>
							<?php }else{ ?>
								<?php $i=0;
									foreach($this->item->insemination_history as $key=>$data){ ?>
									<tr>
										<td><?php echo ++$i; ?></td>
										<td>1</td>
										<td><?php echo formatDate($data->create_date); ?></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td><?php echo formatDate($data->create_date); ?></td>
										<td><?php 
											$user = JFactory::getUser($data->created_by);
											$vet = $user->name;
											echo $vet; ?></td>
										<td></td>
									</tr>
								<?php } ?>
							<?php } ?>
						</tbody>
					</table>
									
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>



		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'child_history', JText::_('ประวัติการออกลูก', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">
					<h3>ประวัติการออกลูก</h3>
					<table class="table table-striped table-bordered table-blue">
						<thead>
							<tr>
								<th style="text-align:center">ครั้งที่</th>
								<th style="text-align:center">วันผสม</th>
								<th style="text-align:center">พ่อพันธ์</th>
								<th style="text-align:center">กำหนดหยุดรีดนม</th>
								<th style="text-align:center">กำหนดคลอด</th>
								<th style="text-align:center">วันหยุดรีดนม</th>
								<th style="text-align:center">วันคลอด</th>
								<th style="text-align:center">ลูกโค</th>
								<th style="text-align:center">เพศ</th>
								<th style="text-align:center">น้ำหนัก</th>
								<th style="text-align:center">หูตัด</th>
								<th style="text-align:center">หมายเลข</th>
								<th style="text-align:center">สัตวแพทย์ผู้ให้บริการ</th>
								<th style="text-align:center">วันที่ปรับปรุงข้อมูล</th>
							</tr>
						</thead>
						<tbody>
							<?php if(count($this->item->cow_has_child_history)==0){ ?>
								<tr>
									<td style="text-align:center" colspan="14">ไม่พบข้อมูล</td>
								</tr>
							<?php }else{ ?>
								<?php $i=0;
									foreach($this->item->cow_has_child_history as $key=>$data){ ?>
									<tr>
										<td><?php echo ++$i; ?></td>
										<td><?php echo formatDate($data->breeding_date);?></td>
										<td><?php echo $data->father; ?></td>
										<td><?php echo formatDate($data->stop_milking_date);?></td>
										<td><?php echo formatDate($data->birth_date);?></td>
										<td><?php echo formatDate($data->stop_milking_date);?></td>
										<td><?php echo formatDate($data->birth_date);?></td>
										<td><?php echo $data->child_name; ?></td>
										<td><?php echo $data->child_gender; ?></td>
										<td><?php echo $data->child_weight; ?></td>
										<td><?php echo $data->child_ear_id; ?></td>
										<td><?php echo $data->child_cow_id_code; ?></td>
										<td><?php 
											$user = JFactory::getUser($data->created_by);
											$vet = $user->name;
											echo $vet; ?></td>
										<td><?php echo formatDate($data->update_date);?></td>
									</tr>
								<?php } ?>
							<?php } ?>
						</tbody>
					</table>
									
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>


		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'health', JText::_('ประวัติการรักษา', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">
					<h3>ประวัติการรักษา</h3>
					<table class="table table-striped table-bordered table-blue">
						<thead>
							<tr>
								<th style="text-align:center">วันที่</th>
								<th style="text-align:center">โรค/อาการ</th>
								<th style="text-align:center">การรักษา</th>
								<th style="text-align:center">เวชภัณฑ์</th>
								<th style="text-align:center">สัตวแพทย์ผู้ให้บริการ</th>
								<th style="text-align:center">วันที่ปรับปรุงข้อมูล</th>
							</tr>
						</thead>
						<tbody>
							<?php if(count($this->item->health_history)==0){ ?>
								<tr>
									<td style="text-align:center" colspan="6">ไม่พบข้อมูล</td>
								</tr>
							<?php }else{ ?>
								<?php $i=0;
									foreach($this->item->health_history as $key=>$data){ ?>
									<tr>
										<td><?php echo formatDate($data->create_date); ?></td>
										<td><?php echo $data->health_service; ?></td>
										<td><?php echo $data->health_sub_service; ?></td>
										<td><?php echo $data->medicine; ?></td>
										<td><?php 
											$user = JFactory::getUser($data->created_by);
											$vet = $user->name;
											echo $vet; ?></td>
										<td><?php echo formatDate($data->create_date); ?></td>
									</tr>
								<?php } ?>
							<?php } ?>
						</tbody>
					</table>
									
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>

<!-- Modal -->
<div id="MoveOutModal" class="modal fade" role="dialog" style="display:none">
<form id="MoveOutForm" action="" method="post">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">บันทึกการย้ายออก</h4>
      </div>
      <div class="modal-body">
        <table cellpadding="5">
        	<tr valign="top">
        		<td width="120">วันที่ย้ายออก*</td>
        		<td>
        			<input type="text" name="move_out_move_out_date" class="datetimepicker" placeholder="DD/MM/YYYY (พ.ศ.)" />
        		</td>
        	</tr>
        	<tr valign="top">
        		<td>วิธีการย้ายออก</td>
        		<td>
        			<input type="radio" id="radio_1" name="move_out_move_type" value="1" /> ตาย <br />
        			<input type="radio" id="radio_2" name="move_out_move_type" value="2" /> ขาย  
        			<select name="move_out_to_farm_id1">
        				<option value="">-- ระบุหมายเลขสมาชิกปลายทาง --</option>
					 	<?php 
							foreach($this->masterData->mas_farm as $bkey=>$bdata){ 
						?>
								<option value="<?php echo $bdata->id;?>"><?php echo $bdata->member_code; ?> <?php echo $bdata->name; ?></option>
						<?php } ?>
        			</select>
        			<br />
        			<input type="radio" id="radio_3" name="move_out_move_type" value="3" /> คัดออก <br />
        			เจตนา
        			<select name="move_out_move_objective">
        				<option value="">-- เลือกวัตถุประสงค์การย้ายออก --</option>
					 	<?php 
							foreach($this->masterData->mas_move_out_objective as $bkey=>$bdata){ 
						?>
								<option value="<?php echo $bdata->name;?>"><?php echo $bdata->name; ?></option>
						<?php } ?>
        			</select>
        			<br />
        			เหตุผล
        			<select name="move_out_move_reason">
        				<option value="">-- เลือกเหตุผลการย้ายออก --</option>
					 	<?php 
							foreach($this->masterData->mas_move_out_reason as $bkey=>$bdata){ 
						?>
								<option value="<?php echo $bdata->name;?>"><?php echo $bdata->name; ?></option>
						<?php } ?>
        			</select>
        			<br />
        			ปลายทาง (ถ้ามี)
        			<select name="move_out_to_farm_id2">
        				<option value="">-- ระบุหมายเลขสมาชิกปลายทาง --</option>
					 	<?php 
							foreach($this->masterData->mas_farm as $bkey=>$bdata){ 
						?>
								<option value="<?php echo $bdata->id;?>"><?php echo $bdata->member_code; ?> <?php echo $bdata->name; ?></option>
						<?php } ?>
        			</select>
        		</td>
        	</tr>
        </table>
        <input type="hidden" name="move_out_id" value="" />
        <input type="hidden" name="move_out_from_farm_id" value="<?php echo $this->item->farm_id;?>" />
        <input type="hidden" name="move_out_cow_id" value="<?php echo $this->item->id;?>" />
        <input type="hidden" name="option" value="<?php echo $_REQUEST['option'];?>" />
        <input type="hidden" name="task" value="cow.saveMoveOut" />
      </div>
      <div class="modal-footer" style="text-align:left">
      	<button type="button" class="btn btn-info" style="width:100px" onClick="javascript:saveMoveOut();">บันทึก</button>&nbsp;&nbsp;
        <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
      </div>
    </div>

  </div>
  </form>
</div>

<script type="text/javascript">
	function updateCowStatus(){
		jQuery("#update-btn").attr("disabled", "disabled");
		jQuery.ajax({
            url: "index.php?option=com_edairy&task=cow.updateCowStatus",
            type: "POST",
            data: {
            	'cow_id': '<?php echo $_REQUEST['id'];?>',
                'cow_status_id': jQuery("#cow_status_cow_status_id").val()
            },
            success: function (data) {
            	var data = jQuery.parseJSON(data);
                if (data.update_result==true) {
                	alert("อับเดทสถานะเรียบร้อย");
                } else {
                    alert("ไม่สามารถอับเดทข้อมูลได้");

                }
                window.location = "index.php?option=com_edairy&view=cow&layout=edit&id=<?php echo $_REQUEST['id'];?>&default_tab=cow_status";


            }

        });
	}

	var breed_dad_array = new Array();
	var breed_mom_array = new Array();

	function searchDad(){
		var cow_id = jQuery("#dad_cow_id").val().split(" ");
		jQuery.ajax({
            url: "index.php?option=com_edairy&task=cow.searchCow",
            type: "GET",
            data: {
            	'cow_id': cow_id[0]
            },
            success: function (data) {
            	var data = jQuery.parseJSON(data);
                console.log(data);

                jQuery("#breed_dad").val(data.cow_id+" "+data.name);

                if(data.granddad_name==null){
                	jQuery("#breed_dad_granddad").val(data.father_cow_id);
                }else{
                	jQuery("#breed_dad_granddad").val(data.granddad_name);
            	}

            	if(data.grandmom_name==null){
                	jQuery("#breed_dad_grandmom").val(data.mother_cow_id);
                }else{
                	jQuery("#breed_dad_grandmom").val(data.grandmom_name);
            	}

            	var breed_string = "";
            	breed_dad_array = data.breeds;

            	for(var i=0;i<data.breeds.length;i++){
            		breed_string+= data.breeds[i].percent+"%"+ data.breeds[i].breed+ ", ";
            	}
            	if(breed_string!=""){
            		breed_string = breed_string.substr(0, breed_string.length-2);
            	}
            	jQuery("#breed_dad_value").val(breed_string);

            }

        });
	}


	function searchMom(){
		var cow_id = jQuery("#mom_cow_id").val().split(" ");
		jQuery.ajax({
            url: "index.php?option=com_edairy&task=cow.searchCow",
            type: "GET",
            data: {
            	'cow_id': cow_id[0]
            },
            success: function (data) {
            	var data = jQuery.parseJSON(data);
                console.log(data);

                jQuery("#breed_mom").val(data.cow_id+" "+data.name);

                if(data.granddad_name==null){
                	jQuery("#breed_mom_granddad").val(data.father_cow_id);
                }else{
                	jQuery("#breed_mom_granddad").val(data.granddad_name);
            	}

            	if(data.grandmom_name==null){
                	jQuery("#breed_mom_grandmom").val(data.mother_cow_id);
                }else{
                	jQuery("#breed_mom_grandmom").val(data.grandmom_name);
            	}

            	var breed_string = "";
            	breed_mom_array = data.breeds;

            	for(var i=0;i<data.breeds.length;i++){
            		breed_string+= data.breeds[i].percent+"%"+ data.breeds[i].breed+ ", ";
            	}
            	if(breed_string!=""){
            		breed_string = breed_string.substr(0, breed_string.length-2);
            	}
            	jQuery("#breed_mom_value").val(breed_string);

            }

        });
	}

	function calculateBreed(){
		if(breed_dad_array.length==0){
			alert("กรุณาเลือกพ่อพันธ์");
			return false;
		}
		if(breed_mom_array.length==0){
			alert("กรุณาเลือกแม่พันธ์");
			return false;
		}

		var breed_calculate_array = new Array();

		for(var i=0;i<breed_dad_array.length;i++){
			breed_calculate_array[breed_dad_array[i].breed]=parseFloat(breed_dad_array[i].percent);
		}

		for(var i=0;i<breed_mom_array.length;i++){
			if(breed_calculate_array[breed_mom_array[i].breed]==null){
				breed_calculate_array[breed_mom_array[i].breed]=parseFloat(breed_mom_array[i].percent);
			}else{
				breed_calculate_array[breed_mom_array[i].breed]+=parseFloat(breed_mom_array[i].percent);
			}
		}

		for(var i=0;i<breed_calculate_array.length;i++){
			breed_calculate_array[breed_dad_array[i].breed]=parseFloat(breed_dad_array[i].percent);
		}

		target = breed_calculate_array;
		for (var k in target){
		    if (target.hasOwnProperty(k)) {
		         target[k]=target[k]/2;
		    }
		}
		breed_calculate_array = target;


		var breed_string = "";
		target = breed_calculate_array;
		for (var k in target){
		    if (target.hasOwnProperty(k)) {
		    	breed_string+=target[k]+"%"+k+", ";
		    }
		}

		if(breed_string!=""){
			breed_string = breed_string.substr(0, breed_string.length-2);
		}

		jQuery("input[name=calculated_breed]").val(breed_string);

		console.log(breed_calculate_array);


	}


	function saveBreed(){
		jQuery("#save-breed-btn").prop("disabled", true);
		jQuery.ajax({
            url: "index.php?option=com_edairy&task=cow.saveBreed",
            type: "POST",
            data: {
            	'cow_id': jQuery("#jform_cow_id").val(),
                'calculated_breed': jQuery("input[name=calculated_breed]").val()
            },
            success: function (data) {
            	var data = jQuery.parseJSON(data);
                if (data.save_result==true) {
                	alert("บันทึกสายพันธ์สำเร็จ");
                } else {
                    alert("ไม่สามารถบันทึกสายพันธ์ได้");

                }

                jQuery("#save-breed-btn").prop("disabled", false);


            }

        });
	}


	jQuery("#dad_cow_id").prop("disabled", true);
	jQuery("#mom_cow_id").prop("disabled", true);
	jQuery.get("index.php?option=com_edairy&task=cow.getAllCow", function(data){
		
		//console.log(data);
	  jQuery("#dad_cow_id").typeahead({ source:data });
	  jQuery("#mom_cow_id").typeahead({ source:data });

	  jQuery("#dad_cow_id").prop("disabled", false);
	jQuery("#mom_cow_id").prop("disabled", false);
	jQuery("#dad_cow_id").prop("placeholder", "Auto Complete");
	jQuery("#mom_cow_id").prop("placeholder", "Auto Complete");

	},'json');


	function saveMoveOut(){
		if(jQuery("input[name=move_out_move_out_date]").val()=="" ){
			alert("กรุณาใส่ข้อมูลให้ครบถ้วน");
			return false;
		}else{
			document.getElementById('MoveOutForm').submit();
		}
		
	}


	function getMoveOut(id){
		jQuery.ajax({
            url: "index.php?option=com_edairy&task=cow.getMoveOut",
            type: "POST",
            data: {
                'id': id
            },
            success: function (data) {
            	var data = jQuery.parseJSON(data);
            	console.log(data);

            	if(data!=null){

	            	var move_out_date = data.move_out_date.split("-");

	            	jQuery("input[name=move_out_move_out_date]").val(move_out_date[2] + "/" + move_out_date[1] + "/" + (parseInt(move_out_date[0])+543));

	            	jQuery("#radio_"+data.move_type).prop("checked", true)

	            	if(data.move_type=="1"){

	            		if(data.to_farm_id!=null){
	            			jQuery("select[name=move_out_to_farm_id1]").val(data.to_farm_id);
	            			jQuery("select[name=move_out_to_farm_id1]").trigger('liszt:updated');
	            		}
	            	}else if(data.move_type=="3"){
	            		if(data.to_farm_id!=null){
		            		jQuery("select[name=move_out_to_farm_id2]").val(data.to_farm_id);
		            		jQuery("select[name=move_out_to_farm_id2]").trigger('liszt:updated');
		            	}
		            		jQuery("select[name=move_out_move_objective]").val(data.move_objective);
		            		jQuery("select[name=move_out_move_objective]").trigger('liszt:updated');
	            			jQuery("select[name=move_out_move_reason]").val(data.move_reason);
	            			jQuery("select[name=move_out_move_reason]").trigger('liszt:updated');
		            	
	            	}

	            	

	            	

	            	jQuery("input[name=move_out_id]").val(data.id);
	            }

            }

        });
	}


	function deleteMoveOut(id){
		jQuery.ajax({
            url: "index.php?option=com_edairy&task=cow.deleteMoveOut",
            type: "POST",
            data: {
                'id': id
            },
            success: function (data) {
            	var data = jQuery.parseJSON(data);
                if (data.delete_result==true) {
                	alert("ลบข้อมูลสำเร็จ");
                } else {
                    alert("ไม่สามารถลบข้อมูลได้");

                }
                window.location = "index.php?option=com_edairy&view=cow&layout=edit&id=<?php echo $_REQUEST['id'];?>&default_tab=move_out";
            }

        });
	}
</script>