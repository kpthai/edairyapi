<?php 

// Include the main TCPDF library (search for installation path).
require_once('../media/com_edairy/tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('EDAIRY');
$pdf->SetTitle('EDAIRY');
$pdf->SetSubject('EDAIRY');
$pdf->SetKeywords('EDAIRY');

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
// set font

//$fontname = $pdf->addTTFfont('fonts/Browa.ttf', 'TrueTypeUnicode', '', 32);
$pdf->SetFont('thsarabunpsk', '', 14, '', true);



//PAGE 3 >> PAGE 1
$pdf->AddPage();
$pdf->setPageOrientation ('P', $autopagebreak='', $bottommargin='');
// get the current page break margin
$bMargin = $pdf->getBreakMargin();
// get current auto-page-break mode
$auto_page_break = $pdf->getAutoPageBreak();
// disable auto-page-break
$pdf->SetAutoPageBreak(true, 0);


function num2thai($number){
	$t1 = array("ศูนย์", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");
	$t2 = array("เอ็ด", "ยี่", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน");
	$zerobahtshow = 0; // ในกรณีที่มีแต่จำนวนสตางค์ เช่น 0.25 หรือ .75 จะให้แสดงคำว่า ศูนย์บาท หรือไม่ 0 = ไม่แสดง, 1 = แสดง
	(string) $number;
	$number = explode(".", $number);
	if(!empty($number[1])){
		if(strlen($number[1]) == 1){
			$number[1] .= "0";
		}else if(strlen($number[1]) > 2){
			if($number[1]{2} < 5){
				$number[1] = substr($number[1], 0, 2);
			}else{
				$number[1] = $number[1]{0}.($number[1]{1}+1);
			}
		}
	}
	
	for($i=0; $i<count($number); $i++){
		$countnum[$i] = strlen($number[$i]);
		if($countnum[$i] <= 7){
			$var[$i][] = $number[$i];
		}else{
			$loopround = ceil($countnum[$i]/6);
			for($j=1; $j<=$loopround; $j++){
				if($j == 1){
					$slen = 0;
					$elen = $countnum[$i]-(($loopround-1)*6);
				}else{
					$slen = $countnum[$i]-((($loopround+1)-$j)*6);
					$elen = 6;
				}
				$var[$i][] = substr($number[$i], $slen, $elen);
			}
		}	

		$nstring[$i] = "";
		for($k=0; $k<count($var[$i]); $k++){
			if($k > 0) $nstring[$i] .= $t2[7];
			$val = $var[$i][$k];
			$tnstring = "";
			$countval = strlen($val);
			for($l=7; $l>=2; $l--){
				if($countval >= $l){
					$v = substr($val, -$l, 1);
					if($v > 0){
						if($l == 2 && $v == 1){
							$tnstring .= $t2[($l)];
						}elseif($l == 2 && $v == 2){
							$tnstring .= $t2[1].$t2[($l)];
						}else{
							$tnstring .= $t1[$v].$t2[($l)];
						}
					}
				}
			}
			if($countval >= 1){
				$v = substr($val, -1, 1);
				if($v > 0){
					if($v == 1 && $countval > 1 && substr($val, -2, 1) > 0){
						$tnstring .= $t2[0];
					}else{
						$tnstring .= $t1[$v];
					}

				}
			}

			$nstring[$i] .= $tnstring;
		}

	}
	$rstring = "";
	if(!empty($nstring[0]) || $zerobahtshow == 1 || empty($nstring[1])){
		if($nstring[0] == "") $nstring[0] = $t1[0];
		$rstring .= $nstring[0]."บาท";
	}
	if(count($number) == 1 || empty($nstring[1])){
		$rstring .= "ถ้วน";
	}else{
		$rstring .= $nstring[1]."สตางค์";
	}
	return $rstring;
}
// set bacground image
/*
$heading_html = <<<EOD
<img src="images/header.png" />

EOD;

$pdf->writeHTMLCell($w=0, $h=0, $x='10', $y='5', $heading_html, $border=0, $ln=1, $fill=0, $reseth=true, $align='L', $autopadding=false);

*/

$create_date = formatDate(date("Y-m-d"));


// $html = <<<EOD
// วันที่ : {$create_date}

// EOD;


// //$money_word = num2thai($this->item->grand_total);

// // print a block of text using Write()
// $pdf->writeHTMLCell($w=0, $h=0, $x='10', $y='10', $html, $border=0, $ln=1, $fill=0, $reseth=true, $align='R', $autopadding=false);



// Line Processing
$insemination_time_map = array(1=>"เช้า", 2=>"เย็น");
$line_html = "";

for($i=0;$i<count($this->item->insemination_line);$i++){
	$index=$i+1;
$line_html .= <<<EOD
<tr>
		<td>{$index}</td>
		<td>{$this->item->insemination_line[$i]->cow->cow_id}</td>
		<td>{$this->item->insemination_line[$i]->cow->name}</td>
		<td></td>
		<td style="text-align:center">{$insemination_time_map[$this->item->insemination_line[$i]->insemination_time]}</td>
		<td>{$this->item->insemination_line[$i]->lot_no}</td>
		<td style="text-align:right">{$this->item->insemination_line[$i]->price}</td>
		<td style="text-align:center">{$this->item->insemination_line[$i]->amount}</td>
		<td style="text-align:right">{$this->item->insemination_line[$i]->total_price}</td>
	</tr>
EOD;
}

$birthdate = formatDate($this->item->birthdate);

$breed_string = "";
						if(count($this->item->breeds)>0){
							foreach($this->item->breeds as $bkey=>$bdata){
								$breed_string .= $bdata->percent . "%" . $bdata->breed . ", ";
							}
							$breed_string = substr($breed_string, 0, strlen($breed_string)-2);

							/*if(strlen($breed_string)>50){
								$breed_string = substr($breed_string, 0, 50) . "...";
							}*/
						}

$html = <<<EOD
<div style="text-align:center;font-size:20px">
			<img src="../images/dpo_logo.jpg" width="70"/><br />
			<b>
				บัตรประจำตัวโคนม<br />
				สมาชิกองค์การส่งเสริมกิจการโคนมแห่งประเทศไทย
			</b>

</div>
<br />
<table border="1" cellpadding="3">
	<tr>
		<th colspan="2" width="370"><b>ข้อมูลโค</b></th>
		<th width="290"><b>รูปพรรณ</b></th>
	</tr>
	<tr>
		<td>หมายเลขประจำตัวโค</td>
		<td>{$this->item->cow_id}</td>
		<td rowspan="6"></td>
	</tr>
	<tr>
		<td>ชื่อ</td>
		<td>{$this->item->name}</td>
	</tr>
	<tr>
		<td>วันเกิด</td>
		<td>{$birthdate}</td>
	</tr>
	<tr>
		<td>พันธ์</td>
		<td>{$breed_string}</td>
	</tr>
	<tr>
		<td>สถานที่เกิด</td>
		<td>-</td>
	</tr>
	<tr>
		<td>เจ้าของ</td>
		<td>{$this->item->farm->farmer_name}</td>
	</tr>
</table>
<br /><br />
<table border="1" cellpadding="3">
	<tr>
		<th colspan="4" width="660"><b>ข้อมูลสายพันธ์</b></th>
	</tr>
	<tr>
		<td rowspan="2" width="100">พ่อ</td>
		<td rowspan="2" width="230">{$this->item->father_cow_id}</td>
		<td width="100">ปู่</td>
		<td width="230">{$this->grandad->father_cow_id}</td>
	</tr>
	<tr>
		<td>ย่า</td>
		<td>{$this->granddad->father_cow_id}</td>
	</tr>
	<tr>
		<td rowspan="2" width="100">แม่</td>
		<td rowspan="2" width="230">{$this->item->mother_cow_id}</td>
		<td width="100">ตา</td>
		<td width="230">{$this->grandmom->father_cow_id}</td>
	</tr>
	<tr>
		<td>ยาย</td>
		<td>{$this->grandmom->mother_cow_id}</td>
	</tr>
</table>
<br /><br />
<table border="1" cellpadding="3">
	<tr>
		<th colspan="5" width="660"><b>รายการเปลี่ยนเจ้าของ</b></th>
	</tr>
	<tr>
		<th>ลำดับที่</th>
		<th>ชื่อเจ้าของ</th>
		<th>หมายเลขสมาชิก</th>
		<th>วันที่</th>
		<th>นายทะเบียน</th>
	</tr>
</table>
<br />
<br />
<br />
<br />
<br />
<table>
	<tr>
		<td width="170" align="center">
			วันที่ออกบัตร<br />{$create_date}
		</td>
		<td width="260" align="center">
			................................................<br />
			ผู้ออกบัตร
		</td>
		<td width="260" align="center">
			................................................<br />
			ผู้รับรอง
		</td>
	</tr>
</table>
EOD;




// print a block of text using Write()
$pdf->writeHTMLCell($w=0, $h=0, $x='10', $y='10', $html, $border=0, $ln=1, $fill=0, $reseth=true, $align='L', $autopadding=false);

 






// set bacground image

// if (isset($_POST['download'])) {
// 	$pdf->Output("detail_id{$_POST[id]}.pdf", 'D');
// }else{
	$pdf->Output("Invoice_{$_GET[id]}.pdf", 'I');
// }
die();
