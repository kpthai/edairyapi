<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_edairy/css/form.css');
?>
<script src="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript" charset="UTF-8"></script>

<link rel="stylesheet" href="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.css">
<style type="text/css">
	/* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #description {
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
      }

      #infowindow-content .title {
        font-weight: bold;
      }

      #infowindow-content {
        display: none;
      }

      #map #infowindow-content {
        display: inline;
      }

      .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
      }

      #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
      }

      .pac-controls {
        display: inline-block;
        padding: 5px 11px;
      }

      .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      #title {
        color: #fff;
        background-color: #4d90fe;
        font-size: 25px;
        font-weight: 500;
        padding: 6px 12px;
      }
      #target {
        width: 345px;
      }

      .tab-content > .tab-pane {
    display: block;
    height:0;
    overflow:hidden;
}

.tab-content > .active {
    display: block;
    height:auto;
}

.panel {
  margin-bottom: 20px;
  background-color: #fff;
  border: 1px solid transparent;
  border-radius: 4px;
  -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);
}
.panel-body {
  padding: 15px;
}
.panel-heading {
  padding: 10px 15px;
  border-bottom: 1px solid transparent;
  border-top-right-radius: 3px;
  border-top-left-radius: 3px;
}
.panel-heading > .dropdown .dropdown-toggle {
  color: inherit;
}
.panel-title {
  margin-top: 0;
  margin-bottom: 0;
  font-size: 16px;
  color: inherit;
}
.panel-title > a,
.panel-title > small,
.panel-title > .small,
.panel-title > small > a,
.panel-title > .small > a {
  color: inherit;
}
.panel-footer {
  padding: 10px 15px;
  background-color: #f5f5f5;
  border-top: 1px solid #ddd;
  border-bottom-right-radius: 3px;
  border-bottom-left-radius: 3px;
}
.panel > .list-group,
.panel > .panel-collapse > .list-group {
  margin-bottom: 0;
}
.panel > .list-group .list-group-item,
.panel > .panel-collapse > .list-group .list-group-item {
  border-width: 1px 0;
  border-radius: 0;
}
.panel > .list-group:first-child .list-group-item:first-child,
.panel > .panel-collapse > .list-group:first-child .list-group-item:first-child {
  border-top: 0;
  border-top-right-radius: 3px;
  border-top-left-radius: 3px;
}
.panel > .list-group:last-child .list-group-item:last-child,
.panel > .panel-collapse > .list-group:last-child .list-group-item:last-child {
  border-bottom: 0;
  border-bottom-right-radius: 3px;
  border-bottom-left-radius: 3px;
}
.panel > .panel-heading + .panel-collapse > .list-group .list-group-item:first-child {
  border-top-right-radius: 0;
  border-top-left-radius: 0;
}
.panel-heading + .list-group .list-group-item:first-child {
  border-top-width: 0;
}
.list-group + .panel-footer {
  border-top-width: 0;
}
.panel > .table,
.panel > .table-responsive > .table,
.panel > .panel-collapse > .table {
  margin-bottom: 0;
}
.panel > .table caption,
.panel > .table-responsive > .table caption,
.panel > .panel-collapse > .table caption {
  padding-left: 15px;
  padding-right: 15px;
}
.panel > .table:first-child,
.panel > .table-responsive:first-child > .table:first-child {
  border-top-right-radius: 3px;
  border-top-left-radius: 3px;
}
.panel > .table:first-child > thead:first-child > tr:first-child,
.panel > .table-responsive:first-child > .table:first-child > thead:first-child > tr:first-child,
.panel > .table:first-child > tbody:first-child > tr:first-child,
.panel > .table-responsive:first-child > .table:first-child > tbody:first-child > tr:first-child {
  border-top-left-radius: 3px;
  border-top-right-radius: 3px;
}
.panel > .table:first-child > thead:first-child > tr:first-child td:first-child,
.panel > .table-responsive:first-child > .table:first-child > thead:first-child > tr:first-child td:first-child,
.panel > .table:first-child > tbody:first-child > tr:first-child td:first-child,
.panel > .table-responsive:first-child > .table:first-child > tbody:first-child > tr:first-child td:first-child,
.panel > .table:first-child > thead:first-child > tr:first-child th:first-child,
.panel > .table-responsive:first-child > .table:first-child > thead:first-child > tr:first-child th:first-child,
.panel > .table:first-child > tbody:first-child > tr:first-child th:first-child,
.panel > .table-responsive:first-child > .table:first-child > tbody:first-child > tr:first-child th:first-child {
  border-top-left-radius: 3px;
}
.panel > .table:first-child > thead:first-child > tr:first-child td:last-child,
.panel > .table-responsive:first-child > .table:first-child > thead:first-child > tr:first-child td:last-child,
.panel > .table:first-child > tbody:first-child > tr:first-child td:last-child,
.panel > .table-responsive:first-child > .table:first-child > tbody:first-child > tr:first-child td:last-child,
.panel > .table:first-child > thead:first-child > tr:first-child th:last-child,
.panel > .table-responsive:first-child > .table:first-child > thead:first-child > tr:first-child th:last-child,
.panel > .table:first-child > tbody:first-child > tr:first-child th:last-child,
.panel > .table-responsive:first-child > .table:first-child > tbody:first-child > tr:first-child th:last-child {
  border-top-right-radius: 3px;
}
.panel > .table:last-child,
.panel > .table-responsive:last-child > .table:last-child {
  border-bottom-right-radius: 3px;
  border-bottom-left-radius: 3px;
}
.panel > .table:last-child > tbody:last-child > tr:last-child,
.panel > .table-responsive:last-child > .table:last-child > tbody:last-child > tr:last-child,
.panel > .table:last-child > tfoot:last-child > tr:last-child,
.panel > .table-responsive:last-child > .table:last-child > tfoot:last-child > tr:last-child {
  border-bottom-left-radius: 3px;
  border-bottom-right-radius: 3px;
}
.panel > .table:last-child > tbody:last-child > tr:last-child td:first-child,
.panel > .table-responsive:last-child > .table:last-child > tbody:last-child > tr:last-child td:first-child,
.panel > .table:last-child > tfoot:last-child > tr:last-child td:first-child,
.panel > .table-responsive:last-child > .table:last-child > tfoot:last-child > tr:last-child td:first-child,
.panel > .table:last-child > tbody:last-child > tr:last-child th:first-child,
.panel > .table-responsive:last-child > .table:last-child > tbody:last-child > tr:last-child th:first-child,
.panel > .table:last-child > tfoot:last-child > tr:last-child th:first-child,
.panel > .table-responsive:last-child > .table:last-child > tfoot:last-child > tr:last-child th:first-child {
  border-bottom-left-radius: 3px;
}
.panel > .table:last-child > tbody:last-child > tr:last-child td:last-child,
.panel > .table-responsive:last-child > .table:last-child > tbody:last-child > tr:last-child td:last-child,
.panel > .table:last-child > tfoot:last-child > tr:last-child td:last-child,
.panel > .table-responsive:last-child > .table:last-child > tfoot:last-child > tr:last-child td:last-child,
.panel > .table:last-child > tbody:last-child > tr:last-child th:last-child,
.panel > .table-responsive:last-child > .table:last-child > tbody:last-child > tr:last-child th:last-child,
.panel > .table:last-child > tfoot:last-child > tr:last-child th:last-child,
.panel > .table-responsive:last-child > .table:last-child > tfoot:last-child > tr:last-child th:last-child {
  border-bottom-right-radius: 3px;
}
.panel > .panel-body + .table,
.panel > .panel-body + .table-responsive,
.panel > .table + .panel-body,
.panel > .table-responsive + .panel-body {
  border-top: 1px solid #ddd;
}
.panel > .table > tbody:first-child > tr:first-child th,
.panel > .table > tbody:first-child > tr:first-child td {
  border-top: 0;
}
.panel > .table-bordered,
.panel > .table-responsive > .table-bordered {
  border: 0;
}
.panel > .table-bordered > thead > tr > th:first-child,
.panel > .table-responsive > .table-bordered > thead > tr > th:first-child,
.panel > .table-bordered > tbody > tr > th:first-child,
.panel > .table-responsive > .table-bordered > tbody > tr > th:first-child,
.panel > .table-bordered > tfoot > tr > th:first-child,
.panel > .table-responsive > .table-bordered > tfoot > tr > th:first-child,
.panel > .table-bordered > thead > tr > td:first-child,
.panel > .table-responsive > .table-bordered > thead > tr > td:first-child,
.panel > .table-bordered > tbody > tr > td:first-child,
.panel > .table-responsive > .table-bordered > tbody > tr > td:first-child,
.panel > .table-bordered > tfoot > tr > td:first-child,
.panel > .table-responsive > .table-bordered > tfoot > tr > td:first-child {
  border-left: 0;
}
.panel > .table-bordered > thead > tr > th:last-child,
.panel > .table-responsive > .table-bordered > thead > tr > th:last-child,
.panel > .table-bordered > tbody > tr > th:last-child,
.panel > .table-responsive > .table-bordered > tbody > tr > th:last-child,
.panel > .table-bordered > tfoot > tr > th:last-child,
.panel > .table-responsive > .table-bordered > tfoot > tr > th:last-child,
.panel > .table-bordered > thead > tr > td:last-child,
.panel > .table-responsive > .table-bordered > thead > tr > td:last-child,
.panel > .table-bordered > tbody > tr > td:last-child,
.panel > .table-responsive > .table-bordered > tbody > tr > td:last-child,
.panel > .table-bordered > tfoot > tr > td:last-child,
.panel > .table-responsive > .table-bordered > tfoot > tr > td:last-child {
  border-right: 0;
}
.panel > .table-bordered > thead > tr:first-child > td,
.panel > .table-responsive > .table-bordered > thead > tr:first-child > td,
.panel > .table-bordered > tbody > tr:first-child > td,
.panel > .table-responsive > .table-bordered > tbody > tr:first-child > td,
.panel > .table-bordered > thead > tr:first-child > th,
.panel > .table-responsive > .table-bordered > thead > tr:first-child > th,
.panel > .table-bordered > tbody > tr:first-child > th,
.panel > .table-responsive > .table-bordered > tbody > tr:first-child > th {
  border-bottom: 0;
}
.panel > .table-bordered > tbody > tr:last-child > td,
.panel > .table-responsive > .table-bordered > tbody > tr:last-child > td,
.panel > .table-bordered > tfoot > tr:last-child > td,
.panel > .table-responsive > .table-bordered > tfoot > tr:last-child > td,
.panel > .table-bordered > tbody > tr:last-child > th,
.panel > .table-responsive > .table-bordered > tbody > tr:last-child > th,
.panel > .table-bordered > tfoot > tr:last-child > th,
.panel > .table-responsive > .table-bordered > tfoot > tr:last-child > th {
  border-bottom: 0;
}
.panel > .table-responsive {
  border: 0;
  margin-bottom: 0;
}
.panel-group {
  margin-bottom: 20px;
}
.panel-group .panel {
  margin-bottom: 0;
  border-radius: 4px;
}
.panel-group .panel + .panel {
  margin-top: 5px;
}
.panel-group .panel-heading {
  border-bottom: 0;
}
.panel-group .panel-heading + .panel-collapse > .panel-body,
.panel-group .panel-heading + .panel-collapse > .list-group {
  border-top: 1px solid #ddd;
}
.panel-group .panel-footer {
  border-top: 0;
}
.panel-group .panel-footer + .panel-collapse .panel-body {
  border-bottom: 1px solid #ddd;
}
.panel-default {
  border-color: #ddd;
}
.panel-default > .panel-heading {
  color: #333333;
  background-color: #f5f5f5;
  border-color: #ddd;
}
.panel-default > .panel-heading + .panel-collapse > .panel-body {
  border-top-color: #ddd;
}
.panel-default > .panel-heading .badge {
  color: #f5f5f5;
  background-color: #333333;
}
.panel-default > .panel-footer + .panel-collapse > .panel-body {
  border-bottom-color: #ddd;
}
.panel-primary {
  border-color: #337ab7;
}
.panel-primary > .panel-heading {
  color: #fff;
  background-color: #337ab7;
  border-color: #337ab7;
}
.panel-primary > .panel-heading + .panel-collapse > .panel-body {
  border-top-color: #337ab7;
}
.panel-primary > .panel-heading .badge {
  color: #337ab7;
  background-color: #fff;
}
.panel-primary > .panel-footer + .panel-collapse > .panel-body {
  border-bottom-color: #337ab7;
}
.panel-success {
  border-color: #d6e9c6;
}
.panel-success > .panel-heading {
  color: #3c763d;
  background-color: #dff0d8;
  border-color: #d6e9c6;
}
.panel-success > .panel-heading + .panel-collapse > .panel-body {
  border-top-color: #d6e9c6;
}
.panel-success > .panel-heading .badge {
  color: #dff0d8;
  background-color: #3c763d;
}
.panel-success > .panel-footer + .panel-collapse > .panel-body {
  border-bottom-color: #d6e9c6;
}
.panel-info {
  border-color: #bce8f1;
}
.panel-info > .panel-heading {
  color: #31708f;
  background-color: #d9edf7;
  border-color: #bce8f1;
}
.panel-info > .panel-heading + .panel-collapse > .panel-body {
  border-top-color: #bce8f1;
}
.panel-info > .panel-heading .badge {
  color: #d9edf7;
  background-color: #31708f;
}
.panel-info > .panel-footer + .panel-collapse > .panel-body {
  border-bottom-color: #bce8f1;
}
.panel-warning {
  border-color: #faebcc;
}
.panel-warning > .panel-heading {
  color: #8a6d3b;
  background-color: #fcf8e3;
  border-color: #faebcc;
}
.panel-warning > .panel-heading + .panel-collapse > .panel-body {
  border-top-color: #faebcc;
}
.panel-warning > .panel-heading .badge {
  color: #fcf8e3;
  background-color: #8a6d3b;
}
.panel-warning > .panel-footer + .panel-collapse > .panel-body {
  border-bottom-color: #faebcc;
}
.panel-danger {
  border-color: #ebccd1;
}
.panel-danger > .panel-heading {
  color: #a94442;
  background-color: #f2dede;
  border-color: #ebccd1;
}
.panel-danger > .panel-heading + .panel-collapse > .panel-body {
  border-top-color: #ebccd1;
}
.panel-danger > .panel-heading .badge {
  color: #f2dede;
  background-color: #a94442;
}
.panel-danger > .panel-footer + .panel-collapse > .panel-body {
  border-bottom-color: #ebccd1;
}

</style>

<script src="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript" charset="UTF-8"></script>

<link rel="stylesheet" href="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.css">
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
    js('.datetimepicker').datetimepicker({
            lang:'th',
            timepicker:false,
            format:'d/m/Y'
        });

		js('#pac-input').bind('keypress', function(e)
{
   if(e.keyCode == 13)
   {
      return false;
   }
});
		
		js('.datetimepicker').datetimepicker({
            lang:'th',
            timepicker:false,
            format:'d/m/Y'
        });

	js('input:hidden.coop_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('coop_idhidden')){
			js('#jform_coop_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_coop_id").trigger("liszt:updated");
	js('input:hidden.province_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('province_idhidden')){
			js('#jform_province_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_province_id").trigger("liszt:updated");
	js('input:hidden.region_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('region_idhidden')){
			js('#jform_region_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_region_id").trigger("liszt:updated");
	js('input:hidden.district_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('district_idhidden')){
			js('#jform_district_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_district_id").trigger("liszt:updated");
	js('input:hidden.sub_district_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('sub_district_idhidden')){
			js('#jform_sub_district_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_sub_district_id").trigger("liszt:updated");
	js('input:hidden.farm_project_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('farm_project_idhidden')){
			js('#jform_farm_project_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_farm_project_id").trigger("liszt:updated");
	js('input:hidden.performance_improvement_project_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('performance_improvement_project_idhidden')){
			js('#jform_performance_improvement_project_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_performance_improvement_project_id").trigger("liszt:updated");
	js('input:hidden.milking_system_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('milking_system_idhidden')){
			js('#jform_milking_system_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_milking_system_id").trigger("liszt:updated");
	});

	Joomla.submitbutton = function (task) {
		if (task == 'farm.cancel') {
			Joomla.submitform(task, document.getElementById('farm-form'));
		}
		else {
			
			if (task != 'farm.cancel' && document.formvalidator.isValid(document.id('farm-form'))) {
				
				Joomla.submitform(task, document.getElementById('farm-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>
<?php
  if($this->item->survey_transaction->create_date!=null && $this->item->survey_transaction->create_date!=""){
    $survey_date = formatDate($this->item->survey_transaction->create_date);
  }else{
    $survey_date = formatDate(date("Y-m-d"));
  }
  
?>
<form
	action="<?php echo JRoute::_('index.php?option=com_edairy&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="farm-form" class="form-validate">

	<div class="form-horizontal">
		
		<h3>สำรวจสถานภาพโค</h3>

    <hr />


    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">ขั้นตอนที่ 1 กรอกข้อมูลเกษตรกร/ฟาร์ม</h3>
      </div>
      <div class="panel-body">
        วันที่สำรวจ <input type="text" value="<?php echo $survey_date; ?>" readonly />

        &nbsp;&nbsp;&nbsp;

        ฟาร์ม:  
        <select name="farm_id" onChange="javascript:window.location='index.php?option=com_edairy&view=farm&layout=farm_survey&farm_id='+this.value;"<?php if($_REQUEST['id']!=""){?> disabled<?php } ?>>
                <option value="">-- เลือกฟาร์ม --</option>
            <?php 
              $farmerInFarmId = 0;
              foreach($this->masterData->mas_farm as $bkey=>$bdata){ 
                  $selected = "";
                  if($this->item->survey_transaction->farm_id == $bdata->id || $_REQUEST["farm_id"] == $bdata->id){
                    $selected = " selected";
                    $farmerInFarmId = $bdata->id;
                  }
            ?>
                <option value="<?php echo $bdata->id;?>"<?php echo $selected; ?>><?php echo $bdata->member_code; ?> : <?php echo $bdata->name; ?></option>
            <?php } ?>
              </select>

        &nbsp;&nbsp;&nbsp;

        ชื่อ - รหัสเกษตรกร:  

        <input type="text" name="farmer_name" disabled />
      </div>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">ขั้นตอนที่ 2 ตรวจสอบสอบสภานภาพโค</h3>
      </div>
      <div class="panel-body">
        <?php 
        echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general'));
    ?>

    <?php if($_REQUEST["farm_id"]!=""){ ?>

    <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('รายชื่อโคที่ไม่ได้อัพเดทสถานภาพนานเกิน 2 เดือน', true)); ?>
        

        <table class="table table-striped table-bordered table-blue">
        <thead>
          <tr>
            <th style="text-align:center">ที่</th>
            <th style="text-align:center">ชื่อ</th>
            <th style="text-align:center">หมายเลข</th>
            <th style="text-align:center">หมายเลขหูตัด</th>
            <th style="text-align:center">สถานภาพ</th>
            <th style="text-align:center">เปลี่ยนสถานภาพ</th>
          </tr>
        </thead>
        <tbody>
          <?php
            $i=1;
            foreach($this->item->cow_2month as $key=>$data){ 
          ?>
            <tr>
              <td style="text-align: center"><?php echo $i++; ?></td>
              <td><?php echo $data->name; ?></td>
              <td><?php echo $data->cow_id; ?></td>
              <td><?php echo $data->ear_id; ?></td>
              <td><?php echo $data->cow_status; ?></td>
              <td style="text-align: center">
              <?php if($_REQUEST["id"]!=""){ ?>
                รายการนี้เป็นประวัติการเปลี่ยนสถานภาพ
              <?php }else{ ?>
                <div id="manage<?php echo $data->id;?>">
                  <input type="button" onClick="javascript:confirmCowStatus('<?php echo $data->id; ?>', '<?php echo $data->cow_status_id; ?>');jQuery(this).prop('disabled', true);" class="btn btn-success" value="ถูกต้อง" />
                  &nbsp;&nbsp;
                  <a data-toggle="modal" data-target="#CowStatusModal" onClick="javascript:setCowData('<?php echo $data->id; ?>');" class="btn btn-danger"> ไม่ถูกต้อง</a>
                </div>
              <?php } ?>
              </td>
            </tr>
          <?php } ?>
          <?php if(count($this->item->cow_2month)==0){ ?>
            <tr>
              <td colspan="6" style="text-align: center">ไม่พบรายชื่อโคที่ไม่ได้อัพเดทสถานภาพนานเกิน 2 เดือน</td>
            </tr>
          <?php } ?>
        </tbody>
      </table>


    <?php echo JHtml::_('bootstrap.endTab'); ?>


    <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'farmer', JText::_('รายชื่อโคทั้งหมดในฟาร์ม', true)); ?>
      
      <table class="table table-striped table-bordered table-blue">
        <thead>
          <tr>
            <th style="text-align:center">ที่</th>
            <th style="text-align:center">ชื่อ</th>
            <th style="text-align:center">หมายเลข</th>
            <th style="text-align:center">หมายเลขหูตัด</th>
            <th style="text-align:center">สถานภาพ</th>
          </tr>
        </thead>
        <tbody>
          <?php
            $i=1;
            foreach($this->item->cow as $key=>$data){ 
          ?>
            <tr>
              <td style="text-align: center"><?php echo $i++; ?></td>
              <td><?php echo $data->name; ?></td>
              <td><?php echo $data->cow_id; ?></td>
              <td><?php echo $data->ear_id; ?></td>
              <td><?php echo $data->cow_status; ?></td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    <?php echo JHtml::_('bootstrap.endTab'); ?>

    <?php }else if($_REQUEST["id"]!=""){ ?>

    <h4>ประวัติการเปลี่ยนสภานภาพ</h4>
      
      <table class="table table-striped table-bordered table-blue">
        <thead>
          <tr>
            <th style="text-align:center">ที่</th>
            <th style="text-align:center">ชื่อ</th>
            <th style="text-align:center">หมายเลข</th>
            <th style="text-align:center">หมายเลขหูตัด</th>
            <th style="text-align:center">สถานภาพ</th>
          </tr>
        </thead>
        <tbody>
          <?php
            $i=1;
            foreach($this->item->cow_2month as $key=>$data){ 
          ?>
            <tr>
              <td style="text-align: center"><?php echo $i++; ?></td>
              <td><?php echo $data->name; ?></td>
              <td><?php echo $data->cow_id; ?></td>
              <td><?php echo $data->ear_id; ?></td>
              <td><?php echo $data->cow_status; ?></td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    <?php } ?>

    <?php echo JHtml::_('bootstrap.endTabSet'); ?>

      </div>
    </div>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>






<!-- Modal -->
<div id="CowStatusModal" class="modal fade" role="dialog" style="display:none">
<form id="CowStatusForm" action="" method="post">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">เปลี่ยนสถานะภาพ</h4>
      </div>
      <div class="modal-body">
        <table>
        	<tr>
        		<td width="120">สภานภาพ*</td>
        		<td>
        			<select name="cow_status_id" style="width:500px">
        				<option value="">-- เลือกสถานภาพ --</option>
					 	<?php 
							foreach($this->masterData->mas_cow_status as $bkey=>$bdata){ 
						?>
								<option value="<?php echo $bdata->id;?>"><?php echo $bdata->name; ?> : <?php echo $bdata->description; ?></option>
						<?php } ?>
        			</select>
        		</td>
        	</tr>
        </table>
        <input type="hidden" name="cow_id" value="" />
      </div>
      <div class="modal-footer" style="text-align:left">
      	<button type="button" class="btn btn-info" style="width:100px" onClick="javascript:confirmCowStatus(jQuery('input[name=cow_id]').val(), jQuery('select[name=cow_status_id]').val());" data-dismiss="modal">บันทึก</button>&nbsp;&nbsp;
        <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
      </div>
    </div>

  </div>
  </form>
</div>

<script type="text/javascript">

	function getFarmerInFarm(id){
		jQuery.ajax({
            url: "index.php?option=com_edairy&task=farm.getFarmerInFarm",
            type: "POST",
            data: {
                'farm_id': id
            },
            success: function (data) {
            	var data = jQuery.parseJSON(data);
            	console.log(data);

            	if(data!=null){
	            	jQuery("input[name=farmer_name]").val(data.citizen_id+" : "+data.name+" "+data.surname);
	            }

            }

        });
	}

  <?php
    if($farmerInFarmId!=0){
      ?>
      getFarmerInFarm(<?php echo $farmerInFarmId; ?>);
      <?php
    }
  ?>
	function addGAP(){
		if(jQuery("input[name=gap_certify_no]").val()=="" || jQuery("input[name=gap_certify_date]").val()=="" || jQuery("input[name=gap_expire_date]").val()=="" || jQuery("input[name=gap_certify_agency]").val()==""){
			alert("กรุณาใส่ข้อมูลให้ครบถ้วน");
			return false;
		}else{
			document.getElementById('GAPForm').submit();
		}
		
	}
	function confirmCowStatus(cow_id, cow_status_id){
		jQuery.ajax({
            url: "index.php?option=com_edairy&task=farm.confirmCowStatus",
            type: "POST",
            data: {
                'cow_id': cow_id,
                'cow_status_id': cow_status_id,
                'farm_id': '<?php echo $_REQUEST["farm_id"]; ?>'
            },
            success: function (data) {
            	var data = jQuery.parseJSON(data);
                if (data.confirm_result==true) {
                	alert("ยืนยันสถานะโคสำเร็จ");
                  jQuery("#manage"+cow_id).html("เปลี่ยนสถานภาพแล้ว");
                } else {
                    alert("ไม่สามารถยืนยันสถานะโคได้");
                    window.location = "index.php?option=com_edairy&view=farm&layout=farm_survey&farm_id=<?php echo $_REQUEST['farm_id'];?>";

                }
                <?php if($_REQUEST["id"]!=""){ ?>
                  window.location = "index.php?option=com_edairy&view=farm&layout=farm_survey&id=<?php echo $_REQUEST['id'];?>";
                <?php } ?>
            }

        });
	}


	function getTraining(id){
		jQuery.ajax({
            url: "index.php?option=com_edairy&task=farm.getTraining",
            type: "POST",
            data: {
                'training_id': id
            },
            success: function (data) {
            	var data = jQuery.parseJSON(data);
            	console.log(data);

            	if(data!=null){

	            	var training_date = data.training_date.split("-");

	            	jQuery("input[name=training_training_date]").val(training_date[2] + "/" + training_date[1] + "/" + (parseInt(training_date[0])+543));

	            	jQuery("select[name=training_training_program_id]").val(data.training_program_id);

	            	jQuery("select[name=training_training_program_id]").trigger('liszt:updated');

	            	jQuery("input[name=training_training_day]").val(data.training_day);

	            	jQuery("input[name=training_training_place]").val(data.training_place);
	            	
	            	jQuery("input[name=training_farmer_id]").val(data.farmer_id);

	            	jQuery("input[name=training_id]").val(data.id);
	            }

            }

        });
	}
	function addTraining(){
		if(jQuery("input[name=training_training_date]").val()=="" || jQuery("select[name=training_training_program_id]").val()=="" || jQuery("input[name=training_training_day]").val()=="" ){
			alert("กรุณาใส่ข้อมูลให้ครบถ้วน");
			return false;
		}else{
			document.getElementById('TrainingForm').submit();
		}
		
	}
	function deleteTraining(id){
		jQuery.ajax({
            url: "index.php?option=com_edairy&task=farm.deleteTraining",
            type: "POST",
            data: {
                'training_id': id
            },
            success: function (data) {
            	var data = jQuery.parseJSON(data);
                if (data.delete_result==true) {
                	alert("ลบข้อมูลสำเร็จ");
                } else {
                    alert("ไม่สามารถลบข้อมูลได้");

                }
                window.location = "index.php?option=com_edairy&view=farm&layout=edit&id=<?php echo $_REQUEST['id'];?>&default_tab=farmer";
            }

        });
	}

	function clearTrainingForm(){
		jQuery("#TrainingForm input").val("");
	}

	function saveFarmer(){
		if(jQuery("input[name=farmer_citizen_id]").val()==""){
			alert("กรุณาใส่ข้อมูลให้ครบถ้วน");
			return false;
		}else{
			document.getElementById('FarmerForm').submit();
		}
		
	}


	function setCowData(id){
		
	     

	    jQuery("input[name=cow_id]").val(id);
	           
	}

	function addOwner(){
		if(jQuery("select[name=owner_farmer_id]").val()=="" || jQuery("input[name=owner_own_date]").val()=="" ){
			alert("กรุณาใส่ข้อมูลให้ครบถ้วน");
			return false;
		}else{
			document.getElementById('CowStatusForm').submit();
		}
		
	}



	function deleteOwner(id){
		jQuery.ajax({
            url: "index.php?option=com_edairy&task=farm.deleteOwner",
            type: "POST",
            data: {
                'owner_id': id
            },
            success: function (data) {
            	var data = jQuery.parseJSON(data);
                if (data.delete_result==true) {
                	alert("ลบข้อมูลสำเร็จ");
                } else {
                    alert("ไม่สามารถลบข้อมูลได้");

                }
                window.location = "index.php?option=com_edairy&view=farm&layout=edit&id=<?php echo $_REQUEST['id'];?>&default_tab=farmer";
            }

        });
	}

	function clearCowStatusForm(){
		jQuery("#CowStatusForm input[type!=hidden]").val("");
		jQuery("#CowStatusForm select").val("");
		jQuery("#CowStatusForm select").trigger("liszt:updated");
	}




</script>

