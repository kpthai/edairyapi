<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_edairy/css/form.css');
?>
<style type="text/css">
	/* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #description {
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
      }

      #infowindow-content .title {
        font-weight: bold;
      }

      #infowindow-content {
        display: none;
      }

      #map #infowindow-content {
        display: inline;
      }

      .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
      }

      #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
      }

      .pac-controls {
        display: inline-block;
        padding: 5px 11px;
      }

      .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      #title {
        color: #fff;
        background-color: #4d90fe;
        font-size: 25px;
        font-weight: 500;
        padding: 6px 12px;
      }
      #target {
        width: 345px;
      }

      .tab-content > .tab-pane {
    display: block;
    height:0;
    overflow:hidden;
}

.tab-content > .active {
    display: block;
    height:auto;
}
</style>

<script src="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript" charset="UTF-8"></script>

<link rel="stylesheet" href="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.css">
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		


		js('#pac-input').bind('keypress', function(e)
{
   if(e.keyCode == 13)
   {
      return false;
   }
});
		
		js('.datetimepicker').datetimepicker({
            lang:'th',
            timepicker:false,
            format:'d/m/Y'
        });

	js('input:hidden.coop_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('coop_idhidden')){
			js('#jform_coop_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_coop_id").trigger("liszt:updated");
	js('input:hidden.province_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('province_idhidden')){
			js('#jform_province_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_province_id").trigger("liszt:updated");
	js('input:hidden.region_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('region_idhidden')){
			js('#jform_region_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_region_id").trigger("liszt:updated");
	js('input:hidden.district_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('district_idhidden')){
			js('#jform_district_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_district_id").trigger("liszt:updated");
	js('input:hidden.sub_district_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('sub_district_idhidden')){
			js('#jform_sub_district_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_sub_district_id").trigger("liszt:updated");
	js('input:hidden.farm_project_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('farm_project_idhidden')){
			js('#jform_farm_project_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_farm_project_id").trigger("liszt:updated");
	js('input:hidden.performance_improvement_project_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('performance_improvement_project_idhidden')){
			js('#jform_performance_improvement_project_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_performance_improvement_project_id").trigger("liszt:updated");
	js('input:hidden.milking_system_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('milking_system_idhidden')){
			js('#jform_milking_system_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_milking_system_id").trigger("liszt:updated");
	});

	Joomla.submitbutton = function (task) {
		if (task == 'farm.cancel') {
			Joomla.submitform(task, document.getElementById('farm-form'));
		}
		else {
			
			if (task != 'farm.cancel' && document.formvalidator.isValid(document.id('farm-form'))) {
				
				Joomla.submitform(task, document.getElementById('farm-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_edairy&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="farm-form" class="form-validate">

	<div class="form-horizontal">
		<?php 
			if($_REQUEST["default_tab"]==""){
				echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general'));
			}else{
				echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => $_REQUEST["default_tab"]));
			} 
		?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('ข้อมูลฟาร์ม', true)); ?>
		<div class="row-fluid">
			<div class="span5 form-horizontal">
				<fieldset class="adminform">

									<div style="display:none">
										<?php echo $this->form->renderField('id'); ?>
									</div>
				<?php echo $this->form->renderField('member_code'); ?>
				<?php echo $this->form->renderField('name'); ?>
				<?php echo $this->form->renderField('coop_id'); ?>

			<?php
				foreach((array)$this->item->coop_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="coop_id" name="jform[coop_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('address'); ?>


			<?php echo $this->form->renderField('region_id'); ?>

			<?php
				foreach((array)$this->item->region_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="region_id" name="jform[region_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>	

				<?php echo $this->form->renderField('province_id'); ?>


			<?php
				foreach((array)$this->item->province_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="province_id" name="jform[province_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>							<?php echo $this->form->renderField('district_id'); ?>

			<?php
				foreach((array)$this->item->district_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="district_id" name="jform[district_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('sub_district_id'); ?>

			<?php
				foreach((array)$this->item->sub_district_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="sub_district_id" name="jform[sub_district_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('post_code'); ?>
				<?php echo $this->form->renderField('latitude'); ?>
				<?php echo $this->form->renderField('longitude'); ?>
				
				<hr />
			<h4>โปรดเลือกพิกัดตำแหน่งที่ตั้ง</h4>
				<input id="pac-input" class="controls" type="text" placeholder="พิมพ์ชื่อสถานที่ ที่นี่" style="margin-top:10px">
				<div id="map" style="height:300px;width:100%"></div>
				</fieldset>
			</div>



			<div class="span5 form-horizontal">
				<fieldset class="adminform">
					<?php echo $this->form->renderField('farm_project_id'); ?>

				<?php
					foreach((array)$this->item->farm_project_id as $value): 
						if(!is_array($value)):
							echo '<input type="hidden" class="farm_project_id" name="jform[farm_project_idhidden]['.$value.']" value="'.$value.'" />';
						endif;
					endforeach;
				?>				<?php echo $this->form->renderField('status'); ?>
					<?php echo $this->form->renderField('is_join_breeding_system'); ?>
					<?php echo $this->form->renderField('farm_condition_id'); ?>
					<?php echo $this->form->renderField('environment'); ?>
					<?php echo $this->form->renderField('performance_improvement_project_id'); ?>

				<?php
					foreach((array)$this->item->performance_improvement_project_id as $value): 
						if(!is_array($value)):
							echo '<input type="hidden" class="performance_improvement_project_id" name="jform[performance_improvement_project_idhidden]['.$value.']" value="'.$value.'" />';
						endif;
					endforeach;
				?>				<?php echo $this->form->renderField('milking_system_id'); ?>

				<?php
					foreach((array)$this->item->milking_system_id as $value): 
						if(!is_array($value)):
							echo '<input type="hidden" class="milking_system_id" name="jform[milking_system_idhidden]['.$value.']" value="'.$value.'" />';
						endif;
					endforeach;
			?>				<?php echo $this->form->renderField('waste_disposal_type'); ?>
				<?php echo $this->form->renderField('member_start_date'); ?>
				<?php echo $this->form->renderField('breeding_start_date'); ?>
				<div style="display:none">
					<?php echo $this->form->renderField('state'); ?>
				</div>
				
				<?php echo $this->form->renderField('created_by'); ?>


					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>


				</fieldset>
			</div>


		</div>

		<?php echo JHtml::_('bootstrap.endTab'); ?>


		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'farmer', JText::_('ข้อมูลเกษตรกร', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<?php if($this->item->id==""){ ?>
					<div class="alert alert-info">กรุณาบันทึกข้อมูลฟาร์มก่อน</div>
				<?php } ?>
				<fieldset class="adminform" <?php if($this->item->id==""){ echo "style='display:none'";}?>>
					<div style="float:right">
					<?php if($this->item->farmer->id==""){ echo "(กรุณาบันทึกข้อมูลเกษตรกรก่อน) ";}?>

						<button class="btn btn-info" data-toggle="modal" data-target="#OwnerModal" <?php if($this->item->farmer->id==""){ echo "disabled";}?> onClick="javascript:clearOwnerForm();">เปลี่ยนเจ้าของฟาร์ม</button>
					</div>
					<h3>ประวัติเจ้าของฟาร์ม</h3>
					<table class="table table-striped table-bordered table-blue">
						<thead>
							<tr>
								<th style="text-align:center">ที่</th>
								<th style="text-align:center">เลขบัตรประชาชน</th>
								<th style="text-align:center">ชื่อ-สกุล</th>
								<th style="text-align:center">วันที่เริ่มเป็นเจ้าของ</th>
								<th style="text-align:center">แก้ไข</th>
								<th style="text-align:center">ลบ</th>
							</tr>
						</thead>
						<tbody>
							<?php if(count($this->item->owner)==0){ ?>
								<tr>
									<td style="text-align:center" colspan="6">ไม่พบข้อมูล</td>
								</tr>
							<?php }else{ ?>
								<?php $i=0;
									foreach($this->item->owner as $key=>$data){ ?>
									<tr>
										<td><?php echo ++$i; ?></td>
										<td><?php echo $data->citizen_id; ?></td>
										<td><?php echo $data->name; ?> <?php echo $data->surname; ?></td>
										<td><?php echo formatDate($data->own_date); ?></td>
										<td style="text-align: center"><button type="button" class="btn btn-warning" data-toggle="modal" data-target="#OwnerModal" onClick="javascript:getOwner(<?php echo $data->id; ?>);">แก้ไข</button></td>
										<td style="text-align: center"><button type="button" class="btn btn-danger" onClick="javascript: if(confirm('ยืนยันการลบ?')){jQuery(this).prop('disabled', true);deleteOwner(<?php echo $data->id; ?>);}">ลบ</button></td>
									</tr>
								<?php } ?>
							<?php } ?>
						</tbody>
					</table>


					<input type="hidden" name="farmer_id" value="<?php echo $this->item->farmer->id; ?>" />

						<div class="span2 form-horizontal">
							<h4>รูปเกษตรกร</h4>
							<?php 
							if($this->item->farmer->image_url!=""){ ?>
								<img src="../upload/<?php echo $this->item->farmer->image_url?>" width="200" />
							<?php } ?>
							<input name="upload_file" type="file" />
						</div>


						<div class="span5 form-horizontal">
							<h4>ข้อมูลทั่วไป</h4>

							<div class="control-group">
								<div class="control-label">
									<label>
										เลขบัตรประชาชน
									</label>
								</div>
								<div class="controls">
									<input type="text" name="farmer_citizen_id" value="<?php echo $this->item->farmer->citizen_id; ?>"/>
								</div>
							</div>


							<div class="control-group">
								<div class="control-label">
									<label>
										คำนำหน้าชื่อ
									</label>
								</div>
								<div class="controls">
									<select name="farmer_title_id">
										<option value="">-- เลือกคำนำหน้า --</option>
										<option value="1" <?php if($this->item->farmer->title_id==1){ echo " selected";} ?>>นาย</option>
										<option value="2" <?php if($this->item->farmer->title_id==2){ echo " selected";} ?>>นางสาว</option>
										<option value="3" <?php if($this->item->farmer->title_id==3){ echo " selected";} ?>>นาง</option>
									</select>
								</div>
							</div>


							<div class="control-group">
								<div class="control-label">
									<label>
										ชื่อ
									</label>
								</div>
								<div class="controls">
									<input type="text" name="farmer_name"  value="<?php echo $this->item->farmer->name; ?>"/>
								</div>
							</div>

							<div class="control-group">
								<div class="control-label">
									<label>
										นามสกุล
									</label>
								</div>
								<div class="controls">
									<input type="text" name="farmer_surname"  value="<?php echo $this->item->farmer->surname; ?>"/>
								</div>
							</div>


							<div class="control-group">
								<div class="control-label">
									<label>
										วันเกิด
									</label>
								</div>
								<div class="controls">
									<input type="text" name="farmer_birthdate" class="datetimepicker"  value="<?php echo formatDate($this->item->farmer->birthdate); ?>" onChange="javascript: calculateFarmerAge(this.value);"/>
								</div>
							</div>

							<div class="control-group">
								<div class="control-label">
									<label>
										อายุ
									</label>
								</div>
								<div class="controls">
									<input type="text" id="farmer_age" name="farmer_age" readonly /> ปี
								</div>
							</div>


							<div class="control-group">
								<div class="control-label">
									<label>
										การศึกษา
									</label>
								</div>
								<div class="controls">
									<select name="farmer_education_level_id">
									</select>
								</div>
							</div>

							<div class="control-group">
									<div class="control-label">
										<label>
											เบอร์โทรศัพท์
										</label>
									</div>
									<div class="controls">
										<input type="text" name="farmer_mobile" value="<?php echo $this->item->farmer->mobile; ?>"/>
									</div>
							</div>

							

						</div>

						<div class="span5 form-horizontal">
							<div class="control-group">
								<div class="control-label">
									<label>
										ที่อยู่
									</label>
								</div>
								<div class="controls">
									<textarea name="farmer_address"><?php echo $this->item->farmer->address; ?></textarea>
								</div>
							</div>
						


							<div class="control-group">
									<div class="control-label">
										<label>
											ภาค
										</label>
									</div>
									<div class="controls">
										<select name="farmer_region_id">
											<option value="">-- เลือกภาค --</option>
										 	<?php 
												foreach($this->masterData->mas_region as $bkey=>$bdata){
													$selected = "";
													if($this->item->farmer->region_id == $bdata->id){
														$selected = " selected";
													} 
											?>
													<option value="<?php echo $bdata->id;?>"<?php echo $selected; ?>><?php echo $bdata->name; ?></option>
											<?php } ?>
										</select>
									</div>
							</div>



							<div class="control-group">
									<div class="control-label">
										<label>
											จังหวัด
										</label>
									</div>
									<div class="controls">
										<select name="farmer_province_id">
											<option value="">-- เลือกจังหวัด --</option>
										 	<?php 
												foreach($this->masterData->mas_province as $bkey=>$bdata){
													$selected = "";
													if($this->item->farmer->province_id == $bdata->id){
														$selected = " selected";
													}  
											?>
													<option value="<?php echo $bdata->id;?>"<?php echo $selected; ?>><?php echo $bdata->name; ?></option>
											<?php } ?>
										</select>
									</div>
							</div>


							<div class="control-group">
									<div class="control-label">
										<label>
											อำเภอ
										</label>
									</div>
									<div class="controls">
										<select name="farmer_district_id">
											<option value="">-- เลือกอำเภอ --</option>
										 	<?php 
												foreach($this->masterData->mas_district as $bkey=>$bdata){
													$selected = "";
													if($this->item->farmer->district_id == $bdata->id){
														$selected = " selected";
													}  
											?>
													<option value="<?php echo $bdata->id;?>"<?php echo $selected; ?>><?php echo $bdata->name; ?></option>
											<?php } ?>
										</select>
									</div>
							</div>


							<div class="control-group">
									<div class="control-label">
										<label>
											ตำบล
										</label>
									</div>
									<div class="controls">
										<select name="farmer_sub_district_id">
											<option value="">-- เลือกตำบล --</option>
										 	<?php 
												foreach($this->masterData->mas_sub_district as $bkey=>$bdata){ 
													$selected = "";
													if($this->item->farmer->sub_district_id == $bdata->id){
														$selected = " selected";
													}  
											?>
													<option value="<?php echo $bdata->id;?>"<?php echo $selected; ?>><?php echo $bdata->name; ?></option>
											<?php } ?>
										</select>
									</div>
							</div>


							<div class="control-group">
									<div class="control-label">
										<label>
											รหัสไปรษณีย์
										</label>
									</div>
									<div class="controls">
										<input type="text" name="farmer_post_code" value="<?php echo $this->item->farmer->post_code; ?>"/>
									</div>
							</div>


							<div class="control-group" style="display:none">
									<div class="control-label">
										<label>
											ละติจูด
										</label>
									</div>
									<div class="controls">
										<input type="text" id="farmer_latitude" name="farmer_latitude" value="<?php echo $this->item->farmer->latitude; ?>"/>
									</div>
							</div>


							<div class="control-group" style="display:none">
									<div class="control-label">
										<label>
											ลองจิจูด
										</label>
									</div>
									<div class="controls">
										<input type="text" id="farmer_longitude" name="farmer_longitude" value="<?php echo $this->item->farmer->longitude; ?>"/>
									</div>
							</div>


							<div class="control-group">
									<div class="control-label">
										<label>
											แฟกซ์
										</label>
									</div>
									<div class="controls">
										<input type="text" name="farmer_fax" value="<?php echo $this->item->farmer->fax; ?>"/>
									</div>
							</div>

							<div class="control-group">
									<div class="control-label">
										<label>
											Line ID
										</label>
									</div>
									<div class="controls">
										<input type="text" name="farmer_line_id" value="<?php echo $this->item->farmer->line_id; ?>"/>
									</div>
							</div>

							<div class="control-group">
									<div class="control-label">
										<label>
											อีเมล
										</label>
									</div>
									<div class="controls">
										<input type="text" name="farmer_email" value="<?php echo $this->item->farmer->email; ?>"/>
									</div>
							</div>

						</div>

					

					<div style="clear:both"></div>

					
			
					<hr />

					<div style="float:right">
						<?php if($this->item->farmer->id==""){ echo "(กรุณาบันทึกข้อมูลเกษตรกรก่อน) ";}?>
						<button class="btn btn-info" data-toggle="modal" data-target="#TrainingModal" <?php if($this->item->farmer->id==""){ echo "disabled ";}?>  onClick="javascript:clearTrainingForm();">เพิ่มรายการ</button>
					</div>
					<h3>ประวัติการฝึกอบรม</h3>
					<table class="table table-striped table-bordered table-blue">
						<thead>
							<tr>
								<th style="text-align:center">ที่</th>
								<th style="text-align:center">ชื่อหลักสูตร</th>
								<th style="text-align:center">วันที่</th>
								<th style="text-align:center">สถานที่ฝึกอบรม</th>
								<th style="text-align:center">จำนวนวัน</th>
								<th style="text-align:center">แก้ไข</th>
								<th style="text-align:center">ลบ</th>
							</tr>
						</thead>
						<tbody>
							<?php if(count($this->item->training)==0){ ?>
								<tr>
									<td style="text-align:center" colspan="7">ไม่พบข้อมูล</td>
								</tr>
							<?php }else{ ?>
								<?php $i=0;
									foreach($this->item->training as $key=>$data){ ?>
									<tr>
										<td><?php echo ++$i; ?></td>
										<td><?php echo $data->training_program; ?></td>
										<td><?php echo formatDate($data->training_date); ?></td>
										<td><?php echo $data->training_place; ?></td>
										<td><?php echo $data->training_day; ?></td>
										<td style="text-align: center"><button type="button" class="btn btn-warning" data-toggle="modal" data-target="#TrainingModal" onClick="javascript:getTraining(<?php echo $data->id; ?>);">แก้ไข</button></td>
										<td style="text-align: center"><button type="button" class="btn btn-danger" onClick="javascript: if(confirm('ยืนยันการลบ?')){jQuery(this).prop('disabled', true);deleteTraining(<?php echo $data->id; ?>);}">ลบ</button></td>
									</tr>
								<?php } ?>
							<?php } ?>
						</tbody>
					</table>
									
				</fieldset>

			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>



		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'gap', JText::_('มาตรฐาน GAP', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<?php if($this->item->id==""){ ?>
					<div class="alert alert-info">กรุณาบันทึกข้อมูลฟาร์มก่อน</div>
				<?php } ?>
				<fieldset class="adminform" <?php if($this->item->id==""){ echo "style='display:none'";}?>>
					<div style="float:right">
						<button class="btn btn-info" data-toggle="modal" data-target="#GAPModal" onClick="javascript:clearGAPForm();">เพิ่มรายการ</button>
					</div>
					<h3>ประวัติการรับรองมาตรฐาน GAP</h3>
					<table class="table table-striped table-bordered table-blue">
						<thead>
							<tr>
								<th style="text-align:center">ที่</th>
								<th style="text-align:center">หมายเลขการรับรอง</th>
								<th style="text-align:center">วันที่ได้รับการรับรอง</th>
								<th style="text-align:center">วันที่หมดอายุ</th>
								<th style="text-align:center">หน่วยงานรับรอง</th>
								<th style="text-align:center">ปรับปรุงข้อมูลล่าสุดโดย</th>
								<th style="text-align:center">วันที่ปรับปรุง</th>
								<th style="text-align:center">แก้ไข</th>
								<th style="text-align:center">ลบ</th>
							</tr>
						</thead>
						<tbody>
							<?php if(count($this->item->gap)==0){ ?>
								<tr>
									<td style="text-align:center" colspan="9">ไม่พบข้อมูล</td>
								</tr>
							<?php }else{ ?>
								<?php $i=0;
									foreach($this->item->gap as $key=>$data){ ?>
									<tr>
										<td><?php echo ++$i; ?></td>
										<td><?php echo $data->certify_no; ?></td>
										<td><?php echo formatDate($data->certify_date); ?></td>
										<td><?php echo formatDate($data->expire_date); ?></td>
										<td><?php echo $data->certify_agency; ?></td>
										<td><?php 
												$user = JFactory::getUser($data->update_by);
												echo $user->name; 
											?>
											
										</td>
										<td><?php echo formatDate($data->update_datetime); ?></td>
										<td style="text-align: center"><button type="button" class="btn btn-warning" data-toggle="modal" data-target="#GAPModal" onClick="javascript:getGAP(<?php echo $data->id; ?>);">แก้ไข</button></td>
										<td style="text-align: center"><button type="button" class="btn btn-danger" onClick="javascript: if(confirm('ยืนยันการลบ?')){jQuery(this).prop('disabled', true);deleteGAP(<?php echo $data->id; ?>);}">ลบ</button></td>
									</tr>
								<?php } ?>
							<?php } ?>
						</tbody>
					</table>
									
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>


<!-- Modal -->
<div id="GAPModal" class="modal fade" role="dialog" style="display:none">
<form id="GAPForm" action="" method="post">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">การรับรองมาตรฐาน GAP</h4>
      </div>
      <div class="modal-body">
        <table>
        	<tr>
        		<td width="120">หมายเลขการรับรอง*</td>
        		<td><input type="text" name="gap_certify_no" /></td>
        	</tr>
        	<tr>
        		<td>วันที่ได้รับการรับรอง*</td>
        		<td><input type="text" name="gap_certify_date" class="datetimepicker" placeholder="DD/MM/YYYY (พ.ศ.)" /></td>
        	</tr>
        	<tr>
        		<td>วันที่หมดอายุ*</td>
        		<td><input type="text" name="gap_expire_date" class="datetimepicker" placeholder="DD/MM/YYYY (พ.ศ.)"/></td>
        	</tr>
        	<tr>
        		<td>หน่วยงานรับรอง*</td>
        		<td><input type="text" name="gap_certify_agency" /></td>
        	</tr>
        </table>
        <input type="hidden" name="gap_id" value="" />
        <input type="hidden" name="gap_farm_id" value="<?php echo $this->item->id;?>" />
        <input type="hidden" name="option" value="<?php echo $_REQUEST['option'];?>" />
        <input type="hidden" name="task" value="farm.saveGAP" />
      </div>
      <div class="modal-footer" style="text-align:left">
      	<button type="button" class="btn btn-info" style="width:100px" onClick="javascript:addGAP();">บันทึก</button>&nbsp;&nbsp;
        <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
      </div>
    </div>

  </div>
  </form>
</div>



<!-- Modal -->
<div id="TrainingModal" class="modal fade" role="dialog" style="display:none">
<form id="TrainingForm" action="" method="post">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">การฝึกอบรม</h4>
      </div>
      <div class="modal-body">
        <table>
        	<tr>
        		<td width="120">ชื่อหลักสูตร*</td>
        		<td>
        			<select name="training_training_program_id">
					 	<option value="">-- เลือกหลักสูตร --</option>
					 	<?php 
							foreach($this->masterData->mas_training_program as $bkey=>$bdata){ 
						?>
								<option value="<?php echo $bdata->id;?>"><?php echo $bdata->name; ?></option>
						<?php } ?>
        			</select>
        		</td>
        	</tr>
        	<tr>
        		<td>วันที่*</td>
        		<td><input type="text" name="training_training_date" class="datetimepicker" placeholder="DD/MM/YYYY (พ.ศ.)" /></td>
        	</tr>
        	<tr>
        		<td>สถานที่ฝึกอบรม</td>
        		<td>
        			<input type="text" name="training_training_place" />
        		</td>
        	</tr>
        	<tr>
        		<td>จำนวนวัน*</td>
        		<td><input type="text" name="training_training_day" /></td>
        	</tr>
        </table>
        <input type="hidden" name="training_id" value="" />
        <input type="hidden" name="training_farm_id" value="<?php echo $this->item->id;?>" />
        <input type="hidden" name="training_farmer_id" value="<?php echo $this->item->farmer->id;?>" />
        <input type="hidden" name="option" value="<?php echo $_REQUEST['option'];?>" />
        <input type="hidden" name="task" value="farm.saveTraining" />
      </div>
      <div class="modal-footer" style="text-align:left">
      	<button type="button" class="btn btn-info" style="width:100px" onClick="javascript:addTraining();">บันทึก</button>&nbsp;&nbsp;
        <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
      </div>
    </div>

  </div>
  </form>
</div>




<!-- Modal -->
<div id="OwnerModal" class="modal fade" role="dialog" style="display:none">
<form id="OwnerForm" action="" method="post">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">เจ้าของฟาร์ม</h4>
      </div>
      <div class="modal-body">
        <table>
        	<tr>
        		<td width="120">เจ้าของฟาร์ม*</td>
        		<td>
        			<select name="owner_farmer_id">
        				<option value="">-- เลือกเจ้าของฟาร์ม --</option>
					 	<?php 
							foreach($this->masterData->mas_farmer as $bkey=>$bdata){ 
						?>
								<option value="<?php echo $bdata->id;?>"><?php echo $bdata->citizen_id; ?> : <?php echo $bdata->name; ?> <?php echo $bdata->surname; ?></option>
						<?php } ?>
        			</select>
        		</td>
        	</tr>
        	<tr>
        		<td>วันที่เริ่มเป็นเจ้าของ</td>
        		<td><input type="text" name="owner_own_date" class="datetimepicker" placeholder="DD/MM/YYYY (พ.ศ.)" /></td>
        	</tr>
        </table>
        <input type="hidden" name="owner_id" value="" />
        <input type="hidden" name="owner_farm_id" value="<?php echo $this->item->id;?>" />
        <input type="hidden" name="owner_farmer_id" value="<?php echo $this->item->farmer->id;?>" />
        <input type="hidden" name="option" value="<?php echo $_REQUEST['option'];?>" />
        <input type="hidden" name="task" value="farm.saveOwner" />
      </div>
      <div class="modal-footer" style="text-align:left">
      	<button type="button" class="btn btn-info" style="width:100px" onClick="javascript:addOwner();">บันทึก</button>&nbsp;&nbsp;
        <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
      </div>
    </div>

  </div>
  </form>
</div>

<script type="text/javascript">

	function getGAP(id){
		jQuery.ajax({
            url: "index.php?option=com_edairy&task=farm.getGAP",
            type: "POST",
            data: {
                'gap_id': id
            },
            success: function (data) {
            	var data = jQuery.parseJSON(data);
            	console.log(data);

            	if(data!=null){
	            	jQuery("input[name=gap_certify_no]").val(data.certify_no);
	            	var certify_date = data.certify_date.split("-");

	            	jQuery("input[name=gap_certify_date]").val(certify_date[2] + "/" + certify_date[1] + "/" + (parseInt(certify_date[0])+543));

	            	var expire_date = data.expire_date.split("-");
	            	jQuery("input[name=gap_expire_date]").val(expire_date[2] + "/" + expire_date[1] + "/" + (parseInt(expire_date[0])+543));

	            	jQuery("input[name=gap_certify_agency]").val(data.certify_agency);

	            	jQuery("input[name=gap_id]").val(data.id);
	            }

            }

        });
	}
	function addGAP(){
		if(jQuery("input[name=gap_certify_no]").val()=="" || jQuery("input[name=gap_certify_date]").val()=="" || jQuery("input[name=gap_expire_date]").val()=="" || jQuery("input[name=gap_certify_agency]").val()==""){
			alert("กรุณาใส่ข้อมูลให้ครบถ้วน");
			return false;
		}else{
			document.getElementById('GAPForm').submit();
		}
		
	}
	function deleteGAP(id){
		jQuery.ajax({
            url: "index.php?option=com_edairy&task=farm.deleteGAP",
            type: "POST",
            data: {
                'gap_id': id
            },
            success: function (data) {
            	var data = jQuery.parseJSON(data);
                if (data.delete_result==true) {
                	alert("ลบข้อมูลสำเร็จ");
                } else {
                    alert("ไม่สามารถลบข้อมูลได้");

                }
                window.location = "index.php?option=com_edairy&view=farm&layout=edit&id=<?php echo $_REQUEST['id'];?>&default_tab=gap";
            }

        });
	}


	function getTraining(id){
		jQuery.ajax({
            url: "index.php?option=com_edairy&task=farm.getTraining",
            type: "POST",
            data: {
                'training_id': id
            },
            success: function (data) {
            	var data = jQuery.parseJSON(data);
            	console.log(data);

            	if(data!=null){

	            	var training_date = data.training_date.split("-");

	            	jQuery("input[name=training_training_date]").val(training_date[2] + "/" + training_date[1] + "/" + (parseInt(training_date[0])+543));

	            	jQuery("select[name=training_training_program_id]").val(data.training_program_id);

	            	jQuery("select[name=training_training_program_id]").trigger('liszt:updated');

	            	jQuery("input[name=training_training_day]").val(data.training_day);

	            	jQuery("input[name=training_training_place]").val(data.training_place);
	            	
	            	jQuery("input[name=training_farmer_id]").val(data.farmer_id);

	            	jQuery("input[name=training_id]").val(data.id);
	            }

            }

        });
	}
	function addTraining(){
		if(jQuery("input[name=training_training_date]").val()=="" || jQuery("select[name=training_training_program_id]").val()=="" || jQuery("input[name=training_training_day]").val()=="" ){
			alert("กรุณาใส่ข้อมูลให้ครบถ้วน");
			return false;
		}else{
			document.getElementById('TrainingForm').submit();
		}
		
	}
	function deleteTraining(id){
		jQuery.ajax({
            url: "index.php?option=com_edairy&task=farm.deleteTraining",
            type: "POST",
            data: {
                'training_id': id
            },
            success: function (data) {
            	var data = jQuery.parseJSON(data);
                if (data.delete_result==true) {
                	alert("ลบข้อมูลสำเร็จ");
                } else {
                    alert("ไม่สามารถลบข้อมูลได้");

                }
                window.location = "index.php?option=com_edairy&view=farm&layout=edit&id=<?php echo $_REQUEST['id'];?>&default_tab=farmer";
            }

        });
	}


	function saveFarmer(){
		if(jQuery("input[name=farmer_citizen_id]").val()==""){
			alert("กรุณาใส่ข้อมูลให้ครบถ้วน");
			return false;
		}else{
			document.getElementById('FarmerForm').submit();
		}
		
	}


	function getOwner(id){
		jQuery.ajax({
            url: "index.php?option=com_edairy&task=farm.getOwner",
            type: "POST",
            data: {
                'owner_id': id
            },
            success: function (data) {
            	var data = jQuery.parseJSON(data);
            	console.log(data);

            	if(data!=null){

	            	var own_date = data.own_date.split("-");

	            	jQuery("input[name=owner_own_date]").val(own_date[2] + "/" + own_date[1] + "/" + (parseInt(own_date[0])+543));

	            	jQuery("select[name=owner_farmer_id]").val(data.farmer_id);

	            	jQuery("select[name=owner_farmer_id]").trigger('liszt:updated');

	            	jQuery("input[name=owner_farm_id]").val(data.farm_id);

	            	jQuery("input[name=owner_id]").val(data.id);
	            }

            }

        });
	}

	function addOwner(){
		if(jQuery("select[name=owner_farmer_id]").val()=="" || jQuery("input[name=owner_own_date]").val()=="" ){
			alert("กรุณาใส่ข้อมูลให้ครบถ้วน");
			return false;
		}else{
			document.getElementById('OwnerForm').submit();
		}
		
	}



	function deleteOwner(id){
		jQuery.ajax({
            url: "index.php?option=com_edairy&task=farm.deleteOwner",
            type: "POST",
            data: {
                'owner_id': id
            },
            success: function (data) {
            	var data = jQuery.parseJSON(data);
                if (data.delete_result==true) {
                	alert("ลบข้อมูลสำเร็จ");
                } else {
                    alert("ไม่สามารถลบข้อมูลได้");

                }
                window.location = "index.php?option=com_edairy&view=farm&layout=edit&id=<?php echo $_REQUEST['id'];?>&default_tab=farmer";
            }

        });
	}

	function clearOwnerForm(){
		jQuery("#OwnerForm input[type!=hidden]").val("");
		jQuery("#OwnerForm input[name=owner_id]").val("");
		jQuery("#OwnerForm select").val("");
		jQuery("#OwnerForm select").trigger("liszt:updated");
	}

	function clearGAPForm(){
		jQuery("#GAPForm input[type!=hidden]").val("");
		jQuery("#GAPForm input[name=gap_id]").val("");
	}

	function clearTrainingForm(){
		jQuery("#TrainingForm input[type!=hidden]").val("");
		jQuery("#TrainingForm input[name=training_id]").val("");
		jQuery("#TrainingForm select").val("");
		jQuery("#TrainingForm select").trigger("liszt:updated");
	}
	
	function getAge(dateString) {
	    var today = new Date();
	    var birthDate = new Date(dateString);
	    var age = today.getFullYear() - birthDate.getFullYear();
	    var m = today.getMonth() - birthDate.getMonth();
	    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
	        age--;
	    }
	    return age;
	}

	function calculateFarmerAge(unformattedDateString){
		var dateStringPart = unformattedDateString.split("/");
		jQuery("#farmer_age").val(getAge(dateStringPart[2]-543+"-"+dateStringPart[1]+"-"+dateStringPart[0]));

	}

	jQuery("#farmer_age").val(getAge("<?php echo $this->item->farmer->birthdate; ?>"));



</script>


<script>
      // This example adds a search box to a map, using the Google Place Autocomplete
      // feature. People can enter geographical searches. The search box will return a
      // pick list containing a mix of places and predicted search terms.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      <?php
      	$lat = 13.765001;
      	$lng = 100.53816;

      	if($this->item->latitude!=0){
      		$lat = $this->item->latitude;
      	}
      	if($this->item->longitude!=0){
      		$lng = $this->item->longitude;
      	}
      ?>
      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: <?php echo $lat; ?>, lng: <?php echo $lng; ?>},
          zoom: 13,
          mapTypeId: 'roadmap'
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];

              var myLatlng = {lat: <?php echo $lat; ?>, lng: <?php echo $lng; ?>};

        var marker = new google.maps.Marker({
          position: myLatlng,
          map: map,
          draggable: true,
   		  animation: google.maps.Animation.DROP
        });

        google.maps.event.addListener(marker, 'dragend', function (event) {
		    document.getElementById("jform_latitude").value = this.getPosition().lat();
		    document.getElementById("jform_longitude").value = this.getPosition().lng();
		});


        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }



          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

    
          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            var marker = new google.maps.Marker({
              map: map,
              title: place.name,
              position: place.geometry.location,
              draggable: true,
   		  		animation: google.maps.Animation.DROP
            });
            markers.push(marker);


            google.maps.event.addListener(marker, 'dragend', function (event) {
			    document.getElementById("jform_latitude").value = this.getPosition().lat();
			    document.getElementById("jform_longitude").value = this.getPosition().lng();
			});

            console.log(place.geometry.location);
            js("#jform_latitude").val(place.geometry.location.lat);
            js("#jform_longitude").val(place.geometry.location.lng);

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
      }

    </script>


    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD9kSzIrHDG2nYTH3S-ni9oBTEGN4JM7wk&libraries=places&callback=initMap">
    </script>

 <script type="text/javascript">

 	jQuery('#jform_province_id').change(function(){
 		getDistrictInProvince(jQuery('#jform_province_id').val());
 	});

 	jQuery('#jform_district_id').change(function(){
 		getSubDistrictInDistrict(jQuery('#jform_district_id').val());
 	});

	function getDistrictInProvince(province_id){
		console.log(province_id);
		jQuery.get("index.php?option=com_edairy&task=farm.getDistrictInProvince&province_id="+province_id, function(data){
			data = jQuery.parseJSON(data);
			console.log(data);
			jQuery("#jform_district_id").empty();
			jQuery('#jform_district_id').append("<option value=''>-- โปรดเลือก --</option>");
			for(var i=0;i<data.length;i++){
				jQuery('#jform_district_id').append("<option value='"+data[i].id+"'>"+data[i].name+"</option>");
			}
			jQuery('#jform_district_id').trigger("liszt:updated");
		});
	}

	function getSubDistrictInDistrict(district_id){
		jQuery.get("index.php?option=com_edairy&task=farm.getSubDistrictInDistrict&district_id="+district_id, function(data){
			data = jQuery.parseJSON(data);
			console.log(data);
			jQuery("#jform_sub_district_id").empty();
			jQuery('#jform_sub_district_id').append("<option value=''>-- โปรดเลือก --</option>");
			for(var i=0;i<data.length;i++){
				jQuery('#jform_sub_district_id').append("<option value='"+data[i].id+"'>"+data[i].name+"</option>");
			}
			jQuery('#jform_sub_district_id').trigger("liszt:updated");
		});
	}
</script>