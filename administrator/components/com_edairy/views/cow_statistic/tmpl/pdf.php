<?php 

// Include the main TCPDF library (search for installation path).
require_once('../media/com_edairy/tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('EDAIRY');
$pdf->SetTitle('EDAIRY');
$pdf->SetSubject('EDAIRY');
$pdf->SetKeywords('EDAIRY');

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
// set font

//$fontname = $pdf->addTTFfont('fonts/Browa.ttf', 'TrueTypeUnicode', '', 32);
$pdf->SetFont('thsarabunpsk', '', 14, '', true);



//PAGE 3 >> PAGE 1
$pdf->AddPage();
$pdf->setPageOrientation ('P', $autopagebreak='', $bottommargin='');
// get the current page break margin
$bMargin = $pdf->getBreakMargin();
// get current auto-page-break mode
$auto_page_break = $pdf->getAutoPageBreak();
// disable auto-page-break
$pdf->SetAutoPageBreak(true, 0);
// set bacground image
/*
$heading_html = <<<EOD
<img src="images/header.png" />

EOD;

$pdf->writeHTMLCell($w=0, $h=0, $x='10', $y='5', $heading_html, $border=0, $ln=1, $fill=0, $reseth=true, $align='L', $autopadding=false);

*/

$create_date = formatDate($this->item->create_date);

$female_sum = $this->item->total5+$this->item->total6+$this->item->total7+$this->item->total8+$this->item->total9;

$html = <<<EOD
<div style="text-align:center;font-size:20px">
<table width="1000">
	<tr>
		<td width="80">
			<img src="../images/dpo_logo.jpg" width="80"/>
		</td>
		<td>
			<br /><br />			<b>องค์การส่งเสริมกิจการโคนมแห่งประเทศไทย (อ.ส.ค.) กระทรวงเกษตรและสหกรณ์<br />ใบสำรวจประชากรโค</b>
		</td>
	</tr>
</table>
</div>


<table>
	<tr>
		<td width="150">
			วันที่สำรวจ: {$create_date}
		</td>
		<td>
			ชื่อ-รหัส เกษตรกร: {$this->item->member_code} {$this->item->farmer}
		</td>
		<td>
			สหกรณ์/ศูนย์รับน้ำนม: {$this->item->coop}
		</td>
	</tr>
</table>

<br /><br />
<table>
	<tr>
		<td width="50"></td>
		<td width="200">
			จำนวนโครีดนม ท้อง
		</td>
		<td align="right" width="100">
			{$this->item->total1}
		</td>
		<td width="80" align="right"> ตัว</td>
	</tr>
	<tr>
		<td></td>
		<td>
			จำนวนโครีดนม ไม่ท้อง
		</td>
		<td align="right">
			{$this->item->total2}
		</td>
		<td align="right"> ตัว</td>
	</tr>
	<tr>
		<td></td>
		<td>
			จำนวนโคหยุดรีดนม ท้อง
		</td>
		<td align="right">
			{$this->item->total3}
		</td>
		<td align="right"> ตัว</td>
	</tr>
	<tr>
		<td></td>
		<td>
			จำนวนโคหยุดรีดนม ไม่ท้อง
		</td>
		<td align="right">
			{$this->item->total4}
		</td>
		<td align="right"> ตัว</td>
	</tr>
	<tr>
		<td></td>
		<td>
			จำนวนโคเพศเมีย ไม่ท้อง
		</td>
		<td align="right">
			{$female_sum}
		</td>
		<td align="right"> ตัว</td>
	</tr>
	<tr>
		<td></td>
		<td>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;อายุ 0-1 เดือน
		</td>
		<td align="right">
			{$this->item->total5}
		</td>
		<td align="right"> ตัว</td>
	</tr>
	<tr>
		<td></td>
		<td>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;อายุ 1-4 เดือน
		</td>
		<td align="right">
			{$this->item->total6}
		</td>
		<td align="right"> ตัว</td>
	</tr>
	<tr>
		<td></td>
		<td>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;อายุ 4 เดือน - 1 ปี
		</td>
		<td align="right">
			{$this->item->total7}
		</td>
		<td align="right"> ตัว</td>
	</tr>
	<tr>
		<td></td>
		<td>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;อายุ 1-2 ปั
		</td>
		<td align="right">
			{$this->item->total8}
		</td>
		<td align="right"> ตัว</td>
	</tr>
	<tr>
		<td></td>
		<td>
			จำนวนโคสาว ท้อง
		</td>
		<td align="right">
			{$this->item->total9}
		</td>
		<td align="right"> ตัว</td>
	</tr>
	<tr>
		<td></td>
		<td>
			จำนวนโคสาว ไม่ท้อง อายุ 2 ปีขึ้นไป
		</td>
		<td align="right">
			{$this->item->total10}
		</td>
		<td align="right"> ตัว</td>
	</tr>
	<tr>
		<td></td>
		<td>
			โคเพศเมีย รวมทั้งหมด
		</td>
		<td align="right">
			{$this->item->total_female}
		</td>
		<td align="right"> ตัว</td>
	</tr>
	<tr>
		<td></td>
		<td>
			โคเพศผู้ รวมทั้งหมด
		</td>
		<td align="right">
			{$this->item->total_male}
		</td>
		<td align="right"> ตัว</td>
	</tr>
	<tr>
		<td></td>
		<td>
			รวมโคทั้งหมด
		</td>
		<td align="right">
			{$this->item->total_cow}
		</td>
		<td align="right"> ตัว</td>
	</tr>
	<tr>
		<td></td>
		<td>
			หมายเหตุ
		</td>
		<td align="right">
			{$this->item->remark}
		</td>
		<td align="right"> </td>
	</tr>
</table>

<br />
<br />
<br />
<br />
<br />
<table>
	<tr>
		<td width="500">
		</td>
		<td width="500">
			................................................<br />
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)
			<br />
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ผู้บันทึก
		</td>
	</tr>
</table>

EOD;




// print a block of text using Write()
$pdf->writeHTMLCell($w=0, $h=0, $x='10', $y='10', $html, $border=0, $ln=1, $fill=0, $reseth=true, $align='L', $autopadding=false);

 






// set bacground image

// if (isset($_POST['download'])) {
// 	$pdf->Output("detail_id{$_POST[id]}.pdf", 'D');
// }else{
	$pdf->Output("Farm_Score_{$_GET[id]}.pdf", 'I');
// }
die();
