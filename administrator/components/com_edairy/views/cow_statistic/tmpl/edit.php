<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Edairy
 * @author     Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright  2017 Natchapol Kittigul
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_edairy/css/form.css');

if($this->item->create_date==""){
	$this->item->create_date = date("Y-m-d");
}
?>
<script src="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.js" type="text/javascript" charset="UTF-8"></script>

<link rel="stylesheet" href="../media/com_edairy/datetimepicker-master/jquery.datetimepicker.css">
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		js('.datetimepicker').datetimepicker({
            lang:'th',
            timepicker:false,
            format:'d/m/Y'
        });
	js('input:hidden.farmer_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('farmer_idhidden')){
			js('#jform_farmer_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_farmer_id").trigger("liszt:updated");
	js('input:hidden.farm_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('farm_idhidden')){
			js('#jform_farm_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_farm_id").trigger("liszt:updated");
	});

	Joomla.submitbutton = function (task) {
		if (task == 'cow_statistic.cancel') {
			Joomla.submitform(task, document.getElementById('cow_statistic-form'));
		}
		else {
			
			if (task != 'cow_statistic.cancel' && document.formvalidator.isValid(document.id('cow_statistic-form'))) {
				
				Joomla.submitform(task, document.getElementById('cow_statistic-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_edairy&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="cow_statistic-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_EDAIRY_TITLE_COW_STATISTIC', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

									<?php echo $this->form->renderField('id'); ?>
				

				<div class="control-group">
					<div class="control-label">
						<label id="jform_create_date-lbl" for="jform_create_date" class="">
							วันที่</label>
					</div>
					<div class="controls"><div class="input-append"><input type="text" title="" name="jform[create_date]" id="jform_create_date" value="<?php echo formatDate($this->item->create_date); ?>" maxlength="45" class="inputbox hasTooltip datetimepicker" placeholder="วันที่"  aria-invalid="false"></div></div>
		</div>


				<?php echo $this->form->renderField('farmer_id'); ?>

			<?php
				foreach((array)$this->item->farmer_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="farmer_id" name="jform[farmer_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('farm_id'); ?>

			<?php
				foreach((array)$this->item->farm_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="farm_id" name="jform[farm_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				
<div style="clear:both"></div>

				<hr />
				<table>
					<tr valign="top">
						<td width="150">โครีดนม</td>
						<td>
							<div class="control-group">
								<div class="control-label">
									<label>ท้อง</label>
								</div>
							<div class="controls">
								<input type="text" name="jform[total1]" id="jform_total1" value="<?php echo $this->item->total1; ?>" placeholder="" aria-invalid="false"> ตัว
							</div>
						</td>
					</tr>
					<tr valign="top">
						<td width="150"></td>
						<td>
							<div class="control-group">
								<div class="control-label">
									<label>ไม่ท้อง</label>
								</div>
							<div class="controls">
								<input type="text" name="jform[total2]" id="jform_total2" value="<?php echo $this->item->total2; ?>" placeholder="" aria-invalid="false"> ตัว
							</div>
						</td>
					</tr>
					<tr valign="top">
						<td width="150">โคหยุดรีด</td>
						<td>
							<div class="control-group">
								<div class="control-label">
									<label>ท้อง</label>
								</div>
							<div class="controls">
								<input type="text" name="jform[total3]" id="jform_total3" value="<?php echo $this->item->total3; ?>" placeholder="" aria-invalid="false"> ตัว
							</div>
						</td>
					</tr>
					<tr valign="top">
						<td width="150"></td>
						<td>
							<div class="control-group">
								<div class="control-label">
									<label>ไม่ท้อง</label>
								</div>
							<div class="controls">
								<input type="text" name="jform[total4]" id="jform_total4" value="<?php echo $this->item->total4; ?>" placeholder="" aria-invalid="false"> ตัว
							</div>
						</td>
					</tr>
					<tr valign="top">
						<td width="150">โคเพศเมียไม่ท้อง</td>
						<td>
							<div class="control-group">
								<div class="control-label">
									<label>0-1 เดือน</label>
								</div>
							<div class="controls">
								<input type="text" name="jform[total5]" id="jform_total5" value="<?php echo $this->item->total5; ?>" placeholder="" aria-invalid="false"> ตัว
							</div>
						</td>
					</tr>
					<tr valign="top">
						<td width="150"></td>
						<td>
							<div class="control-group">
								<div class="control-label">
									<label>1-4 เดือน</label>
								</div>
							<div class="controls">
								<input type="text" name="jform[total6]" id="jform_total6" value="<?php echo $this->item->total6; ?>" placeholder="" aria-invalid="false"> ตัว
							</div>
						</td>
					</tr>
					<tr valign="top">
						<td width="150"></td>
						<td>
							<div class="control-group">
								<div class="control-label">
									<label>4 เดือน - 1 ปี</label>
								</div>
							<div class="controls">
								<input type="text" name="jform[total7]" id="jform_total7" value="<?php echo $this->item->total7; ?>" placeholder="" aria-invalid="false"> ตัว
							</div>
						</td>
					</tr>
					<tr valign="top">
						<td width="150"></td>
						<td>
							<div class="control-group">
								<div class="control-label">
									<label>1-2 ปี</label>
								</div>
							<div class="controls">
								<input type="text" name="jform[total8]" id="jform_total8" value="<?php echo $this->item->total8; ?>" placeholder="" aria-invalid="false"> ตัว
							</div>
						</td>
					</tr>
					<tr valign="top">
						<td width="150">โคสาว</td>
						<td>
							<div class="control-group">
								<div class="control-label">
									<label>ท้อง</label>
								</div>
							<div class="controls">
								<input type="text" name="jform[total9]" id="jform_total9" value="<?php echo $this->item->total9; ?>" placeholder="" aria-invalid="false"> ตัว
							</div>
						</td>
					</tr>
					<tr valign="top">
						<td width="150"></td>
						<td>
							<div class="control-group">
								<div class="control-label">
									<label>ไม่ท้อง 2 ปีขึ้นไป</label>
								</div>
							<div class="controls">
								<input type="text" name="jform[total10]" id="jform_total10" value="<?php echo $this->item->total10; ?>" placeholder="" aria-invalid="false"> ตัว
							</div>
						</td>
					</tr>
				</table>
				<hr />
				<div style="clear:both"></div>

				<br />

				<?php echo $this->form->renderField('total_female'); ?>
				<?php echo $this->form->renderField('total_male'); ?>
				<?php echo $this->form->renderField('total_cow'); ?>
				<?php echo $this->form->renderField('remark'); ?>

				<div style="display:none">
					<?php echo $this->form->renderField('create_by'); ?>
					<?php echo $this->form->renderField('state'); ?>
				</div>

				<?php echo $this->form->renderField('created_by'); ?>


					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>

<script type="text/javascript">
	function calculateSum(){
		var sum = parseInt(jQuery("input[name='jform[total_female]']").val())+parseInt(jQuery("input[name='jform[total_male]']").val());
		if(!isNaN(sum)){
				jQuery("#jform_total_cow").val(sum);
			}
	}
	jQuery( "input[name='jform[total_female]']" ).change(function() {
	  calculateSum();
	});
	jQuery( "input[name='jform[total_male]']" ).change(function() {
	  calculateSum();
	});
</script>