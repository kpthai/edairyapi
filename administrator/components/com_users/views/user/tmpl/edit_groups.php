<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the component HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

?>
<?php echo JHtml::_('access.usergroups', 'jform[groups]', $this->groups, true); ?>


<script type="text/javascript">
	jQuery("#1group_7").attr("checked", "checked");
</script>