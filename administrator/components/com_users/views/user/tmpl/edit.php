<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the component HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('behavior.formvalidator');
JHtml::_('formbehavior.chosen', 'select');

JFactory::getDocument()->addScriptDeclaration("
	Joomla.submitbutton = function(task)
	{
		if (task == 'user.cancel' || document.formvalidator.isValid(document.getElementById('user-form')))
		{
			Joomla.submitform(task, document.getElementById('user-form'));
		}
	};

	Joomla.twoFactorMethodChange = function(e)
	{
		var selectedPane = 'com_users_twofactor_' + jQuery('#jform_twofactor_method').val();

		jQuery.each(jQuery('#com_users_twofactor_forms_container>div'), function(i, el) {
			if (el.id != selectedPane)
			{
				jQuery('#' + el.id).hide(0);
			}
			else
			{
				jQuery('#' + el.id).show(0);
			}
		});
	};
");

// Get the form fieldsets.
$fieldsets = $this->form->getFieldsets();
?>

<form action="<?php echo JRoute::_('index.php?option=com_users&layout=edit&id=' . (int) $this->item->id); ?>" method="post" name="adminForm" id="user-form" class="form-validate form-horizontal" enctype="multipart/form-data">

	<?php echo JLayoutHelper::render('joomla.edit.item_title', $this); ?>

	<fieldset>
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'details')); ?>

			<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'details', JText::_('COM_USERS_USER_ACCOUNT_DETAILS')); ?>


			<div class="well">
				<table>
					<tr>
						<td width="160">บทบาทผู้ใช้งาน: </td>
						<td>
							<select name="role">
								<option>-- เลือก --</option>
								<option value="1" <?php if($this->item->role==1){ ?> selected<?php } ?>>เกษตรกร</option>
								<option value="2" <?php if($this->item->role==2){ ?> selected<?php } ?>>นักส่งเสริม</option>
								<option value="3" <?php if($this->item->role==3){ ?> selected<?php } ?>>สหกรณ์</option>
								<option value="4" <?php if($this->item->role==4){ ?> selected<?php } ?>>ศูนย์รับน้ำนมดิบ</option>
								<option value="5" <?php if($this->item->role==5){ ?> selected<?php } ?>>ผู้บริหาร</option>
								<option value="6" <?php if($this->item->role==6){ ?> selected<?php } ?>>ผู้ดูแลระบบส่วนภูมิภาค</option>
								<option value="7" <?php if($this->item->role==7){ ?> selected<?php } ?>>ผู้ดูแลระบบส่วนกลาง</option>
							</select>
						</td>
					</tr>

					<tr>
						<td>ภาค: </td>
						<td>
							<select name="region_id">
								<option value="">-- โปรดเลือก --</option>
								<?php 
							foreach($this->masterData->mas_region as $bkey=>$bdata){ 
								$selected = "";
								if($bdata->id == $this->item->region_id){
									$selected = " selected";
								}
						?>
								<option value="<?php echo $bdata->id;?>"<?php echo $selected; ?>><?php echo $bdata->name; ?></option>
						<?php } ?>
							</select>
						</td>
					</tr>
					<tr>
						<td>สหกรณ์/ศูนย์รับน้ำนมดิบ: </td>
						<td>
							<select name="coop_id">
								<option value="">-- โปรดเลือก --</option>
								<?php 
							foreach($this->masterData->mas_coop as $bkey=>$bdata){ 
								$selected = "";
								if($bdata->id == $this->item->coop_id){
									$selected = " selected";
								}
						?>
								<option value="<?php echo $bdata->id;?>"<?php echo $selected; ?>><?php echo $bdata->coop_code; ?>: <?php echo $bdata->name; ?></option>
						<?php } ?>
							</select>
						</td>
					</tr>
					<tr>
						<td>ฟาร์ม: </td>
						<td>
							<select name="farm_id">
								<option value="">-- โปรดเลือก --</option>
								<?php 
							foreach($this->masterData->mas_farm as $bkey=>$bdata){ 
								$selected = "";
								if($bdata->id == $this->item->farm_id){
									$selected = " selected";
								}
						?>
								<option value="<?php echo $bdata->id;?>"<?php echo $selected; ?>><?php echo $bdata->member_code; ?>: <?php echo $bdata->name; ?></option>
						<?php } ?>
							</select>
						</td>
					</tr>
					<tr>
						<td>เกษตรกร: </td>
						<td>
							<select name="farmer_id">
								<option value="">-- โปรดเลือก --</option>
								<?php 
							foreach($this->masterData->mas_farmer as $bkey=>$bdata){ 
								$selected = "";
								if($bdata->id == $this->item->farmer_id){
									$selected = " selected";
								}
						?>
								<option value="<?php echo $bdata->id;?>"<?php echo $selected; ?>><?php echo $bdata->citizen_id; ?>: <?php echo $bdata->name; ?> <?php echo $bdata->surname; ?></option>
						<?php } ?>
							</select>
						</td>
					</tr>
				</table>
			</div>

				<?php foreach ($this->form->getFieldset('user_details') as $field) : ?>
					<div class="control-group">
						<div class="control-label">
							<?php echo $field->label; ?>
						</div>
						<div class="controls">
							<?php if ($field->fieldname == 'password') : ?>
								<?php // Disables autocomplete ?> <input type="password" style="display:none">
							<?php endif; ?>
							<?php echo $field->input; ?>
						</div>
					</div>
				<?php endforeach; ?>
			<?php echo JHtml::_('bootstrap.endTab'); ?>

			<?php if ($this->grouplist) : ?>
				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'groups', JText::_('COM_USERS_ASSIGNED_GROUPS')); ?>
					<?php echo $this->loadTemplate('groups'); ?>
				<?php echo JHtml::_('bootstrap.endTab'); ?>
			<?php endif; ?>

			<?php
			$this->ignore_fieldsets = array('user_details');
			echo JLayoutHelper::render('joomla.edit.params', $this);
			?>

		<?php if (!empty($this->tfaform) && $this->item->id): ?>
		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'twofactorauth', JText::_('COM_USERS_USER_TWO_FACTOR_AUTH')); ?>
		<div class="control-group">
			<div class="control-label">
				<label id="jform_twofactor_method-lbl" for="jform_twofactor_method" class="hasTooltip"
						title="<?php echo '<strong>' . JText::_('COM_USERS_USER_FIELD_TWOFACTOR_LABEL') . '</strong><br />' . JText::_('COM_USERS_USER_FIELD_TWOFACTOR_DESC'); ?>">
					<?php echo JText::_('COM_USERS_USER_FIELD_TWOFACTOR_LABEL'); ?>
				</label>
			</div>
			<div class="controls">
				<?php echo JHtml::_('select.genericlist', Usershelper::getTwoFactorMethods(), 'jform[twofactor][method]', array('onchange' => 'Joomla.twoFactorMethodChange()'), 'value', 'text', $this->otpConfig->method, 'jform_twofactor_method', false) ?>
			</div>
		</div>
		<div id="com_users_twofactor_forms_container">
			<?php foreach($this->tfaform as $form): ?>
			<?php $style = $form['method'] == $this->otpConfig->method ? 'display: block' : 'display: none'; ?>
			<div id="com_users_twofactor_<?php echo $form['method'] ?>" style="<?php echo $style; ?>">
				<?php echo $form['form'] ?>
			</div>
			<?php endforeach; ?>
		</div>

		<fieldset>
			<legend>
				<?php echo JText::_('COM_USERS_USER_OTEPS') ?>
			</legend>
			<div class="alert alert-info">
				<?php echo JText::_('COM_USERS_USER_OTEPS_DESC') ?>
			</div>
			<?php if (empty($this->otpConfig->otep)): ?>
			<div class="alert alert-warning">
				<?php echo JText::_('COM_USERS_USER_OTEPS_WAIT_DESC') ?>
			</div>
			<?php else: ?>
			<?php foreach ($this->otpConfig->otep as $otep): ?>
			<span class="span3">
				<?php echo substr($otep, 0, 4) ?>-<?php echo substr($otep, 4, 4) ?>-<?php echo substr($otep, 8, 4) ?>-<?php echo substr($otep, 12, 4) ?>
			</span>
			<?php endforeach; ?>
			<div class="clearfix"></div>
			<?php endif; ?>
		</fieldset>

		<?php echo JHtml::_('bootstrap.endTab'); ?>
		<?php endif; ?>

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>
	</fieldset>

	<input type="hidden" name="task" value="" />
	<?php echo JHtml::_('form.token'); ?>
</form>
