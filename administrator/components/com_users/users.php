<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
JHtml::_('behavior.tabstate');

if (!JFactory::getUser()->authorise('core.manage', 'com_users'))
{
	throw new JAccessExceptionNotallowed(JText::_('JERROR_ALERTNOAUTHOR'), 403);
}

JLoader::register('UsersHelper', __DIR__ . '/helpers/users.php');

?>
<style type="text/css">
	.other-filters{
    padding: 0 14px;
}

.sidebar-sub-1{
      display:none;
}
.sidebar-sub-2{
      display:none;
}
.sidebar-sub-3{
      display:none;
}
.sidebar-sub-4{
      display:none;
}
.sidebar-sub-5{
      display:none;
}
.sidebar-sub-6{
      display:none;
}
.sidebar-sub-7{
      display:none;
}
.sidebar-sub-8{
      display:none;
}
.sidebar-sub-9{
      display:none;
}
.sidebar-sub-10{
      display:none;
}
.in-sub{
      display:inline!important;
}
</style>
<?php
$controller = JControllerLegacy::getInstance('Users');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
