<?php
require_once "init.php";

$access_token = HttpRequest::getParam("access_token");
$cmd = HttpRequest::getParam("cmd");

$user_login_id = 0;

$query = "SELECT * FROM {$jConfig->dbprefix}users WHERE access_token='{$access_token}'";

$result = $db->query($query);

if ($result) {

    $row = $result->fetch_assoc();
    if($cmd == "get_cow_detail"){
        getCowDetail($db,$jConfig->dbprefix,$row);
    }else if($cmd == "add_farmer_survey_transection"){
        addFarmSurvey($db,$jConfig->dbprefix,$row);
    }else{
        ListFarmSurvey($db,$jConfig->dbprefix,$row);
    }
}

function ListFarmSurvey($db,$dbprefix,$row){
    
    $farmer_name = HttpRequest::getParam("farmer_name");
    $coop_id = HttpRequest::getParam("coop_id");
    $start_date = HttpRequest::getParam("start_date");
    $end_date = HttpRequest::getParam("end_date");
    
    $current_page = HttpRequest::getParam("current_page");
    $cur_page = $current_page * 20;
    
    $search_query = "";
    
    print_r($row->role);
    if($row->role == 7 || $row->role == 5){ //admin และ ผู้บริหาร
        $search_query.= " ";
    }else if($row->role == 6 || $row->role == 2){ //ส่วนภูมิภาค และ นักส่งเสริม
        $search_query.= " fm.region_id = $row->region_id and ";
    }else if($row->role == 3 || $row->role == 4){ //สหรณ์ ศุนย์รับน้ำนมดิบ
        $search_query.= " s.coop_id = $row->coop_id and ";
    }else{
        $search_query.= " fm.farm_id='$row->farm_id' and ";
    }

    if($farmer_name!="" && $farmer_name!=null){
            $search_query .= "  (fm.name like '%{$farmer_name}%' or fm.surname like '%{$farmer_name}%' or s.member_code like '%{$farmer_name}%') AND";
    }
    if($coop_id!="" && $coop_id!=null){
            $search_query .= "  s.id in (select id from {$dbprefix}ed_farm f where f.coop_id like '{$coop_id}' ) AND";
    }
    if($start_date!="" && $end_date!=""){
        $search_query .= "  create_date BETWEEN '{$start_date}' AND '{$end_date}' AND";
    }

    $query = "	SELECT sv.*, s.name as farm_name,fm.name,max(sv.create_date) as survey_date

                            FROM {$dbprefix}ed_farm_survey_transaction sv
                LEFT JOIN {$dbprefix}ed_farm s ON sv.farm_id = s.id
                LEFT JOIN {$dbprefix}ed_farmer fm ON fm.farm_id = s.id
                            where {$search_query} s.state = 1 group by sv.farm_id limit $cur_page,20";
     //echo $query;
    $result = $db->query($query);

    if($result){
         // Cycle through results
        while ($row = $result->fetch_object()){
            $row->create_date = formatDate($row->create_date);
            $rows[] = $row;
        }
        // Free result set
        $result->close();
        $db->next_result();
        
    }else{
         HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
    }
    //total
    $queryTt = "	SELECT count(*) as total FROM {$dbprefix}ed_farm_survey_transaction sv
                LEFT JOIN {$dbprefix}ed_farm s ON sv.farm_id = s.id
                LEFT JOIN {$dbprefix}ed_farmer fm ON fm.farm_id = s.id
                            where {$search_query} s.state = 1 group by sv.farm_id ";
                //echo $queryTt;
    $resultTt = $db->query($queryTt);
    $rowTT = $resultTt->fetch_object();
    if($rowTT->total == ""){
        $rowTT->total = 0;
    }
    $rows[]["total"] = $rowTT->total;
    HttpResponse::responseSuccessJson($rows);
    $db->close();
}

function addFarmSurvey($db,$dbprefix,$row){
    
    $farmer_id = HttpRequest::getParam("farmer_id");
    $survey_date = HttpRequest::getParam("survey_date");
    
    if($farmer_id=="" || $farmer_id==null){
           return false;
    }
    
    if($survey_date=="" || $survey_date==null){
            return false;
    }
    
    //get lastId
    $qrLastId = "select id from {$dbprefix}ed_farm_survey_transaction ORDER BY id DESC Limit 1 ";
    $restLastId = $db->query($qrLastId);
    $rowLastId = $restLastId->fetch_assoc();
    $id = $rowLastId["id"]+1;
    
    //get farm_id
    $qrFarmId = "select farm_id from {$dbprefix}ed_farmer where id = '{$farmer_id}'";
    $restFarmId = $db->query($qrFarmId);
    $rowFarmId = $restFarmId->fetch_assoc();
    $farm_id = $rowFarmId["farm_id"];
    
    $query = "  INSERT INTO {$dbprefix}ed_farm_survey_transaction (id,farm_id,create_date,survey_by) VALUES 
                ('{$id}','{$farm_id}','{$survey_date}','{$row["id"]}')";
   
    //echo $query;
    $result = $db->query($query);

    if ($result) {
        HttpResponse::responseSuccessJson($farm_id);
    } else {
        HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
    }
    $db->close();
}
?>
