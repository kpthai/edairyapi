<?php

require_once "init.php";

$access_token = HttpRequest::getParam("access_token");
$cmd = HttpRequest::getParam("cmd");

$user_login_id = 0;

$query = "SELECT * FROM {$jConfig->dbprefix}users WHERE access_token='{$access_token}'";

$result = $db->query($query);

if ($result) {

    $row = $result->fetch_assoc();
    if($cmd == "get_all_cow_status"){
        getAllCowStatus($db,$jConfig->dbprefix);
    }else{
        ListCowStatus($db,$jConfig->dbprefix);
    }
}

function getAllCowStatus($db,$dbprefix){
    $sql = "select * from {$dbprefix}ed_cow_status";
    //print_r($query);
    $result = $db->query($sql);

    if($result){
         // Cycle through results
        while ($row = $result->fetch_object()){
            $rows[] = $row;
        }
        // Free result set
        $result->close();
        $db->next_result();
        HttpResponse::responseSuccessJson($rows);
    }else{
         HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
    }
    
    $db->close();
}

function ListCowStatus($db,$dbprefix){
    
    $id = HttpRequest::getParam("id");
    
    $sql = "select * from {$dbprefix}ed_cow_status where id = {$id} ";
    //print_r($query);
    $result = $db->query($sql);

    if($result){
         // Cycle through results
        $rows = $result->fetch_assoc();
        HttpResponse::responseSuccessJson($rows);
    }else{
        HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
    }
    
    $db->close();
}