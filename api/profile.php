<?php
require_once "init.php";
require_once "../libraries/phpass/PasswordHash.php";

$access_token = HttpRequest::getParam("access_token");
$cmd = HttpRequest::getParam("cmd");

$user_login_id = 0;

$query = "SELECT * FROM {$jConfig->dbprefix}users WHERE access_token='{$access_token}'";

$result = $db->query($query);

if ($result) {
    $row = $result->fetch_assoc();
    
    if($cmd == "update_farmer"){
        updateProfile($db,$jConfig->dbprefix);
    }else if($cmd == "update_password"){
        updatePassword($db,$jConfig->dbprefix,$row);
    }else if($cmd == "get_farmer_info"){
        getFarmerInfo($db,$jConfig->dbprefix);
    }else{
        getProfileInfo($db,$jConfig->dbprefix);
    }
    
    
}

function getFarmerInfo($db,$dbprefix){
    
    $id = HttpRequest::getParam("id");
    
    $query = "	SELECT fm.*,fm.name as farmer_name,fm.surname as farmer_surname,f.name as farm_name,f.coop_id,
		username,mobile,fm.address,rg.name as region_name,pv.name as province_name,dt.name as district_name,
                sdt.name as sub_district_name,rg.id as region_id,pv.id as province_id,dt.id as district_id,
                sdt.id as sub_district_id,c.name as coop_name,f.member_start_date,f.member_code,f.farm_project_id,f.status FROM {$dbprefix}ed_farmer fm
                LEFT JOIN {$dbprefix}users us ON fm.id = us.farmer_id
                LEFT JOIN {$dbprefix}ed_farm f ON f.id = fm.farm_id
                    LEFT JOIN {$dbprefix}ed_coop c ON c.id = f.coop_id
                LEFT JOIN {$dbprefix}ed_region rg ON rg.id = fm.region_id
                LEFT JOIN {$dbprefix}ed_province pv ON pv.id = fm.province_id
                LEFT JOIN {$dbprefix}ed_district dt ON dt.id = fm.district_id
                LEFT JOIN {$dbprefix}ed_sub_district sdt ON sdt.id = fm.sub_district_id
                WHERE fm.id='$id'";
    // echo $query;
    $result = $db->query($query);

    if($result){
        $rows = $result->fetch_assoc();
        HttpResponse::responseSuccessJson($rows);
    }else{
        HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
    }
    
    $db->close();
    
}

function getProfileInfo($db,$dbprefix){
    
    $id = HttpRequest::getParam("id");
    
    $query = "	SELECT fm.*,fm.name as farmer_name,fm.surname as farmer_surname,f.name as farm_name,f.coop_id,
		username,mobile,fm.address,rg.name as region_name,pv.name as province_name,dt.name as district_name,
                sdt.name as sub_district_name,rg.id as region_id,pv.id as province_id,dt.id as district_id,
                sdt.id as sub_district_id,c.name as coop_name,f.member_start_date,f.member_code,f.farm_project_id,f.status FROM {$dbprefix}ed_farmer fm
                LEFT JOIN {$dbprefix}users us ON fm.id = us.farmer_id
                LEFT JOIN {$dbprefix}ed_farm f ON f.id = fm.farm_id
                    LEFT JOIN {$dbprefix}ed_coop c ON c.id = f.coop_id
                LEFT JOIN {$dbprefix}ed_region rg ON rg.id = fm.region_id
                LEFT JOIN {$dbprefix}ed_province pv ON pv.id = fm.province_id
                LEFT JOIN {$dbprefix}ed_district dt ON dt.id = fm.district_id
                LEFT JOIN {$dbprefix}ed_sub_district sdt ON sdt.id = fm.sub_district_id
                WHERE fm.id='$id'";
    // echo $query;
    $result = $db->query($query);

    if($result){
        $rows = $result->fetch_assoc();
        HttpResponse::responseSuccessJson($rows);
    }else{
        HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
    }
    
    $db->close();
    
}

function updatePassword($db,$dbprefix,$row){
    $id = HttpRequest::getParam("id");
    $password = HttpRequest::getParam("password");
    $newpassword = HttpRequest::getParam("newpassword");
    
    $t_hasher = new PasswordHash(10, TRUE);
    $check = $t_hasher->CheckPassword($password, $row["password"]);
    
    if($check){
        $new_password = $t_hasher->HashPassword($newpassword);
        
        $sql = "update {$dbprefix}users set "
                . " password = '$new_password'"
                . " where id = '$id'";

        // echo $query;
        $result = $db->query($sql);
        if($result){
            HttpResponse::responseSuccessJson($result);
        }else{
            HttpResponse::responseErrorJson(500,"update error");
        }
    }else{
        HttpResponse::responseErrorJson(500,"password invalid");
    }
}

function updateProfile($db,$dbprefix){
    $id = HttpRequest::getParam("id");
    $title_id = HttpRequest::getParam("title_id");
    $fname = HttpRequest::getParam("fname");
    $lname = HttpRequest::getParam("lname");
    $edu_id = HttpRequest::getParam("edu_id");
    $address = HttpRequest::getParam("address");
    $region_id = HttpRequest::getParam("region_id");
    $prov_id = HttpRequest::getParam("prov_id");
    $dist_id = HttpRequest::getParam("dist_id");
    $sub_dist_id = HttpRequest::getParam("sub_dist_id");
    $postcode = HttpRequest::getParam("postcode");
    $lat = HttpRequest::getParam("lat");
    $lng = HttpRequest::getParam("lng");
    
    $sql = "update {$dbprefix}ed_farmer set "
            . " title_id = '$title_id',"
            . " name = '$fname',"
            . " surname = '$lname',"
            . " education_level_id = '$edu_id',"
            . " address = '$address',"
            . " region_id = '$region_id',"
            . " province_id = '$prov_id',"
            . " district_id = '$dist_id',"
            . " sub_district_id = '$sub_dist_id',"
            . " post_code = '$postcode',"
            . " latitude = $lat,"
            . " longitude = $lng "
            . " where id = '$id'";
    
    // echo $query;
    $result = $db->query($sql);
    if($result){
        HttpResponse::responseSuccessJson($result);
    }else{
        HttpResponse::responseErrorJson(500,"update error");
    }
}


?>
