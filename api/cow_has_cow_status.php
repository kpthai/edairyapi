<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once "init.php";

$access_token = HttpRequest::getParam("access_token");
$cmd = HttpRequest::getParam("cmd");

$user_login_id = 0;

$query = "SELECT * FROM {$jConfig->dbprefix}users WHERE access_token='{$access_token}'";

$result = $db->query($query);

if ($result) {

    $row = $result->fetch_assoc();
    
    if($cmd == "add_cow_status"){
        addCowHasCowStatus($db,$jConfig->dbprefix,$row);
    }
}

function addCowHasCowStatus($db,$dbprefix,$row){
    
    $cow_id = HttpRequest::getParam("id");
    $cow_status_id = HttpRequest::getParam("cow_status_id");
    $date = HttpRequest::getParam("date");
    
    if($date=="" || $date==null){
        $date = date("Y-m-d");
        //echo $date;
    }
    
    //get lastId
    $qrLastId = "select id from {$dbprefix}ed_cow_has_cow_status ORDER BY id DESC Limit 1 ";
    $restLastId = $db->query($qrLastId);
    $rowLastId = $restLastId->fetch_assoc();
    $id = $rowLastId["id"]+1;
    //echo $id;
    $query = "  INSERT INTO {$dbprefix}ed_cow_has_cow_status (id,cow_id,cow_status_id,status_date,create_date,create_by,ordering,state,checked_out,checked_out_time,created_by) VALUES 
                ('{$id}','{$cow_id}','{$cow_status_id}','{$date}',now(),'{$row["id"]}','0','1','0',now(),'{$row["id"]}')";
   
     //echo $query;
    $result = $db->query($query);

    if ($result) {
        HttpResponse::responseSuccessJson($result);
    } else {
        HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
    }
    $db->close();
}