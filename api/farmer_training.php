<?php
require_once "init.php";

$access_token = HttpRequest::getParam("access_token");
$cmd = HttpRequest::getParam("cmd");

$user_login_id = 0;

$query = "SELECT * FROM {$jConfig->dbprefix}users WHERE access_token='{$access_token}'";
// echo $query;
$result = $db->query($query);

if ($result) {
    $row = $result->fetch_assoc();
    $user_login_id = $row["id"];
    if($cmd == "get_all_training"){
        getAllTrainig($db,$jConfig->dbprefix);
    }else if($cmd == "add_farmer_training"){
        addFarmerTraining($db,$jConfig->dbprefix,$user_login_id);
    }else if($cmd == "update_farmer_training"){
        updateFarmerTraining($db,$jConfig->dbprefix,$user_login_id);
    }else if($cmd == "get_training_info"){
        getTrainingInfo($db,$jConfig->dbprefix);
    }else{
        ListTraining($db,$jConfig->dbprefix);
    }
    //$row = $result->fetch_object();
}

function ListTraining($db,$dbprefix){
    
    $farmer_id = HttpRequest::getParam("farmer_id");
    
    $search_query = "";

    if($farmer_id!=""){
            $search_query .= " ft.farmer_id like '%{$farmer_id}%' and ";
    }

    $query = "	SELECT tp.name AS programName,
                       ft.id AS trainingId,
                       ft.training_date AS trainingDate,
                       ft.training_place AS trainingPlace,
                       ft.training_day AS trainingDay
                            FROM ".$dbprefix."ed_farmer_training AS ft
                LEFT JOIN ".$dbprefix."ed_training_program AS tp ON ft.training_program_id LIKE tp.id
                            WHERE {$search_query} ft.state = 1";
    // echo $query;
    $result = $db->query($query);

    if($result){
         // Cycle through results
        while ($row = $result->fetch_object()){
            $row->trainingDate = formatDate($row->trainingDate);
            $rows[] = $row;
        }
        // Free result set
        $result->close();
        $db->next_result();
    }
    HttpResponse::responseSuccessJson($rows);
    $db->close();
}

function getAllTrainig($db,$dbprefix){
    $query = "SELECT * FROM ".$dbprefix."ed_training_program where state = 1";
     // echo $query;
    $result = $db->query($query);
    
    if($result){
         // Cycle through results
        while ($row = $result->fetch_object()){
            $row->trainingDate = formatDate($row->trainingDate);
            $rows[] = $row;
        }
        // Free result set
        $result->close();
        $db->next_result();
    }
    HttpResponse::responseSuccessJson($rows);
    $db->close();
}

function addFarmerTraining($db,$dbprefix,$user_login_id){
    
    $farmer_id = HttpRequest::getParam("farmer_id");
    $farmer_training_id = HttpRequest::getParam("farmer_training_id");
    $training_date = HttpRequest::getParam("training_date");
    $training_place = HttpRequest::getParam("training_place");
    $training_day = HttpRequest::getParam("training_day");
    
    //get lastId
    $qrLastId = "select id from ".$dbprefix."ed_farmer_training ORDER BY id DESC Limit 1 ";
    $restLastId = $db->query($qrLastId);
    $rowLastId = $restLastId->fetch_assoc();
    $id = $rowLastId["id"]+1;
    
    //add data
    $query = "	insert into ".$dbprefix."ed_farmer_training "
            . " (id,farmer_id,training_program_id,training_date,training_place,training_day,create_by,update_by,create_datetime,update_datetime,ordering,state,checked_out,checked_out_time,created_by) values "
            . " ($id,$farmer_id,$farmer_training_id,'$training_date','$training_place',$training_day,$user_login_id,$user_login_id,now(),now(),0,1,0,now(),$user_login_id) ";
    // echo $query;
    $result = $db->query($query);
    if($result){
        HttpResponse::responseSuccessJson($id);
    }else{
        HttpResponse::responseErrorJson(500,"update error");
    }
}

function updateFarmerTraining($db,$dbprefix){
    
    $id = HttpRequest::getParam("id");
    $farmer_training_id = HttpRequest::getParam("farmer_training_id");
    $training_date = HttpRequest::getParam("training_date");
    $training_place = HttpRequest::getParam("training_place");
    $training_day = HttpRequest::getParam("training_day");
    
    
    //edit data
    $sql = "update {$dbprefix}ed_farmer_training set "
            . " training_program_id = '$farmer_training_id',"
            . " training_date = '$training_date',"
            . " training_place = '$training_place',"
            . " training_day = '$training_day' "
            . " where id = '$id'";
    
    // echo $query;
    $result = $db->query($sql);
    if($result){
        HttpResponse::responseSuccessJson($result);
    }else{
        HttpResponse::responseErrorJson(500,"update error");
    }
}

function getTrainingInfo($db,$dbprefix) {
    $id = HttpRequest::getParam("id");
    

    if($id==""){
        return false;
    }

    $query = "	SELECT * FROM ".$dbprefix."ed_farmer_training  WHERE id = {$id}";
    // echo $query;
    $result = $db->query($query);

    if($result){
         // Cycle through results
        $row = $result->fetch_assoc();
        HttpResponse::responseSuccessJson($row);
    }else{
        HttpResponse::responseErrorJson(500,"พบข้อผิดพลากในการทำงานของระบบ");
    }
    
    $db->close();
}



?>
