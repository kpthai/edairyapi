<?php
session_start();
error_reporting(0);
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
require_once "../configuration.php";
require_once "HttpRequest.class.php";
require_once "HttpResponse.class.php";

$jConfig = new JConfig();
$db = new mysqli($jConfig->host, $jConfig->user, $jConfig->password, $jConfig->db);
$db->set_charset('utf8');
function formatDate($date){

	if($date=="")
		return $date;
  if($date=="0000-00-00")
    return "-";
  $raw_date = explode("-", $date);
  $raw_date[0]+=543;
  return  $raw_date[2] . "/" . $raw_date[1] . "/" . $raw_date[0];
}
function formatSQLDate($date){
  $raw_date = explode("/", $date);
  return $raw_date[2]-543 . "-" . $raw_date[1] . "-" . $raw_date[0];
}

?>
