<?php
require_once "init.php";
$cmd = HttpRequest::getParam("cmd");
$access_token = HttpRequest::getParam("access_token");
$farmer_name = HttpRequest::getParam("farmer_name");
$coop_id = HttpRequest::getParam("coop_id");

$query = "SELECT * FROM {$jConfig->dbprefix}users WHERE access_token='$access_token'";
// echo $query;
$result = $db->query($query);

if ($result) {
    $row = $result->fetch_object();
    
    if($cmd == "qry_farmer_id"){
        
        $search_query = "";
    
        if($row->role == 7 || $row->role == 5){ //admin และ ผู้บริหาร
            $search_query.= " ";
        }else if($row->role == 6 || $row->role == 2){ //ส่วนภูมิภาค และ นักส่งเสริม
            $search_query.= " a.region_id = $row->region_id and ";
        }else if($row->role == 3 || $row->role == 4){ //สหรณ์ ศุนย์รับน้ำนมดิบ
            $search_query.= " b.coop_id = $row->coop_id and ";
        }else{
            $search_query.= " a.farm_id='$row->farm_id' and ";
        }


        if($farmer_name != null && $farmer_name != ""){
                $search_query .= " (a.name like '%$farmer_name%' or a.surname like '%$farmer_name%' or b.member_code like '%$farmer_name%') and ";
        }

        if($coop_id != null && $coop_id != ""){
                $search_query .= " a.farm_id in (select id from {$jConfig->dbprefix}ed_farm f where f.coop_id = '$coop_id' ) and ";
        }



        $query = "	SELECT a.*,b.member_code FROM {$jConfig->dbprefix}ed_farmer a "
                . " join {$jConfig->dbprefix}ed_farm b on b.id = a.farm_id "
                . " where $search_query b.state = 1";

        // echo $query;
        $result = $db->query($query);

        if($result){
             // Cycle through results
            while ($row = $result->fetch_object()){
                $rows[] = $row;
            }
            // Free result set
            $result->close();
            $db->next_result();
        }
        
        $list = array();
        foreach ($rows as $key => $value) {
            array_push($list, array("id" => $value->id,
                                "name" => $value->name." ".$value->surname,
                               "hint" =>  $value->member_code));
        }
        
        HttpResponse::responseSuccessJson($list);
        $db->close();
    }else{
        
        $current_page = HttpRequest::getParam("current_page");
        $cur_page = $current_page * 20;
        $search_query = "";
    
        if($row->role == 7 || $row->role == 5){ //admin และ ผู้บริหาร
            $search_query.= " ";
        }else if($row->role == 6 || $row->role == 2){ //ส่วนภูมิภาค และ นักส่งเสริม
            $search_query.= " a.region_id = $row->region_id and ";
        }else if($row->role == 3 || $row->role == 4){ //สหรณ์ ศุนย์รับน้ำนมดิบ
            $search_query.= " b.coop_id = $row->coop_id and ";
        }else{
            $search_query.= " a.farm_id='$row->farm_id' and ";
        }


        if($farmer_name != null && $farmer_name != ""){
                $search_query .= " (a.name like '%$farmer_name%' or a.surname like '%$farmer_name%' or b.member_code like '%$farmer_name%') and ";
        }

        if($coop_id != null && $coop_id != ""){
                $search_query .= " a.farm_id in (select id from {$jConfig->dbprefix}ed_farm f where f.coop_id = '$coop_id' ) and ";
        }



        $query = "	SELECT a.*,b.member_code FROM {$jConfig->dbprefix}ed_farmer a "
                . " join {$jConfig->dbprefix}ed_farm b on b.id = a.farm_id "
                . " where $search_query b.state = 1 order by b.member_code limit $cur_page,20 ";

        // echo $query;
        $result = $db->query($query);

        if($result){
             // Cycle through results
            while ($row = $result->fetch_object()){
                $rows[] = $row;
            }
            // Free result set
            $result->close();
            $db->next_result();
        }
        //total
        $queryTt = "	SELECT count(*) as total FROM {$jConfig->dbprefix}ed_farmer a "
                . " join {$jConfig->dbprefix}ed_farm b on b.id = a.farm_id "
                . " where $search_query b.state = 1 order by b.member_code ";
        $resultTt = $db->query($queryTt);
        $rowTT = $resultTt->fetch_object();
        $rows[]["total"] = $rowTT->total;
        HttpResponse::responseSuccessJson($rows);
        $db->close();
    }
    
}else{
    HttpResponse::responseErrorJson(404,"พบข้อผิดพลาดในการทำงาน");
}



?>
