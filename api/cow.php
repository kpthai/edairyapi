<?php
require_once "init.php";

$access_token = HttpRequest::getParam("access_token");
$cmd = HttpRequest::getParam("cmd");

$user_login_id = 0;

$query = "SELECT * FROM {$jConfig->dbprefix}users WHERE access_token='{$access_token}'";

$result = $db->query($query);

if ($result) {

    $row = $result->fetch_assoc();
    
    if($cmd == "get_cow_detail"){
        getCowDetail($db,$jConfig->dbprefix,$row);
    }else if($cmd == "update_cow_detail"){
        updateCow($db,$jConfig->dbprefix,$row);
    }else if($cmd == "list_cow_not_vertify"){
        ListCowNotVertify($db,$jConfig->dbprefix,$row);
    }else if($cmd == "list_cow_vertify"){
        ListCowVertify($db,$jConfig->dbprefix,$row);
    }else if($cmd == "update_move_out"){
        updateMoveOut($db,$jConfig->dbprefix,$row);
    }else{
        ListCow($db,$jConfig->dbprefix,$row);
    }
}

function getCowDetail($db,$dbprefix,$row){
    $id = HttpRequest::getParam("id");
    
    $sql = "select * from {$dbprefix}ed_cow where id = {$id} ";
    //print_r($query);
    $result = $db->query($sql);

    if($result){
         // Cycle through results
        $rows = $result->fetch_assoc();
        HttpResponse::responseSuccessJson($rows);
    }else{
        HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
    }
    
    $db->close();
}

function updateCow($db,$dbprefix,$row){
    
    $id = HttpRequest::getParam("id");
    $name = HttpRequest::getParam("name");
    $cow_id = HttpRequest::getParam("cow_id");
    $ear_id = HttpRequest::getParam("ear_id");
    $dld_id = HttpRequest::getParam("dld_id");
    $breed = HttpRequest::getParam("breed");
    $birthdate = HttpRequest::getParam("birthdate");
    $move_farm_date = HttpRequest::getParam("move_farm_date");
    $move_type = HttpRequest::getParam("move_type");
    $from_farm_id = HttpRequest::getParam("from_farm_id");
    $cow_status = HttpRequest::getParam("cow_status");
    
    $sql = "update {$dbprefix}ed_cow set "
            . "name = '$name',"
            . "cow_id = '$cow_id',"
            . "ear_id = '$ear_id',"
            . "dld_id = '$dld_id',"
            . "breed = '$breed',"
            . "birthdate = '$birthdate',"
            . "move_farm_date = '$move_farm_date',"
            . "move_type = '$move_type',"
            . "move_from_farm_id = '$from_farm_id',"
            . "cow_status_id = '$cow_status'"
            . " where id = '$id'";
    
    $result = $db->query($sql);
    if($result){
        HttpResponse::responseSuccessJson($result);
    }else{
        HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
    }
    
    $db->close();
    
}

function updateMoveOut($db,$dbprefix,$row){
    $id = HttpRequest::getParam("id");
    $move_type = HttpRequest::getParam("move_type");
    
    $move_farm_date = HttpRequest::getParam("move_farm_date");
    $discard_select1 = HttpRequest::getParam("discard_select1");
    $discard_select2 = HttpRequest::getParam("$discard_select2");
    $farm_id = HttpRequest::getParam("farm_id");
    
    if($move_type == 1){
        $farm_status_id = 2;
    }else{
        $farm_status_id = 1;
    }
    
    $sql = "update {$dbprefix}ed_cow set "
            . "farm_status_id = '$farm_status_id',"
            . " where id = '$id'";
    
    $result = $db->query($sql);
    
    $sql_get_detail = "select * from {$dbprefix}ed_cow where id = {$id} ";
    //print_r($query);
    $result_get_detail = $db->query($sql_get_detail);
    $row_get_detail = $result_get_detail->fetch_assoc();
    $from_farm_id = $row_get_detail["farm_id"];
    
    //get lastId
    $qrLastId = "select id from {$jConfig->dbprefix}ed_cow_move_out ORDER BY id DESC Limit 1 ";
    $restLastId = $db->query($qrLastId);
    $rowLastId = $restLastId->fetch_assoc();
    $last_id = $rowLastId["id"]+1;

    $query_insert = "  INSERT INTO {$jConfig->dbprefix}ed_cow_move_out (id,cow_id,move_out_date,move_type,from_farm_id,to_farm_id,move_objective,move_reason,ordering,state,created_by) VALUES 
                ('{$last_id}','$id','{$move_farm_date}','{$move_type}','{$from_farm_id}','{$farm_id}','{$discard_select1}','{$discard_select2}','0','1',{$row["id"]})";

     //echo $query;
    $result_insert = $db->query($query_insert);
    
    
    if($result_insert){
        HttpResponse::responseSuccessJson($result_insert);
    }else{
        HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
    }
    
    $db->close();
}

function ListCow($db,$dbprefix,$row){
    $cow_name = HttpRequest::getParam("cow_name");
    $farmer_name = HttpRequest::getParam("farmer_name");
    $coop_id = HttpRequest::getParam("coop_id");
    $farm_id = HttpRequest::getParam("farm_id");
    
    $current_page = HttpRequest::getParam("current_page");
    $cur_page = $current_page * 20;
        
    $search_query = "";
    //print_r($row["role"]);
    if($row["role"] == 7 || $row["role"] == 5){ //admin และ ผู้บริหาร
        $search_query.= " ";
    }else if($row["role"] == 6 || $row["role"] == 2){ //ส่วนภูมิภาค และ นักส่งเสริม
        $search_query.= " b.region_id = ".$row["region_id"]." and ";
    }else if($row["role"] == 3 || $row["role"] == 4){ //สหรณ์ ศุนย์รับน้ำนมดิบ
        $search_query.= " b.coop_id = ".$row["coop_id"]." and ";
    }else{
        $search_query.= " c.farm_id='".$row["farm_id"]."' and ";
    }
    
    if($farm_id!="" && $farm_id!=null){
        $search_query .= " farm_id='{$farm_id}' and ";
    }
    
    if($cow_name!="" && $cow_name!=null){
            $search_query .= " (c.name like '%{$cow_name}%' OR c.cow_id like '%{$cow_name}%') and ";
    }
    if($farmer_name!="" && $farmer_name!=null){
            $search_query .= " c.farm_id in (select farm_id from {$dbprefix}ed_farmer f where f.name like '%{$farmer_name}%' or f.surname like '%{$farmer_name}%' or b.member_code like '%{$farmer_name}%' ) and ";
    }
    if($coop_id!="" && $coop_id!=null){
            $search_query .= " c.farm_id in (select id from {$dbprefix}ed_farm f where f.coop_id like '{$coop_id}' ) and ";
    }



    $query = "	SELECT c.*,cs.name AS cow_status
                            FROM {$dbprefix}ed_cow c
                            left join {$dbprefix}ed_cow_status cs on cs.id = c.cow_status_id
                            join {$dbprefix}ed_farm b on b.id = c.farm_id
                            where {$search_query} c.state = 1 order by c.name limit $cur_page,20 ";
    //print_r($query);
    $result = $db->query($query);

    if($result){
         // Cycle through results
        while ($row = $result->fetch_object()){
            $rows[] = $row;
        }
        // Free result set
        $result->close();
        $db->next_result();
        
    }else{
         HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
    }
    
    
    //total
    $queryTt = "	SELECT count(*) as total FROM {$dbprefix}ed_cow c
                            left join {$dbprefix}ed_cow_status cs on cs.id = c.cow_status_id
                            join {$dbprefix}ed_farm b on b.id = c.farm_id
                            where {$search_query} c.state = 1 ";
    $resultTt = $db->query($queryTt);
    $rowTT = $resultTt->fetch_object();
    $rows[]["total"] = $rowTT->total;
    HttpResponse::responseSuccessJson($rows);
    $db->close();
}

function ListCowNotVertify($db,$dbprefix,$row){
    
    $coop_id = HttpRequest::getParam("coop_id");
    $farm_id = HttpRequest::getParam("farm_id");
    $start_date = HttpRequest::getParam("start_date");
    

    $search_query = "";
    $where_query = "";
    if($row["role"] == 7 || $row["role"] == 5){ //admin และ ผู้บริหาร
        $where_query.= " ";
    }else if($row["role"] == 6 || $row["role"] == 2){ //ส่วนภูมิภาค และ นักส่งเสริม
        $where_query.= " b.region_id = ".$row["region_id"]." and ";
    }else if($row["role"] == 3 || $row["role"] == 4){ //สหรณ์ ศุนย์รับน้ำนมดิบ
        $where_query.= " b.coop_id = ".$row["coop_id"]." and ";
    }else{
        $where_query.= " cd.farm_id='".$row["farm_id"]."' and ";
    }
    
    if($farm_id!="" && $farm_id!=null){
        $search_query .= " WHERE farm_id='{$farm_id}' ";
    }
    
    if($coop_id!="" && $coop_id!=null){
            $search_query .= " AND farm_id in (select id from {$dbprefix}ed_farm f where f.coop_id like '{$coop_id}' )";
    }

    if($start_date!="" && $start_date!=null){
        $end_date = date("Y-m-d");
        $search_query_date = " AND chc.status_date BETWEEN '{$start_date}' AND '{$end_date}' ";
    }


    $query = "	select cd.* from (SELECT c.*,(SELECT name FROM {$dbprefix}ed_cow_status WHERE id=chc.cow_status_id) AS cow_status,
                            (SELECT CONCAT(CONCAT(percent, '%'),breed) FROM {$dbprefix}ed_cow_breed WHERE cow_id=c.cow_id ORDER BY percent DESC LIMIT 1 ) AS cow_breed
                            FROM {$dbprefix}ed_cow c
                            left join {$dbprefix}ed_cow_has_cow_status chc on chc.cow_id = c.id 
                            {$search_query} group by c.id) cd  
                            join farm b on b.id = cd.farm_id
                            where {$where_query} cd.id not in  (SELECT c.id FROM {$dbprefix}ed_cow c
                            join {$dbprefix}ed_cow_has_cow_status chc on chc.cow_id = c.id {$search_query_date}
                            {$search_query} group by c.id )";
    //print_r($query);
    $result = $db->query($query);

    if($result){
         // Cycle through results
        while ($row = $result->fetch_object()){
            $rows[] = $row;
        }
        // Free result set
        $result->close();
        $db->next_result();
        HttpResponse::responseSuccessJson($rows);
    }else{
         HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
    }
    
    $db->close();
}

function ListCowVertify($db,$dbprefix,$row){
    
    $coop_id = HttpRequest::getParam("coop_id");
    $farm_id = HttpRequest::getParam("farm_id");
    $start_date = HttpRequest::getParam("start_date");
    

    $search_query = "";
    if($farm_id!="" && $farm_id!=null){
        $search_query .= " WHERE farm_id='{$farm_id}' ";
    }
    
    if($coop_id!="" && $coop_id!=null){
            $search_query .= " AND farm_id in (select id from {$dbprefix}ed_farm f where f.coop_id like '{$coop_id}' )";
    }

    if($start_date!="" && $start_date!=null){
        $end_date = date("Y-m-d");
        $search_query_date = " AND chc.status_date BETWEEN '{$start_date}' AND '{$end_date}' ";
    }


    $query = "	select cd.* from (SELECT c.*,(SELECT name FROM {$dbprefix}ed_cow_status WHERE id=chc.cow_status_id) AS cow_status,
                            (SELECT CONCAT(CONCAT(percent, '%'),breed) FROM {$dbprefix}ed_cow_breed WHERE cow_id=c.cow_id ORDER BY percent DESC LIMIT 1 ) AS cow_breed
                            FROM {$dbprefix}ed_cow c
                            left join {$dbprefix}ed_cow_has_cow_status chc on chc.cow_id = c.id 
                            {$search_query} group by c.id) cd  where cd.id in  (SELECT c.id FROM {$dbprefix}ed_cow c
                            join {$dbprefix}ed_cow_has_cow_status chc on chc.cow_id = c.id {$search_query_date}
                            {$search_query} group by c.id )";
    //print_r($query);
    $result = $db->query($query);

    if($result){
         // Cycle through results
        while ($row = $result->fetch_object()){
            $rows[] = $row;
        }
        // Free result set
        $result->close();
        $db->next_result();
        HttpResponse::responseSuccessJson($rows);
    }else{
         HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
    }
    
    $db->close();
}

?>
