<?php

require_once "init.php";

$access_token = HttpRequest::getParam("access_token");
$cmd = HttpRequest::getParam("cmd");

$user_login_id = 0;

$query = "SELECT * FROM {$jConfig->dbprefix}users WHERE access_token='{$access_token}'";

$result = $db->query($query);

if ($result) {

    $row = $result->fetch_assoc();
    
    if($cmd == "get_cow_detail"){
        getCowDetail($db,$jConfig->dbprefix,$row);
    }else if($cmd == "update_cow_detail"){
        updateCow($db,$jConfig->dbprefix,$row);
    }else{
        ListBreeder($db,$jConfig->dbprefix,$row);
    }
}

function ListBreeder($db,$dbprefix,$row){


    $query = "SELECT * FROM {$dbprefix}ed_breeder WHERE state='1' ";
    //print_r($query);
    $result = $db->query($query);

    if($result){
         // Cycle through results
        while ($row = $result->fetch_object()){
            $rows[] = $row;
        }
        // Free result set
        $result->close();
        $db->next_result();
        HttpResponse::responseSuccessJson($rows);
    }else{
         HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
    }
    
    $db->close();
}