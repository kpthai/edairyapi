<?php
require_once "init.php";

$access_token = HttpRequest::getParam("access_token");
$cmd = HttpRequest::getParam("cmd");

$user_login_id = 0;

$query = "SELECT * FROM {$jConfig->dbprefix}users WHERE access_token='{$access_token}'";

$result = $db->query($query);

if ($result) {
    $row = $result->fetch_assoc();
    if($cmd == "get_breed_list"){
        getBreedingList($db,$jConfig->dbprefix,$row);
    }else if($cmd == "add_breeding_service"){
        addBreeding($db,$jConfig->dbprefix,$row);
    }else if($cmd == "edit_breeding_service"){
        updateBreeding($db,$jConfig->dbprefix,$row);
    }else if($cmd == "add_breeding_detail"){
        addBreedingLine($db,$jConfig->dbprefix,$row);
    }else if($cmd == "edit_breeding_detail"){
        updateBreedingLine($db,$jConfig->dbprefix,$row);
    }else if($cmd == "get_breeding_detail"){
        getBreedingLine($db,$jConfig->dbprefix,$row);
    }
}

function getBreedingList($db,$dbprefix,$row){
    $id = HttpRequest::getParam("id");
    $search_query = "";

    // if($_GET["farmer_name"]!=""){
    // 	$search_query .= " AND (fm.name like '%{$_GET[farmer_name]}%' or surname like '%{$_GET[farmer_name]}%' or citizen_id like '{$_GET[farmer_name]}')";
    // }

    if($id!="" && $id !=null){
            $search_query .= "  isml.cow_insemination_id like '{$id}' ";
    }else{
             $search_query .= "  1 ";
    }

    $query = "	SELECT *, f.name as farm_name,c.name as cow_name,cs.name as cow_status,isml.id as isml_id,ism.create_date as pay_date,
                ism.payment_type as ism_payment_type,ism.created_by as created_by

                            FROM {$dbprefix}ed_cow_insemination ism
                                join {$dbprefix}ed_cow_insemination_line isml on isml.cow_insemination_id = ism.id
                                join {$dbprefix}ed_cow c on c.id = isml.cow_id
                                join {$dbprefix}ed_cow_status cs on cs.id = c.cow_status_id
                LEFT JOIN {$dbprefix}ed_farm f ON f.id = ism.farm_id
                LEFT JOIN {$dbprefix}ed_farmer fm ON fm.id = farmer_id
                            WHERE {$search_query}";
     //echo $query;
    $result = $db->query($query);

    if($result){
         // Cycle through results
        while ($row = $result->fetch_object()){
            $row->create_date = formatDate($row->create_date);
            $rows[] = $row;
        }
        // Free result set
        $result->close();
        $db->next_result();
        HttpResponse::responseSuccessJson($rows);
    }else{
        HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
    }
    
    $db->close();
}

function addBreeding($db,$dbprefix,$row){
    
    //$coop_id = HttpRequest::getParam("coop_id");
    
    //get lastId
    $qrLastId = "select id from {$dbprefix}ed_cow_insemination ORDER BY id DESC Limit 1 ";
    $restLastId = $db->query($qrLastId);
    $rowLastId = $restLastId->fetch_assoc();
    $id = $rowLastId["id"]+1;
    
    $query = "  INSERT INTO {$dbprefix}ed_cow_insemination (id,run_no,create_date,address,payment_type,user_id1,user_id2,grand_total,farm_id,farmer_id,coop_id,
                ordering,state,checked_out,checked_out_time,created_by,invoice_type,invoice_no) VALUES 
                ('{$id}','0',now(),'0','0','0','0','0','0','0','0','0','0','0',now(),'{$row["id"]}','0','0') ";
   
    // echo $query;
    $result = $db->query($query);

    if ($result) {
        HttpResponse::responseSuccessJson($id);
    } else {
        HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
    }
    $db->close();
}

function updateBreeding($db,$dbprefix,$row){
    
    $id = HttpRequest::getParam("id");
    $pay_no = HttpRequest::getParam("pay_no");
    $pay_date = HttpRequest::getParam("pay_date");
    $pay_type = HttpRequest::getParam("pay_type");
    $total_money = HttpRequest::getParam("total_money");
    $farmer_id = HttpRequest::getParam("farmer_name");
    $coop_id = HttpRequest::getParam("coop_id");
    
    
    
    //get farm
    $qrfarm = "select farm_id from {$dbprefix}ed_farmer where id = $farmer_id ";
    $restfarm = $db->query($qrfarm);
    $rowfarm = $restfarm->fetch_assoc();
    $farm_id = $rowfarm["farm_id"];
    
    if($pay_no == null || $pay_no == ""){
        //get abbr
        $qrabbr = "select coop_abbr from {$dbprefix}ed_coop where id = $coop_id ";
        $restabbr = $db->query($qrabbr);
        $rowabbr = $restabbr->fetch_assoc();
        if($rowabbr == false){
            $rowabbr["coop_abbr"] = "";
        }
        $coop_abbr = $rowabbr["coop_abbr"].substr(date('Y')+543,2);
        
        //echo $rowabbr["coop_abbr"];

        //get lastId
        $qrLastId = "select run_no from {$dbprefix}ed_cow_insemination where run_no like '%".$coop_abbr."%' ORDER BY id DESC Limit 1 ";
        $restLastId = $db->query($qrLastId);
        $rowLastId = $restLastId->fetch_assoc();
        if($restLastId->num_rows != false){
            $rn = substr($rowLastId["run_no"],4);
        }else{
            $rn = 0;
        }
        $last_run_no = str_pad(intval($rn)+1,6,"0",STR_PAD_LEFT);

        $run_no = $coop_abbr.$last_run_no;
    }else{
        $run_no = $pay_no;
    }
    
   
    $sql = "update {$dbprefix}ed_cow_insemination set "
            . "run_no = '$run_no',"
            . "payment_type = '$pay_type',"
            . "user_id1 = '$farmer_id',"
            . "user_id2 = '".$row["id"]."',"
            . "grand_total = '$total_money',"
            . "farm_id = '$farm_id',"
            . "farmer_id = '$farmer_id',"
            . "coop_id = '$coop_id',"
            . "create_date = '$pay_date',"
            . "state = '1'"
            . " where id = '$id'";
     //echo $sql;
    $result = $db->query($sql);
    
    //
    //get ism_line
    //
    $queryline = "SELECT * FROM {$dbprefix}ed_cow_insemination_line WHERE cow_insemination_id= '$id' ";
        //echo $query;
    $resultline = $db->query($queryline);
    if($resultline){
         // Cycle through results
        //update state cow_pregnant_inspection
        while ($row = $resultline->fetch_object()){
            $ism_line_id = $row->id;
             $sqlpn = "update {$dbprefix}ed_cow_pregnant_inspection set "
            . "state = '1'"
            . " where cow_insemination_line_id = '$ism_line_id'";
    
            // echo $query;
            $resultpn = $db->query($sqlpn);

            if ($resultpn == false) {
                HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
            }
        }
        // Free result set
        //$resultpn->close();
        //$db->next_result();
    }else{
        HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
    }
       

    if ($result) {
        HttpResponse::responseSuccessJson($id);
    } else {
        HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
    }
    $db->close();
}

function addBreedingLine($db,$dbprefix,$row){
    
    $cow_id = HttpRequest::getParam("cow_id");
    $men_interval = HttpRequest::getParam("men_interval");
    $insemination_time = HttpRequest::getParam("insemination_time");
    $lot_no = HttpRequest::getParam("lot_no");
    $amount = HttpRequest::getParam("amount");
    $price = HttpRequest::getParam("price");
    $total_price = HttpRequest::getParam("total_price");
    $cow_insemination_id = HttpRequest::getParam("cow_insemination_id");
    
    
    //get lastId
    $qrLastId = "select id from {$dbprefix}ed_cow_insemination_line ORDER BY id DESC Limit 1 ";
    $restLastId = $db->query($qrLastId);
    $rowLastId = $restLastId->fetch_assoc();
    $id = $rowLastId["id"]+1;
    
    $query = "  INSERT INTO {$dbprefix}ed_cow_insemination_line (id,cow_id,create_date,men_interval,insemination_time,lot_no,amount,price,total_price,cow_insemination_id,
                ordering,state,checked_out,checked_out_time,created_by,cow_status_id) VALUES 
                ('{$id}','{$cow_id}',now(),'{$men_interval}','{$insemination_time}','{$lot_no}','{$amount}','{$price}','{$total_price}','{$cow_insemination_id}','0','1','0',now(),'{$row["id"]}','0') ";
   
    // echo $query;
    $result = $db->query($query);
    
    //get lastId pn
    $qrLastIdpn = "select id from {$dbprefix}ed_cow_pregnant_inspection ORDER BY id DESC Limit 1 ";
    $restLastIdpn = $db->query($qrLastIdpn);
    $rowLastIdpn = $restLastIdpn->fetch_assoc();
    $pnid = $rowLastIdpn["id"]+1;
    
    $date1 = date("Y-m-d", strtotime("+18 days"));
    $date2 = date("Y-m-d", strtotime("+39 days"));
    $date3 = date("Y-m-d", strtotime("+60 days"));
    
    $date1_to = date("Y-m-d", strtotime("+24 days"));
    $date2_to = date("Y-m-d", strtotime("+45 days"));
    $date3_to = date("Y-m-d", strtotime("+66 days"));
    
    $querypn = "  INSERT INTO {$dbprefix}ed_cow_pregnant_inspection (id,cow_id,cow_insemination_line_id,insemination_date,date1,date2,date3,date1_to,date2_to,date3_to,
                ordering,state,checked_out,checked_out_time,created_by) VALUES 
                ('{$pnid}','{$cow_id}','{$id}',now(),'{$date1}','{$date2}','{$date3}','{$date1_to}','{$date2_to}','{$date3_to}','0','0','0',now(),'{$row["id"]}') ";
   
    // echo $query;
    $resultpn = $db->query($querypn);

    if ($result) {
        HttpResponse::responseSuccessJson($id);
    } else {
        HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
    }
    $db->close();
}

function updateBreedingLine($db,$dbprefix,$row){
    
    $id = HttpRequest::getParam("id");
    $cow_id = HttpRequest::getParam("cow_id");
    $men_interval = HttpRequest::getParam("men_interval");
    $insemination_time = HttpRequest::getParam("insemination_time");
    $lot_no = HttpRequest::getParam("lot_no");
    $amount = HttpRequest::getParam("amount");
    $price = HttpRequest::getParam("price");
    $total_price = HttpRequest::getParam("total_price");
    
    $sql = "update {$dbprefix}ed_cow_insemination_line set "
            . "cow_id = '$cow_id',"
            . "men_interval = '$men_interval',"
            . "insemination_time = '$insemination_time',"
            . "lot_no = '$lot_no',"
            . "amount = '$amount',"
            . "price = '$price',"
            . "total_price = '$total_price'"
            . " where id = '$id'";
    
    // echo $query;
    $result = $db->query($sql);

    if ($result) {
        HttpResponse::responseSuccessJson($id);
    } else {
        HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
    }
    $db->close();
}

function getBreedingLine($db,$dbprefix,$row){
    $id = HttpRequest::getParam("id");
    
    if($id!="" && $id !=null){
            $search_query .= "  isml.id like '{$id}' ";
            
            $query = "	SELECT *,c.farm_id as cfarm_id , f.name as farm_name,c.name as cow_name,cs.name as cow_status,isml.id as isml_id,
                        isml.cow_id as cow_id

                            FROM {$dbprefix}ed_cow_insemination ism
                                join {$dbprefix}ed_cow_insemination_line isml on isml.cow_insemination_id = ism.id
                                join {$dbprefix}ed_cow c on c.id = isml.cow_id
                                join {$dbprefix}ed_cow_status cs on cs.id = c.cow_status_id
                LEFT JOIN {$dbprefix}ed_farm f ON f.id = c.farm_id
                LEFT JOIN {$dbprefix}ed_farmer fm ON fm.id = farmer_id
                            WHERE {$search_query}";
        //echo $query;
       $result = $db->query($query);

       if($result){
            // Cycle through results
           $row = $result->fetch_assoc();
           HttpResponse::responseSuccessJson($row);
       }else{
            HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
        }
    }else{
        HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
    }
    
    $db->close();
}

?>
