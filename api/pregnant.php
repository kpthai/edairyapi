<?php
require_once "init.php";

$access_token = HttpRequest::getParam("access_token");
$cmd = HttpRequest::getParam("cmd");

$user_login_id = 0;

$query = "SELECT * FROM {$jConfig->dbprefix}users WHERE access_token='{$access_token}'";

$result = $db->query($query);

if ($result) {

    $row = $result->fetch_assoc();
    if($cmd == "get_pregnant_detail"){
        getPregnantInfo($db,$jConfig->dbprefix,$row);
    }else if($cmd == "edit_pregnant"){
        updatePregnant($db,$jConfig->dbprefix,$row);
    }else{
        getPregnantList($db,$jConfig->dbprefix,$row);
    }
}

function getPregnantList($db,$dbprefix,$row){
    
    $farmer_name = HttpRequest::getParam("farmer_name");
    $coop_id = HttpRequest::getParam("coop_id");
    
    $search_query = "";

    if($farmer_name!="" && $farmer_name!=null){
            $search_query .= " AND (fm.name like '%{$farmer_name}%' or fm.surname like '%{$farmer_name}%' or fm.citizen_id like '{$farmer_name}')";
    }

    if($coop_id!="" && $coop_id!=null){
            $search_query .= " AND f.id in (select id from {$dbprefix}ed_farm f where f.coop_id like '{$coop_id}' )";
    }

    $query = "	SELECT *, cw.name as cowName, cpi.id as cpiID

                            FROM {$dbprefix}ed_cow_pregnant_inspection as cpi
                LEFT JOIN {$dbprefix}ed_cow cw ON cw.id = cpi.cow_id
                LEFT JOIN {$dbprefix}ed_farm f ON f.id = cw.farm_id
                LEFT JOIN {$dbprefix}ed_farmer fm ON fm.farm_id = f.id
                LEFT JOIN {$dbprefix}ed_coop cp ON cp.id = f.coop_id
                            WHERE f.id='{$row["farm_id"]}' and cpi.state='1' {$search_query}"; /*{$row->farm_id}*/
    // echo $query;
    $result = $db->query($query);

    if($result){
         // Cycle through results
        while ($row = $result->fetch_object()){
            $row->create_date = formatDate($row->create_date);
            $row->insemination_date = formatDate($row->insemination_date);
            $row->date1 = formatDate($row->date1);
            $row->date2 = formatDate($row->date2);
            $row->date3 = formatDate($row->date3);
            $row->date1_to = formatDate($row->date1_to);
            $row->date2_to = formatDate($row->date2_to);
            $row->date3_to = formatDate($row->date3_to);
            $rows[] = $row;
        }
        // Free result set
        $result->close();
        $db->next_result();
    }
    
    //total
    $queryTt = "	SELECT count(*) as total FROM {$dbprefix}ed_cow_pregnant_inspection as cpi
                LEFT JOIN {$dbprefix}ed_cow cw ON cw.id = cpi.cow_id
                LEFT JOIN {$dbprefix}ed_farm f ON f.id = cw.farm_id
                LEFT JOIN {$dbprefix}ed_farmer fm ON fm.farm_id = f.id
                LEFT JOIN {$dbprefix}ed_coop cp ON cp.id = f.coop_id
                            WHERE f.id='{$row["farm_id"]}' and cpi.state='1' {$search_query} ";
    $resultTt = $db->query($queryTt);
    $rowTT = $resultTt->fetch_object();
    $rows[]["total"] = $rowTT->total;
    HttpResponse::responseSuccessJson($rows);
    $db->close();
}

function getPregnantInfo($db,$dbprefix,$row){
    
    $id = HttpRequest::getParam("id");
    
    if($id!="" && $id!=null){
        $query = "	SELECT *, cw.name as cowName, cpi.id as cpiID

                                FROM {$dbprefix}ed_cow_pregnant_inspection cpi
                    LEFT JOIN {$dbprefix}ed_cow cw ON cw.id = cpi.cow_id
                    LEFT JOIN {$dbprefix}ed_farm f ON f.id = cw.farm_id
                    LEFT JOIN {$dbprefix}ed_farmer fm ON fm.farm_id = f.id
                    LEFT JOIN {$dbprefix}ed_coop cp ON cp.id = f.coop_id
                                WHERE cpi.id = $id and cpi.state='1'"; /*{$row->farm_id}*/
        // echo $query;
        $result = $db->query($query);
        if ($result) {
            $row = $result->fetch_assoc();
            HttpResponse::responseSuccessJson($row);
        } else {
            HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
        }
    }else{
         HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
    }
    
}

function updatePregnant($db,$dbprefix,$row){
    $id = HttpRequest::getParam("id");
    $check_date = HttpRequest::getParam("check_date");
    $result_pn = HttpRequest::getParam("result");
    
    $sql = "update {$dbprefix}ed_cow_pregnant_inspection set "
            . "create_date = '$check_date',"
            . "inspection_result = '$result_pn'"
            . " where id = '$id'";
    
    // echo $query;
    $result = $db->query($sql);
    
    if($result_pn == "1"){
        //check ว่ามีข้อมูลอยู่แล้วหรือไม่ 
        $query = "SELECT * FROM {$dbprefix}ed_cow_stop_milking  WHERE cow_pregnant_inspection_id = $id"; /*{$row->farm_id}*/
        //echo $query;
       $resultsm = $db->query($query);
       if($resultsm->num_rows == 0){//ยังไม่มีข้อมูลให้ add เพิ่ม

            $querycpn = "	SELECT * FROM {$dbprefix}ed_cow_pregnant_inspection  WHERE id = $id"; /*{$row->farm_id}*/
            //echo $query;
            $resultcpn = $db->query($querycpn);
            $rowcpn = $resultcpn->fetch_assoc();
            $cow_id = $rowcpn["cow_id"];
            $insemination_date = $rowcpn["insemination_date"];

           //get lastId sm
            $qrLastIdsm = "select id from {$dbprefix}ed_cow_stop_milking ORDER BY id DESC Limit 1 ";
            $restLastIdsm = $db->query($qrLastIdsm);
            $rowLastIdsm = $restLastIdsm->fetch_assoc();
            $smid = $rowLastIdsm["id"]+1;

            $create_date = date("Y-m-d", strtotime($insemination_date."+210 days")); //วันหยุดรีดนม
            $birth_date = date("Y-m-d", strtotime($insemination_date."+270 days")); //วันคลอด

            $querysm = "  INSERT INTO {$dbprefix}ed_cow_stop_milking (id,cow_id,cow_pregnant_inspection_id,create_date,birth_date,actual_date,create_by,
                        ordering,state,checked_out,checked_out_time,created_by) VALUES 
                        ('{$smid}','{$cow_id}','{$id}','{$create_date}','{$birth_date}','0000-00-00','{$row["id"]}','0','1','0',now(),'{$row["id"]}') ";

            // echo $query;
            $resultsm = $db->query($querysm);
       }
    }
    if ($result) {
        HttpResponse::responseSuccessJson($id);
    } else {
        HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
    }
    $db->close();
}

?>
