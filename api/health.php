<?php
require_once "init.php";

$access_token = HttpRequest::getParam("access_token");
$cmd = HttpRequest::getParam("cmd");

$user_login_id = 0;

$query = "SELECT * FROM {$jConfig->dbprefix}users WHERE access_token='{$access_token}'";

$result = $db->query($query);

if ($result) {

    $row = $result->fetch_assoc();
    if($cmd == "get_health_line_list"){
        getHealthLineList($db,$jConfig->dbprefix,$row);
    }else if($cmd == "add_veter_service"){
        addVeterinaryService($db,$jConfig->dbprefix,$row);
    }else{
        ListVeterinaryService($db,$jConfig->dbprefix,$row);
    }
}

function ListVeterinaryService($db,$dbprefix,$row){
    $farmer_name = HttpRequest::getParam("farmer_name");
    $coop_id = HttpRequest::getParam("coop_id");
    $start_date = HttpRequest::getParam("start_date");
    $end_date = HttpRequest::getParam("end_date");
    
    $search_query = "";
    
    if($row["role"] == 7 || $row["role"] == 5){ //admin และ ผู้บริหาร
        $search_query.= " ";
    }else if($row["role"] == 6 || $row["role"] == 2){ //ส่วนภูมิภาค และ นักส่งเสริม
        $search_query.= " f.region_id = ".$row["region_id"]." and ";
    }else if($row["role"] == 3 || $row["role"] == 4){ //สหรณ์ ศุนย์รับน้ำนมดิบ
        $search_query.= " h.coop_id = ".$row["coop_id"]." and ";
    }else{
        $search_query.= " h.farm_id='".$row["farm_id"]."' and ";
    }

    if($farmer_name!=""){
        $search_query .= "  farmer_id in (select id from {$dbprefix}ed_farmer f where f.name like '%{$farmer_name}%' or f.surname like '%{$farmer_name}%' or f.citizen_id like '{$farmer_name}' ) AND ";
    }

    if($coop_id!=""){
        $search_query .= "  farmer_id in (select id from {$dbprefix}ed_farmer where farm_id in (select id from {$dbprefix}ed_farm f where f.coop_id like '{$coop_id}' )) AND ";
    }

    if($start_date!="" && $end_date!=""){
        $search_query .= "  create_date BETWEEN '{$start_date}' AND '{$end_date}' AND ";
    }



    $query = "	SELECT h.*, (SELECT COUNT(*) FROM {$dbprefix}ed_cow_health_line hl WHERE cow_health_id=h.id) as cows_amount, f.name, f.surname
                            FROM {$dbprefix}ed_cow_health h
                LEFT JOIN {$dbprefix}ed_farmer f ON f.id = h.farmer_id
                            where {$search_query}  h.state='1' ";
     //echo $query;
    $result = $db->query($query);

    if($result){
         // Cycle through results
        while ($row = $result->fetch_object()){
            $rows[] = $row;
        }
        // Free result set
        $result->close();
        $db->next_result();
    }
    //total
    $queryTt = "	SELECT count(*) as total FROM {$dbprefix}ed_cow_health h
                LEFT JOIN {$dbprefix}ed_farmer f ON f.id = h.farmer_id
                            where {$search_query}  h.state='1' ";
                            //echo $queryTt;
    $resultTt = $db->query($queryTt);
    $rowTT = $resultTt->fetch_object();
    $rows[]["total"] = $rowTT->total;
    HttpResponse::responseSuccessJson($rows);
    
    $db->close();
}

function getHealthLineList($db,$dbprefix,$row){
    
    $id = HttpRequest::getParam("id");
    
    $query = "	SELECT * FROM {$dbprefix}ed_cow_health_line hl
                JOIN {$dbprefix}ed_cow c ON c.id = hl.cow_id
                JOIN {$dbprefix}ed_cow_health_service hs ON hs.id = hl.cow_health_service_id
                JOIN {$dbprefix}ed_cow_health_sub_service hss ON hss.id = hl.cow_health_sub_service_id
                where hl.cow_health_id = $id and hl.state='1'";
     //echo $query;
    $result = $db->query($query);

    if($result){
         // Cycle through results
        while ($row = $result->fetch_object()){
            $rows[] = $row;
        }
        // Free result set
        $result->close();
        $db->next_result();
        HttpResponse::responseSuccessJson($rows);
    }else{
         HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
    }
    $db->close();
}

function addVeterinaryService($db,$dbprefix,$row){
    
    //$coop_id = HttpRequest::getParam("coop_id");
    
    //get lastId
    $qrLastId = "select id from {$dbprefix}ed_cow_health ORDER BY id DESC Limit 1 ";
    $restLastId = $db->query($qrLastId);
    $rowLastId = $restLastId->fetch_assoc();
    $id = $rowLastId["id"]+1;
    
    $query = "  INSERT INTO {$dbprefix}ed_cow_health (id,run_no,create_date,address,payment_type,user_id1,user_id2,grand_total,farm_id,farmer_id,coop_id,
                ordering,state,checked_out,checked_out_time,created_by,invoice_type) VALUES 
                ('{$id}','0',now(),'0','0','0','0','0','0','0','0','0','0','0',now(),'{$row["id"]}','0') ";
   
     //echo $query;
    $result = $db->query($query);

    if ($result) {
        HttpResponse::responseSuccessJson($id);
    } else {
        HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
    }
    $db->close();
}
?>