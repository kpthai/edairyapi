<?php
require_once "init.php";

$access_token = HttpRequest::getParam("access_token");
$cmd = HttpRequest::getParam("cmd");

$user_login_id = 0;

$query = "SELECT * FROM {$jConfig->dbprefix}users WHERE access_token='{$access_token}'";

$result = $db->query($query);

if ($result) {

    $row = $result->fetch_assoc();
    if($cmd == "get_birth_detail"){
        getBirthInfo($db,$jConfig->dbprefix,$row);
    }else if($cmd == "edit_birth"){
        updateBirth($db,$jConfig->dbprefix,$row);
    }else{
        getBirthList($db,$jConfig->dbprefix,$row);
    }
}

function getBirthList($db,$dbprefix,$row){

    $farmer_name = HttpRequest::getParam("farmer_name");
    $coop_id = HttpRequest::getParam("coop_id");
    
    $search_query = "";

    if($farmer_name!="" && $farmer_name!=null){
            $search_query .= " AND (fm.name like '%{$farmer_name}%' or fm.surname like '%{$farmer_name}%' or fm.citizen_id like '{$farmer_name}')";
    }

    if($coop_id!="" && $coop_id!=null){
            $search_query .= " AND f.id in (select id from {$dbprefix}ed_farm f where f.coop_id like '{$coop_id}' )";
    }

    $query = "	SELECT *, cw.name as cowName,cbi.id as cbiId

                            FROM {$dbprefix}ed_cow_birth_inspection as cbi
                LEFT JOIN {$dbprefix}ed_cow cw ON cw.id = cbi.cow_id
                LEFT JOIN {$dbprefix}ed_farm f ON f.id = cw.farm_id
                LEFT JOIN {$dbprefix}ed_farmer fm ON fm.farm_id = f.id
                LEFT JOIN {$dbprefix}ed_coop cp ON cp.id = f.coop_id
                            WHERE f.id='{$row["farm_id"]}' {$search_query} order by cbi.id DESC"; /*{$row->farm_id}*/
     //echo $query;
    $result = $db->query($query);

    if($result){
         // Cycle through results
        while ($row = $result->fetch_object()){
            $row->create_date = formatDate($row->create_date);
            $rows[] = $row;
        }
        // Free result set
        $result->close();
        $db->next_result();
    }
    //total
    $queryTt = "	SELECT count(*) as total FROM {$dbprefix}ed_cow_birth_inspection as cbi
                LEFT JOIN {$dbprefix}ed_cow cw ON cw.id = cbi.cow_id
                LEFT JOIN {$dbprefix}ed_farm f ON f.id = cw.farm_id
                LEFT JOIN {$dbprefix}ed_farmer fm ON fm.farm_id = f.id
                LEFT JOIN {$dbprefix}ed_coop cp ON cp.id = f.coop_id
                            WHERE f.id='{$row["farm_id"]}' {$search_query} order by cbi.id DESC";
    $resultTt = $db->query($queryTt);
    $rowTT = $resultTt->fetch_object();
    $rows[]["total"] = $rowTT->total;
    HttpResponse::responseSuccessJson($rows);
    
    $db->close();
}

function getBirthInfo($db,$dbprefix,$row){
    
    $id = HttpRequest::getParam("id");
    
    if($id!="" && $id!=null){
        $query = "	SELECT *, cw.name as cowName, cbi.id as cbiID

                                FROM {$dbprefix}ed_cow_birth_inspection cbi
                    LEFT JOIN {$dbprefix}ed_cow cw ON cw.id = cbi.cow_id
                    LEFT JOIN {$dbprefix}ed_farm f ON f.id = cw.farm_id
                    LEFT JOIN {$dbprefix}ed_farmer fm ON fm.farm_id = f.id
                    LEFT JOIN {$dbprefix}ed_coop cp ON cp.id = f.coop_id
                                WHERE cbi.id = $id"; /*{$row->farm_id}*/
         //echo $query;
        $result = $db->query($query);
        if ($result) {
            $row = $result->fetch_assoc();
            HttpResponse::responseSuccessJson($row);
        } else {
            HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
        }
    }else{
         HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
    }
    
}

function updateBirth($db,$dbprefix,$row){
    $id = HttpRequest::getParam("id");
    $check_date = HttpRequest::getParam("check_date");
    $child_weight = HttpRequest::getParam("child_weight");
    $birth_method = HttpRequest::getParam("birth_method");
    $is_child_alive = HttpRequest::getParam("is_child_alive");
    $child_gender = HttpRequest::getParam("child_gender");
    $child_cow_id = HttpRequest::getParam("child_cow_id");
    
    $sql = "update {$dbprefix}ed_cow_birth_inspection set "
            . "birth_date = '$check_date',"
            . "child_weight = '$child_weight',"
            . "birth_method = '$birth_method',"
            . "is_child_alive = '$is_child_alive',"
            . "child_gender = '$child_gender',"
            . "child_cow_id = '$child_cow_id'"
            . " where id = '$id'";
    
    // echo $query;
    $result = $db->query($sql);
    
    

    if ($result) {
        HttpResponse::responseSuccessJson($id);
    } else {
        HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
    }
    $db->close();
}
?>
