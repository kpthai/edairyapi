<?php
require_once "init.php";

$access_token = HttpRequest::getParam("access_token");
$cmd = HttpRequest::getParam("cmd");

$user_login_id = 0;

$query = "SELECT * FROM {$jConfig->dbprefix}users WHERE access_token='{$access_token}'";

$result = $db->query($query);

if ($result) {

    $row = $result->fetch_assoc();
    if($cmd == "get_cow_detail"){
        getCowDetail($db,$jConfig->dbprefix,$row);
    }else if($cmd == "update_cow_detail"){
        updateCow($db,$jConfig->dbprefix,$row);
    }else{
        ListCowInsemination($db,$jConfig->dbprefix,$row);
    }
}

function ListCowInsemination($db,$dbprefix,$row){
    $farmer_name = HttpRequest::getParam("farmer_name");
    $coop_id = HttpRequest::getParam("coop_id");
    $start_date = HttpRequest::getParam("start_date");
    $end_date = HttpRequest::getParam("end_date");
    
    $search_query = "";
    
    if($row["role"] == 7 || $row["role"] == 5){ //admin และ ผู้บริหาร
        $search_query.= " ";
    }else if($row["role"] == 6 || $row["role"] == 2){ //ส่วนภูมิภาค และ นักส่งเสริม
        $search_query.= " f.region_id = ".$row["region_id"]." and ";
    }else if($row["role"] == 3 || $row["role"] == 4){ //สหรณ์ ศุนย์รับน้ำนมดิบ
        $search_query.= " f.coop_id = ".$row["coop_id"]." and ";
    }else{
        $search_query.= " ism.farm_id='".$row["farm_id"]."' and ";
    }

    if($farmer_name!="" && $farmer_name!=null){
            $search_query .= "  (fm.name like '%{$farmer_name}%' or surname like '%{$farmer_name}%' or citizen_id like '{$farmer_name}') and ";
    }

    if($coop_id!="" && $coop_id!=null){
            $search_query .= "  f.id in (select id from {$dbprefix}ed_farm f where f.coop_id like '{$coop_id}' ) and ";
    }

    if($start_date!="" && $end_date!=""){
        $search_query .= "  create_date BETWEEN '{$start_date}' AND '{$end_date}' and ";
    }

    $query = "	SELECT ism.*, f.name as farm_name,fm.name,fm.surname
                FROM {$dbprefix}ed_cow_insemination ism
                LEFT JOIN {$dbprefix}ed_farm f ON f.id = ism.farm_id
                LEFT JOIN {$dbprefix}ed_farmer fm ON fm.id = farmer_id
                            WHERE ism.farm_id='{$row["farm_id"]}' and {$search_query}  ism.state=1";
    // echo $query;
    $result = $db->query($query);

    if($result){
         // Cycle through results
        while ($row = $result->fetch_object()){
            $row->create_date = formatDate($row->create_date);
            $rows[] = $row;
        }
        // Free result set
        $result->close();
        $db->next_result();
    }
    //total
    $queryTt = "	SELECT count(*) as total
                FROM {$dbprefix}ed_cow_insemination ism
                LEFT JOIN {$dbprefix}ed_farm f ON f.id = ism.farm_id
                LEFT JOIN {$dbprefix}ed_farmer fm ON fm.id = farmer_id
                            WHERE ism.farm_id='{$row["farm_id"]}' and {$search_query}  ism.state=1";
    $resultTt = $db->query($queryTt);
    $rowTT = $resultTt->fetch_object();
    $rows[]["total"] = $rowTT->total;
    HttpResponse::responseSuccessJson($rows);
    
    $db->close();
}
?>
