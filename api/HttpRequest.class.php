<?php

/**
 * 
 * HttpRequest class
 * 
 * @author Suthin Thamsuwan <suthin@unbugsolution.com>
 * @link http://www.unbugsolution.com/
 * @copyright 2013 UNBUG Solution
 * @since 1.0
 * 
 */
class HttpRequest{
    
    // all methods
    public static $HTTP_METHOD_UNKNOWN  = 0;
    public static $HTTP_METHOD_GET      = 1;
    public static $HTTP_METHOD_POST     = 2;
    public static $HTTP_METHOD_PUT      = 3;
    public static $HTTP_METHOD_DELETE   = 4;
    public static $HTTP_METHOD_OPTIONS  = 5;
    public static $HTTP_METHOD_HEAD     = 6;
    
    //protected $http_request_method;
    //protected $params = NULL;

    public function __construct() {
    //    $this->params = new Registry();
    //    $this->ParseParams();
    }
    
    private static function getRequestMethod(){
        $method = strtolower($_SERVER['REQUEST_METHOD']);
        switch($method){
            case 'get':
                return self::$HTTP_METHOD_GET;
            case 'post':
                return self::$HTTP_METHOD_POST;
            case 'put':
                return self::$HTTP_METHOD_PUT;
            case 'head':
                return self::$HTTP_METHOD_HEAD;
            case 'delete':
                return self::$HTTP_METHOD_DELETE;
            case 'options':
                return self::$HTTP_METHOD_OPTIONS;
        }
        return self::$HTTP_METHOD_UNKNOWN;
    }
    
    /**
     * Get the HTTP parameter from GET
     * @param string $param parameter name
     * @return string the value of $param ,or null
     */
    public static function getParamGET($param){
        if(!empty($param)){
            return filter_input(INPUT_GET, $param);
        }
        return null;
    }
    
    /**
     * Get the HTTP parameter from POST
     * @param string $param parameter name
     * @return string the value of $param ,or null
     */
    public static function getParamPOST($param){
        if(!empty($param)){
            return $_POST[$param];
        }
        return null;
    }
    
    public static function getParam($param){
        $param_val = filter_input(INPUT_POST, $param);
        if($param_val == null){
            $param_val = filter_input(INPUT_GET, $param);
        }
        return $param_val;
    }
    
    
    // parse all input HTTP
    public static function getRequestParams($key,$filter = ""){
        //$http_request_method = self::GetRequestMethod();
        if($filter == ""){
            $filter = FILTER_SANITIZE_SPECIAL_CHARS;
        }        
        return filter_input(INPUT_GET | INPUT_POST,$key,$filter);        
    }
  
    public static function getContentType(){
        //
        // To use the following method, we need to have the following line in the .htaccess file
        //
        // RewriteRule .* - [E=HTTP_CONTENT_TYPE:%{HTTP:Content-Type},L]
        //
        //return isset($_SERVER['HTTP_CONTENT_TYPE'])?$_SERVER['HTTP_CONTENT_TYPE']:"";
        
        $headers = getallheaders();
        return isset($headers['Content-Type'])?$headers['Content-Type']:"";
    }
    
    public static function isRawDataInput(){
        $content_type = self::getContentType();
        if(strpos($content_type, 'json') !== false){
            return true;
            
        }else if(strpos($content_type, 'xml') !== false){
            return true;
        }
        
        return false;
    }

    public static function getJsonData(){
        $data = file_get_contents('php://input');
        $json = json_decode($data,true);
        
        return $json;
    }
    
    public static function dumpAllParams(){
        //$get_data = isset($_GET)?$_GET:$_POST;
        $get_params = print_r($_GET,true);
        LogFile::writeDebugLog("HTTP GET Params: ".$get_params);
        
        $post_params = print_r($_POST,true);
        LogFile::writeDebugLog("HTTP POST Params: ".$post_params);
        
        $cookie = print_r($_COOKIE,true);
        LogFile::writeDebugLog("HTTP Cookie: ".$cookie);        
    }
    
    // get all parameters
    public static function getAllRequestParams($filter_password = true){
        $get = $_GET;
        $post = $_POST;
        
        if($filter_password == true){
            $find = array('password','pwd','pwd1','pwd2');
            // GET
            foreach($find as $i => $key){
                if(array_key_exists($key, $get)){
                    $get[$key] = "***";    // ommit the password value
                }
            }
            
            // POST
            foreach($find as $i => $key){
                if(array_key_exists($key, $post)){
                    $post[$key] = "***";    // ommit the password value
                }
            }
        }
        
        if(!empty($get)){
            $extra = Util::serializeArrayKeysValues($get, "&");
        }else{
            $extra = "";
        }
        
        if(!empty($post)){
            if($extra != ""){
                $extra .= "&";
            }
            $extra .= json_encode($post);
            //$extra .= Util::serializeArrayKeysValues($get, "&");
        }
        
        return $extra;
    }

    // get all post chunk data
    public static function getRawPostDataChunk(){
        $content_pos = strpos($GLOBALS['HTTP_RAW_POST_DATA'],"\r\n\r\n");
        $http_content = substr($GLOBALS['HTTP_RAW_POST_DATA'], $content_pos);
        return $http_content;
    }
    
    public static function getRawInputData(){
        return file_get_contents('php://input');
    }
    
    /**
     * get HTTP header value
     * 
     * @param string $header a HTTP header to get its value
     * @return string a hader value or empty string if not found
     */
    public static function getHttpHeaderValue($header){
        /*$headers = getallheaders();
        if(isset($headers[$header])){
            return $headers[$header];
        }
        return "";
         * 
         */
        if(!empty($_SERVER[$header])){
            return $_SERVER[$header];
        }
        return "";
    }    
    
    /**
     * check whether the request is made by Ajax, jQuery
     * 
     * @return boolean true if the request is made by Ajax, jQuery
     */
    public static function isAjaxRequest(){
        $header_value = self::getHttpHeaderValue('HTTP_X_REQUESTED_WITH');
        if($header_value == ""){
            return false;
        }
        return (strtolower($header_value) == "xmlhttprequest");
    }
    
    /**
     * check whether the client expected to get XML response
     */
    public static function isXmlRequest(){
        $header_value = self::getHttpHeaderValue('HTTP_CUSTOM_RETURN');
        if($header_value == ""){
            return false;
        }
        return (strtolower($header_value) == "xml");
    }
 
    public static function logAllHttpHeaders(){
        foreach($_SERVER as $key => $value){
            LogFile::writeDebugLog($key." ".$value);
        }
    }
    
    public static function dumpHeaders(){
        $headers = getallheaders();
        foreach($headers as $k => $v){
            LogFile::writeDebugLog($k."=".$v);
        }
    }
    
}
?>
