<?php

/**
 * Class to Response HTTP data
 * 
 * @author Suthin Thamsuwan <suthin@unbugsolution.com>
 * @link http://www.unbugsolution.com/
 * @copyright 2013 UNBUG Solution
 * @since 1.0
 * 
 */

class HttpResponse{

    public static $STATUS_OK            = "ok";
    public static $STATUS_ERROR         = "error";
    
    // define all fixed values
    private static $TAG_STATUS          = "status";
    private static $TAG_ERROR_DESC      = "error_desc";
    private static $TAG_ERROR_CODE      = "error_code";
    private static $TAG_DATA            = "data";
    private static $TAG_RESPONSE        = "response";
    
    // xml tags
    private static $TAG_XML_DEF         = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
    
    /**
     * response back a JSON format with success status
     * 
     * @param array $data an array containing JSON data to be returned
     */
    public static function responseSuccessJson($data){
        header('Content-type: application/json');
        
        // replace null value with the empty string
        array_walk_recursive($data, function (&$item, $key) {
            $item = null === $item ? '' : $item;
        });
        
        $response_array = array(self::$TAG_STATUS => self::$STATUS_OK, 
                                self::$TAG_ERROR_DESC => "", 
                                self::$TAG_DATA => $data);
        echo json_encode($response_array);
    }
    
    /**
     * response back a JSON format with error status
     * 
     * @param int $error_id an error code
     * @param string $error_desc an error description
     */
    public static function responseErrorJson($error_id, $error_desc = null){
        //header('Content-type: application/json');
        
        // set an error description if no input error description
        if($error_desc == null){
            $error_desc = ErrorInfo::getError($error_id);
        }
        
        $response_array = array(self::$TAG_STATUS => self::$STATUS_ERROR, 
                                self::$TAG_ERROR_CODE => $error_id,
                                self::$TAG_ERROR_DESC => $error_desc, 
                                self::$TAG_DATA => "");
        
        
        echo json_encode($response_array);
    }
    
    /**
     * response the specific JSON data
     * @param array $json a JSON array data
     */
    public static function responseSpecificJson($json){
        header('Content-type: application/json');
        
        echo json_encode($json);
    }
    
    /**
     * response bacn a XML data with success status
     * 
     * @param string $xml_data an xml data to be returned, or null if there is nothing to return
     *      Example:    $xml_data = "<profile><fname>Suthin</fname><lname>Thamsuwan</lname></profile>";
     *              
     */
    public static function responseSuccessXml($xml_data = null){
        //header("Content-type: text/xml");
        
        $xml = self::$TAG_XML_DEF.
            "<".self::$TAG_RESPONSE.">".
                "<".self::$TAG_STATUS.">".self::$STATUS_OK."</".self::$TAG_STATUS.">".
                "<".self::$TAG_DATA.">".$xml_data."</".self::$TAG_DATA.">".
            "</".self::$TAG_RESPONSE.">";
        echo $xml;
    }
    
    /**
     * response bacn a XML data with error status
     * 
     * @param int $error_id an error id
     * @param string $error_desc an error description
     */    
    public static function responseErrorXml($error_id, $error_desc = null){
        //header("Content-type: text/xml");
        
        if($error_desc == null){
            $error_desc = ErrorInfo::getError($error_id);
        }
        
        $xml = self::$TAG_XML_DEF.
            "<".self::$TAG_RESPONSE.">".
                "<".self::$TAG_STATUS.">".self::$STATUS_ERROR."</".self::$TAG_STATUS.">".
                "<".self::$TAG_ERROR_CODE.">".$error_id."</".self::$TAG_ERROR_CODE.">".
                "<".self::$TAG_ERROR_DESC.">".$error_id."</".self::$TAG_ERROR_DESC.">".
            "</".self::$TAG_RESPONSE.">";
        echo $xml;
    }
    
    /**
     * response back the error based on the HTTP requested type
     * 
     * @param type $error_id
     * @param type $error_desc
     */
    public static function responseError($error_id, $error_desc = null){
        if(HttpRequest::isAjaxRequest()){
            self::responseErrorJson($error_id, $error_desc);
        }else{
            self::responseErrorXml($error_id, $error_desc);
        }
    }
    
    
    /**
     * response back the list command
     * 
     * @param array $data a 2-dimension array containing the data to response
     * @param array $map_field an array containing the fields of the data to be map
    */
    public static function responseData($data,$map_field = null){
        if($data != null){
            if($map_field != null){
                // combine the data
                $conv_data = Util::combineAndConvertData($data, $map_field);
                
                //$log_data = print_r($conv_data,true);
                //LogFile::writeDebugLog($log_data);
                
                if(HttpRequest::isAjaxRequest()){
                    self::responseSuccessJson($conv_data);
                }else{
                    $xml = Util::getXmlData($conv_data);
                    
                    //LogFile::writeDebugLog($xml);
                    
                    self::responseSuccessXml($xml);
                }
            }else{
                // response the same fields as in the input data
                if(HttpRequest::isAjaxRequest()){
                    self::responseSuccessJson($data);
                }else{
                    $xml = Util::getXmlData($data);
                    
                    //LogFile::writeDebugLog($xml);
                    
                    self::responseSuccessXml($xml);
                }
            }
        }else{
            self::responseError(ErrorInfo::$ERR_APP_DATA_NOT_FOUND);
        }
    }
    
}

?>