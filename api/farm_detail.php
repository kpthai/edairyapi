<?php
require_once "init.php";

$query = "SELECT * FROM {$jConfig->dbprefix}users WHERE access_token='{$_GET[access_token]}'";

$result = $db->query($query);

if ($result) {

    $row = $result->fetch_object();
}

$search_query = "";

if($_GET["id"]!=""){
	$search_query .= " f.id='{$_GET[id]}' ";
}

$query = "  SELECT f.name AS farmName,
                   c.name AS coopName,
                   f.status AS farmStatus,
                   f.farm_project_id AS farmProjectID,
                   fm.citizen_id AS citizenID,
                   fm.title_id AS titleID,
                   fm.name AS farmerName,
                   fm.surname AS farmerSurname,
                   fm.address AS address,
                   fm.region_id AS regionID,
                   fm.province_id AS provinceID,
                   fm.district_id AS districtID,
                   fm.sub_district_id AS subDistrictID,
                   fm.post_code AS zipcode,
                   fm.latitude AS latitude,
                   fm.longitude AS longitude,
                   fm.id AS farmerID,
                   f.id AS farmID
            FROM {$jConfig->dbprefix}ed_farm AS f
            LEFT JOIN {$jConfig->dbprefix}ed_coop AS c ON c.id = f.coop_id
            LEFT JOIN {$jConfig->dbprefix}ed_farmer AS fm ON fm.farm_id = f.id
            WHERE {$search_query}
         ";
// echo $query;
$result = $db->query($query);

if($result){
    // Cycle through results
    while ($row = $result->fetch_object()){
        // $row->from_date = formatDate($row->from_date);
        // $row->to_date = formatDate($row->to_date);
        $rows[] = $row;
    }
    // Free result set
    $result->close();
    $db->next_result();
}
echo json_encode($rows);
$db->close();
?>
