<?php
require_once "init.php";

$access_token = HttpRequest::getParam("access_token");
$cmd = HttpRequest::getParam("cmd");

$user_login_id = 0;

$query = "SELECT * FROM {$jConfig->dbprefix}users WHERE access_token='{$access_token}'";

$result = $db->query($query);

if ($result) {

    $row = $result->fetch_assoc();
    if($cmd == "get_population_survey_detail"){
        getCowStaticDetail($db,$jConfig->dbprefix,$row);
    }else if($cmd == "add_population_survey"){
        addCowStatic($db,$jConfig->dbprefix,$row);
    }else if($cmd == "update_population_survey"){
        updateCowStatic($db,$jConfig->dbprefix,$row);
    }else{
        ListCowStatic($db,$jConfig->dbprefix,$row);
    }
}

function ListCowStatic($db,$dbprefix,$row){
    $farmer_name = HttpRequest::getParam("farmer_name");
    $coop_id = HttpRequest::getParam("coop_id");
    $start_date = HttpRequest::getParam("start_date");
    $end_date = HttpRequest::getParam("end_date");
    
    $current_page = HttpRequest::getParam("current_page");
    $cur_page = $current_page * 20;
    
    $search_query = "";
    
    //print_r($row["role"]);
    if($row["role"] == 7 || $row["role"] == 5){ //admin และ ผู้บริหาร
        $search_query.= " ";
    }else if($row["role"] == 6 || $row["role"] == 2){ //ส่วนภูมิภาค และ นักส่งเสริม
        $search_query.= " fm.region_id = ".$row["region_id"]." and ";
    }else if($row["role"] == 3 || $row["role"] == 4){ //สหรณ์ ศุนย์รับน้ำนมดิบ
        $search_query.= " f.coop_id = ".$row["coop_id"]." and ";
    }else{
        $search_query.= " s.farm_id='".$row["farm_id"]."' and ";
    }

    if($farmer_name!="" && $farmer_name!=null){
            $search_query .= "  (fm.name like '%{$farmer_name}%' or fm.surname like '%{$farmer_name}%' or f.member_code like '{$farmer_name}') and ";
    }

    if($coop_id!="" && $coop_id!=null){
            $search_query .= "  (s.farm_id in (f.coop_id like '{$coop_id}' )) and ";
    }

    if($start_date!="" && $end_date!=""){
        $search_query .= "  s.create_date BETWEEN '{$start_date}' AND '{$end_date}' and ";
    }

    $query = "	SELECT s.*,fm.name,fm.surname FROM {$dbprefix}ed_cow_statistic s
                LEFT JOIN {$dbprefix}ed_farmer fm ON fm.id = farmer_id
                LEFT join {$dbprefix}ed_farm f on f.id = fm.farm_id
                WHERE  {$search_query} s.state = 1 order by id limit $cur_page,20";
     //echo $query;
    //print_r($query);
    $result = $db->query($query);

    if($result){
         // Cycle through results
        while ($row = $result->fetch_object()){
            $row->create_date = formatDate($row->create_date);
            $rows[] = $row;
        }
        // Free result set
        $result->close();
        $db->next_result();
        
    }else{
         HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
    }
    //total
    $queryTt = "	SELECT count(*) as total FROM {$dbprefix}ed_cow_statistic s
                LEFT JOIN {$dbprefix}ed_farmer fm ON fm.id = farmer_id
                LEFT join {$dbprefix}ed_farm f on f.id = fm.farm_id
                WHERE  {$search_query} s.state = 1 ";
                //echo $queryTt;
    $resultTt = $db->query($queryTt);
    $rowTT = $resultTt->fetch_object();
    $rows[]["total"] = $rowTT->total;
    HttpResponse::responseSuccessJson($rows);
    $db->close();
}

function addCowStatic($db,$dbprefix,$row){
    
    $survey_date = HttpRequest::getParam("survey_date");
    $farmer_name = HttpRequest::getParam("farmer_name");
    $total1 = HttpRequest::getParam("total1");
    $total2 = HttpRequest::getParam("total2");
    $total3 = HttpRequest::getParam("total3");
    $total4 = HttpRequest::getParam("total4");
    $total5 = HttpRequest::getParam("total5");
    $total6 = HttpRequest::getParam("total6");
    $total7 = HttpRequest::getParam("total7");
    $total8 = HttpRequest::getParam("total8");
    $total9 = HttpRequest::getParam("total9");
    $total10 = HttpRequest::getParam("total10");
    $total_female = HttpRequest::getParam("total_female");
    $total_male = HttpRequest::getParam("total_male");
    $total_cow = HttpRequest::getParam("total_cow");
    $remark = HttpRequest::getParam("remark");
    
    //get lastId
    $qrLastId = "select id from {$dbprefix}ed_cow_statistic ORDER BY id DESC Limit 1 ";
    $restLastId = $db->query($qrLastId);
    $rowLastId = $restLastId->fetch_assoc();
    $id = $rowLastId["id"]+1;
    
    $query = "  INSERT INTO {$dbprefix}ed_cow_statistic (id,farmer_id,farm_id,create_date,total1,total2,total3,total4,total5,total6,total7,total8,total9,total10,total11,total12,total13,total14,total15,total_cow,remark,total_female,total_male,create_by,ordering,state,checked_out,checked_out_time,created_by) VALUES 
                ('{$id}','{$farmer_name}','{$row["farm_id"]}','{$survey_date}','{$total1}','{$total2}','{$total3}','{$total4}','{$total5}','{$total6}','{$total7}','{$total8}','{$total9}','{$total10}','0','0','0','0','0','{$total_cow}','{$remark}','{$total_female}','{$total_male}','{$row["id"]}','0','1','0',now(),'{$row["id"]}')";
   
    //echo $query;
    $result = $db->query($query);

    if ($result) {
        HttpResponse::responseSuccessJson($result);
    } else {
        HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
    }
    $db->close();
    
}

function updateCowStatic($db,$dbprefix,$row){
    $id = HttpRequest::getParam("id");
    $survey_date = HttpRequest::getParam("survey_date");
    $farmer_name = HttpRequest::getParam("farmer_name");
    $total1 = HttpRequest::getParam("total1");
    $total2 = HttpRequest::getParam("total2");
    $total3 = HttpRequest::getParam("total3");
    $total4 = HttpRequest::getParam("total4");
    $total5 = HttpRequest::getParam("total5");
    $total6 = HttpRequest::getParam("total6");
    $total7 = HttpRequest::getParam("total7");
    $total8 = HttpRequest::getParam("total8");
    $total9 = HttpRequest::getParam("total9");
    $total10 = HttpRequest::getParam("total10");
    $total_female = HttpRequest::getParam("total_female");
    $total_male = HttpRequest::getParam("total_male");
    $total_cow = HttpRequest::getParam("total_cow");
    $remark = HttpRequest::getParam("remark");
    
    
    $sql = "update {$dbprefix}ed_cow_statistic set "
            . "create_date = '$survey_date',"
            . "farmer_id = '$farmer_name',"
            . "total1 = '$total1',"
            . "total2 = '$total2',"
            . "total3 = '$total3',"
            . "total4 = '$total4',"
            . "total5 = '$total5',"
            . "total6 = '$total6',"
            . "total7 = '$total7',"
            . "total8 = '$total8',"
            . "total9 = '$total9',"
            . "total10 = '$total10',"
            . "total_female = '$total_female',"
            . "total_male = '$total_male',"
            . "total_cow = '$total_cow',"
            . "remark = '$remark'"
            . " where id = '$id'";
    
    $result = $db->query($sql);
    if($result){
        HttpResponse::responseSuccessJson($result);
    }else{
        HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
    }
    
    $db->close();
}

function  getCowStaticDetail($db,$dbprefix,$row){
    $id = HttpRequest::getParam("id");
    
    $sql = "select a.*,b.coop_id from {$dbprefix}ed_cow_statistic a "
        . " left join {$dbprefix}ed_farm b on b.id = a.farm_id "
        . " where a.id = {$id} ";
    //print_r($query);
    $result = $db->query($sql);

    if($result){
         // Cycle through results
        $rows = $result->fetch_assoc();
        HttpResponse::responseSuccessJson($rows);
    }else{
        HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
    }
    
    $db->close();
}
?>
