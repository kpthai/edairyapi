<?php
require_once "init.php";

$query = "SELECT * FROM {$jConfig->dbprefix}users WHERE access_token='{$_GET[access_token]}'";

$result = $db->query($query);

if ($result) {

    $row = $result->fetch_object();
}


$search_query = "";

if($_GET["farmer_name"]!=""){
	$search_query .= " AND (fm.name like '%{$_GET[farmer_name]}%' or fm.surname like '%{$_GET[farmer_name]}%' or fm.citizen_id like '{$_GET[farmer_name]}')";
}

if($_GET["coop_id"]!=""){
	$search_query .= " AND f.id in (select id from {$jConfig->dbprefix}ed_farm f where f.coop_id like '{$_GET[coop_id]}' )";
}

$query = "	SELECT *, cw.name as cowName, chs.name as chsName, chss.name as chssName, u.name as uName

			FROM {$jConfig->dbprefix}ed_cow_heal_result as chr
            LEFT JOIN {$jConfig->dbprefix}ed_cow cw ON cw.id = chr.cow_id
            LEFT JOIN {$jConfig->dbprefix}ed_farm f ON f.id = cw.farm_id
            LEFT JOIN {$jConfig->dbprefix}ed_farmer fm ON fm.farm_id = f.id
            LEFT JOIN {$jConfig->dbprefix}ed_coop cp ON cp.id = f.coop_id
            LEFT JOIN {$jConfig->dbprefix}ed_cow_health_service chs ON chs.created_by = chr.created_by
            LEFT JOIN {$jConfig->dbprefix}ed_cow_health_sub_service chss ON chss.created_by = chr.created_by
            LEFT JOIN {$jConfig->dbprefix}users u ON u.id = chr.created_by
		 	WHERE f.id='{$row->farm_id}' {$search_query}"; /*{$row->farm_id}*/
// echo $query;
$result = $db->query($query);

if($result){
     // Cycle through results
    while ($row = $result->fetch_object()){
        $row->create_date = formatDate($row->create_date);
        $rows[] = $row;
    }
    // Free result set
    $result->close();
    $db->next_result();
}
echo json_encode($rows);
$db->close();
?>
