<?php
require_once "init.php";

$access_token = HttpRequest::getParam("access_token");
$cmd = HttpRequest::getParam("cmd");

$user_login_id = 0;

$query = "SELECT * FROM {$jConfig->dbprefix}users WHERE access_token='{$access_token}'";

$result = $db->query($query);

if ($result) {

    $row = $result->fetch_assoc();
    if($cmd == "get_dairy_detail"){
        getDairyInfo($db,$jConfig->dbprefix,$row);
    }else if($cmd == "edit_dairy"){
        updateDairy($db,$jConfig->dbprefix,$row);
    }else{
        getDairyList($db,$jConfig->dbprefix,$row);
    }
}

function getDairyList($db,$dbprefix,$row){
    $farmer_name = HttpRequest::getParam("farmer_name");
    $coop_id = HttpRequest::getParam("coop_id");
    
    $search_query = "";

    if($farmer_name!="" && $farmer_name!=null){
            $search_query .= " AND (fm.name like '%{$farmer_name}%' or fm.surname like '%{$farmer_name}%' or fm.citizen_id like '{$farmer_name}')";
    }

    if($coop_id!="" && $coop_id!=null){
            $search_query .= " AND f.id in (select id from {$dbprefix}ed_farm f where f.coop_id like '{$coop_id}' )";
    }

    $query = "	SELECT *, cw.name as cowName,csm.id as csmId

                            FROM {$dbprefix}ed_cow_stop_milking as csm
                LEFT JOIN {$dbprefix}ed_cow cw ON cw.id = csm.cow_id
                LEFT JOIN {$dbprefix}ed_farm f ON f.id = cw.farm_id
                LEFT JOIN {$dbprefix}ed_farmer fm ON fm.farm_id = f.id
                LEFT JOIN {$dbprefix}ed_coop cp ON cp.id = f.coop_id
                            WHERE f.id='{$row["farm_id"]}' {$search_query} order by csm.id DESC"; /*{$row->farm_id}*/
     //echo $query;
    $result = $db->query($query);

    if($result){
         // Cycle through results
        while ($row = $result->fetch_object()){
            $row->create_date = formatDate($row->create_date);
            $row->birth_date = formatDate($row->birth_date);
            $rows[] = $row;
        }
        // Free result set
        $result->close();
        $db->next_result();
    }
    //total
    $queryTt = "	SELECT count(*) as total FROM {$dbprefix}ed_cow_stop_milking as csm
                LEFT JOIN {$dbprefix}ed_cow cw ON cw.id = csm.cow_id
                LEFT JOIN {$dbprefix}ed_farm f ON f.id = cw.farm_id
                LEFT JOIN {$dbprefix}ed_farmer fm ON fm.farm_id = f.id
                LEFT JOIN {$dbprefix}ed_coop cp ON cp.id = f.coop_id
                            WHERE f.id='{$row["farm_id"]}' {$search_query} order by csm.id DESC";
    $resultTt = $db->query($queryTt);
    $rowTT = $resultTt->fetch_object();
    $rows[]["total"] = $rowTT->total;
    HttpResponse::responseSuccessJson($rows);
    $db->close();
}

function getDairyInfo($db,$dbprefix,$row){
    
    $id = HttpRequest::getParam("id");
    
    if($id!="" && $id!=null){
        $query = "	SELECT *, cw.name as cowName, csm.id as csmID

                                FROM {$dbprefix}ed_cow_stop_milking csm
                    LEFT JOIN {$dbprefix}ed_cow cw ON cw.id = csm.cow_id
                    LEFT JOIN {$dbprefix}ed_farm f ON f.id = cw.farm_id
                    LEFT JOIN {$dbprefix}ed_farmer fm ON fm.farm_id = f.id
                    LEFT JOIN {$dbprefix}ed_coop cp ON cp.id = f.coop_id
                                WHERE csm.id = $id"; /*{$row->farm_id}*/
         //echo $query;
        $result = $db->query($query);
        if ($result) {
            $row = $result->fetch_assoc();
            HttpResponse::responseSuccessJson($row);
        } else {
            HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
        }
    }else{
         HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
    }
    
}

function updateDairy($db,$dbprefix,$row){
    $id = HttpRequest::getParam("id");
    $check_date = HttpRequest::getParam("check_date");
    
    $sql = "update {$dbprefix}ed_cow_stop_milking set "
            . "actual_date = '$check_date'"
            . " where id = '$id'";
    
    // echo $query;
    $result = $db->query($sql);
    
    //get pn id
    $querypn = "SELECT * FROM {$dbprefix}ed_cow_stop_milking WHERE id = $id"; /*{$row->farm_id}*/
         //echo $query;
    $resultpn = $db->query($querypn);
    $pnData = $resultpn->fetch_assoc();
    $pn_id = $pnData["cow_pregnant_inspection_id"];
    
    //check วามีข้อมูลอยู่หรือไม่
    $querybt = "SELECT * FROM {$dbprefix}ed_cow_birth_inspection WHERE cow_pregnant_inspection_id = $pn_id"; /*{$row->farm_id}*/
    //echo $query;
    $resultbt = $db->query($querybt);
    if($resultbt->num_rows == 0){//ยังไม่มีข้อมูลให้ add เพิ่ม
        $querycpn = "	SELECT * FROM {$dbprefix}ed_cow_pregnant_inspection  WHERE id = $pn_id"; /*{$row->farm_id}*/
        //echo $query;
        $resultcpn = $db->query($querycpn);
        $rowcpn = $resultcpn->fetch_assoc();
        $cow_id = $rowcpn["cow_id"];
        $insemination_date = $rowcpn["insemination_date"];

       //get lastId bt
        $qrLastIdbt = "select id from {$dbprefix}ed_cow_birth_inspection ORDER BY id DESC Limit 1 ";
        $restLastIdbt = $db->query($qrLastIdbt);
        $rowLastIdbt = $restLastIdbt->fetch_assoc();
        $btid = $rowLastIdbt["id"]+1;

        $create_date = date("Y-m-d", strtotime($insemination_date."+270 days")); //วันคลอด

        $querybti = "  INSERT INTO {$dbprefix}ed_cow_birth_inspection (id,cow_id,cow_pregnant_inspection_id,create_date,birth_date,birth_method,is_child_alive,
                        child_gender,child_weight,child_cow_id,create_by,
                    ordering,state,checked_out,checked_out_time,created_by) VALUES 
                    ('{$btid}','{$cow_id}','{$pn_id}','{$create_date}','0000-00-00','0','0','0','0','0','{$row["id"]}','0','1','0',now(),'{$row["id"]}') ";

        // echo $query;
        $resultbti = $db->query($querybti);
    }

    if ($result) {
        HttpResponse::responseSuccessJson($id);
    } else {
        HttpResponse::responseErrorJson(500,"พบข้อผิดพลาดในการทำงาน");
    }
    $db->close();
}
?>
