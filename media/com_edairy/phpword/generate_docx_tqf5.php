<?php
require_once 'PHPWord.php';

require_once '../../../configuration.php';

function find_key($key_data, $find){
	foreach($key_data->elements as $key=>$val){
		if($val->name==$find){
			return $key;
		}
	}
}

function formatThaiDate($date){
	return $date;
}
//Connect mySQL

$config = new JConfig();



//$conn = mysql_connect($JConfig->host, $JConfig->user, $JConfig->password);
$mysqli = new mysqli($config->host, $config->user, $config->password, $config->db);
//mysql_select_db($JConfig->db);

$mysqli->query("SET NAMES UTF8");


//Retrieve General info Data
$result = $mysqli->query("	select *, (select name from tqf_tqf_curriculum where id=curriculum_id) as curriculum
	from tqf5_general_info info
	left join tqf_tqf_course c on c.id = info.course_id
	where submission_id='{$_REQUEST[id]}'");
$row_info = $result->fetch_array();

//Retrieve Teacher info Data

$teacher_result = $mysqli->query("select * from tqf_tqf_course_has_teacher where course_id='{$row_info[course_id]}' and state=1");
$teachers = array();
while($teacher_row = $teacher_result->fetch_array()){
	$teacher_sub_row_result = $mysqli->query("select name from tqf_tqf_teacher where id='{$teacher_row[teacher_id]}' and state=1");
	$teacher_sub_row = $teacher_sub_row_result->fetch_array();
	$teacher_data = $teacher_sub_row["name"] . "  " . $teacher_row["position"];
	array_push($teachers, $teacher_data);
}

//Retrieve Pre-Requisite Data
$pre_result = $mysqli->query("select * from tqf_tqf_course_requisite where course_id='{$row_info[course_id]}' and requisite_type='Pre' and state=1");
$pre_requisites = array();
while($pre_row = $pre_result->fetch_array()){
	$pre_sub_row_result = $mysqli->query("select * from tqf_tqf_course where id='{$pre_row[course_id2]}'");
	$pre_sub_row = $pre_sub_row_result->fetch_array();
	$pre_data = $pre_sub_row["course_number"] . " " . $pre_sub_row["name"];
	array_push($pre_requisites, $pre_data);
}


//Retrieve Co-Requisite Data
$co_result = $mysqli->query("select * from tqf_tqf_course_requisite where course_id='{$row_info[course_id]}' and requisite_type='Co' and state=1");
$co_requisites = array();
while($co_row = $co_result->fetch_array()){
	$co_sub_row_result = $mysqli->query("select * from tqf_tqf_course where id='{$co_row[course_id2]}'");
	$co_sub_row = $co_sub_row_result->fetch_array();
	$co_data = $co_sub_row["course_number"] . " " . $co_sub_row["name"];
	array_push($co_requisites, $co_data);
}


//Retrieve Data
$result = $mysqli->query("select * from tqf_zoo_item where id='{$_REQUEST[id]}'");
$row = $result->fetch_array();



$key_data = file_get_contents("../../../media/zoo/applications/business/types/tqf5.config");
$key_data = json_decode($key_data);

//print_r($key_data);


$elements = json_decode($row["elements"]);




//print_r($JConfig);

// New Word Document
$PHPWord = new PHPWord();


// Styling by AFRONUTT!!
// Define font style for first row
$fontStyle = array('align'=>'center', 'size'=>16, 'name'=>"Browallia New");
$fontStyleBold = array('bold'=>true, 'size'=>16, 'name'=>"Browallia New");

$PHPWord->addParagraphStyle('pStyle', array('align'=>'center'));
$PHPWord->addParagraphStyle('pStyleAlignLeft', array('align'=>'left'));
$PHPWord->addParagraphStyle('pStyleAlignRight', array('align'=>'right'));


// New portrait section
$section = $PHPWord->createSection();


// Header
$header = $section->createHeader();
$table = $header->addTable();
$table->addRow();
$table->addCell(10000)->addText('มคอ.5', $fontStyle, 'pStyleAlignRight');


// Footer

// Add footer
$footer = $section->createFooter();
$footer->addPreserveText('สำนักงานคณะกรรมการอุดมศึกษา                                                                                   {PAGE}', $fontStyle, 'pStyleAlignLeft');


/*
$section->addText('I am inline styled.', array('name'=>'Verdana', 'color'=>'006699'));


$PHPWord->addFontStyle('rStyle', array('bold'=>true, 'italic'=>true, 'size'=>16));
$PHPWord->addParagraphStyle('pStyle', array('align'=>'center', 'spaceAfter'=>100));
$section->addText('I am styled by two style definitions.', 'rStyle', 'pStyle');
$section->addText('I have only a paragraph style definition.', null, 'pStyle');
*/

// Define table style arrays
//'borderSize'=>1, 'borderColor'=>'000000', 
$styleTable = array('cellMargin'=>0);

$styleTableBordered = array('borderSize'=>1, 'borderColor'=>'000000', 'cellMargin'=>10);
//$styleFirstRow = array('borderBottomSize'=>18, 'borderBottomColor'=>'0000FF', 'bgColor'=>'66BBFF');

// Define cell style arrays
//$styleCell = array('valign'=>'center');
//$styleCellBTLR = array('valign'=>'center', 'textDirection'=>PHPWord_Style_Cell::TEXT_DIR_BTLR);



// Add table style
$PHPWord->addTableStyle('TQFTableStyle', $styleTable, $styleFirstRow);
$PHPWord->addTableStyle('TQFTableStyleBordered', $styleTableBordered, $styleFirstRow);

// Add table
$table = $section->addTable('TQFTableStyle');



/*

PAGE 1

*/

// Add row
$table->addRow();

// Add cells
$table->addCell(3000, $styleCell)->addText('ชื่อสถาบันอุดมศึกษา', $fontStyleBold);
$table->addCell(10000, $styleCell)->addText($row_info['faculty'], $fontStyle);

$table->addRow();

$table->addCell(3000, $styleCell)->addText('วิทยาเขต/คณะ/ภาควิชา', $fontStyleBold);
$table->addCell(10000, $styleCell)->addText($row_info['department'], $fontStyle);

$section->addTextBreak(1);
$section->addText('หมวดที่ 1 ข้อมูลโดยทั่วไป', $fontStyleBold, 'pStyle');



// Add table
$table = $section->addTable('TQFTableStyleBordered');

// Add row
$table->addRow();

// Add cells
$cell = $table->addCell(10000, $styleCell);
$cell->addText('1. รหัสและรายชื่อวิชา', $fontStyleBold);
$cell->addText("         {$row_info[course_number]} {$row_info[name]}
	{$row_info[eng_name]}", $fontStyle);

$credit_info = explode(" ", $row_info["credit"]);
/*
$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('2. จำนวนหน่วยกิต', $fontStyleBold);
$cell->addText("         {$credit_info[0]} หน่วยกิต {$credit_info[1]}", $fontStyle);

*/
$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('2. รายวิชาที่ต้องเรียนมาก่อน (Pre-requisite) (ถ้ามี)', $fontStyleBold);
if(count($pre_requisites)==0)
	$cell->addText("         ไม่มี", $fontStyle);
foreach($pre_requisites as $key=>$data){
	$cell->addText("         {$data}", $fontStyle);
}


$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('3. อาจารย์ผู้รับผิดชอบรายวิชาและอาจารย์ผู้สอน', $fontStyleBold);
foreach($teachers as $key=>$data){
	$cell->addText("         {$data}", $fontStyle);
}

/*
$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('3. หลักสูตรและประเภทของวิชา', $fontStyleBold);
$cell->addText("         {$row_info[curriculum]}", $fontStyle);
*/

$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('4. ภาคการศึกษา/ชั้นปีที่เรียน', $fontStyleBold);
$cell->addText("         ภาคการศึกษาที่ {$row_info[semester]} / ชั้นปีที่ {$row_info[year]}", $fontStyle);

/*

$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('7. รายวิชาที่ต้องเรียนพร้อมกัน (Co-requisite) (ถ้ามี)', $fontStyleBold);
if(count($co_requisites)==0)
	$cell->addText("         ไม่มี", $fontStyle);
foreach($co_requisites as $key=>$data){
	$cell->addText("         {$data}", $fontStyle);
}
*/


$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('5. สถานที่เรียน', $fontStyleBold);
$cell->addText("         {$row_info[study_place]}", $fontStyle);

/*
$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('9. วันที่จัดทำหรือปรับปรุงรายละเอียดของรายวิชาครั้งล่าสุด', $fontStyleBold);
$cell->addText("         " . formatThaiDate($row_info['last_modified_date']), $fontStyle);

*/
$section->addPageBreak();

/*

PAGE 2

*/


$section->addText('หมวดที่ 2 การจัดการเรียนการสอนที่เปรียบเทียบกับแผนการสอน', $fontStyleBold, 'pStyle');


//$section->addTextBreak(1);
$section->addText('1. รายงานชั่วโมงการสอนจริงเทียบกับแผนการสอน', $fontStyleBold, 'pStyleLeft');


// Add table
$table = $section->addTable('TQFTableStyleBordered');
$table->addRow();
$table->addCell(2500, $styleCell)->addText('หัวข้อ', $fontStyleBold, 'pStyle');
$table->addCell(2500, $styleCell)->addText('จำนวน
ชั่วโมงตาม
แผนการสอน', $fontStyleBold, 'pStyle');
$table->addCell(2500, $styleCell)->addText('จำนวน
ชั่วโมงที่ได้
สอนจริง', $fontStyleBold, 'pStyle');
$table->addCell(2500, $styleCell)->addText('ระบุสาเหตุที่การสอนจริงต่างจาก
แผนการสอนหากมีความแตกต่างเกิน 25%', $fontStyleBold, 'pStyle');

$planning_result = $mysqli->query("select * from tqf5_actual_vs_plan where submission_id='{$_REQUEST[id]}' order by sequence asc");
$plannings = array();

while($planning_row = $planning_result->fetch_array()){
$table->addRow();


$table->addCell(2500, $styleCell)->addText($planning_row["topic"], $fontStyle, 'pStyleLeft');

$table->addCell(2500, $styleCell)->addText($planning_row["plan_hour"], $fontStyle, 'pStyle');

$table->addCell(2500, $styleCell)->addText($planning_row["actual_hour"], $fontStyle, 'pStyle');

$table->addCell(2500, $styleCell)->addText($planning_row["remark"], $fontStyle, 'pStyleLeft');
}


$section->addTextBreak(1);
$section->addText('2. หัวข้อที่สอนไม่ครอบคลุมตามแผน', $fontStyleBold, 'pStyleLeft');
// Add table
$table = $section->addTable('TQFTableStyleBordered');


// Add table
$table = $section->addTable('TQFTableStyleBordered');
$table->addRow();
$table->addCell(3333, $styleCell)->addText('หัวข้อที่สอนไม่ครอบคลุมตาม
แผน (ถ้ามี)', $fontStyleBold, 'pStyle');
$table->addCell(3333, $styleCell)->addText('นัยสำคัญของหัวข้อที่สอนไม่
ครอบคลุมตามแผน', $fontStyleBold, 'pStyle');
$table->addCell(3333, $styleCell)->addText('แนวทางชดเชย', $fontStyleBold, 'pStyle');

$planning_result = $mysqli->query("select * from tqf5_not_cover_topic where submission_id='{$_REQUEST[id]}' order by sequence asc");
$plannings = array();

while($planning_row = $planning_result->fetch_array()){
$table->addRow();


$table->addCell(3333, $styleCell)->addText($planning_row["not_cover_topic"], $fontStyle, 'pStyleLeft');

$table->addCell(3333, $styleCell)->addText($planning_row["result"], $fontStyle, 'pStyleLeft');

$table->addCell(3333, $styleCell)->addText($planning_row["compensation"], $fontStyle, 'pStyleLeft');
}


//$section->addTextBreak(1);
$section->addPageBreak();

$section->addText('3. ประสิทธิผลของวิธีสอนที่ทำให้เกิดผลการเรียนรู้ตามที่ระบุในรายละเอียดของรายวิชา', $fontStyleBold, 'pStyleLeft');


// Add table
$table = $section->addTable('TQFTableStyleBordered');
$table->addRow();
$table->addCell(2500, array('vMerge' => 'restart'))->addText('ผลการเรียนรู้', $fontStyleBold, 'pStyle');
$table->addCell(3000, array('vMerge' => 'restart'))->addText('วิธีสอนที่ระบุในรายละเอียด
รายวิชา', $fontStyleBold, 'pStyle');
$table->addCell(1500, array('gridSpan' => 2))->addText('ประสิทธิผล', $fontStyleBold, 'pStyle');
$table->addCell(3000, array('vMerge' => 'restart'))->addText('ปัญหาของการใช้วิธีการสอน(ถ้ามี)
พร้อมข้อเสนอแนะในการแก้ไข', $fontStyleBold, 'pStyle');

$table->addRow();
$table->addCell(2500, array('vMerge' => ''));
$table->addCell(3000, array('vMerge' => ''));
$table->addCell(750, $styleCell)->addText('มี', $fontStyleBold, 'pStyle');
$table->addCell(750, $styleCell)->addText('ไม่มี', $fontStyleBold, 'pStyle');
$table->addCell(3000, array('vMerge' => ''));

$planning_result = $mysqli->query("select * from tqf5_performance where submission_id='{$_REQUEST[id]}' order by sequence asc");
$plannings = array();

while($planning_row = $planning_result->fetch_array()){
$table->addRow();


$table->addCell(2500, $styleCell)->addText($planning_row["criteria"], $fontStyle, 'pStyleLeft');

$table->addCell(3000, $styleCell)->addText($planning_row["teaching_method"], $fontStyle, 'pStyleLeft');

$perform = "";
$not_perform = "";

if($planning_row["is_perform"]=="Y")
	$perform="/";
else
	$not_perform="/";

$table->addCell(750, $styleCell)->addText($perform, $fontStyle, 'pStyle');
$table->addCell(750, $styleCell)->addText($not_perform, $fontStyle, 'pStyle');

$table->addCell(3000, $styleCell)->addText($planning_row["problem"], $fontStyle, 'pStyleLeft');
}



$section->addPageBreak();


$key = find_key($key_data,"Teaching Method Recommendation");
$data = $elements->$key->{'0'}->value;

$table = $section->addTable('TQFTableStyleBordered');
$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('4.  ข้อเสนอการดำเนินการเพื่อปรับปรุงวิธีสอน', $fontStyleBold);
$cell->addText("         {$data}", $fontStyle);



$section->addText('หมวดที่ 3 สรุปผลการจัดการเรียนการสอนของรายวิชา', $fontStyleBold, 'pStyle');
$table = $section->addTable('TQFTableStyleBordered');
$table->addRow();
$cell = $table->addCell(10000, $styleCell);

$key = find_key($key_data,"Total Enrolled Student");
$total_enrolled = $elements->$key->{'0'}->value;

$key = find_key($key_data,"Total Finished Student");
$total_finished = $elements->$key->{'0'}->value;

$key = find_key($key_data,"Total Withdrawed Student");
$total_withdrawed = $elements->$key->{'0'}->value;

$cell->addText("1. จำนวนนักศึกษาที่ลงทะเบียนเรียน				{$total_enrolled}	คน
2. จำนวนนักศึกษาที่คงอยู่เมื่อสิ้นสุดภาคการศึกษา		{$total_finished}	คน
   (จำนวนนักศึกษาที่สอบผ่านในรายวิชานี้)
3. จำนวนนักศึกษาที่ถอน (W)					{$total_withdrawed}	คน", $fontStyleBold);


$section->addText('4. การกระจายของระดับคะแนน (เกรด)', $fontStyleBold, 'pStyleLeft');
$table = $section->addTable('TQFTableStyleBordered');
$table->addRow();
$table->addCell(2500, $styleCell)->addText('ระดับคะแนน', $fontStyleBold, 'pStyle');
$table->addCell(2500, $styleCell)->addText('จำนวน', $fontStyleBold, 'pStyle');
$table->addCell(2500, $styleCell)->addText('คิดเป็นร้อยละ', $fontStyleBold, 'pStyle');


$score_data_result = $mysqli->query("select * from tqf5_score_data where submission_id='{$_REQUEST[id]}'");

$score_data = $score_data_result->fetch_array();

$table->addRow();
$table->addCell(2500, $styleCell)->addText('A', $fontStyleBold, 'pStyle');
$table->addCell(2500, $styleCell)->addText($score_data["grade_a_total"], $fontStyleBold, 'pStyle');
$table->addCell(2500, $styleCell)->addText($score_data["grade_a_percent"], $fontStyleBold, 'pStyle');

$table->addRow();
$table->addCell(2500, $styleCell)->addText('B+', $fontStyleBold, 'pStyle');
$table->addCell(2500, $styleCell)->addText($score_data["grade_b_plus_total"], $fontStyleBold, 'pStyle');
$table->addCell(2500, $styleCell)->addText($score_data["grade_b_plus_percent"], $fontStyleBold, 'pStyle');

$table->addRow();
$table->addCell(2500, $styleCell)->addText('B', $fontStyleBold, 'pStyle');
$table->addCell(2500, $styleCell)->addText($score_data["grade_b_total"], $fontStyleBold, 'pStyle');
$table->addCell(2500, $styleCell)->addText($score_data["grade_b_percent"], $fontStyleBold, 'pStyle');

$table->addRow();
$table->addCell(2500, $styleCell)->addText('C+', $fontStyleBold, 'pStyle');
$table->addCell(2500, $styleCell)->addText($score_data["grade_c_plus_total"], $fontStyleBold, 'pStyle');
$table->addCell(2500, $styleCell)->addText($score_data["grade_c_plus_percent"], $fontStyleBold, 'pStyle');

$table->addRow();
$table->addCell(2500, $styleCell)->addText('C', $fontStyleBold, 'pStyle');
$table->addCell(2500, $styleCell)->addText($score_data["grade_c_total"], $fontStyleBold, 'pStyle');
$table->addCell(2500, $styleCell)->addText($score_data["grade_c_percent"], $fontStyleBold, 'pStyle');

$table->addRow();
$table->addCell(2500, $styleCell)->addText('D+', $fontStyleBold, 'pStyle');
$table->addCell(2500, $styleCell)->addText($score_data["grade_d_plus_total"], $fontStyleBold, 'pStyle');
$table->addCell(2500, $styleCell)->addText($score_data["grade_d_plus_percent"], $fontStyleBold, 'pStyle');

$table->addRow();
$table->addCell(2500, $styleCell)->addText('D', $fontStyleBold, 'pStyle');
$table->addCell(2500, $styleCell)->addText($score_data["grade_d_total"], $fontStyleBold, 'pStyle');
$table->addCell(2500, $styleCell)->addText($score_data["grade_d_percent"], $fontStyleBold, 'pStyle');

$table->addRow();
$table->addCell(2500, $styleCell)->addText('F', $fontStyleBold, 'pStyle');
$table->addCell(2500, $styleCell)->addText($score_data["grade_f_total"], $fontStyleBold, 'pStyle');
$table->addCell(2500, $styleCell)->addText($score_data["grade_f_percent"], $fontStyleBold, 'pStyle');

$table->addRow();
$table->addCell(2500, $styleCell)->addText('ไม่สมบูรณ์ (I)', $fontStyleBold, 'pStyle');
$table->addCell(2500, $styleCell)->addText($score_data["grade_i_total"], $fontStyleBold, 'pStyle');
$table->addCell(2500, $styleCell)->addText($score_data["grade_i_percent"], $fontStyleBold, 'pStyle');

$table->addRow();
$table->addCell(2500, $styleCell)->addText('ผ่าน (P,S)', $fontStyleBold, 'pStyle');
$table->addCell(2500, $styleCell)->addText($score_data["grade_p_total"], $fontStyleBold, 'pStyle');
$table->addCell(2500, $styleCell)->addText($score_data["grade_p_percent"], $fontStyleBold, 'pStyle');

$table->addRow();
$table->addCell(2500, $styleCell)->addText('ตก (U)', $fontStyleBold, 'pStyle');
$table->addCell(2500, $styleCell)->addText($score_data["grade_u_total"], $fontStyleBold, 'pStyle');
$table->addCell(2500, $styleCell)->addText($score_data["grade_u_percent"], $fontStyleBold, 'pStyle');

$table->addRow();
$table->addCell(2500, $styleCell)->addText('ถอน (W)', $fontStyleBold, 'pStyle');
$table->addCell(2500, $styleCell)->addText($score_data["grade_w_total"], $fontStyleBold, 'pStyle');
$table->addCell(2500, $styleCell)->addText($score_data["grade_w_percent"], $fontStyleBold, 'pStyle');




$section->addTextBreak(1);

$key = find_key($key_data,"Abnormal Score Causes");
$data = $elements->$key->{'0'}->value;
$table = $section->addTable('TQFTableStyleBordered');
$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 2));
$cell->addText("5. ปัจจัยที่ทำให้ระดับคะแนนผิดปกติ", $fontStyleBold, 'pStyleLeft');
$cell->addText($data, $fontStyle, 'pStyleLeft');

$table = $section->addTable('TQFTableStyleBordered');
$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 2));
$cell->addText("6. ความคลาดเคลื่อนจากแผนการประเมินที่กำหนดไว้ในรายละเอียดรายวิชา
  6.1 ความคลาดเคลื่อนด้านการกำหนดเวลาการประเมิน", $fontStyleBold, 'pStyleLeft');

$table->addRow();
$cell = $table->addCell(5000, $styleCell);
$cell->addText("ความคลาดเคลื่อน", $fontStyleBold, 'pStyleLeft');

$cell = $table->addCell(5000, $styleCell);
$cell->addText("เหตุผล", $fontStyleBold, 'pStyleLeft');


$key = find_key($key_data,"Time Error");
$data = $elements->$key->{'0'}->value;
$table->addRow();
$cell = $table->addCell(5000, $styleCell);
$cell->addText($data, $fontStyle, 'pStyleLeft');


$key = find_key($key_data,"Time Error Reason");
$data = $elements->$key->{'0'}->value;
$cell = $table->addCell(5000, $styleCell);
$cell->addText($data, $fontStyle, 'pStyleLeft');

$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 2));
$cell->addText("  6.2 ความคลาดเคลื่อนด้านวิธีการประเมินผลการเรียนรู้", $fontStyleBold, 'pStyleLeft');

$table->addRow();
$cell = $table->addCell(5000, $styleCell);
$cell->addText("ความคลาดเคลื่อน", $fontStyleBold, 'pStyleLeft');

$cell = $table->addCell(5000, $styleCell);
$cell->addText("เหตุผล", $fontStyleBold, 'pStyleLeft');


$key = find_key($key_data,"Evaluation Error");
$data = $elements->$key->{'0'}->value;
$table->addRow();
$cell = $table->addCell(5000, $styleCell);
$cell->addText($data, $fontStyle, 'pStyleLeft');


$key = find_key($key_data,"Evaluation Error Reason");
$data = $elements->$key->{'0'}->value;
$cell = $table->addCell(5000, $styleCell);
$cell->addText($data, $fontStyle, 'pStyleLeft');

$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 2));
$cell->addText("7. การทวนผลสัมฤทธิ์ของนักศึกษา", $fontStyleBold, 'pStyleLeft');

$table->addRow();
$cell = $table->addCell(5000, $styleCell);
$cell->addText("วิธีการทวนสอบ", $fontStyleBold, 'pStyleLeft');

$cell = $table->addCell(5000, $styleCell);
$cell->addText("สรุปผล", $fontStyleBold, 'pStyleLeft');


$key = find_key($key_data,"Student Investigation Method");
$data = $elements->$key->{'0'}->value;
$table->addRow();
$cell = $table->addCell(5000, $styleCell);
$cell->addText($data, $fontStyle, 'pStyleLeft');


$key = find_key($key_data,"Student Investigation Summary");
$data = $elements->$key->{'0'}->value;
$cell = $table->addCell(5000, $styleCell);
$cell->addText($data, $fontStyle, 'pStyleLeft');

$section->addPageBreak();


$section->addText('หมวดที่ 4 ปัญหาและผลกระทบต่อการดำเนินการ', $fontStyleBold, 'pStyle');
$section->addText("  1. ประเด็นด้านทรัพยากรประกอบการเรียนและสิ่งอำนวยความสะดวก", $fontStyleBold);


$table = $section->addTable('TQFTableStyleBordered');


$table->addRow();
$cell = $table->addCell(5000, $styleCell);
$cell->addText("ปัญหาในการใช้แหล่งทรัพยากร
ประกอบการเรียนการสอน (ถ้ามี)", $fontStyleBold, 'pStyle');

$cell = $table->addCell(5000, $styleCell);
$cell->addText("ผลกระทบ", $fontStyleBold, 'pStyle');


$key = find_key($key_data,"Resource Problem");
$data = $elements->$key->{'0'}->value;
$table->addRow();
$cell = $table->addCell(5000, $styleCell);
$cell->addText($data, $fontStyle, 'pStyleLeft');


$key = find_key($key_data,"Resource Problem Impact");
$data = $elements->$key->{'0'}->value;
$cell = $table->addCell(5000, $styleCell);
$cell->addText($data, $fontStyle, 'pStyleLeft');

$section->addText("  2. ประเด็นด้านการบริหารและองค์กร", $fontStyleBold);


$table = $section->addTable('TQFTableStyleBordered');


$table->addRow();
$cell = $table->addCell(5000, $styleCell);
$cell->addText("ปัญหาด้านการบริหารและองค์กร (ถ้ามี)", $fontStyleBold, 'pStyle');

$cell = $table->addCell(5000, $styleCell);
$cell->addText("ผลกระทบต่อผลการเรียนรู้ของนักศึกษา", $fontStyleBold, 'pStyle');


$key = find_key($key_data,"Management Problem");
$data = $elements->$key->{'0'}->value;
$table->addRow();
$cell = $table->addCell(5000, $styleCell);
$cell->addText($data, $fontStyle, 'pStyleLeft');


$key = find_key($key_data,"Management Problem Impact");
$data = $elements->$key->{'0'}->value;
$cell = $table->addCell(5000, $styleCell);
$cell->addText($data, $fontStyle, 'pStyleLeft');


$section->addText('หมวดที่ 5 การประเมินรายวิชา', $fontStyleBold, 'pStyle');
$table = $section->addTable('TQFTableStyleBordered');
$table->addRow();
$cell = $table->addCell(10000, $styleCell);

$cell->addText("1. ผลการประเมินรายวิชาโดยนักศึกษา", $fontStyleBold);
$table->addRow();
$cell = $table->addCell(10000, $styleCell);

$key = find_key($key_data,"Student Comment");
$data = $elements->$key->{'0'}->value;
$cell->addText("  1.1. ข้อวิพากษ์ที่สำคัญจากผลการประเมินโดยนักศึกษา", $fontStyleBold);
$cell->addText("  {$data}", $fontStyle);


$key = find_key($key_data,"Teacher Comment");
$data = $elements->$key->{'0'}->value;
$cell->addText("  1.2. ความเห็นของอาจารย์ผู้สอนต่อผลการประเมินตามข้อ 1.1", $fontStyleBold);
$cell->addText("  {$data}", $fontStyle);


$cell->addText("2. ผลการประเมินรายวิชาโดยวิธีอื่น", $fontStyleBold);
$table->addRow();
$cell = $table->addCell(10000, $styleCell);

$key = find_key($key_data,"Other Student Comment");
$data = $elements->$key->{'0'}->value;
$cell->addText("  2.1. ข้อวิพากษ์ที่สำคัญจากผลการประเมินโดยวิธีอื่น", $fontStyleBold);
$cell->addText("  {$data}", $fontStyle);


$key = find_key($key_data,"Other Teacher Comment");
$data = $elements->$key->{'0'}->value;
$cell->addText("  2.2. ความเห็นของอาจารย์ผู้สอนต่อผลการประเมินตามข้อ 2.1", $fontStyleBold);
$cell->addText("  {$data}", $fontStyle);

$section->addPageBreak();





$section->addText('หมวดที่ 6 แผนการปรับปรุง', $fontStyleBold, 'pStyle');

$table = $section->addTable('TQFTableStyleBordered');
$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 3));
$cell->addText("1. ความก้าวหนัาของการปรับปรุงการเรียนการสอนตามที่เสนอในรายงาน/รายวิชาครั้งที่ผ่านมา", $fontStyleBold, 'pStyleLeft');

$table->addRow();
$cell = $table->addCell(5000, $styleCell);
$cell->addText("แผนการปรับปรุงที่เสนอในภาคการศึกษา/
ปีการศึกษาที่ผ่านมา", $fontStyleBold, 'pStyle');

$cell = $table->addCell(5000, array('gridSpan' => 2));
$cell->addText("ผลการดำเนินการ", $fontStyleBold, 'pStyle');


$key = find_key($key_data,"Improvement Planning");
$data = $elements->$key->{'0'}->value;
$table->addRow();
$cell = $table->addCell(5000, $styleCell);
$cell->addText($data, $fontStyle, 'pStyleLeft');


$key = find_key($key_data,"Improvement Planning Result");
$data = $elements->$key->{'0'}->value;
$cell = $table->addCell(5000, array('gridSpan' => 2));
$cell->addText($data, $fontStyle, 'pStyleLeft');

$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 3));
$cell->addText("  2. การดำเนินการอื่นๆในการปรับปรุงรายวิชา", $fontStyleBold, 'pStyleLeft');

$key = find_key($key_data,"Other Improvement");
$data = $elements->$key->{'0'}->value;
$cell->addText("  {$data}", $fontStyle, 'pStyleLeft');



$table = $section->addTable('TQFTableStyleBordered');
$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 3));
$cell->addText("3. ข้อเสนอแผนการปรับปรุงสำหรับภาคการศึกษา/ปีการศึกษาต่อไป", $fontStyleBold, 'pStyleLeft');


$table->addRow();
$cell = $table->addCell(3333, $styleCell);
$cell->addText("ข้อเสนอ", $fontStyleBold, 'pStyle');

$cell = $table->addCell(3333, $styleCell);
$cell->addText("กำหนดเวลาที่แล้วเสร็จ", $fontStyleBold, 'pStyle');

$cell = $table->addCell(3333, $styleCell);
$cell->addText("ผู้รับผิดชอบ", $fontStyleBold, 'pStyle');

$improvement_result = $mysqli->query("select * from tqf5_improvement where submission_id='{$_REQUEST[id]}' order by sequence asc");

while($improvement_row = $improvement_result->fetch_array()){
$table->addRow();


$table->addCell(3333, $styleCell)->addText($improvement_row["improvement"], $fontStyle, 'pStyleLeft');

$table->addCell(3333, $styleCell)->addText($improvement_row["assign_time"], $fontStyle, 'pStyleLeft');

$table->addCell(3333, $styleCell)->addText($improvement_row["responsible_person"], $fontStyle, 'pStyleLeft');
}



$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 3));
$cell->addText("4. ข้อเสนอแนะของอาจารย์ผู้รับผิดชอบรายวิชา ต่ออาจารย์ผู้รับผิดชอบหลักสูตร", $fontStyleBold, 'pStyleLeft');
$key = find_key($key_data,"Teacher Recommendation");
$data = $elements->$key->{'0'}->value;
$cell->addText("  {$data}", $fontStyle, 'pStyleLeft');


$section->addTextBreak(3);

$table = $section->addTable('TQFTableStyle');
$table->addRow();
$cell = $table->addCell(2000, $styleCell);
$cell->addText("ลงชื่อ", $fontStyle);

$cell = $table->addCell(8000, $styleCell);
foreach($teachers as $key=>$data){
	$cell->addText("{$data}", $fontStyle);
}

$cell->addText("อาจารย์ผู้รับผิดชอบรายวิชา/ผู้รายงาน", $fontStyle);

// Save File
$objWriter = PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
$objWriter->save('tqf5_report/tqf5.docx');

header("location: tqf5_report/tqf5.docx");
?>
<a href="tqf5_report/tqf5.docx" target="_blank">Download</a>