<?php
require_once 'PHPWord.php';

require_once '../../../configuration.php';

function find_key($key_data, $find){
	foreach($key_data->elements as $key=>$val){
		if($val->name==$find){
			return $key;
		}
	}
}

function formatThaiDate($date){

	$month_map = array("01"=>"มกราคม", "02"=>"กุมภาพันธ์", "03"=>"มีนาคม", "04"=>"เมษายน", "05"=>"พฤษภาคม", "06"=>"มิถุนายน", "07"=>"กรกฎาคม", "08"=>"สิงหาคม", "09"=>"กันยายน", "10"=>"ตุลาคม", "11"=>"พฤศจิกายน", "12"=>"ธันวาคม");
	if($date=="")
		return $date;
  $raw_date = explode("-", $date);
  $raw_date[0]+=543;
  return  intval($raw_date[2]) . " " . $month_map[$raw_date[1]] . " " . $raw_date[0];
}



//Connect mySQL

$config = new JConfig();



//$conn = mysql_connect($JConfig->host, $JConfig->user, $JConfig->password);
$mysqli = new mysqli($config->host, $config->user, $config->password, $config->db);
//mysql_select_db($JConfig->db);

$mysqli->query("SET NAMES UTF8");


//Retrieve General info Data
$result = $mysqli->query("	select *, (select name from tqf_tqf_curriculum where id=curriculum_id) as curriculum
	from tqf7_general_info info
	where submission_id='{$_REQUEST[id]}'");
$row_info = $result->fetch_array();

//Retrieve Teacher info Data
// $teacher_result = $mysqli->query("select * from tqf_tqf_course_has_teacher where course_id='{$row_info[course_id]}' and state=1");
// $teachers = array();
// while($teacher_row = $teacher_result->fetch_array()){
// 	$teacher_sub_row_result = $mysqli->query("select name from tqf_tqf_teacher where id='{$teacher_row[teacher_id]}' and state=1");
// 	$teacher_sub_row = $teacher_sub_row_result->fetch_array();
// 	$teacher_data = $teacher_sub_row["name"] . "  " . $teacher_row["position"];
// 	array_push($teachers, $teacher_data);
// }

// //Retrieve Pre-Requisite Data
// $pre_result = $mysqli->query("select * from tqf_tqf_course_requisite where course_id='{$row_info[course_id]}' and requisite_type='Pre' and state=1");
// $pre_requisites = array();
// while($pre_row = $pre_result->fetch_array()){
// 	$pre_sub_row_result = $mysqli->query("select * from tqf_tqf_course where id='{$pre_row[course_id2]}'");
// 	$pre_sub_row = $pre_sub_row_result->fetch_array();
// 	$pre_data = $pre_sub_row["course_number"] . " " . $pre_sub_row["name"];
// 	array_push($pre_requisites, $pre_data);
// }


// //Retrieve Co-Requisite Data
// $co_result = $mysqli->query("select * from tqf_tqf_course_requisite where course_id='{$row_info[course_id]}' and requisite_type='Co' and state=1");
// $co_requisites = array();
// while($co_row = $co_result->fetch_array()){
// 	$co_sub_row_result = $mysqli->query("select * from tqf_tqf_course where id='{$co_row[course_id2]}'");
// 	$co_sub_row = $co_sub_row_result->fetch_array();
// 	$co_data = $co_sub_row["course_number"] . " " . $co_sub_row["name"];
// 	array_push($co_requisites, $co_data);
// }


//Retrieve Data
$result = $mysqli->query("select * from tqf_zoo_item where id='{$_REQUEST[id]}'");
$row = $result->fetch_array();



$key_data = file_get_contents("../../../media/zoo/applications/business/types/tqf7.config");
$key_data = json_decode($key_data);

//print_r($key_data);


$elements = json_decode($row["elements"]);




//print_r($JConfig);

// New Word Document
$PHPWord = new PHPWord();


// Styling by AFRONUTT!!
// Define font style for first row
$fontStyle = array('align'=>'center', 'size'=>16, 'name'=>"Browallia New");
$fontStyleBold = array('bold'=>true, 'size'=>16, 'name'=>"Browallia New");

$PHPWord->addParagraphStyle('pStyle', array('align'=>'center'));
$PHPWord->addParagraphStyle('pStyleAlignLeft', array('align'=>'left'));
$PHPWord->addParagraphStyle('pStyleAlignRight', array('align'=>'right'));


// New portrait section
$section = $PHPWord->createSection();


// Header
$header = $section->createHeader();
$table = $header->addTable();
$table->addRow();
$table->addCell(10000)->addText('มคอ.7', $fontStyle, 'pStyleAlignRight');


// Footer

// Add footer
$footer = $section->createFooter();
$footer->addPreserveText('สำนักงานคณะกรรมการอุดมศึกษา                                                                                   {PAGE}', $fontStyle, 'pStyleAlignLeft');


/*
$section->addText('I am inline styled.', array('name'=>'Verdana', 'color'=>'006699'));


$PHPWord->addFontStyle('rStyle', array('bold'=>true, 'italic'=>true, 'size'=>16));
$PHPWord->addParagraphStyle('pStyle', array('align'=>'center', 'spaceAfter'=>100));
$section->addText('I am styled by two style definitions.', 'rStyle', 'pStyle');
$section->addText('I have only a paragraph style definition.', null, 'pStyle');
*/

// Define table style arrays
//'borderSize'=>1, 'borderColor'=>'000000', 
$styleTable = array('cellMargin'=>0);

$styleTableBordered = array('borderSize'=>1, 'borderColor'=>'000000', 'cellMargin'=>10);
//$styleFirstRow = array('borderBottomSize'=>18, 'borderBottomColor'=>'0000FF', 'bgColor'=>'66BBFF');

// Define cell style arrays
//$styleCell = array('valign'=>'center');
//$styleCellBTLR = array('valign'=>'center', 'textDirection'=>PHPWord_Style_Cell::TEXT_DIR_BTLR);



// Add table style
$PHPWord->addTableStyle('TQFTableStyle', $styleTable, $styleFirstRow);
$PHPWord->addTableStyle('TQFTableStyleBordered', $styleTableBordered, $styleFirstRow);

$section->addText('รายงานผลการดำเนินการของหลักสูตรรัฐประศาสนศาสตรบัณฑิต   
ประจำปีการศึกษา '.$row_info['report_academic_year'], $fontStyleBold, 'pStyle');
$section->addTextBreak(1);

// Add table
$table = $section->addTable('TQFTableStyle');



/*

PAGE 1

*/

// Add row
$table->addRow();

// Add cells
$table->addCell(3000, $styleCell)->addText('ชื่อสถาบันอุดมศึกษา', $fontStyleBold);
$table->addCell(10000, $styleCell)->addText($row_info['study_place'], $fontStyle);
//$row_info['faculty']

$table->addRow();

$table->addCell(3000, $styleCell)->addText('วิทยาเขต/คณะ/ภาควิชา', $fontStyleBold);
$table->addCell(10000, $styleCell)->addText($row_info['curriculum'], $fontStyle);
//$row_info['department']
$section->addTextBreak(1);
$section->addText('หมวดที่ 1 ข้อมูลทั่วไป', $fontStyleBold, 'pStyle');

// Add table
$table = $section->addTable('TQFTableStyleBordered');

// Add row
$table->addRow();

// Add cells
$cell = $table->addCell(10000, array('gridSpan' => 4));
$cell->addText('1. หลักสูตร  '.$row_info['curriculum'], $fontStyleBold);


// $cell->addText("         {$row_info[course_number]} {$row_info[name]}
// 	{$row_info[eng_name]}", $fontStyle);

// $credit_info = explode(" ", $row_info["credit"]);
/*
$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('2. จำนวนหน่วยกิต', $fontStyleBold);
$cell->addText("         {$credit_info[0]} หน่วยกิต {$credit_info[1]}", $fontStyle);
*/

$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 4));
$cell->addText('2. ระดับคุณวุฒิ '.$row_info['degree'], $fontStyleBold);
// if(count($pre_requisites)==0)
// 	$cell->addText("         ไม่มี", $fontStyle);
// foreach($pre_requisites as $key=>$data){
// 	$cell->addText("         {$data}", $fontStyle);
// }


$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 4));
$cell->addText('3. อาจารย์ผู้รับผิดชอบหลักสูตร', $fontStyleBold);
// $cell->addText('ผู้ช่วยศาสตราจารย์ ดร. จตุวัฒน์ วโรดมพันธ์ และอาจารย์ผู้รับผิดชอบหลักสูตร มีดังนี้', $fontStyle);

$table->addRow();
$table->addCell(500, $styleCell)->addText('ลำดับ', $fontStyleBold, 'pStyle');
$table->addCell(5000, $styleCell)->addText('ชื่อ-นามสกุล', $fontStyleBold, 'pStyle');
$table->addCell(3500, $styleCell)->addText('คุณวุฒิ', $fontStyleBold, 'pStyle');
$table->addCell(3000, $styleCell)->addText('เลขประจำตัวประชาชน', $fontStyleBold, 'pStyle');




$result2 = $mysqli->query("	select *, (select name from tqf_tqf_teacher where id=teacher_id) as teacher_name
	from tqf7_curriculum_has_teacher 
	where submission_id='{$_REQUEST[id]}'");

$i=1;
while ($row2 = $result2->fetch_array()) {
	$row2["qualification"] = str_replace(",", "\n", $row2["qualification"]);
	$table->addRow();
	$table->addCell(500, $styleCell)->addText($i, $fontStyle, 'pStyle');
	$table->addCell(5000, $styleCell)->addText($row2["teacher_name"], $fontStyle, 'pStyleLeft');
	$table->addCell(3500, $styleCell)->addText($row2["qualification"], $fontStyle, 'pStyleLeft');
	$table->addCell(3000, $styleCell)->addText($row2["citizen_no"], $fontStyle, 'pStyleLeft');
	$i++;
}

// $table->addRow();
// $table->addCell(500, $styleCell)->addText('2', $fontStyle, 'pStyle');
// $table->addCell(3000, $styleCell)->addText('นางนงศิรนารถ กุศลวงษ์', $fontStyle, 'pStyleLeft');
// $table->addCell(1000, $styleCell)->addText('อาจารย์', $fontStyle, 'pStyle');
// $table->addCell(2500, $styleCell)->addText('นบ.(นิติศาสตรบัณฑิต)
// นม.(นิติศาสตร์มหาบัณฑิต)', $fontStyle, 'pStyleLeft');
// $table->addCell(3000, $styleCell)->addText('จุฬาลงกรณ์มหาวิทยาลัย
// จุฬาลงกรณ์มหาวิทยาลัย', $fontStyle, 'pStyleLeft');

// $table->addRow();
// $table->addCell(500, $styleCell)->addText('3', $fontStyle, 'pStyle');
// $table->addCell(3000, $styleCell)->addText('ว่าที่ร้อยตำรวจโท
// กมล ตันจินวัฒนกุล', $fontStyle, 'pStyleLeft');
// $table->addCell(1000, $styleCell)->addText('อาจารย์', $fontStyle, 'pStyle');
// $table->addCell(2500, $styleCell)->addText('นบ.(นิติศาสตรบัณฑิต)
// Master of Law', $fontStyle, 'pStyleLeft');
// $table->addCell(3000, $styleCell)->addText('มหาวิทยาลัยธรรมศาตร์
// Palacky University Czech republic', $fontStyle, 'pStyleLeft');

// $table->addRow();
// $table->addCell(500, $styleCell)->addText('4', $fontStyle, 'pStyle');
// $table->addCell(3000, $styleCell)->addText('นางสาวนฤมล ติบวงษ์ษา', $fontStyle, 'pStyleLeft');
// $table->addCell(1000, $styleCell)->addText('อาจารย์', $fontStyle, 'pStyle');
// $table->addCell(2500, $styleCell)->addText('นบ.(นิติศาสตรบัณฑิต)
// นม.(นิติศาสตร์มหาบัณฑิต)
// นบท. (กฎหมาย)', $fontStyle, 'pStyleLeft');
// $table->addCell(3000, $styleCell)->addText('มหาวิทยาลัยรามคำแหง
// มหาวิทยาลัยรามคำแหง
// สำนักอบรมศึกษาแห่งเนติบัณฑิตยสภา', $fontStyle, 'pStyleLeft');

// $table->addRow();
// $table->addCell(500, $styleCell)->addText('5', $fontStyle, 'pStyle');
// $table->addCell(3000, $styleCell)->addText('นายสันติเลิศ เพ็ชรอาภรณ์', $fontStyle, 'pStyleLeft');
// $table->addCell(1000, $styleCell)->addText('อาจารย์', $fontStyle, 'pStyle');
// $table->addCell(2500, $styleCell)->addText('นบ.(นิติศาสตรบัณฑิต)
// นม.(นิติศาสตร์มหาบัณฑิต)', $fontStyle, 'pStyleLeft');
// $table->addCell(3000, $styleCell)->addText('มหาวิทยาลัยธุรกิจบัณฑิต
// มหาวิทยาลัยธุรกิจบัณฑิต', $fontStyle, 'pStyleLeft');

// $table->addRow();
// $table->addCell(500, $styleCell)->addText('6', $fontStyle, 'pStyle');
// $table->addCell(3000, $styleCell)->addText('นางสาวปานรดา วัชรสินธ์', $fontStyle, 'pStyleLeft');
// $table->addCell(1000, $styleCell)->addText('อาจารย์', $fontStyle, 'pStyle');
// $table->addCell(2500, $styleCell)->addText('นบ.(นิติศาสตรบัณฑิต)
// นม.(นิติศาสตร์มหาบัณฑิต)
// นบท. (กฎหมาย)', $fontStyle, 'pStyleLeft');
// $table->addCell(3000, $styleCell)->addText('มหาวิทยาลัยรามคำแหง
// มหาวิทยาลัยรามคำแหง
// สำนักอบรมศึกษาแห่งเนติบัณฑิตยสภา', $fontStyle, 'pStyleLeft');

$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 4));
$cell->addText('4. วันที่รายงาน', $fontStyleBold);
$cell->addText("         " . formatThaiDate($row_info['report_date']), $fontStyle);


$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 4));
$cell->addText('5. ปีการศึกษาที่รายงาน', $fontStyleBold);
$cell->addText("         {$row_info[report_academic_year]}", $fontStyle);
// $cell->addText("2556", $fontStyle);


$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 4));
$cell->addText('6. สถานที่ตั้ง', $fontStyleBold);
$cell->addText("         {$row_info[study_place]}", $fontStyle);

$section->addPageBreak();

/*

PAGE 2

*/

$section->addText('หมวดที่ 2 ข้อมูลเชิงสถิติ', $fontStyleBold, 'pStyle');
// Add table
$table = $section->addTable('TQFTableStyleBordered');


$key = find_key($key_data,"Total First Year Student");
$data = $elements->$key->{'0'}->value;

// Add row
$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 8));
$cell->addText("1. จำนวนนักศึกษาชั้นปีที่ 1 ที่รับเข้าในปีการศึกษาที่รายงาน	{$data}	คน", $fontStyleBold, 'pStyleLeft');



$key = find_key($key_data,"Total Graduate Student");
$data = $elements->$key->{'0'}->value;

// Add row
$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 8));
$cell->addText("2. จำนวนนักศึกษาที่สำเร็จการศึกษาในปีที่รายงาน		{$data}	คน", $fontStyleBold, 'pStyleLeft');

$key = find_key($key_data,"Total Graduate Before");
$data = $elements->$key->{'0'}->value;

$cell->addText("  2.1 จำนวนนักศึกษาที่สำเร็จการศึกษาก่อนกำหนดเวลาของหลักสูตร	 {$data}  คน", $fontStyle, 'pStyleLeft');

$key = find_key($key_data,"Total Graduate On Time");
$data = $elements->$key->{'0'}->value;

$cell->addText("  2.2 จำนวนนักศึกษาที่สำเร็จการศึกษาตามกำหนดเวลาของหลักสูตร	{$data}  คน", $fontStyle, 'pStyleLeft');

$key = find_key($key_data,"Total Graduate After");
$data = $elements->$key->{'0'}->value;

$cell->addText("  2.3 จำนวนนักศึกษาที่สำเร็จการศึกษาหลังกำหนดเวลาของหลักสูตร	 {$data}  คน", $fontStyle, 'pStyleLeft');
$cell->addText("  2.4 จำนวนนักศึกษาที่สำเร็จการศึกษาในวิชาเอกต่าง ๆ (ระบุ)", $fontStyle, 'pStyleLeft');
$key = find_key($key_data,"Total Graduate By Major");

foreach($elements->$key as $eKey=>$eValue){
	$graduate_major = $eValue->value;
	$cell->addText("         {$graduate_major} คน", $fontStyle, 'pStyleLeft');
}

// foreach($graduate_major as $key=>$data){ }


$key = find_key($key_data,"Total Graduate Student Percent");
$data = $elements->$key->{'0'}->value;

$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 8));
$cell->addText('3. รายละเอียดเกี่ยวกับอัตราการสำเร็จการศึกษา', $fontStyleBold, 'pStyleLeft');
$cell->addText("	{$data}", $fontStyle, 'pStyleLeft');

// Add row
$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 8));
$cell->addText('4. จำนวนและร้อยละนักศึกษาที่สอบผ่านตามแผนการศึกษาของหลักสูตรในแต่ละปี', $fontStyleBold, 'pStyleLeft');

$table->addRow();
$table->addCell(3000, array('vMerge' => 'restart'))->addText('รุ่น/ปีที่เข้า', $fontStyleBold, 'pStyle');
$table->addCell(7000, array('gridSpan' => 7))->addText('ปีการศึกษา(จำนวนคนรับจริง)', $fontStyleBold, 'pStyle');

$table->addRow();
$table->addCell(3000,array('vMerge' => ''));
$table->addCell(1000, $styleCell)->addText('2552', $fontStyleBold, 'pStyle');
$table->addCell(1000, $styleCell)->addText('2553', $fontStyleBold, 'pStyle');
$table->addCell(1000, $styleCell)->addText('2554', $fontStyleBold, 'pStyle');
$table->addCell(1000, $styleCell)->addText('2555', $fontStyleBold, 'pStyle');
$table->addCell(1000, $styleCell)->addText('2556', $fontStyleBold, 'pStyle');
$table->addCell(1000, $styleCell)->addText('2557', $fontStyleBold, 'pStyle');
$table->addCell(1000, $styleCell)->addText('2558', $fontStyleBold, 'pStyle');

//Retrieve student_amount_history Data
$result = $mysqli->query("	select *
	from tqf7_student_amount_history
	where submission_id='{$_REQUEST[id]}'");
	$amount_array = array();
	$residues_array = array();
	$total_array = array();
	$graduate_array = array();
	$percent_array = array();

while ($row_info = $result->fetch_array()) {
	if($row_info['type']==1){
			$amount_array[$row_info['batch']][$row_info['year']] = $row_info['amount'];
	}else if($row_info['type']==2){
			$residues_array[$row_info['year']] = $row_info['amount'];
	}else if($row_info['type']==3){
			$total_array[$row_info['year']] = $row_info['amount'];
	}else if($row_info['type']==4){
			$graduate_array[$row_info['year']] = $row_info['amount'];
	}else if($row_info['type']==5){
			$percent_array[$row_info['year']] = $row_info['amount'];
	}
}

$table->addRow();
$table->addCell(3000, $styleCell)->addText('รุ่น 7 (1/2558)', $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText('', $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText('', $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText('', $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText('', $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText('', $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText('', $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($amount_array[7][2558], $fontStyle, 'pStyle');

$table->addRow();
$table->addCell(3000, $styleCell)->addText('รุ่น 6 (1/2557)', $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText('', $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText('', $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText('', $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText('', $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText('', $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($amount_array[6][2557], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($amount_array[6][2558], $fontStyle, 'pStyle');

$table->addRow();
$table->addCell(3000, $styleCell)->addText('รุ่น 5 (1/2556)', $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText('', $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText('', $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText('', $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText('', $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($amount_array[5][2556], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($amount_array[5][2557], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($amount_array[5][2558], $fontStyle, 'pStyle');

$table->addRow();
$table->addCell(3000, $styleCell)->addText('รุ่น 4 (1/2555)', $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText('', $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText('', $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText('', $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($amount_array[4][2555], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($amount_array[4][2556], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($amount_array[4][2557], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($amount_array[4][2558], $fontStyle, 'pStyle');

$table->addRow();
$table->addCell(3000, $styleCell)->addText('รุ่น 3 (1/2554)', $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText('', $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText('', $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($amount_array[3][2554], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($amount_array[3][2555], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($amount_array[3][2556], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($amount_array[3][2557], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($amount_array[3][2558], $fontStyle, 'pStyle');

$table->addRow();
$table->addCell(3000, $styleCell)->addText('รุ่น 2 (1/2553)', $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText('', $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($amount_array[2][2553], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($amount_array[2][2554], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($amount_array[2][2555], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($amount_array[2][2556], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($amount_array[2][2557], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($amount_array[2][2558], $fontStyle, 'pStyle');

$table->addRow();
$table->addCell(3000, $styleCell)->addText('รุ่น 1 (1/2552)', $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($amount_array[1][2552], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($amount_array[1][2553], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($amount_array[1][2554],$fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($amount_array[1][2555], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($amount_array[1][2556], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($amount_array[1][2557], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($amount_array[1][2558], $fontStyle, 'pStyle');

$table->addRow();
$table->addCell(3000, $styleCell)->addText('ตกค้าง', $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($residues_array[2552], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($residues_array[2553], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($residues_array[2554], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($residues_array[2555], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($residues_array[2556], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($residues_array[2557], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($residues_array[2558], $fontStyle, 'pStyle');

$table->addRow();
$table->addCell(3000, $styleCell)->addText('รวม', $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($total_array[2552], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($total_array[2553], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($total_array[2554], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($total_array[2555], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($total_array[2556], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($total_array[2557], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($total_array[2558], $fontStyle, 'pStyle');

$table->addRow();
$table->addCell(3000, $styleCell)->addText('จบ', $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($graduate_array[2552], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($graduate_array[2553], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($graduate_array[2554], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($graduate_array[2555], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($graduate_array[2556], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($graduate_array[2557], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($graduate_array[2558], $fontStyle, 'pStyle');

$table->addRow();
$table->addCell(3000, $styleCell)->addText('ร้อยละนักศึกษาที่สอบผ่านตามแผนกำหนดการศึกษา(คำนวณจากจำนวนนักศึกษาปีที่ 2 ของแต่ละรุ่น)', $fontStyleBold, 'pStyle');
$table->addCell(1000, $styleCell)->addText($percent_array[2552], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($percent_array[2553], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($percent_array[2554], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($percent_array[2555], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($percent_array[2556], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($percent_array[2557], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($percent_array[2558], $fontStyle, 'pStyle');
//$cell->addText('ร้อยละของนักศึกษาที่สำเร็จการศึกษาตามหลักสูตร 94 %', $fontStyle, 'pStyle');


// Add row

$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 8));
$cell->addText('5.อัตราการเปลี่ยนแปลงจำนวนนักศึกษาในแต่ละปีการศึกษา (คิดจากนักศึกษารุ่น 2)', $fontStyleBold, 'pStyleLeft');
$cell->addText('	สัดส่วนของนักศึกษาที่สอบผ่านตามแผนกำหนดการศึกษาและยังคงศึกษาต่อในหลักสูตรเปรียบเทียบกับจำนวนนักศึกษาทั้งหมดของรุ่นในปีที่ผ่านมา', $fontStyle, 'pStyleLeft');

$key = find_key($key_data,"Total 1to2 Student");
$data = $elements->$key->{'0'}->value;

$cell->addText("นักศึกษาชั้นปีที่ 1 ที่เรียนต่อชั้นปีที่ 2  {$data}", $fontStyle, 'pStyle');

$key = find_key($key_data,"Total 2to3 Student");
$data = $elements->$key->{'0'}->value;

$cell->addText("นักศึกษาชั้นปีที่ 2 ที่เรียนต่อชั้นปีที่ 3  {$data}", $fontStyle, 'pStyle');

$key = find_key($key_data,"Total 3to4 Student");
$data = $elements->$key->{'0'}->value;
$cell->addText("นักศึกษาชั้นปีที่ 3 ที่เรียนต่อชั้นปีที่ 4  {$data}", $fontStyle, 'pStyle');

$key = find_key($key_data,"Total 4to5 Student");
$data = $elements->$key->{'0'}->value;

if($data!="")
	$cell->addText("นักศึกษาชั้นปีที่ 4 ที่เรียนต่อชั้นปีที่ 5  {$data}", $fontStyle, 'pStyle');

// Add row
$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 8));
$cell->addText('6. ปัจจัย/สาเหตุที่มีผลกระทบต่อจำนวนนักศึกษาตามแผนการศึกษา', $fontStyleBold, 'pStyleLeft');

$key = find_key($key_data,"Total Number Of Student Causes");
$data = $elements->$key->{'0'}->value;


$cell->addText("	{$data}", $fontStyle, 'pStyleLeft');

// Add row
$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 8));
$cell->addText('7. ภาวะการได้งานทำของบัณฑิตภายในระยะ 1 ปีหลังการศึกษา', $fontStyleBold, 'pStyleLeft');
// $cell->addText("	บัณฑิตในระดับปริญญาได้ทำงานทำ 75%", $fontStyle, 'pStyleLeft');

$key = find_key($key_data,"Questionnaire Academic Year");
$data = $elements->$key->{'0'}->value;

$cell->addText("	วันที่สำรวจ ณ วันซ้อมรับปริญญา ประจำปีการศึกษา {$data}", $fontStyle, 'pStyleLeft');

$key = find_key($key_data,"Total Questionnaire");
$data = $elements->$key->{'0'}->value;

$key = find_key($key_data,"Total Answer Questionnaire");
$data2 = $elements->$key->{'0'}->value;

$cell->addText("	จำนวนแบบสอบถามที่ส่ง {$data} ฉบับ จำนวนแบบสอบถามที่ตอบกลับ {$data2} ฉบับ", $fontStyle, 'pStyleLeft');

$key = find_key($key_data,"Total Answer Questionnaire Percent");
$data = $elements->$key->{'0'}->value;
$cell->addText("	ร้อยละของผู้ตอบแบบสอบถามกลับ {$data}", $fontStyle, 'pStyleLeft');

//Retrieve work_statistics Data
$result = $mysqli->query("	select *
	from tqf7_work_statistics
	where submission_id='{$_REQUEST[id]}'");
$row_info = $result->fetch_array();

$cell->addText("	การกระจายภาวะการได้งานทำเทียบกับจำนวนผู้ตอบแบบสอบถาม", $fontStyle, 'pStyleLeft');

$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 8));
$cell->addText('การกระจายภาวะการได้งานทำเทียบกับจำนวนผู้ตอบแบบสอบถาม', $fontStyle, 'pStyleLeft');

$table->addRow();
$table->addCell(1000, array('gridSpan' => 2),array('vMerge' => 'restart'))->addText('การได้งานทำ', $fontStyleBold, 'pStyle');
$table->addCell(4000, array('gridSpan' => 2))->addText('ได้งานทำแล้ว', $fontStyle, 'pStyle');
$table->addCell(4000, array('gridSpan' => 2))->addText('ไม่ประสงค์จะทำงาน', $fontStyle, 'pStyle');
$table->addCell(1000, array('gridSpan' => 2),array('vMerge' => 'restart'))->addText('ยังไม่ได้งานทำ', $fontStyle, 'pStyle');


$table->addRow();
$table->addCell(1000, array('gridSpan' => 2),array('vMerge' => ''));
$table->addCell(2000, $styleCell)->addText('ตรงสาขาที่เรียน', $fontStyle, 'pStyle');
$table->addCell(2000, $styleCell)->addText('ไม่ตรงสาขาที่เรียน', $fontStyle, 'pStyle');
$table->addCell(2000, $styleCell)->addText('ศึกษาต่อ', $fontStyle, 'pStyle');
$table->addCell(2000, $styleCell)->addText('สาเหตุอื่น', $fontStyle, 'pStyle');
$table->addCell(1000, array('gridSpan' => 2),array('vMerge' => ''));

$table->addRow();
$table->addCell(1000, array('gridSpan' => 2))->addText('จำนวน', $fontStyle, 'pStyle');
$table->addCell(2000, $styleCell)->addText($row_info['work_direct'], $fontStyle, 'pStyle');
$table->addCell(2000, $styleCell)->addText($row_info['work_indirect'], $fontStyle, 'pStyle');
$table->addCell(2000, $styleCell)->addText($row_info['study'], $fontStyle, 'pStyle');
$table->addCell(2000, $styleCell)->addText($row_info['other_cause'], $fontStyle, 'pStyle');
$table->addCell(1000, array('gridSpan' => 2))->addText($row_info['unemployed'], $fontStyle, 'pStyle');

$table->addRow();
$table->addCell(1000, array('gridSpan' => 2))->addText('ร้อยละของผู้ตอบกลับ', $fontStyle, 'pStyle');
$table->addCell(2000, $styleCell)->addText($row_info['work_direct_percent'], $fontStyle, 'pStyle');
$table->addCell(2000, $styleCell)->addText($row_info['work_indirect_percent'], $fontStyle, 'pStyle');
$table->addCell(2000, $styleCell)->addText($row_info['study_percent'], $fontStyle, 'pStyle');
$table->addCell(2000, $styleCell)->addText($row_info['other_cause_percent'], $fontStyle, 'pStyle');
$table->addCell(1000, array('gridSpan' => 2))->addText($row_info['unemployed_percent'], $fontStyle, 'pStyle');


$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 8));
$cell->addText('* ร้อยละให้คิดจากจำนวนแบบสอบถามของผู้ตอบกลับ', $fontStyle, 'pStyleLeft');

$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 8));
$cell->addText('8. การวิเคราะห์ผลที่ได้', $fontStyleBold, 'pStyleLeft');

$key = find_key($key_data,"Questionnaire Analysis");
$data = $elements->$key->{'0'}->value;
$cell->addText("	{$data}", $fontStyle, 'pStyleLeft');

//$section->addTextBreak(1);
$section->addPageBreak();


$section->addText('หมวดที่ 3 การเปลี่ยนแปลงที่มีผลกระทบต่อหลักสูตร', $fontStyleBold, 'pStyle');
// Add table
$table = $section->addTable('TQFTableStyleBordered');

// Add row
$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('1. การเปลี่ยนแปลงภายในสถาบัน ที่มีผลกระทบต่อหลักสูตรในช่วง 2 ปีที่ผ่านมา', $fontStyleBold, 'pStyleLeft');
$key = find_key($key_data,"Internal Affected Change");
$data = $elements->$key->{'0'}->value;
$cell->addText("	{$data}", $fontStyle, 'pStyleLeft');

// Add row
$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('2. การเปลี่ยนแปลงภายนอกสถาบัน ที่มีผลกระทบต่อหลักสูตรในช่วง 2 ปีที่ผ่านมา', $fontStyleBold, 'pStyleLeft');
$key = find_key($key_data,"External Affected Change");
$data = $elements->$key->{'0'}->value;
$cell->addText("	{$data}", $fontStyle, 'pStyleLeft');

$section->addPageBreak();


$section->addText('หมวดที่ 4 ข้อมูลสรุปรายวิชาของหลักสูตร', $fontStyleBold, 'pStyle');

$section->addText('1. สรุปผลรายวิชาที่เปิดสอนในการศึกษา/ปีการศึกษา', $fontStyleBold, 'pStyleLeft');

$table = $section->addTable('TQFTableStyleBordered');

// $table->addRow();
// $cell = $table->addCell(10000, array('gridSpan' => 14));
// $cell->addText('จำนวนนักศึกษาแต่ละรุ่นในภาคการศึกษา 2556 มีจำนวนทั้งหมด 320 คน สอบผ่านทุกรายวิชา เฉลี่ยส่วนใหญ่มีระดับคะแนนอยู่ที่ 60-69', $fontStyle, 'pStyleLeft');

$table->addRow();
$table->addCell(4000, $styleCell)->addText('ชื่อรายวิชา', $fontStyleBold, 'pStyle');
$table->addCell(1000, $styleCell)->addText('ภาค/ปีการศึกษา', $fontStyleBold, 'pStyle');
$table->addCell(500, $styleCell)->addText('A', $fontStyleBold, 'pStyle');
$table->addCell(500, $styleCell)->addText('B+', $fontStyleBold, 'pStyle');
$table->addCell(500, $styleCell)->addText('B', $fontStyleBold, 'pStyle');
$table->addCell(500, $styleCell)->addText('C+', $fontStyleBold, 'pStyle');
$table->addCell(500, $styleCell)->addText('C', $fontStyleBold, 'pStyle');
$table->addCell(500, $styleCell)->addText('D+', $fontStyleBold, 'pStyle');
$table->addCell(500, $styleCell)->addText('D', $fontStyleBold, 'pStyle');
$table->addCell(500, $styleCell)->addText('F', $fontStyleBold, 'pStyle');
$table->addCell(500, $styleCell)->addText('S', $fontStyleBold, 'pStyle');
$table->addCell(500, $styleCell)->addText('U', $fontStyleBold, 'pStyle');
$table->addCell(500, $styleCell)->addText('จำนวน นศ.ที่ลงเรียน', $fontStyleBold, 'pStyle');
$table->addCell(500, $styleCell)->addText('จำนวน นศ.ที่สอบผ่าน', $fontStyleBold, 'pStyle');

//Retrieve course_grade_summary Data
$result = $mysqli->query("	select *,(select name from tqf_tqf_course where id=course_id) as course,(select semester from tqf_tqf_course where id=course_id) as semester,(select academic_year from tqf_tqf_course where id=course_id) as academic_year
	from tqf7_course_grade_summary cgs
	left join tqf_tqf_course c on c.id = cgs.course_id
	where submission_id='{$_REQUEST[id]}'");
While ($row_info = $result->fetch_array()) {
$table->addRow();
$table->addCell(4000, $styleCell)->addText($row_info['course_number'] . " " . $row_info['course'], $fontStyle, 'pStyleLeft');
$table->addCell(1000, $styleCell)->addText($row_info['semester']."/".$row_info['academic_year'], $fontStyle, 'pStyle');
$table->addCell(500, $styleCell)->addText($row_info['a'], $fontStyle, 'pStyle');
$table->addCell(500, $styleCell)->addText($row_info['b_plus'], $fontStyle, 'pStyle');
$table->addCell(500, $styleCell)->addText($row_info['b'], $fontStyle, 'pStyle');
$table->addCell(500, $styleCell)->addText($row_info['c_plus'], $fontStyle, 'pStyle');
$table->addCell(500, $styleCell)->addText($row_info['c'], $fontStyle, 'pStyle');
$table->addCell(500, $styleCell)->addText($row_info['d_plus'], $fontStyle, 'pStyle');
$table->addCell(500, $styleCell)->addText($row_info['d'], $fontStyle, 'pStyle');
$table->addCell(500, $styleCell)->addText($row_info['f'], $fontStyle, 'pStyle');
$table->addCell(500, $styleCell)->addText($row_info['s'], $fontStyle, 'pStyle');
$table->addCell(500, $styleCell)->addText($row_info['u'], $fontStyle, 'pStyle');
$table->addCell(500, $styleCell)->addText($row_info['student_amount'], $fontStyle, 'pStyle');
$table->addCell(500, $styleCell)->addText($row_info['pass_student_amount'], $fontStyle, 'pStyle');
}


$section->addTextBreak(1);
// $key = find_key($key_data,"Abnormal Score Causes");
// $data = $elements->$key->{'0'}->value;
//$cell->addText($data, $fontStyle, 'pStyleLeft');

///////////////////////// 4.2
$table = $section->addTable('TQFTableStyleBordered');
$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 5));
$cell->addText("2. การวิเคราะห์รายวิชาที่มีผลการเรียนไม่ดี", $fontStyleBold, 'pStyleLeft');

$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 5));
$cell->addText("2.1 รหัสและชื่อรายวิชา", $fontStyleBold, 'pStyleLeft');

$table->addRow();
$cell = $table->addCell(2000, $styleCell);
$cell->addText("รหัสและชื่อรายวิชา", $fontStyleBold, 'pStyle');

$cell = $table->addCell(2000, $styleCell);
$cell->addText("ความไม่ปกติที่พบ", $fontStyleBold, 'pStyle');

$cell = $table->addCell(2000, $styleCell);
$cell->addText("การดำเนินการตรวจสอบ", $fontStyleBold, 'pStyle');

$cell = $table->addCell(2000, $styleCell);
$cell->addText("เหตุผลที่ทำให้เกิดความไม่ปกติจากข้อกำหนด", $fontStyleBold, 'pStyle');

$cell = $table->addCell(2000, $styleCell);
$cell->addText("มาตรการแก้ไขที่ได้ดำเนินการแล้ว", $fontStyleBold, 'pStyle');


//Retrieve close_subject Data
$result = $mysqli->query("	select *,(select name from tqf_tqf_course where id=course_id) as course
	from tqf7_abnormal_analysis cs
	left join tqf_tqf_course c on c.id = cs.course_id
	where submission_id='{$_REQUEST[id]}'");
while($row_info = $result->fetch_array()){
$table->addRow();	
$cell = $table->addCell(2000, $styleCell);
$cell->addText($row_info['course'], $fontStyle, 'pStyleLeft');

$cell = $table->addCell(2000, $styleCell);
$cell->addText($row_info['abnormality'], $fontStyle, 'pStyleLeft');

$cell = $table->addCell(2000, $styleCell);
$cell->addText($row_info['inspection'], $fontStyle, 'pStyleLeft');

$cell = $table->addCell(2000, $styleCell);
$cell->addText($row_info['cause'], $fontStyle, 'pStyleLeft');

$cell = $table->addCell(2000, $styleCell);
$cell->addText($row_info['fix'], $fontStyle, 'pStyleLeft');
}

//Retrieve abnormal_analysis Data
/*
$result = $mysqli->query("	select *
	from tqf7_abnormal_analysis
	where submission_id='{$_REQUEST[id]}'");
$row_info = $result->fetch_array();



$cell->addText('	ยังไม่ได้เรียกมา', $fontStyle, 'pStyleLeft');

$cell = $table->addCell(5000, $styleCell);
$cell->addText("ความไม่ปกติที่พบ", $fontStyleBold, 'pStyleLeft');
$cell->addText($row_info['abnormality'], $fontStyle, 'pStyleLeft');


$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 2));
$cell->addText("การดำเนินการตรวจสอบ", $fontStyleBold, 'pStyleLeft');
$cell->addText($row_info['inspection'], $fontStyle, 'pStyleLeft');
$cell->addText('เหตุผลที่ทำให้เกิดความไม่ปกติจากข้อกำหนด หรือ เกณฑ์ที่ตั้งไว้', $fontStyleBold, 'pStyleLeft');
$cell->addText(		$row_info['cause'], $fontStyle, 'pStyleLeft');

$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 2));
$cell->addText('มาตรการแก้ไขที่ได้ดำเนินการแล้ว', $fontStyleBold, 'pStyleLeft');
$cell->addText(		$row_info['fix'], $fontStyle, 'pStyleLeft');
*/
$section->addTextBreak(1);

////////////////////////////// 4.3
$section->addText('3. การเปิดรายวิชาในภาคหรือปีการศึกษา', $fontStyleBold, 'pStyle');
$table = $section->addTable('TQFTableStyleBordered');

$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 3));
$cell->addText("3.1 รายวิชาที่ไม่ได้เปิดตามแผนการศึกษา และเหตุผลที่ไม่ได้เปิด", $fontStyleBold, 'pStyleLeft');

$table->addRow();
$cell = $table->addCell(3400, $styleCell);
$cell->addText("รหัสและรายวิชา", $fontStyleBold, 'pStyle');

$cell = $table->addCell(3300, $styleCell);
$cell->addText("คำอธิบาย", $fontStyleBold, 'pStyle');

$cell = $table->addCell(3300, $styleCell);
$cell->addText("มาตรการทดแทนที่ได้ดำเนินการ", $fontStyleBold, 'pStyle');


//Retrieve close_subject Data
$result = $mysqli->query("	select *,(select name from tqf_tqf_course where id=course_id) as course
	from tqf7_close_subject cs
	left join tqf_tqf_course c on c.id = cs.course_id
	where submission_id='{$_REQUEST[id]}'");
while($row_info = $result->fetch_array()){
$table->addRow();	
$cell = $table->addCell(3400, $styleCell);
$cell->addText($row_info['course'], $fontStyle, 'pStyleLeft');

$cell = $table->addCell(3300, $styleCell);
$cell->addText($row_info['detail'], $fontStyle, 'pStyleLeft');

$cell = $table->addCell(3300, $styleCell);
$cell->addText($row_info['fix'], $fontStyle, 'pStyleLeft');
}

$section->addTextBreak(1);

//3.2

$table = $section->addTable('TQFTableStyleBordered');

$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 4));
$cell->addText("3.2 วิธีแก้ไขกรณีที่มีการสอนเนื้อหาในรายวิชาไม่ครบถ้วน", $fontStyleBold, 'pStyleLeft');

$table->addRow();
$cell = $table->addCell(2500, $styleCell);
$cell->addText("รายวิชา", $fontStyleBold, 'pStyle');

$cell = $table->addCell(2500, $styleCell);
$cell->addText("สาระหรือหัวข้อที่ขาด", $fontStyleBold, 'pStyle');

$cell = $table->addCell(2500, $styleCell);
$cell->addText("สาเหตุที่ไม่ได้สอน", $fontStyleBold, 'pStyle');

$cell = $table->addCell(2500, $styleCell);
$cell->addText("การแก้ไขที่ได้ดำเนินการแล้ว", $fontStyleBold, 'pStyle');

//Retrieve incomplete_subject Data
$result = $mysqli->query("	select *,(select name from tqf_tqf_course where id=course_id) as course
	from tqf7_incomplete_subject ins
	left join tqf_tqf_course c on c.id = ins.course_id
	where submission_id='{$_REQUEST[id]}'");

while($row_info = $result->fetch_array()){
$table->addRow();
$cell = $table->addCell(2500, $styleCell);
$cell->addText($row_info['course'], $fontStyle, 'pStyleLeft');

$cell = $table->addCell(2500, $styleCell);
$cell->addText($row_info['topic'], $fontStyle, 'pStyleLeft');

$cell = $table->addCell(2500, $styleCell);
$cell->addText($row_info['cause'], $fontStyle, 'pStyleLeft');

$cell = $table->addCell(2500, $styleCell);
$cell->addText($row_info['fix'], $fontStyle, 'pStyleLeft');
}

$section->addPageBreak();



/////////////////////////////////// 5
$section->addText('หมวดที่ 5 การบริหารหลักสูตร', $fontStyleBold, 'pStyle');
$table = $section->addTable('TQFTableStyleBordered');

$table->addRow();
$cell = $table->addCell(3400, $styleCell);
$cell->addText("ปัญหาในการบริหารหลักสูตร", $fontStyleBold, 'pStyle');

$cell = $table->addCell(3300, $styleCell);
$cell->addText("ผลกระทบของปัญหาต่อสัมฤทธิผลตามวัตถุประสงค์ของหลักสูตร", $fontStyleBold, 'pStyle');

$cell = $table->addCell(3300, $styleCell);
$cell->addText("แนวทางการป้องกันและแก้ไขปัญหาในอนาคต", $fontStyleBold, 'pStyle');

//Retrieve management_problem Data
$result = $mysqli->query("	select * from tqf7_management_problem where submission_id='{$_REQUEST[id]}'");

while($row_info = $result->fetch_array()){

$table->addRow();
$cell = $table->addCell(3400, $styleCell);
$cell->addText($row_info['problem'], $fontStyle, 'pStyleLeft');

$cell = $table->addCell(3300, $styleCell);
$cell->addText($row_info['effect'], $fontStyle, 'pStyleLeft');

$cell = $table->addCell(3300, $styleCell);
$cell->addText($row_info['prevention'], $fontStyle, 'pStyleLeft');
}

$section->addPageBreak();

//////////////////////////////////////// 6
$section->addText('หมวดที่ 6 สรุปการประเมินหลักสูตร', $fontStyleBold, 'pStyle');

$table = $section->addTable('TQFTableStyleBordered');

$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 2));
$cell->addText("1. การประเมินจากผู้ที่กำลังจะสำเร็จการศึกษา (รายงานตามปีที่สำรวจ)", $fontStyleBold, 'pStyleLeft');

$key = find_key($key_data,"Evaluation Date");
$data = $elements->$key->{'0'}->value;
$cell->addText("	วันที่สำรวจ {$data} ", $fontStyle, 'pStyleLeft');

$table->addRow();
$cell = $table->addCell(5000, $styleCell);
$cell->addText("  1.1 ข้อวิพากษ์ที่สำคัญจากผลการประเมิน", $fontStyleBold, 'pStyle');

$key = find_key($key_data,"Important Opinion Weak");
$data = $elements->$key->{'0'}->value;

$key = find_key($key_data,"Important Opinion Strong");
$data2 = $elements->$key->{'0'}->value;

$cell->addText("จุดอ่อน  {$data}
จุดแข็ง {$data2}", $fontStyle, 'pStyleLeft');

$cell = $table->addCell(5000, $styleCell);
$cell->addText("ข้อคิดเห็นของคณาจารย์ต่อผลการประเมิน", $fontStyleBold, 'pStyle');

$key = find_key($key_data,"Teacher Opinion");
$data = $elements->$key->{'0'}->value;
$cell->addText("{$data}", $fontStyle, 'pStyleLeft');

$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 2));
$cell->addText("  1.2 ข้อเสนอการเปลี่ยนแปลงในหลักสูตรขากผลการประเมิน 1.1", $fontStyleBold, 'pStyleLeft');

$key = find_key($key_data,"Suggestion");
$data = $elements->$key->{'0'}->value;
$cell->addText("    {$data}", $fontStyle, 'pStyleLeft');

$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 2));
$cell->addText("2. การประเมินจากผู้มีส่วนเกี่ยวข้อง", $fontStyleBold, 'pStyleLeft');
$key = find_key($key_data,"Related Evaluation");
$data = $elements->$key->{'0'}->value;
$cell->addText("  {$data}", $fontStyle, 'pStyleLeft');

$table->addRow();
$cell = $table->addCell(5000, $styleCell);
$cell->addText("  2.1 ข้อวิพากษ์ที่สำคัญจากผลการประเมิน", $fontStyleBold, 'pStyleLeft');

$key = find_key($key_data,"Related Important Opinion Weak");
$data = $elements->$key->{'0'}->value;

$key = find_key($key_data,"Related Important Opinion Stong");
$data2 = $elements->$key->{'0'}->value;
$cell->addText("จุดอ่อน  {$data}
จุดแข็ง {$data2}", $fontStyle, 'pStyleLeft');

$cell = $table->addCell(5000, $styleCell);
$cell->addText("ข้อคิดเห็นของคณาจารย์ต่อผลการประเมิน", $fontStyleBold, 'pStyleLeft');
$key = find_key($key_data,"Related Teacher Opinion");
$data3 = $elements->$key->{'0'}->value;
$cell->addText("{$data3}", $fontStyle, 'pStyleLeft');



$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 2));
$cell->addText("  2.2 ข้อเสนอการเปลี่ยนแปลงในหลักสูตรจากผลการประเมินข้อ 2.1", $fontStyleBold, 'pStyleLeft');
$key = find_key($key_data,"Related Suggestion");
$data = $elements->$key->{'0'}->value;
$cell->addText("    {$data}", $fontStyle, 'pStyleLeft');

$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 2));
$cell->addText("3. การประเมินคุณภาพหลักสูตรตามกรอบมาตรฐานคุรวุฒิฯ", $fontStyleBold, 'pStyleLeft');

$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 2));
$cell->addText("  3.1 ระบบการประเมินและการให้ช่วงคะแนนที่ใช้ในการประเมินการเรียนการสอน และการประเมินอื่น ๆ ที่ใช้กับการประเมินหลักสูตร", $fontStyleBold, 'pStyleLeft');
$cell->addText("ตัวบ่งชี้ผลการดำเนินงาน (Key Performance Indicators) ของหลักสูตร", $fontStyleBold, 'pStyleLeft');
$cell->addText("ชนิดของตัวบ่งชี้ : กระบวนการ", $fontStyleBold, 'pStyleLeft');
$cell->addText("เกณฑ์มาตรฐาน : ระดับ", $fontStyleBold, 'pStyleLeft');

$section->addTextBreak(1);


$table = $section->addTable('TQFTableStyleBordered');
$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 5));
$cell->addText('ตัวบ่งชี้และเกณฑ์การประเมินคุณภาพเพื่อการประกันคุณภาพหลักสูตรและการเรียนการสอน หลักสูตรนิติศาสตร์', $fontStyleBold,'pStyleLeft');

$table->addRow();
$table->addCell(6000, $styleCell)->addText('ดัชนีบ่งชี้ผลการดำเนินงาน', $fontStyleBold, 'pStyle');
$table->addCell(1000, $styleCell)->addText('ปีที่ 1', $fontStyleBold, 'pStyle');
$table->addCell(1000, $styleCell)->addText('ปีที่ 2', $fontStyleBold, 'pStyle');
$table->addCell(1000, $styleCell)->addText('ปีที่ 3', $fontStyleBold, 'pStyle');
$table->addCell(1000, $styleCell)->addText('ปีที่ 4', $fontStyleBold, 'pStyle');

$map_kpi = array('1' => "(1) มีอาจารย์ผู้รับผิดชอบหลักสูตร ตามเกณฑ์มาตรฐานหลักสูตรของสำนักงานคณะกรรมการการอุดมศึกษา",
'2' => "(2) มีการจัดทำรายละเอียดของหลักสูตร ตามแบบ มคอ.2 ที่สอดคล้องกับมาตรฐานคุณวุฒิระดับปริญญาตรีสาขาคอมพิวเตอร์",
'3' => "(3) อาจารย์ประจำมีส่วนร่วมในการวางแผน ติดตาม และทบทวนผลการดำเนินงานหลักสูตร",
'4' => "(4) มีการจัดทำรายละเอียดของรายวิชาและประสบการณ์ภาคสนาม(ถ้ามี) ที่จัดการเรียนการสอนตามแบบ มคอ.3 และ มคอ.4 ก่อนการเปิดสอนให้ครบทุกรายการวิชา",
'5' => "(5) มีการจัดทำรายงานผลการดำเนินการของรายวิชาและประสบการณ์ภาคสนาม (ถ้ามี) ตามแบบ มคอ.5 และ มคอ.6 ภายใน 30 วัน หลังการเรียนการสอนให้ครบทุกรายวิชา",
'6' => "(6) มีการจัดทำรายงานผลการดำเนินการของหลักสูตร ตามแบบมคอ.7 ภายใน 60 วัน หลังสิ้นสุดปีการศึกษา",
'7' => "(7) มีการพัฒนา/ปรับปรุงการจัดการเรียนการสอน กลยุทธ์การสอนหรือ การประเมินผลการเรียนรู้ จากผลการประเมินการดำเนินงานที่รายงานในปีก่อน",
'8' => "(8) มีการทวนสอบผลสัมฤทธิ์ของนักศึกษาตามมาตรฐานผลการเรียนรู้ที่กำหนดในมาตรฐานคุณวุฒิ",
'9' => "(9) อาจารย์ใหม่ทุกคนได้รับการปฐมนิเทศหรือคำแนะนำด้านการจัดการเรียนการสอน (เฉพาะปีที่มีการรับรองอาจารย์ใหม่)",
'10'=> "(10) อาจารย์ประจำได้รับการพัฒนา ไม่น้อยกว่าร้อยละ 50 ปี",
'11' => "(11) จำนวนบุคลากรสนับสนุนการเรียนการสอนได้รับการพัฒนาด้านการเรียนการสอนและอื่นๆ ไม่น้อยกว่าร้อยละ 50 ต่อปี",
'12' => "(12) ระดับความพึงพอใจของนักศึกษาต่อคุณภาพการเรียนการสอนและทรัพยากรสนับสนุนในสาขาวิชา เฉลี่ยไม่น้อยกว่า 3.5 จากคะแนนเต็ม 5.0",
'13' => "(13) จำนวนนักศึกษาที่สำเร็จการศึกษาตามกำหนดเวลาของหลักสูตรไม่น้อยกว่าร้อยละ90 ของจำนวนนักศึกษาที่คงอยู่ในชั้นปีที่ 2",
'14' => "(14) จำนวนนักศึกษาที่รับเข้าเป็นไปตามแผน",
'15' => "(15) ความพึงพอใจของผู้ใช้บัณฑิตไม่ต่ำกว่า 3.5 จากระดับ 5 (หลังจากบัณฑิตสำเร็จการศึกษาอย่างน้อย 1ปี)",
'16' => "(16) ร้อยละของนักศึกษามีงานทำภายใน 1 ปี หลังจากสำเร็จการศึกษา ไม่ต่ำกว่าร้อยละ 80",
'17' => "(17) บัณฑิตที่ได้งานทำได้รับเงินเดือนเริ่มต้นไม่ตำ่กว่าเกณฑ์ ก.พ. กำหนด");
$map_is = array('0' => "X",'1' => "/" );



//Retrieve kpi Data
$result = $mysqli->query("	select *
	from tqf7_kpi
	where submission_id='{$_REQUEST[id]}'");
while($row_info = $result->fetch_array()){
$table->addRow();
$table->addCell(6000, $styleCell)->addText($map_kpi[$row_info['kpi_no']], $fontStyle, 'pStyleLeft');
$table->addCell(1000, $styleCell)->addText($map_is[$row_info['is_year1']], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($map_is[$row_info['is_year2']], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($map_is[$row_info['is_year3']], $fontStyle, 'pStyle');
$table->addCell(1000, $styleCell)->addText($map_is[$row_info['is_year4']], $fontStyle, 'pStyle');
}

$section->addTextBreak(1);


$table = $section->addTable('TQFTableStyleBordered');

$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 3));
$cell->addText('การประกันคุณภาพหลักสูตรตามตัวบ่งชี้และเกณฑ์การประเมินคุณภาพเพื่อการประกันคุณภาพหลักสูตร และการเรียนการสอน', $fontStyleBold,'pStyleLeft');

$table->addRow();
$table->addCell(6000, $styleCell)->addText('ประเด็น/ตัวบ่งชี้การประกันคุณภาพหลักสูตร', $fontStyleBold, 'pStyle');
$table->addCell(1000, $styleCell)->addText('การปฏิบัติ มี(/),ไม่มี(x)', $fontStyleBold, 'pStyle');
$table->addCell(3000, $styleCell)->addText('คำชี้แจงการปฏิบัติหรือรายการหลักฐาน', $fontStyleBold, 'pStyle');


//Retrieve index Data
$result = $mysqli->query("	select *
	from tqf7_index
	where submission_id='{$_REQUEST[id]}'");
while($row_info = $result->fetch_array()){
$table->addRow();
$table->addCell(6000, $styleCell)->addText($map_kpi[$row_info['index_no']], $fontStyle, 'pStyleLeft');
$table->addCell(1000, $styleCell)->addText($map_is[$row_info['is_yearly_result']]."  ".$row_info['yearly_result_detail'], $fontStyle, 'pStyle');
$table->addCell(3000, $styleCell)->addText($row_info['evidence'], $fontStyle, 'pStyleLeft');
}

$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 3));
$cell->addText('	สรุปผลการดำเนินงาน การประกันคุณภาพหลักสูตรตามตัวบ่งชี้และเกณฑ์การประเมินคุณภาพเพื่อการประกันคุณภาพหลักสูตร และการเรียนการสอน ในปีการศึกษา 2556 มีผลการดำเนินงาน 10 ข้อ จากจำนวนประเด็น/ตัวบ่งชี้ที่ต้องดำเนินการ ทั้งหมด 12 ข้อ คิดเป็นร้อยละ 83.3 ผลการประเมิน ผ่าน', $fontStyle,'pStyleLeft');

$section->addPageBreak();

$section->addText('หมวดที่ 7 คุณภาพการสอน', $fontStyleBold, 'pStyle');
// Add table
$table = $section->addTable('TQFTableStyleBordered');

$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 6));
$cell->addText('1. การประเมินรายวิชาที่เปิดสอนในปีที่รายงาน', $fontStyleBold,'pStyleLeft');
$cell->addText('	ประเมินคุณภาพการสอน ผลการประเมินคุณภาพการสอนอยู่ในระดับ 3.51 ขึ้นไป จากคะแนนเต็ม 5 ในทุกรายวิชา', $fontStyle,'pStyleLeft');
$cell->addText('  1.1 รายวิชาที่มีการประเมินคุณภาพการสอนและแผนการปรับปรุงจากผลการประเมิน (ตัวอย่าง)', $fontStyleBold, 'pStyleLeft');

$table->addRow();
$table->addCell(3000, array('vMerge' => 'restart'))->addText('รายวิชาที่เปิดสอน', $fontStyleBold, 'pStyle');
$table->addCell(2000, array('gridSpan' => 2))->addText('การประเมินโดยนักศึกษา', $fontStyleBold, 'pStyle');
$table->addCell(3000,array('vMerge' => 'restart'))->addText('การประเมินคุณภาพการสอนโดยวิธีอื่นๆ', $fontStyleBold, 'pStyle');
$table->addCell(2000, array('gridSpan' => 2))->addText('แผนปรับปรุงคุณภาพการสอน', $fontStyleBold, 'pStyle');

$table->addRow();
$table->addCell(3000, array('vMerge' => ''));
$table->addCell(1000, $styleCell)->addText('มี', $fontStyleBold, 'pStyle');
$table->addCell(1000, $styleCell)->addText('ไม่มี', $fontStyleBold, 'pStyle');
$table->addCell(3000, array('vMerge' => ''));
$table->addCell(1000, $styleCell)->addText('มี', $fontStyleBold, 'pStyle');
$table->addCell(1000, $styleCell)->addText('ไม่มี', $fontStyleBold, 'pStyle');



//Retrieve evaluation Data
$result = $mysqli->query("	select *,(select name from tqf_tqf_course where id=course_id) as course
	from tqf7_evaluation e
	left join tqf_tqf_course c on c.id = e.course_id
	where submission_id='{$_REQUEST[id]}'");

while($row_info = $result->fetch_array()){
$table->addRow();
$table->addCell(3000, $styleCell)->addText($row_info['course'], $fontStyle, 'pStyleLeft');

if($row_info['is_evaluate']==1){ 
	$data_evaluate = $map_is[$row_info['is_evaluate']];
}

$table->addCell(1000, $styleCell)->addText($data_evaluate, $fontStyle, 'pStyle');

if($row_info['is_evaluate']==0){ 
	$data_not_evaluate = $map_is[$row_info['is_evaluate']];
}
$table->addCell(1000, $styleCell)->addText($data_not_evaluate, $fontStyle, 'pStyle');
$table->addCell(3000, $styleCell)->addText($row_info['course'], $fontStyle, 'pStyleLeft');

if($row_info['is_plan_operate']==1){
	$data_plan_operate = $map_is[$row_info['is_plan_operate']];
}
$table->addCell(1000, $styleCell)->addText($data_plan_operate, $fontStyle, 'pStyle');

if($row_info['is_plan_operate']==0){
 	$data_not_plan_operate = $map_is[$row_info['is_plan_operate']];
}
$table->addCell(1000, $styleCell)->addText($data_not_plan_operate, $fontStyle, 'pStyle');
}                                                                                                                                                                                                                                                                                     

$section->addPageBreak();

$section->addText('  1.2 ผลการประเมินคุณภาพการสอนโดยรวม ', $fontStyleBold, 'pStyleLeft');
$table = $section->addTable('TQFTableStyleBordered');

$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 2));
$cell->addText('2. ประสิทธิผลของกลยุทธ์การสอน', $fontStyleBold,'pStyleLeft');
$key = find_key($key_data,"Teaching Performance Evaluation");
$data = $elements->$key->{'0'}->value;
$cell->addText("	{$data}", $fontStyle, 'pStyleLeft');

$table->addRow();
$cell = $table->addCell(5000,$styleCell);
$cell->addText('	สรุปข้อคิดเห็นของผู้สอน และข้อมูลป้อนกลับจากแหล่งต่าง ๆ ต่อสัมฤทธิผลของการสอนและผลการเรียนรู้จามกลุ่มสาระหลักทั้ง 5 ประการ', $fontStyleBold,'pStyleLeft');
$cell = $table->addCell(5000,$styleCell);
$cell->addText('	แนวทางแก้ไข/ปรับปรุง', $fontStyleBold,'pStyleLeft');

$table->addRow();
$cell = $table->addCell(5000,$styleCell);

$key = find_key($key_data,"Teaching Evaluation Comment");
$data = $elements->$key->{'0'}->value;

$key = find_key($key_data,"Teaching Evaluation Improvement");
$data2 = $elements->$key->{'0'}->value;

$cell->addText("	{$data}", $fontStyle,'pStyleLeft');
$cell = $table->addCell(5000,$styleCell);
$cell->addText("	{$data2}", $fontStyle,'pStyleLeft');

$table->addRow();
$cell = $table->addCell(5000,$styleCell);
$cell->addText('  2.1 คุณธรรม จริยธรรม', $fontStyleBold,'pStyleLeft');
$key = find_key($key_data,"Moral Evaluation");
$data = $elements->$key->{'0'}->value;
$cell->addText("	{$data}", $fontStyle,'pStyleLeft');
$cell = $table->addCell(5000,$styleCell);

$key = find_key($key_data,"Moral Evaluation Improvement");
$data2 = $elements->$key->{'0'}->value;
$cell->addText("  แนวทางแก้ไข/ปรับปรุง คุณธรรม จริยธรรม", $fontStyleBold,'pStyleLeft');
$cell->addText("	{$data2}", $fontStyle,'pStyleLeft');

$table->addRow();
$cell = $table->addCell(5000,$styleCell);
$cell->addText('  2.2 ความรู้', $fontStyleBold,'pStyleLeft');
$key = find_key($key_data,"Knowledge Evaluation");
$data = $elements->$key->{'0'}->value;
$cell->addText("	{$data}", $fontStyle,'pStyleLeft');
$cell = $table->addCell(5000,$styleCell);

$key = find_key($key_data,"Knowledge Evaluation Improvement");
$data2 = $elements->$key->{'0'}->value;
$cell->addText("  แนวทางแก้ไข/ปรับปรุง ความรู้", $fontStyleBold,'pStyleLeft');
$cell->addText("	{$data2}");

$table->addRow();
$cell = $table->addCell(5000,$styleCell);
$cell->addText('  2.3 ทักษะทางปัญญา', $fontStyleBold,'pStyleLeft');
$key = find_key($key_data,"Skill Evaluation");
$data = $elements->$key->{'0'}->value;
$cell->addText("	{$data}", $fontStyle,'pStyleLeft');
$cell = $table->addCell(5000,$styleCell);

$key = find_key($key_data,"Skill Evaluation Improvement");
$data2 = $elements->$key->{'0'}->value;
$cell->addText('  แนวทางแก้ไข/ปรับปรุง ทักษะทางปัญญา', $fontStyleBold,'pStyleLeft');
$cell->addText("	{$data2}", $fontStyle,'pStyleLeft');

$table->addRow();
$cell = $table->addCell(5000,$styleCell);
$cell->addText('  2.4 ทักษะด้านความสัมพันธ์ระหว่างบุคคลและความรับผิดชอบ', $fontStyleBold,'pStyleLeft');
$key = find_key($key_data,"Human Relation Evaluation");
$data = $elements->$key->{'0'}->value;
$cell->addText("{$data}", $fontStyle,'pStyleLeft');
$cell = $table->addCell(5000,$styleCell);

$key = find_key($key_data,"Human Relation Evaluation Improvement");
$data2 = $elements->$key->{'0'}->value;
$cell->addText('  แนวทางแก้ไข/ปรับปรุง ทักษะด้านความสัมพันธ์ระหว่างบุคคลและความรับผิดชอบ', $fontStyleBold,'pStyleLeft');
$cell->addText("{$data2}", $fontStyle,'pStyleLeft');

$table->addRow();
$cell = $table->addCell(5000,$styleCell);
$cell->addText('  2.5 ทักษะการวิเคราะห์เชิงตัวเลข การสื่อสารและการใช้เทคโนโลยีสารสนเทศ', $fontStyleBold,'pStyleLeft');
$key = find_key($key_data,"Analysis Evaluation");
$data = $elements->$key->{'0'}->value;
$cell->addText("{$data}", $fontStyle,'pStyleLeft');
$cell = $table->addCell(5000,$styleCell);

$key = find_key($key_data,"Analysis Evaluation Improvement");
$data2 = $elements->$key->{'0'}->value;
$cell->addText('  แนวทางแก้ไข/ปรับปรุง ทักษะการวิเคราะห์เชิงตัวเลข การสื่อสารและการใช้เทคโนโลยีสารสนเทศ', $fontStyleBold,'pStyleLeft');
$cell->addText("{$data2}", $fontStyle,'pStyleLeft');

$section->addTextBreak(1);
$table = $section->addTable('TQFTableStyleBordered');
$table->addRow();
$cell = $table->addCell(10000,$styleCell);
$cell->addText('3. การปฐมนิเทศอาจารย์ใหม่', $fontStyleBold,'pStyleLeft');

$key = find_key($key_data,"New Teacher Brief");
$data = $elements->$key->{'0'}->value;
$cell->addText("	จำนวน {$data} คน", $fontStyle,'pStyleLeft');

$table->addRow();
$cell = $table->addCell(10000,$styleCell);
$cell->addText('  3.1 สรุปสาระสำคัญในการดำเนินการ', $fontStyleBold,'pStyleLeft');

$key = find_key($key_data,"New Teacher Brief Summary");
$data = $elements->$key->{'0'}->value;
$cell->addText("	{$data}", $fontStyle,'pStyleLeft');

$table->addRow();
$cell = $table->addCell(10000,$styleCell);
$cell->addText('  3.2 สรุปการประเมินอาจารย์ที่เข้าร่วมกิจกรรมปฐมนิเทศ', $fontStyleBold,'pStyleLeft');
$key = find_key($key_data,"New Teacher Brief Evaluation");
$data = $elements->$key->{'0'}->value;
$cell->addText("	{$data}", $fontStyle,'pStyleLeft');

$table->addRow();
$cell = $table->addCell(10000,$styleCell);
$cell->addText('  3.3 หากไม่มีการจัดปฐมนิเทศ ให้แสดงเหตุผลที่ไม่ได้ดำเนินการ', $fontStyleBold,'pStyleLeft');
$key = find_key($key_data,"New Teacher Brief Cancel Reason");
$data = $elements->$key->{'0'}->value;
$cell->addText("	{$data}", $fontStyle,'pStyleLeft');

$section->addTextBreak(1);
$table = $section->addTable('TQFTableStyleBordered');
$table->addRow();
$table->addCell(7000, array('vMerge' => 'restart'))->addText('4. กิจกรรมการพัฒนาวิชาชีพของอาจารย์และบุคลากรสายสนุบสนุน
	    4.1 กิจกรรมที่จัดหรือเข้าร่วม', $fontStyleBold, 'pStyleLeft');
$table->addCell(3000, array('gridSpan' => 2))->addText('จำนวนผู้เข้าร่วม', $fontStyleBold, 'pStyle');

$table->addRow();
$table->addCell(7000, array('vMerge' => ''));
$table->addCell(1500, $styleCell)->addText('อาจารย์', $fontStyleBold, 'pStyle');
$table->addCell(1500, $styleCell)->addText('บุคลากรสายสนับสนุน', $fontStyleBold, 'pStyle');

//Retrieve activity Data
$result = $mysqli->query("	select *
	from tqf7_activity
	where submission_id='{$_REQUEST[id]}'");
while($row_info = $result->fetch_array()){
$table->addRow();
$table->addCell(7000,$styleCell)->addText($row_info['activity'], $fontStyle,'pStyleLeft');
$table->addCell(1500,$styleCell)->addText($row_info['teacher_amount'], $fontStyle,'pStyle');
$table->addCell(1500,$styleCell)->addText($row_info['support_amount'], $fontStyle,'pStyle');
}














$table->addRow();
$cell = $table->addCell(10000, array('gridSpan' => 3));
$cell->addText('  4.2 ประโยชน์ที่ได้รับจากการเข้าร่วมกิจกรรม', $fontStyleBold,'pStyleLeft');
$key = find_key($key_data,"Support Teacher Activity Comment");
$data = $elements->$key->{'0'}->value;
$cell->addText("	{$data}", $fontStyle,'pStyleLeft');

$section->addPageBreak();

$section->addText('หมวดที่ 8 ข้อคิดเห็นและข้อเสนอแนะเกี่ยวกับคุณภาพหลักสูตรจากผู้ประเมินอิสระ', $fontStyleBold, 'pStyle');
// Add table
$table = $section->addTable('TQFTableStyleBordered');

$table->addRow();
$cell = $table->addCell(5000,$styleCell);
$cell->addText('1. ข้อคิดเห็นหรือสาระที่ได้รับการเสนอแนะจากผู้ประเมิน', $fontStyleBold,'pStyleLeft');
$cell = $table->addCell(5000,$styleCell);
$cell->addText('ความเห็นของประธานหลักสูตรต่อข้อคิดเห็นหรือสาระที่ได้รับการเสนอแนะ', $fontStyleBold,'pStyleLeft');

$table->addRow();
$cell = $table->addCell(5000,$styleCell);
$key = find_key($key_data,"Audit Comment");
$data = $elements->$key->{'0'}->value;
$cell->addText("	{$data}", $fontStyle,'pStyleLeft');

$cell = $table->addCell(5000,$styleCell);
$key = find_key($key_data,"Chairman Comment");
$data2 = $elements->$key->{'0'}->value;
$cell->addText("	{$data2}", $fontStyle,'pStyleLeft');


$table->addRow();
$cell = $table->addCell(10000,array('gridSpan' => 2));
$cell->addText('2. การนำไปดำเนินการเพื่อการวางแผนหรือปรับปรุงหลักสูตร', $fontStyleBold,'pStyleLeft');
$key = find_key($key_data,"Curriculum Improvement Planning");
$data = $elements->$key->{'0'}->value;
$cell->addText("	{$data}", $fontStyle,'pStyleLeft');

$section->addPageBreak();

$section->addText('หมวดที่ 9 แผนการดำเนินการเพื่อพัฒนาหลักสูตร', $fontStyleBold, 'pStyle');
// Add table
$table = $section->addTable('TQFTableStyleBordered');
$table->addRow();
$cell = $table->addCell(10000,$styleCell);
$cell->addText('1. ความก้าวหน้าของการดำเนินงานตามแผนที่เสนอในรายงานของปีที่ผ่านมา', $fontStyleBold,'pStyleLeft');
$cell->addText('	มีการประเมินผลความคิดเห็นจากผู้ใช้บัณฑิตและผู้ประกอบการแล้ว', $fontStyle, 'pStyleLeft');


// $table->addRow();
// $table->addCell(4000,$styleCell)->addText('แผนการดำเนินการ', $fontStyleBold,'pStyle');
// $table->addCell(3000,$styleCell)->addText('วันสิ้นสุดการดำเนินการตามแผน', $fontStyleBold,'pStyle');
// $table->addCell(3000,$styleCell)->addText('ผู้รับผิดชอบ', $fontStyleBold,'pStyle');
// $table->addCell(4000,$styleCell)->addText('ความสำเร็จของแผน', $fontStyleBold,'pStyle');

// $table->addRow();
// $table->addCell(4000,$styleCell)->addText('1.1 แผนการปรับเนื้อหารายวิชาให้สอดคล้องกับการเปลี่ยนแปลงทางเทคโนโลยี', $fontStyle,'pStyle');
// $table->addCell(3000,$styleCell)->addText('ตุลาคม 2556', $fontStyle,'pStyle');
// $table->addCell(3000,$styleCell)->addText('อ.สุเมธ อังคศิริกุล', $fontStyle,'pStyle');
// $table->addCell(4000,$styleCell)->addText('ได้ปรับบางเนื้อหาในรายวิชา INT 101 Information System Fundamental ให้มีความทันสมัย', $fontStyle,'pStyle');

// $table->addRow();
// $table->addCell(4000,$styleCell)->addText('1.2 แผนการเรียนเชิญวิทยากรจากภายนอกมาให้ความรู้ในหัวข้อที่เกี่ยวข้อง', $fontStyle,'pStyle');
// $table->addCell(3000,$styleCell)->addText('ตุลาคม 2556', $fontStyle,'pStyle');
// $table->addCell(3000,$styleCell)->addText('นายสยาม แย้มแสงสังข์', $fontStyle,'pStyle');
// $table->addCell(4000,$styleCell)->addText('ได้เรียนเชิยวิทยากรมาทั้งหมด 2 ท่าน ในหัวข้อวิชา Information Ethics และ User Interface Design', $fontStyle,'pStyle');

//$cell = $table->addCell(10000, array('gridSpan' => 4));
$table->addRow();
$cell = $table->addCell(10000,$styleCell);
$cell->addText('เหตุผลที่ไม่สามารถดำเนินการให้สำเร็จ', $fontStyleBold,'pStyleLeft');
$key = find_key($key_data,"Faild Reason");
$data = $elements->$key->{'0'}->value;
$cell->addText("	{$data}", $fontStyle,'pStyleLeft');

$table->addRow();
$cell = $table->addCell(10000,$styleCell);
$cell->addText('2. ข้อเสนอในการพัฒนาหลักสูตร', $fontStyleBold,'pStyleLeft');

$table->addRow();
$cell = $table->addCell(10000,$styleCell);
$cell->addText('  2.1 ข้อเสนอในการปรับโครงสร้างหลักสูตร(จำนวนหน่วยกิต รายวิชาแกน รายวิชาเลือก ฯ)', $fontStyleBold,'pStyleLeft');
$key = find_key($key_data,"Modification Suggestion");
$data = $elements->$key->{'0'}->value;
$cell->addText("	{$data}", $fontStyle, 'pStyleLeft');

$table->addRow();
$cell = $table->addCell(10000,$styleCell);
$cell->addText('  2.2 ข้อเสนอในการเปลี่ยนแปลงรายวิชา', $fontStyleBold,'pStyleLeft');
$key = find_key($key_data,"Change Suggestion");
$data = $elements->$key->{'0'}->value;
$cell->addText("	{$data}", $fontStyle, 'pStyleLeft');

$table->addRow();
$cell = $table->addCell(10000,$styleCell);
$cell->addText('  2.3 กิจกรรมการพัฒนาคณาจารย์และบุคลากรสายสนับสนุน', $fontStyleBold,'pStyleLeft');
$key = find_key($key_data,"Improve Activity");
$data = $elements->$key->{'0'}->value;
$cell->addText("	{$data}", $fontStyle, 'pStyleLeft');

$section->addTextBreak(1);

$table = $section->addTable('TQFTableStyleBordered');
$table->addRow();
$cell = $table->addCell(10000,$styleCell);
$cell->addText('3. รายงานการประชุมเรื่องการปรับปรุงการจัดการเรียนการสอนสำหรับปี  2557', $fontStyleBold,'pStyleLeft');
$cell->addText('	มีการปรับแผนให้ครอบคลุมทุกพันธกิจ
	๑.มีรายงานการประชุมที่เกี่ยวกับกิจกรรมทางวิชาการเพื่อบูรณาการกับการเรียนการสอน
	๒.มีรายงานการประชุมที่เกี่ยวกับการบูรณาการงานวิจัยกับการเรียนการสอน
	๓.มีรายงานการประชุมที่เกี่ยวกับการบูรณาการศิลปวัฒนธรรมกับการเรียนการสอน
	๔.มีรายงานการประชุมที่เกี่ยวกับการบูรณาการงานบริการวิชาการกับการเรียนการสอน
', $fontStyle, 'pStyleLeft');



// $table->addRow();
// $table->addCell(6000,$styleCell)->addText('แผนการปฏิบัติการ', $fontStyleBold,'pStyle');
// $table->addCell(2000,$styleCell)->addText('วันที่คาดว่าจะสิ้นสุดแผน', $fontStyleBold,'pStyle');
// $table->addCell(2000,$styleCell)->addText('ผู้รับผิดชอบ', $fontStyleBold,'pStyle');

// $table->addRow();
// $table->addCell(6000,$styleCell)->addText('แผนปรับปรุงเนื้อหารายวิชา INT 351 Seminar I', $fontStyle,'pStyleLeft');
// $table->addCell(2000,$styleCell)->addText('31 พฤษภาคม 2558', $fontStyle,'pStyle');
// $table->addCell(2000,$styleCell)->addText('อ.อัจฉรา ธารอุไรกุล', $fontStyle,'pStyleLeft');

// $table->addRow();
// $table->addCell(6000,$styleCell)->addText('แผนปรับปรุงโปรแกรมที่ใช้เพื่อการเรียนการสอนในห้องปฏิบัติการ โดยใช้เวอร์ชั่นที่เป็นที่นิยมในตลาด', $fontStyle,'pStyleLeft');
// $table->addCell(2000,$styleCell)->addText('31 พฤษภาคม 2558', $fontStyle,'pStyle');
// $table->addCell(2000,$styleCell)->addText('ฝา่ยโครงสร้างพื้นฐาน', $fontStyle,'pStyleLeft');

// $table->addRow();
// $table->addCell(6000,$styleCell)->addText('แผนการขยายห้องปฏิบัติการ และเพิ่มจำนวนคอมพิวเตอร์ให้เป็นห้องละ 50 เครื่อง', $fontStyle,'pStyleLeft');
// $table->addCell(2000,$styleCell)->addText('30 กันยายน 2558', $fontStyle,'pStyle');
// $table->addCell(2000,$styleCell)->addText('ฝ่ายโครงสร้างพื้นฐาน', $fontStyle,'pStyleLeft');

// $table->addRow();
// $table->addCell(6000,$styleCell)->addText('การขออนุมัติตั้งงบประมาณในการนำนักศึกษาไปดูงาน ณ สถานประกอยการที่มีอุปกรณ์คอมพิวเตอร์ที่ทันสมัย', $fontStyle,'pStyleLeft');
// $table->addCell(2000,$styleCell)->addText('30 กันยายน 2558', $fontStyle,'pStyle');
// $table->addCell(2000,$styleCell)->addText('อ.สยาม แย้มแสงสังข์ ประธานหลักสูตร', $fontStyle,'pStyleLeft');

// $table->addRow();
// $table->addCell(6000,$styleCell)->addText('แผนการจัดซื้ออุปกรณ์เครือข่าย เพื่อใช้ในการเรียนวิชา INT 201 Computer Network I เพิ่มเติม', $fontStyle,'pStyleLeft');
// $table->addCell(2000,$styleCell)->addText('30 กันยายน 2559', $fontStyle,'pStyle');
// $table->addCell(2000,$styleCell)->addText('อ.สยาม แย้มแสงสังข์ ประธานหลักสูตร', $fontStyle,'pStyleLeft');

// $table->addRow();
// $table->addCell(6000,$styleCell)->addText('แผนการจัดตั้งงบประมาณเพิ่มเติม ในการเชิญวิทยากรมาให้ความรู้เกี่ยวกับเทคโนโลยีสมัยใหม่', $fontStyle,'pStyleLeft');
// $table->addCell(2000,$styleCell)->addText('30 กันยายน 2558', $fontStyle,'pStyle');
// $table->addCell(2000,$styleCell)->addText('อ.สยาม แย้มแสงสังข์ ประธานหลักสูตร', $fontStyle,'pStyleLeft');

$section->addPageBreak();


$result = $mysqli->query("	select *, (select name from tqf_tqf_curriculum where id=curriculum_id) as curriculum
	from tqf7_general_info info
	where submission_id='{$_REQUEST[id]}'");
$row_info = $result->fetch_array();

$result2 = $mysqli->query("	select *, (select name from tqf_tqf_teacher where id=teacher_id) as teacher_name
	from tqf7_curriculum_has_teacher 
	where submission_id='{$_REQUEST[id]}'");
// Add table
	$table = $section->addTable('TQFTableStyle');
	$table->addRow();
	$table->addCell(4000, $styleCell)->addText('อาจารย์ผู้รับผิดชอบหลักสูตร :', $fontStyleBold, 'pStyleLeft');
$i=1;
while ($row2 = $result2->fetch_array()) {
	$row2["qualification"] = str_replace(",", "\n", $row2["qualification"]);
	$table->addRow();
// Add cells
	$table->addCell(4000, $styleCell)->addText($i.". ".$row2["teacher_name"], $fontStyle);
	$table->addCell(10000, $styleCell)->addText("  ลายเซน :			 วันที่รายงาน :".formatThaiDate($row_info['report_date']), $fontStyle);

	$i++;
}

// $section->addText('ประธานหลักสูตร : อ.ปฏิพัทธ์  ภู่งาม', $fontStyleBold, 'pStyleLeft');
// $section->addText('ลายเซน :				วันที่รายงาน : 24 มิ.ย.57', $fontStyle, 'pStyleLeft');

// $section->addText('เห็นชอบโดย อ.ปฏิพัทธ์  ภู่งาม (ประธานสาขาวิชา)', $fontStyleBold, 'pStyleLeft');
// $section->addText('ลายเซน :				วันที่รายงาน : 24 มิ.ย.57', $fontStyle, 'pStyleLeft');

// $section->addText('เห็นชอบโดย รศ.ดร.สุพจน์ แสงเงิน (คณบดี)', $fontStyleBold, 'pStyleLeft');
// $section->addText('ลายเซน :				วันที่รายงาน : 24 มิ.ย.57', $fontStyle, 'pStyleLeft');

$section->addTextBreak(1);
$table = $section->addTable('TQFTableStyle');
	$table->addRow();
	$table->addCell(3000, $styleCell)->addText('เอกสารประกอบรายงาน', $fontStyleBold, 'pStyleLeft');
	$table->addRow();
	$table->addCell(10000, $styleCell)->addText('1. สำเนารายงานรายวิชาทุกวิชา
2. วิธีการให้คะแนนตามกำหนดเกณฑ์มาตรฐานที่ใช้ในการประเมิน
3. ข้อสรุปผลการประเมินของบัณฑิตที่จบการศึกษาในปีที่ประเมิน
4. ข้อสรุปผลการประเมินจากบุคคลภายนอก 
5. รายงานการประชุมที่เกี่ยวข้องกับการปรับปรุงการเรียนการสอนในปีการศึกษา 2557
', $fontStyle, 'pStyleLeft');
// Save File
$objWriter = PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
$objWriter->save('tqf7_report/tqf7.docx');

header("location: tqf7_report/tqf7.docx");
?>
<a href="tqf7_report/tqf7.docx" target="_blank">Download</a>