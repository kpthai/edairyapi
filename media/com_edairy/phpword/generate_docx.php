<?php
require_once 'PHPWord.php';

require_once '../../../configuration.php';

function find_key($key_data, $find){
	foreach($key_data->elements as $key=>$val){
		if($val->name==$find){
			return $key;
		}
	}
}

function formatThaiDate($date){
	return $date;
}
//Connect mySQL

$config = new JConfig();



//$conn = mysql_connect($JConfig->host, $JConfig->user, $JConfig->password);
$mysqli = new mysqli($config->host, $config->user, $config->password, $config->db);
//mysql_select_db($JConfig->db);

$mysqli->query("SET NAMES UTF8");


//Retrieve General info Data
$result = $mysqli->query("	select *, (select name from tqf_tqf_curriculum where id=curriculum_id) as curriculum
	from tqf3_general_info info
	left join tqf_tqf_course c on c.id = info.course_id
	where submission_id='{$_REQUEST[id]}'");
$row_info = $result->fetch_array();

//Retrieve Teacher info Data

$teacher_result = $mysqli->query("select * from tqf_tqf_course_has_teacher where course_id='{$row_info[course_id]}'");
$teachers = array();
while($teacher_row = $teacher_result->fetch_array()){
	$teacher_sub_row_result = $mysqli->query("select name from tqf_tqf_teacher where id='{$teacher_row[teacher_id]}'");
	$teacher_sub_row = $teacher_sub_row_result->fetch_array();
	$teacher_data = $teacher_sub_row["name"] . "  " . $teacher_row["position"];
	array_push($teachers, $teacher_data);
}

//Retrieve Pre-Requisite Data
$pre_result = $mysqli->query("select * from tqf_tqf_course_requisite where course_id='{$row_info[course_id]}' and requisite_type='Pre'");
$pre_requisites = array();
while($pre_row = $pre_result->fetch_array()){
	$pre_sub_row_result = $mysqli->query("select * from tqf_tqf_course where id='{$pre_row[course_id2]}'");
	$pre_sub_row = $pre_sub_row_result->fetch_array();
	$pre_data = $pre_sub_row["course_number"] . " " . $pre_sub_row["name"];
	array_push($pre_requisites, $pre_data);
}


//Retrieve Co-Requisite Data
$co_result = $mysqli->query("select * from tqf_tqf_course_requisite where course_id='{$row_info[course_id]}' and requisite_type='Co'");
$co_requisites = array();
while($co_row = $co_result->fetch_array()){
	$co_sub_row_result = $mysqli->query("select * from tqf_tqf_course where id='{$co_row[course_id2]}'");
	$co_sub_row = $co_sub_row_result->fetch_array();
	$co_data = $co_sub_row["course_number"] . " " . $co_sub_row["name"];
	array_push($co_requisites, $co_data);
}


//Retrieve Data
$result = $mysqli->query("select * from tqf_zoo_item where id='{$_REQUEST[id]}'");
$row = $result->fetch_array();



$key_data = file_get_contents("../../../media/zoo/applications/business/types/tqf3.config");
$key_data = json_decode($key_data);

//print_r($key_data);


$elements = json_decode($row["elements"]);




//print_r($JConfig);

// New Word Document
$PHPWord = new PHPWord();


// Styling by AFRONUTT!!
// Define font style for first row
$fontStyle = array('align'=>'center', 'size'=>16, 'name'=>"Browallia New");
$fontStyleBold = array('bold'=>true, 'size'=>16, 'name'=>"Browallia New");

$PHPWord->addParagraphStyle('pStyle', array('align'=>'center'));
$PHPWord->addParagraphStyle('pStyleAlignLeft', array('align'=>'left'));
$PHPWord->addParagraphStyle('pStyleAlignRight', array('align'=>'right'));


// New portrait section
$section = $PHPWord->createSection();


// Header
$header = $section->createHeader();
$table = $header->addTable();
$table->addRow();
$table->addCell(10000)->addText('มคอ.3', $fontStyle, 'pStyleAlignRight');


// Footer

// Add footer
$footer = $section->createFooter();
$footer->addPreserveText('สำนักงานคณะกรรมการอุดมศึกษา                                                                                   {PAGE}', $fontStyle, 'pStyleAlignLeft');


/*
$section->addText('I am inline styled.', array('name'=>'Verdana', 'color'=>'006699'));
$section->addTextBreak(2);

$PHPWord->addFontStyle('rStyle', array('bold'=>true, 'italic'=>true, 'size'=>16));
$PHPWord->addParagraphStyle('pStyle', array('align'=>'center', 'spaceAfter'=>100));
$section->addText('I am styled by two style definitions.', 'rStyle', 'pStyle');
$section->addText('I have only a paragraph style definition.', null, 'pStyle');
*/

// Define table style arrays
//'borderSize'=>1, 'borderColor'=>'000000', 
$styleTable = array('cellMargin'=>0);

$styleTableBordered = array('borderSize'=>1, 'borderColor'=>'000000', 'cellMargin'=>10);
//$styleFirstRow = array('borderBottomSize'=>18, 'borderBottomColor'=>'0000FF', 'bgColor'=>'66BBFF');

// Define cell style arrays
//$styleCell = array('valign'=>'center');
//$styleCellBTLR = array('valign'=>'center', 'textDirection'=>PHPWord_Style_Cell::TEXT_DIR_BTLR);



// Add table style
$PHPWord->addTableStyle('TQFTableStyle', $styleTable, $styleFirstRow);
$PHPWord->addTableStyle('TQFTableStyleBordered', $styleTableBordered, $styleFirstRow);

// Add table
$table = $section->addTable('TQFTableStyle');



/*

PAGE 1

*/

// Add row
$table->addRow();

// Add cells
$table->addCell(3000, $styleCell)->addText('ชื่อสถาบันอุดมศึกษา', $fontStyleBold);
$table->addCell(10000, $styleCell)->addText($row_info['faculty'], $fontStyle);

$table->addRow();

$table->addCell(3000, $styleCell)->addText('วิทยาเขต/คณะ/ภาควิชา', $fontStyleBold);
$table->addCell(10000, $styleCell)->addText($row_info['department'], $fontStyle);

$section->addTextBreak(1);
$section->addText('หมวดที่ 1 ข้อมูลโดยทั่วไป', $fontStyleBold, 'pStyle');
$section->addTextBreak(2);


// Add table
$table = $section->addTable('TQFTableStyleBordered');

// Add row
$table->addRow();

// Add cells
$cell = $table->addCell(10000, $styleCell);
$cell->addText('1. รหัสและรายชื่อวิชา', $fontStyleBold);
$cell->addText("         {$row_info[course_number]} {$row_info[name]}
	{$row_info[eng_name]}", $fontStyle);

$credit_info = explode(" ", $row_info["credit"]);

$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('2. จำนวนหน่วยกิต', $fontStyleBold);
$cell->addText("         {$credit_info[0]} หน่วยกิต {$credit_info[1]}", $fontStyle);


$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('3. หลักสูตรและประเภทของวิชา', $fontStyleBold);
$cell->addText("         {$row_info[curriculum]}", $fontStyle);


$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('4. อาจารย์ผู้รับผิดชอบรายวิชาและอาจารย์ผู้สอน', $fontStyleBold);
foreach($teachers as $key=>$data){
	$cell->addText("         {$data}", $fontStyle);
}

$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('5. ภาคการศึกษา/ชั้นปีที่เรียน', $fontStyleBold);
$cell->addText("         ภาคการศึกษาที่ {$row_info[semester]} / ชั้นปีที่ {$row[year]}", $fontStyle);

$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('6. รายวิชาที่ต้องเรียนมาก่อน (Pre-requisite) (ถ้ามี)', $fontStyleBold);
if(count($pre_requisites)==0)
	$cell->addText("         ไม่มี", $fontStyle);
foreach($pre_requisites as $key=>$data){
	$cell->addText("         {$data}", $fontStyle);
}


$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('7. รายวิชาที่ต้องเรียนพร้อมกัน (Co-requisite) (ถ้ามี)', $fontStyleBold);
if(count($co_requisites)==0)
	$cell->addText("         ไม่มี", $fontStyle);
foreach($co_requisites as $key=>$data){
	$cell->addText("         {$data}", $fontStyle);
}


$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('8. สถานที่เรียน', $fontStyleBold);
$cell->addText("         {$row_info[study_place]}", $fontStyle);


$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('9. วันที่จัดทำหรือปรับปรุงรายละเอียดของรายวิชาครั้งล่าสุด', $fontStyleBold);
$cell->addText("         " . formatThaiDate($row_info['last_modified_date']), $fontStyle);


$section->addPageBreak();

/*

PAGE 2

*/


$section->addText('หมวดที่ 2 จุดมุ่งหมายและวัตถุประสงค์', $fontStyleBold, 'pStyle');
$section->addTextBreak(2);


// Add table
$table = $section->addTable('TQFTableStyleBordered');


$key = find_key($key_data,"Objective");
$data = $elements->$key->{'0'}->value;

$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('1. จุดมุ่งหมายของรายวิชา', $fontStyleBold);
$cell->addText("         {$data}", $fontStyle);


$key = find_key($key_data,"Development Purpose");
$data = $elements->$key->{'0'}->value;

$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('2.  วัตถุประสงค์ในการพัฒนา/ปรับปรุงรายวิชา', $fontStyleBold);
$cell->addText("         {$data}", $fontStyle);



$section->addTextBreak(1);

$section->addText('หมวดที่ 3 ลักษณะและการดำเนินการ', $fontStyleBold, 'pStyle');
$section->addTextBreak(1);

// Add table
$table = $section->addTable('TQFTableStyleBordered');

$key = find_key($key_data,"Course Description");
$data = $elements->$key->{'0'}->value;

$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('1. คำอธิบายรายวิชา', $fontStyleBold);
$cell->addText("         {$data}", $fontStyle);



$section->addTextBreak(1);

// Add table
$table = $section->addTable('TQFTableStyle');

// Add row
$table->addRow();

// Add cells
$table->addCell(10000, $styleCell)->addText('2.  จำนวนชั่วโมงที่ใช้ต่อภาคการศึกษา', $fontStyleBold);


$section->addTextBreak(1);

// Add table
$table = $section->addTable('TQFTableStyleBordered');
$table->addRow();
$table->addCell(2500, $styleCell)->addText('บรรยาย', $fontStyleBold, 'pStyle');
$table->addCell(2500, $styleCell)->addText('สอนเสริม', $fontStyleBold, 'pStyle');
$table->addCell(2500, $styleCell)->addText('การฝึกปฎิบัติ/งาน
ภาคสนาม/การฝึกงาน', $fontStyleBold, 'pStyle');
$table->addCell(2500, $styleCell)->addText('การศึกษาด้วยตัวเอง', $fontStyleBold, 'pStyle');

$table->addRow();

$key = find_key($key_data,"Lecture Hours");
$data = $elements->$key->{'option'}->{'0'};

$table->addCell(2500, $styleCell)->addText($data, $fontStyle, 'pStyleLeft');

$key = find_key($key_data,"Extra Hours");
$data = $elements->$key->{'option'}->{'0'};

$table->addCell(2500, $styleCell)->addText($data, $fontStyle, 'pStyleLeft');

$key = find_key($key_data,"Field Training Hours");
$data = $elements->$key->{'option'}->{'0'};

$table->addCell(2500, $styleCell)->addText($data, $fontStyle, 'pStyleLeft');

$key = find_key($key_data,"Self Study Hours");
$data = $elements->$key->{'option'}->{'0'};
$table->addCell(2500, $styleCell)->addText($data, $fontStyle, 'pStyleLeft');



$section->addPageBreak();

/*

PAGE 3

*/


$section->addText('หมวดที่ 4 การพัฒนาการเรียนรู้ของนักศึกษา', $fontStyleBold, 'pStyle');
$section->addTextBreak(2);


// Add table
$table = $section->addTable('TQFTableStyleBordered');

// Add row
$table->addRow();

// Add cells
$table->addCell(10000, $styleCell)->addText('1. คุณธรรมจริยธรรม', $fontStyleBold);

$key = find_key($key_data,"Moral");
$data = $elements->$key->{'0'}->value;

$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('     1.1 คุณธรรมจริยธรรมที่ต้องพัฒนา', $fontStyleBold);
if($data!="")
	$cell->addText("              {$data}", $fontStyle);

$key = find_key($key_data,"Moral Choices");
$data_set = $elements->$key->{'option'};
$i=0;
foreach($data_set as $key=>$data){
	$cell->addText("              - {$data}", $fontStyle);
	$i++;
}


$key = find_key($key_data,"Teaching Method");
$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('     1.2 วิธีการสอน', $fontStyleBold);
$data_set = $elements->$key->{'option'};
$i=0;
foreach($data_set as $key=>$data){
	$cell->addText("              - {$data}", $fontStyle);
	$i++;
}



$key = find_key($key_data,"Evaluation Method");


$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('     1.3 วิธีการประเมินผล', $fontStyleBold);
$data_set = $elements->$key->{'option'};
$i=0;
foreach($data_set as $key=>$data){
	$cell->addText("              - {$data}", $fontStyle);
	$i++;
}



$section->addPageBreak();

/*

PAGE 4

*/


// Add table
$table = $section->addTable('TQFTableStyleBordered');

// Add row
$table->addRow();

// Add cells
$table->addCell(10000, $styleCell)->addText('2. ความรู้', $fontStyleBold);

$key = find_key($key_data,"Knowledge");
$data = $elements->$key->{'0'}->value;

$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('     2.1 ความรู้ที่ต้องได้รับ', $fontStyleBold);
if($data!="")
	$cell->addText("              {$data}", $fontStyle);


$key = find_key($key_data,"Knowledge Choices");
$data_set = $elements->$key->{'option'};
$i=0;
foreach($data_set as $key=>$data){
	$cell->addText("              - {$data}", $fontStyle);
	$i++;
}



$key = find_key($key_data,"Knowledge Teaching Method");


$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('     2.2 วิธีการสอน', $fontStyleBold);
$data_set = $elements->$key->{'option'};
$i=0;
foreach($data_set as $key=>$data){
	$cell->addText("              - {$data}", $fontStyle);
	$i++;
}



$key = find_key($key_data,"Knowledge Evaluation Method");


$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('     2.3 วิธีการประเมินผล', $fontStyleBold);
$data_set = $elements->$key->{'option'};
$i=0;
foreach($data_set as $key=>$data){
	$cell->addText("              - {$data}", $fontStyle);
	$i++;
}



$table->addRow();

// Add cells
$table->addCell(10000, $styleCell)->addText('3. ทักษะทางปัญญา', $fontStyleBold);


$key = find_key($key_data,"Skill");
$data = $elements->$key->{'0'}->value;

$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('     3.1 ทักษะทางปัญญาที่ต้องพัฒนา', $fontStyleBold);
if($data!="")
	$cell->addText("              {$data}", $fontStyle);

$key = find_key($key_data,"Skill Choices");
$data_set = $elements->$key->{'option'};
$i=0;
foreach($data_set as $key=>$data){
	$cell->addText("              - {$data}", $fontStyle);
	$i++;
}



$key = find_key($key_data,"Skill Teaching Method");

$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('     3.2 วิธีการสอน', $fontStyleBold);
$data_set = $elements->$key->{'option'};
$i=0;
foreach($data_set as $key=>$data){
	$cell->addText("              - {$data}", $fontStyle);
	$i++;
}

$key = find_key($key_data,"Skill Evaluation Method");

$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('     3.3 วิธีการประเมินผล', $fontStyleBold);
$data_set = $elements->$key->{'option'};
$i=0;
foreach($data_set as $key=>$data){
	$cell->addText("              - {$data}", $fontStyle);
	$i++;
}


$section->addPageBreak();






/*

PAGE 5

*/


// Add table
$table = $section->addTable('TQFTableStyleBordered');

// Add row
$table->addRow();

// Add cells
$table->addCell(10000, $styleCell)->addText('4. ทักษะความสัมพันธ์ระหว่างบุคคลและความรับผิดชอบ', $fontStyleBold);

$key = find_key($key_data,"Human Relation");
$data = $elements->$key->{'0'}->value;

$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('     4.1 ทักษะความสัมพันธ์ระหว่างบุคคลและความรับผิดชอบที่ต้องพัฒนา', $fontStyleBold);
if($data!="")
	$cell->addText("              {$data}", $fontStyle);


$key = find_key($key_data,"Human Relation Choices");
$data_set = $elements->$key->{'option'};
$i=0;
foreach($data_set as $key=>$data){
	$cell->addText("              - {$data}", $fontStyle);
	$i++;
}


$key = find_key($key_data,"Human Relation Teaching Method");
$data = $elements->$key->{'option'}->{'0'};

$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('     4.2 วิธีการสอน', $fontStyleBold);
$data_set = $elements->$key->{'option'};
$i=0;
foreach($data_set as $key=>$data){
	$cell->addText("              - {$data}", $fontStyle);
	$i++;
}


$key = find_key($key_data,"Human Relation Evaluation Method");


$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('     4.3 วิธีการประเมินผล', $fontStyleBold);
$data_set = $elements->$key->{'option'};
$i=0;
foreach($data_set as $key=>$data){
	$cell->addText("              - {$data}", $fontStyle);
	$i++;
}

$table->addRow();

// Add cells
$table->addCell(10000, $styleCell)->addText('5. ทักษะการวิเคราะห์เชิงตัวเลข การสื่อสาร และการใช้เทคโนโลยีสารสนเทศ', $fontStyleBold);

$key = find_key($key_data,"Analysis");
$data = $elements->$key->{'0'}->value;

$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('     5.1 ทักษะการวิเคราะห์เชิงตัวเลข การสื่อสาร และการใช้เทคโนโลยีสารสนเทศที่ต้องพัฒนา', $fontStyleBold);
if($data!="")
	$cell->addText("              {$data}", $fontStyle);

$key = find_key($key_data,"Analysis Choices");
$data_set = $elements->$key->{'option'};
$i=0;
foreach($data_set as $key=>$data){
	$cell->addText("              - {$data}", $fontStyle);
	$i++;
}


$key = find_key($key_data,"Analysis Teaching Method");

$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('     5.2 วิธีการสอน', $fontStyleBold);
$data_set = $elements->$key->{'option'};
$i=0;
foreach($data_set as $key=>$data){
	$cell->addText("              - {$data}", $fontStyle);
	$i++;
}

$key = find_key($key_data,"Analysis Evaluation Method");

$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('     5.3 วิธีการประเมินผล', $fontStyleBold);
$data_set = $elements->$key->{'option'};
$i=0;
foreach($data_set as $key=>$data){
	$cell->addText("              - {$data}", $fontStyle);
	$i++;
}

$table->addRow();

// Add cells
$table->addCell(10000, $styleCell)->addText('6. ทักษะทางวิชาชีพ', $fontStyleBold);

$key = find_key($key_data,"Career");

$data = $elements->$key->{'0'}->value;

$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('     6.1 ทักษะทางวิชาชีพที่ต้องพัฒนา', $fontStyleBold);
if($data!="")
	$cell->addText("              {$data}", $fontStyle);

$key = find_key($key_data,"Career Choices");

$data_set = $elements->$key->{'option'};
$i=0;
foreach($data_set as $key=>$data){
	$cell->addText("              - {$data}", $fontStyle);
	$i++;
}


$key = find_key($key_data,"Career Teaching Method");

$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('     6.2 วิธีการสอน', $fontStyleBold);
$data_set = $elements->$key->{'option'};
$i=0;
foreach($data_set as $key=>$data){
	$cell->addText("              - {$data}", $fontStyle);
	$i++;
}

$key = find_key($key_data,"Career Evaluation Method");

$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText('     6.3 วิธีการประเมินผล', $fontStyleBold);
$data_set = $elements->$key->{'option'};
$i=0;
foreach($data_set as $key=>$data){
	$cell->addText("              - {$data}", $fontStyle);
	$i++;
}


$section->addPageBreak();



/*

PAGE 6

*/


$section->addText('หมวดที่ 5 แผนการสอนและการประเมินผล', $fontStyleBold, 'pStyle');
$section->addTextBreak(2);

// Add table
$table = $section->addTable('TQFTableStyleBordered');


$table->addRow();
// Add cells
$table->addCell(10000, $styleCell)->addText('1. แผนการสอน', $fontStyleBold);

$section->addTextBreak(1);

$table = $section->addTable('TQFTableStyleBordered');

$table->addRow();
$table->addCell(500, $styleCell)->addText('สัปดาห์
ที่', $fontStyleBold, 'pStyle');
$table->addCell(3000, $styleCell)->addText('หัวข้อ/รายละเอียด', $fontStyleBold, 'pStyle');
$table->addCell(500, $styleCell)->addText('จำนวน
ชั่วโมง', $fontStyleBold, 'pStyle');
$table->addCell(3000, $styleCell)->addText('กิจกรรมการเรียน การ
สอน สื่อที่ใช้(ถ้ามี)', $fontStyleBold, 'pStyle');
$table->addCell(3000, $styleCell)->addText('ผู้สอน', $fontStyleBold, 'pStyle');

$planning_result = $mysqli->query("select * from tqf3_planning where submission_id='{$_REQUEST[id]}' order by week asc");
$plannings = array();

while($planning_row = $planning_result->fetch_array()){

	$table->addRow();
	$table->addCell(500, $styleCell)->addText($planning_row["week"], $fontStyle, 'pStyle');
	$table->addCell(3000, $styleCell)->addText($planning_row["detail"], $fontStyle);
	$table->addCell(500, $styleCell)->addText($planning_row["hour"], $fontStyle, 'pStyle');
	$table->addCell(3000, $styleCell)->addText($planning_row["learning_activity"], $fontStyle);

	$planning_row["teacher"] = str_replace("|", "\n", $planning_row["teacher"]);
	$table->addCell(3000, $styleCell)->addText($planning_row["teacher"], $fontStyle);

}

$section->addPageBreak();


/*

PAGE 7

*/


// Add table
$table = $section->addTable('TQFTableStyleBordered');


$table->addRow();
// Add cells
$table->addCell(10000, $styleCell)->addText('2. แผนการประเมินผลการเรียนรู้', $fontStyleBold);

$section->addTextBreak(1);

$table = $section->addTable('TQFTableStyleBordered');

$table->addRow();
$table->addCell(2000, $styleCell)->addText('กิจกรรมที่', $fontStyleBold, 'pStyle');
$table->addCell(2000, $styleCell)->addText('ผลการเรียนรู้', $fontStyleBold, 'pStyle');
$table->addCell(2000, $styleCell)->addText('วิธีการประเมิน', $fontStyleBold, 'pStyle');
$table->addCell(2000, $styleCell)->addText('สัปดาห์
ที่ประเมิน', $fontStyleBold, 'pStyle');
$table->addCell(2000, $styleCell)->addText('สัดส่วนของ
การประเมินผล', $fontStyleBold, 'pStyle');

/*
for($i=0;$i<5;$i++){
	$table->addRow();
	$table->addCell(2000, $styleCell)->addText($i+1, $fontStyle);
	$table->addCell(2000, $styleCell)->addText('Lorem ipsum', $fontStyle);
	$table->addCell(2000, $styleCell)->addText('Lorem ipsum', $fontStyle);
	$table->addCell(2000, $styleCell)->addText('Lorem ipsum', $fontStyle);
	$table->addCell(2000, $styleCell)->addText('Lorem ipsum', $fontStyle);
}*/

$evaluation_result = $mysqli->query("select * from tqf3_evaluation where submission_id='{$_REQUEST[id]}' order by activity_id asc");
$evaluations = array();

while($evaluation_row = $evaluation_result->fetch_array()){
	$table->addRow();
	$table->addCell(500, $styleCell)->addText($evaluation_row["activity_id"], $fontStyle, 'pStyle');
	$table->addCell(3000, $styleCell)->addText($evaluation_row["learning_result"], $fontStyle);
	$table->addCell(500, $styleCell)->addText($evaluation_row["evaluation_detail"], $fontStyle);
	$table->addCell(3000, $styleCell)->addText($evaluation_row["week"], $fontStyle);
	$table->addCell(3000, $styleCell)->addText($evaluation_row["total_percent"], $fontStyle, 'pStyle');
}

$section->addTextBreak(2);

$section->addText('หมวดที่ 6 ทรัพยากรประกอบการเรียนการสอน', $fontStyleBold, 'pStyle');
$section->addTextBreak(2);

// Add table
$table = $section->addTable('TQFTableStyleBordered');


$key = find_key($key_data,"Text Book");
$data = $elements->$key->{'0'}->value;

// Add row
$table->addRow();
// Add cells
$table->addCell(3000, $styleCell)->addText("1. เอกสารและตำราหลัก 
	{$data}", $fontStyleBold);

$key = find_key($key_data,"Important Document");
$data = $elements->$key->{'0'}->value;

$table->addRow();
$table->addCell(10000, $styleCell)->addText("2. เอกสารและข้อมูลสำคัญ
	{$data}", $fontStyleBold);

$key = find_key($key_data,"Suggested Document");
$data = $elements->$key->{'0'}->value;

$table->addRow();
$table->addCell(10000, $styleCell)->addText("3. เอกสารและข้อมูลแนะนำ
	{$data}", $fontStyleBold);

$section->addPageBreak();


/*

PAGE 8

*/

$section->addText('หมวดที่ 7 การประเมินและปรับปรุงการดำเนินการของรายวิชา', $fontStyleBold, 'pStyle');
$section->addTextBreak(2);

// Add table
$table = $section->addTable('TQFTableStyleBordered');



// Add row
$table->addRow();
// Add cells
$cell = $table->addCell(3000, $styleCell);
$cell->addText("1. กลยุทธ์การประเมินประสิทธิผลของรายวิชาโดยนักศึกษา", $fontStyleBold);

$key = find_key($key_data,"Student Evaluation Strategy");
$data_set = $elements->$key->{'option'};
$i=0;
foreach($data_set as $key=>$data){
	$cell->addText("              - {$data}", $fontStyle);
	$i++;
}

$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText("2. กลยุทธ์การประเมินการสอน", $fontStyleBold);

$key = find_key($key_data,"Evaluation Strategy");
$data_set = $elements->$key->{'option'};
$i=0;
foreach($data_set as $key=>$data){
	$cell->addText("              - {$data}", $fontStyle);
	$i++;
}

$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText("3. การปรับปรุงการสอน", $fontStyleBold);


$key = find_key($key_data,"Improvement");
$data_set = $elements->$key->{'option'};
$i=0;
foreach($data_set as $key=>$data){
	$cell->addText("              - {$data}", $fontStyle);
	$i++;
}

$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText("4. การทวนสอบมาตรฐานผลสัมฤทธิ์ของนักศึกษาในรายวิชา", $fontStyleBold);

$key = find_key($key_data,"Student Standard Evaluation");
$data_set = $elements->$key->{'option'};
$i=0;
foreach($data_set as $key=>$data){
	$cell->addText("              - {$data}", $fontStyle);
	$i++;
}

$table->addRow();
$cell = $table->addCell(10000, $styleCell);
$cell->addText("5. การดำเนินการทบทวนและการวางแผนปรับปรุงประสิทธิผลของรายวิชา", $fontStyleBold);

$key = find_key($key_data,"Improvement Planning");
$data_set = $elements->$key->{'option'};
$i=0;
foreach($data_set as $key=>$data){
	$cell->addText("              - {$data}", $fontStyle);
	$i++;
}

// Save File
$objWriter = PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
$objWriter->save('tqf3_report/tqf3.docx');

header("location: tqf3_report/tqf3.docx");
?>
<a href="tqf3_report/tqf3.docx" target="_blank">Download</a>