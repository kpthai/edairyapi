<?php

/**
 * @version     CVS: 1.0.0
 * @package     com_edairy
 * @subpackage  mod_edairy
 * @author      Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright   2017 Natchapol Kittigul
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

// Include the syndicate functions only once
JLoader::register('ModEdairyHelper', dirname(__FILE__) . '/helper.php');

$doc = JFactory::getDocument();

/* */
$doc->addStyleSheet(JURI::base() . '/media/mod_edairy/css/style.css');

/* */
$doc->addScript(JURI::base() . '/media/mod_edairy/js/script.js');

require JModuleHelper::getLayoutPath('mod_edairy', $params->get('content_type', 'blank'));
