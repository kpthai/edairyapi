<?php

/**
 * @version     CVS: 1.0.0
 * @package     com_edairy
 * @subpackage  mod_edairy
 * @author      Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright   2017 Natchapol Kittigul
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Helper for mod_edairy
 *
 * @package     com_edairy
 * @subpackage  mod_edairy
 * @since       1.6
 */
class ModEdairyHelper
{
	/**
	 * Retrieve component items
	 *
	 * @param   Joomla\Registry\Registry &$params module parameters
	 *
	 * @return array Array with all the elements
	 */
	public static function getList(&$params)
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		/* @var $params Joomla\Registry\Registry */
		$query
			->select('*')
			->from($params->get('table'))
			->where('state = 1');

		$db->setQuery($query, $params->get('offset'), $params->get('limit'));
		$rows = $db->loadObjectList();

		return $rows;
	}

	/**
	 * Retrieve component items
	 *
	 * @param   Joomla\Registry\Registry &$params module parameters
	 *
	 * @return mixed stdClass object if the item was found, null otherwise
	 */
	public static function getItem(&$params)
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		/* @var $params Joomla\Registry\Registry */
		$query
			->select('*')
			->from($params->get('item_table'))
			->where('id = ' . intval($params->get('item_id')));

		$db->setQuery($query);
		$element = $db->loadObject();

		return $element;
	}

	/**
	 * Render element
	 *
	 * @param   Joomla\Registry\Registry $table_name  Table name
	 * @param   string                   $field_name  Field name
	 * @param   string                   $field_value Field value
	 *
	 * @return string
	 */
	public static function renderElement($table_name, $field_name, $field_value)
	{
		$result = '';
		
		switch ($table_name)
		{
			
		case '#__ed_cow_insemination':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'run_no':
		$result = $field_value;
		break;
		case 'create_date':
		$result = $field_value;
		break;
		case 'address':
		$result = $field_value;
		break;
		case 'payment_type':
		$result = $field_value;
		break;
		case 'user_id1':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		case 'user_id2':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		case 'grand_total':
		$result = $field_value;
		break;
		case 'farm_id':
		$result = self::loadValueFromExternalTable('#__ed_farm', 'id', '', $field_value);
		break;
		case 'farmer_id':
		$result = self::loadValueFromExternalTable('#__ed_farmer', 'id', '', $field_value);
		break;
		case 'coop_id':
		$result = self::loadValueFromExternalTable('#__ed_coop', 'id', '', $field_value);
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_cow_insemination_line':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'cow_id':
		$result = $field_value;
		break;
		case 'create_date':
		$result = $field_value;
		break;
		case 'men_interval':
		$result = $field_value;
		break;
		case 'insemination_time':
		$result = $field_value;
		break;
		case 'lot_no':
		$result = $field_value;
		break;
		case 'amount':
		$result = $field_value;
		break;
		case 'price':
		$result = $field_value;
		break;
		case 'total_price':
		$result = $field_value;
		break;
		case 'cow_insemination_id':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_receive_milk_quantity':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'run_no':
		$result = $field_value;
		break;
		case 'create_datetime':
		$result = $field_value;
		break;
		case 'round':
		$result = $field_value;
		break;
		case 'total_amount':
		$result = $field_value;
		break;
		case 'coop_id':
		$result = self::loadValueFromExternalTable('#__ed_coop', 'id', '', $field_value);
		break;
		case 'farmer_id':
		$result = self::loadValueFromExternalTable('#__ed_farmer', 'id', '', $field_value);
		break;
		case 'receiver_id':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		case 'remark':
		$result = $field_value;
		break;
		case 'denied_amount':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_receive_milk_quantity_line':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'receive_milk_quantity_id':
		$result = $field_value;
		break;
		case 'amount':
		$result = $field_value;
		break;
		case 'farmer_id':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_receive_milk_quantity_cow':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'create_date':
		$result = $field_value;
		break;
		case 'cow_id':
		$result = self::loadValueFromExternalTable('#__ed_cow', 'id', '', $field_value);
		break;
		case 'morning_amount':
		$result = $field_value;
		break;
		case 'evening_amount':
		$result = $field_value;
		break;
		case 'total_amount':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_send_milk_quantity':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'run_no':
		$result = $field_value;
		break;
		case 'create_date':
		$result = $field_value;
		break;
		case 'coop_id':
		$result = self::loadValueFromExternalTable('#__ed_coop', 'id', '', $field_value);
		break;
		case 'leave_time':
		$result = $field_value;
		break;
		case 'license_plate':
		$result = $field_value;
		break;
		case 'driver_name':
		$result = $field_value;
		break;
		case 'litre_amount':
		$result = $field_value;
		break;
		case 'kg_amount':
		$result = $field_value;
		break;
		case 'temperature':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_cow_statistic':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'create_date':
		$result = $field_value;
		break;
		case 'farmer_id':
		$result = self::loadValueFromExternalTable('#__ed_farmer', 'id', '', $field_value);
		break;
		case 'farm_id':
		$result = self::loadValueFromExternalTable('#__ed_farm', 'id', '', $field_value);
		break;
		case 'total1':
		$result = $field_value;
		break;
		case 'total2':
		$result = $field_value;
		break;
		case 'total3':
		$result = $field_value;
		break;
		case 'total4':
		$result = $field_value;
		break;
		case 'total5':
		$result = $field_value;
		break;
		case 'total6':
		$result = $field_value;
		break;
		case 'total7':
		$result = $field_value;
		break;
		case 'total8':
		$result = $field_value;
		break;
		case 'total9':
		$result = $field_value;
		break;
		case 'total10':
		$result = $field_value;
		break;
		case 'total11':
		$result = $field_value;
		break;
		case 'total12':
		$result = $field_value;
		break;
		case 'total13':
		$result = $field_value;
		break;
		case 'total14':
		$result = $field_value;
		break;
		case 'total15':
		$result = $field_value;
		break;
		case 'total_female':
		$result = $field_value;
		break;
		case 'total_male':
		$result = $field_value;
		break;
		case 'total_cow':
		$result = $field_value;
		break;
		case 'remark':
		$result = $field_value;
		break;
		case 'create_by':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_cow_birth_inspection':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'cow_id':
		$result = self::loadValueFromExternalTable('#__ed_cow', 'id', '', $field_value);
		break;
		case 'birth_date':
		$result = $field_value;
		break;
		case 'birth_method':
		$result = $field_value;
		break;
		case 'is_child_alive':
		$result = $field_value;
		break;
		case 'child_gender':
		$result = $field_value;
		break;
		case 'child_weight':
		$result = $field_value;
		break;
		case 'child_cow_id':
		$result = self::loadValueFromExternalTable('#__ed_cow', 'id', '', $field_value);
		break;
		case 'create_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_cow_pregnant_inspection':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'cow_id':
		$result = self::loadValueFromExternalTable('#__ed_cow', 'id', '', $field_value);
		break;
		case 'create_date':
		$result = $field_value;
		break;
		case 'inspection_result':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_cow_medicine':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'name':
		$result = $field_value;
		break;
		case 'unit':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_cow_health':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'run_no':
		$result = $field_value;
		break;
		case 'create_date':
		$result = $field_value;
		break;
		case 'address':
		$result = $field_value;
		break;
		case 'payment_type':
		$result = $field_value;
		break;
		case 'user_id1':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		case 'user_id2':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		case 'grand_total':
		$result = $field_value;
		break;
		case 'farm_id':
		$result = self::loadValueFromExternalTable('#__ed_farm', 'id', '', $field_value);
		break;
		case 'farmer_id':
		$result = self::loadValueFromExternalTable('#__ed_farmer', 'id', '', $field_value);
		break;
		case 'coop_id':
		$result = self::loadValueFromExternalTable('#__ed_coop', 'id', '', $field_value);
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_cow_health_service':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'name':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_cow_health_sub_service':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'name':
		$result = $field_value;
		break;
		case 'cow_health_service_id':
		$result = self::loadValueFromExternalTable('#__ed_cow_health_service', 'id', 'name', $field_value);
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_cow_health_line':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'cow_id':
		$result = $field_value;
		break;
		case 'cow_name':
		$result = $field_value;
		break;
		case 'is_not_specify':
		$result = $field_value;
		break;
		case 'cow_health_service_id':
		$result = $field_value;
		break;
		case 'cow_health_sub_service_id':
		$result = $field_value;
		break;
		case 'cow_medicine_id':
		$result = $field_value;
		break;
		case 'amount':
		$result = $field_value;
		break;
		case 'price':
		$result = $field_value;
		break;
		case 'total_price':
		$result = $field_value;
		break;
		case 'cow_health_id':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_cow_heal_result':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'cow_id':
		$result = self::loadValueFromExternalTable('#__ed_cow', 'id', '', $field_value);
		break;
		case 'create_date':
		$result = $field_value;
		break;
		case 'heal_result':
		$result = $field_value;
		break;
		case 'cow_health_line_id':
		$result = self::loadValueFromExternalTable('#__ed_cow_health_line', 'id', '', $field_value);
		break;
		case 'vet_id':
		$result = $field_value;
		break;
		case 'create_by':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_cow_stop_milking':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'cow_id':
		$result = self::loadValueFromExternalTable('#__ed_cow', 'id', '', $field_value);
		break;
		case 'create_date':
		$result = $field_value;
		break;
		case 'create_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_region':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'name':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_province':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'name':
		$result = $field_value;
		break;
		case 'region_id':
		$result = self::loadValueFromExternalTable('#__ed_region', 'id', 'name', $field_value);
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_district':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'name':
		$result = $field_value;
		break;
		case 'province_id':
		$result = self::loadValueFromExternalTable('#__ed_province', 'id', 'name', $field_value);
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_sub_district':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'name':
		$result = $field_value;
		break;
		case 'district_id':
		$result = self::loadValueFromExternalTable('#__ed_district', 'id', 'name', $field_value);
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_coop':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'coop_code':
		$result = $field_value;
		break;
		case 'coop_abbr':
		$result = $field_value;
		break;
		case 'name':
		$result = $field_value;
		break;
		case 'address':
		$result = $field_value;
		break;
		case 'region_id':
		$result = self::loadValueFromExternalTable('#__ed_region', 'id', 'name', $field_value);
		break;
		case 'province_id':
		$result = self::loadValueFromExternalTable('#__ed_province', 'id', 'name', $field_value);
		break;
		case 'district_id':
		$result = self::loadValueFromExternalTable('#__ed_district', 'id', 'name', $field_value);
		break;
		case 'sub_district_id':
		$result = self::loadValueFromExternalTable('#__ed_sub_district', 'id', 'name', $field_value);
		break;
		case 'post_code':
		$result = $field_value;
		break;
		case 'latitude':
		$result = $field_value;
		break;
		case 'longitude':
		$result = $field_value;
		break;
		case 'create_by':
		$result = $field_value;
		break;
		case 'update_by':
		$result = $field_value;
		break;
		case 'create_datetime':
		$result = $field_value;
		break;
		case 'update_datetime':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_coop_gmp_certify':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'coop_id':
		$result = self::loadValueFromExternalTable('#__ed_coop', 'id', '', $field_value);
		break;
		case 'certify_no':
		$result = $field_value;
		break;
		case 'certify_date':
		$result = $field_value;
		break;
		case 'expire_date':
		$result = $field_value;
		break;
		case 'certify_agency':
		$result = $field_value;
		break;
		case 'create_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		case 'update_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		case 'create_datetime':
		$result = $field_value;
		break;
		case 'update_datetime':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_coop_send_milk_company':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'coop_id':
		$result = self::loadValueFromExternalTable('#__ed_coop', 'id', '', $field_value);
		break;
		case 'name':
		$result = $field_value;
		break;
		case 'address':
		$result = $field_value;
		break;
		case 'create_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		case 'update_by':
		$result = $field_value;
		break;
		case 'create_datetime':
		$result = $field_value;
		break;
		case 'update_datetime':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_coop_has_mou':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'coop_id':
		$result = self::loadValueFromExternalTable('#__ed_coop', 'id', '', $field_value);
		break;
		case 'year':
		$result = $field_value;
		break;
		case 'daily_amount':
		$result = $field_value;
		break;
		case 'create_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		case 'update_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		case 'create_datetime':
		$result = $field_value;
		break;
		case 'update_datetime':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_cow_project':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'name':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_farm_condition':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'name':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_performance_improvement_project':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'name':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_milking_system':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'name':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_farm':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'member_code':
		$result = $field_value;
		break;
		case 'name':
		$result = $field_value;
		break;
		case 'coop_id':
		$result = self::loadValueFromExternalTable('#__ed_coop', 'id', '', $field_value);
		break;
		case 'address':
		$result = $field_value;
		break;
		case 'province_id':
		$result = self::loadValueFromExternalTable('#__ed_province', 'id', 'name', $field_value);
		break;
		case 'region_id':
		$result = self::loadValueFromExternalTable('#__ed_region', 'id', 'name', $field_value);
		break;
		case 'district_id':
		$result = self::loadValueFromExternalTable('#__ed_district', 'id', 'name', $field_value);
		break;
		case 'sub_district_id':
		$result = self::loadValueFromExternalTable('#__ed_sub_district', 'id', 'name', $field_value);
		break;
		case 'post_code':
		$result = $field_value;
		break;
		case 'latitude':
		$result = $field_value;
		break;
		case 'longitude':
		$result = $field_value;
		break;
		case 'farm_project_id':
		$result = self::loadValueFromExternalTable('#__ed_cow_project', 'id', 'name', $field_value);
		break;
		case 'status':
		$result = $field_value;
		break;
		case 'is_join_breeding_system':
		$result = $field_value;
		break;
		case 'farm_condition_id':
		$result = $field_value;
		break;
		case 'environment':
		$result = $field_value;
		break;
		case 'performance_improvement_project_id':
		$result = self::loadValueFromExternalTable('#__ed_performance_improvement_project', 'id', 'name', $field_value);
		break;
		case 'milking_system_id':
		$result = self::loadValueFromExternalTable('#__ed_milking_system', 'id', 'name', $field_value);
		break;
		case 'waste_disposal_type':
		$result = $field_value;
		break;
		case 'member_start_date':
		$result = $field_value;
		break;
		case 'breeding_start_date':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_farm_gap_certify':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'farm_id':
		$result = self::loadValueFromExternalTable('#__ed_farm', 'id', '', $field_value);
		break;
		case 'certify_no':
		$result = $field_value;
		break;
		case 'certify_date':
		$result = $field_value;
		break;
		case 'expire_date':
		$result = $field_value;
		break;
		case 'certify_agency':
		$result = $field_value;
		break;
		case 'create_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		case 'update_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		case 'create_datetime':
		$result = $field_value;
		break;
		case 'update_datetime':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_farm_grade_certify':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'farm_id':
		$result = self::loadValueFromExternalTable('#__ed_farm', 'id', '', $field_value);
		break;
		case 'grade':
		$result = $field_value;
		break;
		case 'certify_date':
		$result = $field_value;
		break;
		case 'book_no':
		$result = $field_value;
		break;
		case 'create_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		case 'update_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		case 'create_datetime':
		$result = $field_value;
		break;
		case 'update_datetime':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_farm_grade_announcement':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'year':
		$result = $field_value;
		break;
		case 'announce_date':
		$result = $field_value;
		break;
		case 'detail':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_farmer':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'citizen_id':
		$result = $field_value;
		break;
		case 'image_url':
						if (!empty($field_value)) {
							$result = JPATH_ADMINISTRATOR . 'components/com_edairy/upload//' . $field_value;
						} else {
							$result = $field_value;
						}
		break;
		case 'title_id':
		$result = $field_value;
		break;
		case 'name':
		$result = $field_value;
		break;
		case 'surname':
		$result = $field_value;
		break;
		case 'birthdate':
		$result = $field_value;
		break;
		case 'education_level_id':
		$result = $field_value;
		break;
		case 'address':
		$result = $field_value;
		break;
		case 'region_id':
		$result = self::loadValueFromExternalTable('#__ed_region', 'id', 'name', $field_value);
		break;
		case 'province_id':
		$result = self::loadValueFromExternalTable('#__ed_province', 'id', 'name', $field_value);
		break;
		case 'district_id':
		$result = self::loadValueFromExternalTable('#__ed_district', 'id', 'name', $field_value);
		break;
		case 'sub_district_id':
		$result = self::loadValueFromExternalTable('#__ed_sub_district', 'id', 'name', $field_value);
		break;
		case 'post_code':
		$result = $field_value;
		break;
		case 'latitude':
		$result = $field_value;
		break;
		case 'longitude':
		$result = $field_value;
		break;
		case 'mobile':
		$result = $field_value;
		break;
		case 'fax':
		$result = $field_value;
		break;
		case 'email':
		$result = $field_value;
		break;
		case 'line_id':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_farm_has_farmer':
		switch($field_name){
		case 'farm_id':
		$result = $field_value;
		break;
		case 'farmer_id':
		$result = $field_value;
		break;
		case 'own_date':
		$result = $field_value;
		break;
		case 'id':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_training_program':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'name':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_farmer_training':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'farmer_id':
		$result = self::loadValueFromExternalTable('#__ed_farmer', 'id', '', $field_value);
		break;
		case 'training_program_id':
		$result = self::loadValueFromExternalTable('#__ed_training_program', 'id', 'name', $field_value);
		break;
		case 'training_date':
		$result = $field_value;
		break;
		case 'training_place':
		$result = $field_value;
		break;
		case 'training_day':
		$result = $field_value;
		break;
		case 'create_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		case 'update_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		case 'create_datetime':
		$result = $field_value;
		break;
		case 'update_datetime':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_farm_score':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'farm_id':
		$result = self::loadValueFromExternalTable('#__ed_farm', 'id', '', $field_value);
		break;
		case 'assessment_date':
		$result = $field_value;
		break;
		case 'score':
		$result = $field_value;
		break;
		case 'score_result':
		$result = $field_value;
		break;
		case 'assessment_data':
		$result = $field_value;
		break;
		case 'remark':
		$result = $field_value;
		break;
		case 'create_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		case 'update_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		case 'create_datetime':
		$result = $field_value;
		break;
		case 'update_datetime':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_cow':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'farm_id':
		$result = self::loadValueFromExternalTable('#__ed_farm', 'id', '', $field_value);
		break;
		case 'cow_id':
		$result = $field_value;
		break;
		case 'name':
		$result = $field_value;
		break;
		case 'dld_id':
		$result = $field_value;
		break;
		case 'ear_id':
		$result = $field_value;
		break;
		case 'birthdate':
		$result = $field_value;
		break;
		case 'gender':
		$result = $field_value;
		break;
		case 'move_farm_date':
		$result = $field_value;
		break;
		case 'move_type':
		$result = $field_value;
		break;
		case 'move_from_farm_id':
		$result = self::loadValueFromExternalTable('#__ed_farm', 'id', '', $field_value);
		break;
		case 'breed':
		$result = $field_value;
		break;
		case 'father_cow_id':
		$result = self::loadValueFromExternalTable('#__ed_cow', 'id', '', $field_value);
		break;
		case 'mother_cow_id':
		$result = self::loadValueFromExternalTable('#__ed_cow', 'id', '', $field_value);
		break;
		case 'create_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		case 'update_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		case 'create_datetime':
		$result = $field_value;
		break;
		case 'update_datetime':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_cow_status':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'name':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_cow_has_cow_status':
		switch($field_name){
		case 'cow_id':
		$result = $field_value;
		break;
		case 'status_code':
		$result = $field_value;
		break;
		case 'cow_status_id':
		$result = $field_value;
		break;
		case 'status_date':
		$result = $field_value;
		break;
		case 'create_by':
		$result = $field_value;
		break;
		case 'update_by':
		$result = $field_value;
		break;
		case 'create_date':
		$result = $field_value;
		break;
		case 'update_date':
		$result = $field_value;
		break;
		case 'id':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_cow_move_out':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'cow_id':
		$result = self::loadValueFromExternalTable('#__ed_cow', 'id', '', $field_value);
		break;
		case 'move_out_date':
		$result = $field_value;
		break;
		case 'move_type':
		$result = $field_value;
		break;
		case 'from_farm_id':
		$result = self::loadValueFromExternalTable('#__ed_farm', 'id', '', $field_value);
		break;
		case 'to_farm_id':
		$result = self::loadValueFromExternalTable('#__ed_farm', 'id', '', $field_value);
		break;
		case 'move_objective':
		$result = $field_value;
		break;
		case 'move_reason':
		$result = $field_value;
		break;
		case 'create_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		case 'update_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		case 'create_date':
		$result = $field_value;
		break;
		case 'update_date':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_cow_breeding':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'cow_id':
		$result = self::loadValueFromExternalTable('#__ed_cow', 'id', '', $field_value);
		break;
		case 'round':
		$result = $field_value;
		break;
		case 'no':
		$result = $field_value;
		break;
		case 'breeding_date':
		$result = $field_value;
		break;
		case 'father_id':
		$result = self::loadValueFromExternalTable('#__ed_cow', 'id', '', $field_value);
		break;
		case 'lot_no':
		$result = $field_value;
		break;
		case 'semen_source':
		$result = $field_value;
		break;
		case 'inspection_date':
		$result = $field_value;
		break;
		case 'inspection_result':
		$result = $field_value;
		break;
		case 'vet_id':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		case 'create_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		case 'update_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		case 'create_datetime':
		$result = $field_value;
		break;
		case 'update_datetime':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		case '#__ed_cow_has_child':
		switch($field_name){
		case 'id':
		$result = $field_value;
		break;
		case 'cow_id':
		$result = $field_value;
		break;
		case 'breeding_date':
		$result = $field_value;
		break;
		case 'father_id':
		$result = $field_value;
		break;
		case 'stop_milking_date':
		$result = $field_value;
		break;
		case 'birth_date':
		$result = $field_value;
		break;
		case 'child_status':
		$result = $field_value;
		break;
		case 'child_gender':
		$result = $field_value;
		break;
		case 'child_weight':
		$result = $field_value;
		break;
		case 'child_ear_id':
		$result = $field_value;
		break;
		case 'vet_id':
		$result = $field_value;
		break;
		case 'create_by':
		$result = $field_value;
		break;
		case 'update_by':
		$result = $field_value;
		break;
		case 'create_date':
		$result = $field_value;
		break;
		case 'update_date':
		$result = $field_value;
		break;
		case 'created_by':
		$user = JFactory::getUser($field_value);
		$result = $user->name;
		break;
		}
		break;
		}

		return $result;
	}

	/**
	 * Returns the translatable name of the element
	 *
	 * @param   Joomla\Registry\Registry &$params Module parameters
	 * @param   string                   $field   Field name
	 *
	 * @return string Translatable name.
	 */
	public static function renderTranslatableHeader(&$params, $field)
	{
		return JText::_(
			'MOD_EDAIRY_HEADER_FIELD_' . str_replace('#__', '', strtoupper($params->get('table'))) . '_' . strtoupper($field)
		);
	}

	/**
	 * Checks if an element should appear in the table/item view
	 *
	 * @param   string $field name of the field
	 *
	 * @return boolean True if it should appear, false otherwise
	 */
	public static function shouldAppear($field)
	{
		$noHeaderFields = array('checked_out_time', 'checked_out', 'ordering', 'state');

		return !in_array($field, $noHeaderFields);
	}

	

    /**
     * Method to get a value from a external table
     * @param string $source_table Source table name
     * @param string $key_field Source key field 
     * @param string $value_field Source value field
     * @param mixed  $key_value Value for the key field
     * @return mixed The value in the external table or null if it wasn't found
     */
    private static function loadValueFromExternalTable($source_table, $key_field, $value_field, $key_value) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query
                ->select($db->quoteName($value_field))
                ->from($source_table)
                ->where($db->quoteName($key_field) . ' = ' . $db->quote($key_value));


        $db->setQuery($query);
        return $db->loadResult();
    }
}
