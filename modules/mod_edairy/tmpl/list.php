<?php
/**
 * @version     CVS: 1.0.0
 * @package     com_edairy
 * @subpackage  mod_edairy
 * @author      Natchapol Kittigul <nat.k@dreamvector.co.th>
 * @copyright   2017 Natchapol Kittigul
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
$elements = ModEdairyHelper::getList($params);
?>

<?php if (!empty($elements)) : ?>
	<table class="table">
		<?php foreach ($elements as $element) : ?>
			<tr>
				<th><?php echo ModEdairyHelper::renderTranslatableHeader($params, $params->get('field')); ?></th>
				<td><?php echo ModEdairyHelper::renderElement(
						$params->get('table'), $params->get('field'), $element->{$params->get('field')}
					); ?></td>
			</tr>
		<?php endforeach; ?>
	</table>
<?php endif;
